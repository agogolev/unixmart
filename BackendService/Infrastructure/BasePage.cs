﻿using System;
using System.Web;
using System.Web.UI;
using Autofac;

namespace BackendService.Infrastructure
{
	public abstract class BasePage<T> : Page where T : class 
	{
        protected BasePage()
        {
            InjectDependencies();
        }

        private void InjectDependencies()
        {
            HttpContext context = HttpContext.Current;

            var accessor = context?.ApplicationInstance as IContainerAccessor;

            if (accessor == null)
                return;

            IContainer container = accessor.GetContainer();

            if (container == null)
                throw new InvalidOperationException(
                    "Container on Global Application Class is Null. Cannot perform BuildUp.");

            container.InjectUnsetProperties(this as T);
        }

        protected override void OnPreInit(EventArgs e)
		{
			InjectDependencies();
			base.OnPreInit(e);
		}

	}
}