﻿using System;
using System.Web;
using System.Web.Services;
using Autofac;

namespace BackendService.Infrastructure
{
    public abstract class BaseService<T> : WebService where T : class
    {
        protected BaseService()
        {
            InjectDependencies();
        }

        private void InjectDependencies()
        {
            HttpContext context = HttpContext.Current;

            if (context == null)
                return;

            var accessor = context.ApplicationInstance as IContainerAccessor;

            if (accessor == null)
                return;

            IContainer container = accessor.GetContainer();

            if (container == null)
                throw new InvalidOperationException(
                    "Container on Global Application Class is Null. Cannot perform BuildUp.");

            container.InjectUnsetProperties(this as T);
        }
    }
}