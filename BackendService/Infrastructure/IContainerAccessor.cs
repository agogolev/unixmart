﻿using Autofac;

namespace BackendService.Infrastructure
{
    public interface IContainerAccessor
    {
        IContainer GetContainer();
    }
}
