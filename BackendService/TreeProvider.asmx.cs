using System.Collections;
using System.Data;
using System.Text;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Services.Protocols;
using BackendService.Infrastructure;
using DBReader;
using MetaData;

namespace BackendService
{
	[WebService(Namespace="http://www.invento.ru/BackendService")]
	public class TreeProvider : AuthentificatedService<TreeProvider>
    {
		[WebMethod, SoapHeader("CurrentUser")]
		public string GetCategoryTree() 
		{
			SqlCommand cmd = DBReader.QueryConstructor.GetCommand(buildGetCategoryTree, CommandType.Text, Usr);
			cmd.Connection.Open();
			StringBuilder sb = new StringBuilder();
			sb.Append("<?xml version=\"1.0\" encoding=\"Windows-1251\" ?><root>");
			try {
				SqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read()) {
					sb.Append((string)dr[0]);
				}
			}
			finally {
				sb.Append("</root>");
				cmd.Connection.Close();
			}
			return sb.ToString();
		}

	const string buildGetCategoryTree = @"
SELECT DISTINCT
	Tag = 1,
	Parent = null,
	[category!1!OID] = t.OID,
	[category!1!] = t.themeName
FROM
	t_Theme t
ORDER BY
	[category!1!], [category!1!OID], Tag
FOR XML EXPLICIT";

		[WebMethod, SoapHeader("CurrentUser")]
		public DataSet GetTreeContent(string contentDescription) 
		{
			return DBReader.TreeProvider.GetTreeContent(contentDescription.RestoreCr(), Usr); 
		}
		[WebMethod]
		public int CreateNode(string nodeDescription) 
		{
			int result = DBReader.TreeProvider.CreateNode(nodeDescription);

			//сохранили - теперь обнуляем веб кэш
			WebServiceExtensions.ClearWebCache("www_");

			return result; 
		}
		[WebMethod]
		public void UpdateNode(string nodeDescription) 
		{
			DBReader.TreeProvider.UpdateNode(nodeDescription);

			//сохранили - теперь обнуляем веб кэш
			WebServiceExtensions.ClearWebCache("www_");
		}
		[WebMethod, SoapHeader("CurrentUser")]
		public void DeleteNode(int nodeID) 
		{
			string sql = "spDeleteNode";
			Hashtable prms = new Hashtable();
			prms["nodeID"] = nodeID;
			DBReader.DBHelper.ExecuteCommand(sql, prms, true);

			//удалили - теперь обнуляем веб кэш
			WebServiceExtensions.ClearWebCache("www_");
		}

		[WebMethod(MessageName="DeleteNodeAndChildren"), SoapHeader("CurrentUser")]
		public void DeleteNode(int nodeID, bool deleteChildren) 
		{
			string sql = "spDeleteNode";
			Hashtable prms = new Hashtable();
			prms["nodeID"] = nodeID;
			prms["deleteChildren"] = deleteChildren;
			DBReader.DBHelper.ExecuteCommand(sql, prms, true);

			//удалили - теперь обнуляем веб кэш
			WebServiceExtensions.ClearWebCache("www_");
		}

	}
}
