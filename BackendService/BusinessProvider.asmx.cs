using System;
using System.Collections;
using System.Data;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using DBReader;
using Ecommerce.Repository.CommodityGroup;
using MetaData;
using ELB.Services;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BackendService.Properties;
using NLog;
using Ecommerce.Extensions;
using System.Data.SqlClient;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BackendService.Infrastructure;
using ECommerce.LoadData;
using NoName.Repository.Contracts;
using BasketContent = ELB.Domain.BasketContent;

namespace BackendService
{
    /// <summary>
    /// Summary description for BusinessProvider.
    /// </summary>
    /// 
    [WebService(Namespace = "http://www.invento.ru/BackendService")]
    public class BusinessProvider : AuthentificatedService<BusinessProvider>
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public ICommodityGroupRepository CommodityGroupRepository { get; set; }

        public IGoodsRepository GoodsRepository { get; set; }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetStockInfo(int shopType, int minStockAmount, int minShopAmount, int minPathAmount, int minCentralStockAmount, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string svLogin;
            string svPassword;
            if (shopType == 1 && shops.Contains(shopType))
            {
                svLogin = Settings.Default.MskSVLogin;
                svPassword = Settings.Default.MskSVPassword;
            }
            else
            {
                exISM = new ExceptionISM("Данный магазин не обслуживается", "NotValidShop");
                return null;
            }
            try
            {
                return ExportToSVService.GetStockInfo(minStockAmount, minShopAmount, minPathAmount, minCentralStockAmount, svLogin, svPassword);
            }
            catch (Exception ex)
            {
                exISM = new ExceptionISM(ex);
                return null;
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public IAsyncResult BeginGetStockInfo1C(AsyncCallback callback, object state)
        {
            CheckSession();
            var tcs = new TaskCompletionSource<string>(state);
            var service = new GoodsManager();
            Task<string> task = service.GetContent(Settings.Default.ApiUrl);
            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                    tcs.TrySetException(t.Exception.InnerExceptions);
                else if (t.IsCanceled)
                    tcs.TrySetCanceled();
                else
                    tcs.TrySetResult(t.Result);

                callback?.Invoke(tcs.Task);
            });

            return tcs.Task;
        }

        [WebMethod]
        public string EndGetStockInfo1C(IAsyncResult result)
        {
            return ((Task<string>)result).GetAwaiter().GetResult();
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public IAsyncResult BeginGetAccessoryInfo1C(AsyncCallback callback, object state)
        {
            CheckSession();
            var tcs = new TaskCompletionSource<string>(state);
            var service = new AccessoriesManager();
            Task<string> task = service.GetContent(Settings.Default.ApiUrl);
            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                    tcs.TrySetException(t.Exception.InnerExceptions);
                else if (t.IsCanceled)
                    tcs.TrySetCanceled();
                else
                    tcs.TrySetResult(t.Result);

                callback?.Invoke(tcs.Task);
            });

            return tcs.Task;
        }

        [WebMethod]
        public string EndGetAccessoryInfo1C(IAsyncResult result)
        {
            return ((Task<string>)result).GetAwaiter().GetResult();
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void ExportOrder(int orderId, int shopType, out ExceptionISM exISM)
        {
            exISM = null;
            //var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(s => int.Parse(s)).ToList();
            //CheckSession();
            //string svLogin;
            //string svPassword;
            //string defaultCity;
            //if (shopType == 1 && shops.Contains(shopType))
            //{
            //    svLogin = Settings.Default.MskSVLogin;
            //    svPassword = Settings.Default.MskSVPassword;
            //    defaultCity = Settings.Default.MskDefaultCity;
            //}
            //else if (shopType == 2 && shops.Contains(shopType))
            //{
            //    svLogin = Settings.Default.SpbSVLogin;
            //    svPassword = Settings.Default.SpbSVPassword;
            //    defaultCity = Settings.Default.SpbDefaultCity;
            //}
            //else if (shopType == 3 && shops.Contains(shopType))
            //{
            //    svLogin = Settings.Default.NskSVLogin;
            //    svPassword = Settings.Default.NskSVPassword;
            //    defaultCity = Settings.Default.NskDefaultCity;
            //}
            //else if (shopType == 4 && shops.Contains(shopType))
            //{
            //    svLogin = Settings.Default.KdrSVLogin;
            //    svPassword = Settings.Default.KdrSVPassword;
            //    defaultCity = Settings.Default.KdrDefaultCity;
            //}
            //else return;
            //try
            //{
            //    Order item = OrderService.GetOrderById(orderId);
            //    ExportToSVService.ExportOrder(item, svLogin, svPassword, defaultCity);
            //    OrderService.SetOrderSent(orderId);
            //}
            //catch (Exception e)
            //{
            //    exISM = new ExceptionISM(e);
            //}
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void StatusReady(int orderId, int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string svLogin;
            string svPassword;
            if (shopType == 1 && shops.Contains(shopType))
            {
                svLogin = Settings.Default.MskSVLogin;
                svPassword = Settings.Default.MskSVPassword;
            }
            else
            {
                return;
            }

            try
            {
                ExportToSVService.StatusReady(orderId, svLogin, svPassword);
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public decimal CalcDeliveryPrice(int deliveryType, BasketContent[] orderContent, out ExceptionISM exISM)
        {
            exISM = null;
            return 0;
            //CheckSession();
            //try
            //{
            //    IList<BasketContent> items = OrderService.GetOrderContent(orderContent);
            //    return DeliveryService.GetAddPrice(deliveryType) + DeliveryService.CalcDeliveryPrice(items);
            //}
            //catch (Exception e)
            //{
            //    exISM = new ExceptionISM(e);
            //    return 0;
            //}
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetDataSetISM(int shopType, string sql, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string svLogin;
            string svPassword;
            if (shopType == 1 && shops.Contains(shopType))
            {
                svLogin = Settings.Default.MskSVLogin;
                svPassword = Settings.Default.MskSVPassword;
            }
            else
            {
                exISM = new ExceptionISM("Данный магазин не обслуживается", "NotValidShop");
                return null;
            }

            try
            {
                DataSetISM ds = ExportToSVService.GetDataSetISM(sql.RestoreCr(), null, false, svLogin, svPassword);
                return ds.SaveToString();
            }
            catch (Exception ex)
            {
                exISM = new ExceptionISM(ex);
                return null;
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshYandexYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            YMService.Timeout = 360000;
            string exception = YMService.RefreshMarketData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshMarketDataException");
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshSiteMap(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            YMService.Timeout = 600000;
            string exception = YMService.RefreshSiteMapData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshSiteMapDataException");
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshAdmitadYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            YMService.Timeout = 360000;
            string exception = YMService.RefreshAdmitadData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshAdmitadYml");
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshGoogleYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            string exception = YMService.RefreshGoogleData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshGoogleYml");
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshWikimartYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            string exception = YMService.RefreshWikimartData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshWikimartYml");
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshWikiYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            string exception = YMService.RefreshWikiData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshWikiYml");
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshNadaviYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            string exception = YMService.RefreshNadaviData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshNadaviYml");
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void RefreshUtinetYml(int shopType, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            exISM = null;
            CheckSession();
            string url;
            if (shopType == 1 && shops.Contains(shopType))
            {
                url = Settings.Default.MskUrl;
            }
            else
            {
                return;
            }

            var YMService = new YandexMarketWebService.YandexMarketWebService();
            var re = new Regex("https?://[^/]*/");
            YMService.Url = re.Replace(YMService.Url, string.Format("{0}/", url));
            string exception = YMService.RefreshUtinetData();
            if (exception != null)
            {
                exISM = new ExceptionISM(exception, "RefreshUtinetYml");
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public ExceptionISM RefreshLandingPageCount(int shopType)
        {
            CheckSession();
            try
            {
                var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
                string url;
                if (shopType == 1 && shops.Contains(shopType))
                {
                    url = Settings.Default.MskUrl;
                }
                else
                {
                    return new ExceptionISM("invalid shopType", "RefreshLandingPageCount");
                }

                const string sql = @"
SELECT
    o.objectId,
    o.shortLink
FROM
    t_Object o 
    inner join t_LandingPage l on l.OID = o.OID
";
                IEnumerable<DataRow> rows = sql.ExecSql();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{url}");
                    foreach (var r in rows)
                    {
                        int id = r.Field<int>("objectId");
                        string shortLink = r.Field<string>("shortLink");
                        HttpResponseMessage response = client.GetAsync($"api/catalog/{shortLink}").Result;
                        int goodsCount = 0;
                        if (response.IsSuccessStatusCode)
                        {
                            string result = response.Content.ReadAsStringAsync().Result;
                            int.TryParse(result, out goodsCount);
                        }
                        DBHelper.ExecuteCommand(@"
UPDATE t_LandingPage 
    SET actualGoodsCount = @goodsCount
FROM 
    t_LandingPage l 
    inner join t_Object o on l.OID = o.OID 
WHERE 
    o.objectId = @id
", new Hashtable { { "goodsCount", goodsCount }, { "id", id } }, false);

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Message: {ex.Message}");
                return new ExceptionISM(ex.Message, "RefreshLandingPageCount");
            }
            return null;
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetGoodsUrl(int shopType, int objectId)
        {
            CheckSession();
            return GoodsService.GetGoodsUrl(shopType, new[] { objectId }).DefaultIfEmpty(new KeyValuePair<int, string>(0, "")).First().Value;
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public List<XElement> GetGoodsUrlList(int shopType, int[] objectIds)
        {
            CheckSession();
            return GoodsService.GetGoodsUrl(shopType, objectIds)
                .Select(item => new XElement("item", new XElement("key", item.Key), new XElement("value", item.Value))).ToList();
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetCategoryUrl(int shopType, int objectId)
        {
            //var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            CheckSession();
                           //int rootNode;
                           //if (shopType == 1 && shops.Contains(shopType))
                           //{
                           //    rootNode = Settings.Default.MskCatalog;
                           //}
                           //else if (shopType == 2 && shops.Contains(shopType))
                           //{
                           //    rootNode = Settings.Default.SpbCatalog;
                           //}
                           //else if (shopType == 3 && shops.Contains(shopType))
                           //{
                           //    rootNode = Settings.Default.NskCatalog;
                           //}
                           //else if (shopType == 4 && shops.Contains(shopType))
                           //{
                           //    rootNode = Settings.Default.KdrCatalog;
                           //}
                           //else return null;
            return CatalogService.GetCategoryUrl(objectId);
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void GenerateAllCommodityGroupContent(int shopType, out ExceptionISM exISM)
        {
            CheckSession();
            exISM = null;
            try
            {
                const string sql = @"
SELECT * FROM t_CommodityGroup
WHERE isActive = 1 and shopType = @shopType
ORDER BY
	lastGenerate
";
                var ds = sql.ExecSql(new { shopType }).AsEnumerable().ToList();
                foreach (var oid in ds.Select(row => row.Field<Guid>("OID")))
                {
                    CommodityGroupRepository.GenerateCommodityGroupContent(oid, true, true, true);
                    GenerateCommodityGroupXml(oid, out exISM);
                }
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void GenerateCommodityGroupXml(Guid OID, out ExceptionISM exISM)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            CheckSession();
            exISM = null;
            try
            {
                var shopType = "SELECT shopType FROM t_CommodityGroup WHERE OID = @OID".ExecSql(new { OID }).Select(row => row.Field<int>("shopType")).SingleOrDefault();
                string siteUrl;
                if (shopType == 1 && shops.Contains(shopType))
                {
                    siteUrl = Settings.Default.MskUrl;
                }
                else
                {
                    return;
                }

                var service = new YandexMarketWebService.YandexMarketWebService { Url = string.Format("{0}/YandexMarketWebService.asmx", siteUrl) };
                string ret = service.RefreshCommodityData(OID);
                if (ret != null) exISM = new ExceptionISM(ret, "ServiceException");
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetYandexGroupGoodsContent(Guid OID, out ExceptionISM exISM)
        {
            CheckSession();
            exISM = null;
            try
            {
                int? shopType = 0;
                Guid? commodityGroup = Guid.Empty;
                int? rootNode = 0;
                @"
SELECT c.OID, c.shopType, ts.themeRootNode FROM 
	t_CommodityGroup c
	inner join t_YandexGroup y on c.OID = y.commodityGroup and y.OID = @OID
	inner join t_TypeShop ts on c.shopType = ts.shopType".ExecSql(new { OID })
    .ToList()
    .ForEach(row =>
    {
        shopType = row.Field<int?>("shopType");
        rootNode = row.Field<int?>("themeRootNode");
        commodityGroup = row.Field<Guid?>("OID");
    });

                string sql = string.Format(@"
SELECT
	g.OID,
	goodsName = ISNULL(g.productType, '')+' '+ISNULL(c.companyName, '') + ' ' + ISNULL(g.model, ''),
	g.model,
	g.postavshik_id,
	o.objectID
FROM
	{0}

", CommodityGroupRepository.GetCommodityGroupContentSql(commodityGroup ?? Guid.Empty));

                return DBHelper.GetDataSetISMSql(sql,
                    null,
                    new Hashtable { { "shopType", shopType ?? 0 }, { "rootNode", rootNode ?? 0 } },
                    false,
                    null).SaveToString();
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
                return null;
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetUnAssignedOrder(int shopType, int startNumber, out ExceptionISM exISM)
        {
            CheckSession();
            exISM = null;
            try
            {
                return OrderService.GetUnAssignedOrders(shopType, startNumber).SaveToString();
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
                return null;
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void AssignOrder(Guid OID, string svID, out ExceptionISM exISM)
        {
            CheckSession();
            exISM = null;
            try
            {
                OrderService.AssignOrder(OID, svID);
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public DataSet GetJobsLookupContent()
        {
            CheckSession();

            var jobs = Settings.Default.Jobs.Split(",".ToCharArray()).Select(item => new Guid(item)).ToList();
            string sql = string.Format(@"
SELECT * FROM t_TypeErrorLevel WHERE [level] > 2

SELECT * FROM t_Job WHERE OID {0}
", jobs.InClause());
            using (var conn = new SqlConnection(Settings.Default.JobServiceConnection))
            {
                conn.Open();
                return DBHelper.GetDataSetMTSql(sql, null, false, conn, null);
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public DataSetISM GetJobLog(Guid job, int level, DateTime dateBegin, DateTime dateEnd)
        {
            CheckSession();
            var sql = new StringBuilder();
            sql.Append(@"
SELECT * FROM t_JobServiceLog 
WHERE 
	job = {0} and
	dateOccur BETWEEN {1} and {2}");
            if (level > 0) sql.Append(" and [level] = {3}");
            using (var conn = new SqlConnection(Settings.Default.JobServiceConnection))
            {
                conn.Open();
                return DBHelper.GetDataSetMTSql(string.Format(sql.ToString(), job.ToSqlString(), dateBegin.ToSqlString(), dateEnd.ToSqlString(), level.ToSqlString()),
                    null, false, conn, null);
            }

        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetAppSettings(int shopType, string optionName)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            CheckSession();
            var fileName = "";
            const string sectionName = "NoName.Repository.Properties.Settings";
            if (shopType == 1 && shops.Contains(shopType))
            {
                fileName = Settings.Default.MskWebConfigPath;
            }
            else
            {
                return fileName;
            }
            fileName += "web.config";
            var doc = XDocument.Load(fileName);
            if (CheckExistense(doc, sectionName, optionName))
            {
                return doc.Descendants("applicationSettings")
                    .Single()
                    .Elements(sectionName)
                    .Single()
                    .Elements("setting")
                    .Single(e => e.Attribute("name").Value == optionName)
                    .Elements("value")
                    .Single()
                    .Value;
            }
            return "";
        }

        private bool CheckExistense(XDocument doc, string sectionName, string optionName)
        {
            return doc.Descendants("applicationSettings")
               .Single()
               .Elements(sectionName)
               .Single()
               .Elements("setting")
               .Count(e => e.Attribute("name").Value == optionName) != 0;
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void SetAppSettings(int shopType, string optionName, string optionValue)
        {
            var shops = Settings.Default.ActiveShops.Split(",".ToCharArray()).Select(int.Parse).ToList();
            CheckSession();
            string fileName;
            const string sectionName = "NoName.Repository.Properties.Settings";
            if (shopType == 1 && shops.Contains(shopType))
            {
                fileName = Settings.Default.MskWebConfigPath;
            }
            else
            {
                return;
            }
            fileName += "web.config";
            var doc = XDocument.Load(fileName);
            if (CheckExistense(doc, sectionName, optionName))
            {
                doc.Descendants("applicationSettings")
                   .Single()
                   .Elements(sectionName)
                   .Single()
                   .Elements("setting")
                   .Single(e => e.Attribute("name").Value == optionName)
                   .Elements("value")
                   .Single()
                   .Value = optionValue;
            }
            else
            {
                doc.Descendants("applicationSettings")
                   .Single()
                   .Elements(sectionName)
                   .Single()
                   .Add(new XElement("setting",
                           new XAttribute("name", optionName),
                           new XAttribute("serializeAs", "String"),
                           new XElement("value", optionValue)));
            }

            using (var wrt = new XmlTextWriter(fileName, Encoding.UTF8))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                doc.Save(wrt);
            }

        }
        [WebMethod]
        public CategoryFilters GetCategoryManufacturers(int shopType, int rootNode, Guid themeOID)
        {
            var sql = @"
SELECT TOP 1
    n.nodeID 
FROM 
    t_Nodes n 
    inner join t_Nodes n1 on n.tree = n.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
WHERE 
    n.object = @themeOID";
            int nodeID = sql.ExecSql(new { themeOID, rootNode }).AsEnumerable().Select(r => r.Field<int>("nodeID")).DefaultIfEmpty(0).Single();
            return new CategoryFilters
            {
                Manufacturers = GoodsRepository.GetManufacturers(shopType, nodeID).Select(m => new Manufacturer
                {
                    OID = m.OID,
                    Name = m.Name
                }).ToArray(),
                Filters = GoodsRepository.GetFilters(shopType, nodeID).Select(f => new CategoryFilter
                {
                    OID = f.ParamType,
                    Name = f.Name,
                    Values = f.Values.Select(v => new CategoryFilterValue
                    {
                        Id = v.Id,
                        Name = v.Name
                    }).ToArray()
                }).ToArray()
            };
        }
    }

    public class CategoryFilters
    {
        public Manufacturer[] Manufacturers { get; set; }
        public CategoryFilter[] Filters { get; set; }
    }

    public class Manufacturer
    {
        public Guid OID { get; set; }

        public string Name { get; set; }
    }

    public class CategoryFilter
    {
        public Guid OID { get; set; }
        public string Name { get; set; }
        public CategoryFilterValue[] Values { get; set; }
    }

    public class CategoryFilterValue
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
