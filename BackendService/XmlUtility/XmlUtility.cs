using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Text;
using System.IO;

namespace BackendService.XmlUtility
{
	/// <summary>
	/// Summary description for XmlUtility.
	/// </summary>
	public class XmlUtility
	{
		public static string Reader2Xml(SqlDataReader dr, string rootTag) {
			StringBuilder sb = new StringBuilder();
			StringWriter wrt = new StringWriter(sb);
			XmlTextWriter w = new XmlTextWriter(wrt);
			w.Formatting = Formatting.Indented;
			if (rootTag != null) w.WriteStartElement(rootTag);
			while(dr.Read()) {
				w.WriteStartElement("row");
				for(int i = 0; i < dr.FieldCount; i++) {
					if(!dr.IsDBNull(i)) {
						w.WriteElementString(dr.GetName(i), dr[i].ToString());
					}
				}
				w.WriteEndElement();
			}
			if (rootTag != null) w.WriteEndElement();
			return sb.ToString();
		}
	}
}
