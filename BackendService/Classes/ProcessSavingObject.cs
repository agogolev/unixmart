using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.Globalization;
using DBReader;
using MetaData;
using System.Configuration;

namespace BackendService.Classes
{
	/// <summary>
	/// Summary description for ProcessSavingObject.
	/// </summary>
	public class ProcessSavingObject {
		
		public static string _connStr = ConfigurationManager.AppSettings["dbdata.connection"]; 
				
		//public static string SaveTheme(DBObject obj, string xml) {
		//  string result;
		//  XmlDocument doc = new XmlDocument();
		//  XmlDocument docParams = null;
		//  doc.LoadXml(xml);

		//  SqlConnection conn = new SqlConnection(_connStr);
		//  conn.Open();
		//  SqlTransaction trans = conn.BeginTransaction();
			
		//  try {
		//    XmlNode prms = doc.DocumentElement.SelectSingleNode("extMultiProp/Params");
		//    if (prms != null) {
		//      docParams = new XmlDocument();
		//      docParams.LoadXml(prms.OuterXml);
		//      prms.ParentNode.RemoveChild(prms);
		//    }

		//    //сохранение объекта
		//    result = obj.SaveXml(doc.OuterXml, conn, trans);

		//    if (docParams != null) {//если были переданы билеты
		//      //удалим записи о параметрах
		//      XmlNodeList nl = docParams.DocumentElement.SelectNodes("delete");
		//      string sql = "DELETE t_TemplateParams WHERE OID = @OID And  paramTypeOID = @paramTypeOID";
		//      Hashtable parms = new Hashtable();
		//      foreach(XmlNode n in nl) {
		//        parms["@OID"] = obj.OID;
		//        parms["@paramTypeOID"] = new Guid(n.SelectSingleNode("OID").InnerText);
		//        DBReader.DBHelper.ExecuteCommand(sql, parms, false, conn, trans);
		//      }

		//      //создадим новые записи о билетах
		//      nl = docParams.DocumentElement.SelectNodes("add");
		//      sql = "INSERT INTO t_TemplateParams VALUES(@OID, @paramTypeOID, @ordValue, @isMandatory, @isFullName, @isFilter)";
		//      foreach(XmlNode n in nl) {
		//        parms["@OID"] = obj.OID;
		//        parms["@paramTypeOID"] = new Guid(n.SelectSingleNode("OID").InnerText);
		//        parms["@ordValue"] = int.Parse(n.SelectSingleNode("ordValue").InnerText);
		//        parms["@isMandatory"] = bool.Parse(n.SelectSingleNode("isMandatory").InnerText);
		//        parms["@isFullName"] = bool.Parse(n.SelectSingleNode("isFullName").InnerText);
		//        parms["@isFilter"] = bool.Parse(n.SelectSingleNode("isFilter").InnerText);
		//        DBReader.DBHelper.ExecuteCommand(sql, parms, false, conn, trans);
		//      }
		//      //сохраним модифицированные строки записи о параметрах
		//      nl = docParams.DocumentElement.SelectNodes("modify");
		//      sql = "UPDATE t_TemplateParams SET ordValue = @ordValue, isMandatory = @isMandatory, isFullName = @isFullName, isFilter = @isFilter WHERE OID = @OID And  paramTypeOID = @paramTypeOID";
		//      foreach(XmlNode n in nl) {
		//        parms["@OID"] = obj.OID;
		//        parms["@paramTypeOID"] = new Guid(n.SelectSingleNode("OID").InnerText);
		//        parms["@ordValue"] = int.Parse(n.SelectSingleNode("ordValue").InnerText);
		//        parms["@isMandatory"] = bool.Parse(n.SelectSingleNode("isMandatory").InnerText);
		//        parms["@isFullName"] = bool.Parse(n.SelectSingleNode("isFullName").InnerText);
		//        parms["@isFilter"] = bool.Parse(n.SelectSingleNode("isFilter").InnerText);
		//        DBReader.DBHelper.ExecuteCommand(sql, parms, false, conn, trans);
		//      }
		//    }

		//    trans.Commit();
		//  }
		//  catch(Exception e) {
		//    trans.Rollback();
		//    throw e;
		//  }
		//  finally {			
		//    conn.Close();
		//  }
			
		//  return result;
		//}

		public static string SaveGoods(DBObject obj, string xml) {
			string result;
			XmlDocument doc = new XmlDocument();
			XmlDocument docParams = null;
			doc.LoadXml(xml);

			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();
			SqlTransaction trans = conn.BeginTransaction();
			
			try {
				XmlNode prms = doc.DocumentElement.SelectSingleNode("extMultiProp/Params");
				if (prms != null) {
					docParams = new XmlDocument();
					docParams.LoadXml(prms.OuterXml);
					prms.ParentNode.RemoveChild(prms);
				}

				//сохранение объекта
				result = obj.SaveXml(doc.OuterXml, conn, trans);
				string sqlDelete = @"
DELETE t_GoodsParams WHERE OID = @OID And  paramTypeOID = @paramTypeOID
";
				string sqlInsert = @"
INSERT INTO t_GoodsParams VALUES(@OID, @paramTypeOID, @ordValue, @value, @unit)";
				string sqlUpdate = @"
UPDATE t_GoodsParams SET ordValue = @ordValue, value = @value, unit = @unit WHERE OID = @OID And  paramTypeOID = @paramTypeOID";

				if (docParams != null) {//если были переданы параметры
					//удалим записи о параметрах
					XmlNodeList nl = docParams.DocumentElement.SelectNodes("delete");
					
					Hashtable parms = new Hashtable();
					foreach(XmlNode n in nl) {
						parms["OID"] = obj.OID;
						parms["paramTypeOID"] = new Guid(n.SelectSingleNode("OID").InnerText);
						DBReader.DBHelper.ExecuteCommand(sqlDelete, parms, false, conn, trans);
					}

					//создадим новые записи о параметрах
					nl = docParams.DocumentElement.SelectNodes("add");
					
					foreach(XmlNode n in nl) {
						Guid paramTypeOID = new Guid(n.SelectSingleNode("paramType").InnerText);
						int type = int.Parse(n.SelectSingleNode("type").InnerText);
						parms["OID"] = obj.OID;
						parms["ordValue"] = int.Parse(n.SelectSingleNode("ordValue").InnerText);
						parms["paramTypeOID"] = paramTypeOID;
						parms["value"] = GetSqlVariantValue(type, n.SelectSingleNode("value"));
						parms["unit"] = string.IsNullOrWhiteSpace(n.SelectSingleNode("unit").InnerText) ? "" : (object)n.SelectSingleNode("unit").InnerText;
						if (!string.IsNullOrWhiteSpace(parms["value"].ToString()))
							DBReader.DBHelper.ExecuteCommand(sqlInsert, parms, false, conn, trans);
						else
							DBReader.DBHelper.ExecuteCommand(sqlDelete, parms, false, conn, trans);
					}
					nl = docParams.DocumentElement.SelectNodes("modify");
					foreach(XmlNode n in nl) {
						Guid paramTypeOID = new Guid(n.SelectSingleNode("paramType").InnerText);
						int type = int.Parse(n.SelectSingleNode("type").InnerText);
						parms["OID"] = obj.OID;
						parms["ordValue"] = int.Parse(n.SelectSingleNode("ordValue").InnerText);
						parms["paramTypeOID"] = paramTypeOID;
						parms["value"] = GetSqlVariantValue(type, n.SelectSingleNode("value"));
						parms["unit"] = string.IsNullOrWhiteSpace(n.SelectSingleNode("unit").InnerText) ? "" : (object)n.SelectSingleNode("unit").InnerText;
						if (!string.IsNullOrWhiteSpace(parms["value"].ToString()))
							DBReader.DBHelper.ExecuteCommand(sqlUpdate, parms, false, conn, trans);
						else
							DBReader.DBHelper.ExecuteCommand(sqlDelete, parms, false, conn, trans);
					}
				}

				trans.Commit();
			}
			catch(Exception e) {
				trans.Rollback();
				throw e;
			}
			finally {			
				conn.Close();
			}
			
			return result;
		}

		public static string SaveOrder(DBObject obj, string xml) {
			string result;
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);

			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();
			SqlTransaction trans = conn.BeginTransaction();
			
			try {

				//сохранение объекта
				obj.LoadXml(doc.OuterXml);
				SavePrevOrder((COrder)obj, conn, trans);
				result = obj.SaveXml(doc.OuterXml, conn, trans);
				trans.Commit();
			}
			catch(Exception e) {
				trans.Rollback();
				throw e;
			}
			finally {			
				conn.Close();
			}
			
			return result;
		}

		private static void SavePrevOrder(COrder order, SqlConnection conn, SqlTransaction tran) {
			if(order.OID == Guid.Empty || order.Owner == Guid.Empty) return;
			Hashtable prms = new Hashtable();
			prms["orderOID"] = order.OID;
			string sql = @"upGetFullOrderInfo";
			DataSetISM ds = DBReader.DBHelper.GetDataSetISMSql(sql, null, prms, true, conn, tran);
			Guid owner = order.Owner;
			sql = @"
INSERT INTO t_ObjectVersions (OID, dateOccur, author, xmlContent)
VALUES (@orderOID, GetDate(), @owner, @xmlContent)
";
			prms["owner"] = owner;
			prms["xmlContent"] = ds.SaveToString();
			DBReader.DBHelper.ExecuteCommand(sql, prms, false, conn, tran);
		}
		#region HelperFunctions

		private static object GetSqlVariantValue(int paramType, XmlNode nValue)
		{
			if (nValue != null)
			{
				if (nValue.InnerText == "\0") return DBNull.Value;
				else
				{
					switch (paramType)
					{
						case ELBExpImp.Constants.STRING_PARAM:
							return nValue.InnerText;
						case ELBExpImp.Constants.BOOL_PARAM:
							return bool.Parse(nValue.InnerText);
						case ELBExpImp.Constants.INT_PARAM:
							return int.Parse(nValue.InnerText);
						case ELBExpImp.Constants.NUMERIC_PARAM:
							return decimal.Parse(nValue.InnerText);
						case ELBExpImp.Constants.DATE_PARAM:
							return DateTime.Parse(nValue.InnerText);
						default:
							return DBNull.Value;
					}
				}
			}
			else return DBNull.Value;
		}
//		protected static void ExecSQL(String sql, SqlConnection conn, SqlTransaction trans) {
//			SqlCommand cmd = new SqlCommand(sql, conn, trans);
//			try {
//				cmd.ExecuteNonQuery();
//			}
//			catch(Exception e) {
//				throw new DBException(cmd.CommandText, e.Message);
//			}			
//		}

//		protected static void SaveMultiProp(Guid OID, string ClassName, XmlNode propXml, SqlConnection conn, SqlTransaction trans) {			
//			string propName = propXml.Name;
//			XmlDocument doc = new XmlDocument();
//			XmlElement rt = doc.CreateElement("root");
//			rt.InnerXml = propXml.InnerXml;
//					
//			string processContent = "<?xml version=\"1.0\" encoding=\"Windows-1251\" ?>"+rt.OuterXml;
//			DBReader.CollectionAdapter ca = new DBReader.CollectionAdapter(OID.ToString(), ClassName, propName, processContent);
//			ca.AcceptChanges(conn, trans);								
//		}

		#endregion
	}
}
