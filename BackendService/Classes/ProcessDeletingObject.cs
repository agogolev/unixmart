using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.Globalization;
using System.Configuration;

namespace BackendService.Classes
{	
	public class ProcessDeletingObject
	{
		public static string _connStr = ConfigurationManager.AppSettings["dbdata.connection"]; 
		
		public static void DeletePoll(Guid OID) {
			//TemplateDelete(OID, "upDeleteDelivery");			
			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();
			SqlTransaction trans = conn.BeginTransaction();
			
			try {				
				Hashtable prms = new Hashtable();
				prms["tree"] = Poll;
				prms["OID"] = OID;			
				DBReader.DBHelper.ExecuteCommand("DELETE FROM t_Nodes WHERE tree = @tree and object = @OID", prms, false, conn, trans);

				prms.Clear();
				prms["OID"] = OID;
				DBReader.DBHelper.ExecuteCommand("spDeleteObject", prms, true, conn, trans);				
				
				trans.Commit();
			}
			catch(Exception e) {
				trans.Rollback();
				throw e;
			}
			finally {			
				conn.Close();
			}	
			

		}		

		public static void DeleteStringItem(Guid OID) {
			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();
			
			try {				
				string sql = @"
Declare
	@paramType uniqueidentifier,
	@value varchar(1000)
SELECT @paramType = paramType, @value = value FROM t_StringItem WHERE OID = @OID

if EXISTS (SELECT * FROM t_GoodsParams WHERE paramTypeOID = @paramType And value = @value) RAISERROR('Значение осталось в базе', 16, 1)
";
				Hashtable prms = new Hashtable();
				prms["OID"] = OID;			
				DBReader.DBHelper.ExecuteScalar(sql, prms, false);
				DBReader.DBHelper.ExecuteCommand("spDeleteObject", prms, true, conn, null);				
				
			}
			catch(Exception e) {
				throw e;
			}
			finally {			
				conn.Close();
			}	
			

		}		

		public static void DeleteGoods(Guid OID) {
			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();
			SqlTransaction trans = conn.BeginTransaction();
			
			try {				
				string sql = @"
	Declare 
		@cur CURSOR
		@paramOID uniqueidentifier

	Set @cur = CURSOR FOR SELECT paramOID FROM t_GoodsParams WHERE OID = @OID
	open @cur
	FETCH NEXT FROM @cur INTO @paramOID

	while @FETCH_STATUS = 0 Begin
		DELETE FROM t_GoodsParams WHERE OID = @OID And paramOID = @paramOID
		exec spDeleteObject @paramOID
		FETCH NEXT FROM @cur INTO @paramOID
	end
	close @cur
	deallocate @cur
	
	spDeleteObject @OID
";
				Hashtable prms = new Hashtable();
				prms["OID"] = OID;			
				DBReader.DBHelper.ExecuteCommand(sql, prms, false, conn, trans);
				trans.Commit();
			}
			catch(Exception e) {
				trans.Rollback();
				throw e;
			}
			finally {			
				conn.Close();
			}	
			

		}		

		#region HelperFunctions
		/// <summary>
		/// функция удаления объекта, сначала вызывается хранимая процедура в которую передается OID удаляемого объекта,
		/// потом удаляется сам объект
		/// </summary>
		/// <param name="procName">имя хранимой процедуры, которая будет вызвана перед удалением</param>
		protected static void TemplateDelete(Guid OID, string procName) 
		{
			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();
			SqlTransaction trans = conn.BeginTransaction();
			
			try
			{				
				//выполнение требуемых процедур
				string sqlProc = "exec " + procName+" '"+OID.ToString()+"'";	
				ExecSQL(sqlProc, conn, trans);

				//удаление объекта
				sqlProc = "exec spDeleteObject '"+OID.ToString()+"'";	
				ExecSQL(sqlProc, conn, trans);

				trans.Commit();
			}
			catch(Exception e) 
			{
				trans.Rollback();
				throw e;
			}
			finally 
			{			
				conn.Close();
			}			
		}

		protected static void ExecSQL(String sql, SqlConnection conn, SqlTransaction trans) 
		{			
			SqlCommand cmd = new SqlCommand(sql, conn, trans);
			cmd.ExecuteNonQuery();						
		}		
		
		protected static void ExecSQL(String sql) 
		{
			SqlConnection conn = new SqlConnection(_connStr);
			conn.Open();

			SqlCommand cmd = new SqlCommand(sql, conn);
			try 
			{
				cmd.ExecuteNonQuery();
			}						
			finally 
			{			
				conn.Close();
			}	
		}
		#endregion

		//дерево конструтора опросов
		private static Guid Poll = new Guid("F050043D-1B31-45FE-BCB2-B29D6D1E7155");
	
	}
}
