//------------------------------------------------------------------------------
// <auto-generated>
//     ���� ��� ������ ����������.
//     ����������� ������:2.0.50727.5466
//
//     ��������� � ���� ����� ����� �������� � ������������ ������ � ����� �������� � ������
//     ��������� ��������� ����.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BackendService.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using System.Data.SqlClient;
	
	
	public class CAction : CObject
	{
		
		private string _name;
		
		private bool _name_isModified;
		
		private DateTime _dateBegin;
		
		private bool _dateBegin_isModified;
		
		private DateTime _dateEnd;
		
		private bool _dateEnd_isModified;
		
		private bool _isPublic;
		
		private bool _isPublic_isModified;
		
		private Guid _description;
		
		private bool _description_isModified;
		
		private Guid _commodityGroup;
		
		private bool _commodityGroup_isModified;
		
		private int _actionType;
		
		private bool _actionType_isModified;
		
		private bool _geoInclusive;
		
		private bool _geoInclusive_isModified;
		
		public CAction(string connStr) : 
				base(connStr)
		{
			_name = string.Empty;
			_name_isModified = false;
			_dateBegin = DateTime.MinValue;
			_dateBegin_isModified = false;
			_dateEnd = DateTime.MinValue;
			_dateEnd_isModified = false;
			_isPublic = false;
			_isPublic_isModified = false;
			_description = Guid.Empty;
			_description_isModified = false;
			_commodityGroup = Guid.Empty;
			_commodityGroup_isModified = false;
			_actionType = int.MinValue;
			_actionType_isModified = false;
			_geoInclusive = false;
			_geoInclusive_isModified = false;
		}
		
		public CAction(string connStr, Guid OID) : 
				base(connStr, OID)
		{
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("19106297-DD4C-40D4-AA42-DC5464BC213E");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("19106297-DD4C-40D4-AA42-DC5464BC213E");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CAction";
			}
		}
		
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
				_name_isModified = true;
			}
		}
		
		public DateTime DateBegin
		{
			get
			{
				return _dateBegin;
			}
			set
			{
				_dateBegin = value;
				_dateBegin_isModified = true;
			}
		}
		
		public DateTime DateEnd
		{
			get
			{
				return _dateEnd;
			}
			set
			{
				_dateEnd = value;
				_dateEnd_isModified = true;
			}
		}
		
		public bool IsPublic
		{
			get
			{
				return _isPublic;
			}
			set
			{
				_isPublic = value;
				_isPublic_isModified = true;
			}
		}
		
		public Guid Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
				_description_isModified = true;
			}
		}
		
		public Guid CommodityGroup
		{
			get
			{
				return _commodityGroup;
			}
			set
			{
				_commodityGroup = value;
				_commodityGroup_isModified = true;
			}
		}
		
		public int ActionType
		{
			get
			{
				return _actionType;
			}
			set
			{
				_actionType = value;
				_actionType_isModified = true;
			}
		}
		
		public bool GeoInclusive
		{
			get
			{
				return _geoInclusive;
			}
			set
			{
				_geoInclusive = value;
				_geoInclusive_isModified = true;
			}
		}
		
		public override void BuildNameValues()
		{
			string ourString = "";
			if (_name_isModified)
			{
				ourString = ToOurString(_name);
				_fields["name"] = ourString;
			}
			if (_dateBegin_isModified)
			{
				ourString = ToOurString(_dateBegin);
				_fields["dateBegin"] = ourString;
			}
			if (_dateEnd_isModified)
			{
				ourString = ToOurString(_dateEnd);
				_fields["dateEnd"] = ourString;
			}
			if (_isPublic_isModified)
			{
				ourString = ToOurString(_isPublic);
				_fields["isPublic"] = ourString;
			}
			if (_description_isModified)
			{
				ourString = ToOurString(_description);
				_fields["description"] = ourString;
			}
			if (_commodityGroup_isModified)
			{
				ourString = ToOurString(_commodityGroup);
				_fields["commodityGroup"] = ourString;
			}
			if (_actionType_isModified)
			{
				ourString = ToOurString(_actionType);
				_fields["actionType"] = ourString;
			}
			if (_geoInclusive_isModified)
			{
				ourString = ToOurString(_geoInclusive);
				_fields["geoInclusive"] = ourString;
			}
			base.BuildNameValues();
		}
		
		public override void BuildFieldValues(SqlDataReader dr)
		{
			_name = ((string)(GetValue(dr, "name")));
			_name_isModified = false;
			_dateBegin = ((DateTime)(GetValue(dr, "dateBegin")));
			_dateBegin_isModified = false;
			_dateEnd = ((DateTime)(GetValue(dr, "dateEnd")));
			_dateEnd_isModified = false;
			_isPublic = ((bool)(GetValue(dr, "isPublic")));
			_isPublic_isModified = false;
			_description = ((Guid)(GetValue(dr, "description")));
			_description_isModified = false;
			_commodityGroup = ((Guid)(GetValue(dr, "commodityGroup")));
			_commodityGroup_isModified = false;
			_actionType = ((int)(GetValue(dr, "actionType")));
			_actionType_isModified = false;
			_geoInclusive = ((bool)(GetValue(dr, "geoInclusive")));
			_geoInclusive_isModified = false;
			base.BuildFieldValues(dr);
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("singleProp/name") != null) 
						&& (root.SelectSingleNode("singleProp/name").InnerText != null)))
			{
				Name = root.SelectSingleNode("singleProp/name").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/dateBegin") != null) 
						&& (root.SelectSingleNode("singleProp/dateBegin").InnerText != null)))
			{
				DateBegin = DateTime.Parse(root.SelectSingleNode("singleProp/dateBegin").InnerText, this.DateTimeFormat);
			}
			if (((root.SelectSingleNode("singleProp/dateEnd") != null) 
						&& (root.SelectSingleNode("singleProp/dateEnd").InnerText != null)))
			{
				DateEnd = DateTime.Parse(root.SelectSingleNode("singleProp/dateEnd").InnerText, this.DateTimeFormat);
			}
			if (((root.SelectSingleNode("singleProp/isPublic") != null) 
						&& (root.SelectSingleNode("singleProp/isPublic").InnerText != null)))
			{
				IsPublic = Convert.ToBoolean(root.SelectSingleNode("singleProp/isPublic").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/description") != null) 
						&& (root.SelectSingleNode("singleProp/description").InnerText != null)))
			{
				Description = new Guid(root.SelectSingleNode("singleProp/description").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/commodityGroup") != null) 
						&& (root.SelectSingleNode("singleProp/commodityGroup").InnerText != null)))
			{
				CommodityGroup = new Guid(root.SelectSingleNode("singleProp/commodityGroup").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/actionType") != null) 
						&& (root.SelectSingleNode("singleProp/actionType").InnerText != null)))
			{
				ActionType = Convert.ToInt32(root.SelectSingleNode("singleProp/actionType").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/geoInclusive") != null) 
						&& (root.SelectSingleNode("singleProp/geoInclusive").InnerText != null)))
			{
				GeoInclusive = Convert.ToBoolean(root.SelectSingleNode("singleProp/geoInclusive").InnerText);
			}
		}
		
		protected override void ProcessMultiProp()
		{
			base.ProcessMultiProp();
			if ((root.SelectSingleNode("multiProp/locations") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/locations/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					System.Guid deleted = new Guid(delNodes[i].SelectSingleNode("propValue").InnerText);
					this.DeleteMulti("locations", deleted, null);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/locations/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					this.AddMulti("locations", added, null);
				}
			}
		}
	}
}
