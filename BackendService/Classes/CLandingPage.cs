//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.8941
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BackendService.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using System.Data.SqlClient;
	
	
	public class CLandingPage : CObject
	{
		
		private string _name;
		
		private bool _name_isModified;
		
		private string _url;
		
		private bool _url_isModified;
		
		private Guid _commodityGroup;
		
		private bool _commodityGroup_isModified;
		
		private Guid _article;
		
		private bool _article_isModified;
		
		private int _actualGoodsCount;
		
		private bool _actualGoodsCount_isModified;
		
		private string _h1;
		
		private bool _h1_isModified;
		
		public CLandingPage(string connStr) : 
				base(connStr)
		{
			_name = string.Empty;
			_name_isModified = false;
			_url = string.Empty;
			_url_isModified = false;
			_commodityGroup = Guid.Empty;
			_commodityGroup_isModified = false;
			_article = Guid.Empty;
			_article_isModified = false;
			_actualGoodsCount = int.MinValue;
			_actualGoodsCount_isModified = false;
			_h1 = string.Empty;
			_h1_isModified = false;
		}
		
		public CLandingPage(string connStr, Guid OID) : 
				base(connStr, OID)
		{
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("33C55F3B-E427-4F98-B2FD-B1F8A7221E1D");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("33C55F3B-E427-4F98-B2FD-B1F8A7221E1D");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CLandingPage";
			}
		}
		
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
				_name_isModified = true;
			}
		}
		
		public string Url
		{
			get
			{
				return _url;
			}
			set
			{
				_url = value;
				_url_isModified = true;
			}
		}
		
		public Guid CommodityGroup
		{
			get
			{
				return _commodityGroup;
			}
			set
			{
				_commodityGroup = value;
				_commodityGroup_isModified = true;
			}
		}
		
		public Guid Article
		{
			get
			{
				return _article;
			}
			set
			{
				_article = value;
				_article_isModified = true;
			}
		}
		
		public int ActualGoodsCount
		{
			get
			{
				return _actualGoodsCount;
			}
			set
			{
				_actualGoodsCount = value;
				_actualGoodsCount_isModified = true;
			}
		}
		
		public string H1
		{
			get
			{
				return _h1;
			}
			set
			{
				_h1 = value;
				_h1_isModified = true;
			}
		}
		
		public override void BuildNameValues()
		{
			string ourString = "";
			if (_name_isModified)
			{
				ourString = ToOurString(_name);
				_fields["name"] = ourString;
			}
			if (_url_isModified)
			{
				ourString = ToOurString(_url);
				_fields["url"] = ourString;
			}
			if (_commodityGroup_isModified)
			{
				ourString = ToOurString(_commodityGroup);
				_fields["commodityGroup"] = ourString;
			}
			if (_article_isModified)
			{
				ourString = ToOurString(_article);
				_fields["article"] = ourString;
			}
			if (_actualGoodsCount_isModified)
			{
				ourString = ToOurString(_actualGoodsCount);
				_fields["actualGoodsCount"] = ourString;
			}
			if (_h1_isModified)
			{
				ourString = ToOurString(_h1);
				_fields["h1"] = ourString;
			}
			base.BuildNameValues();
		}
		
		public override void BuildFieldValues(SqlDataReader dr)
		{
			_name = ((string)(GetValue(dr, "name")));
			_name_isModified = false;
			_url = ((string)(GetValue(dr, "url")));
			_url_isModified = false;
			_commodityGroup = ((Guid)(GetValue(dr, "commodityGroup")));
			_commodityGroup_isModified = false;
			_article = ((Guid)(GetValue(dr, "article")));
			_article_isModified = false;
			_actualGoodsCount = ((int)(GetValue(dr, "actualGoodsCount")));
			_actualGoodsCount_isModified = false;
			_h1 = ((string)(GetValue(dr, "h1")));
			_h1_isModified = false;
			base.BuildFieldValues(dr);
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("singleProp/name") != null) 
						&& (root.SelectSingleNode("singleProp/name").InnerText != null)))
			{
				Name = root.SelectSingleNode("singleProp/name").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/url") != null) 
						&& (root.SelectSingleNode("singleProp/url").InnerText != null)))
			{
				Url = root.SelectSingleNode("singleProp/url").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/commodityGroup") != null) 
						&& (root.SelectSingleNode("singleProp/commodityGroup").InnerText != null)))
			{
				CommodityGroup = new Guid(root.SelectSingleNode("singleProp/commodityGroup").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/article") != null) 
						&& (root.SelectSingleNode("singleProp/article").InnerText != null)))
			{
				Article = new Guid(root.SelectSingleNode("singleProp/article").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/actualGoodsCount") != null) 
						&& (root.SelectSingleNode("singleProp/actualGoodsCount").InnerText != null)))
			{
				ActualGoodsCount = Convert.ToInt32(root.SelectSingleNode("singleProp/actualGoodsCount").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/h1") != null) 
						&& (root.SelectSingleNode("singleProp/h1").InnerText != null)))
			{
				H1 = root.SelectSingleNode("singleProp/h1").InnerText;
			}
		}
		
		protected override void ProcessMultiProp()
		{
			base.ProcessMultiProp();
		}
	}
}
