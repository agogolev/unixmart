//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.573
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace BackendService.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using System.Data.SqlClient;
	
	
	public class CBasket : CObject
	{
		
		private Guid _session;
		
		private bool _session_isModified;
		
		private string _comment;
		
		private bool _comment_isModified;
		
		private string _additionalEMail;
		
		private bool _additionalEMail_isModified;
		
		public CBasket(string connStr) : 
				base(connStr)
		{
			_session = Guid.Empty;
			_session_isModified = false;
			_comment = string.Empty;
			_comment_isModified = false;
			_additionalEMail = string.Empty;
			_additionalEMail_isModified = false;
		}
		
		public CBasket(string connStr, Guid OID) : 
				base(connStr, OID)
		{
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("3E8FE4F1-12DD-4C69-A7A3-1E0B7F5613CE");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("3E8FE4F1-12DD-4C69-A7A3-1E0B7F5613CE");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CBasket";
			}
		}
		
		public Guid Session
		{
			get
			{
				return _session;
			}
			set
			{
				_session = value;
				_session_isModified = true;
			}
		}
		
		public string Comment
		{
			get
			{
				return _comment;
			}
			set
			{
				_comment = value;
				_comment_isModified = true;
			}
		}
		
		public string AdditionalEMail
		{
			get
			{
				return _additionalEMail;
			}
			set
			{
				_additionalEMail = value;
				_additionalEMail_isModified = true;
			}
		}
		
		public override void BuildNameValues()
		{
			string ourString = "";
			if (_session_isModified)
			{
				ourString = ToOurString(_session);
				_fields["session"] = ourString;
			}
			if (_comment_isModified)
			{
				ourString = ToOurString(_comment);
				_fields["comment"] = ourString;
			}
			if (_additionalEMail_isModified)
			{
				ourString = ToOurString(_additionalEMail);
				_fields["additionalEMail"] = ourString;
			}
			base.BuildNameValues();
		}
		
		public override void BuildFieldValues(SqlDataReader dr)
		{
			_session = ((Guid)(GetValue(dr, "session")));
			_session_isModified = false;
			_comment = ((string)(GetValue(dr, "comment")));
			_comment_isModified = false;
			_additionalEMail = ((string)(GetValue(dr, "additionalEMail")));
			_additionalEMail_isModified = false;
			base.BuildFieldValues(dr);
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("singleProp/session") != null) 
						&& (root.SelectSingleNode("singleProp/session").InnerText != null)))
			{
				Session = new Guid(root.SelectSingleNode("singleProp/session").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/comment") != null) 
						&& (root.SelectSingleNode("singleProp/comment").InnerText != null)))
			{
				Comment = root.SelectSingleNode("singleProp/comment").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/additionalEMail") != null) 
						&& (root.SelectSingleNode("singleProp/additionalEMail").InnerText != null)))
			{
				AdditionalEMail = root.SelectSingleNode("singleProp/additionalEMail").InnerText;
			}
		}
		
		protected override void ProcessMultiProp()
		{
			base.ProcessMultiProp();
			if ((root.SelectSingleNode("multiProp/objects") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/objects/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					int deleted = Convert.ToInt32(delNodes[i].SelectSingleNode("ordValue").InnerText);
					this.DeleteMulti("objects", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/objects/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					int addedOrd = Convert.ToInt32(addNodes[i].SelectSingleNode("ordValue").InnerText);
					this.AddMulti("objects", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/objects/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					System.Guid modifed = new Guid(setNodes[i].SelectSingleNode("propValue").InnerText);
					int modifedOrd = Convert.ToInt32(setNodes[i].SelectSingleNode("ordValue").InnerText);
					this.SetMulti("objects", modifed, modifedOrd);
				}
			}
		}
	}
}
