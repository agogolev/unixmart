//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.2407
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace BackendService.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using System.Data.SqlClient;
	
	
	public class CSubscription : CObject
	{
		
		private string _eMail;
		
		private bool _eMail_isModified;
		
		private bool _subscriptionNews;
		
		private bool _subscriptionNews_isModified;
		
		private bool _subscriptionReviews;
		
		private bool _subscriptionReviews_isModified;
		
		public CSubscription(string connStr) : 
				base(connStr)
		{
			_eMail = string.Empty;
			_eMail_isModified = false;
			_subscriptionNews = false;
			_subscriptionNews_isModified = false;
			_subscriptionReviews = false;
			_subscriptionReviews_isModified = false;
		}
		
		public CSubscription(string connStr, Guid OID) : 
				base(connStr, OID)
		{
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("8B272B47-5D1C-4F58-81C7-29149357A620");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("8B272B47-5D1C-4F58-81C7-29149357A620");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CSubscription";
			}
		}
		
		public string EMail
		{
			get
			{
				return _eMail;
			}
			set
			{
				_eMail = value;
				_eMail_isModified = true;
			}
		}
		
		public bool SubscriptionNews
		{
			get
			{
				return _subscriptionNews;
			}
			set
			{
				_subscriptionNews = value;
				_subscriptionNews_isModified = true;
			}
		}
		
		public bool SubscriptionReviews
		{
			get
			{
				return _subscriptionReviews;
			}
			set
			{
				_subscriptionReviews = value;
				_subscriptionReviews_isModified = true;
			}
		}
		
		public override void BuildNameValues()
		{
			string ourString = "";
			if (_eMail_isModified)
			{
				ourString = ToOurString(_eMail);
				_fields["eMail"] = ourString;
			}
			if (_subscriptionNews_isModified)
			{
				ourString = ToOurString(_subscriptionNews);
				_fields["subscriptionNews"] = ourString;
			}
			if (_subscriptionReviews_isModified)
			{
				ourString = ToOurString(_subscriptionReviews);
				_fields["subscriptionReviews"] = ourString;
			}
			base.BuildNameValues();
		}
		
		public override void BuildFieldValues(SqlDataReader dr)
		{
			_eMail = ((string)(GetValue(dr, "eMail")));
			_eMail_isModified = false;
			_subscriptionNews = ((bool)(GetValue(dr, "subscriptionNews")));
			_subscriptionNews_isModified = false;
			_subscriptionReviews = ((bool)(GetValue(dr, "subscriptionReviews")));
			_subscriptionReviews_isModified = false;
			base.BuildFieldValues(dr);
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("singleProp/eMail") != null) 
						&& (root.SelectSingleNode("singleProp/eMail").InnerText != null)))
			{
				EMail = root.SelectSingleNode("singleProp/eMail").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/subscriptionNews") != null) 
						&& (root.SelectSingleNode("singleProp/subscriptionNews").InnerText != null)))
			{
				SubscriptionNews = Convert.ToBoolean(root.SelectSingleNode("singleProp/subscriptionNews").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/subscriptionReviews") != null) 
						&& (root.SelectSingleNode("singleProp/subscriptionReviews").InnerText != null)))
			{
				SubscriptionReviews = Convert.ToBoolean(root.SelectSingleNode("singleProp/subscriptionReviews").InnerText);
			}
		}
		
		protected override void ProcessMultiProp()
		{
			base.ProcessMultiProp();
		}
	}
}
