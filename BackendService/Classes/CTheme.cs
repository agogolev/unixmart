//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.8943
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BackendService.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using System.Data.SqlClient;
	
	
	public class CTheme : CObject
	{
		
		private string _themeName;
		
		private bool _themeName_isModified;
		
		private string _h1;
		
		private bool _h1_isModified;
		
		private string _altName;
		
		private bool _altName_isModified;
		
		private int _shopType;
		
		private bool _shopType_isModified;
		
		private bool _loadToYandex;
		
		private bool _loadToYandex_isModified;
		
		private decimal _priceLimit;
		
		private bool _priceLimit_isModified;
		
		private string _css1;
		
		private bool _css1_isModified;
		
		private string _css2;
		
		private bool _css2_isModified;
		
		private string _css3;
		
		private bool _css3_isModified;
		
		private Guid _google;
		
		private bool _google_isModified;
		
		private bool _goodsList;
		
		private bool _goodsList_isModified;
		
		private int _goodsCount;
		
		private bool _goodsCount_isModified;
		
		public CTheme(string connStr) : 
				base(connStr)
		{
			_themeName = string.Empty;
			_themeName_isModified = false;
			_h1 = string.Empty;
			_h1_isModified = false;
			_altName = string.Empty;
			_altName_isModified = false;
			_shopType = int.MinValue;
			_shopType_isModified = false;
			_loadToYandex = false;
			_loadToYandex_isModified = false;
			_priceLimit = decimal.MinValue;
			_priceLimit_isModified = false;
			_css1 = string.Empty;
			_css1_isModified = false;
			_css2 = string.Empty;
			_css2_isModified = false;
			_css3 = string.Empty;
			_css3_isModified = false;
			_google = Guid.Empty;
			_google_isModified = false;
			_goodsList = false;
			_goodsList_isModified = false;
			_goodsCount = int.MinValue;
			_goodsCount_isModified = false;
		}
		
		public CTheme(string connStr, Guid OID) : 
				base(connStr, OID)
		{
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("8086C9D3-FDCF-41D2-8FDF-252F5F0E3C12");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("8086C9D3-FDCF-41D2-8FDF-252F5F0E3C12");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CTheme";
			}
		}
		
		public string ThemeName
		{
			get
			{
				return _themeName;
			}
			set
			{
				_themeName = value;
				_themeName_isModified = true;
			}
		}
		
		public string H1
		{
			get
			{
				return _h1;
			}
			set
			{
				_h1 = value;
				_h1_isModified = true;
			}
		}
		
		public string AltName
		{
			get
			{
				return _altName;
			}
			set
			{
				_altName = value;
				_altName_isModified = true;
			}
		}
		
		public int ShopType
		{
			get
			{
				return _shopType;
			}
			set
			{
				_shopType = value;
				_shopType_isModified = true;
			}
		}
		
		public bool LoadToYandex
		{
			get
			{
				return _loadToYandex;
			}
			set
			{
				_loadToYandex = value;
				_loadToYandex_isModified = true;
			}
		}
		
		public decimal PriceLimit
		{
			get
			{
				return _priceLimit;
			}
			set
			{
				_priceLimit = value;
				_priceLimit_isModified = true;
			}
		}
		
		public string Css1
		{
			get
			{
				return _css1;
			}
			set
			{
				_css1 = value;
				_css1_isModified = true;
			}
		}
		
		public string Css2
		{
			get
			{
				return _css2;
			}
			set
			{
				_css2 = value;
				_css2_isModified = true;
			}
		}
		
		public string Css3
		{
			get
			{
				return _css3;
			}
			set
			{
				_css3 = value;
				_css3_isModified = true;
			}
		}
		
		public Guid Google
		{
			get
			{
				return _google;
			}
			set
			{
				_google = value;
				_google_isModified = true;
			}
		}
		
		public bool GoodsList
		{
			get
			{
				return _goodsList;
			}
			set
			{
				_goodsList = value;
				_goodsList_isModified = true;
			}
		}
		
		public int GoodsCount
		{
			get
			{
				return _goodsCount;
			}
			set
			{
				_goodsCount = value;
				_goodsCount_isModified = true;
			}
		}
		
		public override void BuildNameValues()
		{
			string ourString = "";
			if (_themeName_isModified)
			{
				ourString = ToOurString(_themeName);
				_fields["themeName"] = ourString;
			}
			if (_h1_isModified)
			{
				ourString = ToOurString(_h1);
				_fields["h1"] = ourString;
			}
			if (_altName_isModified)
			{
				ourString = ToOurString(_altName);
				_fields["altName"] = ourString;
			}
			if (_shopType_isModified)
			{
				ourString = ToOurString(_shopType);
				_fields["shopType"] = ourString;
			}
			if (_loadToYandex_isModified)
			{
				ourString = ToOurString(_loadToYandex);
				_fields["loadToYandex"] = ourString;
			}
			if (_priceLimit_isModified)
			{
				ourString = ToOurString(_priceLimit);
				_fields["priceLimit"] = ourString;
			}
			if (_css1_isModified)
			{
				ourString = ToOurString(_css1);
				_fields["css1"] = ourString;
			}
			if (_css2_isModified)
			{
				ourString = ToOurString(_css2);
				_fields["css2"] = ourString;
			}
			if (_css3_isModified)
			{
				ourString = ToOurString(_css3);
				_fields["css3"] = ourString;
			}
			if (_google_isModified)
			{
				ourString = ToOurString(_google);
				_fields["google"] = ourString;
			}
			if (_goodsList_isModified)
			{
				ourString = ToOurString(_goodsList);
				_fields["goodsList"] = ourString;
			}
			if (_goodsCount_isModified)
			{
				ourString = ToOurString(_goodsCount);
				_fields["goodsCount"] = ourString;
			}
			base.BuildNameValues();
		}
		
		public override void BuildFieldValues(SqlDataReader dr)
		{
			_themeName = ((string)(GetValue(dr, "themeName")));
			_themeName_isModified = false;
			_h1 = ((string)(GetValue(dr, "h1")));
			_h1_isModified = false;
			_altName = ((string)(GetValue(dr, "altName")));
			_altName_isModified = false;
			_shopType = ((int)(GetValue(dr, "shopType")));
			_shopType_isModified = false;
			_loadToYandex = ((bool)(GetValue(dr, "loadToYandex")));
			_loadToYandex_isModified = false;
			_priceLimit = ((decimal)(GetValue(dr, "priceLimit")));
			_priceLimit_isModified = false;
			_css1 = ((string)(GetValue(dr, "css1")));
			_css1_isModified = false;
			_css2 = ((string)(GetValue(dr, "css2")));
			_css2_isModified = false;
			_css3 = ((string)(GetValue(dr, "css3")));
			_css3_isModified = false;
			_google = ((Guid)(GetValue(dr, "google")));
			_google_isModified = false;
			_goodsList = ((bool)(GetValue(dr, "goodsList")));
			_goodsList_isModified = false;
			_goodsCount = ((int)(GetValue(dr, "goodsCount")));
			_goodsCount_isModified = false;
			base.BuildFieldValues(dr);
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("singleProp/themeName") != null) 
						&& (root.SelectSingleNode("singleProp/themeName").InnerText != null)))
			{
				ThemeName = root.SelectSingleNode("singleProp/themeName").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/h1") != null) 
						&& (root.SelectSingleNode("singleProp/h1").InnerText != null)))
			{
				H1 = root.SelectSingleNode("singleProp/h1").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/altName") != null) 
						&& (root.SelectSingleNode("singleProp/altName").InnerText != null)))
			{
				AltName = root.SelectSingleNode("singleProp/altName").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/shopType") != null) 
						&& (root.SelectSingleNode("singleProp/shopType").InnerText != null)))
			{
				ShopType = Convert.ToInt32(root.SelectSingleNode("singleProp/shopType").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/loadToYandex") != null) 
						&& (root.SelectSingleNode("singleProp/loadToYandex").InnerText != null)))
			{
				LoadToYandex = Convert.ToBoolean(root.SelectSingleNode("singleProp/loadToYandex").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/priceLimit") != null) 
						&& (root.SelectSingleNode("singleProp/priceLimit").InnerText != null)))
			{
				PriceLimit = Convert.ToDecimal(root.SelectSingleNode("singleProp/priceLimit").InnerText, this.NumberFormat);
			}
			if (((root.SelectSingleNode("singleProp/css1") != null) 
						&& (root.SelectSingleNode("singleProp/css1").InnerText != null)))
			{
				Css1 = root.SelectSingleNode("singleProp/css1").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/css2") != null) 
						&& (root.SelectSingleNode("singleProp/css2").InnerText != null)))
			{
				Css2 = root.SelectSingleNode("singleProp/css2").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/css3") != null) 
						&& (root.SelectSingleNode("singleProp/css3").InnerText != null)))
			{
				Css3 = root.SelectSingleNode("singleProp/css3").InnerText;
			}
			if (((root.SelectSingleNode("singleProp/google") != null) 
						&& (root.SelectSingleNode("singleProp/google").InnerText != null)))
			{
				Google = new Guid(root.SelectSingleNode("singleProp/google").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/goodsList") != null) 
						&& (root.SelectSingleNode("singleProp/goodsList").InnerText != null)))
			{
				GoodsList = Convert.ToBoolean(root.SelectSingleNode("singleProp/goodsList").InnerText);
			}
			if (((root.SelectSingleNode("singleProp/goodsCount") != null) 
						&& (root.SelectSingleNode("singleProp/goodsCount").InnerText != null)))
			{
				GoodsCount = Convert.ToInt32(root.SelectSingleNode("singleProp/goodsCount").InnerText);
			}
		}
		
		protected override void ProcessMultiProp()
		{
			base.ProcessMultiProp();
			if ((root.SelectSingleNode("multiProp/masters") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/masters/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					System.Guid deleted = new Guid(delNodes[i].SelectSingleNode("propValue").InnerText);
					this.DeleteMulti("masters", deleted, null);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/masters/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					this.AddMulti("masters", added, null);
				}
			}
			if ((root.SelectSingleNode("multiProp/objects") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/objects/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					System.Guid deleted = new Guid(delNodes[i].SelectSingleNode("propValue").InnerText);
					this.DeleteMulti("objects", deleted, null);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/objects/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					this.AddMulti("objects", added, null);
				}
			}
			if ((root.SelectSingleNode("multiProp/params") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/params/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					int deleted = Convert.ToInt32(delNodes[i].SelectSingleNode("ordValue").InnerText);
					this.DeleteMulti("params", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/params/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					int addedOrd = Convert.ToInt32(addNodes[i].SelectSingleNode("ordValue").InnerText);
					this.AddMulti("params", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/params/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					System.Guid modifed = new Guid(setNodes[i].SelectSingleNode("propValue").InnerText);
					int modifedOrd = Convert.ToInt32(setNodes[i].SelectSingleNode("ordValue").InnerText);
					this.SetMulti("params", modifed, modifedOrd);
				}
			}
			if ((root.SelectSingleNode("multiProp/priceIntervals") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/priceIntervals/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					decimal deleted = Convert.ToDecimal(delNodes[i].SelectSingleNode("ordValue").InnerText, this.NumberFormat);
					this.DeleteMulti("priceIntervals", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/priceIntervals/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					decimal added = Convert.ToDecimal(addNodes[i].SelectSingleNode("propValue").InnerText, this.NumberFormat);
					decimal addedOrd = Convert.ToDecimal(addNodes[i].SelectSingleNode("ordValue").InnerText, this.NumberFormat);
					this.AddMulti("priceIntervals", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/priceIntervals/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					decimal modifed = Convert.ToDecimal(setNodes[i].SelectSingleNode("propValue").InnerText, this.NumberFormat);
					decimal modifedOrd = Convert.ToDecimal(setNodes[i].SelectSingleNode("ordValue").InnerText, this.NumberFormat);
					this.SetMulti("priceIntervals", modifed, modifedOrd);
				}
			}
			if ((root.SelectSingleNode("multiProp/salesNotes") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/salesNotes/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					int deleted = Convert.ToInt32(delNodes[i].SelectSingleNode("ordValue").InnerText);
					this.DeleteMulti("salesNotes", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/salesNotes/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					string added = addNodes[i].SelectSingleNode("propValue").InnerText;
					int addedOrd = Convert.ToInt32(addNodes[i].SelectSingleNode("ordValue").InnerText);
					this.AddMulti("salesNotes", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/salesNotes/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					string modifed = setNodes[i].SelectSingleNode("propValue").InnerText;
					int modifedOrd = Convert.ToInt32(setNodes[i].SelectSingleNode("ordValue").InnerText);
					this.SetMulti("salesNotes", modifed, modifedOrd);
				}
			}
			if ((root.SelectSingleNode("multiProp/seoTexts") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/seoTexts/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					int deleted = Convert.ToInt32(delNodes[i].SelectSingleNode("ordValue").InnerText);
					this.DeleteMulti("seoTexts", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/seoTexts/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					int addedOrd = Convert.ToInt32(addNodes[i].SelectSingleNode("ordValue").InnerText);
					this.AddMulti("seoTexts", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/seoTexts/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					System.Guid modifed = new Guid(setNodes[i].SelectSingleNode("propValue").InnerText);
					int modifedOrd = Convert.ToInt32(setNodes[i].SelectSingleNode("ordValue").InnerText);
					this.SetMulti("seoTexts", modifed, modifedOrd);
				}
			}
			if ((root.SelectSingleNode("multiProp/tags") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/tags/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					int deleted = Convert.ToInt32(delNodes[i].SelectSingleNode("ordValue").InnerText);
					this.DeleteMulti("tags", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/tags/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					System.Guid added = new Guid(addNodes[i].SelectSingleNode("propValue").InnerText);
					int addedOrd = Convert.ToInt32(addNodes[i].SelectSingleNode("ordValue").InnerText);
					this.AddMulti("tags", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/tags/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					System.Guid modifed = new Guid(setNodes[i].SelectSingleNode("propValue").InnerText);
					int modifedOrd = Convert.ToInt32(setNodes[i].SelectSingleNode("ordValue").InnerText);
					this.SetMulti("tags", modifed, modifedOrd);
				}
			}
			if ((root.SelectSingleNode("multiProp/titles") != null))
			{
				XmlNodeList delNodes = root.SelectNodes("multiProp/titles/delete");
				for (int i = 0; (i < delNodes.Count); i = (i + 1))
				{
					System.Guid deleted = new Guid(delNodes[i].SelectSingleNode("ordValue").InnerText);
					this.DeleteMulti("titles", null, deleted);
				}
				XmlNodeList addNodes = root.SelectNodes("multiProp/titles/add");
				for (int i = 0; (i < addNodes.Count); i = (i + 1))
				{
					string added = addNodes[i].SelectSingleNode("propValue").InnerText;
					System.Guid addedOrd = new Guid(addNodes[i].SelectSingleNode("ordValue").InnerText);
					this.AddMulti("titles", added, addedOrd);
				}
				XmlNodeList setNodes = root.SelectNodes("multiProp/titles/modify");
				for (int i = 0; (i < setNodes.Count); i = (i + 1))
				{
					string modifed = setNodes[i].SelectSingleNode("propValue").InnerText;
					System.Guid modifedOrd = new Guid(setNodes[i].SelectSingleNode("ordValue").InnerText);
					this.SetMulti("titles", modifed, modifedOrd);
				}
			}
		}
	}
}
