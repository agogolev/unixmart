using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Services.Protocols;
using MetaData;
using System.Xml.Serialization;
using System.Configuration;
using BackendService.Infrastructure;

namespace BackendService
{
	/// <summary>
	/// Summary description for AuthorizeProvider.
	/// </summary>
	/// 
	[WebService(Namespace="http://www.invento.ru/BackendService")]
	public class AuthorizeProvider : AuthentificatedService<AuthorizeProvider>
    {
		[WebMethod]
		public AuthorizeResponse Authorize(string login, string password, string clientVersion, out ExceptionISM exISM) 
		{
			AuthorizeResponse resp = new AuthorizeResponse();
			resp.Ticket = null;
			exISM = null;
			try 
			{
				//если вместо clientVersion пустая строка - то пропускаем (это для всяких служб)
				if(clientVersion != "" && ConfigurationManager.AppSettings["ccv"].ToString() != clientVersion)//проверка версии клиента
					throw new CCVException();
				else resp.Ticket = CheckLogin(login, password);
			}
			catch(Exception e) 
			{
				exISM = new ExceptionISM(e);
			}

			resp.Info = ConfigurationManager.AppSettings["message"];

			return resp;
		}


		[WebMethod, SoapHeader("CurrentUser")]
		public ExceptionISM CloseSession() 
		{
			ExceptionISM exISM = null;
			if(CurrentUser != null) 
			{
				SqlCommand cmd = DBReader.QueryConstructor.GetCommand("upCloseSession", CommandType.StoredProcedure, null);
				cmd.Parameters.AddWithValue("@sessionOID", new Guid(CurrentUser.Ticket));
				cmd.Connection.Open();
				try 
				{
					cmd.ExecuteNonQuery();
					Context.Cache.Remove(CurrentUser.Ticket);
				}
				catch(Exception e) 
				{
					exISM = new ExceptionISM(e);
				}
				finally 
				{
					cmd.Connection.Close();
				}
			}
			else exISM = new ExceptionISM("Access denied!","CloseSession");

			return exISM;
		}

		[WebMethod]
		public string Ping() 
		{
			return "Pong";
		}

		[WebMethod]
		public string GetCurrentVersion() 
		{
			return ConfigurationManager.AppSettings["ccv"].ToString();
		}
	}

	public class AuthorizeResponse 
	{
		// Both types of attributes can be applied. Depending on which type
		// the method used, either one will affect the call.
		[SoapElement(ElementName = "Ticket")]
		[XmlElement(ElementName = "Ticket")]
		public string Ticket;

		[SoapElement(ElementName = "Info")]
		[XmlElement(ElementName = "Info")]
		public string Info;
	}
}