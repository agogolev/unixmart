using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.Services.Protocols;
using BackendService.Classes;
using BackendService.Infrastructure;
using DBReader;
using MetaData;

namespace BackendService
{
    [WebService(Namespace = "http://www.invento.ru/BackendService")]
    public class ObjectProvider : AuthentificatedService<ObjectProvider>
    {
        [WebMethod, SoapHeader("CurrentUser")]
        public string GetObject(string OID, string fieldNames, string multiProps)
        {
            CheckSession(); //проверим аутентифицирован ли пользователь   
            var oid = new Guid(OID);
            var sql = QueryConstructor.BuildGetObject(oid, null, fieldNames.RestoreCr(), multiProps, null);
            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, Usr);
            cmd.Parameters.AddWithValue("@OID", oid);
            cmd.Connection.Open();
            var sb = new StringBuilder();
            sb.Append("<?xml version=\"1.0\" encoding=\"Windows-1251\" ?>");
            try
            {
                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    sb.Append((string) dr[0]);
                }
            }
            finally
            {
                cmd.Connection.Close();
            }
            return sb.ToString();
        }

        [WebMethod(MessageName = "GetObjectExt"), SoapHeader("CurrentUser")]
        public string GetObject(string OID, string fieldNames, string multiProps, string className)
        {
            CheckSession(); //проверим аутентифицирован ли пользователь

            var oid = new Guid(OID);
            var sql = QueryConstructor.BuildGetObject(oid, className, fieldNames.RestoreCr(), multiProps, null);
            var cmd = QueryConstructor.GetCommand(sql, CommandType.Text, Usr);
            cmd.Parameters.AddWithValue("@OID", oid);
            cmd.Connection.Open();
            var sb = new StringBuilder();
            sb.Append("<?xml version=\"1.0\" encoding=\"Windows-1251\" ?>");
            try
            {
                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    sb.Append((string) dr[0]);
                }
            }
            finally
            {
                cmd.Connection.Close();
            }
            return sb.ToString();
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string GetMultiCollection(string OID, string propName, string content, string className)
        {
            CheckSession(); //проверим аутентифицирован ли пользователь

            var oid = new Guid(OID);
            if (className == null || className == "")
            {
                var prms = new Hashtable();
                prms["OID"] = new Guid(OID);
                className = (string) DBHelper.ExecuteScalar(@"SELECT dbo.className(@OID)", prms, true);
            }
            var ca = new CollectionAdapter(OID, className, propName, content != null ? content.RestoreCr() : null);
            try
            {
                return ca.GetDataSet().SaveToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public string SaveObject(string xml, out ExceptionISM exISM)
        {
            exISM = null;

            try
            {
                CheckSession(); //проверим аутентифицирован ли пользователь
            }
            catch (Exception exc)
            {
                exISM = new ExceptionISM(exc);
                return null;
            }

            xml = xml.RestoreCr();
            //Regex re = new Regex("<CID>(.*)</CID>");
            var re = new Regex("<CID>([^<]*)</CID>");
            var reOID = new Regex("OID=\"([^\"]*)\"");
            var OID = Guid.Empty;
            DBObject obj = null;
            var m = reOID.Match(xml);
            if (m.Success)
            {
                OID = new Guid(m.Groups[1].Value);
            }
            m = re.Match(xml);
            var result = "";
            if (m.Success)
            {
                result = m.Groups[1].Value.ToUpper();
            }

            string dbdata_connection = ConfigurationManager.AppSettings["dbdata.connection"];
            switch (result)
            {
                case "0D4DBE70-C1F2-4F6C-9786-3AFB39E2AC7D":
                    obj = new CBinaryData(dbdata_connection);
                    break;
                case "8086C9D3-FDCF-41D2-8FDF-252F5F0E3C12":
                    obj = new CTheme(dbdata_connection);
                    break;
                case "59696B42-6766-497D-80B2-8BB639584C5D":
                    obj = new CTree(dbdata_connection);
                    break;
                case "A73A4F5B-771C-4FA9-9030-3E94E9D8D37F":
                    obj = new CCompany(dbdata_connection);
                    break;
                case "C2D8F281-971B-4700-86E3-F1A5A40CED72":
                    obj = new CArticle(dbdata_connection);
                    break;
                case "6989C9C2-5203-4B25-B200-A7B5B65B23D7":
                    obj = new CPeople(dbdata_connection);
                    break;
                case "083DA417-7E76-461C-8AD2-4B1ED039FEBD":
                    obj = new CGroup(dbdata_connection);
                    break;
                case "7F8F2945-65D6-41C5-8F34-CEC78A50A206":
                    obj = new CPage(dbdata_connection);
                    break;
                case "EEE7C372-6018-49A4-8CAA-1054A9AB91DC":
                    obj = new CPromo(dbdata_connection);
                    break;
                case "8B13C41A-81C1-456E-9A59-6899DC983A81":
                    obj = new COrder(dbdata_connection);
                    break;
                case "355D97CB-31E3-4711-9BF5-797D733CD4BE":
                    obj = new CGoods(dbdata_connection);
                    break;
                case "5329A292-7D72-4392-93DF-6FB844FC9FD7":
                    obj = new CParam(dbdata_connection);
                    break;
                case "9C71D0DB-6EC0-45FC-921A-122F5D6DF38D":
                    obj = new CParamType(dbdata_connection);
                    break;
                case "ED24B7E1-6743-49D4-BE0F-B958A75C8EB2":
                    obj = new CStringItem(dbdata_connection);
                    break;
                case "6BC76A6A-2BB5-4813-953C-BA8E2382C2B6":
                    obj = new CComment(dbdata_connection);
                    break;
                case "8B272B47-5D1C-4F58-81C7-29149357A620":
                    obj = new CSubscription(dbdata_connection);
                    break;
                case "13BB4FAD-D2A8-4765-BCDB-A149041C24FB":
                    obj = new CMasterCatalog(dbdata_connection);
                    break;
                case "E3626119-132D-4B5F-984F-48DAEAA22796":
                    obj = new CCreditAnketa(dbdata_connection);
                    break;
                case "5B7A2835-ECDC-427F-800C-C81EBA218093":
                    obj = new CMailConstructor(dbdata_connection);
                    break;
                case "890E467F-2057-45E1-BA8E-E31017FCBF42":
                    obj = new CPreOrder(dbdata_connection);
                    break;
                case "DB599149-2CF5-43A5-9354-AAD5BFD5BB4B":
                    obj = new CAdvertMessage(dbdata_connection);
                    break;
                case "9560E44D-7FE7-4DD5-AED3-58C8ACA71F58":
                    obj = new CSearch(dbdata_connection);
                    break;
                case "381AD04B-AA73-4ACC-BEFA-8FF6DDB5B6FB":
                    obj = new CDisableExpressDelivery(dbdata_connection);
                    break;
                case "A7B176CB-9542-4441-814D-E35DF7A9A234":
                    obj = new CCommodityGroup(dbdata_connection);
                    break;
                case "1FA5A0E4-0768-457D-8885-EE4D2F4725A3":
                    obj = new CYandexGroup(dbdata_connection);
                    break;
                case "19106297-DD4C-40D4-AA42-DC5464BC213E":
                    obj = new CAction(dbdata_connection);
                    break;
                case "2159F930-F02C-4997-A3A9-344BF6C0E11F":
                    obj = new CAdvertItem(dbdata_connection);
                    break;
                case "4D83D8EE-1889-4644-87AC-BB759E5B0E86":
                    obj = new CAdvertCompany(dbdata_connection);
                    break;
                case "D1810ADC-0847-4DA1-9FAB-05D8FBE41AB1":
                    obj = new CAdvertPlace(dbdata_connection);
                    break;
                case "7F7D09C4-08D4-49C6-8A1C-D50779730045":
                    obj = new CLocation(dbdata_connection);
                    break;
                case "049910F9-4BD8-4F37-9BED-E82C473ADFD8":
                    obj = new CLocation(dbdata_connection);
                    break;
                case "DA9ADBC6-16D9-46D0-A315-F45BD8A3E555":
                    obj = new CGoogleTaxonomy(dbdata_connection);
                    break;
                case "33C55F3B-E427-4F98-B2FD-B1F8A7221E1D":
                    obj = new CLandingPage(dbdata_connection);
                    break;
                case "E98D7BD7-F78B-47F6-9CB1-54AF30201D01":
                    obj = new CPromoCode(dbdata_connection);
                    break;
                case "380E5EFD-243E-4F4F-97B1-E54A6100C310":
                    obj = new CDefaultTitle(dbdata_connection);
                    break;
                case "CB071382-6C37-4711-8097-4CEBC0A06238":
                    obj = new CFormRefuse(dbdata_connection);
                    break;
                default:
                    throw new Exception("Error in CID");
            }
            try
            {
                result = ProcessSaveObject(obj, xml);
            }
            catch (Exception e)
            {
                exISM = new ExceptionISM(e);
                return null;
            }


            //сохранили - теперь обнуляем веб кэш
            WebServiceExtensions.ClearWebCache("www_");

            return "<?xml version=\"1.0\" encoding=\"Windows-1251\" ?><item OID=\"" + obj.OID.ToString().ToUpper() +
                   "\"><CID>" + obj.CID.ToString().ToUpper() + "</CID>" + result + "</item>";
        }

        /// <summary>
        ///     Метод сохранения объекта
        /// </summary>
        /// <param name="obj">Пустой объект соответствующего класса</param>
        /// <param name="xml">XML объекта</param>
        /// <returns>В этот string можно что-нибудь дополнительно запросить и вернуть на клиент</returns>
        private string ProcessSaveObject(DBObject obj, string xml /*, Guid OID*/)
        {
            var result = "";

            switch (obj.CID.ToString().ToUpper())
            {
                //case "8086C9D3-FDCF-41D2-8FDF-252F5F0E3C12":
                //  result = ProcessSavingObject.SaveTheme(obj, xml);
                //  break;
                case "355D97CB-31E3-4711-9BF5-797D733CD4BE":
                    result = ProcessSavingObject.SaveGoods(obj, xml);
                    break;
                case "8B13C41A-81C1-456E-9A59-6899DC983A81":
                    result = ProcessSavingObject.SaveOrder(obj, xml);
                    break;
                default:
                    result = obj.SaveXml(xml);
                    break;
            }

            return result;
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public ExceptionISM DeleteObject(Guid OID)
        {
            try
            {
                CheckSession(); //проверим аутентифицирован ли пользователь

                var sqlQuery = "SELECT CID FROM t_Object WHERE OID='" + OID + "'";
                var CID = (Guid) DBHelper.ExecuteScalar(sqlQuery, null, false);
                //TODO: перед удалением стоит проверять не занят ли объект!
                ProcessDeleteObject(OID, CID);
                //удалили - теперь обнуляем веб кэш
                WebServiceExtensions.ClearWebCache("www_");
            }
            catch (Exception e)
            {
                return new ExceptionISM(e);
            }

            return null;
        }

        [WebMethod(MessageName = "DeleteObjectExt"), SoapHeader("CurrentUser")]
        public ExceptionISM DeleteObject(Guid OID, bool useFalseDel)
        {
            try
            {
                CheckSession(); //проверим аутентифицирован ли пользователь

                var sqlQuery = "SELECT CID FROM t_Object WHERE OID='" + OID + "'";
                var CID = (Guid) DBHelper.ExecuteScalar(sqlQuery, null, false);
                //TODO: перед удалением стоит проверять не занят ли объект!
                ProcessDeleteObject(OID, CID);

                //удалили - теперь обнуляем веб кэш
                WebServiceExtensions.ClearWebCache("www_");
            }
            catch (SqlException e)
            {
                if (e.Number == 547 && useFalseDel) //из-за constraint, если useFalseDel=true используем псевдо удаление
                    try
                    {
                        var sql = @"UPDATE t_Object SET isDeleted=1 WHERE OID='" + OID + "'";
                        DBHelper.ExecuteCommand(sql, null, false);
                    }
                    catch (Exception ex)
                    {
                        return new ExceptionISM(ex);
                    }
                else return new ExceptionISM(e);
            }
            catch (Exception e)
            {
                return new ExceptionISM(e);
            }

            return null;
        }

        private void ProcessDeleteObject(Guid OID, Guid CID)
        {
            switch (CID.ToString().ToUpper())
            {
                case "4E742166-7C4A-4725-A71F-65C1969DC88A":
                    ProcessDeletingObject.DeletePoll(OID);
                    break;
                case "ED24B7E1-6743-49D4-BE0F-B958A75C8EB2":
                    ProcessDeletingObject.DeleteStringItem(OID);
                    break;
                default:
                    var prms = new Hashtable();
                    prms["OID"] = OID;

                    DBHelper.ExecuteCommand("spDeleteObject", prms, true);
                    break;
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public ExceptionISM ProcessCollection(string className, string propName, string processContent, string OID)
        {
            try
            {
                CheckSession(); //проверим аутентифицирован ли пользователь

                if (className == null || className == "")
                {
                    var prms = new Hashtable();
                    prms["OID"] = new Guid(OID);
                    className = (string) DBHelper.ExecuteScalar(@"SELECT dbo.className(@OID)", prms, true);
                }

                var ca = new CollectionAdapter(OID, className, propName, processContent.RestoreCr());
                ca.AcceptChanges(null, null);

                //сохранили - теперь обнуляем веб кэш
                WebServiceExtensions.ClearWebCache("www_");
            }
            catch (Exception e)
            {
                return new ExceptionISM(e);
            }

            return null;
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public ExceptionISM ProcessTableUpdate(string tableName, string processContent)
        {
            try
            {
                CheckSession(); //проверим аутентифицирован ли пользователь

                var ta = new TableAdapter(tableName, processContent.RestoreCr());
                ta.AcceptChanges();

                //сохранили - теперь обнуляем веб кэш
                WebServiceExtensions.ClearWebCache("www_");
            }
            catch (Exception e)
            {
                return new ExceptionISM(e);
            }
            return null;
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void ExecuteCommand(string sql)
        {
            CheckSession(); //проверим аутентифицирован ли пользователь
            DBHelper.ExecuteCommand(sql.RestoreCr(), null, false);
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void ExecuteCommandParams(string sql, NameValuePair[] paramsCollection, bool isStoreProc)
        {
            try
            {
                CheckSession(); //проверим аутентифицирован ли пользователь
                var prms = new Hashtable();
                paramsCollection.ToList().ForEach(k => { prms.Add(k.Name, k.Value.RestoreCr().StringToObject()); });
                DBHelper.ExecuteCommand(sql.RestoreCr(), prms, isStoreProc);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [WebMethod, SoapHeader("CurrentUser")]
        public void ClearCache()
        {
            CheckSession(); //проверим аутентифицирован ли пользователь
            WebServiceExtensions.ClearWebCache("www_");
        }
    }
}