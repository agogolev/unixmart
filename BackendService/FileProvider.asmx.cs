using System;
using System.Collections;
using System.Web.Services;
using System.IO;
using MetaData;
using System.Configuration;
using BackendService.Infrastructure;

namespace BackendService
{
	[WebService(Namespace="http://www.invento.ru/BackendService")]
	public class FileProvider : AuthentificatedService<FileProvider>
    {
		[WebMethod]
		public string UploadFile(string fileName, FileMode fileMode, byte[] data, out ExceptionISM exISM) 
		{
			exISM = null;
			string dir = Server.MapPath(ConfigurationManager.AppSettings["fileDir"]);
			string path = ConfigurationManager.AppSettings["wwwFileDir"]+"/"+fileName;
			FileStream fs = null;

			try
			{
				fs = new FileStream(dir+"/"+fileName, fileMode);
				fs.Write(data, 0, data.Length);
			}
			catch(Exception e)
			{
				exISM = new ExceptionISM(e);
				return null;
			}
			finally
			{
				if (fs!=null)
					fs.Close();
			}

			return path;
		}

		[WebMethod]
		public byte[] DownloadFile(string fileName, ref ExceptionISM ex) 
		{
			string dir = Server.MapPath(ConfigurationManager.AppSettings["updateDir"]);
			byte [] content;
			try
			{
				var fs = new FileStream(dir+"/"+fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
				content = new byte[fs.Length];
				fs.Read(content, 0, (int)fs.Length);
				fs.Close();
			}
			catch(Exception e)
			{
				ex  = new ExceptionISM(e);
				return null;
			}
			return content;
		}

		[WebMethod]
		public string[] GetAllFileNames(ref ExceptionISM ex)
		{
			var dir = Server.MapPath(ConfigurationManager.AppSettings["updateDir"]);
			ArrayList fileNames;
			try
			{
				var di = new DirectoryInfo(dir);
				var fis = di.GetFiles();
				fileNames = new ArrayList();
				foreach (var fi in fis)
				{
					fileNames.Add(fi.Name);
				}

				var dis = di.GetDirectories();
				foreach (var dif in dis)
				{
					fis = dif.GetFiles();
					foreach (var fi in fis)
					{
						fileNames.Add(dif.Name + "/" + fi.Name);
					}
				}
			}
			catch (Exception e)
			{
				ex = new ExceptionISM(e);
				return null;
			}

			var ret = new string[fileNames.Count];
			fileNames.CopyTo(ret);
			return ret;
		}

	}
}
