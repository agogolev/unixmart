<?xml version="1.0" encoding="windows-1251" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:nls="http://www.asbis.com/NLS/2000"
	exclude-result-prefixes="nls">
	<xsl:output method="html" version="4.0" encoding="windows-1251" indent="yes" />
	
	<xsl:template match="/">
		<xsl:apply-templates select="article" />
	</xsl:template>
	
	<xsl:template match="article">
		<div id="subarticles">
			<xsl:for-each select="articleNote">
				<div id="{@OID}" style="border:1pt black solid; background-color:Khaki" onMouseOver="highLight()" onMouseOut="deLight()" onClick="insertArticle()">
					<img src="images/page_off.gif" />&#160;<xsl:copy-of select="substring(body/doc, 0, 100)" />...
					<div id="inp_{@OID}" style="display:none">
						<xsl:copy-of select="body/doc" />
					</div>
				</div>
				<br />
			</xsl:for-each>
		</div>
	</xsl:template>
	
</xsl:stylesheet>
