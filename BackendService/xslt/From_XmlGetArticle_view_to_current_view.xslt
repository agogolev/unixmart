<?xml version="1.0" encoding="windows-1251" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="windows-1251" indent="yes"/>
	
	<xsl:template match="/">
		<xsl:element name="root">
			<xsl:apply-templates select="article/body" />
			<xsl:apply-templates select="article/titleImage" />
			<xsl:element name="articleNotes">
				<xsl:apply-templates select="article/articleNote" />
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="article/body">
		<xsl:apply-templates select="@*|node()"/>
	</xsl:template>
	
	<xsl:template match="body//@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="article/titleImage">
		<xsl:element name="linkimages">
			<xsl:attribute name="OID">
				<xsl:value-of select="."/>
			</xsl:attribute>
			<xsl:attribute name="title">
				<xsl:value-of select="@title"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="article/articleNote">
		<xsl:copy>
			<xsl:attribute name="OID">
				<xsl:value-of select="@OID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()" />
		</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>