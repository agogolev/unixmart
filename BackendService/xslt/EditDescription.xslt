<?xml version="1.0" encoding="windows-1251" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:nls="http://www.asbis.com/NLS/2000"
	exclude-result-prefixes="nls">
	<xsl:output method="html" version="4.0" encoding="windows-1251" indent="yes" />

	<xsl:template match="/">
		<xsl:apply-templates select="root/doc" />
	</xsl:template>
	
	<xsl:template match="root/doc">
		<xsl:apply-templates select="@*|node()" />
	</xsl:template>
	<xsl:template match="doc//@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="//linkForm">
		<img src="images/formExample.gif?OID={@OID}" />
	</xsl:template>
	
	<!--xsl:template match="//linkimages[@mode='inside']">
		<img src="bin.aspx?ID={@oid}"	id="{@oid}" width="{@width}" height="{@height}"/>
	</xsl:template-->
	
	<xsl:template match="//linkArticle">
		<div contenteditable='false' style='position:relative; background-color:#cccccc' name='linkedArticle' 
			id='{concat("linkedArticle_",@OID)}'>
			<xsl:variable name="curOID" select="@OID"/>
			<xsl:copy-of select="/root/articleNotes/articleNote[@OID=$curOID]/body/doc" />
		</div>
	</xsl:template>
	
</xsl:stylesheet>
