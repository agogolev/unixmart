using System;
using System.Collections;
using System.Linq;
using System.Data;
using System.Web.Services;
using System.Web.Services.Protocols;
using BackendService.Infrastructure;
using DBReader;
using MetaData;
using NLog;

namespace BackendService
{
	/// <summary>
	/// Summary description for ListProvider.
	/// </summary>
	[WebService(Namespace = "http://www.invento.ru/BackendService")]
	public class ListProvider : AuthentificatedService<ListProvider>
    {
		private static readonly Logger log = LogManager.GetCurrentClassLogger();
		[WebMethod, SoapHeader("CurrentUser")]
		public DataSet GetList(string xml)
		{
			try
			{
				CheckSession();//проверим аутентифицирован ли пользователь 
				return DBReader.ListProvider.GetList(xml.RestoreCr(), Usr);
			}
			catch (Exception)
			{
				log.Error(xml);
				throw;
			}

		}

		[WebMethod(MessageName = "GetListExt"), SoapHeader("CurrentUser")]
		public DataSet GetList(string xml, bool showAll)
		{
			CheckSession();//проверим аутентифицирован ли пользователь
			return DBReader.ListProvider.GetList(xml.RestoreCr(), showAll, Usr);
		}

		[WebMethod, SoapHeader("CurrentUser")]
		public string GetLookupValues(string className)
		{
			CheckSession();//проверим аутентифицирован ли пользователь
			return DBReader.ListProvider.GetLookupValues(className);
		}

		[WebMethod, SoapHeader("CurrentUser")]
		public string GetDataSetSql(string sql)
		{
			CheckSession();//проверим аутентифицирован ли пользователь
			return DBHelper.GetDataSetISMSql(sql.RestoreCr(), null, Usr).SaveToString();
		}
		[WebMethod, SoapHeader("CurrentUser")]
		public string GetDataSetSqlParams(string sql, NameValuePair[] paramsCollection)
		{
			CheckSession();//проверим аутентифицирован ли пользователь
			var prms = new Hashtable();
			paramsCollection.ToList().ForEach(k => prms.Add(k.Name, k.Value.RestoreCr()));
			return DBHelper.GetDataSetISMSql(sql.RestoreCr(), null, prms, false, Usr).SaveToString();
		}

		//[WebMethod(MessageName="GetDataSetSqlExc"), SoapHeader("CurrentUser")]
		//public string GetDataSetSql(string sql, out ExceptionISM exISM)
		//{
		//  exISM = null;
		//  try
		//  {
		//    CheckSession();//проверим аутентифицирован ли пользователь
		//    return DBReader.DBHelper.GetDataSetISMSql(sql, null, Usr).SaveToString();
		//  }
		//  catch(Exception exc)
		//  {
		//    exISM = new ExceptionISM(exc);
		//    return null;
		//  }
		//} 

	}
}
