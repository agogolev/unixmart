using System;
using System.Collections;
using System.Data;
using DBReader;

namespace BackendService.Browse {
	/// <summary>
	/// Summary description for browseImage.
	/// </summary>
	public partial class BrowseImage_ : System.Web.UI.Page {
		private Guid _OID;
	
		protected void Page_Load(object sender, System.EventArgs e) {
			if(!IsPostBack) {
				if(Request["OID"] != null && Request["OID"] != "") _OID = new Guid(Request["OID"]);
				DataBind();
			}
			else imgPreview.Visible = false;
		}

	#region Web Form Designer generated code
	override protected void OnInit(EventArgs e) {
	//
	// CODEGEN: This call is required by the ASP.NET Web Form Designer.
	//
	InitializeComponent();
	base.OnInit(e);
}
		
	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {    

}
	#endregion
		public override void DataBind() {
			ShowImage(_OID);
			base.DataBind ();
		}

		private void ShowImage(Guid OID) {
			if(_OID != Guid.Empty) {
				string sql = @"
SELECT
	bd.width,
	bd.height
FROM
	t_BinaryData bd
WHERE
	bd.OID = @OID";
				Hashtable prms = new Hashtable();
				prms["@OID"] = OID;
				DataSet ds = DBHelper.GetDataSetSql(sql, null, prms, false, null);
				if(ds.Tables[0].Rows.Count != 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					imgPreview.ImageUrl = "bin.aspx?ID="+OID;
					if(dr["width"] != DBNull.Value) imgPreview.Width = (int)dr["width"];
					if(dr["height"] != DBNull.Value) imgPreview.Height = (int)dr["height"];
				}
				else imgPreview.Visible = false; 
			}
			else imgPreview.Visible = false; 
		}
	}
}
