﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VideoBrowser.aspx.cs" Inherits="BackendService.Browse.VideoBrowser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Index</title>
    <script type="text/javascript">
        function selectFile(name) {
            window.opener.CKEDITOR.tools.callFunction( <%= FuncNum.Value %>, name);
            window.close();
            window.opener.focus();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:HiddenField runat="server" ID="FuncNum" />
        <div class="editor-label">
            <label for="file">
                Имя файла</label>
        </div>
        <div class="editor-field">
            <input type="file" name="File" id="File" runat="server" />
            <input type="submit" value="Загрузить" />
        </div>
        <div class="editor-label">
            <label for="file">
                Файлы на сервере</label>
        </div>
        <div class="editor-field">
            <table border="0" cellspacing="0" cellpadding="5" width="400">
                <tr>
                    <th>Файл
                    </th>
                    <th colspan="2">Действия
                    </th>
                </tr>
                <asp:Repeater runat="server" ID="Images">
                    <ItemTemplate>
                        <tr class="even">
                            <td>
                                <%# DataBinder.Eval(Container.DataItem, "FileName") %>
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick="<%# DataBinder.Eval(Container.DataItem, "FileName", "selectFile('{0}')") %>">Выбрать</a>
                            </td>
                            <td>
                                <asp:LinkButton runat="server" ID="delImage" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' Text="Удалить" OnCommand="delImage_OnCommand"></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr class="odd">
                            <td>
                                <%# DataBinder.Eval(Container.DataItem, "FileName") %>
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick="<%# DataBinder.Eval(Container.DataItem, "FileName", "selectFile('{0}')") %>">Выбрать</a>
                            </td>
                            <td>
                                <asp:LinkButton runat="server" ID="delImage" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' Text="Удалить" OnCommand="delImage_OnCommand"></asp:LinkButton>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </form>
</body>
</html>
