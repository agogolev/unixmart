﻿<%@ Register TagPrefix="cc1" Namespace="HtmlEditor" Assembly="HtmlEditor" %>

<%@ Page ValidateRequest="false" Language="c#" CodeBehind="EditDescription.aspx.cs"
    AutoEventWireup="True" Inherits="BackendService.Browse.EditDescription" %>

<% var OID = Editor1.OID; %>
<!DOCTYPE html>
<html>
<head id="head">
    <title>EditDescription</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        .ckeditor-container {
            min-width: 800px;
            min-height: 400px;
        }
    </style>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.6.2.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/ckeditor/ckeditor.js")%>"></script>
    <script type="text/javascript">
	<!--
        var textChanged = false,
            maxLength = 20;
        $(function() {

            CKEDITOR.editorConfig = function(config) {
                config.language = 'ru';
                config.extraPlugins = 'onchange';
            };

            CKEDITOR.replace('Editor1',
            {
                toolbar:
                [
                    ['Save', '-', 'Source', '-', 'NewPage', 'Preview'],
                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
                    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
                    ['Maximize', 'ShowBlocks'],
                    '/',
                    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                    ['Styles', 'Format', 'FontSize', 'lineheight', 'TextColor', 'BGColor'],
                    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                    ['Link', 'Unlink'],
                    ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe', 'Video'],
                ],
                //height: '280px',
                filebrowserBrowseUrl: "<%=ResolveUrl("~/Browse/FileBrowser.aspx?OID="+OID)%>",
                filebrowserVideoBrowseUrl: "<%=ResolveUrl("~/Browse/VideoBrowser.aspx")%>",
                on: {
                    instanceReady: function (evt) {
                        var editor = evt.editor;
                        editor.execCommand('maximize');
                    },
                    save: function(evt) {
                        var editor = evt.editor,
                            html = editor.getData(),
                            expr = /\<[^\>]+\>/gi,
                            entity = /&[^;]+;/gi,
                            newLine = /\n/gi,
                            text = html.replace(expr, '').replace(entity, ' ').replace(newLine, '');
                        if (text.length >= maxLength) {
                            alert("Содержимое статьи " + text.length + " символов, должно быть не более " + maxLength + " символов. Сократите статью.");
                            return false;
                        }
                        return true;
                    },
                    change: function() {
                        textChanged = true;
                    }
                }
            });
        });
        //-->
    </script>
</head>
<body>
    <form runat="server" id="Form1">
        <cc1:TextBoxEditor ID="Editor1" runat="server"></cc1:TextBoxEditor>
    </form>
</body>
</html>
