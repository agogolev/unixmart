﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using BackendService.Classes;
using BackendService.Infrastructure;
using HtmlEditor;
namespace BackendService.Browse
{
    public partial class VideoBrowser : BasePage<VideoBrowser>
	{
		public IBinDataManager BinDataManager { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				DataBind();
			}
			else
			{
				if (File.PostedFile != null && File.PostedFile.ContentLength != 0) SaveFile();

			}
		}

		public override void DataBind()
		{
		    FuncNum.Value = Request["CKEditorFuncNum"];
			var manager = new BinDataManager();

            Images.DataSource = manager.GetImagesByFolder(Server.MapPath(@"~\..\video"), "video");
			base.DataBind();
		}
		private void SaveFile()
		{
			var manager = new BinDataManager();
			manager.SaveImageToFolder(Server.MapPath("\\video"), new HttpPostedFileWrapper(File.PostedFile));
			DataBind();
		}

		protected void delImage_OnCommand(object sender, CommandEventArgs e)
		{
            //var manager = new BinDataManager();
            //manager.DelArticleImage(ArticleOID, (string)e.CommandArgument);
            //DataBind();
		}
	}
}