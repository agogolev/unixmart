using System;
using System.Configuration;
using System.Text.RegularExpressions;
using Ecommerce.Extensions;
using HtmlEditor;

namespace BackendService.Browse
{
	/// <summary>
	/// Summary description for EditDescription.
	/// </summary>
	public partial class EditDescription : System.Web.UI.Page
	{
        private static Regex _imageW = new Regex(@"\<img[^\>]+(?<width>width:[\d]+px;?)[^\>]+\>", RegexOptions.IgnoreCase);
        private static Regex _imageH = new Regex(@"\<img[^\>]+(?<height>height:[\d]+px;?)[^\>]+\>", RegexOptions.IgnoreCase);
        private static Regex _styleEmpty = new Regex(@"style=(""\s*""|'\s*')", RegexOptions.IgnoreCase);
        protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) DataBind();
			else SaveField();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		public override void DataBind() 
		{
			Editor1.OID = new Guid(Request["OID"]);
			Editor1.FieldString = Request["FieldString"];
			Editor1.ClassName = Request["ClassName"];
			Editor1.ConnectionString = ConfigurationManager.AppSettings["dbdata.connection"];
			base.DataBind();
		}
		public void SaveField()
		{
			var saver = new XmlSaver();
            //saver.ModifyXml += SaverOnModifyXml;
			saver.Save(Editor1.OID, Editor1.FieldString, Editor1.Text, Editor1.ClassName);
		}

	    private void SaverOnModifyXml(object sender, ModifyXmlEventArgs e)
	    {
            var body = _imageW.ReplaceGroup(e.XmlBody, "width", "");
            body = _imageH.ReplaceGroup(body, "height", "");
	        e.XmlBody = _styleEmpty.Replace(body, "");
	    }
	}
}
