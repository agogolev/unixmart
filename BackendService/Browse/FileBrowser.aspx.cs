﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using BackendService.Infrastructure;
using HtmlEditor;

namespace BackendService.Browse
{
	public partial class FileBrowser : BasePage<FileBrowser>
	{
		public IBinDataManager BinDataManager { get; set; }

		public Guid ArticleOID
		{
			get
			{
				object o = ViewState["ArticleOID"];
				return o == null ? Guid.Empty : (Guid)o;
			}
			set
			{
				ViewState["ArticleOID"] = value;
			}
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ArticleOID = new Guid(Request["OID"]);
				DataBind();
			}
			else
			{
				if (File.PostedFile != null && File.PostedFile.ContentLength != 0) SaveImage();

			}
		}

		public override void DataBind()
		{
            FuncNum.Value = Request["CKEditorFuncNum"];
			var manager = new BinDataManager();

			Images.DataSource = manager.GetImagesByArticle(ArticleOID);
			base.DataBind();
		}
		private void SaveImage()
		{
			var manager = new BinDataManager();
			manager.SaveImageForArticle(ArticleOID, new HttpPostedFileWrapper(File.PostedFile));
			DataBind();
		}

		protected void delImage_OnCommand(object sender, CommandEventArgs e)
		{
			var manager = new BinDataManager();
			manager.DelArticleImage(ArticleOID, (string)e.CommandArgument);
			DataBind();
		}
	}
}