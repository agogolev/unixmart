using System;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using Autofac;
using BackendService.Infrastructure;

namespace BackendService 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication, IContainerAccessor
    {
        public static IContainer Container { get; private set; }

        public IContainer GetContainer()
        {
            return Container;
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            CreateContainer();
        }

        protected virtual void CreateContainer()
        {
            var builder = new ContainerBuilder();

            RegisterInterfaces(builder);

            Container = builder.Build();
        }

        private static void RegisterInterfaces(ContainerBuilder builder)
        {
            foreach (var assembly in BuildManager.GetReferencedAssemblies().Cast<Assembly>())
            {
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.Name.EndsWith("Repository"))
                    .AsImplementedInterfaces();
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.Name.EndsWith("Manager"))
                    .AsImplementedInterfaces();
            }
        }

        protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
	}
}

