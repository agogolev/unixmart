﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    public partial class LayoutController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;
        private readonly IPromoBlockRepository _promoBlockRepository;
        private readonly ISessionRepository _sessionRepository;

        public LayoutController(
            IMapper mapper,
            IMenuRepository menuRepository,
            IPromoBlockRepository promoBlockRepository, 
            ISessionRepository sessionRepository)
        {
            _mapper = mapper;
            _menuRepository = menuRepository;
            _promoBlockRepository = promoBlockRepository;
            _sessionRepository = sessionRepository;
        }

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public virtual ActionResult Header(bool homePage)
        {
            ViewBag.HomePage = homePage;
            return PartialView(new HeaderModel
            {
                ShopPhone = Settings.Default.SHOP_PHONE_TEXT,
                PurePhone = Settings.Default.SHOP_PHONE_PURE,
                AlertMessage = new HtmlString(Settings.Default.AlertMessage),
                Menu = _menuRepository.GetNodeMenu(Settings.Default.TOP_MENU)
            });
        }

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public virtual ActionResult HeaderCat()
        {
            return PartialView(_mapper.Map<CatalogMenuModel[]>(_menuRepository.GetCatalogMenu(Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE)));
        }

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public virtual ActionResult FinishCat()
        {
            return PartialView(_mapper.Map<CatalogMenuModel[]>(_menuRepository.GetCatalogMenu(Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE)));
        }

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public virtual ActionResult CatNav(bool homePage)
        {
            ViewBag.HomePage = homePage;
            return PartialView(_mapper.Map<CatalogMenuModel[]>(_menuRepository.GetCatalogMenu(Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE)));
        }

        public virtual ActionResult Referal()
        {
            return PartialView(MVC.Layout.Views.Referal, _sessionRepository.RefererLabel);
        }

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public virtual ActionResult FooterNav()
        {
            return PartialView(new FooterNavModel
            {
                InfoMenu = new List <InfoMenu>(), 
                CatalogMenu = _mapper.Map<CatalogMenuModel[]>(_menuRepository.GetCatalogMenu(Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE)),
                ShopPhone = Settings.Default.SHOP_PHONE_TEXT,
                PurePhone = Settings.Default.SHOP_PHONE_PURE,
                WorkingTime = Settings.Default.SHOP_WORKING_TIME
            });
        }

        //[OutputCache(Duration = 3600, VaryByParam = "*")]
        public virtual ActionResult LeftPromoBlock()
        {
            return PartialView(_promoBlockRepository.GetPromoList(Settings.Default.LEFT_PROMO_BLOCK, null));
        }

        //public virtual ActionResult InfoMenu(int id)
        //{
        //    ViewBag.SelectedMenu = id;
        //    return PartialView(_menuRepository.GetInfoMenu(Settings.Default.INFO_ROOT));
        //}
    }
}