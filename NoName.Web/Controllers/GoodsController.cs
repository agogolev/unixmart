﻿using System;
using System.Web.Mvc;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 600, VaryByParam = "*")]
    public partial class GoodsController : BaseController
    {
        private readonly IGoodsPageBuilder _goodsPageBuilder;

        private readonly IUrlRepository _urlRepository;

        public GoodsController(
            IGoodsPageBuilder goodsPageBuilder,
            IUrlRepository urlRepository)
        {
            _goodsPageBuilder = goodsPageBuilder;
            _urlRepository = urlRepository;
        }

        public virtual ActionResult Index(string itemId, int id)
        {
            string canonicalUrl = _urlRepository.CanonicalGoodsUrl(id);
            if (Request.Path != canonicalUrl)
            {
                return RedirectPermanent(canonicalUrl);
            }

            ViewBag.Canonical = Request.Url.GetLeftPart(UriPartial.Path);

            if (Request.Browser.IsMobileDevice)
            {
                return View(MVC.Goods.Views.Index, _goodsPageBuilder.CreateItem(id));
            }
            return View(MVC.Goods.Views.Index, _goodsPageBuilder.CreateItem(id));
        }

        public virtual ActionResult SameCharsBlock(int id, int oid)
        {
            return PartialView(MVC.Goods.Views.SameCharsBlock, _goodsPageBuilder.GetSputnikGoods(id, oid, 6));
        }

        public virtual ActionResult Warn(int id)
        {
            if(Request.IsAjaxRequest())
            {
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("Warn", _goodsPageBuilder.CreateItem(id)) },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            return new EmptyResult();
        }
    }
}