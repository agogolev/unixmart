﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NLog;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    public class MarketController : Controller
    {
        private const string MarketUrl = "market0qznhpgm";
        private const int BUFFER_SIZE = 1024;

        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly List<string> _preserveHeaders = new List<string>()
        {
            "authorization",
            "charset",
            "cookie"
        };

        [Route(MarketUrl)]
        public ActionResult Index()
        {
            return new EmptyResult();
        }
        [HttpPost]
        [Route(MarketUrl + "/cart")]
        public async Task<ActionResult> Cart()
        {
            _log.Info("Start car request");
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, new Uri($"{Settings.Default.MarketDestination}cart"));
                await BuildMessage(message);
                await LogEventInformation(message);
                var response = await client.SendAsync(message);
                await LogEventInformation(response);
                return await BuildResult(response);
            }
        }

        [HttpPost]
        [Route(MarketUrl + "/order/accept")]
        public async Task<ActionResult> Accept()
        {
            _log.Info("Start order accept request");
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, new Uri($"{Settings.Default.MarketDestination}order/accept"));
                await BuildMessage(message);
                await LogEventInformation(message);
                var response = await client.SendAsync(message);
                await LogEventInformation(response);
                return await BuildResult(response);
            }
        }

        [HttpPost]
        [Route(MarketUrl + "/order/status")]
        public async Task<ActionResult> Status()
        {
            _log.Info("Start order status request");
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, new Uri($"{Settings.Default.MarketDestination}order/status"));

                await BuildMessage(message);
                await LogEventInformation(message);
                var response = await client.SendAsync(message);
                await LogEventInformation(response);
                return await BuildResult(response);
            }
        }

        [HttpPost]
        [Route(MarketUrl + "/stocks")]
        public async Task<ActionResult> Stocks()
        {
            _log.Info("Start stocks request");
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, new Uri($"{Settings.Default.MarketDestination}stocks"));
                await BuildMessage(message);
                await LogEventInformation(message);
                var response = await client.SendAsync(message);
                await LogEventInformation(response);
                return await BuildResult(response);
            }
        }

        [HttpPost]
        [Route(MarketUrl + "/order/cancellation/notify")]
        public async Task<ActionResult> Notify()
        {
            _log.Info("Start order cancellation notify request");
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post, new Uri($"{Settings.Default.MarketDestination}order/cancellation/notify"));
                await BuildMessage(message);
                await LogEventInformation(message);
                var response = await client.SendAsync(message);
                await LogEventInformation(response);
                return await BuildResult(response);
            }
        }

        private async Task BuildMessage(HttpRequestMessage message)
        {
            foreach (string name in Request.Headers.AllKeys
                .Where(h => _preserveHeaders.Any(i => string.Compare(i, h, StringComparison.InvariantCultureIgnoreCase) == 0)))
            {
                message.Headers.Add(name, Request.Headers[name]);
            }

            var stream = Request.GetBufferedInputStream();
            int count;
            var messageStream = new MemoryStream();
            var buffer = new byte[BUFFER_SIZE];
            do
            {
                count = await stream.ReadAsync(buffer, 0, BUFFER_SIZE);
                await messageStream.WriteAsync(buffer, 0, count);
            } while (count != 0);

            messageStream.Position = 0;
            message.Content = new StreamContent(messageStream);
        }

        private async Task<ActionResult> BuildResult(HttpResponseMessage response)
        {
            var respStream = await response.Content.ReadAsStreamAsync();
            int count;
            byte[] buffer = new byte[BUFFER_SIZE];
            var messageStream = new MemoryStream();
            do
            {
                count = await respStream.ReadAsync(buffer, 0, BUFFER_SIZE);
                await messageStream.WriteAsync(buffer, 0, count);
            } while (count != 0);

            messageStream.Position = 0;
            buffer = messageStream.ToArray();
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    var result = new ContentResult
                    {
                        Content = Encoding.UTF8.GetString(buffer),
                        ContentEncoding = Encoding.UTF8,
                        ContentType = "application/json"
                    };
                    return result;
                default:
                    var errorResult = new ContentResult
                    {
                        Content = buffer.Length != 0 ? Encoding.UTF8.GetString(buffer) : response.ReasonPhrase,
                        ContentEncoding = Encoding.UTF8,
                        ContentType = "application/html"
                    };
                    Response.StatusCode = (int)response.StatusCode;
                    return errorResult;
            }
        }

        private async Task LogEventInformation(HttpRequestMessage message)
        {
            _log.Debug($"\r\n{message}");
            var content = await message.Content.ReadAsStringAsync();
            _log.Debug($"\r\n{content}");
        }

        private async Task LogEventInformation(HttpResponseMessage message)
        {
            _log.Debug($"\r\n{message}");
            var content = await message.Content.ReadAsStringAsync();
            _log.Debug($"\r\n{content}");
        }
    }
}