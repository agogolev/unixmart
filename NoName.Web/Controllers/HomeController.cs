﻿using System;
using System.Linq;
using System.Web.Mvc;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 3600, VaryByParam = "*")]
    public partial class HomeController : Controller
    {
        private readonly IHomePageBuilder _homePageBuilder;
        private readonly ICompanyMenuRepository _companyMenuRepository;
        private readonly IGoodsRepository _goodsRepository;
        private readonly IUrlRepository _urlRepository;
        public HomeController(
            IHomePageBuilder homePageBuilder,
            ICompanyMenuRepository companyMenuRepository,
            IGoodsRepository goodsRepository, 
            IUrlRepository urlRepository)
        {
            _companyMenuRepository = companyMenuRepository;
            _goodsRepository = goodsRepository;
            _urlRepository = urlRepository;
            _homePageBuilder = homePageBuilder;
        }

        public virtual ActionResult Index()
        {
            var canonicalUrl = _urlRepository.CanonicalHomeUrl();
            if (Request.Path != canonicalUrl) return RedirectPermanent(canonicalUrl);
            ViewBag.HomePage = true;
            ViewBag.Canonical = Request.Url.GetLeftPart(UriPartial.Path);
            return View(_homePageBuilder.Create());
        }

        public virtual ActionResult AdvertWideList()
        {
            return PartialView(_homePageBuilder.AdvertWideList());
        }

        public virtual ActionResult Abc()
        {
            return PartialView(new AbcModel { Companys = _companyMenuRepository.GetCompanyMenuByABC(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT) });
        }

        public virtual ActionResult Hits()
        {
            return PartialView(MVC.Home.Views.GoodsList, _goodsRepository.GetHits(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT).Take(20).ToList());
        }

        public virtual ActionResult New()
        {
            return PartialView(MVC.Home.Views.GoodsList, _goodsRepository.GetNew(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT).Take(20).ToList());
        }

        public virtual ActionResult Special()
        {
            return PartialView(MVC.Home.Views.GoodsList, _goodsRepository.GetSpecial(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT).Take(20).ToList());
        }

    }
}