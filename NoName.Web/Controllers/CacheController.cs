﻿using System;
using System.Web.Mvc;
using DBReader;
using System.Configuration;

namespace NoName.Web.Controllers
{
	public partial class CacheController : Controller
	{
		public virtual ActionResult Index(Guid key, string prefix)
		{
			if (key != new Guid(ConfigurationManager.AppSettings["cacheControl"]))
				return new EmptyResult();
			WebServiceExtensions.ClearCurrentCache(prefix);
			return new EmptyResult();
		}

	}
}
