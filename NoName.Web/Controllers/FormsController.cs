﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Properties;
using Recaptcha.Web.Mvc;

namespace NoName.Web.Controllers
{
    public partial class FormsController : Controller
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IMailRepository _mailRepository;
        private readonly IFormRefuseRepository _formRefuseRepository;

        public FormsController(
            IArticleRepository articleRepository,
            IFormRefuseRepository formRefuseRepository,
            IMailRepository mailRepository, 
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _formRefuseRepository = formRefuseRepository;
            _mailRepository = mailRepository;
            _menuRepository = menuRepository;
        }
        [HttpGet]
        public virtual ActionResult Refuse()
        {
            var article = _articleRepository.GetNodeArticle(Settings.Default.RefuseArticle, null);
            var model = new FormRefuse
            {
                Article = article,
                SelectedId = article.Id,
                Menu = _menuRepository.GetInfoMenu(Settings.Default.INFO_ROOT)
            };

            return View(model);
        }
        [HttpPost]
        public virtual ActionResult Refuse(FormRefuse model)
        {
            model = model ?? new FormRefuse();
            var recaptchaHelper = this.GetRecaptchaVerificationHelper();

            if (string.IsNullOrEmpty(recaptchaHelper.Response))
            {
                ModelState.AddModelError("ReCaptcha", "подтвердите что вы не робот.");
            }

            try
            {
                var recaptchaResult = recaptchaHelper.VerifyRecaptchaResponse();

                if (!recaptchaResult.Success)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var err in recaptchaResult.ErrorCodes)
                    {
                        sb.AppendLine(err);
                    }

                    ModelState.AddModelError("ReCaptcha", sb.ToString());
                }
            }
            catch
            {
                ModelState.AddModelError("ReCaptcha", "подтвердите что вы не робот.");
            }



            if (!ModelState.IsValid)
            {
                model.WelcomeString = String.Empty;
            }
            else
            {
                //  Отключили сохранение формы заявки на возврат товара
                //_formRefuseRepository.SaveFormRefuse(model);
                _mailRepository.SendFormRefuse(model, Settings.Default.SERVICE_MAIL);
                model.WelcomeTitle = "Ваше обращение поступило.";
                model.WelcomeString = "Сотрудники нашей компании приступают к его обработке. Ответ мы отправим на указанный Вами электронный адрес.";
            }

            var article = _articleRepository.GetNodeArticle(Settings.Default.RefuseArticle, null);
            var tempArticle = new Article
            {
                Header = article.Header,
                Annotation = article.Annotation,
                Title = article.Title,
                Keywords = article.Keywords,
                PageDescription = article.PageDescription
            };
            model.Article = tempArticle;
            model.SelectedId = article.Id;
            model.Menu = _menuRepository.GetInfoMenu(Settings.Default.INFO_ROOT);

            return View(model);
        }
    }
}