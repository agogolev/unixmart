﻿using System.Web.Mvc;
using NoName.Repository.Contracts;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
	public partial class RequestController : Controller
	{
	    private readonly IMailRepository _mailRepository;

	    public RequestController(IMailRepository mailRepository)
	    {
	        _mailRepository = mailRepository;
	    }

        public virtual JsonResult Index(string callbackPhone)
		{
            if (!string.IsNullOrWhiteSpace(callbackPhone))
			{
                _mailRepository.RequestOperator(callbackPhone.Trim(), Settings.Default.REQUEST_MAIL);
				if (Request.IsAjaxRequest())
				{
					return Json(new
					{
						message = "Ваш запрос на вызов оператора принят"
					});
				}
			}
			else
			{
				if (Request.IsAjaxRequest())
				{
					return Json(new
					{
						message = "Вы не ввели номера телефона"
					});
				}
			}
			return Json(new
			{
				message = "empty"
			});
		}
	}
}
