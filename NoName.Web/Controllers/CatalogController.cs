﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 600, VaryByParam = "*")]
    public partial class CatalogController : Controller
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        private readonly ICatalogPageBuilder _catalogPageBuilder;
        private readonly IMenuRepository _menuRepository;
        private readonly IUrlRepository _urlRepository;

        public CatalogController(ICatalogPageBuilder catalogPageBuilder, IMenuRepository menuRepository, IUrlRepository urlRepository)
        {
            _catalogPageBuilder = catalogPageBuilder;
            _menuRepository = menuRepository;
            _urlRepository = urlRepository;
        }

        [Route("catalog/{catid}/{manufId?}")]
        public virtual ActionResult Category(string catid, string manufId, int? page, string sort = null)
        {
            var canonicalUrl = _urlRepository.CanonicalCatShortLinkUrl(catid, manufId, Settings.Default.CATALOG_ROOT);
            if (Request.Path != canonicalUrl)
            {
                if (canonicalUrl != "/catalog/")
                {
                    return RedirectPermanent(canonicalUrl);
                }
                else
                {
                    log.Error($"{catid}, {manufId} не найден");
                    throw new HttpException(404, "Page not found");
                }
            }

            ViewBag.Canonical = Request.Url.GetLeftPart(UriPartial.Path);
            if (page.HasValue && page == 1)
            {
                return RedirectToActionPermanent(MVC.Catalog.Category(catid, manufId, null, sort));
            }
            var category = _menuRepository.FromShortName(catid, Settings.Default.CATALOG_ROOT);
            if (category.IsTag)
            {
                if(manufId != null)
                {
                    log.Error($"{catid}, {manufId} не найден");
                    throw new HttpException(404, "Смешали лендинг с брендом. Page not found");
                }
                return LandingPage(category.NodeId, category.StringRepresentation, page, sort);
            }
            return category.HasChild && !category.GoodsList && string.IsNullOrEmpty(manufId) ? List(category.NodeId, catid) : Subcategory(category.NodeId, catid, manufId, page, sort, category.GoodsList);
        }

        [HttpPost]
        [Route("catalog/filterList")]
        public virtual ActionResult FilterList(CatalogFilterModel model)
        {
            if (Request.IsAjaxRequest())
            {
                if (model.LandingPage) return Content(_catalogPageBuilder.GetLandingRedirect(model));
                if (Request.Browser.IsMobileDevice)
                {
                    return PartialView(MVC.Catalog.Views.FilterList_Mobile, _catalogPageBuilder.FilterList(model));
                }
                return PartialView(MVC.Catalog.Views.FilterList, _catalogPageBuilder.FilterList(model));
            }
            return new EmptyResult();
        }
        private ActionResult List(int id, string itemId)   
        {
            var model = _catalogPageBuilder.Create(id, itemId);

            return View(MVC.Catalog.Views.List, model);
        }

        private ActionResult Subcategory(int id, string itemId, string manufId, int? page, string sort, bool withSameLevel)
        {
            GoodsListPageModel model = _catalogPageBuilder.CreateGoodsList(id, itemId, manufId, page, sort, withSameLevel);
            if (model.GoodsItems.Count == 0)
            {
                throw new HttpException(404, "Page not found");
                //model.SimilarGoods = _catalogPageBuilder.GetSimilarGoods(model.BreadCrumbs.Last().Name, model.BreadCrumbs.Select(m => m.NodeId).ToList());
            }

            if (Request.Browser.IsMobileDevice)
            {
                return View(MVC.Catalog.Views.Subcategory_Mobile, model);
            }
            return View(MVC.Catalog.Views.Subcategory, model);
        }

        private ActionResult LandingPage(int objectId, string itemId, int? page, string sort)
        {
            GoodsListPageModel model = _catalogPageBuilder.CreateLandingPage(objectId, itemId, page, sort);
            if (model.GoodsItems.Count == 0)
            {
                //throw new HttpException(404, "Empty goods list");
                var currentCategoryName = model.BreadCrumbs.Last().Name;
                var catalogList = model.BreadCrumbs.Select(m => m.NodeId).TakeWhile(n => n != 0).ToList();
                model.SimilarGoods = _catalogPageBuilder.GetSimilarGoods(currentCategoryName, catalogList);
            }
            return View(MVC.Catalog.Views.LandingPage, model);
        }

    }
}