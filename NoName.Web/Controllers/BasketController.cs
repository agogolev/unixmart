﻿using System;
using System.Web.Mvc;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    public partial class BasketController : BaseController
    {
        private readonly IBasketPageBuilder _basketPageBuilder;
        private readonly IGoodsRepository _goodsRepository;
        private readonly IMailRepository _mailRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly ISessionRepository _sessionRepository;

        public BasketController(
            IBasketPageBuilder basketPageBuilder,
            IGoodsRepository goodsRepository,
            IMailRepository mailRepository,
            IOrderRepository orderRepository, 
            ISessionRepository sessionRepository)
        {
            _basketPageBuilder = basketPageBuilder;
            _goodsRepository = goodsRepository;
            _mailRepository = mailRepository;
            _orderRepository = orderRepository;
            _sessionRepository = sessionRepository;
        }

        public virtual ActionResult BasketAmount()
        {
            return PartialView(_sessionRepository.Basket);
        }

        [HttpGet]
        public virtual ActionResult Index()
        {
            BasketViewModel model = _basketPageBuilder.Create();
            if (model.TotalGoodsCount == 0)
            {
                return RedirectToAction(MVC.Home.Index());
            }

            ViewBag.CurrentStep = 0;
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Index(BasketViewModel model)
        {
            _sessionRepository.TotalFinishCount = 0;
            if (Request.IsAjaxRequest()) 
            {
                var newModel = _basketPageBuilder.Save(model);
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketContent", newModel), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }

            if (ModelState.IsValid)
            {
                _basketPageBuilder.Save(model);
                return RedirectToAction(MVC.Order.Index());
            }

            _basketPageBuilder.FillModel(model);
            return View(model);
        }

        public virtual ActionResult Add(int goodsID)
        {
            _basketPageBuilder.Add(goodsID);
            if (Request.IsAjaxRequest())
            {
                var basket = _sessionRepository.Basket;
                var viewName = "BasketAdd";
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString(viewName, basket), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult AddSale(int goodsID)
        {
            _basketPageBuilder.AddSale(goodsID);
            if (Request.IsAjaxRequest())
            {
                var basket = _sessionRepository.Basket;
                var viewName = "BasketAdd";
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString(viewName, basket), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        //[HttpPost]
        public virtual ActionResult Set(int ordValue, int amount, bool isSale)
        {
            _basketPageBuilder.Set(ordValue, amount, isSale);
            if (Request.IsAjaxRequest())
            {
                var basket = _sessionRepository.Basket;
                var viewName = "BasketAdd";
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString(viewName, basket), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult AddAccessory(int goodsID)
        {
            _basketPageBuilder.Add(goodsID);
            if (Request.IsAjaxRequest())
            {
                BasketViewModel model = _basketPageBuilder.Create();
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketContent", model), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult Delete(int id, bool isSale)
        {
            _basketPageBuilder.Delete(id, isSale);
            if (Request.IsAjaxRequest())
            {
                BasketViewModel model = _basketPageBuilder.Create();
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketContent", model), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult BuyOneClick(string goodsOID)
        {
            Guid OID;
            if (!Guid.TryParse(goodsOID, out OID)) return new EmptyResult();
            _sessionRepository.TotalFinishCount = 0;
            return PartialView(new BuyOneClickModel
            {
                GoodsOID = OID
            });
        }

        [HttpPost]
        public virtual ActionResult BuyOneClick(BuyOneClickModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var id = _basketPageBuilder.QuickSave(model);
                    Order order = _orderRepository.GetOrder(id, Settings.Default.CATALOG_ROOT);

                    // отправляем сообщения о совершении заказа
                    //_mailRepository.SendOrderInfo(
                    //    order,
                    //    Settings.Default.ORDER_MAIL,
                    //    Settings.Default.SITE_URL,
                    //    null);

                    //запоминаем номер последнего сделанного заказа
                    _sessionRepository.LastOrderOID = order.OID;

                    return Json(new { redirect = $"/finish?orderOID={order.OID}"}, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { message = "Произошла ошибка " + e.Message }, JsonRequestBehavior.AllowGet);
                }

            }

            return Json(new { message = "Заполните обязательные поля" }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult ReportAvailable()
        {
            return PartialView(MVC.Basket.Views.ReportAvailable, new ReportAvailableModel());
        }

        [HttpPost]
        public virtual ActionResult ReportAvailable(ReportAvailableModel model)
        {
            var goods = _goodsRepository.GetGoodsItem(
                model.GoodsID, 
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT);
            _mailRepository.ReportAvailable(goods, model.Phone, model.Email, Settings.Default.REQUEST_MAIL);
            return Json(new
            {
                message = "Запрос принят"
            });
        }
    }
}