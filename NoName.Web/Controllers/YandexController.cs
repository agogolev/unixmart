﻿using System.Text;
using System.Web.Mvc;
using NoName.Repository.Contracts;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    public partial class YandexController : Controller
	{
	    private readonly IYandexMarketRepository _yandexMarketRepository;
	    private readonly ILocationRepository _locationRepository;

	    public YandexController(ILocationRepository locationRepository, IYandexMarketRepository yandexMarketRepository)
	    {
	        _locationRepository = locationRepository;
	        _yandexMarketRepository = yandexMarketRepository;
	    }

        public void Index()
		{
			Response.ContentType = "text/xml";
			Response.ContentEncoding = Encoding.GetEncoding(1251);
			_yandexMarketRepository.GetMarketData(Response, Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                    Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup, Settings.Default.HasStore, false);
		}

        public void IndexSecond()
        {
            Response.ContentType = "text/xml";
            Response.ContentEncoding = Encoding.GetEncoding(1251);
            _yandexMarketRepository.GetMarketData(Response, Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup, Settings.Default.HasStore, true);
        }

		public void IndexRise()
        {
            Response.ContentType = "text/xml";
            Response.ContentEncoding = Encoding.GetEncoding(1251);
            _yandexMarketRepository.GetMarketDataRise(Response, Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup, Settings.Default.HasStore);
        }

  //      public void Admin()
		//{
		//	Response.ContentType = "text/xml";
		//	Response.ContentEncoding = Encoding.GetEncoding(1251);
		//	_yandexMarketRepository.GetMarketData(Response, Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
  //                  Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup, Settings.Default.HasStore, true);
		//}
		
        public void Refresh()
		{
			_yandexMarketRepository.RefreshMarketData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                    Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup, Settings.Default.HasStore);
		}

        public virtual ActionResult Map(int id)
        {
            return View(_locationRepository.GetLocation(id));
        }

        [HttpPost]
        public virtual JsonResult GetLocation(int id)
        {
            var location = _locationRepository.GetLocation(id);
            return Json(location);
        }

	}
}
