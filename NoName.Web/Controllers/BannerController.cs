﻿using System;
using System.Web.Mvc;
using Ecommerce.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 3600, VaryByParam = "*")]
    public partial class BannerController : Controller
	{
		private readonly IBannerRepository bannerRepository;
		private readonly ISessionRepository sessionRepository;

	    public BannerController(IBannerRepository bannerRepository, ISessionRepository sessionRepository)
	    {
	        this.bannerRepository = bannerRepository;
            this.sessionRepository = sessionRepository;
        }

        public virtual ActionResult Show(int pid, Guid? oid)
		{
			var cid = Settings.Default.BannerClientID;
			Guid sessionId = sessionRepository.AdvertSessionId;
			int region = sessionRepository.RegionId;
			AdvertPlace item = bannerRepository.GetBanners(cid, pid, sessionId, region, oid, Settings.Default.CATALOG_ROOT);

			return View(item.Html());
		}
		public virtual ActionResult Click(int id, Guid key)
		{
			bannerRepository.RegisterClick(id, key);
			string url = bannerRepository.GetBannerUrl(id);
			return new RedirectResult(url);
		}

	}
}
