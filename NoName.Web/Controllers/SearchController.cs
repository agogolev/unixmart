﻿using System.Web.Mvc;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;

namespace NoName.Web.Controllers
{
    public partial class SearchController : Controller
    {
        private readonly ISearchPageBuilder _searchPageBuilder;

        public SearchController(ISearchPageBuilder searchPageBuilder)
        {
            _searchPageBuilder = searchPageBuilder;
        }

        public virtual ActionResult Index(string search_query, int? id, int? page, string sort = null)
        {
            SearchPageModel model = _searchPageBuilder.Create(search_query, id, page, sort);
            if (model.PagingModel.Total == 1)
            {
                var goods = model.PagingModel.Items[0];
                return RedirectToAction(MVC.Goods.Index(goods.ItemUrl, goods.ObjectID));
            }
            return View(model);
        }
    }
}