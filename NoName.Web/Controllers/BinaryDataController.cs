﻿using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using NoName.Repository.Contracts;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 3600, VaryByParam = "*")]
    public partial class BinaryDataController : Controller
    {
        private static readonly Regex re = new Regex(@"^\d+");
        private readonly IBinaryDataRepository _binaryDataRepository;

        public BinaryDataController(IBinaryDataRepository binaryDataRepository)
        {
            _binaryDataRepository = binaryDataRepository;
        }

        //[AcceptVerbs("GET", "HEAD")]
        public virtual ActionResult Index(string id)
        {
            try
            {
                string mimeType;
                var data = _binaryDataRepository.GetImageById(ParseInt(id), out mimeType);
                return new FileContentResult(data, mimeType);
            }
            catch
            {
                return HttpNotFound();
            }
        }

        private static int ParseInt(string idString)
        {
            var match = re.Match(idString);
            if (!match.Success)
            {
                throw new ArgumentException("Not an integer");
            }
            return int.Parse(match.Value);
        }
    }
}