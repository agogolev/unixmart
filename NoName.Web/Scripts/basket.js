﻿(function ($) {
    if ($(".cart-goods-item").length) {
        $(document).on("click", ".cart-goods-item-quantity-decrease", function () {
            var input = $(this).siblings("input[type=text]"),
                i = parseInt(input.val());
            if (input.attr("readonly") === "readonly") return;
            input.prop("readonly", true);
            if (!isNaN(i) && i > 1) {
                input.val(i - 1);
                $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
                    $("#basketForm").html(response.html);
                    $("#basketAmount").html(response.basketAmount);
                    input.prop("readonly", false);
                });
            } else {
                input.val("1");
                input.prop("readonly", false);
            }
        });
        $(document).on("click", ".cart-goods-item-quantity-increase", function () {
            var input = $(this).siblings("input[type=text]"),
                i = parseInt(input.val());
            if (input.attr("readonly") === "readonly") return;
            input.prop("readonly", true);
            if (!isNaN(i)) {
                input.val(i + 1);
                $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
                    $("#basketForm").html(response.html);
                    $("#basketAmount").html(response.basketAmount);
                    input.prop("readonly", false);
                });
            } else {
                input.val("1");
                input.prop("readonly", false);
            }
        });
    }

    $(document).on("click", ".cart-goods-item-close", function () {
        var id = $(this).data("id");
        var sale = $(this).data("sale");
        $.post("/basket/delete/", { "id": id, "isSale": sale }, function (response) {
            $("#basketForm").html(response.html);
            $("#basketAmount").html(response.basketAmount);
        });
        return false;
    });

    $(document).on("change", "input[name='DeliveryType']", function () {
        $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
            $("#basketForm").html(response.html);
            $("#basketAmount").html(response.basketAmount);
        });
    });

    $(document).on("focusout", "input[name='CalcDistance']", function () {
        $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
            $("#basketForm").html(response.html);
            $("#basketAmount").html(response.basketAmount);
        });
    });

    $(document).on("focusout", "input[name='PromoCode']", function () {
        $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
            $("#basketForm").html(response.html);
            $("#basketAmount").html(response.basketAmount);
        });
    });

    $(document).on("change", "input[name='PayType']", function () {
        $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
            $("#basketForm").html(response.html);
            $("#basketAmount").html(response.basketAmount);
        });
    });


    $(document).on("click", "#SelectCoupon", function () {
        $.post($("#basketForm").attr("action"), $("#basketForm").serialize(), function (response) {
            $("#basketForm").html(response.html);
            $("#basketAmount").html(response.basketAmount);
        });
    });

})(jQuery);