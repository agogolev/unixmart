﻿(function ($) {
    $("[data-action=ajax-dropdown]").each(function () {
        $(this).pr_dropdown2(buyGoods);
    });

    $("[data-action=ajax-sale-dropdown]").each(function () {
        $(this).pr_dropdown2(buySale);
    });

    $("[data-action=available-dropdown]").each(function () {
        $(this).pr_dropdown2(reportAvailable);
    });

    $(document).on("click", ".accessory-buy", function ()
    {
        var id = $(this).data("id");
        $.ajax({
            async: true,
            type: "POST",
            url: "/basket/addAccessory",
            data: { "goodsID": id },
            success: function (response) {
                $("#basketForm").html(response.html);
                $("#basketAmount").html(response.basketAmount);
            }
        });
    });

    $(document).on("click", "#Basket_Plus", function () {
        var ordValue = $(this).siblings("#LastAddedItem_OrdValue").val(),
            isSale = $(this).siblings("#LastAddedItem_IsSale").val() === "1",
            amount = $(this).siblings("#LastAddedItem_Amount").val(),
            cartId = isSale ? $("#cartSale") : $("#cartPopup");

        if (!isNaN(amount)) {
            amount++;
        }

        $.ajax({
            async: true,
            type: "POST",
            url: "/basket/set",
            data: { "ordValue" : ordValue, "isSale": isSale, "amount": amount },
            success: function (data) {
                $(cartId).html(data.html);
                $("#basketAmount").html(data.basketAmount);
            }
        });

        return false;
    });
    $(document).on("click", "#Basket_Minus", function () {
        var ordValue = $(this).siblings("#LastAddedItem_OrdValue").val(),
            isSale = $(this).siblings("#LastAddedItem_IsSale").val() === "1",
            amount = $(this).siblings("#LastAddedItem_Amount").val(),
            cartId = isSale ? $("#cartSale") : $("#cartPopup");

        if (!isNaN(amount) && amount > 1) {
            amount--;
        }

        $.ajax({
            async: true,
            type: "POST",
            url: "/basket/set",
            data: { "ordValue": ordValue, "isSale": isSale, "amount": amount },
            success: function (data) {
                $(cartId).html(data.html);
                $("#basketAmount").html(data.basketAmount);
            }
        });
    });

    $(".requestCallback").submit(function () {
        var form = $(this),
            phoneText = form.find("input[name=callbackPhone]");
        if (phoneText.val() === "") {
            $(phoneText).attr("placeholder", "Телефон?");
            $(phoneText).addClass("validation-error");
            return false;
        }
        $("body").find(".popup-container").removeClass("opened");
        $.post(form.attr("action"), form.serialize(), function (response) {
            var message = response.message;
            if (message === "empty") message = "Вы не ввели номер телефона";
            $("body").trigger("pr_dropdown_close_all", []);
            alert(message);
        });
        return false;
    });

    $("#reportAvailableForm").submit(function () {
        var form = $(this);
        $("body").find(".popup-container").removeClass("opened");
        $.post(form.attr("action"), form.serialize(), function (response) {
            var message = response.message;
            $("body").trigger("pr_dropdown_close_all", []);
            alert(message);
        });
        return false;
    });

    $("#BuyOneClick").validate({
        focusInvalid: true
    });
    $("#BuyOneClick").submit(function () {
        $("#BtnBuyOneClick").attr("disabled", "disabled");
        $.post($(this).attr("action"), $(this).serialize(), function (data) {
            if (data.message != null) {
                alert(data.message);
            }
            if (data.redirect != null) {
                location.href = data.redirect;
            }
            $("#BtnBuyOneClick").removeAttr("disabled");
        });
        return false;
    });

    $(".mask-phone").mask("9(999)9999999");   

})(jQuery);

function showInfo(id) {
    $.get("/goods/warn", { id: id }, function (data) {
            $("#warn").html(data.html);
        }
    );
}

function buyGoods(elem) {
    var id = $(elem).data("id"),
        cartId = $("#cartPopup");
    $.ajax({
        async: true,
        type: "POST",
        url: "/basket/add",
        data: { "goodsID": id },
        success: function(data) {
            $(cartId).html(data.html);
            $("#basketAmount").html(data.basketAmount);
        }
    });
}

function buySale(elem) {
    var id = $(elem).data("id"),
        cartId = $("#cartSale");
    $.ajax({
        async: true,
        type: "POST",
        url: "/basket/addSale",
        data: { "goodsID": id },
        success: function (data) {
            $(cartId).html(data.html);
            $("#basketAmount").html(data.basketAmount);
        }
    });
}

function reportAvailable(elem) {
    var id = $(elem).data("id");
    $("#GoodsID").val(id);
}