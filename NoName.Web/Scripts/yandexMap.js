﻿$(function () {
    var myMap;
    $("div[id^='yandexMap_']").each(function () {
        var id = this.id;
        var locationId = id.split('_')[1];

        $.post("/yandex/getlocation", { id: locationId }, function (data) {
            ymaps.ready(function () {
                myMap = new ymaps.Map('yandexMap_' + data.Id, {
                    center: [data.Lattitude, data.Longitude],
                    zoom: 13
                });

                myMap.controls
				.add('zoomControl')
				.add('typeSelector')
				.add('mapTools');

                myMap.behaviors.enable('scrollZoom');

                var myRedCollection = new ymaps.GeoObjectCollection({}, {
                    preset: 'twirl#redDotIcon' //все метки красные
                });
                myRedCollection.add(
					new ymaps.Placemark([data.Lattitude, data.Longitude], {
					    name: data.Name,
					    address: data.Address
					})
				);
                var myBalloonLayout = ymaps.templateLayoutFactory.createClass(
					'<p><strong>Адрес:</strong> $[properties.address]</p>'
				);
                ymaps.layout.storage.add('my#superlayout', myBalloonLayout);
                myRedCollection.options.set({
                    balloonContentBodyLayout: 'my#superlayout',
                    balloonMaxWidth: 300
                });
                myMap.geoObjects.add(myRedCollection);
            });
        });
    });
});