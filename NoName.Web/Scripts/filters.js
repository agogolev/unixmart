﻿var handler = function () {
    $("#Page").val("1");
    var state = {};
    state[$(this).attr("id")] = true;
    var checked = $(this).prop("checked");
    if (checked)
        $.bbq.pushState(state, 1);
    else
        $.bbq.removeState($(this).attr("id"));
};

function loadContent(response) {
    $("#GoodsList").html(response);
    $(".catalogContent-tags").readmore({ collapsedHeight: 60, moreLink: '<a href="#">показать больше</a>', lessLink: '<a href="#">скрыть</a>' });
}

$(function () {
    $(document).on("change", "input[id^='Manufs']", handler);
    $(document).on("change", "input[id^='CustomFilters']", handler);

    $(document).on("click", ".sortOption-up", function () {
        $("#Page").val(null);
        $("#TypeSort").val("up");
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });
    $(document).on("click", ".sortOption-down", function () {
        $("#Page").val(null);
        $("#TypeSort").val("down");
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });

    $(document).on("click", ".sortOption-pop", function () {
        $("#Page").val(null);
        $("#TypeSort").val("pop");
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });

    $(document).on("click", ".numOnPage", function () {
        $("#Page").val($(this).attr("rel"));
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });
    $(window).bind('hashchange', function () {
        $("input[id^='Manufs']").each(function () {
            var defaultState = this.id === defaultCheckBox;
            var state = $.bbq.getState(this.id, true) || defaultState;
            if (state) {
                $(this).attr("checked", "checked");
            }
            else {
                $(this).removeAttr("checked");
            }

        });
        $("input[id^='CustomFilters']").each(function () {
            var state = $.bbq.getState(this.id, true) || false;
            if (state) {
                $(this).attr("checked", "checked");
            }
            else {
                $(this).removeAttr("checked");
            }

        });
        if ($("#filterForm").length != 0) {
            $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
                loadContent(response);
                $("[data-action=ajax-dropdown]").each(function () {
                    $(this).pr_dropdown2(buyGoods);
                });

                $("[data-action=ajax-sale-dropdown]").each(function () {
                    $(this).pr_dropdown2(buySale);
                });

                $("[data-action=available-dropdown]").each(function () {
                    $(this).pr_dropdown2(reportAvailable);
                });
            });
        }

    });

    $(window).trigger('hashchange');
});
