﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class CardGalleryModel
    {
        public string Classes { get; set; }
        public GoodsItem GoodsItem { get; set; }
    }
}