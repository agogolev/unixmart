﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoName.Web.Models
{
    public class ReportAvailableModel
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public int GoodsID { get; set; }
    }
}