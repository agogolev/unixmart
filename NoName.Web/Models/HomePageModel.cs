﻿using System.Collections;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class HomePageModel
    {
        public IList<PromoBlock> PromoList { get; set; }
        public Article SeoArticle { get; set; }
    }
}