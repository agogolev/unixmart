﻿using System;

namespace NoName.Web.Models
{
    public class ManufacturerModel
    {
        public Guid OID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string AltName { get; set; }
        public string ShortLink { get; set; }
        public bool Selected { get; set; }
        public int GoodsCount { get; set; }
    }
}