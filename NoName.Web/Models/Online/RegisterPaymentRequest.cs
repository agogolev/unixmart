﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoName.Web.Models.Online
{
    public class RegisterPaymentRequest
    {
        public int OrderNumber { get; set; }
        public int PriceKop { get; set; }
        public string PaymentNumber { get; set; }
        public string RedirectUrl { get; set; }
    }
}