﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoName.Web.Models.Online
{
    public class RegisterPaymentResponse
    {
        public string OrderId { get; set; }
        public string FormUrl { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}