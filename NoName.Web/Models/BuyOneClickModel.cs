﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NoName.Web.Models
{
    public class BuyOneClickModel
    {
		[Required(ErrorMessage = "*")]
        public string Phone { get; set; }
		[Required(ErrorMessage = "*")]
		public string Name { get; set; }
        public string Comments { get; set; }
        public Guid GoodsOID { get; set; }
    }
}