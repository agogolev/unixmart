﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecommerce.Extensions;

namespace NoName.Web.Models
{
	public class BasketAmountModel
	{
		public int GoodsCount { get; set; }
		public decimal TotalPrice { get; set; }
		public HtmlString CartAmount { get
		{
			return
				string.Format("<span>{0} на сумму {1:#,#0.00} руб.</span>", "товар".Decline(GoodsCount),
				              TotalPrice).ToHtml();
		} }
	}
}