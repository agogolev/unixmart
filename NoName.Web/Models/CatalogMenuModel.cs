﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class CatalogMenuModel
    {
        public ManufacturerModel SelectedBrand { get; set; }
        public bool UseBrandUrl { get; set; }
        public Guid OID { get; set; }
        public int OrdValue { get; set; }
        public Image Image { get; set; }
        public int NodeId { get; set; }
        public int ParentNodeId { get; set; }

        public string StringRepresentation { get; set; }
        public string Name { get; set; }
        public string Canonical { get; set; }
        public string Css1 { get; set; }
        public string Css2 { get; set; }
        public virtual ActionResult Action => UseBrandUrl && SelectedBrand != null ?
            MVC.Manufacturer.List(SelectedBrand.ShortLink, StringRepresentation) :
            MVC.Catalog.Category(StringRepresentation, SelectedBrand?.ShortLink, null);
        public bool HasChild { get; set; }
        public bool Selected { get; set; }
        public IList<CatalogMenuModel> ChildMenu { get; set; }
        public int GoodsCount { get; set; }
        public IList<GoodsItem> TopGoodsItems { get; set; }
        public string Title { get; set; }
        public string H1 { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
        public IList<CatalogMenuModel> BreadCrumbs { get; set; }
        public Article SeoArticle { get; set; }
        public SimilarGoodsModel SimilarGoods { get; set; }
    }
}