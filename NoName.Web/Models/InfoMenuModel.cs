﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NoName.Web.Models
{
	public class InfoMenuModel
	{
	    public string Text { get; set; }

        public HtmlString HtmlText { get { return new HtmlString(Text);} }

	    public ActionResult Action { get; set; }
	}
}