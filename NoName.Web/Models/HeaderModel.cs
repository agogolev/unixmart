﻿using System.Collections.Generic;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class HeaderModel
    {
        public string ShopPhone { get; set; }
        public string PurePhone { get; set; }

        public HtmlString AlertMessage { get; set; }
        public IEnumerable<NodeMenu> Menu { get; set; } 
    }
}