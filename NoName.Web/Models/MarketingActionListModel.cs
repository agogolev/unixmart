﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class MarketingActionListModel
    {
        public IList<InfoMenu> Menu { get; set; }

        public PagingModel<MarketingAction> Items { get; set; }

        public Article Article { get; set; }

        //public string Title { get; set; }

        //public string Keywords { get; set; }

        //public string PageDescription { get; set; }
    }
}