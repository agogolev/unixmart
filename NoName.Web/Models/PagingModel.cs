﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NoName.Web.Models
{
    public class PagingModel<T> : IEnumerable<int>
    {
        private int? _page;
        public IList<T> Items { get; set; }

        public int Page
        {
            get { return _page ?? 1; }
            set { _page = value; }
        }

        public int NumOnPage { get; set; }

        public int Total { get; set; }

        public string Sort { get; set; }

        public string Fragment { get; set; }

        public int TotalPages => Page > 0 ? (int) Math.Ceiling(Total*1.0/NumOnPage) : 1;

        public bool HasPrev => Page > 1;

        public bool HasNext => Page > 0 && Page != TotalPages;

        public IEnumerator<int> GetEnumerator()
        {
            for (var i = 1; i <= TotalPages; i++)
            {
                if (i == 1 ||
                    (i == TotalPages && TotalPages - Page < 20) ||
                    (i%10 == 0 && ((i > Page && i - Page < 20) || (i < Page && Page - i < 20))) ||
                    i == Page ||
                    ((Page >= 3 || TotalPages - Page >= 3) && ((i > Page && i - Page < 3) || (i < Page && Page - i < 3))) ||
                    ((Page == 1 || Page == 2) && i < 6) ||
                    ((Page == TotalPages || Page == TotalPages - 1) && TotalPages - i < 5)
                    )
                    yield return i;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}