﻿using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class SearchPageModel
    {
        public SearchPagingModel PagingModel { get; set; }

        public IList<CatalogMenu> Menu { get; set; }

        public string PhoneIfEmpty { get; set; }
    }
}