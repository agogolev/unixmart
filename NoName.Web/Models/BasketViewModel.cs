﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class BasketViewModel
    {
        public Guid OID { get; set; }
        public string CouponCode { get; set; }
        public bool NeedCityInOrder { get; set; }
        public decimal VariableDeliveryPart { get; set; }
        public decimal DeliveryPrice { get; set; }
        public IList<BasketContent> Items { get; set; }
        public int? DeliveryType { get; set; }
        public decimal DeliveryPriceWithoutDistance { get; set; }
        public int CalcDistance { get; set; }
        public decimal Discount { get; set; }
        public string PromoCode { get; set; }
        public string PromoCodeText {
            get
            {
                if (Discount != 0) return $"скидка: {Discount:#,#0 руб\\.}";
                return "";
            }
        }
        public IList<DeliveryInfoItem> Deliveries { get; set; }
        [Required(ErrorMessage = "Укажите способ оплаты")]
        public int? PayType { get; set; }
        public IList<PayInfoItem> PayTypes { get; set; }
        public int TotalGoodsCount => Items?.Sum(k => k.Amount) ?? 0;
        public decimal TotalPrice
        {
            get
            {
                decimal totalPrice = 0;
                if (Items != null) totalPrice += Items.Sum(item => item.TotalPrice);
                return totalPrice;
            }
        }
        public decimal TotalFullPrice => TotalPrice + DeliveryPrice;
        public string TotalFullPriceString => TotalFullPrice.ToString("#,#0 руб\\.");
        public IList<GoodsItem> Accessories { get; set; }
        public bool OnlinePayment { get; set; }
    }
}