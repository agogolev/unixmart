﻿using System;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class GoodsPagingModel : PagingModel<GoodsItem>
    {
        public int Id { get; set; }
        public string ItemId { get; set; }
        public string ManufId { get; set; }
        public string SearchQuery { get; set; }
    }
}