using System;
using System.Collections.Generic;
using Ecommerce.Domain;
using NoName.Web.Models;

namespace NoName.Web.Infrastructure.Factories
{
	public interface ICatalogFilterFactory
	{
        CatalogFilterModel Create(int catalogID, int parentNodeId, int? page, int numOnPage, string sort, string itemId, string manufId, IList<ManufacturerModel> manufs, IList<CustomFilter> filters);
	    IList<Guid> CreateManufacturerFilter(IList<ManufacturerModel> manufs);
        IList<CustomFilter> CreateCustomFilter(IList<CustomFilterMenuModel> filters, IList<CustomFilter> allFilters);
    }
}