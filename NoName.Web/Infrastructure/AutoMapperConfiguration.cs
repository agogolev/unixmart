﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Infrastructure
{
    public class AutoMapperConfiguration
    {
        public static IMapper Configure()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => {
                cfg.CreateMap<CatalogMenu, CatalogMenuModel>();
                cfg.CreateMap<CatalogMenu, GoodsListPageModel>();
                cfg.CreateMap<Basket, BasketViewModel>()
                    .ForMember(dest => dest.PromoCode,
                        opts => opts.MapFrom(src => src.Coupon.PromoCode));
                cfg.CreateMap<Company, ManufacturerModel>();
            });

            return mapperConfiguration.CreateMapper();
        }
    }
}