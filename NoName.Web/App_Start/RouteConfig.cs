﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ecommerce.Extensions;

namespace NoName.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.AppendTrailingSlash = true;

            routes.MapRoute(
                null,
                "cache_control.aspx", // URL with parameters
                MVC.Cache.Index()
                );


            routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });
            routes.IgnoreRoute("{*allphp}", new { allphp = @".*\.php(/.*)?" });
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("bin.aspx");
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("robots.txt");
            routes.IgnoreRoute("images/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");
            //routes.IgnoreRoute("{*allpng}", new { allpng = @".*\.png(/.*)?" });
            //routes.IgnoreRoute("{*allgif}", new { allgif = @".*\.gif(/.*)?" });
            //routes.IgnoreRoute("{*alljpg}", new { alljpg = @".*\.jpg(/.*)?" });

            // user defines ignores
            routes.IgnoreRoute("reenter_files/{*pathInfo}");

            routes.LowercaseUrls = true;

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                null,
                "brand/{manufId}/{itemId}", // URL with parameters
                MVC.Manufacturer.List().AddRouteValue("itemId", null)
                );

            
            routes.MapRoute(
                null,
                "goods/{itemId}-{id}", // URL with parameters
                MVC.Goods.Index()
                );

            routes.MapRoute(
                null,
                "image/{id}/{width}/{height}", // URL with parameters
                new { controller = "Image", action = "Index" } // Parameter defaults
                );

            routes.MapRoute(
                null,
                "info/{itemId}", // URL with parameters
                MVC.Article.Index()
                );

            routes.MapRoute(
                null,
                "basket", // URL with parameters
                new { controller = "Basket", action = "Index", Area = "" } // Parameter defaults
                );

            routes.MapRoute(
                null,
                "order", // URL with parameters
                new { controller = "Order", action = "Index", Area = "" } // Parameter defaults
                );

            routes.MapRoute(
                null,
                "finish", // URL with parameters
                new { controller = "Order", action = "Finish", Area = "" } // Parameter defaults
                );

            routes.MapRoute(
                null,
                "exportOrders", // URL with parameters
                new { controller = "Order", action = "ExportOrders", Area = "" } // Parameter defaults
            );

            routes.MapRoute(
                null,
                "refuse", // URL with parameters
                new { controller = "Forms", action = "Refuse" } // Parameter defaults
            );

            routes.MapRoute(
                null,
                "yml/unix.goodslist.xml", // URL with parameters
                new { controller = "Yandex", action = "Index" } // Parameter defaults
                );

            routes.MapRoute(
                null,
                "yml/unixmart.goodslist.xml", // URL with parameters
                new { controller = "Yandex", action = "IndexRise" } // Parameter defaults
            );

            routes.MapRoute(
                null,
                "yml/unix.list.xml", // URL with parameters
                new { controller = "Yandex", action = "IndexSecond" } // Parameter defaults
            );

            routes.MapRoute(
                null,
                "img/{id}", // URL with parameters
                new { controller = "BinaryData", action = "Index" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, Area = "" }
                );
        }
    }
}
