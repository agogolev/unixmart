﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public static class CdnUrl
    {
        public static string ToCdn(this string url)
        {
            if (Settings.Default.cdn)
            {
                return $"{Settings.Default.cdnAddress}{url}";
            }

            return url;
        }
    }
}