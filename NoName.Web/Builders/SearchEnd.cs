﻿using System;
using NoName.Web.Builders.Contracts;

namespace NoName.Web.Builders
{
    class SearchEnd : ISearchEnd
    {
        private readonly string _endOf;

        public SearchEnd(string endOf)
        {
            _endOf = $"{endOf}%";
        }

        public bool IsEnding(string item)
        {
            return item.EndsWith(_endOf, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}