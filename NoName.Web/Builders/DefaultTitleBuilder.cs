﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using NoName.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;

namespace NoName.Web.Builders
{
    public class DefaultTitleBuilder : IDefaultTitleBuilder
    {
        private readonly IDictionary<ISearchEnd, Func<string, string>> _suffixMatch;
        private readonly IDefaultTitleRepository _defaultTitleRepository;

        public DefaultTitleBuilder(IDefaultTitleRepository defaultTitleRepository)
        {
            _defaultTitleRepository = defaultTitleRepository;
            _suffixMatch = new Dictionary<ISearchEnd, Func<string, string>>
            {
                { new SearchEnd("LFW"), s => s.ToLowerFirstWord()},
                { new SearchEnd("UP"), s => s.ToUpper()},
                { new SearchEnd("LOW"), s => s.ToLower()}
            };
        }
        /// <summary>
        /// копируем объект так как потом над ним будут производиться манипуляции
        /// </summary>
        /// <param name="defaultType"></param>
        /// <param name="shortLink"></param>
        /// <param name="rootNode"></param>
        /// <returns></returns>
        public TitleData GetDefault(int defaultType, string shortLink, int rootNode)
        {
            var data = _defaultTitleRepository.LoadTitle(defaultType, shortLink, rootNode);
            return CloneTitleData(data);
        }

        /// <summary>
        /// копируем объект так как потом над ним будут производиться манипуляции
        /// </summary>
        /// <param name="defaultType"></param>
        /// <param name="categoryOID"></param>
        /// <param name="rootNode"></param>
        /// <returns></returns>
        public TitleData GetDefault(int defaultType, Guid categoryOID, int rootNode)
        {
            var data = _defaultTitleRepository.LoadTitle(defaultType, categoryOID, rootNode);
            return CloneTitleData(data);
        }

        public void Normalize(TitleData data, IDictionary<string, string> substitutes)
        {
            foreach (KeyValuePair<string, string> item in substitutes)
            {
                data.Title = NormalizeTarget(data.Title, item);
                data.Keywords = NormalizeTarget(data.Keywords, item);
                data.PageDescription = NormalizeTarget(data.PageDescription, item);

            }

            //throw new System.NotImplementedException();
        }

        private string NormalizeTarget(string target, KeyValuePair<string, string> item)
        {
            if (target == null) return null;
            Regex re = new Regex($"\\%{item.Key}[^\\%]*\\%");
            return re.Replace(target, m =>
            {
                var found = m.Value;

                var value = item.Value;
                foreach (var suffix in _suffixMatch)
                {
                    ISearchEnd suffixKey = suffix.Key;
                    Func<string, string> f = suffix.Value;
                    if (suffixKey.IsEnding(found))
                    {
                        value = f(value);
                        break;
                    }
                }

                return value;
            });
        }

        private static TitleData CloneTitleData(TitleData data)
        {
            return new TitleData
            {
                Title = data.Title,
                Keywords = data.Keywords,
                PageDescription = data.PageDescription
            };
        }
    }
}