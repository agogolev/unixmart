﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using NoName.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Infrastructure.Factories;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class CatalogPageBuilder : ICatalogPageBuilder
    {
        private const int maxValueForList = 6;

        private readonly IArticleRepository _articleRepository;
        private readonly ICatalogFilterFactory _catalogFilterFactory;
        private readonly ICompanyMenuRepository _companyMenuRepository;
        private readonly IDefaultTitleBuilder _defaultTitleBuilder;
        private readonly IGoodsRepository _goodsRepository;
        private readonly ILandingPageRepository _landingPageRepository;
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;

        public CatalogPageBuilder(
            IArticleRepository articleRepository,
            ICatalogFilterFactory catalogFilterFactory,
            ICompanyMenuRepository companyMenuRepository, 
            IDefaultTitleBuilder defaultTitleBuilder,
            IGoodsRepository goodsRepository,
            ILandingPageRepository landingPageRepository,
            IMapper mapper,
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _catalogFilterFactory = catalogFilterFactory;
            _companyMenuRepository = companyMenuRepository;
            _defaultTitleBuilder = defaultTitleBuilder;
            _goodsRepository = goodsRepository;
            _landingPageRepository = landingPageRepository;
            _mapper = mapper;
            _menuRepository = menuRepository;
        }

        public CatalogMenuModel Create(int id, string itemId)
        {
            var catalogMenu = _menuRepository.GetCatalogSubMenu(id, Settings.Default.SHOP_TYPE);
            var model = _mapper.Map<CatalogMenuModel>(catalogMenu);
            model.BreadCrumbs =
                _mapper.Map<CatalogMenuModel[]>(_menuRepository.GetCategoryPath(Settings.Default.CATALOG_ROOT, id,
                    Settings.Default.SHOP_TYPE));
            model.Name = catalogMenu.Name;
            model.H1 = model.H1 = string.IsNullOrWhiteSpace(catalogMenu.H1) ? catalogMenu.Name : catalogMenu.H1;

            var defaultTitle = _defaultTitleBuilder.GetDefault(1, itemId, Settings.Default.CATALOG_ROOT);
            var associate = new Dictionary<string, string>()
            {
                {"CATALOG_NAME", catalogMenu.Name},
                {"CATALOG_H1", catalogMenu.H1},
                {"PHONE", Settings.Default.SHOP_PHONE_TEXT},
                {"WORKING", Settings.Default.SHOP_WORKING_TIME}
            };
            _defaultTitleBuilder.Normalize(defaultTitle, associate);

            model.Title = !string.IsNullOrWhiteSpace(catalogMenu.Title)
                ? catalogMenu.Title : defaultTitle.Title;
 //               : $"Купить {catalogMenu.Name.ToLowerFirstWord()} в интернет-магазине UnixMart.ru в Москве, цены на {catalogMenu.Name}";
            model.Keywords = catalogMenu.Keywords ?? defaultTitle.Keywords;
            model.PageDescription = !string.IsNullOrWhiteSpace(catalogMenu.PageDescription)
                ? catalogMenu.PageDescription : defaultTitle.PageDescription;
//                : $"Купить {catalogMenu.Name.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
            model.SeoArticle = _articleRepository.GetSeoArticle(id, null, null);
            model.Canonical = $"/catalog/{itemId}";
            return model;
        }

        public GoodsListPageModel CreateGoodsList(int id, string itemId, string manufId, int? page, string sort, bool withSameLevel)
        {
            var company = _companyMenuRepository.GetCompany(manufId);
            var numOnPage = Constants.NUM_ON_PAGE;

            var total = 0; 
            var catalog = company.Id == 0
                ? _menuRepository.GetCatalogSubMenu(id, Settings.Default.SHOP_TYPE)
                : _menuRepository.GetManufacturerSubMenu(id, Settings.Default.SHOP_TYPE, company.Id, company.ShortLink);
            var model = _mapper.Map<GoodsListPageModel>(catalog);
            model.ChildMenu = _mapper.Map<CatalogMenuModel[]>(
                _menuRepository.GetCatalogSubMenu(
                    catalog.ParentNodeId,
                    Settings.Default.SHOP_TYPE).ChildMenu);
            model.ChildMenu
                .Where(m => m.NodeId == id)
                .DefaultIfEmpty(new CatalogMenuModel()).Single()
                .OrdValue = 0;
            //if (withSameLevel)
            //{
            model.SameLevelMenu = _mapper.Map<CatalogMenuModel[]>(catalog.ChildMenu);
            //}

            model.BreadCrumbs = _mapper.Map<CatalogMenuModel[]>(
                company.OID == Guid.Empty
                    ? _menuRepository.GetCategoryPath(
                        Settings.Default.CATALOG_ROOT, id,
                        Settings.Default.SHOP_TYPE)
                    : _menuRepository.GetManufCategoryPath(
                        Settings.Default.CATALOG_ROOT, id,
                        Settings.Default.SHOP_TYPE, company.Name));
            var filters = _catalogFilterFactory.Create(
                id,
                catalog.ParentNodeId,
                page,
                numOnPage,
                sort,
                itemId,
                manufId,
                _mapper.Map<ManufacturerModel[]>(_goodsRepository.GetManufacturers(Settings.Default.SHOP_TYPE, id)),
                _goodsRepository.GetFilters(Settings.Default.SHOP_TYPE, id));
            filters.WithSameLevel = true;
            var showEmpty = false;
            if (company.OID != Guid.Empty)
            {
                if (filters.Manufs.Any(m => m.OID == company.OID))
                {
                    filters.Manufs.Single(m => m.OID == company.OID).Selected = true;
                    filters.SelectedCheckBoxId =
                        $"Manufs_{filters.Manufs.IndexOf(filters.Manufs.Single(m => m.OID == company.OID))}__Selected";
                }
                else
                {
                    showEmpty = true;
                }
                filters.CompanyOID = company.OID;
            }
            var manufs = _catalogFilterFactory.CreateManufacturerFilter(filters.Manufs);
            model.Name = catalog.Name;
            catalog.H1 = string.IsNullOrWhiteSpace(catalog.H1) ? catalog.Name : catalog.H1;
            model.H1 = $"{catalog.H1} {company.Name}".Trim();
            //if ((page ?? 1) > 1) model.H1 += $" страница {page}";
            model.CanonicalLink = MVC.Catalog.Category(itemId, manufId, null);
            model.FilterModel = filters;
            model.GoodsItems = !showEmpty
                ? _goodsRepository.GetGoodsByCategory(
                    Settings.Default.SHOP_TYPE,
                    id,
                    ((page ?? 1) - 1) * numOnPage,
                    numOnPage,
                    sort,
                    manufs,
                    null,
                    out total)
                : new List<GoodsItem>();
            model.GoodsCount = total;
            model.Tags = _goodsRepository.GetCategoryTags(id);
            model.PagingModel = new GoodsPagingModel
            {
                Id = id,
                ItemId = itemId,
                ManufId = manufId,
                Page = page ?? 1,
                NumOnPage = numOnPage,
                Fragment = filters.GetUrlFragment(),
                Total = total,
                Sort = sort
            };
            model.SeoArticle = (page ?? 1) == 1 ? _articleRepository.GetSeoArticle(id, company.OID, null) : null;

            var defaultTitle = _defaultTitleBuilder.GetDefault(company.Name == null ? 2 : 5, itemId, Settings.Default.CATALOG_ROOT);
            var associate = new Dictionary<string, string>()
            {
                {"COMPANY_NAME", company.Name},
                {"COMPANY_ALT", company.AltName},
                {"CATALOG_NAME", catalog.Name},
                {"CATALOG_H1", catalog.H1},
                {"PHONE", Settings.Default.SHOP_PHONE_TEXT},
                {"WORKING", Settings.Default.SHOP_WORKING_TIME}
            };
            _defaultTitleBuilder.Normalize(defaultTitle, associate);

            if (company.Name != null)
            {
                string title, keywords, pageDescription;
                _companyMenuRepository.GetCompanyCategoryTitle(id, company.OID, out title, out keywords, out pageDescription);

                if (!string.IsNullOrEmpty(model.SeoArticle?.Title))
                {
                    model.Title = model.SeoArticle.Title;
                }
                else
                {
                    if (string.IsNullOrEmpty(company.AltName)) company.AltName = company.Name;
                    model.Title = title ?? defaultTitle.Title;
                          //$"Купить {catalog.H1.ToLowerFirstWord()} {company.AltName}. Самые выгодные цены на {catalog.H1.ToLowerFirstWord()} {company.Name} в Москве с доставкой - UnixMart.ru";
                }

                if ((page ?? 1) > 1) model.Title += $" страница {page}";

                if (!string.IsNullOrEmpty(model.SeoArticle?.PageDescription))
                {
                    model.PageDescription = model.SeoArticle.PageDescription;
                }
                else
                {
                    model.PageDescription = pageDescription ?? defaultTitle.PageDescription;
                          //$"Купить {catalog.H1.ToLowerFirstWord()} {company.Name} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
                }

                if (!string.IsNullOrEmpty(model.SeoArticle?.Keywords))
                {
                    model.Keywords = model.SeoArticle.Keywords;
                }
                else
                {
                    model.Keywords = keywords ?? defaultTitle.Keywords;
                }
            }
            else
            {
                model.Title = !string.IsNullOrWhiteSpace(catalog.Title)
                    ? catalog.Title : defaultTitle.Title;
                    //: $"Купить {catalog.H1.ToLowerFirstWord()}. Самые выгодные цены на {catalog.H1.ToLowerFirstWord()} в Москве с доставкой - UnixMart.ru";
                if ((page ?? 1) > 1) model.Title += $" страница {page}";
                model.PageDescription = !string.IsNullOrWhiteSpace(catalog.PageDescription)
                    ? catalog.PageDescription : defaultTitle.PageDescription;
                    //: $"Купить {catalog.H1.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
                model.Keywords = catalog.Keywords ?? defaultTitle.Keywords;
            }
            return model;
        }

        public GoodsListPageModel FilterList(CatalogFilterModel filter)
        {
            int total;
            var manufacturerFilter = _catalogFilterFactory.CreateManufacturerFilter(filter.Manufs);
            var catalog = manufacturerFilter.Count != 1 ?
                _menuRepository.GetCatalogSubMenu(filter.CatalogID, Settings.Default.SHOP_TYPE) :
                _menuRepository.GetManufacturerSubMenu(filter.CatalogID, Settings.Default.SHOP_TYPE, filter.Manufs.First(m => m.Selected).Id, filter.Manufs.First(m => m.Selected).ShortLink);
            var model = new GoodsListPageModel
            {
                Name = catalog.Name,
                GoodsItems = _goodsRepository.GetGoodsByCategory(
                    Settings.Default.SHOP_TYPE,
                    filter.CatalogID,
                    filter.NumOnPage * ((filter.Page ?? 1) - 1),
                    filter.NumOnPage,
                    filter.TypeSort,
                    manufacturerFilter,
                    _catalogFilterFactory.CreateCustomFilter(
                        filter.CustomFilters,
                        _goodsRepository.GetFilters(
                            Settings.Default.SHOP_TYPE,
                            filter.CatalogID)),
                    out total),
                Tags = _goodsRepository.GetCategoryTags(filter.CatalogID),
                ChildMenu =
                    _mapper.Map<CatalogMenuModel[]>(
                        _menuRepository.GetCatalogSubMenu(filter.ParentNodeId, Settings.Default.SHOP_TYPE).ChildMenu),
                SameLevelMenu = filter.WithSameLevel ? _mapper.Map<CatalogMenuModel[]>(catalog.ChildMenu) : null,
                FilterModel = filter,
                PagingModel = new GoodsPagingModel
                {
                    Id = filter.CatalogID,
                    ItemId = filter.ItemId,
                    ManufId = filter.ManufId,
                    Page = filter.Page ?? 1,
                    NumOnPage = filter.NumOnPage,
                    Fragment = filter.GetUrlFragment(),
                    Total = total,
                    Sort = filter.TypeSort
                },
                SeoArticle =
                    (filter.Page ?? 1) == 1
                        ? _articleRepository.GetSeoArticle(filter.CatalogID, filter.CompanyOID, null)
                        : null
            };

            model.ChildMenu
                .Where(m => m.NodeId == filter.CatalogID)
                .DefaultIfEmpty(new CatalogMenuModel()).Single()
                .OrdValue = 0;
            return model;
        }

        public GoodsListPageModel CreateLandingPage(int id, string itemId, int? page, string sort)
        {
            var numOnPage = page.HasValue && page.Value == 0 ? -1 : Constants.NUM_ON_PAGE;
            //выясняем, нужно ли фильтровать каталог

            int total;
            var catalog = _landingPageRepository.GetItem(id);
            var model = new GoodsListPageModel
            {
                ChildMenu = _mapper.Map<CatalogMenuModel[]>(catalog.ChildMenu)
            };
            var parentsMenu = _landingPageRepository.GetParentsMenu(id);
            var categoryNode = parentsMenu.LastOrDefault(m => !string.IsNullOrEmpty(m.StringRepresentation))?.NodeId ??
                               0;
            model.BreadCrumbs = _mapper.Map<CatalogMenuModel[]>(parentsMenu);
            var allFilters = _goodsRepository.GetFilters(Settings.Default.SHOP_TYPE, categoryNode);
            var filters = _catalogFilterFactory.Create(
                id,
                categoryNode,
                page,
                numOnPage,
                sort,
                itemId,
                null,
                _mapper.Map<ManufacturerModel[]>(_goodsRepository.GetManufacturers(Settings.Default.SHOP_TYPE, categoryNode)),
                allFilters);
            var manufsInPage = _landingPageRepository.GetPageManufacturers(id);
            var filtersInPage = _landingPageRepository.GetPageFilters(id);
            filters.Manufs.ToList().ForEach(m => m.Selected = manufsInPage.Any(mp => mp == m.OID));
            filters.CustomFilters.SelectMany(c => c.CustomFilters).ToList()
                .ForEach(m => m.Selected = filtersInPage.Any(i => i == m.Id));
            filters.LandingPage = true;
            model.Name = catalog.Name;
            model.H1 = string.IsNullOrWhiteSpace(catalog.H1) ? catalog.Name : catalog.H1;
            //if ((page ?? 1) > 1) model.H1 += $" страница {page}";
            model.CanonicalLink = MVC.Catalog.Category(itemId, null, null);
            model.FilterModel = filters;
            model.GoodsItems = _landingPageRepository.GetGoodsList(
                Settings.Default.SHOP_TYPE,
                id,
                ((page ?? 1) - 1) * numOnPage,
                numOnPage,
                sort,
                null,
                _catalogFilterFactory.CreateCustomFilter(filters.CustomFilters, allFilters),
                out total);
            if (model.ChildMenu.Count > 0 && model.ChildMenu[0].GoodsCount == 0)
            {
                model.ChildMenu[0].GoodsCount = total;
            }
            model.GoodsCount = total;

            model.Tags = _goodsRepository.GetLandingPageTags(id);
            model.PagingModel = new GoodsPagingModel
            {
                Id = id,
                ItemId = itemId,
                ManufId = null,
                Page = page ?? 1,
                NumOnPage = numOnPage,
                Fragment = filters.GetUrlFragment(),
                Total = total,
                Sort = sort
            };

            var defaultTitle = _defaultTitleBuilder.GetDefault(6, null, Settings.Default.CATALOG_ROOT);
            var associate = new Dictionary<string, string>()
            {
                {"CATALOG_NAME", catalog.Name},
                {"CATALOG_H1", catalog.H1},
                {"PHONE", Settings.Default.SHOP_PHONE_TEXT},
                {"WORKING", Settings.Default.SHOP_WORKING_TIME}
            };
            _defaultTitleBuilder.Normalize(defaultTitle, associate);

            model.SeoArticle = (page ?? 1) == 1 ? _landingPageRepository.GetSeoArticle(id, null) : null;
            model.Title = !string.IsNullOrWhiteSpace(catalog.Title)
                ? catalog.Title
                : $"Купить {model.H1.ToLowerFirstWord()} в интернет-магазине UnixMart.ru в Москве, цены на {model.H1.ToLowerFirstWord()}";
            if ((page ?? 1) > 1) model.Title += $" страница {page}";
            model.Keywords = catalog.Keywords;
            model.PageDescription = !string.IsNullOrWhiteSpace(catalog.PageDescription)
                ? catalog.PageDescription
                : $"Купить {model.H1.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
            return model;
        }

        public string GetLandingRedirect(CatalogFilterModel filter)
        {
            var url = _landingPageRepository.GetLandingUrl(filter.CatalogID);
            return $"{url}#{filter.GetUrlFragment()}";
        }

        public SimilarGoodsModel GetSimilarGoods(string categoryName, IList<int> nodeIds)
        {
            var result = new SimilarGoodsModel();
            IList<GoodsItem> sputnicGoods = new List<GoodsItem>();
            for (var i = nodeIds.Count - 1; i >= 0; i--)
            {
                var parentNode = nodeIds[i];
                sputnicGoods = _goodsRepository.GetSimilarGoods(Settings.Default.SHOP_TYPE,
                    Settings.Default.CATALOG_ROOT, parentNode, 0, 0, maxValueForList, maxValueForList);
                if (sputnicGoods.Count > 0) break;
            }
            if (sputnicGoods.Count == 0)
            {
                sputnicGoods = _goodsRepository.GetSimilarGoods(Settings.Default.SHOP_TYPE,
                    Settings.Default.CATALOG_ROOT, Settings.Default.CATALOG_ROOT, 0, 0, 0, maxValueForList);
            }
            result.Header = sputnicGoods.Count > 0
                ? $"Извините, все товары в категории &laquo;{categoryName}&raquo; закончились, но мы можем вам предложить другие популярные товары!"
                : $"Извините, все товары в категории &laquo;{categoryName}&raquo; закончились!";
            result.Items = sputnicGoods;
            return result;
        }
    }
}