﻿namespace NoName.Web.Builders.Contracts
{
    public interface ISearchEnd
    {
        bool IsEnding(string item);
    }
}