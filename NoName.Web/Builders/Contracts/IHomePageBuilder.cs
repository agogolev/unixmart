using System.Collections;
using System.Collections.Generic;
using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IHomePageBuilder
    {
        HomePageModel Create();
        IList<PromoBlock> AdvertList();
        IList<PromoBlock> AdvertWideList();
    }
}