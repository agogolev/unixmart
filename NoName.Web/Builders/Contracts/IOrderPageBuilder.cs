﻿using System;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IOrderPageBuilder
    {
        OrderViewModel Create();

        OrderViewModel FillModel(OrderViewModel model);

        Guid Save(OrderViewModel model);
        OrderViewModel Finish(Guid id);
    }
}