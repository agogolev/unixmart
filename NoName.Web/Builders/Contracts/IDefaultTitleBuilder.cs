﻿using System;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Web.Builders.Contracts
{
    public interface IDefaultTitleBuilder
    {
        TitleData GetDefault(int defaultType, string shortLink, int rootNode);
        TitleData GetDefault(int defaultType, Guid categoryOID, int rootNode);
        void Normalize(TitleData data, IDictionary<string, string> substitutes);
    }
}