﻿using System;

namespace NoName.Web.Builders.Contracts
{
    public interface IExportOrdersBuilder
    {
        void BuildExport(Guid key, string dirName);
    }
}