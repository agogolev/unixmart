﻿using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IManufacturerPageBuilder
    {
        CatalogMenuModel Create(string manufRepresentation, string categoryRepresentation);
    }
}