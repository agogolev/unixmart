﻿using System;
using System.Linq;
using Ecommerce.Repository.Referer;
using NoName.Repository;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class OrderPageBuilder : IOrderPageBuilder
    {
        private readonly IBasketRepository _basketRepository;
        private readonly ICookieRepository _cookieRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IRefererLabelCalc _refererLabelCalc;
        private readonly ISessionRepository _sessionRepository;

        public OrderPageBuilder(
            IBasketRepository basketRepository,
            ICookieRepository cookieRepository, 
            IOrderRepository orderRepository, 
            IRefererLabelCalc refererLabelCalc, 
            ISessionRepository sessionRepository)
        {
            _basketRepository = basketRepository;
            _cookieRepository = cookieRepository;
            _orderRepository = orderRepository;
            _refererLabelCalc = refererLabelCalc;
            _sessionRepository = sessionRepository;
        }

        public OrderViewModel Create()
        {
            return FillModel(new OrderViewModel());
        }

        public OrderViewModel FillModel(OrderViewModel model)
        {
            Guid sessionOID = _cookieRepository.SessionId;

            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            model.NeedCityInOrder = basket.NeedCityInOrder;
            model.ShowAddress = DeliveryConstants.SelfDeliveryIds.All(id => basket.DeliveryType != id);
            model.BreadCrumbs = new[]
            {
                new InfoMenuModel
                {
                    Text = "Главная",
                    Action = MVC.Home.Index()
                },
                new InfoMenuModel
                {
                    Text = "Корзина",
                    Action = MVC.Basket.Index()
                },
                new InfoMenuModel
                {
                    Text = "Оформление заказа",
                    Action = null
                }
            };
            return model;
        }

        public Guid Save(OrderViewModel model)
        {
            Guid sessionOID = _cookieRepository.SessionId;

            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);

            basket.Name = model.Name;
            basket.Email = model.Email;
            basket.Phone = model.Phone;
            if (basket.NeedCityInOrder)
            {
                basket.City = model.City;
            }
            basket.StreetName = model.StreetName;
            basket.House = model.House;
            basket.Flat = model.Flat;
            basket.Comments = model.Comments;
            basket.Referer = _refererLabelCalc.ReverseCalculate(_sessionRepository.RefererLabel);
            if (basket.PayType == _orderRepository.CardPayment)
            {
                model.IsOnlinePayment = true;
                return _orderRepository.SavePreOrder(basket, Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT, Constants.DeliveryMoscow);
            }
            return _orderRepository.SaveOrder(basket, Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT, Constants.DeliveryMoscow);
        }

        public OrderViewModel Finish(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}