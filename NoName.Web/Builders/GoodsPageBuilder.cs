﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class GoodsPageBuilder : IGoodsPageBuilder
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IDefaultTitleBuilder _defaultTitleBuilder;
        private readonly IDeliveryRepository _deliveryRepository;
        private readonly IGoodsRepository _goodsRepository;
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;

        public GoodsPageBuilder(
            IArticleRepository articleRepository, 
            IDefaultTitleBuilder defaultTitleBuilder,
            IDeliveryRepository deliveryRepository,
            IGoodsRepository goodsRepository,
            IMapper mapper,
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _defaultTitleBuilder = defaultTitleBuilder;
            _deliveryRepository = deliveryRepository;
            _goodsRepository = goodsRepository;
            _mapper = mapper;
            _menuRepository = menuRepository;
        }

        public GoodsItemPageModel CreateItem(int id)
        {
            GoodsItem goodsItem = _goodsRepository.GetGoodsItem(id, Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT);

            var defaultTitle = _defaultTitleBuilder.GetDefault(7, goodsItem.CategoryOID ?? Guid.Empty, Settings.Default.CATALOG_ROOT);
            var associate = new Dictionary<string, string>()
            {
                {"GOODS_FULLNAME", goodsItem.FullName},
                {"GOODS_MODEL", goodsItem.Model},
                {"GOODS_BRAND_NAME", goodsItem.ManufacturerName},
                {"GOODS_BRAND_ALT_NAME", goodsItem.ManufacturerAltName},
                {"PHONE", Settings.Default.SHOP_PHONE_TEXT},
                {"WORKING", Settings.Default.SHOP_WORKING_TIME}
            };
            _defaultTitleBuilder.Normalize(defaultTitle, associate);
            goodsItem.Title = !string.IsNullOrEmpty(goodsItem.Title) ? goodsItem.Title : defaultTitle.Title;
            goodsItem.Keywords = !string.IsNullOrEmpty(goodsItem.Keywords) ? goodsItem.Keywords : defaultTitle.Keywords;
            goodsItem.PageDescription = !string.IsNullOrEmpty(goodsItem.PageDescription) ? goodsItem.PageDescription : defaultTitle.PageDescription;

            if (string.IsNullOrEmpty(goodsItem.PseudoID))
            {
                throw new HttpException(404, "Goods not found");
            }
            var breadcrumbs = _mapper.Map<List<CatalogMenuModel>>(_menuRepository.GetCategoryPath(Settings.Default.CATALOG_ROOT, goodsItem.CategoryId, Settings.Default.SHOP_TYPE));
            DeliveryInfo delivery = goodsItem.Delivery;
            if (goodsItem.InStock)
            {
                delivery.Items = _deliveryRepository.GetDeliveries(Settings.Default.SHOP_TYPE, delivery, null, delivery.InstantDelivery);
            }

            breadcrumbs.Add(new CatalogMenuModel
            {
                Name = goodsItem.FullName
            });

            var model = new GoodsItemPageModel
            {
                PageClass = "home category-01",
                GoodsItem = goodsItem,
                BreadCrumbs = breadcrumbs,
                DeliveryInfo = delivery,
                ShopPhone = Settings.Default.SHOP_PHONE_TEXT,
                InfoList = _articleRepository.GetLinkedArticles(Settings.Default.GoodsPageInfoList),
                Accessory = _goodsRepository.GetAccessoryGoods(id, Settings.Default.SHOP_TYPE),
                SameChars = _goodsRepository.GetSputnikGoods(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT, goodsItem.CategoryId, id, goodsItem.Price, 4),
                OnlinePayment = Settings.Default.OnlinePayment
            };

            return model;
        }

        public IList<GoodsItem> GetSputnikGoods(int selectedNode, int objectId, int amount)
        {
            var allItems = _goodsRepository.GetAllGoodsByCategory(Settings.Default.SHOP_TYPE, selectedNode);
            var currentGoods = (from item in allItems
                                where item.ObjectID == objectId
                                select item).FirstOrDefault();
            if (currentGoods == null) return null;
            var topItems = allItems.Where(item => item.InStock && item.ObjectID != currentGoods.ObjectID && item.Price >= currentGoods.Price && item.InPresent).OrderBy(item => item.Price).Take(amount).ToList();
            var bottomItems = allItems.Where(item => item.InStock && item.Price < currentGoods.Price && item.InPresent).OrderByDescending(item => item.Price).Take(amount).ToList();
            var items = bottomItems.Union(topItems).OrderBy(item => item.Price).ToList();
            return items;
        }
    }
}