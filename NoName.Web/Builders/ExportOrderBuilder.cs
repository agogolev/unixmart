﻿// Decompiled with JetBrains decompiler
// Type: NoName.Web.Builders.ExportOrdersBuilder
// Assembly: NoName.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E8CD2E18-17E2-4A6F-BE2C-EA00F6225E2F
// Assembly location: C:\Users\Alec N. Gogolev\Desktop\Unixmart\bin\NoName.Web.dll

using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace NoName.Web.Builders
{
    internal class ExportOrdersBuilder : IExportOrdersBuilder
    {
        private readonly IOrderRepository _orderRepository;

        public ExportOrdersBuilder(IOrderRepository orderRepository)
        {
            this._orderRepository = orderRepository;
        }

        public void BuildExport(Guid key, string dirName)
        {
            if (key != Settings.Default.ExportOrderKey)
                return;
            IList<Order> items = this._orderRepository.ExportOrders();
            this.SaveFile(dirName, "exportOrders.xml", items);
        }

        private void SaveFile(string dirName, string fileName, IList<Order> items)
        {
            XDocument xdocument = new XDocument(new XElement( "Документы",  items.Select((item => new XElement( "Документ", 
                new XElement( "Номер",  item.OrderNum),
                new XElement( "Дата",  item.DateCreate.ToString("dd.MM.yyyy")),
                new XElement( "Сумма",  item.TotalPrice.ToString("#.00")),
                new XElement( "ФИО",  item.Person),
                new XElement( "Адрес",  item.FullAddress),
                new XElement( "СпособОплаты",  item.PayName),
                new XElement( "СпособДоставки",  item.DeliveryName),
                new XElement( "Время",  item.DateCreate.ToString("HH:mm:ss")),
                new XElement( "Комментарий",  item.Comments),
                new XElement( "Товары",  item.Items.Select((good => Enumerable.Repeat(new XElement( "Товар", 
                    new XElement( "Ид",  good.PseudoID),
                    new XElement( "Артикул",  good.PostavshikID),
                    new XElement( "Наименование",  good.FullName),
                    new XElement( "ЦенаЗаЕдиницу",  good.Price.ToString("#.00")),
                    new XElement( "Количество",  1),
                    new XElement( "Сумма",  good.Price.ToString("#.00")),
                    new XElement( "СуммаДоставки",  (item.DeliveryPrice / item.Items.Sum(g => g.Amount)).ToString("#.00"))
                ), good.Amount))).ToList()),
                new XElement( "ДисконтнаяКарта",  item.Coupon?.PromoCode ?? ""),
                new XElement( "КодАвторизации",  item.ApprovalCode),
                new XElement( "СуммаЭквайринг",  ""),
                new XElement( "Город",  (item.City ?? "")),
                new XElement( "Улица",  (item.Street ?? "")),
                new XElement( "Дом",  (item.House ?? "")),
                new XElement( "Квартира",  (item.Flat ?? "")),
                new XElement( "Корпус",  ""),
                new XElement( "Подъезд",  ""),
                new XElement( "Предоплата",  "нет"),
                new XElement( "Реклама",  item.RefererName),
                new XElement( "Телефон1",  item.Phone),
                new XElement( "Телефон2",  ""),
                new XElement( "Телефон3",  ""),
                new XElement( "ЭлПочта",  item.EMail)
            ))).ToList()));
            using (var xmlTextWriter = new XmlTextWriter(string.Format("{0}\\{1}", dirName, fileName), Encoding.UTF8))
            {
                xmlTextWriter.Formatting = Formatting.Indented;
                xmlTextWriter.IndentChar = '\t';
                xmlTextWriter.Indentation = 2;
                xdocument.Save(xmlTextWriter);
            }
        }
    }
}
