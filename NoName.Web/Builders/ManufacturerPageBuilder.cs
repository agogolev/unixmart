﻿using System;
using System.Collections.Generic;
using System.Web;
using AutoMapper;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class ManufacturerPageBuilder : IManufacturerPageBuilder
    {

        private readonly IArticleRepository _articleRepository;
        private readonly ICompanyMenuRepository _companyMenuRepository;
        private readonly IDefaultTitleBuilder _defaultTitleBuilder;
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;

        public ManufacturerPageBuilder(
            IArticleRepository articleRepository,
            ICompanyMenuRepository companyMenuRepository,
            IDefaultTitleBuilder defaultTitleBuilder,
            IMapper mapper,
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _companyMenuRepository = companyMenuRepository;
            _defaultTitleBuilder = defaultTitleBuilder;
            _mapper = mapper;
            _menuRepository = menuRepository;
        }
        public CatalogMenuModel Create(string manufRepresentation, string categoryRepresentation)
        {
            var company = _companyMenuRepository.GetCompany(manufRepresentation);
            if (company.OID == Guid.Empty) throw new HttpException(404, "Page not found");
            var catNode = _menuRepository.FindNodeByObject(Settings.Default.CATALOG_ROOT, categoryRepresentation);
            var manufacturerMenu = _menuRepository.GetManufacturerSubMenu(catNode ?? Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE, company.Id, manufRepresentation);
            var model = _mapper.Map<CatalogMenuModel>(manufacturerMenu);
            model.BreadCrumbs = _mapper.Map<CatalogMenuModel[]>(_menuRepository.GetManufacturerPath(
                Settings.Default.CATALOG_ROOT,
                catNode ?? Settings.Default.CATALOG_ROOT,
                Settings.Default.SHOP_TYPE,
                company.Id,
                manufRepresentation, company.Name));
            foreach (var item in model.BreadCrumbs) item.UseBrandUrl = true;
            var defaultTitle = _defaultTitleBuilder.GetDefault(catNode.HasValue ? 4 : 3, categoryRepresentation, Settings.Default.CATALOG_ROOT);
            var associate = new Dictionary<string, string>()
            {
                {"COMPANY_NAME", company.Name},
                {"COMPANY_ALT", company.AltName},
                {"CATALOG_NAME", model.Name},
                {"CATALOG_H1", model.H1},
                {"PHONE", Settings.Default.SHOP_PHONE_TEXT},
                {"WORKING", Settings.Default.SHOP_WORKING_TIME}
            };
            _defaultTitleBuilder.Normalize(defaultTitle, associate);

            model.SeoArticle = _articleRepository.GetSeoArticle(catNode ?? 0, company.OID, null);
            model.H1 = catNode.HasValue ? $"{model.H1} {company.Name}" : company.Name;
            if (string.IsNullOrEmpty(categoryRepresentation))
            {
                model.Title = !string.IsNullOrEmpty(company.Title) ? company.Title : defaultTitle.Title;
//                    $"Каталог бытовой техники и электроники {company.Name} в интернет-магазине UnixMart.ru в Москве";
                model.PageDescription = !string.IsNullOrEmpty(company.PageDescription) ? company.PageDescription : defaultTitle.PageDescription;
//                    $"Каталог продукции {company.Name} интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
                model.Keywords = !string.IsNullOrEmpty(company.Keywords) ? company.Keywords : defaultTitle.Keywords;
            }
            else
            {
                string title, keywords, pageDescription;
                _companyMenuRepository.GetCompanyCategoryTitle(catNode ?? Settings.Default.CATALOG_ROOT, company.OID, out title, out keywords, out pageDescription);
                model.Title = title ?? defaultTitle.Title;
//                              $"Купить {model.Name.ToLowerFirstWord()} {company.AltName}. Самые выгодные цены на {model.Name.ToLowerFirstWord()} {company.Name} в Москве с доставкой - UnixMart.ru";
                model.PageDescription = pageDescription ?? defaultTitle.PageDescription;
//                    $"Купить {model.Name.ToLowerFirstWord()} {company.Name} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
                model.Keywords = keywords ?? defaultTitle.Keywords;
            }

            return model;
        }
    }
}