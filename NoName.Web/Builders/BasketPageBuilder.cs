﻿using System;
using System.Linq;
using AutoMapper;
using Ecommerce.Repository.Referer;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class BasketPageBuilder : IBasketPageBuilder
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IBasketRepository _basketRepository;
        private readonly ICookieRepository _cookieRepository;
        private readonly ICouponRepository _couponRepository;
        private readonly IDeliveryRepository _deliveryRepository;
        private readonly IGoodsRepository _goodsRepository;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;
        private readonly IRefererLabelCalc _refererLabelCalc;
        private readonly ISessionRepository _sessionRepository;

        public BasketPageBuilder(
            IBaseRepository baseRepository,
            IBasketRepository basketRepository,
            ICookieRepository cookieRepository,
            ICouponRepository couponRepository,
            IDeliveryRepository deliveryRepository,
            IGoodsRepository goodsRepository,
            IMapper mapper,
            IOrderRepository orderRepository,
            IRefererLabelCalc refererLabelCalc,
            ISessionRepository sessionRepository)
        {
            _baseRepository = baseRepository;
            _basketRepository = basketRepository;
            _cookieRepository = cookieRepository;
            _couponRepository = couponRepository;
            _deliveryRepository = deliveryRepository;
            _goodsRepository = goodsRepository;
            _mapper = mapper;
            _orderRepository = orderRepository;
            _refererLabelCalc = refererLabelCalc;
            _sessionRepository = sessionRepository;
        }

        public BasketViewModel Create()
        {
            Basket basket = _sessionRepository.Basket;
            var model = _mapper.Map<BasketViewModel>(basket);
            FillModel(model);
            return model;
        }

        public BasketViewModel Save(BasketViewModel model)
        {
            //узнаем ID сессии
            Guid sessionOID = _cookieRepository.SessionId;

            _basketRepository.SaveDeliveryType(model.OID, model.DeliveryType ?? Constants.DeliveryMoscow, model.CalcDistance);
            _basketRepository.SavePromoCode(model.OID, model.PromoCode);

            if (model.PayType.HasValue)
            {
                _basketRepository.SavePayType(model.OID, model.PayType.Value);
            }

            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);

            //сохраняем кол-во товаров
            foreach (BasketContent basketContent in model.Items)
            {
                var goodsOID = basket.Items.Where(b => b.OrdValue == basketContent.OrdValue).Select(b => b.OID).SingleOrDefault();
                _basketRepository.ModifyGoodsInBasket(
                    basket,
                    goodsOID,
                    basketContent.Amount, 
                    false,
                    basketContent.IsSale);
            }

            basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            _sessionRepository.Basket = basket;
            var newModel = _mapper.Map<BasketViewModel>(basket);
            newModel.OnlinePayment = Settings.Default.OnlinePayment;
            newModel.Deliveries = _deliveryRepository.GetDeliveries(
                Settings.Default.SHOP_TYPE,
                new DeliveryInfo { Price = basket.VariableDeliveryPart },
                basket.Items,
                _sessionRepository.InstantDeliveryPossible); 
            newModel.PayTypes = _orderRepository.GetPayTypeLookup(Settings.Default.SHOP_TYPE).Select(k => new PayInfoItem {
                Name = k.Name,
                Description = k.Description,
                OnlinePay = k.OnlinePay,
                PayType = k.PayType
            }).ToList();

            if (newModel.Items.All(item => !item.OnlinePay))
            {
                newModel.PayTypes = newModel.PayTypes.Where(item => !item.OnlinePay).ToList();
            }

            if (model.PayType.HasValue)
            {
                newModel.PayTypes.Single(k => k.PayType == model.PayType).Selected = true;
            }

            newModel.Accessories = _basketRepository.GetBasketAccessory(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                _cookieRepository.SessionId);
            return newModel;
        }

        public Guid QuickSave(BuyOneClickModel model)
        {
            Guid sessionOID = _cookieRepository.SessionId;

            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);

            //сохраняем кол-во товаров
            _basketRepository.ModifyGoodsInBasket(
                    basket,
                    model.GoodsOID,
                    1,
                    true,
                    false);
            _basketRepository.SaveDeliveryType(basket.OID, Constants.DeliveryMoscow, 0);

            _basketRepository.SavePayType(basket.OID, Constants.DefaultPayment);


            basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            basket.Name = model.Name;
            basket.Comments = model.Comments;
            basket.Phone = model.Phone;
            basket.Referer = _refererLabelCalc.ReverseCalculate(_sessionRepository.RefererLabel);
            return _orderRepository.SaveOrder(basket, Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT, Constants.DeliveryMoscow);
        }

        public void FillModel(BasketViewModel model)
        {
            Basket basket = _sessionRepository.Basket;
            model.Items = basket.Items;
            model.Deliveries = _deliveryRepository.GetDeliveries(
                Settings.Default.SHOP_TYPE,
                new DeliveryInfo { Price = basket.VariableDeliveryPart },
                basket.Items,
                _sessionRepository.InstantDeliveryPossible);
            _sessionRepository.InstantDeliveryPossible = basket.InstantDeliveryPossible;
            model.PayTypes = _orderRepository.GetPayTypeLookup(Settings.Default.SHOP_TYPE).Select(k => k).ToList();
            if (model.Items.All(item => !item.OnlinePay))
            {
                model.PayTypes = model.PayTypes.Where(item => !item.OnlinePay).ToList();
            }

            if (model.PayType.HasValue && model.PayTypes.Any(k => k.PayType == model.PayType))
            {
                model.PayTypes.Single(k => k.PayType == model.PayType).Selected = true;
            }
            else
            {
                model.PayTypes.First().Selected = true;
            }

            model.Accessories = _basketRepository.GetBasketAccessory(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                _cookieRepository.SessionId);
            model.OnlinePayment = Settings.Default.OnlinePayment;
        }

        public void Add(int goodsId)
        {
            Guid sessionOID = _cookieRepository.SessionId;
            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            var selectedGoods = _goodsRepository.GetGoodsItem(
                goodsId,
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT);
            _basketRepository.ModifyGoodsInBasket(
                basket, 
                selectedGoods.OID, 
                1, 
                true,
                false);
            _sessionRepository.Basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT, 
                sessionOID);
            DetermineLastIndex(goodsId, false);
        }

        public void AddSale(int goodsId)
        {
            Guid sessionOID = _cookieRepository.SessionId;
            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            var selectedGoods = _goodsRepository.GetGoodsItem(
                goodsId,
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT);
            _basketRepository.ModifyGoodsInBasket(
                basket,
                selectedGoods.OID,
                1,
                true,
                true);
            _sessionRepository.Basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            
            DetermineLastIndex(goodsId, true);
        }

        private void DetermineLastIndex(int goodsId, bool isSale)
        {
            for (int i = 0; i < _sessionRepository.Basket.Items.Count; i++)
            {
                var item = _sessionRepository.Basket.Items[i];
                if (item.ObjectId == goodsId && item.IsSale == isSale)
                {
                    _sessionRepository.Basket.LastAddedItem = i;
                    break;
                }
            }
        }

        public void Add(Guid goodsOID)
        {
            var goodsId = _baseRepository.GetObjectID(goodsOID);
            if (goodsId.HasValue)
            {
                Add(goodsId.Value);
            }
        }

        public void Delete(int ordValue, bool isSale = false)
        {
            Set(ordValue, 0, isSale);
        }

        public void Set(int ordValue, int amount, bool isSale = false)
        {
            Guid sessionOID = _cookieRepository.SessionId;
            var basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            var goodsOID = basket.Items.Where(b => b.OrdValue == ordValue).Select(b => b.OID).SingleOrDefault();
            var goodsId = basket.Items.Where(b => b.OrdValue == ordValue).Select(b => b.ObjectId).SingleOrDefault();
            _basketRepository.ModifyGoodsInBasket(
                basket,
                goodsOID,
                amount,
                false,
                isSale);
            _sessionRepository.Basket = _basketRepository.GetBasketBySession(
                Settings.Default.SHOP_TYPE,
                Settings.Default.CATALOG_ROOT,
                sessionOID);
            DetermineLastIndex(goodsId, isSale);
        }
    }
}