﻿using System.Linq;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class MarketingActionPageBuilder : IMarketingActionPageBuilder
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IMarketingActionRepository _marketingActionRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly ISessionRepository _sessionRepository;

        public MarketingActionPageBuilder(
            IArticleRepository articleRepository, 
            IMarketingActionRepository marketingActionRepository, 
            IMenuRepository menuRepository,
            ISessionRepository sessionRepository)
        {
            _articleRepository = articleRepository;
            _marketingActionRepository = marketingActionRepository;
            _menuRepository = menuRepository;
            _sessionRepository = sessionRepository;
        }

        public MarketingActionListModel CreateList(int? page)
        {
            int numOnPage = page == 0 ? -1 : Constants.NUM_ON_PAGE;
            var article = _articleRepository.GetNodeArticle(Settings.Default.MarketingActionArticle, null);
            var sessionId = _sessionRepository.AdvertSessionId;
			var region = _sessionRepository.RegionId;
            var items = _marketingActionRepository.GetActions(sessionId, region);
            return new MarketingActionListModel
            {
                Items = new PagingModel<MarketingAction>
                {
                    Page = page ?? 1,
                    NumOnPage = numOnPage,
                    Total = items.Count,
                    Items = numOnPage == -1 ? items : items.Skip(numOnPage * ((page ?? 1) - 1)).Take(numOnPage).ToList()
                },
                Menu = _menuRepository.GetInfoMenu(Settings.Default.INFO_ROOT),
                Article = article,
            };
        }

        public MarketingAction CreateItem(int id)
        {
            return _marketingActionRepository.GetActionById(id, Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE);
        }
    }
}