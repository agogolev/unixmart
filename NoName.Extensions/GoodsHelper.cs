﻿using Ecommerce.Extensions;

namespace NoName.Extensions
{
	public class GoodsHelper
	{
		public static string FullName(string productType, string brandName, string model)
		{
            return
                $"{(productType ?? "").Trim()} {(brandName ?? "").Trim()} {(model ?? "").Trim()}".TrimInner().Trim();
		}

        public static string GoodsUrl(string siteUrl, string productType, string manufacturerName, string model, int objectID)
        {
			return $"{siteUrl}{GoodsPath(productType, manufacturerName, model, objectID)}";
		}

        public static string GoodsPath(string productType, string manufacturerName, string model, int objectID)
        {
            return $"/goods/{ShortNameEncoded(productType, manufacturerName, model)}-{objectID}";
        }

        public static string ShortNameEncoded(string productType, string manufacturerName, string model)
        {
            return
                $"{(productType ?? string.Empty).Trim().ToLower()} {(manufacturerName ?? string.Empty).Trim().ToLower()} {(model ?? string.Empty).Trim().ToLower()}"
                    .TrimInner().Trim().TranslitEncode(true);
        }
	}
}
