﻿using System.Linq;
using System.Text;
using System.Web.Mvc;
using Ecommerce.Domain;

namespace NoName.Extensions
{
	public static class HtmlHelper
	{
		//private static Regex reAnchor = new Regex(@"\<a[^\>]+href=['""]?([^'""\s]+)['""]?[^\>]*\>", RegexOptions.IgnoreCase | RegexOptions.Multiline);
		public static MvcHtmlString Html(this AdvertItem item, bool widthMain)
		{
			var sb = new StringBuilder();
			switch (item.AdvertType)
			{
				case 1:
					sb.AppendFormat("<a href=\"/banner/click?id={0}&key={1}\"", item.ObjectId, item.ShowKey);
					sb.Append(item.ParentType == 1 ? "" : " target=\"_blank\"");
					sb.AppendFormat("><img src=\"/bin.aspx?ID={0}\" {1} {2} border=\"0\" /></a>", item.BinData,
						widthMain ? string.Format("width=\"{0}\"", item.Width) : "", !widthMain ? string.Format("height=\"{0}\"", item.Height) : "");
					return MvcHtmlString.Create(sb.ToString());
				case 2:
					sb.Append(item.Html);
					return MvcHtmlString.Create(sb.ToString());
				default:
					return MvcHtmlString.Empty;
			}
		}
		public static MvcHtmlString Html(this AdvertPlace item)
		{
			var sb = new StringBuilder();
			if (item.AdvertPlaceType == 2)
			{
				sb.AppendFormat(@"
	<style type=""text/css""> 
		div.banner-content-slider
		{{
			width: {0}px;    
			height: {1}px;
		}}
	</style>
	<script type=""text/javascript"">
		$(function () {{
		  $(""#banner"").coinslider({{ width: {2}, height: {3}, spw: 1, sph: 1, navigation: true, delay: 10000 }});
		}});
	</script>
  ", item.Width, item.Height, item.ItemWidth, item.ItemHeight);
			}
			switch (item.AdvertPlaceType)
			{
				case 1:
					sb.Append(FormatTable(item));
					break;
				case 2:
					sb.Append(FormatList(item));
					break;
			}
			return MvcHtmlString.Create(sb.ToString());
		}
		private static string FormatTable(AdvertPlace item)
		{
			var sb = new StringBuilder();
			sb.AppendFormat(@"
<div class=""banner-content"">
	<table cellpadding=""0"" cellspacing=""0"" border=""0"" width=""{0}"">", item.Width);
			if (item.IsVerical)
			{
				int delta = item.ItemHeight;
				for (int i = 0; i < item.Items.Count; i++)
				{
					sb.Append(@"
		<tr>
			<td>");
					sb.Append(item.Items[i].Html(item.IsVerical));
					sb.Append(@"
			</td>
		</tr>");
					if (i != item.Items.Count - 1)
					{
						sb.AppendFormat(@"
		<tr>
			<td height=""{0}""></td>
		</tr>", delta);
					}
				}
			}
			else
			{
				int delta = item.ItemWidth;
				sb.Append(@"
		<tr>");
				for (int i = 0; i < item.Items.Count; i++)
				{
					sb.Append(@"
			<td>");
					sb.Append(item.Items[i].Html(item.IsVerical));
					sb.Append(@"
			</td>");
					if (i != item.Items.Count - 1)
					{
						sb.AppendFormat(@"
			<td width=""{0}""></td>", delta);
					}
				}
				sb.Append(@"
		</tr>");
			}
			sb.Append(@"
	</table>
</div>");
			return sb.ToString();
		}
		private static string FormatList(AdvertPlace item)
		{
			var sb = new StringBuilder();
			try
			{
				sb.Append(@"
<div class=""slider-wrapper"">
	<div class=""slider-item"">");
				foreach (var banner in item.Items.Where(promo => promo.AdvertType == 1))
				{
					sb.Append(banner.Html(item.IsVerical));
				}
			}
			finally
			{
				sb.Append(@"
	</div>
</div>");
			}
			return sb.ToString();
		}
	}
}
