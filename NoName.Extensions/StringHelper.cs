﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NoName.Extensions
{
    public static class StringHelper
    {
        private static readonly Regex firstWord = new Regex(@"^\b.+?\b");
        public static string ToLowerFirstWord(this string name)
        {
            name = name ?? "";
            var m = firstWord.Match(name);
            if (!m.Success) return name.ToLower();
            var word = m.Value;
            return word.ToLower() + name.Substring(m.Index + m.Length);
        }
    }
}
