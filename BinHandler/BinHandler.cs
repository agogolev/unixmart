﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Net;
using System.Web;
using BinHandler.Properties;
using Invento.Web;

namespace BinHandler
{
    public class BinHandler : ImageHandler
    {
        public BinHandler()
        {
            EnableClientCache = true;
            ClientCacheExpiration = TimeSpan.FromMinutes(10);
            EnableServerCache = true;
        }

        public override bool IsReusable
        {
            get { return true; }
        }

        public override ImageInfo GenerateImage(NameValueCollection parameters)
        {
            try
            {
                Guid oid;
                return Guid.TryParse(parameters["ID"], out oid)
                           ? CreateImage(oid, Settings.Default.Connection)
                           : CreateImage(Settings.Default.BlankImageId, Settings.Default.Connection);
            }
            catch (Exception e)
            {
                HttpContext.Current.Response.AppendToLog(e.Message);
                return new ImageInfo(HttpStatusCode.InternalServerError);
            }
        }

        private ImageInfo CreateImage(Guid OID, string connection)
        {
            HttpResponse response = HttpContext.Current.Response;
            bool contentDisposition = false;
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings[connection]))
            {
                conn.Open();
                using (var cmd = new SqlCommand("select data, mimetype, fileName from t_BinaryData where OID = @ID", conn))
                {
                    cmd.Parameters.AddWithValue("id", OID);
                    var rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        var mimeType = ((string)rdr[1]).ToLower();
                        var fileName = (string)rdr[2];
                        if (mimeType.IndexOf("image", StringComparison.Ordinal) != -1)
                        {
                            response.ContentType = mimeType;
                            if (mimeType.IndexOf("png", StringComparison.Ordinal) != -1)
                            {
                                ContentType = ImageFormat.Png;
                            }
                            else if (mimeType.IndexOf("gif", StringComparison.Ordinal) != -1)
                            {
                                ContentType = ImageFormat.Gif;
                            }
                            else if (mimeType.IndexOf("jpeg", StringComparison.Ordinal) != -1)
                            {
                                ContentType = ImageFormat.Jpeg;
                            }
                        }
                        else if (mimeType.IndexOf("msword", StringComparison.Ordinal) != -1 ||
                                 mimeType.IndexOf("ms-excel", StringComparison.Ordinal) != -1)
                        {
                            response.ContentType = mimeType;
                            contentDisposition = true;
                        }
                        else if (mimeType.IndexOf("pdf", StringComparison.Ordinal) != -1)
                        {
                            response.ContentType = mimeType;
                            contentDisposition = true;
                        }
                        else
                        {
                            response.AppendHeader("Content-Type", "application/download");
                            contentDisposition = true;
                        }

                        if (contentDisposition)
                        {
                            response.AppendHeader("content-disposition",
                                                  "attachment;filename=" + HttpUtility.UrlEncode(fileName));
                            response.OutputStream.Write(rdr.GetSqlBinary(0).Value, 0, rdr.GetSqlBinary(0).Length);
                            response.OutputStream.Close();
                        }
                        else
                        {
                            return new ImageInfo((Byte[])rdr[0]);
                        }
                    }
                    return new ImageInfo(HttpStatusCode.Accepted);
                }
            }
        }
    }
}