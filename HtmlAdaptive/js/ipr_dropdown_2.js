(function ($) {

    jQuery.prototype.pr_dropdown2 = function (callback) {

        var _self = this;
        var _callback = callback;

        _self.targetID = _self.attr("id");
        _self.caller = jQuery("[data-dropdown2=" + _self.targetID + "]").toArray();

        _self.caller.forEach(function (item) {
            jQuery(item).on("click", function () {
                _self.toggleState($(this));
                return false;
            });

        })

        _self.on("click", "[data-action='close']", function () {
            _self.close();
            return false;
        })

        jQuery("body").on("pr_dropdown_close_all", function (event_handler, param) {
            if (!param || param.caller != _self.targetID) {
                _self.close();
            }
        })


        /*
        
        event_handler.originalEvent.path: true
        event_handler.originalEvent.target: false
        */
        jQuery("body").on("click", function (event_handler) {
            if (_self.hasClass("opened") && !$(event_handler.originalEvent.target).parents("[id='" + _self.targetID + "']").length) {
                _self.close();
            }
        })

        _self.toggleState = function (caller) {
            if (_self.hasClass("opened")) {
                _self.close(caller);
            } else {
                // close all the rest dropdowns
                jQuery("body").trigger("pr_dropdown_close_all", [{ "caller": _self.targetID }]);
                _self.open(caller);
            }
        }

        _self.close = function (caller) {
            _self.removeClass("opened");
            if (caller) {
                caller.removeClass("opened");
            } else {
                _self.caller.forEach(function (item) {
                    $(item).removeClass("opened");
                })
            }
            //_self.target.removeClass("opened");
            jQuery("body").removeClass(_self.targetID + "-opened");
        }

        _self.open = function (caller) {
            if (_callback != null) {
                _callback(caller);
            }
            _self.addClass("opened");
            if (caller) {
                caller.addClass("opened");
            } else {
                _self.caller.forEach(function (item) {
                    $(item).addClass("opened");
                })
            }
            jQuery("body").addClass(_self.targetID + "-opened");
        }


        _self.swtichOffAll = function () {

        }
    }

})(jQuery)

