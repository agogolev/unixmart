﻿var handler = function () {
    $("#Page").val("1");
    var state = {};
    state[$(this).attr("id")] = true;
    var checked = $(this).prop("checked");
    if (checked)
        $.bbq.pushState(state, 1);
    else
        $.bbq.removeState($(this).attr("id"));
};

function redirectPage(response) {
    location.href = response;
}

$(function () {
    $(document).on("change", "input[id^='Manufs']", handler);
    $(document).on("change", "input[id^='CustomFilters']", handler);

    $(window).bind('hashchange', function () {
        if ($("#filterForm").length != 0) {
            $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
                redirectPage(response);
            });
        }

    });
});
