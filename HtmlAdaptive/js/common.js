(function($){

  $(document).ready(function(){
  
    $("[data-action=dropdown2]").each(function(){
      $(this).pr_dropdown2(null);
    });

    $(".banners-slider").bxSlider({
      mode: 'fade',
      controls: false
    });

    function getSlideParams(sliderContainer, slides) {
      return  {
        controls: true,
        pager: true,
        maxSlides: slides,
        minSlides: slides,
        moveSlides: slides,
        slideWidth: 400,
        onSliderLoad: function(){
          var slidesNum = sliderContainer.find(".bx-pager").find(".bx-pager-item").length;
          if(slidesNum<2) {
            sliderContainer.find(".bx-pager").hide();
          }
        }
      };
    }
    
    $(".content-slider").each(function(){

      var itemH3=0,
          slides=$(this).attr("data-slide-items");
          slidesSm=$(this).attr("data-slide-items-sm");
          slidesXs=$(this).attr("data-slide-items-xs");
      var sliderContainer = $(this).children(".content-slider-wrapper");
      
      var slider = sliderContainer.bxSlider(getSlideParams(sliderContainer, ($(window).width()>=980 ? slides : ($(window).width()>=768 ? slidesSm : slidesXs)) ));
      
      $(window).resize(function(){
        sliderContainer.reloadSlider(getSlideParams(sliderContainer, ($(window).width()>=980 ? slides : ($(window).width()>=768 ? slidesSm : slidesXs)) ));
      })
      
      
      
      
    });
    
    
    $(".columns").each(function(){
      var columns = $(this).attr("data-columns");
      $(this).columnize({
        columns: columns,
        lastNeverTallest: true
      });
    });
    
    
    $(document).on("click", ".catListSub > ul > li > a", function(){
      if($(this).next("ul,.charsList").length) {
        $(this).parent().toggleClass("catListSub-selected");
        return false;
      } else {
        return true;
      }
    });
    $(document).on("click", ".charsList-group-title", function(){
      $(this).parent().toggleClass("charsList-group-open");
      return false;
    });
    
    //$(".nnm_popup").nnm_popup();
    

    $(".regLink-link").click(function(){
      $(this).parents(".popup").toggleClass("popup-opened");
      $("#registration").toggleClass("popup-opened");
      return false;
    });
    
    $(".hasSubitems").on("click",function(e){
      if($(window).width()<980) {
        if(!$(e.target).parent().hasClass("hasSubitems")) {
          return true;
        }
        $(this).siblings().removeClass("opened");
        if($(this).hasClass("opened")) {
          $(this).removeClass("opened");
        } else {
          $(this).addClass("opened");
        }
        return false;
      }
    });
    $(".hasSubitems").on("mouseover",function(){
      if($(window).width()>=980) {
        $(this).siblings().removeClass("hovered");
        $(this).addClass("hovered");        
      }
    });
    $(".nav-catalog-dropdown .nav-catalog-dropdown-item:first-child").mouseover();
    
    $(".card-tabs").tabs();
    
    
    //
    // Range
    //
    $(".range-full").each(function () {
        var slider = $(this),
            inputS = $(this).siblings(".range-input-start"),
            inputE = $(this).siblings(".range-input-end");
        var s = parseInt(slider.attr("data-start")),
            e = parseInt(slider.attr("data-end")),
            sf = parseInt(slider.attr("data-fact-start")),
            ef = parseInt(slider.attr("data-fact-end")),
            sP = (sf - s) / ((e - s) / 100),
            eP = (ef - s) / ((e - s) / 100),
            r = parseInt(slider.attr("data-round"));
        slider.noUiSlider({
            start: [sP, eP],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
        slider.on("slide", function () {
            setRangeValues($(this));
        })
        setRangeValues(slider);

        function setRangeValues(slider) {
            var rt = slider.siblings(".range-text"),
                rts = rt.find(".range-text-start"),
                rte = rt.find(".range-text-end"),
                val = slider.val(),
                valS = ((e - s) / 100 * val[0] + s).toFixed(r),
                valE = ((e - s) / 100 * val[1] + s).toFixed(r);
            rts.text(valS);
            rte.text(valE);
            inputS.val(valS);
            inputE.val(valE);
            return false;
        }
    });

    
    if($(".cart-goods-item").length) {
      $(".cart-goods-item-quantity-decrease").click(function(){
        var input = $(this).siblings("input[type=text]"),
            input_v = parseInt(input.val());
        if(!isNaN(input_v) && input_v>1) {
          input.val(input_v-1);
        } else {
          input.val("1");
        }
      });
      $(".cart-goods-item-quantity-increase").click(function(){
        var input = $(this).siblings("input[type=text]"),
            input_v = parseInt(input.val());
        if(!isNaN(input_v)) {
          input.val(input_v+1);
        } else {
          input.val("1");
        }
      });
    }
    
  
    $(".footerNav-2top").click(function(){
      $("html, body").animate({
        scrollTop: $('#top').offset().top+"px"
        },{
        duration: 500
      });
      return false;
    });


    $("a[rel=goods-image]").fancybox();
    
    $(".card-gallery-thmbs > a").click(function(){
      var i = $(this).attr("data-slide-prv");
      $(".card-gallery-large").find("a[data-slide]").removeClass("card-gallery-large-selected");
      $(".card-gallery-large").find("a[data-slide="+i+"]").addClass("card-gallery-large-selected");
      $(this).siblings().removeClass("active");
      $(this).addClass("active");
      return false;
    });
    /////
    $(".card-gallery-next").click(function(){
      var c = $(".card-gallery-thmbs").find(".active");
      if(c.next().length) 
        c.next().click();
      else
        $(".card-gallery-thmbs a:first-child").click();
    });
    /////
    $(".card-gallery-prev").click(function(){
      var c = $(".card-gallery-thmbs").find(".active");
      if(c.prev().length) 
        c.prev().click();
      else
        $(".card-gallery-thmbs a:last-child").click();
    });
  })

})(jQuery);