﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using AutoMapper;
using Ecommerce.Repository.GeoTargeting;
using Ecommerce.Repository.Referer;
using NLog;
using NoName.Repository;
using NoName.Repository.Contracts;
using NoName.Repository.Goods.Contracts;
using NoName.Web.App_Start;
using NoName.Web.Builders;
using NoName.Web.Builders.Contracts;
using NoName.Web.Controllers;
using NoName.Web.Infrastructure;
using NoName.Web.Infrastructure.Factories;
using NoName.Web.Properties;

namespace NoName.Web
{
    public class MvcApplication : HttpApplication, IContainerAccessor
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public static IContainer Container { get; private set; }

        public IContainer GetContainer()
        {
            return Container;
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var mapper = AutoMapperConfiguration.Configure();

            CreateContainer(mapper);

            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        protected virtual void CreateContainer(IMapper mapper)
        {
            var builder = new ContainerBuilder();

            RegisterInterfaces(builder);

            // Register your MVC controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            // Register your MVC controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            
            builder.RegisterInstance(mapper).As<IMapper>();

            Container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }

        private static void RegisterInterfaces(ContainerBuilder builder)
        {
            foreach (var assembly in BuildManager.GetReferencedAssemblies().Cast<Assembly>())
            {
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.Name.EndsWith("Repository"))
                    .AsImplementedInterfaces();
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.Name.EndsWith("Service"))
                    .AsImplementedInterfaces();
                builder.RegisterAssemblyTypes(assembly)
                    .Where(t => t.Name.EndsWith("Builder"))
                    .AsImplementedInterfaces();
                builder.RegisterType<CatalogFilterFactory>().As<ICatalogFilterFactory>();
                builder.RegisterType<GeoTargetingRepository>()
                    .As<IGeoTargetingRepository>()
                    .WithParameter(
                        "dbFileName",
                        HttpContext.Current.Server.MapPath("~/App_Data/cidr_optim.txt"));
                builder.RegisterType<NoNameRefererCalc>().As<IRefererLabelCalc>();
                builder.RegisterType<OnlineBuilder>().As<IOnlineBuilder>()
                    .WithParameters(new []
                    {
                        new NamedParameter("onlineUrl", Settings.Default.OnlineUrl),
                        new NamedParameter("userName", Settings.Default.OnlineLogin),
                        new NamedParameter("password", Settings.Default.OnlinePassword),
                        new NamedParameter("errorUrl", Settings.Default.OnlineErrorUrl),
                        new NamedParameter("resultUrl", Settings.Default.OnlineResultUrl)
                    });
            }
        }

        protected void Session_Start()
        {
            var ip = Request.UserHostAddress;

            using (var scope = Container.BeginLifetimeScope())
            {
                var basketRepository = scope.Resolve<IBasketRepository>();
                var cookieRepository = scope.Resolve<ICookieRepository>();
                var geoTargetingRepository = scope.Resolve<IGeoTargetingRepository>();
                var referRepository = scope.Resolve<IRefererRepository>();
                var sessionRepository = scope.Resolve<ISessionRepository>();
                var yandexMarketRepository = scope.Resolve<IYandexMarketRepository>();

                var persistSessionId = cookieRepository.SessionId;
                // перезаписываем сессионную куку чтобы она продлила срок действия ещё на положекнный срок от текущего
                cookieRepository.SessionId = persistSessionId;

                if (Request.Params["actionpay"] != null) cookieRepository.ActionpayId = Request.Params["actionpay"];

                //ip = "80.71.36.114"; // тест москва
                var region = geoTargetingRepository.GetRegion(ip);
                sessionRepository.RegionId = region;
                sessionRepository.UserHostAddress = ip;
                sessionRepository.RegisterSession();
                sessionRepository.Basket = basketRepository.GetBasketBySession(
                    Settings.Default.SHOP_TYPE,
                    Settings.Default.CATALOG_ROOT,
                    persistSessionId);

                var refUrl = Request.UrlReferrer;

                var url = Request.Url;
                try
                {
                    sessionRepository.RefererLabel = refUrl != null
                        ? referRepository.GetLabel(refUrl.Host, refUrl.Query, url.Query)
                        : referRepository.GetLabel("самонабор", null, url.Query);
                    var param = HttpUtility.ParseQueryString(url.Query);
                    if (Settings.Default.LogSessionStart)
                    {
                        logger.Info(url);    
                    }

                    if (param["frommarket"] != null)
                    {
                        yandexMarketRepository.RegisterMarket(param["fromMarket"], url.ToString());
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Message: {0}\r\nStackTrace:{1}", e.Message, e.StackTrace);
                }

                if (Request.Params["cpamit_uid"] != null)
                {
                    // определяем рекламную площадку admitad
                    sessionRepository.RefererLabel = Settings.Default.AdmitadCodeLabel;
                }
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.Url.Host.IndexOf("localhost", StringComparison.InvariantCultureIgnoreCase) == -1 && Request.Url.Host.IndexOf("unixmart", StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                Response.RedirectPermanent($"https://www.{Request.Url.Host}{Request.RawUrl}", true);
            }
            if (Settings.Default.SecureOnly && !Request.IsSecureConnection)
            {
                Response.RedirectPermanent($"https://{Request.Url.Host}{Request.RawUrl}", true);
            }
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();
            Response.Clear();
            var id = 500;
            if (ex is HttpException)
            {
                var filePath = Context.Request.FilePath;
                var url = Request.Url;
                var host = Request.UserHostAddress;
                logger.Error("Message: {0}\r\nStackTrace: {1}\r\nUrl:{2}\r\nFilePath:{3}\r\nHost:{4}", ex.Message,
                    ex.StackTrace, url, filePath, host);
                id = ((HttpException)ex).GetHttpCode();
            }
            else
                logger.Error("Message: {0}\r\nRequest: {1}\r\nStackTrace:{2}", ex.Message, Request.RawUrl, ex.StackTrace);

            IController errorController = DependencyResolver.Current.GetService<ErrorController>();
            var routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Oops");
            routeData.Values.Add("id", id);
            Server.ClearError();
            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}