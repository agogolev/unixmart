(function ($) {
    $.widget("ui.nnm_popup", {
        options: {
            openClass: "popup-opened",
            container: ".popup-container",
            remote: false,
            remote_pos: "above",
            remote_x: 0,
            remote_Y: 0,

            //callback
            clickMe: null

        },

        _init: function () {
            var self = this,
                e = self.element,
                o = self.options;
            if (e.attr("data-remote")) {
                o.remote = true;
                o.container = e.attr("data-target");
                o.remote_pos = e.attr("data-pos");
            }

            e.find(".popup-link").click(function () {
                self._toggleState();
                return false;
            });

            if (o.remote) {
                $(o.container).find(".popup-container-close").click(function () {
                    self._close();
                    self._closeRemote();
                    return false;
                });
            } else {
                e.find(".popup-container-close").click(function () {
                    self._close();
                    return false;
                });
            }
        },


        _toggleState: function () {
            var self = this,
                e = self.element,
                o = self.options;
            switch (o.remote) {
                case (false): {
                    if (e.hasClass(o.openClass)) {
                        self._close();
                    } else {
                        self._open();
                    }
                    break;
                }
                case (true): {
                    if (e.hasClass(o.openClass)) {
                        self._closeRemote();
                    } else {
                        self._setPosRemote();
                        self._open();
                    }
                    break;
                }
            }
            return false;
        },

        _open: function () {
            var self = this,
                e = self.element,
                o = self.options;
            $("body").find(".popup").removeClass(o.openClass);

            e.addClass(o.openClass);
            return false;
        },

        _close: function () {
            var self = this,
                e = self.element,
                o = self.options;
            e.removeClass(o.openClass);
            return false;
        },

        _openRemote: function () {
            var self = this,
                e = self.element,
                o = self.options;
        },

        _closeRemote: function () {
            var self = this,
                e = self.element,
                o = self.options;
            $(o.container).hide();
        },


        _setPosRemote: function () {
            var self = this,
                 e = self.element,
                 o = self.options,
                callback = self.options.clickMe;
            var x = e.offset().left,
                y = e.offset().top,
                xW = $(window).outerWidth(),
                w = $(o.container).outerWidth();
            switch (o.remotePos) {
                default: {
                    if (x + w > xW) {
                        x = xW - w - 30;
                    } else {
                        x = x - w / 2;
                    }
                    if (x < 30) {
                        x = 30;
                    }
                    o.remote_x = x;
                    o.remote_y = y - 150;
                    $(o.container).css("left", o.remote_x);
                    $(o.container).css("top", o.remote_y);
                    e.addClass(o.openClass);
                    if (callback != null) {
                        callback(e);
                    }
                    $(o.container).show();
                }
            }
        }


    });
})(jQuery);