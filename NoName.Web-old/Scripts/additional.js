﻿(function($){
    $(".popup").nnm_popup({
        clickMe: buyGoods
    });
    $(document).on("click", ".accessory-buy", function ()
    {
        var id = $(this).data("id");
        $.ajax({
            async: true,
            type: "POST",
            url: "/basket/addAccessory",
            data: { goodsID: id },
            success: function (response) {
                $("#basketForm").html(response.html);
                $("#basketAmount").html(response.basketAmount);
            }
        });
    });

    //
    // Вызов звонка оператора
    //
    $(".requestCallback").submit(function () {
        var form = $(this),
            phoneText = form.find("input[name=callbackPhone]");
        if (phoneText.val() === "") {
            $(phoneText).attr("placeholder", "Телефон?");
            $(phoneText).addClass("validation-error");
            return false;
        }
        $("#submitButton").attr("disabled", "disabled");
        $.post(form.attr("action"), form.serialize(), function (response) {
            var message = response.message;
            if (message === "empty") message = "Вы не ввели номер телефона";
            phoneText.val("");
            $("#submitButton").removeAttr("disabled");
            alert(message);
            $("body").find(".popup").removeClass("popup-opened");
        });
        return false;
    });

    $("#BuyOneClick").validate({
        focusInvalid: true
    });
    $("#BuyOneClick").submit(function () {
        $.post($(this).attr("action"), $(this).serialize(), function (data) {
            if (data.message != null) {
                alert(data.message);
            }
            if (data.redirect != null) {
                location.href = data.redirect;
            }
        });
        return false;
    });

    $(".mask-phone").mask("9(999)9999999");

})(jQuery);

function buyGoods(elem) {
    var id = $(elem).data("id"),
        cartId = $(elem).data("target") + " .innerContent";
    $.ajax({
        async: true,
        type: "POST",
        url: "/basket/add",
        data: { goodsID: id },
        success: function(data) {
            $(cartId).html(data.html);
            $("#basketAmount").html(data.basketAmount);
        }
    });
}