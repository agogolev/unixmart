﻿var handler = function () {
    $("#Page").val("1");
    var state = {};
    state[$(this).attr("id")] = true;
    var checked = $(this).prop("checked");
    if (checked)
        $.bbq.pushState(state, 1);
    else
        $.bbq.removeState($(this).attr("id"));
};

function loadContent(response) {
    $("#GoodsList").html(response);
}

$(function () {
    $(document).on("change", "input[id^='Manufs']", handler);
    $(document).on("change", "input[id^='CustomFilters']", handler);

    $(document).on("click", ".sortOption-up", function () {
        $("#Page").val(null);
        $("#TypeSort").val("up");
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });
    $(document).on("click", ".sortOption-down", function () {
        $("#Page").val(null);
        $("#TypeSort").val("down");
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });

    $(document).on("click", ".numOnPage", function () {
        $("#Page").val($(this).attr("rel"));
        $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
            loadContent(response);
        });
        return false;
    });
    $(window).bind('hashchange', function () {
        $("input[id^='Manufs']").each(function () {
            var defaultState = this.id === defaultCheckBox;
            var state = $.bbq.getState(this.id, true) || defaultState;
            if (state) {
                $(this).attr("checked", "checked");
            }
            else {
                $(this).removeAttr("checked");
            }

        });
        $("input[id^='CustomFilters']").each(function () {
            var state = $.bbq.getState(this.id, true) || false;
            if (state) {
                $(this).attr("checked", "checked");
            }
            else {
                $(this).removeAttr("checked");
            }

        });
        if ($("#filterForm").length != 0) {
            $.post($("#filterForm").attr("action"), $("#filterForm").serialize(), function (response) {
                loadContent(response);
                $(".popup").nnm_popup({
                    clickMe: buyGoods
                });
            });
        }

    });

    $(window).trigger('hashchange');
});
