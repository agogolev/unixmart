﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NoName.Web.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration configuration)
        {
            configuration.MapHttpAttributeRoutes();

            //configuration.Routes.MapHttpRoute(
            //    null,
            //    "api/catalog/{catId}/{manufId}",
            //    new { Controller = "Landing", Action = "GetCount", manufId = RouteParameter.Optional });

            configuration.Routes.MapHttpRoute(
                "API Default", 
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });
        }
    }
}