﻿using System;
using System.Net.Mail;
using System.Web.Mvc;
using NoName.Domain;
using NoName.Repository;
using NoName.Repository.Contracts;
using NoName.Repository.Goods.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    public partial class BasketController : BaseController
    {
        private readonly IBasketPageBuilder _basketPageBuilder;
        private readonly IMailRepository _mailRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly ISessionRepository _sessionRepository;

        public BasketController(
            IBasketPageBuilder basketPageBuilder,
            IMailRepository mailRepository,
            IOrderRepository orderRepository, 
            ISessionRepository sessionRepository)
        {
            _basketPageBuilder = basketPageBuilder;
            _mailRepository = mailRepository;
            _orderRepository = orderRepository;
            _sessionRepository = sessionRepository;
        }

        public virtual ActionResult BasketAmount()
        {
            return PartialView(_sessionRepository.Basket);
        }

        [HttpGet]
        public virtual ActionResult Index()
        {
            BasketViewModel model = _basketPageBuilder.Create();
            if (model.TotalGoodsCount == 0)
            {
                return RedirectToAction(MVC.Home.Index());
            }

            ViewBag.CurrentStep = 0;
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Index(BasketViewModel model)
        {

            if (Request.IsAjaxRequest()) 
            {
                var newModel = _basketPageBuilder.Save(model);
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketContent", newModel), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }

            //if (!model.DeliveryType.HasValue || model.DeliveryType.Value != SelfDeliveryMoscow)
            //{
            //    if (string.IsNullOrWhiteSpace(model.StreetName)) ModelState.AddModelError("StreetName", "Укажите улицу");
            //    if (string.IsNullOrWhiteSpace(model.House)) ModelState.AddModelError("StreetName", "Укажите дом");
            //    if (string.IsNullOrWhiteSpace(model.Flat)) ModelState.AddModelError("StreetName", "Укажите квартиру");
            //}
            if (ModelState.IsValid)
            {
                _basketPageBuilder.Save(model);
                return RedirectToAction(MVC.Order.Index());
            }

            _basketPageBuilder.FillModel(model);
            return View(model);
        }

        public virtual ActionResult Add(int goodsID)
        {
            _basketPageBuilder.Add(goodsID);
            if (Request.IsAjaxRequest())
            {
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketAdd", basket), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult AddAccessory(int goodsID)
        {
            _basketPageBuilder.Add(goodsID);
            if (Request.IsAjaxRequest())
            {
                BasketViewModel model = _basketPageBuilder.Create();
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketContent", model), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult Delete(int id)
        {
            _basketPageBuilder.Delete(id);
            if (Request.IsAjaxRequest())
            {
                BasketViewModel model = _basketPageBuilder.Create();
                var basket = _sessionRepository.Basket;
                return new JsonResult
                {
                    Data = new { html = RenderPartialViewToString("BasketContent", model), basketAmount = RenderPartialViewToString("BasketAmount", basket) }
                };
            }
            return RedirectToAction(MVC.Basket.Index());
        }

        public virtual ActionResult BuyOneClick(Guid goodsOID)
        {
            return PartialView(new BuyOneClickModel
            {
                GoodsOID = goodsOID
            });
        }
        [HttpPost]
        public virtual ActionResult BuyOneClick(BuyOneClickModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var id = _basketPageBuilder.QuickSave(model);
                    Order order = _orderRepository.GetOrder(id, Settings.Default.CATALOG_ROOT);

                    // отправляем сообщения о совершении заказа
                    _mailRepository.SendOrderInfo(
                        order,
                        Settings.Default.ORDER_MAIL,
                        Settings.Default.SITE_URL,
                        null);

                    //запоминаем номер последнего сделанного заказа
                    _sessionRepository.LastOrderOID = order.OID;

                    return Json(new { redirect = $"/finish?orderOID={order.OID}"}, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { message = "Произошла ошибка " + e.Message }, JsonRequestBehavior.AllowGet);
                }

            }

            return Json(new { message = "Заполните обязательные поля" }, JsonRequestBehavior.AllowGet);
        }
    }
}