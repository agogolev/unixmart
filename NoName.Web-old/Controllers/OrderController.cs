﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Goods.Contracts;
using NoName.Services.Interfaces;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    public partial class OrderController : BaseController
    {
        private readonly IActionpayService _actionpayService;
        private readonly IBasketRepository _basketRepository;
        private readonly ICookieRepository _cookieRepository;
        private readonly IECommerceService _eCommerceService;
        private readonly IMailRepository _mailRepository;
        private readonly IOrderPageBuilder _orderPageBuilder;
        private readonly IOrderRepository _orderRepository;
        private readonly ISessionRepository _sessionRepository;

        public OrderController(
            IActionpayService actionpayService,
            IBasketRepository basketRepository,
            ICookieRepository cookieRepository,
            IECommerceService eCommerceService,
            IMailRepository mailRepository,
            IOrderPageBuilder orderPageBuilder, 
            IOrderRepository orderRepository, 
            ISessionRepository sessionRepository)
        {
            _actionpayService = actionpayService;
            _basketRepository = basketRepository;
            _cookieRepository = cookieRepository;
            _eCommerceService = eCommerceService;
            _mailRepository = mailRepository;
            _orderPageBuilder = orderPageBuilder;
            _orderRepository = orderRepository;
            _sessionRepository = sessionRepository;
        }
        
        // GET: /Order/
        public virtual ActionResult Index()
        {
            return View(_orderPageBuilder.Create());
        }

        [HttpPost]
        public virtual ActionResult Index(OrderViewModel model)
        {
            var basket = _sessionRepository.Basket;
            if (basket == null || basket.TotalGoodsCount == 0)
                return RedirectToAction(MVC.Home.Index());

            if (!model.OfertaAccept)
            {
                ModelState.AddModelError("OfertaAccept", "Необходимо согласие с условиями предоставления услуг");
            }

            if (basket.DeliveryType != Constants.SelfDeliveryMoscow)
            {
                if (model.NeedCityInOrder && string.IsNullOrWhiteSpace(model.City))
                {
                    ModelState.AddModelError("City", "Укажите город");
                }

                if (string.IsNullOrWhiteSpace(model.StreetName))
                {
                    ModelState.AddModelError("StreetName", "Укажите улицу");
                }

                if (string.IsNullOrWhiteSpace(model.House))
                {
                    ModelState.AddModelError("House", "Укажите дом");
                }

                if (string.IsNullOrWhiteSpace(model.Flat))
                {
                    ModelState.AddModelError("Flat", "Укажите квартиру");
                }
            }

            if (ModelState.IsValid)
            {
                var id = _orderPageBuilder.Save(model);
                if (model.IsOnlinePayment)
                {
                    return RedirectToAction(MVC.Online.Index(id));
                }
                Order order = _orderRepository.GetOrder(id, Settings.Default.CATALOG_ROOT);

                //запоминаем номер последнего сделанного заказа
                _sessionRepository.LastOrderOID = order.OID;
                //переход на страницу завершения заказа
                return RedirectToAction(MVC.Order.Finish(order.OID));
            }
            return View(_orderPageBuilder.FillModel(model));
        }

        public virtual ActionResult Finish(Guid orderOID)
        {
            //принадлежит ли заказ текущему пользователю
            if (_sessionRepository.LastOrderOID != orderOID)
                return InvokeHttp404(HttpContext);

            //ищем заказ в БД
            Order order = _orderRepository.GetOrder(orderOID, Settings.Default.CATALOG_ROOT);
            if (order == null)
                return InvokeHttp404(HttpContext);

            // отправляем сообщения о совершении заказа
            _mailRepository.SendOrderInfo(
                order,
                Settings.Default.ORDER_MAIL,
                Settings.Default.SITE_URL,
                null);

            ViewBag.ECommerce = _eCommerceService.GeneratePurchaseScript(order);

            if (!string.IsNullOrEmpty(_cookieRepository.ActionpayId))
            {
                _actionpayService.RequestActionpay(_cookieRepository.ActionpayId, order.OrderNum, order.TotalPrice, Settings.Default.ActionpayTargetId);
            }

            _sessionRepository.Basket = _basketRepository.GetBasketBySession(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT, _cookieRepository.SessionId);

            return View(order);
        }
    }
}