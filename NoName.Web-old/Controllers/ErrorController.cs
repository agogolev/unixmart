﻿using System.Net;
using System.Web.Mvc;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
	public partial class ErrorController : BaseController
	{
        private readonly ICatalogPageBuilder _catalogPageBuilder;

	    public ErrorController(ICatalogPageBuilder catalogPageBuilder)
	    {
	        _catalogPageBuilder = catalogPageBuilder;
	    }

	    [HandleError]
		public virtual ActionResult Oops(int id)
		{
			Response.StatusCode = id;
			Response.TrySkipIisCustomErrors = true;

			return View(new ErrorPageModel
			{
			    StatusCode = id,
                ErrorMessage = id == 404 ? "Ошибка 404. Неправильно указан адрес" : "Уупс! Что-то пошло не так",
                ErrorDescription =  id == 404 ? "Страница с указанным адресом не существует, либо в данный момент она недоступна" : "На сайте произошла непредвиденная ошибка, которая не позволила нам отобразить то, что Вы хотели",
                PhoneNumber = Settings.Default.SHOP_PHONE_TEXT,
                Menu = _catalogPageBuilder.Create(Settings.Default.CATALOG_ROOT, "catalog")
            });
		}

        public virtual ActionResult Description(string header, string body)
        {
            Response.StatusCode = 500;
            Response.TrySkipIisCustomErrors = true;

            return View(MVC.Error.Views.Oops, new ErrorPageModel
            {
                StatusCode = Response.StatusCode,
                ErrorMessage = header,
                ErrorDescription = body,
                PhoneNumber = Settings.Default.SHOP_PHONE_TEXT,
                Menu = _catalogPageBuilder.Create(Settings.Default.CATALOG_ROOT, "catalog")
            });
        }

    }
}
