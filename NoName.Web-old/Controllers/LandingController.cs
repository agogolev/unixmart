﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using NoName.Repository.Contracts;
using NoName.Repository.Menu.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 3600, VaryByParam = "*")]
    public class LandingController : ApiController
    {
        private readonly ICatalogPageBuilder _catalogPageBuilder;
        private readonly IMenuRepository _menuRepository;

        public LandingController(ICatalogPageBuilder catalogPageBuilder, IMenuRepository menuRepository)
        {
            _catalogPageBuilder = catalogPageBuilder;
            _menuRepository = menuRepository;
        }

        [System.Web.Http.Route("api/catalog/{catid}/{manufId?}")]
        public int Get(string catId, string manufId = null)
        {
            var category = _menuRepository.FromShortName(catId, Settings.Default.CATALOG_ROOT);
            if (category.IsTag)
            {
                return LandingPage(category.NodeId, category.StringRepresentation);
            }
            return category.HasChild && !category.GoodsList ? 0 : Subcategory(category.NodeId, catId, manufId, category.GoodsList);
        }

        private int Subcategory(int id, string itemId, string manufId, bool withSameLevel)
        {
            GoodsListPageModel model = _catalogPageBuilder.CreateGoodsList(id, itemId, manufId, 1, null, withSameLevel);
            return model.GoodsCount;
        }

        private int LandingPage(int objectId, string itemId)
        {
            GoodsListPageModel model = _catalogPageBuilder.CreateLandingPage(objectId, itemId, 1, null);
            return model.GoodsCount;
        }
    }
}
