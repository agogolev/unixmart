﻿using System;
using System.EnterpriseServices;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NLog;
using NoName.Repository.Contracts;
using NoName.Repository.Goods.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models.Online;

namespace NoName.Web.Controllers
{
    public partial class OnlineController : Controller
    {
        private static Logger _log = LogManager.GetCurrentClassLogger();
        private readonly IOnlineBuilder _onlineBuilder;
        private readonly IOrderRepository _orderRepository;
        private readonly ISessionRepository _sessionRepository;
        private readonly int _orderStatusSuccess = 2;

        public OnlineController(IOnlineBuilder onlineBuilder, IOrderRepository orderRepository, ISessionRepository sessionRepository)
        {
            _onlineBuilder = onlineBuilder;
            _orderRepository = orderRepository;
            _sessionRepository = sessionRepository;
        }

        public virtual async Task<ActionResult> Index(Guid preorderId)
        {
            try
            {
                var preOrder = _orderRepository.GetPreOrder(preorderId);
                var model = new RegisterPaymentRequest
                {
                    OrderNumber = preOrder.OrderNum,
                    PriceKop = (int)((preOrder.Items.Sum(c => c.FullPrice) + preOrder.DeliveryPrice) * 100)
                };
                await _onlineBuilder.RegisterPaymentSession(model);
                _orderRepository.SavePaymentNumber(preorderId, model.PaymentNumber);
                return new RedirectResult(model.RedirectUrl);
            }
            catch (Exception ex)
            {
                return RedirectToAction(MVC.Error.Description("Ошибка процедуры оплаты", ex.Message));
            }
        }

        public virtual async Task<ActionResult> Result(string orderId)
        {
            try
            {
                GetOrderStatusResponse response = await _onlineBuilder.GetOrderStatus(orderId);
                if (response.OrderStatus != _orderStatusSuccess)
                {
                    return RedirectToAction(MVC.Error.Description("Заказ не был оплачен", "Попробуйте ещё раз"));
                }
                var preOrder = _orderRepository.GetPreOrder(orderId);
                var orderOID = _orderRepository.SaveOrderFromPreOrder(preOrder.OID, _sessionRepository.Basket.OID, response.ApprovalCode);
                //запоминаем номер последнего сделанного заказа
                _sessionRepository.LastOrderOID = orderOID;
                //переход на страницу завершения заказа
                return RedirectToAction(MVC.Order.Finish(orderOID));
            }
            catch (Exception ex)
            {
                return RedirectToAction(MVC.Error.Description("Ошибка процедуры оплаты", ex.Message));
            }
        }
    }
}