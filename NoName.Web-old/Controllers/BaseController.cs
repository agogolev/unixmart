﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NoName.Web.Controllers
{
	public abstract partial class BaseController : Controller
	{
		protected override void HandleUnknownAction(string actionName)
		{
			//If controller is ErrorController dont 'nest' exceptions
			if (GetType() != typeof(ErrorController))
				InvokeHttp404(HttpContext);
		}

		public virtual ActionResult InvokeHttp404(HttpContextBase httpContext)
		{
            IController errorController = DependencyResolver.Current.GetService<ErrorController>();
			var errorRoute = new RouteData();
			errorRoute.Values.Add("controller", "Error");
			errorRoute.Values.Add("action", "Http404");
			errorController.Execute(new RequestContext(
				 httpContext, errorRoute));

			return new EmptyResult();
		}

		protected string RenderPartialViewToString(string viewName, object model)
		{
			if (string.IsNullOrEmpty(viewName))
				viewName = ControllerContext.RouteData.GetRequiredString("action");

			ViewData.Model = model;

			using (var sw = new StringWriter())
			{
				var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
				var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
				viewResult.View.Render(viewContext, sw);

				return sw.GetStringBuilder().ToString();
			}
		}
	}
}