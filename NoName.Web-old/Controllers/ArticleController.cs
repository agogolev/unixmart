﻿using System.Web.Mvc;
using NoName.Web.Builders.Contracts;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 3600, VaryByParam = "*")]
    public partial class ArticleController : Controller
    {
        private readonly IArticlePageBuilder _articlePageBuilder;

        public ArticleController(IArticlePageBuilder articlePageBuilder)
        {
            _articlePageBuilder = articlePageBuilder;
        }

        public virtual ActionResult Index(string itemId)
        {

            return View(_articlePageBuilder.Create(itemId));
        }
    }
}