﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 3600, VaryByParam = "*")]
    public partial class ManufacturerController : Controller
    {
        private readonly ICatalogPageBuilder _catalogPageBuilder;
        private readonly IManufacturerPageBuilder _manufacturerPageBuilder;

        public ManufacturerController(ICatalogPageBuilder catalogPageBuilder, IManufacturerPageBuilder manufacturerPageBuilder)
        {
            _catalogPageBuilder = catalogPageBuilder;
            _manufacturerPageBuilder = manufacturerPageBuilder;
        }

        public virtual ActionResult List(string manufId, string itemId = null)
        {
            var model = _manufacturerPageBuilder.Create(manufId, itemId);
            if (model.ChildMenu.Count == 0)
            {
                model.SimilarGoods = _catalogPageBuilder.GetSimilarGoods(model.BreadCrumbs.Last().Name, model.BreadCrumbs.Select(m => m.NodeId).ToList());
            }
            return View(model);
        }
	}
}