﻿using System.Web.Mvc;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;

namespace NoName.Web.Controllers
{
    //[OutputCache(Duration = 600, VaryByParam = "*")]
    public partial class GoodsController : Controller
    {
        private readonly IGoodsPageBuilder _goodsPageBuilder;

        private readonly IUrlRepository _urlRepository;

        public GoodsController(
            IGoodsPageBuilder goodsPageBuilder,
            IUrlRepository urlRepository)
        {
            _goodsPageBuilder = goodsPageBuilder;
            _urlRepository = urlRepository;
        }

        public virtual ActionResult Index(string itemId, int id)
        {
            string canonicalUrl = _urlRepository.CanonicalGoodsUrl(id);
            if (Request.Path != canonicalUrl)
            {
                return RedirectPermanent(canonicalUrl);
            }

            return View(_goodsPageBuilder.CreateItem(id));
        }

        public virtual ActionResult SameCharsBlock(int id, int oid)
        {
            return PartialView(_goodsPageBuilder.GetSputnikGoods(id, oid, 6, false));
        }
    }
}