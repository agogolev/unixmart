﻿using System;
using System.Web.Services;
using NoName.Repository.Contracts;
using NoName.Web.Infrastructure;
using NoName.Web.Properties;

namespace NoName.Web
{
	/// <summary>
	/// Summary description for YadexMarketService
	/// </summary>
	[WebService(Namespace = "https://www.unixmart.ru/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
    public class YandexMarketWebService : BaseService<YandexMarketWebService>
	{
		public IYandexMarketRepository YandexMarketRepository { get; set; }

		[WebMethod]
		public string RefreshMarketData()
		{
            YandexMarketRepository.RefreshMarketData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup, Settings.Default.HasStore);
			return null;

		}
        [WebMethod]
        public string RefreshAdmitadData()
        {
            YandexMarketRepository.RefreshAdmitadData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup);
            return null;
        }
		[WebMethod]
		public string RefreshGoogleData()
		{
            YandexMarketRepository.RefreshGoogleData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT);
			return null;

		}
		[WebMethod]
		public string RefreshWikimartData()
		{
            YandexMarketRepository.RefreshWikimartData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup);
			return null;

		}
		[WebMethod]
		public string RefreshWikiData()
		{
            YandexMarketRepository.RefreshWikiData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup);
			return null;

		}
		[WebMethod]
		public string RefreshNadaviData()
		{
            YandexMarketRepository.RefreshNadaviData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup);
			return null;

		}
		[WebMethod]
		public string RefreshUtinetData()
		{
            YandexMarketRepository.RefreshUtinetData(Settings.Default.SHOP_TYPE, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME,
                Settings.Default.SITE_URL, Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup);
			return null;
		}
		[WebMethod]
		public string RefreshCommodityData(Guid OID)
		{
			YandexMarketRepository.RefreshCommodityGroupData(OID, Settings.Default.SHOP_NAME, Settings.Default.SHOP_NAME, Settings.Default.SITE_URL,
				Settings.Default.CATALOG_ROOT, Settings.Default.HasPickup);
			return null;
		}
	}
}
