﻿using AutoMapper;
using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Infrastructure
{
    public class AutoMapperConfiguration
    {
        public static IMapper Configure()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => {
                cfg.CreateMap<CatalogMenu, CatalogMenuModel>();
                cfg.CreateMap<CatalogMenu, GoodsListPageModel>();
                cfg.CreateMap<Basket, BasketViewModel>();
                cfg.CreateMap<ManufacturerMenu, ManufacturerMenuModel>();
            });

            return mapperConfiguration.CreateMapper();
        }
    }
}