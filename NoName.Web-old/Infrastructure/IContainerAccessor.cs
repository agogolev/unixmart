﻿using Autofac;

namespace NoName.Web.Infrastructure
{
    public interface IContainerAccessor
    {
        IContainer GetContainer();
    }
}
