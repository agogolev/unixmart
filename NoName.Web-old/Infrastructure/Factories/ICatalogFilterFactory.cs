using System;
using System.Collections.Generic;
using NoName.Domain;
using NoName.Web.Models;
using CustomFilter = Ecommerce.Domain.CustomFilter;
using PriceInterval = Ecommerce.Domain.PriceInterval;
using PriceIntervalMenu = Ecommerce.Domain.PriceIntervalMenu;

namespace NoName.Web.Infrastructure.Factories
{
	public interface ICatalogFilterFactory
	{
        CatalogFilterModel Create(int catalogID, int parentNodeId, int? page, int numOnPage, string sort, string itemId, string manufId, IList<ManufacturerMenu> manufs, IList<CustomFilter> filters);
	    IList<Guid> CreateManufacturerFilter(IList<ManufacturerMenuModel> manufs);
        IList<CustomFilter> CreateCustomFilter(IList<CustomFilterMenuModel> filters, IList<CustomFilter> allFilters);
    }
}