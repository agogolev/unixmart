﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ecommerce.Domain;
using NoName.Web.Models;
using ManufacturerMenu = NoName.Domain.ManufacturerMenu;

namespace NoName.Web.Infrastructure.Factories
{
    public class CatalogFilterFactory : ICatalogFilterFactory
    {
        public CatalogFilterModel Create(
            int catalogID, 
            int parentNodeId, 
            int? page, 
            int numOnPage, 
            string sort, 
            string itemId, 
            string manufId,
            IList<Domain.ManufacturerMenu> manufs, 
            IList<CustomFilter> filters)
        {
            return new CatalogFilterModel
            {
                CatalogID = catalogID,
                ParentNodeId = parentNodeId,
                Page = page,
                NumOnPage = numOnPage,
                TypeSort = sort,
                ItemId = itemId,
                ManufId = manufId,
                Manufs = manufs.Where(m => m.GoodsCount > 0).Select(m => new ManufacturerMenuModel
                {
                    Name = m.Name,
                    ManufRepresentation = m.ManufRepresentation,
                    OID = m.OID,
                    ManufId = m.ManufId,
                    GoodsCount = m.GoodsCount,
                    Checked = m.Checked
                }).ToList(),
                CustomFilters = filters.Where(f => f.ItemsCount > 0).Select(f => new CustomFilterMenuModel
                {
                    ParamType = f.ParamType,
                    Name = f.Name,
                    CustomFilters = f.Values
                        .Where(v => v.ItemsCount > 0)
                        .Select(v => new CustomFilterMenuItem
                        {
                            Id = v.Id,
                            Name = v.Name,
                            OrdValue = v.OrdValue,
                            ItemsCount = v.ItemsCount
                        })
                        .ToList()
                }).ToList()
            };
        }
        public IList<Guid> CreateManufacturerFilter(IList<ManufacturerMenuModel> manufs)
        {
            return manufs?.Where(m => m.Checked).Select(m => m.OID).ToList();
        }

        public IList<CustomFilter> CreateCustomFilter(IList<CustomFilterMenuModel> filters, IList<CustomFilter> allFilters)
        {
            return filters?.Where(m => m.CustomFilters.Any(f => f.Checked))
                .Select(m => new CustomFilter
                {
                    ParamType = m.ParamType,
                    Values = m.CustomFilters
                        .Where(f => f.Checked)
                        .Select(f => new CustomFilterValue
                        {
                            ParamType = m.ParamType,
                            OrdValue = f.OrdValue,
                            Associations = allFilters.Single(f1 => f1.ParamType == m.ParamType)
                                .Values.Single(v => v.OrdValue == f.OrdValue).Associations
                        })
                        .ToList()
                })
                .ToList();
        }
    }
}