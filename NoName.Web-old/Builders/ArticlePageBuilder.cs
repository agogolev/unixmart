﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Menu.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class ArticlePageBuilder : IArticlePageBuilder
    {
        private readonly IArticleRepository _articleRepository;

        private readonly IMenuRepository _menuRepository;

        public ArticlePageBuilder(IArticleRepository articleRepository, IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _menuRepository = menuRepository;
        }

        public ArticleModel Create(string id)
        {
            var article = _articleRepository.GetArticle(id, null);
            return new ArticleModel
            {
                SelectedId = article.Id,
                Article = article,
                Menu = _menuRepository.GetInfoMenu(Settings.Default.INFO_ROOT)
            };
        }

    }
}