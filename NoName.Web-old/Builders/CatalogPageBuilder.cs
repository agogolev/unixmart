﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using NoName.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Goods.Contracts;
using NoName.Repository.Menu.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Infrastructure.Factories;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class CatalogPageBuilder : ICatalogPageBuilder
    {
        private const int maxValueForList = 6;

        private readonly IArticleRepository _articleRepository;
        private readonly ICatalogFilterFactory _catalogFilterFactory;
        private readonly ICompanyMenuRepository _companyMenuRepository;
        private readonly IGoodsRepository _goodsRepository;
        private readonly ILandingPageRepository _landingPageRepository;
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;

        public CatalogPageBuilder(
            IArticleRepository articleRepository,
            ICatalogFilterFactory catalogFilterFactory,
            ICompanyMenuRepository companyMenuRepository,
            IGoodsRepository goodsRepository,
            ILandingPageRepository landingPageRepository,
            IMapper mapper,
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _catalogFilterFactory = catalogFilterFactory;
            _companyMenuRepository = companyMenuRepository;
            _goodsRepository = goodsRepository;
            _landingPageRepository = landingPageRepository;
            _mapper = mapper;
            _menuRepository = menuRepository;
        }

        public CatalogMenuModel Create(int id, string itemId)
        {
            var catalogMenu = _menuRepository.GetCatalogSubMenu(id, Settings.Default.SHOP_TYPE);
            var model = _mapper.Map<CatalogMenuModel>(catalogMenu);
            model.BreadCrumbs =
                _mapper.Map<CatalogMenuModel[]>(_menuRepository.GetCategoryPath(Settings.Default.CATALOG_ROOT, id,
                    Settings.Default.SHOP_TYPE));
            model.Name = catalogMenu.Name;
            model.H1 = model.H1 = string.IsNullOrWhiteSpace(catalogMenu.H1) ? catalogMenu.Name : catalogMenu.H1;
            model.Title = !string.IsNullOrWhiteSpace(catalogMenu.Title)
                ? catalogMenu.Title
                : $"Купить {catalogMenu.Name.ToLowerFirstWord()} в интернет-магазине UnixMart.ru в Москве, цены на {catalogMenu.Name}";
            model.Keywords = catalogMenu.Keywords;
            model.PageDescription = !string.IsNullOrWhiteSpace(catalogMenu.PageDescription)
                ? catalogMenu.PageDescription
                : $"Купить {catalogMenu.Name.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
            //model.TopGoodsItems = _goodsRepository.GetHitsNewSpecial(Settings.Default.SHOP_TYPE, id);
            model.SeoArticle = _articleRepository.GetSeoArticle(id, null, null);
            model.Canonical = $"/catalog/{itemId}";
            return model;
        }

        public GoodsListPageModel CreateGoodsList(int id, string itemId, string manufId, int? page, string sort,
            bool withSameLevel)
        {
            var company = _companyMenuRepository.GetCompany(manufId);
            var numOnPage = Constants.NUM_ON_PAGE;

            var total = 0;
            var catalog = _menuRepository.GetCatalogSubMenu(id, Settings.Default.SHOP_TYPE);
            var model = _mapper.Map<GoodsListPageModel>(catalog);
            model.ChildMenu = _mapper.Map<CatalogMenuModel[]>(
                _menuRepository.GetCatalogSubMenu(
                    catalog.ParentNodeId,
                    Settings.Default.SHOP_TYPE).ChildMenu);
            model.ChildMenu
                .Where(m => m.NodeId == id)
                .DefaultIfEmpty(new CatalogMenuModel()).Single()
                .OrdValue = 0;
            if (withSameLevel)
            {
                model.SameLevelMenu = _mapper.Map<CatalogMenuModel[]>(catalog.ChildMenu);
            }

            model.BreadCrumbs = _mapper.Map<CatalogMenuModel[]>(
                company.OID == Guid.Empty
                    ? _menuRepository.GetCategoryPath(
                        Settings.Default.CATALOG_ROOT, id,
                        Settings.Default.SHOP_TYPE)
                    : _menuRepository.GetManufCategoryPath(
                        Settings.Default.CATALOG_ROOT, id,
                        Settings.Default.SHOP_TYPE, company.Name));
            var filters = _catalogFilterFactory.Create(
                id,
                catalog.ParentNodeId,
                page,
                numOnPage,
                sort,
                itemId,
                manufId,
                _goodsRepository.GetManufacturers(Settings.Default.SHOP_TYPE, id),
                _goodsRepository.GetFilters(Settings.Default.SHOP_TYPE, id));
            filters.WithSameLevel = withSameLevel;
            var showEmpty = false;
            if (company.OID != Guid.Empty)
            {
                if (filters.Manufs.Any(m => m.OID == company.OID))
                {
                    filters.Manufs.Single(m => m.OID == company.OID).Checked = true;
                    filters.SelectedCheckBoxId =
                        $"Manufs_{filters.Manufs.IndexOf(filters.Manufs.Single(m => m.OID == company.OID))}__Checked";
                }
                else
                {
                    showEmpty = true;
                }
                filters.CompanyOID = company.OID;
            }
            var manufs = _catalogFilterFactory.CreateManufacturerFilter(filters.Manufs);
            model.Name = catalog.Name;
            model.H1 = $"{(string.IsNullOrWhiteSpace(catalog.H1) ? catalog.Name : catalog.H1)} {company.Name}".Trim();
            if ((page ?? 1) > 1) model.H1 += $" страница {page}";
            model.CanonicalLink = MVC.Catalog.Category(itemId, manufId, null);
            model.FilterModel = filters;
            model.GoodsItems = !showEmpty
                ? _goodsRepository.GetGoodsByCategory(
                    Settings.Default.SHOP_TYPE,
                    id,
                    ((page ?? 1) - 1)*numOnPage,
                    numOnPage,
                    sort,
                    manufs,
                    null,
                    out total)
                : new List<GoodsItem>();
            model.GoodsCount = total;
            model.Tags = _goodsRepository.GetCategoryTags(id);
            model.PagingModel = new GoodsPagingModel
            {
                Id = id,
                ItemId = itemId,
                ManufId = manufId,
                Page = page ?? 1,
                NumOnPage = numOnPage,
                Fragment = filters.GetUrlFragment(),
                Total = total,
                Sort = sort
            };
            model.SeoArticle = (page ?? 1) == 1 ? _articleRepository.GetSeoArticle(id, company.OID, null) : null;
            model.Title = !string.IsNullOrWhiteSpace(catalog.Title)
                ? catalog.Title
                : (company.Name != null
                    ? $"Купить {catalog.Name.ToLowerFirstWord()} {company.Name} в интернет-магазине UnixMart.ru в Москве, цены на {catalog.Name} {company.Name}"
                    : $"Купить {catalog.Name.ToLowerFirstWord()} в интернет-магазине UnixMart.ru в Москве, цены на {catalog.Name}");
            if ((page ?? 1) > 1) model.Title += $" страница {page}";
            model.Keywords = catalog.Keywords;
            model.PageDescription = !string.IsNullOrWhiteSpace(catalog.PageDescription)
                ? catalog.PageDescription
                : (company.Name != null
                    ? $"Купить {catalog.Name.ToLowerFirstWord()} {company.Name} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!"
                    : $"Купить {catalog.Name.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!");
            return model;
        }

        public GoodsListPageModel FilterList(CatalogFilterModel filter)
        {
            int total;
            var catalog = _menuRepository.GetCatalogSubMenu(filter.CatalogID, Settings.Default.SHOP_TYPE);
            var model = new GoodsListPageModel
            {
                Name = catalog.Name,
                GoodsItems = _goodsRepository.GetGoodsByCategory(
                    Settings.Default.SHOP_TYPE,
                    filter.CatalogID,
                    filter.NumOnPage*((filter.Page ?? 1) - 1),
                    filter.NumOnPage,
                    filter.TypeSort,
                    _catalogFilterFactory.CreateManufacturerFilter(filter.Manufs),
                    _catalogFilterFactory.CreateCustomFilter(
                        filter.CustomFilters,
                        _goodsRepository.GetFilters(
                            Settings.Default.SHOP_TYPE,
                            filter.CatalogID)),
                    out total),
                Tags = _goodsRepository.GetCategoryTags(filter.CatalogID),
                ChildMenu =
                    _mapper.Map<CatalogMenuModel[]>(
                        _menuRepository.GetCatalogSubMenu(filter.ParentNodeId, Settings.Default.SHOP_TYPE).ChildMenu),
                SameLevelMenu = filter.WithSameLevel ? _mapper.Map<CatalogMenuModel[]>(catalog.ChildMenu) : null,
                FilterModel = filter,
                PagingModel = new GoodsPagingModel
                {
                    Id = filter.CatalogID,
                    ItemId = filter.ItemId,
                    ManufId = filter.ManufId,
                    Page = filter.Page ?? 1,
                    NumOnPage = filter.NumOnPage,
                    Fragment = filter.GetUrlFragment(),
                    Total = total,
                    Sort = filter.TypeSort
                },
                SeoArticle =
                    (filter.Page ?? 1) == 1
                        ? _articleRepository.GetSeoArticle(filter.CatalogID, filter.CompanyOID, null)
                        : null
            };

            model.ChildMenu
                .Where(m => m.NodeId == filter.CatalogID)
                .DefaultIfEmpty(new CatalogMenuModel()).Single()
                .OrdValue = 0;
            return model;
        }

        public GoodsListPageModel CreateLandingPage(int id, string itemId, int? page, string sort)
        {
            var numOnPage = page.HasValue && page.Value == 0 ? -1 : Constants.NUM_ON_PAGE;
            //выясняем, нужно ли фильтровать каталог

            int total;
            var catalog = _landingPageRepository.GetItem(id);
            var model = new GoodsListPageModel
            {
                ChildMenu = _mapper.Map<CatalogMenuModel[]>(catalog.ChildMenu)
            };
            var parentsMenu = _landingPageRepository.GetParentsMenu(id);
            var categoryNode = parentsMenu.LastOrDefault(m => !string.IsNullOrEmpty(m.StringRepresentation))?.NodeId ??
                               0;
            model.BreadCrumbs = _mapper.Map<CatalogMenuModel[]>(parentsMenu);
            var allFilters = _goodsRepository.GetFilters(Settings.Default.SHOP_TYPE, categoryNode);
            var filters = _catalogFilterFactory.Create(
                id,
                categoryNode,
                page,
                numOnPage,
                sort,
                itemId,
                null,
                _goodsRepository.GetManufacturers(Settings.Default.SHOP_TYPE, categoryNode),
                allFilters);
            var manufsInPage = _landingPageRepository.GetPageManufacturers(id);
            var filtersInPage = _landingPageRepository.GetPageFilters(id);
            filters.Manufs.ToList().ForEach(m => m.Checked = manufsInPage.Any(mp => mp == m.OID));
            filters.CustomFilters.SelectMany(c => c.CustomFilters).ToList()
                .ForEach(m => m.Checked = filtersInPage.Any(i => i == m.Id));
            filters.LandingPage = true;
            model.Name = catalog.Name;
            model.H1 = string.IsNullOrWhiteSpace(catalog.H1) ? catalog.Name : catalog.H1;
            if ((page ?? 1) > 1) model.H1 += $" страница {page}";
            model.CanonicalLink = MVC.Catalog.Category(itemId, null, null);
            model.FilterModel = filters;
            model.GoodsItems = _landingPageRepository.GetGoodsList(
                Settings.Default.SHOP_TYPE,
                id,
                ((page ?? 1) - 1)*numOnPage,
                numOnPage,
                sort,
                null,
                _catalogFilterFactory.CreateCustomFilter(filters.CustomFilters, allFilters),
                out total);
            if (model.ChildMenu.Count > 0 && model.ChildMenu[0].GoodsCount == 0)
            {
                model.ChildMenu[0].GoodsCount = total;
            }
            model.GoodsCount = total;

            model.Tags = new List<Tag>();
            model.PagingModel = new GoodsPagingModel
            {
                Id = id,
                ItemId = itemId,
                ManufId = null,
                Page = page ?? 1,
                NumOnPage = numOnPage,
                Fragment = filters.GetUrlFragment(),
                Total = total,
                Sort = sort
            };
            model.SeoArticle = (page ?? 1) == 1 ? _landingPageRepository.GetSeoArticle(id, null) : null;
            model.Title = !string.IsNullOrWhiteSpace(catalog.Title)
                ? catalog.Title
                : $"Купить {catalog.Name.ToLowerFirstWord()} в интернет-магазине UnixMart.ru в Москве, цены на {catalog.Name}";
            if ((page ?? 1) > 1) model.Title += $" страница {page}";
            model.Keywords = catalog.Keywords;
            model.PageDescription = !string.IsNullOrWhiteSpace(catalog.PageDescription)
                ? catalog.PageDescription
                : $"Купить {catalog.Name.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
            return model;
        }

        public string GetLandingRedirect(CatalogFilterModel filter)
        {
            var url = _landingPageRepository.GetLandingUrl(filter.CatalogID);
            return $"{url}#{filter.GetUrlFragment()}";
        }

        public SimilarGoodsModel GetSimilarGoods(string categoryName, IList<int> nodeIds)
        {
            var result = new SimilarGoodsModel();
            IList<GoodsItem> sputnicGoods = new List<GoodsItem>();
            for (var i = nodeIds.Count - 1; i >= 0; i--)
            {
                var parentNode = nodeIds[i];
                sputnicGoods = _goodsRepository.GetSimilarGoods(Settings.Default.SHOP_TYPE,
                    Settings.Default.CATALOG_ROOT, parentNode, 0, 0, maxValueForList, maxValueForList);
                if (sputnicGoods.Count > 0) break;
            }
            if (sputnicGoods.Count == 0)
            {
                sputnicGoods = _goodsRepository.GetSimilarGoods(Settings.Default.SHOP_TYPE,
                    Settings.Default.CATALOG_ROOT, Settings.Default.CATALOG_ROOT, 0, 0, 0, maxValueForList);
            }
            result.Header = sputnicGoods.Count > 0
                ? $"Извините, все товары в категории &laquo;{categoryName}&raquo; закончились, но мы можем вам предложить другие популярные товары!"
                : $"Извините, все товары в категории &laquo;{categoryName}&raquo; закончились!";
            result.Items = sputnicGoods;
            return result;
        }
    }
}