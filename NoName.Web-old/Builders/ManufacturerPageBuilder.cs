﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Menu.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class ManufacturerPageBuilder : IManufacturerPageBuilder
    {

        private readonly IArticleRepository _articleRepository;
        private readonly ICompanyMenuRepository _companyMenuRepository;
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;

        public ManufacturerPageBuilder(
            IArticleRepository articleRepository,
            ICompanyMenuRepository companyMenuRepository,
            IMapper mapper,
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _companyMenuRepository = companyMenuRepository;
            _mapper = mapper;
            _menuRepository = menuRepository;
        }
        public ManufacturerMenuModel Create(string manufRepresentation, string categoryRepresentation)
        {
            var company = _companyMenuRepository.GetCompany(manufRepresentation);
            if (company.OID == Guid.Empty) throw new HttpException(404, "Page not found");
            var catNode = _menuRepository.FindNodeByObject(Settings.Default.CATALOG_ROOT, categoryRepresentation);
            var manufacturerMenu = _menuRepository.GetManufacturerSubMenu(catNode ??  Settings.Default.CATALOG_ROOT, Settings.Default.SHOP_TYPE, company.Id, manufRepresentation);
            var model = _mapper.Map<ManufacturerMenuModel>(manufacturerMenu);
            model.BreadCrumbs = _mapper.Map<ManufacturerMenuModel[]>(_menuRepository.GetManufacturerPath(
                Settings.Default.CATALOG_ROOT,
                catNode ?? Settings.Default.CATALOG_ROOT, 
                Settings.Default.SHOP_TYPE,
                company.Id, 
                manufRepresentation, company.Name));
            model.SeoArticle = _articleRepository.GetSeoArticle(catNode ?? 0, company.OID, null);
            model.H1 = catNode.HasValue ? $"{model.Name} {company.Name}" : company.Name;
            model.Title = categoryRepresentation != null ? $"Купить {model.Name.ToLowerFirstWord()} {company.Name} в интернет-магазине UnixMart.ru в Москве, цены на {model.Name.ToLowerFirstWord()} {company.Name}" :
                $"Каталог бытовой техники и электроники {company.Name} в интернет-магазине UnixMart.ru в Москве";
            model.PageDescription = categoryRepresentation != null ? $"Купить {model.Name.ToLowerFirstWord()} {company.Name} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!" :
                $"Каталог продукции {company.Name} интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
            return model;
        }
    }
}