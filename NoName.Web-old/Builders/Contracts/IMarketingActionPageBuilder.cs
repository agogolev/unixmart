﻿using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IMarketingActionPageBuilder
    {
        MarketingActionListModel CreateList(int? page);

        MarketingAction CreateItem(int id);
    }
}