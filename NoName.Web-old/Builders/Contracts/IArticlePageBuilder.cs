﻿using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IArticlePageBuilder
    {
        ArticleModel Create(string id);
    }
}