﻿using System.Threading.Tasks;
using NoName.Web.Models.Online;

namespace NoName.Web.Builders.Contracts
{
    public interface IOnlineBuilder
    {
        Task RegisterPaymentSession(RegisterPaymentRequest model);
        Task<GetOrderStatusResponse> GetOrderStatus(string paymentNumber);
    }
}