﻿using System;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IBasketPageBuilder
    {
        BasketViewModel Create();
        BasketViewModel Save(BasketViewModel model);
        Guid QuickSave(BuyOneClickModel model);
        void FillModel(BasketViewModel model);
        void Add(int goodsId);
        void Add(Guid goodsOID);
        void Delete(int ordValue);
    }
}