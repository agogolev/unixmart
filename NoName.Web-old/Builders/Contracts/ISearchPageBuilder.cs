﻿using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface ISearchPageBuilder
    {
        SearchPageModel Create(string search_query, int? id, int? page, string sort);
    }
}