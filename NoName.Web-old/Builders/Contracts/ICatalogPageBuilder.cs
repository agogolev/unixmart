﻿using System.Collections.Generic;
using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface ICatalogPageBuilder
    {
        CatalogMenuModel Create(int id, string itemId);
        GoodsListPageModel CreateGoodsList(int id, string itemId, string manufId, int? page, string sort, bool withSameLevel);
        GoodsListPageModel FilterList(CatalogFilterModel filter);
        GoodsListPageModel CreateLandingPage(int id, string itemId, int? page, string sort);
        string GetLandingRedirect(CatalogFilterModel filter);
        SimilarGoodsModel GetSimilarGoods(string categoryName, IList<int> nodeIds);
    }
}