using System.Collections;
using System.Collections.Generic;
using NoName.Domain;
using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IGoodsPageBuilder
    {
        GoodsItemPageModel CreateItem(int id);
        IList<GoodsItem> GetSputnikGoods(int selectedNode, int objectId, int amount, bool includeAbsent);
    }
}