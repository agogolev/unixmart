﻿using NoName.Web.Models;

namespace NoName.Web.Builders.Contracts
{
    public interface IManufacturerPageBuilder
    {
        ManufacturerMenuModel Create(string manufRepresentation, string categoryRepresentation);
    }
}