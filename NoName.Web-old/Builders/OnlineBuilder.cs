﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using NLog;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models.Online;

namespace NoName.Web.Builders
{
    public class OnlineBuilder : IOnlineBuilder
    {
        private static Logger _log = LogManager.GetCurrentClassLogger();
        private readonly Uri _urlOnlineRest;
        private readonly string _userName;
        private readonly string _password;
        private readonly string _errorUrl;
        private readonly string _resultUrl;

        public OnlineBuilder(string onlineUrl, string userName, string password, string errorUrl, string resultUrl)
        {
            _urlOnlineRest = new Uri(onlineUrl);
            _userName = userName;
            _password = password;
            _errorUrl = errorUrl;
            _resultUrl = resultUrl;
        }

        public async Task RegisterPaymentSession(RegisterPaymentRequest model)
        {
            using (HttpClient client = GetInstance())
            {
                LogResult("RegisterPaymentSession request", model);

                var builder = new UriBuilder(client.BaseAddress + "register.do");
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["userName"] = _userName;
                query["password"] = _password;
                query["orderNumber"] = model.OrderNumber.ToString();
                query["amount"] = model.PriceKop.ToString();
                query["returnUrl"] = $"{_resultUrl}";
                query["failUrl"] = _errorUrl;
                builder.Query = query.ToString();
                string url = builder.ToString();
                var result = await client.GetStringAsync(url);
                var jsonResult = JsonConvert.DeserializeObject<RegisterPaymentResponse>(result);
                LogResult("RegisterPaymentSession response", jsonResult);

                if (jsonResult.ErrorCode != 0) throw new Exception(jsonResult.ErrorMessage);
                model.PaymentNumber = jsonResult.OrderId;
                model.RedirectUrl = jsonResult.FormUrl;
            }
        }

        public async Task<GetOrderStatusResponse> GetOrderStatus(string paymentNumber)
        {
            using (HttpClient client = GetInstance())
            {
                LogResult<int?>("GetOrderStatus paymentNumber: " + paymentNumber, null);

                var builder = new UriBuilder(client.BaseAddress + "getOrderStatus.do");
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["userName"] = _userName;
                query["password"] = _password;
                query["orderId"] = paymentNumber;
                builder.Query = query.ToString();
                string url = builder.ToString();
                var result = await client.GetStringAsync(url);
                var jsonResult = JsonConvert.DeserializeObject<GetOrderStatusResponse>(result);
                LogResult("GetOrderStatus response", jsonResult);

                if (jsonResult.ErrorCode != 0) throw new Exception(jsonResult.ErrorMessage);
                return jsonResult;
            }
        }

        private HttpClient GetInstance()
        {
            HttpClient client = new HttpClient { BaseAddress = _urlOnlineRest };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            return client;
        }

        private void LogResult<TItem>(string header, TItem item)
        {
            StringBuilder sb = new StringBuilder(header + "\r\n");
            if (item != null)
            {
                var properties = item.GetType().GetProperties();
                foreach (var prop in properties)
                {
                    sb.AppendFormat("\t{0}: {1}\r\n", prop.Name, prop.GetValue(item));
                }
            }
            _log.Info(sb.ToString);
        }
    }
}