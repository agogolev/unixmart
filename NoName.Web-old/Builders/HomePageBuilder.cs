﻿using System.Collections.Generic;
using System.Linq;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Menu.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class HomePageBuilder : IHomePageBuilder
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IPromoBlockRepository _promoBlockRepository;

        public HomePageBuilder(IArticleRepository articleRepository, IPromoBlockRepository promoBlockRepository)
        {
            _articleRepository = articleRepository;
            _promoBlockRepository = promoBlockRepository;
        }

        public HomePageModel Create()
        {
            return new HomePageModel
            {
                PromoList = _promoBlockRepository.GetPromoList(Settings.Default.MainPagePromo, p => p.Description = p.Description.Replace("%URL%", p.Url)),
                SeoArticle = _articleRepository.GetNodeArticle(Settings.Default.MainPageArticle, a =>
                {
                    a.Title = a.Title.Replace("%PHONE%", Settings.Default.SHOP_PHONE_TEXT);
                    a.PageDescription = a.PageDescription.Replace("%PHONE%", Settings.Default.SHOP_PHONE_TEXT);
                    a.Keywords = a.Keywords.Replace("%PHONE%", Settings.Default.SHOP_PHONE_TEXT);
                })
            };
        }

        public IList<PromoBlock> AdvertList()
        {
            var result = _promoBlockRepository.GetPromoList(Settings.Default.AdvertMain, null);
            if (result.Count > 0 && result.Count % 3 != 0)
            {
                while (result.Count % 3 != 0)
                {
                    result.Add(new PromoBlock
                    {
                        Url = "#",
                        Image = new Image
                        {
                            OID = Settings.Default.BlankImage
                        }
                    });
                }
            }
            return result;
        }

        public IList<PromoBlock> AdvertWideList()
        {
            var result = _promoBlockRepository.GetPromoList(Settings.Default.AdvertMainWide, null);
            return result;
        }
    }
}