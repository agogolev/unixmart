﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Goods.Contracts;
using NoName.Repository.Menu.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Infrastructure.Factories;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class GoodsPageBuilder : IGoodsPageBuilder
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IDeliveryRepository _deliveryRepository;
        private readonly IGoodsRepository _goodsRepository;
        private readonly IMapper _mapper;
        private readonly IMenuRepository _menuRepository;

        public GoodsPageBuilder(
            IArticleRepository articleRepository,
            IDeliveryRepository deliveryRepository,
            IGoodsRepository goodsRepository,
            IMapper mapper,
            IMenuRepository menuRepository)
        {
            _articleRepository = articleRepository;
            _deliveryRepository = deliveryRepository;
            _goodsRepository = goodsRepository;
            _mapper = mapper;
            _menuRepository = menuRepository;
        }

        public GoodsItemPageModel CreateItem(int id)
        {
            GoodsItem goodsItem = _goodsRepository.GetGoodsItem(id, Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT);
            var breadcrumbs = _mapper.Map<List<CatalogMenuModel>>(_menuRepository.GetCategoryPath(Settings.Default.CATALOG_ROOT, goodsItem.CategoryId, Settings.Default.SHOP_TYPE));
            DeliveryInfo delivery = goodsItem.Delivery;
            if (goodsItem.InStock)
            {
                var basketContent = new[] { new BasketContent { Amount = 1, Price = 0 } };
                delivery.Items = _deliveryRepository.GetDeliveries(Settings.Default.SHOP_TYPE, delivery.Price, basketContent, delivery.InstantDelivery);
            }

            breadcrumbs.Add(new CatalogMenuModel
            {
                Name = goodsItem.FullName
            });

            var model = new GoodsItemPageModel
            {
                PageClass = "home category-01",
                GoodsItem = goodsItem,
                BreadCrumbs = breadcrumbs,
                DeliveryInfo = delivery,
                ShopPhone = Settings.Default.SHOP_PHONE_TEXT,
                InfoList = _articleRepository.GetLinkedArticles(Settings.Default.GoodsPageInfoList),
                Accessory = _goodsRepository.GetAccessoryGoods(id, Settings.Default.SHOP_TYPE),
                SameChars = _goodsRepository.GetSputnikGoods(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT, goodsItem.CategoryId, id, goodsItem.Price, 4),
                OnlinePayment = Settings.Default.OnlinePayment
            };

            return model;
        }

        public IList<GoodsItem> GetSputnikGoods(int selectedNode, int objectId, int amount, bool includeAbsent)
        {
            var allItems = _goodsRepository.GetAllGoodsByCategory(Settings.Default.SHOP_TYPE, selectedNode, includeAbsent);
            var currentGoods = (from item in allItems
                                where item.ObjectID == objectId
                                select item).FirstOrDefault();
            if (currentGoods == null) return null;
            var topItems = allItems.Where(item => item.ObjectID != currentGoods.ObjectID && item.Price >= currentGoods.Price && item.InPresent).OrderBy(item => item.Price).Take(amount).ToList();
            var bottomItems = allItems.Where(item => item.Price < currentGoods.Price && item.InPresent).OrderByDescending(item => item.Price).Take(amount).ToList();
            var items = bottomItems.Union(topItems).OrderBy(item => item.Price).ToList();
            return items;
        }
    }
}