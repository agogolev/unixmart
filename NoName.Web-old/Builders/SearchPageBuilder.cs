﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Web.Builders.Contracts;
using NoName.Web.Models;
using NoName.Web.Properties;

namespace NoName.Web.Builders
{
    public class SearchPageBuilder : ISearchPageBuilder
    {
        private readonly ISearchRepository _searchRepository;
        private readonly ISessionRepository _sessionRepository;

        public SearchPageBuilder(ISearchRepository searchRepository, ISessionRepository sessionRepository)
        {
            _searchRepository = searchRepository;
            _sessionRepository = sessionRepository;
        }

        public SearchPageModel Create(string search_query, int? id, int? page, string sort)
        {
            int numOnPage = page.HasValue && page.Value == 0 ? -1 : Constants.NUM_ON_PAGE;
            search_query = search_query?.Trim();
            IList<SearchedGoods> items;
            if (_sessionRepository.SearchQuery == search_query)
            {
                items = _sessionRepository.SearchResult;
            }
            else
            {
                items = _searchRepository.SearchResult(Settings.Default.SHOP_TYPE, Settings.Default.CATALOG_ROOT,
                    search_query);
                //запоминаем результат запроса в сессию
                _sessionRepository.SearchResult = items;
                _sessionRepository.SearchQuery = search_query;
            }

            var total = 0;
            var filteredItems = _searchRepository.SearchFilteredResult(items, numOnPage*((page ?? 1) - 1), numOnPage, sort, id ?? 0, out total);
            var model = new SearchPageModel
            {
                PagingModel = new SearchPagingModel
                {
                    Id = id,
                    SearchQuery = search_query,
                    Page = page ?? 1,
                    NumOnPage = numOnPage,
                    Total = total,
                    Sort = sort,
                    Items = filteredItems
                },
                Menu = _searchRepository.SearchResultCatalogs(_sessionRepository.SearchResult),
                PhoneIfEmpty = Settings.Default.SHOP_PHONE_TEXT
            };

            return model;
        }
    }
}