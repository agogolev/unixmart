﻿namespace NoName.Web.Models
{
	public static class Constants
	{
        public const int NUM_ON_PAGE = 12;
        public const int SelfDeliveryMoscow = 5;
        public const int DeliveryMoscow = 1;
        public const int DefaultPayment = 1;
    }
}