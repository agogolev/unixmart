﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class GoodsListPageModel
    {
        public int NodeId { get; set; }
        public string Name { get; set; }
        public string H1 { get; set; }
        public ActionResult CanonicalLink { get; set; }
        public IList<CatalogMenuModel> ChildMenu { get; set; }
        public CatalogFilterModel FilterModel { get; set; }
        public int GoodsCount { get; set; }
        public IList<GoodsItem> GoodsItems { get; set; }
        public GoodsPagingModel PagingModel { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
        public IList<CatalogMenuModel> BreadCrumbs { get; set; }
        public Article SeoArticle { get; set; }
        public IList<Tag> Tags { get; set; }
        public SimilarGoodsModel SimilarGoods { get; set; }
        public string CanonicalUrl { get; set; }
        public IList<CatalogMenuModel> SameLevelMenu { get; set; }
    }
}