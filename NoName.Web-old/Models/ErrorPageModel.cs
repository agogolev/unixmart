﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoName.Web.Models
{
    public class ErrorPageModel
    {
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDescription { get; set; }
        public string PhoneNumber { get; set; }
        public CatalogMenuModel Menu { get; set; }
    }
}