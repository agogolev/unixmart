﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoName.Web.Models
{
	public static class DocsTypeAssociation
	{
		public static string ClassNameAttribute(this string item)
		{
			if (item.StartsWith("инструкция", StringComparison.InvariantCultureIgnoreCase)) return "pdf";
			if (item.StartsWith("сертификат", StringComparison.InvariantCultureIgnoreCase)) return "rost";
			return "jpg";
		}
	}
}