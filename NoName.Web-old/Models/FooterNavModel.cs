﻿using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class FooterNavModel
    {
        public IList<InfoMenu> InfoMenu { get; set; }
        public IList<CatalogMenuModel> CatalogMenu { get; set; }
        public string ShopPhone { get; set; }
        public string WorkingTime { get; set; }
    }
}