﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoName.Web.Models.Online
{
    public class GetOrderStatusResponse
    {
        public string OrderId { get; set; }
        public string ApprovalCode { get; set; }
        public int OrderStatus { get; set; }
        public int OrderNumber { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}