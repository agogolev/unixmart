﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class ShowPriceModel
    {
        public GoodsItem Goods { get; set; }
        public bool ShowMeta { get; set; }
    }
}