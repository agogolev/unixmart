﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class SimilarGoodsModel
    {
        public string Header { get; set; }
        public IList<GoodsItem> Items { get; set; }
    }
}