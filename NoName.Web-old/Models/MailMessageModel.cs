﻿using System.Collections.Generic;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
	public class MailMessageModel
	{
		public string SiteUrl { get; set; }
		public HtmlString ShopPhone { get; set; }
		public HtmlString WorkTimeCaption { get; set; }
		public IList<NodeMenu> Menu { get; set; }
		public Article MailArticle { get; set; }
		public Order Item { get; set; }
	}
}