﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Web.Properties;

namespace NoName.Web.Models
{
	public static class UrlExtensions
	{
		public static string ToFullUrl(this string item)
		{
			return string.Format("{0}{1}", Settings.Default.SITE_URL, item);
		}
	}
}