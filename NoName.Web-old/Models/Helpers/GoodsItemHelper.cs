﻿using System.Text;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models.Helpers
{
    public static class GoodsItemHelper
    {
        public static HtmlString SpecialCaption(this GoodsItem item)
        {
            var sb = new StringBuilder();
            if (item.IsHit)
            {
                sb.Append("популярный товар, ");
            }

            if (item.IsNew)
            {
                sb.Append("новинка, ");
            }

            if (item.IsSale)
            {
                sb.Append("cпецпредложение, ");
            }

            if (sb.Length > 0)
            {
                sb.Length -= 2;
                return new HtmlString(sb.ToString().Substring(0, 1).ToUpper() + sb.ToString().Substring(1));
            }
            return new HtmlString("&nbsp;");
        }
    }
}