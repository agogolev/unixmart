﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class SearchPagingModel : PagingModel<GoodsItem>
    {
        public int? Id { get; set; }

        public string SearchQuery { get; set; }
    }
}