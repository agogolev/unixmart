﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class ArticleModel
    {
        public int SelectedId { get; set; }

        public Article Article { get; set; }

        public IList<InfoMenu> Menu { get; set; }
    }
}