﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            OfertaAccept = true;
        }

        public bool NeedCityInOrder { get; set; }

        public bool ShowAddress { get; set; }

        public bool OfertaAccept { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Укажите свой телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Укажите свой e-mail")]
        public string Email { get; set; }

        public string City { get; set; }

        public string StreetName { get; set; }

        public string House { get; set; }

        public string Flat { get; set; }

        public string Comments { get; set; }

        public bool IsOnlinePayment { get; set; }
        public string FullAddress
        {
            get
            {
                var sb = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(City)) sb.AppendFormat("{0}, ", City);
                if (!string.IsNullOrWhiteSpace(StreetName)) sb.AppendFormat("{0}, ", StreetName);
                if (!string.IsNullOrWhiteSpace(House)) sb.AppendFormat("д. {0}, ", House);
                if (!string.IsNullOrWhiteSpace(Flat)) sb.AppendFormat("кв. {0}, ", Flat);
                if (sb.Length > 0) sb.Length -= 2;
                return sb.ToString();
            }
        }

        public IList<InfoMenuModel> BreadCrumbs { get; set; }
    }
}