﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoName.Web.Models
{
	public class CatalogFilterModel
	{
        //public CatalogFilterModel()
        //{
        //    TypeSort = "up";
        //}
		public int CatalogID { get; set; }
        public Guid CompanyOID { get; set; }
        public int ParentNodeId { get; set; }
		public int? Page { get; set; }
		public int NumOnPage { get; set; }
		public string TypeSort { get; set; }
        public string ItemId { get; set; }
        public string ManufId { get; set; }
        public string SelectedCheckBoxId { get; set; }
        public int Total { get; set; }
	    public bool LandingPage { get; set; }
        public IList<ManufacturerMenuModel> Manufs { get; set; }
        public IList<CustomFilterMenuModel> CustomFilters { get; set; }
	    public bool WithSameLevel { get; set; }
		public string GetUrlFragment()
		{
			var sb = new StringBuilder();
            var i = 0;
		    Manufs?.ToList().ForEach(m =>
		    {
		        if (m.Checked) sb.AppendFormat("Manufs_{0}__Checked=True&", i);
		        i++;
		    });

		    if (CustomFilters != null)
			{
				for (var j = 0; j < CustomFilters.Count; j++)
				{
					var filter = CustomFilters[j];
					i = 0;
					filter.CustomFilters.ToList().ForEach(p =>
					{
						if (p.Checked) sb.AppendFormat("CustomFilters_{0}__CustomFilters_{1}__Checked=True&", j, i);
						i++;
					});
				}
			}
			if (sb.Length > 0) sb.Length -= 1;
			return sb.ToString();
		}
	}

	public class CustomFilterMenuModel
	{
		public Guid ParamType { get; set; }
		public string Name { get; set; }
		public IList<CustomFilterMenuItem> CustomFilters { get; set; }
		public bool NameLess
		{
			get { return Name != null && Name.ToLower() == "тип"; }
		}
	}

	public class CustomFilterMenuItem
	{
	    public int Id { get; set; }
		public string Name { get; set; }
		public int OrdValue { get; set; }
		public int ItemsCount { get; set; }
		public bool Checked { get; set; }
	}
}