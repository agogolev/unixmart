﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class SiteMapModel
    {
        public IList<CatalogMenu> CatalogMenu { get; set; }
        //public NodeMenu ActionMenu { get; set; }
        //public NodeMenu NewsMenu { get; set; }
        public NodeMenu InfoMenu { get; set; }
    }
}