﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoName.Domain;
using NoName.Extensions;

namespace NoName.Web.Models
{
    public class GoodsItemPageModel
    {
        public string PageClass { get; set; }
        public GoodsItem GoodsItem { get; set; }
        public IList<CatalogMenuModel> BreadCrumbs { get; set; }
        public DeliveryInfo DeliveryInfo { get; set; }
        public IList<Article> InfoList { get; set; }
        public string ShopPhone { get; set; }
        public IList<GoodsItem> Accessory { get; set; }
        public IList<GoodsItem> SameChars { get; set; }
        public string Title => !string.IsNullOrWhiteSpace(GoodsItem.Title)
            ? GoodsItem.Title
            : $"Купить {GoodsItem.FullName.ToLowerFirstWord()} в интернет-магазине UnixMart.ru в Москве";
        public string PageDescription => !string.IsNullOrWhiteSpace(GoodsItem.PageDescription)
            ? GoodsItem.PageDescription
            : $"Купить {GoodsItem.FullName.ToLowerFirstWord()} в каталоге интернет-магазина UnixMart.ru с доставкой по Москве и МО! Регулярные скидки и акции, лучшие цены и качество гарантируем!";
        public string Keywords => GoodsItem.Keywords;
        public bool OnlinePayment { get; set; }
    }
}