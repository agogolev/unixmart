﻿using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class HeaderModel
    {
        public string ShopPhone { get; set; }

        public string AlertMessage { get; set; }
        public IEnumerable<NodeMenu> Menu { get; set; } 
    }
}