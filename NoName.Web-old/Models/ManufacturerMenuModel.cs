﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using NoName.Domain;

namespace NoName.Web.Models
{
    public class ManufacturerMenuModel
    {
        public Image Image { get; set; }
        public int ManufId { get; set; }
        public Guid OID { get; set; }
        public string ManufRepresentation { get; set; }
        public int NodeId { get; set; }
        public string CategoryRepresentation { get; set; }
        public string Name { get; set; }
        public string Css1 { get; set; }
        public string Css2 { get; set; }
        public ActionResult Action => HasChild ? MVC.Manufacturer.List(ManufRepresentation, CategoryRepresentation) : MVC.Catalog.Category(CategoryRepresentation, ManufRepresentation, null);
        public bool HasChild { get; set; }
        public bool Checked { get; set; }
        public IList<ManufacturerMenuModel> ChildMenu { get; set; }
        public int GoodsCount { get; set; }
        public IList<GoodsItem> TopGoodsItems { get; set; }
        public string H1 { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
        public IList<ManufacturerMenuModel> BreadCrumbs { get; set; }
        public SimilarGoodsModel SimilarGoods { get; set; }
        public Article SeoArticle { get; set; }
    }
}