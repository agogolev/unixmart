﻿using System.Web;
using NoName.Domain;

namespace NoName.Web.Models
{
	public static class MarketingActionExtensions
	{
		public static HtmlString Period (this MarketingAction item)
		{
			return new HtmlString(string.Format("{0}{1}",
				item.DateBegin.ToString("dd.MM.yyyy"),
				item.DateEnd.HasValue ? string.Format("&nbsp;&mdash;&nbsp;{0:dd.MM.yyyy}", item.DateEnd.Value) : ""));
		}
	}
}