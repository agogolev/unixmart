﻿using System.Collections.Generic;
using NoName.Domain;
using Shouldly;
using Xunit;

namespace NoName.Repository.UnitTests
{
    public class DeliveryCalcServiceTests
    {
        private decimal selfDeliveryUnitPrice = 180M;

        [Fact]
        public void GetDeliveries_fiveItemsSelfDelivery_ReturnAppropriatePriceInList()
        {
            var info = new DeliveryInfo { PriceSelf = selfDeliveryUnitPrice};
            int amountInBasket = 5;
            var infoItems = new []
            {
                new DeliveryInfoItem
                {
                    DeliveryType = DeliveryConstants.SelfDeliveryIds[0],
                    Price = selfDeliveryUnitPrice
                }
            };


            var deliveryCalcService = new DeliveryCalcService();

            var items = deliveryCalcService.GetDeliveries(infoItems, info, new List<BasketContent>());
            items.Count.ShouldBe(1);
            items[0].Price.ShouldBe(amountInBasket * selfDeliveryUnitPrice);
        }
    }
}
