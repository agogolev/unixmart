﻿using Xunit;

namespace NoName.Repository.UnitTests
{
    public class YandexMarketRepositoryUnitTests
    {
        [Fact]
        public void Cut_EmptyString_ReturnEmpty()
        {
            var source = "";
            var maxChar = 100;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal("", dest);
        }

        [Fact]
        public void Cut_Null_ReturnEmpty()
        {
            string source = null;
            var maxChar = 100;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal("", dest);
        }

        [Fact]
        public void Cut_10CharStringMaxCharTheSame_ReturnSelf()
        {
            string source = "1234567890";
            var maxChar = 10;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal(source, dest);
        }

        [Fact]
        public void Cut_20CharStringMaxCharTheSame_ReturnSelf()
        {
            string source = "Духовка: электрическая, ";
            var maxChar = 20;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal("Духовка: электрическая ...", dest);
        }

        [Fact]
        public void Cut_22CharStringMaxChar20_ReturnSelf()
        {
            string source = "Духовка: электрическая:., 12";
            var maxChar = 20;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal("Духовка: электрическая ...", dest);
        }

        [Fact]
        public void Cut_11CharStringWithoutPatternMaxChar10_ReturnEmpty()
        {
            string source = "1234567890";
            var maxChar = 10;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal(source, dest);
        }

        [Fact]
        public void Cut_SmallString_ReturnSelf()
        {
            var source = "123, 34444, ggggg";
            var maxChar = 100;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal(source, dest);
        }

        [Fact]
        public void Cut_20charstringWithSpaces_Return1234_67890()
        {
            string source = "1234 67890 234567890";
            var maxChar = 10;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal("1234 67890 ...", dest);
        }

        [Fact]
        public void Cut_20charstringWithSpaces_Return1234()
        {
            string source = "1234 67890 234567890";
            var maxChar = 8;
            var dest = YandexMarketRepository.Cut(source, maxChar);

            Assert.Equal("1234 ...", dest);
        }
    }
}
