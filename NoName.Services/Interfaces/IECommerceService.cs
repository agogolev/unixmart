using System.Web;
using NoName.Domain;

namespace NoName.Services.Interfaces
{
	public interface IECommerceService
	{
		HtmlString GenerateScript(Order order);
        HtmlString GeneratePurchaseScript(Order order);
    }
}