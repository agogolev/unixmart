﻿using System.Threading.Tasks;

namespace NoName.Services.Interfaces
{
    public interface IActionpayService
    {
        void RequestActionpay(string actionpayId, int orderNum, decimal price, int targetId);
    }
}