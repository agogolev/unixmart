﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using NoName.Services.Interfaces;
using Tavis.UriTemplates;

namespace NoName.Services
{
    class ActionpayService : IActionpayService
    {
        private string actionpayUrl = "http://x.actionpay.ru/";
        public bool Responce { get; set; }
        public void RequestActionpay(string actionpayId, int orderNum, decimal price, int targetId)
        {
            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken =>
            {
                using (var client = new HttpClient())
                {
                    var url = new UriTemplate(actionpayUrl + "ok/" + targetId + ".png{?actionpay,apid,price}")
                    .AddParameters(new { actionpay = actionpayId, apid = orderNum, price = price.ToString("0") })
                    .Resolve();
                    client.GetAsync(url, cancellationToken);
                }
            });
        }
    }
}