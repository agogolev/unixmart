﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using NoName.Domain;
using NoName.Services.Interfaces;

namespace NoName.Services
{
	public class ECommerceService : IECommerceService
	{
		public HtmlString GenerateScript(Order order)
		{
			var culture = System.Globalization.CultureInfo.InvariantCulture;
			Thread.CurrentThread.CurrentCulture = culture;
			var builder = new StringBuilder();

			if (order != null && order.OID != Guid.Empty)
			{
				builder.Append("<script type=\"text/javascript\">\r\n");
				builder.Append("\tdataLayer = [{\r\n");
				builder.AppendFormat("\t\t\"transactionId\" : {0}, \r\n", order.OrderNum);
				builder.AppendFormat("\t\t\"transactionTotal\" : {0:0}, \r\n", order.TotalPrice);
				builder.AppendFormat("\t\t\"transactionShipping\" : {0:0}, \r\n", order.DeliveryPrice);
				builder.AppendFormat("\t\t\"tranShippingMethod\" : \"{0}\", \r\n", order.DeliveryName);
				builder.Append("\t\t\"transactionCurrency\" : \"RUB\", \r\n");
				builder.Append("\t\t\"transactionProducts\": [\r\n");
				foreach (var item in order.Items)
				{
					builder.Append("\t\t\t{\r\n");
					builder.AppendFormat("\t\t\t\t\"id\" : {0},\r\n", item.ObjectID);
					builder.AppendFormat("\t\t\t\t\"name\" : \"{0}\",\r\n", item.FullName);
					builder.AppendFormat("\t\t\t\t\"category\" : \"{0}\",\r\n", item.CategoryFullName);
					builder.AppendFormat("\t\t\t\t\"price\" : {0:0},\r\n", item.Price);
					builder.AppendFormat("\t\t\t\t\"quantity\" : {0},\r\n", item.Amount);
					builder.Append("\t\t\t}\r\n");
				}
				builder.Append("\t\t]\r\n");
				builder.Append("\t}]\r\n");
				builder.Append("</script>\r\n");

			}
			else
			{
				builder.Append("<script type=\"text/javascript\">\r\n");
				builder.Append("\tdataLayer = []\r\n");
				builder.Append("</script>\r\n");
			}
			return new HtmlString(builder.ToString());
		}

	    public HtmlString GeneratePurchaseScript(Order order)
	    {
            var culture = System.Globalization.CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentCulture = culture;
            var builder = new StringBuilder();

            if (order != null && order.OID != Guid.Empty)
            {
                builder.Append("<script type=\"text/javascript\">\r\n");
                builder.Append("\tdataLayer = [{\r\n");
                builder.Append("\t\t\'ecommerce': {\r\n");
                builder.Append("\t\t\t\'purchase': {\r\n");
                builder.Append("\t\t\t\t\'actionField': {\r\n");
                builder.AppendFormat("\t\t\t\t\t\'id\' : {0}, \r\n", order.OrderNum);
                builder.AppendFormat("\t\t\t\t\t\'revenue\' : {0:0}, \r\n", order.TotalPrice);
                builder.AppendFormat("\t\t\t\t\t\'shipping\' : {0:0}, \r\n", order.DeliveryPrice);
                builder.Append("\t\t\t\t},\r\n");
                builder.Append("\t\t\t\t\'products\': [\r\n");
                foreach (var item in order.Items)
                {
                    builder.Append("\t\t\t\t\t{\r\n");
                    builder.AppendFormat("\t\t\t\t\t\t\'name\' : \'{0}\',\r\n", item.FullName);
                    builder.AppendFormat("\t\t\t\t\t\t\'id\' : {0},\r\n", item.ObjectID);
                    builder.AppendFormat("\t\t\t\t\t\t\'price\' : {0:0},\r\n", item.Price);
                    builder.AppendFormat("\t\t\t\t\t\t\'brand\' : \'{0}\',\r\n", item.ManufacturerName);
                    builder.AppendFormat("\t\t\t\t\t\t\'category\' : \'{0}\',\r\n", item.CategoryFullName);
                    builder.AppendFormat("\t\t\t\t\t\t\'quantity\' : {0},\r\n", item.Amount);
                    if (item != order.Items.Last())
                    {
                        builder.Append("\t\t\t\t\t},\r\n");
                    }
                    else
                    {
                        builder.Append("\t\t\t\t\t}\r\n");
                    }
                }
                builder.Append("\t\t\t\t]\r\n");
                builder.Append("\t\t\t}\r\n");
                builder.Append("\t\t}\r\n");
                builder.Append("\t}]\r\n");
                builder.Append("</script>\r\n");

            }
            else
            {
                builder.Append("<script type=\"text/javascript\">\r\n");
                builder.Append("\tdataLayer = [{}]\r\n");
                builder.Append("</script>\r\n");
            }
            return new HtmlString(builder.ToString());
        }
	}
}
