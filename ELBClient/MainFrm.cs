﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using ColumnMenuExtender.Forms;
using ELBClient.AuthorizeProvider;
using ELBClient.Classes;
using ELBClient.Forms.Banners;
using ELBClient.Forms.Dialogs;
using ELBClient.Forms.Guides;
using ELBClient.Forms.Load;
using ELBClient.Forms.Reports;
using MetaData;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Constants = ELBExpImp.Constants;
using ExceptionISM = ELBClient.AuthorizeProvider.ExceptionISM;

namespace ELBClient
{
    /// <summary>
    ///     Summary description for Form1.
    /// </summary>
    public partial class MainFrm : GenericForm, IfmMain
    {
        private const int ErrorFileNotFound = 2;
        private const int ErrorAccessDenied = 5;
        private static readonly ArrayList _groups = new ArrayList();

        private AuthorizeProvider.AuthorizeProvider _authProvider;
        private bool _connectionStatus = true;

        private IContainer components;

        public MainFrm()
        {
            InitializeComponent();
            MainForm = this;
        }

        public string ThemeKey
        {
            get
            {
                return
                    Regex.Replace(string.Format("Theme_{0}", ConfigurationManager.AppSettings["connection"].ToUpper()),
                        @"\W", "");
            }
        }

        public bool HasWholesale { get; set; }
        public bool IsDiscount { get; set; }
        public IList<ShopMenu> Shops { get; set; }

        private void TimerTick(object sender, EventArgs e)
        {
            ShowCurrentTime();
        }

        private void ShowCurrentTime()
        {
            sbStatus.Panels[2].Text = DateTime.Now.ToString("dd MMMM yyy");
            sbStatus.Panels[3].Text = DateTime.Now.ToString("HH:mm");
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            Text = "Система управления контентом сайта: " + ConfigurationManager.AppSettings["connection"].ToUpper();
            LoadCurrentTheme();
            CreateMenuShops();
            DisableMenu(); // с самого начала всё отключено!
            ShowCurrentTime();
            pingTimer.Enabled = false;
            ShowLogin();
            //Test fm = new Test { MdiParent = this };
            //fm.Shows();
        }

        private void CreateMenuShops()
        {
            Shops = new List<ShopMenu>
            {
                new ShopMenu
                {
                    ShopType = 1,
                    Menus = new List<RadMenuItem>
                    {
                        LoadMoscowMenu
                    }
                },
                new ShopMenu
                {
                    ShopType = 2,
                    Menus = new List<RadMenuItem>
                    {
                        LoadSpbMenu
                    }
                },
            };
        }

        private void PingTimerTick(object sender, EventArgs e)
        {
            pingTimer.Enabled = false;

            if (_authProvider == null)
                _authProvider = ServiceUtility.AuthorizeProvider;

            try
            {
                var result = _authProvider.Ping();
                if (!_connectionStatus && result == "Pong")
                {
                    ShowWarning("Соединение с сервером восстановлено");
                    _connectionStatus = true;
                }
            }
            catch
            {
                if (_connectionStatus)
                {
                    _connectionStatus = false;
                    ShowWarning("Соединение с сервером потеряно");
                }
            }

            pingTimer.Enabled = true;
        }

        private void ShowLogin()
        {
            AuthorizeResponse ar = null;
            ExceptionISM ex = null;
            _authProvider = ServiceUtility.AuthorizeProvider;
            var s = Assembly.GetExecutingAssembly().GetName().Version.Major + "." +
                    Assembly.GetExecutingAssembly().GetName().Version.Minor;
            string log, pas;
#if(DEBUG)
            log = "ag";
            pas = "qqq";

#else
            var fm = new Login();
            if (fm.ShowDialog(this) == DialogResult.OK)
            {
                log = fm.edLogin.Text;
                pas = fm.edPassword.Text;
            }
            else return;
#endif

            try
            {
                ar = _authProvider.Authorize(log, pas, s, out ex);
            }
            catch (SoapException err)
            {
                ShowSoapException(err);
            }

            if (ex != null)
            {
                if (ex.LiteralExceptionType == "MetaData.CCVException")
                {
                    ShowWarning(
                        "Соединение с сервером не может быть установлено! Ваша версия клиента устарела.\nСейчас будет запущена система автоматического обновления.");
                    UpdateClient();
                }
                else ShowError(ex.LiteralMessage);
            }
            else if (ar == null || ar.Ticket == null || ar.Ticket.Trim() == "")
            {
//если вернулось null
                ShowError("Не удалось получить билет");
            }
            else
            {
//всё ОК
                var data = ar.Ticket.Split(',');
                _authProvider.SecurityHeaderValue.Ticket = data[0];
                Manager = new Guid(data[1]);
                ManagerName = data[2];
                GetGroupMembership();
                DetermineWholesalePossibility();
                DetermineDiscountPossibility();
                ModifyMainMenu();

                LoginMenu.Enabled = false;
                LogoutMenu.Enabled = true;
                EnableMenu(); // если всё нормально - всё включаем
                //LoadAccessRights("stock.fmMain");

                if (ar.Info != null && ar.Info.Trim() != "")
                {
                    var fmvt = new ViewTicket(ar.Info, "Системное сообщение", SystemIcons.Information);
                    fmvt.ShowDialog(this);
                }
            }
        }

        private void GetGroupMembership()
        {
            _groups.Clear();
            var prov = ServiceUtility.ListProvider;
            var ds = new DataSetISM(prov.GetDataSetSql("exec upGroupMember '" + Manager + "'"));
            foreach (DataRow dr in ds.Table.Rows)
            {
                _groups.Add(dr["groupOID"]);
            }
        }

        public static bool IsGroupMember(Guid groupOID)
        {
            return _groups.Cast<Guid>().Any(OID => OID == groupOID);
        }

        private void ModifyMainMenu()
        {
            WindowMenu.Visibility = ElementVisibility.Visible;
            foreach (
                var mi in
                    MainMenu.Items.Where(menu => (menu != FileMenu) && (menu != WindowMenu) && (menu != VisualThemeMenu))
                )
            {
                mi.Visibility = ElementVisibility.Collapsed;
            }

            if (IsGroupMember(Constants.GoodsDescriptorOID))
            {
                SpravMenu.Visibility = ElementVisibility.Visible;
                BinaryDataMenu.Visibility = ElementVisibility.Visible;
                GoodsMenu.Visibility = ElementVisibility.Visible;
                GoodsItemMenu.Visibility = IsDiscount ? ElementVisibility.Visible : ElementVisibility.Collapsed;

                LoadMenu.Visibility = ElementVisibility.Visible;
                LoadProductTypeMenu.Visibility = ElementVisibility.Visible;
            }

            if (IsGroupMember(Constants.ContentManagerOID))
            {
                SpravMenu.Visibility = ElementVisibility.Visible;
                ArticleMenu.Visibility = ElementVisibility.Visible;
                ThemeMenu.Visibility = ElementVisibility.Visible;
                DisableExpressDeliveryMenu.Visibility = ElementVisibility.Visible;
            }

            if (IsGroupMember(Constants.SellerOID))
            {
                SpravMenu.Visibility = ElementVisibility.Visible;
                BinaryDataMenu.Visibility = ElementVisibility.Visible;
                GoodsMenu.Visibility = ElementVisibility.Visible;
                GoodsItemMenu.Visibility = IsDiscount ? ElementVisibility.Visible : ElementVisibility.Collapsed;
                OrderMenu.Visibility = ElementVisibility.Visible;
                WebConfigMenu.Visibility = ElementVisibility.Visible;
                if (HasWholesale) WholesaleOrderMenu.Visibility = ElementVisibility.Visible;
            }

            if (IsGroupMember(Constants.PriceManagerOID))
            {
                SpravMenu.Visibility = ElementVisibility.Visible;
                AddPriceMenu.Visibility = ElementVisibility.Visible;
                BinaryDataMenu.Visibility = ElementVisibility.Visible;
                GoodsMenu.Visibility = ElementVisibility.Visible;
                GoodsItemMenu.Visibility = IsDiscount ? ElementVisibility.Visible : ElementVisibility.Collapsed;
                MasterCatalogMenu.Visibility = ElementVisibility.Visible;
                OrderMenu.Visibility = ElementVisibility.Visible;
                if (HasWholesale) WholesaleOrderMenu.Visibility = ElementVisibility.Visible;
                ThemeMenu.Visibility = ElementVisibility.Visible;

                CommodityGroupMenu.Visibility = ElementVisibility.Visible;
                ActionMenu.Visibility = ElementVisibility.Visible;
                BannerRootMenu.Visibility = ElementVisibility.Visible;

                TepmlatesWorkMenu.Visibility = ElementVisibility.Visible;
                LoadParamsMenu.Visibility = ElementVisibility.Visible;
                ModifyTemplateMenu.Visibility = ElementVisibility.Visible;

                LoadMenu.Visibility = ElementVisibility.Visible;
                //LoadGoodsMenu.Visibility = ElementVisibility.Visible;
                LoadKeywordsMenu.Visibility = ElementVisibility.Visible;
                LoadProductTypeMenu.Visibility = ElementVisibility.Visible;
                LoadImagesMenu.Visibility = ElementVisibility.Visible;
                LoadMoscowMenu.Visibility = ElementVisibility.Visible;
                LoadSpbMenu.Visibility = ElementVisibility.Visible;

                SiteMenu.Visibility = ElementVisibility.Visible;
                ObjectTreeMenu.Visibility = ElementVisibility.Visible;
            }

            if (IsGroupMember(Constants.AdminOID))
            {
                SpravMenu.Visibility = ElementVisibility.Visible;
                AddPriceMenu.Visibility = ElementVisibility.Visible;
                ArticleMenu.Visibility = ElementVisibility.Visible;
                BinaryDataMenu.Visibility = ElementVisibility.Visible;
                CommentMenu.Visibility = ElementVisibility.Visible;
                CompanyMenu.Visibility = ElementVisibility.Visible;
                GoodsMenu.Visibility = ElementVisibility.Visible;
                GoodsItemMenu.Visibility = IsDiscount ? ElementVisibility.Visible : ElementVisibility.Collapsed;
                MasterCatalogMenu.Visibility = ElementVisibility.Visible;
                OrderMenu.Visibility = ElementVisibility.Visible;
                if (HasWholesale) WholesaleOrderMenu.Visibility = ElementVisibility.Visible;
                PeopleMenu.Visibility = ElementVisibility.Visible;
                PromoMenu.Visibility = ElementVisibility.Visible;
                ThemeMenu.Visibility = ElementVisibility.Visible;
                TreeMenu.Visibility = ElementVisibility.Visible;
                DisableExpressDeliveryMenu.Visibility = ElementVisibility.Visible;
                DefaultTypeMenu.Visibility = ElementVisibility.Visible;
                BrowseDefaultTitleMenu.Visibility = ElementVisibility.Visible;

                CommodityGroupMenu.Visibility = ElementVisibility.Visible;
                ActionMenu.Visibility = ElementVisibility.Visible;
                WebConfigMenu.Visibility = ElementVisibility.Visible;
                BannerRootMenu.Visibility = ElementVisibility.Visible;

                TepmlatesWorkMenu.Visibility = ElementVisibility.Visible;
                LoadParamsMenu.Visibility = ElementVisibility.Visible;
                ModifyTemplateMenu.Visibility = ElementVisibility.Visible;

                LoadMenu.Visibility = ElementVisibility.Visible;
                //LoadGoodsMenu.Visibility = ElementVisibility.Visible;
                LoadKeywordsMenu.Visibility = ElementVisibility.Visible;
                LoadImagesMenu.Visibility = ElementVisibility.Visible;
                LoadMoscowMenu.Visibility = ElementVisibility.Visible;
                LoadSpbMenu.Visibility = ElementVisibility.Visible;
                LoadProductTypeMenu.Visibility = ElementVisibility.Visible;
                ListJobLogMenu.Visibility = ElementVisibility.Visible;
                UploadGoods4YandexMenu.Visibility = ElementVisibility.Visible;

                SiteMenu.Visibility = ElementVisibility.Visible;
                ObjectTreeMenu.Visibility = ElementVisibility.Visible;
                LinkReviewMenu.Visibility = ElementVisibility.Visible;
            }
            ConfigureShops();
        }

        private void ConfigureShops()
        {
            new DataSetISM(ServiceUtility.ListProvider.GetDataSetSql("SELECT * FROM t_TypeShop")).Table
                .AsEnumerable().ToList().ForEach(row =>
                {
                    var shopType = row.Field<int>("shopType");
                    var isActive = row.Field<bool>("active");
                    //Shops.Single(s => s.ShopType == shopType).Menus.ToList().ForEach(m => m.Visible = isActive);
                    //if(!isActive)
                    //    Shops.Single(s => s.ShopType == shopType).Menus.ToList().ForEach(m => m.Visible = false);
                    var item = Shops.Single(s => s.ShopType == shopType);
                    item.RootNode = row.Field<int>("themeRootNode");
                    item.SiteUrl = row.Field<string>("address");
                    item.IsActive = isActive;
                });
            Shops.Where(s => !s.IsActive)
                .SelectMany(s => s.Menus)
                .ToList()
                .ForEach(m => m.Visibility = ElementVisibility.Collapsed);
        }


        private void UpdateClient()
        {
            try
            {
                Process.Start("AutoUpdate.exe", ConfigurationManager.AppSettings["connection"]);
            }
            catch (Win32Exception e)
            {
                if (e.NativeErrorCode == ErrorFileNotFound)
                    ShowError("Программа автоматического обновления не найдена!");
                else if (e.NativeErrorCode == ErrorAccessDenied)
                    ShowError(
                        "У Вас недостаточно прав для запуска автоматического обновления!\nСвяжитесь с администратором.");
            }
            finally
            {
                //Close process by sending a close message to its main window
                Process.GetCurrentProcess().CloseMainWindow();
                //Free resources associated with process.
                Process.GetCurrentProcess().Close();
            }
        }

        private void fmMain_Closing(object sender, CancelEventArgs e)
        {
            /*			if (MessageBox.Shows("Выйти из программы?", "Торговая система", 
							MessageBoxButtons.YesNo, MessageBoxIcon.Question, 
							MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;*/
            CloseSession(); //текущую закрываем сессию
        }

        private void CloseSession() //функция просто закрывает текущую сессию и закрывает все окна (это оч важно)!
        {
            if (CurrentTicket != "")
            {
                for (var i = MdiChildren.Length - 1; i >= 0; i--)
                    MdiChildren[i].Close();
                OpenedForms.Clear();

                _authProvider.CloseSession();
                _authProvider = null;
            }
        }

        private void CloseMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoginMenu_Click(object sender, EventArgs e)
        {
            ShowLogin();
        }

        private void LogoutMenu_Click(object sender, EventArgs e)
        {
            CloseSession(); //текущую закрываем сессию
            DisableMenu();
            LoginMenu.Enabled = true;
            LogoutMenu.Enabled = false;
        }

        private void DisableMenu()
            //просто отключает все минюшки кроме первой (в первой находятся отключиться\подключиться)
        {
            for (var i = 1; i < MainMenu.Items.Count - 1; i++)
            {
                MainMenu.Items[i].Visibility = ElementVisibility.Collapsed;
                //foreach (var item in MainMenu.Items[i])
                //{
                //    item.Visible = false;
                //}
            }
        }

        private void EnableMenu()
            //просто включает все минюшки кроме первой (в первой находятся отключиться\подключиться)
        {
            for (var i = 1; i < MainMenu.Items.Count; i++)
                MainMenu.Items[i].Enabled = true;
        }

        private void ItemTHMenu_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ItemTVMenu_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void ItemCascadeMenu_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void BinaryDataMenu_Click(object sender, EventArgs e)
        {
            var form = new ListBinaryData {MdiParent = this};
            form.Show();
        }

        private void ThemeMenu_Click(object sender, EventArgs e)
        {
            var form = new ListTheme {MdiParent = this};
            form.Show();
        }

        private void ArticleMenu_Click(object sender, EventArgs e)
        {
            var form = new ListArticle {MdiParent = this};
            form.Show();
        }

        private void PeopleMenu_Click(object sender, EventArgs e)
        {
            var form = new ListPeople {MdiParent = this, HasWholesale = HasWholesale};
            form.Show();
        }

        private void CompanyMenu_Click(object sender, EventArgs e)
        {
            var form = new ListContraAgents {MdiParent = this};
            form.Show();
        }

        private void PromoMenu_Click(object sender, EventArgs e)
        {
            var form = new ListPromo {MdiParent = this};
            form.Show();
        }

        private void GoodsMenu_Click(object sender, EventArgs e)
        {
            var form = new ListGoods {MdiParent = this, IsDiscount = IsDiscount};
            form.Show();
        }

        private void ObjectTreeMenu_Click(object sender, EventArgs e)
        {
            var form = new EditMasterTreeView
            {
                ParentNode = Constants.MASTER_NODE_ROOT,
                ShopType = 1,
                Text = @"Мастер каталог",
                MdiParent = this
            };

            form.Show();
        }


        private void TreeMenu_Click(object sender, EventArgs e)
        {
            var form = new ListTree {MdiParent = this};
            form.Show();
        }

        private void OrderMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListOrder {MdiParent = this, HasWholesale = HasWholesale, IsDiscount = IsDiscount};
            if (HasWholesale) fm.ShowWholesale = false;
            if (IsGroupMember(Constants.AdminOID)) fm.IsAdmin = true;
            if (IsGroupMember(Constants.SellerOID)) fm.IsOperator = true;
            fm.Show();
        }

        private void WholesaleOrderMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListOrder {MdiParent = this, HasWholesale = HasWholesale, IsDiscount = IsDiscount};
            if (HasWholesale) fm.ShowWholesale = true;
            if (IsGroupMember(Constants.AdminOID)) fm.IsAdmin = true;
            if (IsGroupMember(Constants.SellerOID)) fm.IsOperator = true;
            fm.Show();
        }


        private void CommentMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListComment {MdiParent = this};
            fm.Show();
        }

        private void ExportOrderMenu_Click(object sender, EventArgs e)
        {
            var fm = new ExportOrders {MdiParent = this};
            fm.Show();
        }

        private void LinkReviewMenu_Click(object sender, EventArgs e)
        {
            var fm = new LinkReviews {MdiParent = this};
            fm.Show();
        }

        private void ProcessTochkaMenu_Click(object sender, EventArgs e)
        {
            var fm = new FindInTochka {MdiParent = this};
            fm.Show();
        }

        //private void ProcessParams_Click(object sender, EventArgs e)
        //{
        //    var fm = new ProcessTochkaDb { MdiParent = this };
        //    fm.Show();
        //}

        private void LoadParamsMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadParams {MdiParent = this};
            fm.Show();
        }

        private void MasterCatalogMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListMasterCatalog {MdiParent = this};
            fm.Show();
        }

        private void LoadImagesMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadImages {MdiParent = this};
            fm.Show();
        }

        private void AddPriceMenu_Click(object sender, EventArgs e)
        {
            var fm = new EditAddPrices {MdiParent = this};
            fm.Show();
        }

        private void ModifyTemplateMenu_Click(object sender, EventArgs e)
        {
            var fm = new EditTemplates {MdiParent = this};
            fm.Show();
        }

        private void LoadBidsMoscowMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadBids {MdiParent = this, ShopType = 1, Tag = 1, ShopName = "Москва"};
            fm.Show();
        }

        private void LoadBidsSpbMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadBids {MdiParent = this, ShopType = 2, Tag = 2, ShopName = "Санкт-Петербург"};
            fm.Show();
        }

        private void LoadBidsNskMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadBids {MdiParent = this, ShopType = 3, Tag = 3, ShopName = "Новосибирск"};
            fm.Show();
        }

        private void LoadSitePricesMoscowMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadSitePrice {MdiParent = this, ShopType = 1, Tag = 1, ShopName = "Москва"};
            fm.Show();
        }

        private void DisableExpressDeliveryMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListDisableExpressDelivery {MdiParent = this};
            fm.Show();
        }

        private void ExportOrdersFromSVMoscowMenu_Click(object sender, EventArgs e)
        {
            var fm = new ExportSVOrders {MdiParent = this, ShopType = 1, Tag = 1, ShopName = "Москва"};
            fm.Show();
        }

        private void ExportOrdersFromSVSpbMenu_Click(object sender, EventArgs e)
        {
            var fm = new ExportSVOrders {MdiParent = this, ShopType = 2, Tag = 2, ShopName = "Санкт-Петербург"};
            fm.Show();
        }

        private void ExportOrdersFromSVNskMenu_Click(object sender, EventArgs e)
        {
            var fm = new ExportSVOrders {MdiParent = this, ShopType = 3, Tag = 3, ShopName = "Новосибирск"};
            fm.Show();
        }

        private void ExportOrdersFromSVKdrMenu_Click(object sender, EventArgs e)
        {
            var fm = new ExportSVOrders {MdiParent = this, ShopType = 4, Tag = 4, ShopName = "Регионы"};
            fm.Show();
        }

        private void LoadBidsKdrMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadBids {MdiParent = this, ShopType = 4, Tag = 4, ShopName = "Регионы"};
            fm.Show();
        }

        private void XMLGenerationMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListCommodityGroup {MdiParent = this, GroupType = 1, Tag = 1};
            fm.Show();
        }

        private void LoadProductTypeMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadProductType {MdiParent = this};
            fm.Show();
        }

        private void LoadDescriptionMoscowMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadDescription {MdiParent = this, ShopType = 1, Tag = 1, ShopName = "Москва"};
            fm.Show();
        }

        private void LoadDescriptionSpbMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadDescription {MdiParent = this, ShopType = 2, Tag = 2, ShopName = "Санкт-Петербург"};
            fm.Show();
        }

        private void LoadDescriptionNskMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadDescription {MdiParent = this, ShopType = 3, Tag = 3, ShopName = "Новосибирск"};
            fm.Show();
        }

        private void LoadDescriptionKdrMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadDescription {MdiParent = this, ShopType = 4, Tag = 4, ShopName = "Регионы"};
            fm.Show();
        }

        private void YandexGroupMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListYandexGroup {MdiParent = this};
            fm.Show();
        }

        private void CommodityGroup4YandexMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListCommodityGroup {MdiParent = this, GroupType = 2, Tag = 2};
            fm.Show();
        }

        private void LoadKeywordsMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadKeywords {MdiParent = this};
            fm.Show();
        }

        public int CatalogRoot(int shopType)
        {
            return Shops.Where(s => s.ShopType == shopType).Select(s => s.RootNode).SingleOrDefault();
        }

        private void CatalogTreeMenu_Click(object sender, EventArgs e)
        {
            var form = new EditTreeView
            {
                ParentNode = CatalogRoot(1),
                Text = @"Каталог сайта",
                MdiParent = this
            };
            form.Show();
        }

        private void LinkMasterMenu_Click(object sender, EventArgs e)
        {
            var fm = new LinkMaster
            {
                CatalogNode = CatalogRoot(1),
                MasterNode = Constants.MASTER_NODE_ROOT,
                MdiParent = this
            };
            fm.Show();
        }

        private void radMenuItem10_Click(object sender, EventArgs e)
        {
            SetTheme("Aqua");
        }

        private void radMenuItem11_Click(object sender, EventArgs e)
        {
            SetTheme("Breeze");
        }

        private void radMenuItem12_Click(object sender, EventArgs e)
        {
            SetTheme("Desert");
        }

        private void radMenuItem13_Click(object sender, EventArgs e)
        {
            SetTheme("Office2010Black");
        }

        private void radMenuItem14_Click(object sender, EventArgs e)
        {
            SetTheme("Windows7");
        }

        private void SetTheme(string themeName)
        {
            ThemeResolutionService.ApplicationThemeName = themeName;
            SaveCurrentTheme();
        }

        private void LoadCurrentTheme()
        {
            if (Additionals.ContainsKey(ThemeKey))
                ThemeResolutionService.ApplicationThemeName = Additionals[ThemeKey];
        }

        private void SaveCurrentTheme()
        {
            Additionals[ThemeKey] =
                ThemeResolutionService.ApplicationThemeName;
        }

        private void LoadAccessoriesMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadAccessories {MdiParent = this};
            fm.Show();
        }

        private void LoadAnalogsMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadAnalogs {MdiParent = this};
            fm.Show();
        }

        private void ActionMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListAction {MdiParent = this};
            fm.Show();
        }

        private void AdvertCompanyMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListAdvertCompany {MdiParent = this};
            fm.Show();
        }

        private void AdvertItemMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListAdvertItem {MdiParent = this};
            fm.Show();
        }

        private void AdvertImageMenu_Click(object sender, EventArgs e)
        {
            var form = new ListBinaryDataBanner {MdiParent = this};
            form.Show();
        }

        private void ReportAdvertShowMenu_Click(object sender, EventArgs e)
        {
            var fm = new ReportBanners {MdiParent = this};
            fm.Show();
        }

        private void radMenuItem1_Click(object sender, EventArgs e)
        {
            var fm = new ListAdvertPlace {MdiParent = this};
            fm.Show();
        }

        private void ListJobLogMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListJobLog {MdiParent = this};
            fm.Show();
        }

        private void radMenuItem1_Click_1(object sender, EventArgs e)
        {
            //Test fm = new Test();
            //fm.MdiParent = this;
            //fm.Show();
        }

        private void DetermineWholesalePossibility()
        {
            lp = ServiceUtility.ListProvider;
            const string sql = "SELECT optionValue FROM t_Option WHERE optionId = 3";
            var value =
                new DataSetISM(lp.GetDataSetSql(sql)).Table.AsEnumerable()
                    .Select(row => row.Field<string>("optionValue"))
                    .SingleOrDefault();
            HasWholesale = value == "1";
        }

        private void DetermineDiscountPossibility()
        {
            lp = ServiceUtility.ListProvider;
            const string sql = "SELECT optionValue FROM t_Option WHERE optionId = 4";
            var value =
                new DataSetISM(lp.GetDataSetSql(sql)).Table.AsEnumerable()
                    .Select(row => row.Field<string>("optionValue"))
                    .SingleOrDefault();
            IsDiscount = value == "1";
        }

        private void WebConfigMenu_Click(object sender, EventArgs e)
        {
            var fm = new AppSettingsEdit {MdiParent = this};
            fm.Show();
        }

        private void UploadGoods4YandexMenu_Click(object sender, EventArgs e)
        {
            var fm = new UploadGoods4Yandex {MdiParent = this};
            fm.Show();
        }

        private void GoodsItemMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListGoodsItems {MdiParent = this};
            fm.Show();
        }

        private void LoadInStockMenu_Click(object sender, EventArgs e)
        {
            var fm = new LoadGoodsFromExcel {MdiParent = this};
            fm.Show();
        }

        private void LandingPageMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListLandingPage {MdiParent = this};
            fm.Show();
        }

        #region IfmMain

        public static Hashtable OpenedForms { get; } = new Hashtable();

        public bool isCloseWarningShow { get; set; } = true;

        public bool isCloseAfterSave { get; set; }

        public bool isExportBeforePrint { get; set; }

        public bool isSaveFilters { get; set; } = true;

        public string CurrentTicket
        {
            get
            {
                if (_authProvider == null || _authProvider.SecurityHeaderValue == null ||
                    _authProvider.SecurityHeaderValue.Ticket == null) return "";
                return _authProvider.SecurityHeaderValue.Ticket;
            }
        }

        public Guid Manager { get; set; }

        public string ManagerName { get; set; }

        #endregion

        private void radMenuItem2_Click(object sender, EventArgs e)
        {
            var bp = ServiceUtility.BusinessProvider;
            var xml = bp.GetStockInfo1C();
            var ds = new DataSetISM(xml);
            ShowError(ds.Table.Rows.Count.ToString());
        }

        private void YandexReportMenu_Click(object sender, EventArgs e)
        {
            var fm = new YandexReport { MdiParent = this };
            fm.Show();
        }

        private void DefaultTypeMenu_Click(object sender, EventArgs e)
        {
            var fm = new ListDefaultTitle { MdiParent = this };
            fm.Show();
        }

        private void BrowseDefaultTitleMenu_Click(object sender, EventArgs e)
        {
            var fm = new BrowseDefaultTitle { MdiParent = this };
            fm.Show();
        }
    }

    public class ShopMenu
    {
        public int ShopType { get; set; }
        public int RootNode { get; set; }
        public string SiteUrl { get; set; }
        public bool IsActive { get; set; }
        public IList<RadMenuItem> Menus { get; set; }
    }
}