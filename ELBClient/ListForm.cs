﻿using System;
using System.Windows.Forms;

namespace ELBClient
{
    /// <summary>
    ///     Summary description for GenericForm.
    /// </summary>
    public partial class ListForm : GenericForm
    {
        private const int WM_SHOWWINDOW = 0x0018;

        public virtual void LoadData()
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            //сохраняем форму в коллекции открытых форм, чтобы позже при вызове данной формы
            //не открывать новую, а активировать уже имеющуюся
            if (!DesignMode)
            {
                MainFrm.OpenedForms[GetType().ToString() + Tag] = this;
            }

            base.OnLoad(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (!DesignMode)
            {
                MainFrm.OpenedForms[GetType().ToString() + Tag] = null;
            }
            base.OnClosed(e);
        }


        public override void Show()
        {
            ListForm form = null;
            form = (ListForm) MainFrm.OpenedForms[GetType().ToString() + Tag];

            if (form != null)
            {
                if (form.WindowState == FormWindowState.Minimized)
                    form.WindowState = FormWindowState.Normal;
                form.Activate();
            }
            else
                base.Show();
        }
    }

    public delegate void ReloadDelegate();
}