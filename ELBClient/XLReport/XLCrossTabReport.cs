using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using ColumnMenuExtender;
using ExcelApp = Microsoft.Office.Interop.Excel;


namespace ELBClient.XLReport
{
	/// <summary>
	/// Summary description for XLSpecReport.
	/// </summary>
	public class XLCrossTabReport
	{
		private Regex reInt = new Regex(@"\d*");
		private string templateFilename;
		private string outputFilename;
		public string OutputFilename {
			get {
				return this.outputFilename;
			}
			set {
				this.outputFilename = value;
			}
		}
		private string outputPath = "";
		private ExcelApp.Application excelApplication;

		private ExcelApp.Workbooks workbooks;
		private ExcelApp.Workbook workbook;
		private ExcelApp.Sheets worksheets;
		private ExcelApp.Worksheet worksheet;
		private ExcelApp.Worksheet newWorksheet;
		private string defaultPath;
		private DataSet dataSet;

		private XLReportTask task;
		private Thread mThread;
		public string DateFormat = "";

		private bool duplicateRows = true;
		public bool DuplicateRows {
			get { return duplicateRows; }
			set { duplicateRows = value; }
		}

		private bool noFormuls = false;
		public bool NoFormuls {
			get { return noFormuls; }
			set { noFormuls = value; }
		}
		private string cutPath(string path) {
			if (path.Substring(path.Length - 10, 10) == @"bin\Debug\") {
				return path.Substring(0, path.Length - 10);
			}
			else
				return path;
		}
		private void initFileName(string tmplFileName, string outFileName) {
			defaultPath = Application.StartupPath + @"\";
			templateFilename = cutPath(defaultPath) + "XLTemplates\\" + tmplFileName;
			if (outputPath == "") {
				//�������� ����� Reports � ����� ���������� ����
				DirectoryInfo dir = new DirectoryInfo(Application.StartupPath);
				try {
					dir.CreateSubdirectory("Reports");
				}
				finally {
					outputPath = defaultPath + @"Reports\";
				}
			}
			OutputFilename = outputPath + outFileName;
		}
		public XLCrossTabReport(DataSet dataSet, string tmplFileName, string outFileName) {
			this.dataSet = dataSet;
			initFileName(tmplFileName, outFileName);
		}
		public void Create() {
			Create(false);
		}
		public void Create(bool saveOnly) {
			if (saveOnly)
				task = XLReportTask.Save;
			else
				task = XLReportTask.Print;

			mThread = new Thread(new ThreadStart(startCreate));
			mThread.Start();
		}
		public void Export() {
			task = XLReportTask.Export;

			mThread = new Thread(new ThreadStart(startCreate));
			mThread.Start();
		}
		private void startCreate() {
			excelApplication = new ExcelApp.Application();
			try {
				string outputFilenameTemp = Path.GetDirectoryName(OutputFilename) + "\\" + Path.GetFileNameWithoutExtension(OutputFilename);
				int fileNum = 0;

				bool looking = true;

				while (looking) {
					if (File.Exists(outputFilenameTemp + ".xls")) {
						//outputFilenameTemp = outputFilename + "(" + (++fileNum).ToString() + ")";
						try {
							File.Delete(outputFilenameTemp + ".xls");
							looking = false;
						}
						catch {
							outputFilenameTemp = OutputFilename + "(" + (++fileNum).ToString() + ")";
						}
					}
					else
						looking = false;
				}

				OutputFilename = outputFilenameTemp + ".xls";

				try {
					workbooks = excelApplication.Workbooks;
					workbook = workbooks.Open(templateFilename,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing);
				}
				catch (Exception e) {
					GenericForm.ShowError(null, "������� ����������", e);//������ �� ������!
					if (excelApplication != null) excelApplication.Quit();
					return;
				}
				try {
					if (workbook != null) {
						workbook.SaveAs(OutputFilename, Type.Missing, Type.Missing, Type.Missing,
							Type.Missing, Type.Missing, ExcelApp.XlSaveAsAccessMode.xlNoChange,
							Type.Missing, Type.Missing, Type.Missing, Type.Missing,
							Type.Missing);
					}
				}
				catch (Exception e) {
					GenericForm.ShowError(null, "�� ������� ��������� ��������", e);//���������� ���������!
					if (excelApplication != null) excelApplication.Quit();
					return;
				}
				worksheets = workbook.Worksheets;
				worksheet = (ExcelApp.Worksheet)worksheets[1];


				newWorksheet = (ExcelApp.Worksheet)worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
				if (newWorksheet != null) {
					newWorksheet.Name = "hiddenSheet";
					newWorksheet.Visible = ExcelApp.XlSheetVisibility.xlSheetHidden;
				}

				System.Threading.Thread thisThread = System.Threading.Thread.CurrentThread;
				System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;

				try {
					thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

					CreateCrossTabReport();
				}
				catch (Exception e) {
					CustomMessageBox.Show("�������� ������ ��� �������� ������.\r\n�������� ������:\r\n" + e.ToString(), "������!");
					return;
				}
				finally {
					thisThread.CurrentCulture = originalCulture;
				}

				if (task == XLReportTask.Print) {
					try {
						if (excelApplication != null) excelApplication.Visible = true;
						if (worksheet != null) worksheet.PrintPreview(false);

					}
					catch (System.Runtime.InteropServices.COMException e) {
						MessageBox.Show(e.Message);
					}
					finally {
						if (excelApplication != null) excelApplication.Visible = false;
					}
				}
				else if (task == XLReportTask.Export) {
					if (excelApplication != null) excelApplication.Visible = true;
				}

				workbook.Save();

				if (task != XLReportTask.Export) {
					if (workbook != null) workbook.Close(false, Type.Missing, Type.Missing);
					if (excelApplication != null) excelApplication.Quit();
				}
				else {
					ExcelApp.Windows wnds = null;
					ExcelApp.Window wnd = null;
					try {
						wnds = (ExcelApp.Windows)excelApplication.Windows;
						wnd = (ExcelApp.Window)wnds[1];
						wnd.Activate();
					}
					finally {
						NAR(wnd);
						NAR(wnds);
					}
				}
			}
			catch (Exception e) {
				CustomMessageBox.Show("General failure!\r\n-------------\r\n'" + e.ToString(), "Error!");
				if (excelApplication != null) excelApplication.Quit();
			}
			finally {
				NAR(newWorksheet);
				NAR(worksheet);
				NAR(worksheets);
				NAR(workbook);
				NAR(workbooks);
				NAR(excelApplication);
				if (task == XLReportTask.Print)
					File.Delete(OutputFilename);
			}
		}
		
		#region ������ � ��������
		private string[] parseAddress(string address) {
			string[] arStr;
			arStr = new string[4];
			Regex re = new Regex("([a-zA-Z]+)([0-9]+)");

			Match m = re.Match(address);
			if (m.Success) {
				arStr[0] = m.Groups[1].Value;
				arStr[1] = m.Groups[2].Value;

				m = m.NextMatch();
				if (m.Success) {
					arStr[2] = m.Groups[1].Value;
					arStr[3] = m.Groups[2].Value;
				}
				else {
					arStr[2] = arStr[0];
					arStr[3] = arStr[1];
				}
				return arStr;
			}
			else throw new ApplicationException("Wrong excel address!");
		}

		private string next(string addr, int num) {
			string res = addr;
			for (int i = 0; i < num; i++) {
				res = next(res);
			}
			return res;
		}

		private string next(string addr) {
			int i;
			reInt.IsMatch(addr);
			if (reInt.IsMatch(addr)) {
				i = int.Parse(addr);
				return (i + 1).ToString();
			}
			else {
				if (addr.Length == 1) {
					char chr = addr[0];
					if (chr == 'Z') return "AA";
					chr = (char)(chr + 1);
					return "" + chr;
				}
				if (addr.Length == 2) {
					if (addr[1] == 'Z') {
						return "" + (char)(addr[0] + 1) + 'A';
					}
					return "" + addr[0] + (char)(addr[1] + 1);
				}
				throw new ApplicationException("Dont use addresses with three letter!");
			}
		}

		private string prev(string addr, int num) {
			string res = addr;
			for (int i = 0; i < num; i++) {
				res = prev(res);
			}
			return res;
		}

		private string prev(string addr) {
			int i;
			if (reInt.IsMatch(addr)) {
				i = int.Parse(addr);
				return (i - 1).ToString();
			}
			else {
				if (addr.Length == 1) {
					char chr = addr[0];
					if (chr == 'A')
						throw new ApplicationException("Out of range!");
					chr = (char)(chr - 1);
					return "" + chr;
				}
				if (addr.Length == 2) {
					if (addr[1] == 'A') {
						if (addr[0] == 'A') return "Z";
						else return "" + (char)(addr[0] - 1) + 'Z';
					}
					return "" + addr[0] + (char)(addr[1] - 1);
				}
				throw new ApplicationException("Dont use addresses with three letter!");
			}
		}

		private ExcelApp.Range nextToRightRange(ExcelApp.Range rng) {
			string addr = rng.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = next(str[0]) + str[1];
			string addrNext1 = next(str[2]) + str[3];
			return worksheet.get_Range(addrNext, addrNext1);
		}

		private ExcelApp.Range nextToLeftRange(ExcelApp.Range rng) {
			string addr = rng.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = prev(str[0]) + str[1];
			string addrNext1 = prev(str[2]) + str[3];
			return worksheet.get_Range(addrNext, addrNext1);
		}

		private ExcelApp.Range nextToDownRange(ExcelApp.Range rng) {
			string addr = rng.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + next(str[1]);
			string addrNext1 = str[2] + next(str[3]);
			return worksheet.get_Range(addrNext, addrNext1);
		}

		private ExcelApp.Range nextToDownRange(ExcelApp.Range rng, int num) {
			string addr = rng.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + next(str[1], num);
			string addrNext1 = str[2] + next(str[3], num);
			return worksheet.get_Range(addrNext, addrNext1);
		}

		private ExcelApp.Range nextToUpRange(ExcelApp.Range rng) {
			string addr = rng.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + prev(str[1]);
			string addrNext1 = str[2] + prev(str[3]);
			return worksheet.get_Range(addrNext, addrNext1);
		}
		private ExcelApp.Range nextToUpRange(ExcelApp.Range rng, int num) {
			string addr = rng.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			string[] str = parseAddress(addr);
			string addrNext = str[0] + prev(str[1], num);
			string addrNext1 = str[2] + prev(str[3], num);
			return worksheet.get_Range(addrNext, addrNext1);
		}

		#endregion
		private void NAR(object obj) {
			if (obj == null) return;
			try {
				System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
			}
			catch { }
			finally {
				obj = null;
			}
		}
		//�������� �������� � ������� � ������� ����
		private string GetColValue(object o, string colName) {
			string result = "";

			if (o is DateTime) {
				if (DateFormat == "")
					result = ((DateTime)o).ToShortDateString();
				else
					result = ((DateTime)o).ToString(DateFormat);
			}
			else result = o.ToString();

			return result;
		}
		private void AutoFitMergedCellRowHeight(ExcelApp.Range Rng) {

			ExcelApp.Range rngTmp = null;
			ExcelApp.Range rngTmp1 = null;
			ExcelApp.Worksheet sheet = null;
			try {

				rngTmp = (ExcelApp.Range)Rng.Cells[1, 1];
				double mergedCellRgWidth = 0, rngHeight, rngWidth, possNewRowHeight;

				rngTmp1 = Rng.MergeArea;
				rngWidth = (double)rngTmp.ColumnWidth;
				rngHeight = (double)rngTmp.RowHeight;

				for (int i = 1; i <= rngTmp1.Columns.Count; i++)
					mergedCellRgWidth += (double)((ExcelApp.Range)rngTmp1.Cells[1, i]).Width;

				mergedCellRgWidth = (mergedCellRgWidth * 4 / 3 - 5) / 7;
				sheet = (ExcelApp.Worksheet)rngTmp1.Parent;
				sheet.Application.ScreenUpdating = false;

				rngTmp1.MergeCells = false;
				rngTmp.ColumnWidth = mergedCellRgWidth;
				rngTmp1.EntireRow.AutoFit();
				possNewRowHeight = (double)rngTmp1.RowHeight;
				rngTmp.ColumnWidth = rngWidth;
				rngTmp1.MergeCells = true;
				rngTmp1.EntireRow.RowHeight = possNewRowHeight > rngHeight ? possNewRowHeight : rngHeight;

				sheet.Application.ScreenUpdating = true;
			}
			finally {
				NAR(rngTmp);
				NAR(rngTmp1);
				NAR(sheet);
			}
		}
		private void InsertRow(ExcelApp.Range rngLeftBottom, ExcelApp.Range copyRange, bool hidden) {
			ExcelApp.Range rngTmp = null;
			try {
				rngLeftBottom.EntireRow.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, false);
				if (copyRange != null) {
				 //���� ���� ���-�� ����������� (��� EntireRow.Insert �� ����������, ��������, �������)
					rngTmp = nextToUpRange(rngLeftBottom);//���������� ���������� ������ 
					copyRange.Copy(rngTmp);
				}
				if (hidden) rngTmp.EntireRow.Hidden = true;
			}
			finally {
				NAR(rngTmp);
			}
		}
		private void InsertColumn(ExcelApp.Range rngRight, ExcelApp.Range copyRange, bool hidden) {
			ExcelApp.Range rngTmp = null;
			try {
				rngRight.EntireColumn.Insert(ExcelApp.XlInsertShiftDirection.xlShiftToRight, false);
				if (copyRange != null) {
				 //���� ���� ���-�� ����������� (��� EntireColumn.Insert �� ����������, ��������, �������)
					rngTmp = nextToLeftRange(rngRight);//���������� ���������� ������ 
					copyRange.Copy(rngTmp);
				}
				if (hidden) rngTmp.EntireColumn.Hidden = true;
			}
			finally {
				NAR(rngTmp);
			}
		}
		private void DeleteRow(ExcelApp.Range rngRow) {
			rngRow.EntireRow.Delete(ExcelApp.XlDeleteShiftDirection.xlShiftUp);
		}

		/// <summary>
		///�������� ������ ������� ��� ��������� Range
		///[0] - ��� �������
		///[1] - ����� ������� (������ ������� � �� ������!!!)
		/// </summary>
		/// <param name="rngRow">��� ������</param>
		/// <param name="table">��� ����� ������� ������</param>
		/// <param name="rowNumRange">�������� � rowNumRange, ���� ������� ��������� (��� ���� ������ ����� ������� �����)</param>
		/// <returns>���������� ������ � ���������� ���� ����������</returns>
		private ArrayList GetColRange(ExcelApp.Range rngRow, DataTable table, out string rowNumRange) {
			ExcelApp.Range rngTemp;
			ArrayList colRanges = new ArrayList();
			rowNumRange = null;

			if (table != null) {
			 //1
				string[] str = parseAddress(rngRow.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
				string l_c = str[0];
				string t_r = str[1];
				string r_c = str[2];
				//string b_r = str[3];

				string addrRightTopNext = next(r_c) + t_r;// ����� ������ �� ��������� ��������� Right+1;Top

				rngTemp = worksheet.get_Range(l_c + t_r, l_c + t_r);//����� ������� ������ � ��������� Left;Top
				string addrTemp = rngTemp.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);

				int i = 0;

				do {
					object value2 = rngTemp.Formula;
					if (value2 is string) {
						string val = (string)value2;

						if (val != null && val.Length > 0 && val[0] == '=') {
							val = val.Substring(1);
							string[] adrTmpArr = parseAddress(addrTemp);
							//if (dataSet != null)
							//{
							if (table.Columns.IndexOf(val) >= 0)
								colRanges.Add(new object[] { val, adrTmpArr[0] });//���� ����� Column
							else if (val.ToLower().Equals("rownum"))
								rowNumRange = adrTmpArr[0];//���� ����� Column
						}
					}
					rngTemp = nextToRightRange(rngTemp);//��������� ������
					addrTemp = rngTemp.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
					i++;
				}
				while (addrTemp != addrRightTopNext /*&& i < 50*/);
			}//1

			return colRanges;
		}
		/// <summary>
		/// ���������� ������ ������� ����� ��� ������������� ��������� � ������ pivot table
		/// </summary>
		/// <param name="rngRow"></param>
		/// <returns></returns>
		private int[] GetLegendCells(ExcelApp.Range rngRow) {
			int i = 1;
			ExcelApp.Range rngTemp;
			int[] rows = new int[3];

			string[] str = parseAddress(rngRow.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
			string l_c = str[0];
			string t_r = str[1];
			string r_c = str[2];
			string b_r = str[3];

			string addrBottomNext = l_c + next(b_r);// ����� ������ �� ��������� ��������� left;bottom+1;

			rngTemp = worksheet.get_Range(l_c + t_r, l_c + t_r);//����� ������� ������ � ��������� Left;Top
			string addrTemp = rngTemp.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);

			do {
				object value2 = rngTemp.Formula;
				if (value2 is string) {
					string val = (string)value2;

					if (val != null && val.Length > 0 && val.ToLower().StartsWith("=legend_")) {
						val = val.Substring(8);

						string[] adrTmpArr = parseAddress(addrTemp);

						if (val.ToLower() == "name") rows[0] = int.Parse(adrTmpArr[1]);
						else if (val.ToLower() == "id") rows[i++] = int.Parse(adrTmpArr[1]);
					}
				}
				rngTemp = nextToDownRange(rngTemp);//��������� ����
				addrTemp = rngTemp.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
			}
			while (addrTemp != addrBottomNext);

			return rows;
		}
		private void CreateCrossTabReport() {
			ExcelApp.Range rngLegTable = null;//Range ��� ���������� �������������� �������
			ExcelApp.Range rngNextLeg = null;//Range ������
			ExcelApp.Range rngTable = null;//Range ������� �������
			ExcelApp.Range rngTopRow = null;
			ExcelApp.Range rngLeftBottom = null;
			ExcelApp.Range rngCell = null;
			try {
				// �������� �������������� �������
				DataTable leg = dataSet.Tables["legend"];
				rngLegTable = worksheet.get_Range("pivot_data", Type.Missing);
				rngNextLeg = nextToRightRange(rngLegTable);
				int[] legRows = GetLegendCells(rngLegTable);
				int rowLegName = legRows[0];
				int rowLegData = legRows[1];
				int rowLegData1 = legRows[2];

				string[] oAddr = parseAddress(rngLegTable.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
				string addr = oAddr[0];

				for(int i = 0; i < leg.Rows.Count; i++) {
					DataRow dr = leg.Rows[i];
					string name = (string)dr["name"];
					string formula = "=" + dr["ID"];

					rngCell = worksheet.get_Range(addr + rowLegName, addr + rowLegName);
					rngCell.Value2 = name;
					rngCell = worksheet.get_Range(addr + rowLegData, addr + rowLegData);
					rngCell.Formula = formula;
					rngCell = worksheet.get_Range(addr + rowLegData1, addr + rowLegData1);
					rngCell.Formula = formula;

					if (i != leg.Rows.Count - 1) {
						InsertColumn(rngNextLeg, rngLegTable, false);
						addr = next(addr);
					}
				}


				DataTable dtTmp = dataSet.Tables["Table"];

				rngTable = worksheet.get_Range("data", Type.Missing);
				ArrayList colRanges; ;//����� ����������� �������

				string rowNumRange = null;//����� ��� rowNum �������� �������

				string[] str = parseAddress(rngTable.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing));
				string l_c = str[0];
				string t_r = str[1];
				string r_c = str[2];
				string b_r = str[3];
				rngTopRow = worksheet.get_Range(l_c + t_r, r_c + t_r);
				rngLeftBottom = worksheet.get_Range(l_c + b_r, l_c + b_r);

				//��������� ColRanges
				colRanges = GetColRange(rngTopRow, dtTmp, out rowNumRange);


				// �������� ��������� ������
				if (dtTmp.Rows.Count == 0) {
					DeleteRow(rngTopRow);
				}
				
				for (int i = 1; i < dtTmp.Rows.Count; i++) {
					InsertRow(rngLeftBottom, rngTopRow, false);
				}
				
				int curRowNum = Convert.ToInt32(t_r);//������� ������, ���� ������ ������

				for (int i = 0; i < dtTmp.Rows.Count; i++) {
				  //��� ������ ������
					foreach (object[] oArr in colRanges) {
					 //��� ������ �������
						string colName = (string)oArr[0];
						object o = dtTmp.Rows[i][colName];

						rngCell = worksheet.get_Range((string)oArr[1] + curRowNum, (string)oArr[1] + curRowNum);
						rngCell.Value2 = GetColValue(o, colName);

						if ((bool)rngCell.MergeCells && (ExcelApp.XlVAlign)rngCell.VerticalAlignment == ExcelApp.XlVAlign.xlVAlignJustify) {
							AutoFitMergedCellRowHeight(rngCell);
						}
					}
					if (rowNumRange != null) {
						ExcelApp.Range rngRowNum = null;
						try {
							rngRowNum = worksheet.get_Range(rowNumRange + curRowNum, rowNumRange + curRowNum);
							rngRowNum.Value2 = i + 1;
						}
						finally {
							NAR(rngRowNum);
						}
					}
					curRowNum++;//����������� ����� ������
				}
			}
			finally {
				NAR(rngLegTable);
				NAR(rngTable);
				NAR(rngTopRow);
				NAR(rngLeftBottom);
				NAR(rngCell);
			}
		}
	}
}
