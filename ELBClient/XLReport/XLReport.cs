﻿using System;
using System.Data;
using System.Linq;
using System.Collections;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using ColumnMenuExtender.Forms;
using ELBClient.Classes;
using ExcelApp = Microsoft.Office.Interop.Excel;
using ColumnMenuExtender;
using System.IO;
using System.Threading;
using MetaData;

namespace ELBClient.XLReport
{
	public enum XLReportTask
	{
		Save,
		Print,
		Export
	}
	public class XLReport
	{
		private string templateFilename;
		private string outputFilename;
		private ExcelApp.Application excelApplication;

		private ExcelApp.Workbook workbook;
		private ExcelApp.Worksheet worksheet;
		private ExcelApp.Worksheet newWorksheet;

		private string[] classNames;
		private string defaultPath;

		private CObject[] _objs;
		private Type[] objTypes;
		private readonly Hashtable objLookupFields;

		private DataSet dataSet;
		private DataView[] dataViews;
		private string[] tableNames;
		private string[] rangeNames;

		private string[] stringArray;
		private object[] objectArray;

		public string DateFormat { get; set; }
		public bool InsertRows { get; set; }
		public bool InsertColumns { get; set; }
		public int QuantityOfConstRows { get; set; }

		public bool NoDuplicateRows { get; set; }

		public bool NoFormuls { get; set; }

		private const string secTableRange = "";

		private string cutPath(string path)
		{
			if (path.Substring(path.Length - 10, 10) == @"bin\Debug\")
			{
				return path.Substring(0, path.Length - 10);
			}
			return path;
		}

		private void initFileName(string tmplFileName, string outFileName)
		{
			defaultPath = Application.StartupPath + @"\";
			templateFilename = cutPath(defaultPath) + tmplFileName;
			//создадим папку Reports и будем складывать туда
			var dir = new DirectoryInfo(Application.StartupPath);
			try
			{
				dir.CreateSubdirectory("Reports");
			}
			finally
			{
				outputFilename = defaultPath + @"Reports\" + outFileName;
			}
		}


		#region .ctor

		public XLReport(CObject _object)
		{
			init(new[] { _object }, null, null, null, null, classNames[0] + ".xls", "out.xls");
		}

		public XLReport(CObject[] _objects, Hashtable objLookupFields, DataSet dataSet, string[] tableNames, string[] rangeNames, object[] objectArray, string tmplFileName, string outFileName)
		{
			this.objLookupFields = objLookupFields;
			init(_objects, dataSet, tableNames, rangeNames, objectArray, tmplFileName, outFileName);
		}

		public XLReport(CObject[] _objects, Hashtable objLookupFields, DataSet dataSet, string[] tableNames, string[] rangeNames, string templateFileName, string outFileName)
		{
			this.objLookupFields = objLookupFields;
			init(_objects, dataSet, tableNames, rangeNames, null, templateFileName, outFileName);
		}

		public XLReport(CObject[] _objects, Hashtable objLookupFields, DataSet dataSet, string[] tableNames, string[] rangeNames, string[] stringArray, string templateFileName, string outFileName)
		{
			this.objLookupFields = objLookupFields;
			init(_objects, dataSet, tableNames, rangeNames, stringArray, templateFileName, outFileName);
		}

		public XLReport(CObject[] _objects, Hashtable objLookupFields, DataView[] dataView, string[] rangeNames, object[] objectArray, string templateFileName, string outFileName)
		{
			this.objLookupFields = objLookupFields;
			init(_objects, dataView, rangeNames, objectArray, templateFileName, outFileName);
		}

		public XLReport(CObject[] _objects, Hashtable objLookupFields, DataView[] dataView, string[] rangeNames, string templateFileName, string outFileName)
		{
			this.objLookupFields = objLookupFields;
			init(_objects, dataView, rangeNames, null, templateFileName, outFileName);
		}

		public XLReport(CObject[] _objects, Hashtable objLookupFields, DataView[] dataView, string[] rangeNames, string[] stringArray, string templateFileName, string outFileName)
		{
			this.objLookupFields = objLookupFields;
			init(_objects, dataView, rangeNames, stringArray, templateFileName, outFileName);
		}

		public XLReport(DataSet dataSet, string[] tableNames, string[] rangeNames, object[] objectArray, string templateFileName, string outFilename)
		{
			init(null, dataSet, tableNames, rangeNames, objectArray, templateFileName, outFilename);
		}

		public XLReport(DataSet dataSet, string[] tableNames, string[] rangeNames, string templateFileName, string outFilename)
		{
			init(null, dataSet, tableNames, rangeNames, null, templateFileName, outFilename);
		}

		public XLReport(DataSet dataSet, string[] tableNames, string[] rangeNames, string[] stringArray, string templateFileName, string outFilename)
		{
			init(null, dataSet, tableNames, rangeNames, stringArray, templateFileName, outFilename);
		}

		public XLReport(DataView[] dataView, string[] rangeNames, string templateFileName, string outFilename)
		{
			init(null, dataView, rangeNames, null, templateFileName, outFilename);
		}

		public XLReport(DataView[] dataView, string[] rangeNames, string[] stringArray, string templateFileName, string outFilename)
		{
			init(null, dataView, rangeNames, stringArray, templateFileName, outFilename);
		}

		public XLReport(DataView[] dataView, string[] rangeNames, object[] objectArray, string templateFileName, string outFilename)
		{
			init(null, dataView, rangeNames, objectArray, templateFileName, outFilename);
		}

		#endregion

		#region init
		private void init(CObject[] _objects, DataSet dataSetParam, string[] tableNamesParam, string[] rangeNamesParam, string[] stringArrayParam, string templateFileName, string outFileName)
		{
			if (_objects != null)
			{
				_objs = _objects;
				var n = _objects.Length;
				objTypes = new Type[n];
				classNames = new String[n];
				for (var i = 0; i < n; i++)
				{
					objTypes[i] = _objs[i].GetType();
					classNames[i] = _objs[i].ClassName;
				}
			}

			if (dataSetParam != null)
			{
				dataSet = dataSetParam;
				dataViews = null;
				tableNames = tableNamesParam;
				rangeNames = rangeNamesParam;
			}

			stringArray = stringArrayParam;

			initFileName(templateFileName, outFileName);
		}

		private void init(CObject[] _objects, DataView[] dataViewsParam, string[] rangeNamesParam, string[] stringArrayParam, string templateFileName, string outFileName)
		{
			if (_objects != null)
			{
				_objs = _objects;
				var n = _objects.Length;
				objTypes = new Type[n];
				classNames = new String[n];
				for (var i = 0; i < n; i++)
				{
					objTypes[i] = _objs[i].GetType();
					classNames[i] = _objs[i].ClassName;
				}
			}

			if (dataViewsParam != null)
			{
				dataViews = dataViewsParam;
				dataSet = null;
				tableNames = null;
				rangeNames = rangeNamesParam;
			}

			stringArray = stringArrayParam;

			initFileName(templateFileName, outFileName);
		}

		private void init(CObject[] _objects, DataSet dataSetParam, string[] tableNamesParam, string[] rangeNamesParam, object[] objectArrayParam, string templateFileName, string outFileName)
		{
			if (_objects != null)
			{
				_objs = _objects;
				var n = _objects.Length;
				objTypes = new Type[n];
				classNames = new String[n];
				for (var i = 0; i < n; i++)
				{
					objTypes[i] = _objs[i].GetType();
					classNames[i] = _objs[i].ClassName;
				}
			}

			if (dataSetParam != null)
			{
				dataSet = dataSetParam;
				dataViews = null;
				tableNames = tableNamesParam;
				rangeNames = rangeNamesParam;
			}

			objectArray = objectArrayParam;

			initFileName(templateFileName, outFileName);
		}

		private void init(CObject[] _objects, DataView[] dataViewsParam, string[] rangeNamesParam, object[] objectArrayParam, string templateFileName, string outFileName)
		{
			if (_objects != null)
			{
				_objs = _objects;
				int n = _objects.Length;
				objTypes = new Type[n];
				classNames = new String[n];
				for (int i = 0; i < n; i++)
				{
					objTypes[i] = _objs[i].GetType();
					classNames[i] = _objs[i].ClassName;
				}
			}

			if (dataViewsParam != null)
			{
				dataViews = dataViewsParam;
				dataSet = null;
				tableNames = null;
				rangeNames = rangeNamesParam;
			}

			objectArray = objectArrayParam;

			initFileName(templateFileName, outFileName);
		}
		#endregion

		public void Create()
		{
			Create(false);
		}

		private XLReportTask task;
		private Thread mThread;

		public void Create(bool saveOnly)
		{
			task = saveOnly ? XLReportTask.Save : XLReportTask.Print;

			mThread = new Thread(startCreate);
			mThread.Start();
		}

		public void Export()
		{
			task = XLReportTask.Export;

			mThread = new Thread(startCreate);
			mThread.Start();
		}

		private void startCreate()
		{
			excelApplication = new ExcelApp.Application();
			try
			{
				string outputFilenameTemp = outputFilename;
				int fileNum = 0;

				bool looking = true;

				while (looking)
				{
					if (File.Exists(outputFilenameTemp + ".xls"))
					{
						//outputFilenameTemp = outputFilename + "(" + (++fileNum).ToString() + ")";
						try
						{
							File.Delete(outputFilenameTemp + ".xls");
							looking = false;
						}
						catch
						{
							outputFilenameTemp = outputFilename + "(" + (++fileNum).ToString() + ")";
						}
					}
					else
						looking = false;
				}

				outputFilename = outputFilenameTemp;

				try
				{
					workbook = excelApplication.Workbooks.Open(templateFilename,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing, Type.Missing, Type.Missing,
						Type.Missing, Type.Missing);
				}
				catch (Exception e)
				{
					BaseGenericForm.ShowError(null, "Экспорт невозможен", e);//Шаблон не найден!
					if (excelApplication != null) excelApplication.Quit();
					return;
				}
				try
				{
					if (workbook != null)
					{
						workbook.SaveAs(outputFilename, Type.Missing, Type.Missing, Type.Missing,
							Type.Missing, Type.Missing, ExcelApp.XlSaveAsAccessMode.xlNoChange,
							Type.Missing, Type.Missing, Type.Missing, Type.Missing,
							Type.Missing);
					}
				}
				catch (Exception e)
				{
					BaseGenericForm.ShowError(null, "Не удалось сохранить документ", e);//Невозможно сохранить!
					if (excelApplication != null) excelApplication.Quit();
					return;
				}
				worksheet = (ExcelApp.Worksheet)workbook.Worksheets[1];


				newWorksheet = (ExcelApp.Worksheet)workbook.Worksheets.Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
				if (newWorksheet != null)
				{
					newWorksheet.Name = "hiddenSheet";
					newWorksheet.Visible = ExcelApp.XlSheetVisibility.xlSheetHidden;
				}

				Thread thisThread = Thread.CurrentThread;
				System.Globalization.CultureInfo originalCulture = thisThread.CurrentCulture;

				try
				{
					thisThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

					//rowCounter = 0;
					if (_objs != null)
					{
						for (var i = 0; i < _objs.Length; i++)
							createXLReportByObject(i);
					}
					if (dataSet != null)
					{
						if (NoDuplicateRows)
						{
							createXLReportByDataTableNonDup();
						}
						else
						{
							if (tableNames.Length != rangeNames.Length)
								throw new ApplicationException("tableNamesParam.Length != rangeNamesParam.Length!!!!!!!");
							for (var i = 0; i < tableNames.Length; i++)
							{
								if (NoFormuls)
								{
									createXLReportByTwoDataTable(i);
								}
								else
								{
									createXLReportByDataTable(i);
								}
							}
						}
					}
					else if (dataViews != null)
					{
						if (NoDuplicateRows)
						{
							createXLReportByDataTableNonDup();
						}
						else
						{
							for (var i = 0; i < dataViews.Length; i++)
							{
								if (NoFormuls)
								{
									createXLReportByTwoDataTable(i);
								}
								else
								{
									createXLReportByDataTable(i);
								}
							}
						}
					}
					if (stringArray != null)
					{
						for (var i = 0; i < stringArray.Length; i++)
						{
							createXLReportByStringArray(i);
						}
					}
					if (objectArray != null)
					{
						for (var i = 0; i < objectArray.Length; i++)
							createXLReportByObjectArray(i);
					}
				}
				catch (Exception e)
				{
					CustomMessageBox.Show("Возникла ошибка при создании отчета.\r\nОписание ошибки:\r\n" + e, "Ошибка!");
					if (workbook != null) workbook.Close(false, Type.Missing, Type.Missing);
					if (excelApplication != null) excelApplication.Quit();
					File.Delete(outputFilename + ".xls");
					return;
				}
				finally
				{
					thisThread.CurrentCulture = originalCulture;
				}

				if (task == XLReportTask.Print)
				{
					try
					{
						if (excelApplication != null) excelApplication.Visible = true;
						if (worksheet != null) worksheet.PrintPreview(false);

					}
					catch (System.Runtime.InteropServices.COMException e)
					{
						MessageBox.Show(e.Message);
					}
					finally
					{
						if (excelApplication != null) excelApplication.Visible = false;
					}
				}
				else if (task == XLReportTask.Export)
				{
					if (excelApplication != null) excelApplication.Visible = true;
				}

				workbook.Save();

				if (task == XLReportTask.Export)
				{
				}
				else
				{
					if (workbook != null) workbook.Close(false, Type.Missing, Type.Missing);

					if (excelApplication != null) excelApplication.Quit();
					if (task == XLReportTask.Print)
						File.Delete(outputFilename + ".xls");
				}
			}
			catch (Exception e)
			{
				CustomMessageBox.Show("General failure!\r\n-------------\r\n'" + e, "Error!");
				if (excelApplication != null) excelApplication.Quit();
			}
			finally
			{
				NAR(newWorksheet);
				NAR(worksheet);
				NAR(workbook);
				NAR(excelApplication);
			}
		}

		private string[] parseAddress(string address)
		{
			var arStr = new string[4];
			var re = new Regex("([a-zA-Z]+)([0-9]+)");

			var m = re.Match(address);
			if (!m.Success)
				throw new ApplicationException("Wrong excel address!");
			arStr[0] = m.Groups[1].Value;
			arStr[1] = m.Groups[2].Value;

			m = m.NextMatch();
			if (m.Success)
			{
				arStr[2] = m.Groups[1].Value;
				arStr[3] = m.Groups[2].Value;
			}
			else
			{
				arStr[2] = arStr[0];
				arStr[3] = arStr[1];
			}
			return arStr;
		}

		private string next(string addr, int num)
		{
			string res = addr;
			for (int i = 0; i < num; i++)
			{
				res = next(res);
			}
			return res;
		}

		private string next(string addr)
		{
			try
			{
				int i = Convert.ToInt32(addr);
				return (i + 1).ToString();
			}
			catch
			{
				/*				if (addr.Length > 1)
									throw new ApplicationException("Dont use addresses with double letter!");
								char chr = addr[0];
								chr = (char)(chr + 1);
								return "" + chr;*/
				if (addr.Length == 1)
				{
					char chr = addr[0];
					if (chr == 'Z') return "AA";
					chr = (char)(chr + 1);
					return "" + chr;
				}
				if (addr.Length == 2)
				{
					if (addr[1] == 'Z')
					{
						return "" + (char)(addr[0] + 1) + 'A';
					}
					return "" + addr[0] + (char)(addr[1] + 1);
				}
				throw new ApplicationException("Dont use addresses with three letter!");
			}
		}

		private string prev(string addr)
		{
			try
			{
				int i = Convert.ToInt32(addr);
				return (i - 1).ToString();
			}
			catch
			{
				if (addr.Length == 1)
				{
					char chr = addr[0];
					if (chr == 'A')
						throw new ApplicationException("Out of range!");
					chr = (char)(chr - 1);
					return "" + chr;
				}
				if (addr.Length == 2)
				{
					if (addr[1] == 'A')
					{
						if (addr[0] == 'A') return "Z";
						return "" + (char)(addr[0] - 1) + 'Z';
					}
					return "" + addr[0] + (char)(addr[1] - 1);
				}
				throw new ApplicationException("Dont use addresses with three letter!");
			}
		}

		private ExcelApp.Range nextToRightRange(ExcelApp.Range rng)
		{
			var addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
			var str = parseAddress(addr);
			var addrNext = next(str[0]) + str[1];
			return worksheet.Range[addrNext, addrNext];
		}

		//private ExcelApp.Range moveRangeOnThisRow(ExcelApp.Range rng, string LeftColumn)
		//{
		//    string addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
		//    string[] str = parseAddress(addr);
		//    if (string.IsNullOrEmpty(LeftColumn)) LeftColumn = str[0];
		//    string addrNext = LeftColumn + str[1];
		//    return worksheet.Range[addrNext, addrNext];
		//}

		private ExcelApp.Range nextToDownRange(ExcelApp.Range rng)
		{
			//			return nextToDownRange(rng, 1);Может быть стоит экономить на вызовах раз оно не шустро работает?
			string addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
			string[] str = parseAddress(addr);
			string addrNext = str[0] + next(str[1]);
			return worksheet.Range[addrNext, addrNext];
		}

		private ExcelApp.Range nextToDownRange(ExcelApp.Range rng, int num)
		{
			string addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
			string[] str = parseAddress(addr);
			string addrNext = str[0] + next(str[1], num);
			return worksheet.Range[addrNext, addrNext];
		}

		//private ExcelApp.Range nextToUpRange(ExcelApp.Range rng)
		//{
		//    string addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
		//    string[] str = parseAddress(addr);
		//    string addrNext = str[0] + prev(str[1]);
		//    return worksheet.Range[addrNext, addrNext];
		//}

		private ExcelApp.Range nextToUpRange(ExcelApp.Range rng, string LeftColumn)
		{
			string addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
			string[] str = parseAddress(addr);
			if (string.IsNullOrEmpty(LeftColumn)) LeftColumn = str[0];
			string addrNext = LeftColumn + prev(str[1]);
			return worksheet.Range[addrNext, addrNext];
		}

		//private ExcelApp.Range nextToDownNotSingleRange(ExcelApp.Range rng, int num)
		//{
		//    string addr = rng.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
		//    string[] str = parseAddress(addr);
		//    string addrNext1 = str[0] + next(str[1], num);
		//    string addrNext2 = str[2] + next(str[3], num);
		//    return worksheet.Range[addrNext1, addrNext2];
		//}

		//private string nextToDown(string addr)
		//{
		//    string[] str = parseAddress(addr);
		//    return str[0] + next(str[1]);
		//}

		private void createXLReportByStringArray(int k)
		{
			var objectCount = _objs != null ? _objs.Length : 0;
			var chr = (char)('A' + objectCount + k);
			var name = "Array_" + k.ToString();
			if (workbook != null)
			{
				workbook.Names.Add(name, "=hiddenSheet!$" + chr + "$1", Type.Missing,
					Type.Missing, Type.Missing, Type.Missing, Type.Missing,
					Type.Missing, Type.Missing, Type.Missing, Type.Missing);
			}
			if (newWorksheet != null)
			{
				newWorksheet.Range[name, Type.Missing].Value2 = "=\"" + stringArray[k] + "\"";
			}
		}

		private void createXLReportByObjectArray(int k)
		{
			var objectCount = _objs != null ? _objs.Length : 0;
			var chr = (char)('A' + objectCount + k);
			var name = "Array_" + k.ToString();
			if (workbook != null)
			{
				workbook.Names.Add(name, "=hiddenSheet!$" + chr + "$1", Type.Missing,
					Type.Missing, Type.Missing, Type.Missing, Type.Missing,
					Type.Missing, Type.Missing, Type.Missing, Type.Missing);
			}
			if (objectArray[k] is String) objectArray[k] = "'" + objectArray[k];
			if (newWorksheet != null)
			{
				newWorksheet.Range[name, Type.Missing].Value2 = objectArray[k];
			}
		}

		private void createXLReportByDataTable(int k)
		{
			ExcelApp.Range rngTable = null;//Range обычной таблицы
			ExcelApp.Range rngSecTable = null;//Range вторичной таблицы
			ExcelApp.Range rngTop = null;
			ExcelApp.Range rngLeftBottom = null;
			ExcelApp.Range rngTopSec = null;
			ExcelApp.Range rngToGroup = null;//текущая группировка
			ExcelApp.Range Rng = null;
			ExcelApp.Range RngSec = null;

			try
			{
				DataTable dtTmp = null;
				if (dataSet != null)
					dtTmp = dataSet.Tables[tableNames[k]];
				else if (dataViews != null)
					dtTmp = dataViews[k].Table;

				if (dtTmp != null)
				{
					//dtTmp != null
					string rngExc = "";

					try
					{
						rngExc = rangeNames[k];
						rngTable = worksheet.Range[rangeNames[k], Type.Missing];
						rngExc = secTableRange;
					}
					catch
					{
						throw new ApplicationException("Имя \"" + rngExc + "\" в шаблоне не найдено!");
					}

					if (rngTable != null)
					{
						//rngTable != null
						string[] str = parseAddress(rngTable.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
						string l_c = str[0];
						string t_r = str[1];
						string r_c = str[2];
						string b_r = str[3];

						rngTop = worksheet.Range[l_c + t_r, r_c + t_r];
						rngLeftBottom = worksheet.Range[l_c + b_r, l_c + b_r];

						//Заполняем ColRanges
						object rowNumRange;//адрес для rowNum основной таблицы
						var colRanges = GetColRange(rngTop, dtTmp, out rowNumRange);//имена необходимых колонок


						int end, endTotal;
						if (dataViews != null)
							end = endTotal = dataViews[k].Count - 1;
						else
							end = endTotal = dtTmp.Rows.Count - 1;

						var curRowNum = Convert.ToInt32(t_r);//текущая строка, куда писать


						for (int j = 0, jTotal = 0; j <= end; j++)
						{//для каждой строки
							if (j > QuantityOfConstRows && jTotal != endTotal)
							{
								InsertRow(ref rngLeftBottom, rngTop, "");
							}

							foreach (object[] oArr in colRanges)
							{//для каждой колонки
								var colName = (string)oArr[0];
								var o = dataViews != null ? dataViews[k][j][colName] : dtTmp.Rows[j][colName];

								Rng = worksheet.Range[oArr[1] + curRowNum.ToString(), oArr[1] + curRowNum.ToString()];
								Rng.Value2 = GetColValue(o, colName);

								if ((bool)Rng.MergeCells && (ExcelApp.XlVAlign)Rng.VerticalAlignment == ExcelApp.XlVAlign.xlVAlignJustify)
								{
									AutoFitMergedCellRowHeight(Rng);
								}
							}

							if (rowNumRange != null)
								worksheet.Range[rowNumRange + curRowNum.ToString(), rowNumRange + curRowNum.ToString()].Value2 = j + 1;

							curRowNum++;//Увеличиваем номер строки
							jTotal++;//Увеличиваем общее количество для DataRow (в главной и вспомогательных таблицах)

							//если есть вторичная таблица
						}
					}//rngTable != null
				}//dtTmp != null
			}
			finally
			{
				NAR(rngTable);//Range обычной таблицы
				NAR(rngSecTable);//Range вторичной таблицы
				NAR(rngTop);
				NAR(rngLeftBottom);
				NAR(rngTopSec);
				NAR(rngToGroup);//текущая группировка
				NAR(Rng);
				NAR(RngSec);
			}
		}

		private void createXLReportByTwoDataTable(int k)
		{
			ExcelApp.Range rngTable = null;//Range обычной таблицы
			ExcelApp.Range rngTop = null;
			ExcelApp.Range rngLeft = null;
			ExcelApp.Range rngLeftBottom = null;
			ExcelApp.Range rngRightTop = null;
			ExcelApp.Range rngToGroup = null;//текущая группировка
			ExcelApp.Range Rng = null;

			try
			{
				DataTable dtTmp = null;
				if (dataSet != null)
				{
					dtTmp = dataSet.Tables[tableNames[k]];
				}
				else if (dataViews != null)
				{
					dtTmp = dataViews[k].Table;
				}

				if (dtTmp != null)
				{
					string rngExc = "";
					ExcelApp.Range rngSecond;//Range обычной таблицы
					try
					{
						//Находим диапазон для именовоной области.
						rngExc = rangeNames[k];
						rngTable = worksheet.Range[rangeNames[k], Type.Missing];
						rngSecond = worksheet.Range[rangeNames[1], Type.Missing];
					}
					catch
					{
						throw new ApplicationException("Имя \"" + rngExc + "\" в шаблоне не найдено!");
					}

					if (rngTable != null)
					{
						var str = parseAddress(rngTable.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
						var l_c = str[0];
						var t_r = str[1];
						var r_c = str[2];
						var b_r = str[3];
						rngTop = worksheet.Range[l_c + b_r, r_c + b_r];

						str = parseAddress(rngSecond.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
						string b_r2 = str[3];

						rngLeft = worksheet.Range[r_c + t_r, r_c + b_r2];
						rngRightTop = worksheet.Range[r_c + t_r, r_c + b_r2];

						//Заполняем ColRanges
						object rowNumRange;//адрес для rowNum основной таблицы
						ArrayList colRanges = GetColRangeWithOutFormula(rngTop, dtTmp, out rowNumRange);//имена необходимых колонок
						for (int i = 1; i < colRanges.Count - 1; i++)
						{
							InsertColumn(ref rngRightTop, rngLeft);
						}

						str = parseAddress(rngRightTop.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
						r_c = str[2];

						ExcelApp.Range RngTmp = worksheet.Range[r_c + b_r, r_c + b_r];
						RngTmp = nextToRightRange(RngTmp);
						str = parseAddress(RngTmp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
						r_c = str[2];

						rngTop = worksheet.Range[l_c + b_r, r_c + b_r];
						rngLeftBottom = worksheet.Range[l_c + b_r, r_c + b_r];

						int end, endTotal;
						if (dataViews != null)
							end = endTotal = dataViews[k].Count - 1;
						else
							end = endTotal = dtTmp.Rows.Count - 1;

						int curRowNum = Convert.ToInt32(t_r);//текущая строка, куда писать

						for (int j = 0, jTotal = 0; j <= end; j++)
						{//для каждой строки
							if (j > QuantityOfConstRows && jTotal != endTotal)
							{
								InsertRow1(ref rngLeftBottom, rngTop, "");
							}

							foreach (object[] oArr in colRanges)
							{//для каждой колонки
								var colName = (string)oArr[0];
								object o = dataViews != null ? dataViews[k][j][colName] : dtTmp.Rows[j][colName];

								Rng = worksheet.Range[oArr[1] + curRowNum.ToString(), oArr[1] + curRowNum.ToString()];
								Rng.Value2 = GetColValue(o, colName);

								if ((bool)Rng.MergeCells && (ExcelApp.XlVAlign)Rng.VerticalAlignment == ExcelApp.XlVAlign.xlVAlignJustify)
								{
									AutoFitMergedCellRowHeight(Rng);
								}
							}

							if (rowNumRange != null)
								worksheet.Range[rowNumRange + curRowNum.ToString(), rowNumRange + curRowNum.ToString()].Value2 = j + 1;

							curRowNum++;//Увеличиваем номер строки
							jTotal++;//Увеличиваем общее количество для DataRow (в главной и вспомогательных таблицах)
						}
					}//rngTable != null
				}//dtTmp != null
			}
			finally
			{
				NAR(rngTable);//Range обычной таблицы
				NAR(rngTop);
				NAR(rngLeft);
				NAR(rngLeftBottom);
				NAR(rngRightTop);
				NAR(rngToGroup);//текущая группировка
				NAR(Rng);
			}
		}

		private void InsertRow(ref ExcelApp.Range rngLeftBottom, ExcelApp.Range copyRange, string LeftColumn)
		{
			ExcelApp.Range rngTmp = null;
			try
			{
				if (InsertRows)
				{
					rngLeftBottom.EntireRow.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, false);
					if (copyRange != null)
					{//если надо что-то скопировать (при EntireRow.Insert не копируются, например, формулы)
						rngTmp = nextToUpRange(rngLeftBottom, LeftColumn);//необходимо сдвинуться наверх и если задано в определённую колонку
						copyRange.Copy(rngTmp);//при Insert происходит сдвиг rngLeftBottom
					}
				}
				else
				{//Иногда надо сохранять высоту ячеек - поэтому копируем не всю строку
					copyRange.Copy(Type.Missing);
					rngLeftBottom.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, Type.Missing);
				}
			}
			finally
			{
				NAR(rngTmp);
			}
		}

		private void InsertRow1(ref ExcelApp.Range rngLeftBottom, ExcelApp.Range copyRange, string LeftColumn)
		{
			ExcelApp.Range rngTmp = null;
			try
			{
				string[] str = parseAddress(copyRange.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
				string l_c = str[0];
				string t_r = str[1];
				string r_c = str[2];
				string b_r = str[3];

				if (InsertRows)
				{
					rngLeftBottom.EntireRow.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, false);
					//если надо что-то скопировать (при EntireRow.Insert не копируются, например, формулы)
					rngTmp = nextToUpRange(rngLeftBottom, LeftColumn);//необходимо сдвинуться наверх и если задано в определённую колонку
					rngTmp = worksheet.Range[l_c + t_r, r_c + b_r];
					copyRange.Copy(rngTmp);//при Insert происходит сдвиг rngLeftBottom
				}
				else
				{//Иногда надо сохранять высоту ячеек - поэтому копируем не всю строку
					copyRange.Copy(Type.Missing);
					rngLeftBottom.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, Type.Missing);
				}
			}
			finally
			{
				NAR(rngTmp);
			}
		}

		private void InsertColumn(ref ExcelApp.Range rngLeftBottom, ExcelApp.Range copyRange)
		{
			ExcelApp.Range rngTmp = null;
			try
			{
				if (InsertColumns)
				{
					string[] str = parseAddress(copyRange.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
					string l_c = str[0];
					string t_r = str[1];
					string r_c = str[2];
					string b_r = str[3];

					rngLeftBottom.EntireColumn.Insert(ExcelApp.XlInsertShiftDirection.xlShiftToRight, false);
					//если надо что-то скопировать (при EntireRow.Insert не копируются, например, формулы)
					//rngTmp = nextToRightRange(rngLeftBottom);//необходимо сдвинуться наверх и если задано в определённую колонку
					rngTmp = worksheet.Range[l_c + t_r, r_c + b_r];
					rngLeftBottom.Copy(rngTmp);//при Insert происходит сдвиг rngLeftBottom
				}
				else
				{//Иногда надо сохранять высоту ячеек - поэтому копируем не всю строку
					copyRange.Copy(Type.Missing);
					rngLeftBottom.Insert(ExcelApp.XlInsertShiftDirection.xlShiftToRight, Type.Missing);
				}
			}
			finally
			{
				NAR(rngTmp);
			}
		}

		//private void CopyRow(ExcelApp.Range rngDestination, ExcelApp.Range sourceRange, string LeftColumn)
		//{
		//    ExcelApp.Range rngTmp = null;
		//    try
		//    {
		//        if (sourceRange != null)
		//        {//если надо что-то скопировать
		//            rngTmp = moveRangeOnThisRow(rngDestination, LeftColumn);
		//            sourceRange.Copy(rngTmp);
		//        }
		//    }
		//    finally
		//    {
		//        NAR(rngTmp);
		//    }
		//}

		//Приводим значение в колонке к нужному виду
		private string GetColValue(object o, string colName)
		{
			string result;

			//если это лукап значение
			if (!string.IsNullOrEmpty(colName) && o != DBNull.Value &&
				objLookupFields != null && objLookupFields.ContainsKey(colName))
			{
				foreach (ListItem li in ((ComboBox.ObjectCollection)objLookupFields[colName]))
				{
					if (o != null && li.Value == (int)o)
					{
						o = li.Text;
						break;
					}
				}
			}

			if (o is DateTime)
			{
				result = !string.IsNullOrWhiteSpace(DateFormat) ? ((DateTime)o).ToShortDateString() : ((DateTime)o).ToString(DateFormat);
			}
			else result = o.ToString();

			return result;
		}

		/// <summary>
		///Получаем состав колонок для заданного Range
		///[0] - Имя колонки
		///[1] - Адрес колонки (именно колонки а не ячейки!!!)
		/// </summary>
		/// <param name="rngTemp">Где искать</param>
		/// <param name="table">Для какой таблицы искать</param>
		/// <param name="rowNumRange">Заполням и rowNumRange, если указана нумерация (тут тоже только номер колонки будет)</param>
		/// <returns>Возвращает массив с указанными выше элементами</returns>
		private ArrayList GetColRange(ExcelApp.Range rngTemp, DataTable table, out object rowNumRange)
		{
			var colRanges = new ArrayList();
			rowNumRange = null;

			if (table != null)
			{//1
				var str = parseAddress(rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
				var l_c = str[0];
				var t_r = str[1];
				var r_c = str[2];

				string addrRightTopNext = next(r_c) + t_r;//Right+1;Top

				rngTemp = worksheet.Range[l_c + t_r, l_c + t_r];//Left;Top
				string addrTemp = rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];

				int i = 0;

				do
				{
					object value2 = rngTemp.Formula;
					var val = value2 as string;
					if (val != null)
					{
						if (val.Length > 0 && val[0] == '=')
						{
							val = val.Substring(1);
							var adrTmpArr = parseAddress(addrTemp);
							//if (dataSetParam != null)
							//{
							if (table.Columns.IndexOf(val) >= 0)
								colRanges.Add(new object[] { val, adrTmpArr[0] });//берём Left Column
							else if (val.ToLower().Equals("rownum"))
								rowNumRange = adrTmpArr[0];//берём Left Column
							/*}
							else if (dataViewsParam != null)
							{
								if (dataViewsParam[k].Table.Columns.IndexOf(val) >= 0)
									colRanges.Add(new object[] {val, rngTemp.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing)});
								else if (val.ToLower().Equals("rownum") && rowNumRange != null)
									rowNumRange = rngTemp.get_Address(false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
							}*/
						}
					}
					rngTemp = nextToRightRange(rngTemp);//Двигаемся вправо
					addrTemp = rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
					i++;
				}
				while (addrTemp != addrRightTopNext && i < /*15*/50);
			}//1

			return colRanges;
		}

		private ArrayList GetColRangeWithOutFormula(ExcelApp.Range rngTemp, DataTable table, out object rowNumRange)
		{
			var colRanges = new ArrayList();
			rowNumRange = null;

			if (table != null)
			{
				var str = parseAddress(rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
				var l_c = str[0];
				for (var i = 0; i < table.Columns.Count; i++)
				{
					colRanges.Add(new object[] { table.Columns[i].ColumnName, l_c });
					l_c = next(l_c);
				}
			}
			return colRanges;
		}

		private int diff(string str1, string str2)
		{
			try
			{
				int i1 = Convert.ToInt32(str1);
				int i2 = Convert.ToInt32(str2);
				return Math.Abs(i2 - i1) + 1;
			}
			catch
			{
				throw new Exception("XLReport.cs: Ошибка!");
			}
		}

		private void createXLReportByDataTableNonDup()
		{
			ExcelApp.Range rngTable = null;
			ExcelApp.Range rngTemp = null;
			ExcelApp.Range rngLeftTop = null;
			ExcelApp.Range rngRightTop = null;
			ExcelApp.Range rngTop = null;
			ExcelApp.Range rngLeftBottom = null;

			try
			{
				var rowCount = 0;

				foreach (var rangeName in rangeNames)
				{
					try
					{
						rngTable = worksheet.Range[rangeName, Type.Missing];
					}
					catch
					{
						throw new ApplicationException("Имя \"" + rangeName + "\" в шаблоне не найдено!");
					}
					var colRanges = new ArrayList(); // адреса колонок
					object rowNumRange = null; // адрес для rowNum

					if (rngTable == null) continue;
					var str = parseAddress(rngTable.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing]);
					var l_c = str[0];
					var t_r = str[1];
					var r_c = str[2];
					var b_r = str[3];

					var currentRowNum = diff(t_r, b_r);

					rngLeftTop = worksheet.Range[l_c + t_r, l_c + t_r];
					rngRightTop = rngTable.Range[r_c + t_r, r_c + t_r];
					rngTop = worksheet.Range[l_c + t_r, r_c + t_r];

					rngLeftBottom = nextToDownRange(rngLeftTop);//rngTable.get_Range("A2","A2");

					var addrRightTopNext = next(r_c) + t_r;

					rngTemp = rngLeftTop;
					var i = 0;

					// заполняем colRanges 
					string addrTemp;
					do
					{
						object value2 = rngTemp.Formula;
						var val = value2 as string;
						if (val != null)
						{
							if (!string.IsNullOrEmpty(val) && val[0] == '=')
							{
								val = val.Substring(1);
								if (dataSet != null)
								{
									if (dataSet.Tables[tableNames[0]].Columns.IndexOf(val) >= 0)
										colRanges.Add(new object[] { val, rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing] });
									else if (val.ToLower().Equals("rownum"))
										rowNumRange = rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
									//rngTemp.Value2 = i.ToString();
								}
								else if (dataViews != null)
								{
									if (dataViews[0].Table.Columns.IndexOf(val) >= 0)
										colRanges.Add(new object[] { val, rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing] });
									else if (val.ToLower().Equals("rownum"))
										rowNumRange = rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
								}
							}
						}
						rngTemp = nextToRightRange(rngTemp);
						addrTemp = rngTemp.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
						i++;
					}
					while (addrTemp != addrRightTopNext && i < 100);

					if (dataSet != null)
					{
						if (rowCount >= dataSet.Tables[tableNames[0]].Rows.Count)
						{
							foreach (object[] oArr in colRanges)
							{
								var rngExact = worksheet.Range[oArr[1], oArr[1]];
								try
								{
									rngExact.Value2 = "";
								}
								finally
								{
									NAR(rngExact);
								}


							}
							if (rowNumRange != null)
							{
								var rngExact = worksheet.Range[rowNumRange, rowNumRange];
								try
								{
									rngExact.Value2 = "";
								}
								finally
								{
									NAR(rngExact);
								}
							}
						}
						else
						{
							for (var j = rowCount; j < rowCount + currentRowNum && j < dataSet.Tables[tableNames[0]].Rows.Count; j++)
							{
								//rngTop.Copy(Type.Missing);
								//rngLeftBottom.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, Type.Missing);
								//rngLeftBottom = nextToDownRange(rngLeftTop);//rngTable.get_Range("A2","A2");
								foreach (object[] oArr in colRanges)
								{
									var colName = (string)oArr[0];
									object[] o = { dataSet.Tables[tableNames[0]].Rows[j][colName] };
									if (o[0] != DBNull.Value && objLookupFields != null && objLookupFields.ContainsKey(colName))
									{
										foreach (var li in from ListItem li in (ComboBox.ObjectCollection)objLookupFields[colName] where li.Value == (int)o[0] select li)
										{
											o[0] = li.Text;
											break;
										}
									}
									ExcelApp.Range rngExact = worksheet.Range[oArr[1], oArr[1]];
									try
									{
										rngExact = nextToDownRange(rngExact, j - rowCount);
										if (o[0] is DateTime)
										{
											rngExact.Value2 = !string.IsNullOrWhiteSpace(DateFormat) ? ((DateTime)o[0]).ToShortDateString() : ((DateTime)o[0]).ToString(DateFormat);
										}
										else
											rngExact.Value2 = o[0].ToString();
									}
									finally
									{
										NAR(rngExact);
									}
								}
								if (rowNumRange != null)
								{
									ExcelApp.Range rngExact = worksheet.Range[rowNumRange, rowNumRange];
									try
									{
										rngExact = nextToDownRange(rngExact, j - rowCount);
										rngExact.Value2 = j + 1;
									}
									finally
									{
										NAR(rngExact);
									}
								}
							}

						}
					}
					else if (dataViews != null)
					{
						if (rowCount >= dataViews[0].Count)
						{
							foreach (object[] oArr in colRanges)
							{
								var rngExact = worksheet.Range[oArr[1], oArr[1]];
								try
								{
									rngExact.Value2 = "";
								}
								finally
								{
									NAR(rngExact);
								}
							}
							if (rowNumRange != null)
							{
								var rngExact = worksheet.Range[rowNumRange, rowNumRange];
								try
								{
									rngExact.Value2 = "";
								}
								finally
								{
									NAR(rngExact);
								}
							}
						}
						else
						{
							for (int j = rowCount; j < rowCount + currentRowNum && j < dataViews[0].Count; j++)
							{
								//rngTop.Copy(Type.Missing);
								//rngLeftBottom.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, Type.Missing);
								//rngLeftBottom = nextToDownRange(rngLeftTop);//rngTable.get_Range("A2","A2");
								foreach (object[] oArr in colRanges)
								{
									var colName = (string)oArr[0];
									var o = dataViews[0][j][colName];
									if (o != DBNull.Value && objLookupFields != null && objLookupFields.ContainsKey(colName))
									{
										foreach (ListItem li in (ComboBox.ObjectCollection)objLookupFields[colName])
										{
											if (li.Value != (int)o) continue;
											o = li.Text;
											break;
										}
									}
									ExcelApp.Range rngExact = worksheet.Range[oArr[1], oArr[1]];
									try
									{
										rngExact = nextToDownRange(rngExact, j - rowCount);
										if (o is DateTime)
										{
											rngExact.Value2 = !string.IsNullOrWhiteSpace(DateFormat) ? ((DateTime)o).ToShortDateString() : ((DateTime)o).ToString(DateFormat);
										}
										else
											rngExact.Value2 = o.ToString();
									}
									finally
									{
										NAR(rngExact);
									}

								}
								if (rowNumRange != null)
								{
									ExcelApp.Range rngExact = worksheet.Range[rowNumRange, rowNumRange];
									try
									{
										rngExact = nextToDownRange(rngExact, j - rowCount);
										rngExact.Value2 = j + 1;
									}
									finally
									{
										NAR(rngExact);
									}
								}
							}

						}
					}
					rowCount += currentRowNum;

					//if (rowCount >= dataSetParam.Tables[tableNamesParam[0]].Rows.Count)
					//	continue;

					//rngTop.Delete(ExcelApp.XlDeleteShiftDirection.xlShiftUp);
				}
			}
			finally
			{
				NAR(rngTable);
				NAR(rngTemp);
				NAR(rngLeftTop);
				NAR(rngRightTop);
				NAR(rngTop);
				NAR(rngLeftBottom);
			}
		}

		private void createXLReportByObject(int k)
		{
			ExcelApp.Range rngVal = null;
			ExcelApp.Range rngName = null;
			ExcelApp.Range rngLeftTop = null;
			ExcelApp.Range rngRightTop = null;
			ExcelApp.Range rngLeftBottom = null;
			ExcelApp.Range rngTop = null;
			ExcelApp.Range rngNK = null;
			ExcelApp.Range rngOrdNK = null;
			ExcelApp.Range rng = null;
			try
			{
				object rngValue;
				try
				{
					rngVal = worksheet.Range[classNames[k], Type.Missing];
					rngValue = rngVal.Value2;
				}
				catch
				{
					throw new ApplicationException("Имя \"" + classNames[k] + "\" в шаблоне не найдено!");
				}
				var props = rngValue as string;
				if (props != null)
				{
					var arProps = props.Split(',');

					for (var i = 0; i < arProps.Length; i++)
					{


						var pi = objTypes[k].GetProperty(arProps[i]);
						if (pi != null)
						{
							object propValue = pi.GetValue(_objs[k], null);

							string name = classNames[k] + "_" + arProps[i];

							if (propValue is MultiContainer)
							{
								var mcPropValue = propValue as MultiContainer;
								var nameNK = name + "_NK";
								var nameOrdNK = name + "_OrdValue_NK";

								rngName = worksheet.Range[name, Type.Missing];
								if (rngName != null)
								{

									rngLeftTop = rngName.Range["A1", "A1"];
									string addrLeftTop = rngLeftTop.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
									rngRightTop = rngName.End[ExcelApp.XlDirection.xlToRight];
									string addrRightTop = rngRightTop.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
									rngLeftBottom = rngName.Range["A2", "A2"];
									rngTop = worksheet.Range[addrLeftTop, addrRightTop];

									string addrRngNK = "";
									string addrRngOrdNK = "";


									rngNK = rngTop.Find(nameNK, Type.Missing,
														ExcelApp.XlFindLookIn.xlFormulas, ExcelApp.XlLookAt.xlPart,
														ExcelApp.XlSearchOrder.xlByRows, ExcelApp.XlSearchDirection.xlNext,
														false, Type.Missing, Type.Missing);
									if (rngNK != null)
									{
										rngNK = (ExcelApp.Range)rngNK.Item[2, 1];
										addrRngNK = rngNK.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
									}


									rngOrdNK = rngTop.Find(nameOrdNK, Type.Missing,
														   ExcelApp.XlFindLookIn.xlFormulas, ExcelApp.XlLookAt.xlPart,
														   ExcelApp.XlSearchOrder.xlByRows, ExcelApp.XlSearchDirection.xlNext,
														   false, Type.Missing, Type.Missing);
									if (rngOrdNK != null)
									{
										rngOrdNK = (ExcelApp.Range)rngOrdNK.Item[2, 1];
										addrRngOrdNK = rngOrdNK.Address[false, false, ExcelApp.XlReferenceStyle.xlA1, Type.Missing, Type.Missing];
									}


									for (int j = mcPropValue.Rows.Count - 1; j >= 0; j--)
									{
										if (InsertRows)
										{
											rngLeftBottom.EntireRow.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, Type.Missing);
											rngTop.Copy(rngLeftBottom);
										}
										else
										{
											rngTop.Copy(Type.Missing);
											rngLeftBottom.Insert(ExcelApp.XlInsertShiftDirection.xlShiftDown, Type.Missing);
										}
										rngLeftBottom = rngName.Range["A2", "A2"];
										if (addrRngNK != "")
											worksheet.Range[addrRngNK, addrRngNK].Value2 = "'" + mcPropValue.Rows[j]["PropValue_NK"];
										if (addrRngOrdNK != "")
											worksheet.Range[addrRngOrdNK, addrRngOrdNK].Value2 = "'" + mcPropValue.Rows[j]["OrdValue_NK"];
									}

									rngTop.Delete(ExcelApp.XlDeleteShiftDirection.xlShiftUp);
								}
							}
							else
							{
								var chr = (char)('A' + k);
								workbook.Names.Add(name, "=hiddenSheet!$" + chr + "$" + (i + 1).ToString(), Type.Missing,
												   Type.Missing, Type.Missing, Type.Missing, Type.Missing,
												   Type.Missing, Type.Missing, Type.Missing, Type.Missing);
								//rowCounter++;

								if (objLookupFields != null && objLookupFields.ContainsKey(arProps[i]))
								{
									foreach (ListItem li in (ComboBox.ObjectCollection)objLookupFields[arProps[i]])
									{// .Value;//[ind][(int)propValue+1];
										if (li.Value == (int)propValue)
										{
											propValue = li.Text;
											break;
										}
									}
								}


								string str = " ";
								rng = newWorksheet.Range[name, Type.Missing];

								if (propValue is string)
								{
									rng.NumberFormat = "@";
									if ((string)propValue != "")
										str = "'" + propValue;
								}
								else if (propValue is int)
								{
									if ((int)propValue != Int32.MinValue)
										str = ((int)propValue).ToString();
								}
								else if (propValue is DateTime)
									str = ((DateTime)propValue).ToShortDateString();
								else if (propValue is ObjectItem)
								{
									if (((ObjectItem)propValue).NK != "")
										str = "'" + ((ObjectItem)propValue).NK;
								}
								else if (propValue != null)
								{
									if (propValue.ToString() != "")
										str = propValue.ToString();
								}

								rng.Value2 = str;
							}
						}

					}
					worksheet.Range[classNames[k], Type.Missing].Value2 = "";
				}
			}
			finally
			{
				NAR(rngVal);
				NAR(rngName);
				NAR(rngLeftTop);
				NAR(rngRightTop);
				NAR(rngLeftBottom);
				NAR(rngTop);
				NAR(rngNK);
				NAR(rngOrdNK);
				NAR(rng);
			}
		}

		public string TemplateFilename
		{
			get
			{
				return templateFilename;
			}
			set
			{
				templateFilename = value;
			}
		}


		private void AutoFitMergedCellRowHeight(ExcelApp.Range Rng)
		{

			ExcelApp.Range rngTmp = null;
			ExcelApp.Worksheet sheet = null;
			try
			{

				rngTmp = (ExcelApp.Range)Rng.Cells[1, 1];
				double mergedCellRgWidth = 0;

				Rng = Rng.MergeArea;
				var rngWidth = (double)rngTmp.ColumnWidth;
				var rngHeight = (double)rngTmp.RowHeight;

				for (var i = 1; i <= Rng.Columns.Count; i++)
					mergedCellRgWidth = (double)((ExcelApp.Range)Rng.Cells[1, i]).Width + mergedCellRgWidth;

				mergedCellRgWidth = (mergedCellRgWidth * 4 / 3 - 5) / 7;
				sheet = (ExcelApp.Worksheet)Rng.Parent;
				sheet.Application.ScreenUpdating = false;

				Rng.MergeCells = false;
				rngTmp.ColumnWidth = mergedCellRgWidth;
				Rng.EntireRow.AutoFit();
				var possNewRowHeight = (double)Rng.RowHeight;
				rngTmp.ColumnWidth = rngWidth;
				Rng.MergeCells = true;
				Rng.EntireRow.RowHeight = possNewRowHeight > rngHeight ? possNewRowHeight : rngHeight;

				sheet.Application.ScreenUpdating = true;
			}
			finally
			{
				NAR(rngTmp);
				NAR(sheet);
			}
		}
		private void NAR(object obj)
		{
			if (obj == null) return;
			try
			{
				System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
			}
			catch (Exception)
			{ }
			finally
			{
				obj = null;
			}
		}
	}
}
