<?xml version="1.0" encoding="Utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" version="4.0" encoding="Windows-1251" indent="yes" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
		doctype-system="http://www.w3.org/TR/html4/loose.dtd" />
	<xsl:param name="address" />
	<xsl:param name="must" />
	<xsl:template match="/">
		<xsl:apply-templates select="body" />
	</xsl:template>
	<xsl:template match="body">
		<html>
			<head>
				<title>Информационное письмо интернет-магазина 123.ru</title>
			</head>
			<body style="background-image: url(http://s56.radikal.ru/i152/0810/6a/c436ed4f20ea.gif); background-repeat: repeat-x; margin-left: 22px; margin-top: 22px; margin-right: 0px; margin-bottom: 0px; color: #454545; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;">
				<style type="text/css">
				<![CDATA[
					<!--
a:link {
	color: #004ca9;
	text-decoration: underline;
}
a:visited {
	color: #004ca9;
	text-decoration: underline;
}
a:hover {
	color: #333333;
	text-decoration: none;
}
a:active {
	color: #333333;
	text-decoration: none;
}
-->
]]>
				</style>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="205">
							<a href="http://www.123.ru/" target="_blank">
								<img src="http://s47.radikal.ru/i118/0810/f3/aafcad5e35de.jpg" alt="интернет-магазин 123.ru"
									width="205" height="86" border="0" />
							</a>
						</td>
						<td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;">Заказ 
					по телефону: +7 (495) 933-9295<br />
					Бытовая техника и электроника с доставкой</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="48" height="26"></td>
						<td width="230"></td>
						<td width="74"></td>
						<td width="18">
							<img src="http://s54.radikal.ru/i144/0810/17/07a864be55e1.gif" width="18" height="26"
								border="0" />
						</td>
						<td>Эл.почта: <a href="mailto:info@123.eu" target="_blank" style="text-decoration: none; color: #000000;">
							info@123.ru</a></td>
					</tr>
					<tr>
						<td height="32" colspan="5" background="http://s50.radikal.ru/i128/0810/6d/11d4beb05b5f.gif">
							<img src="http://s57.radikal.ru/i157/0810/d3/8363a2cf7f6d.gif" width="32" height="32" />
						</td>
					</tr>
				</table>
				<p style="font-size: 20px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 13px;">Здравствуйте, 
			%NAME%!</p>
				<p style="font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 13px;">Это 
			информационное письмо <a href="http://www.123.ru/" target="_blank" style="text-decoration: none; color: #000000;">
				интернет-магазина 123.ru</a>.
			<br />
			Вы получаете это письмо потому, что подписались на новости &#8212; и поверьте, 
			сделали очень правильно.
			<br />
			Теперь Вы будет постоянно информированы о самых интересных акциях и товарах 
			нашего магазина.</p>
				<p style="font-size: 18px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px;">Содержание:</p>
				<xsl:if test="promo">
					<p style="font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; margin-left: 40px; font-weight:bold;">
						<a href="#special">Cпециальные акции</a>
					</p>
				</xsl:if>
				<xsl:if test="goods">
					<p style="font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; margin-left: 40px; font-weight:bold;">
						<a href="#sales">Новинки и распродажи</a>
					</p>
				</xsl:if>
				<xsl:if test="review">
					<p style="font-size: 18px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; margin-left: 40px;">Обзоры и статьи:</p>
				</xsl:if>
				<xsl:for-each select="review">
					<p style="font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; margin-left: 80px; font-weight:bold;">
						<a href="{concat('#review', @ordValue)}">
							<xsl:value-of select="header"></xsl:value-of>
						</a>
					</p>
				</xsl:for-each>
				<p style="margin-top:0px;margin-bottom:15px;"></p>
				<xsl:if test="promo">
					<table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#9e0309">
						<tr>
							<td style="font-size: 24px; color: #FFFFFF;"><a name="special"></a>Специальные акции в 123.ru</td>
						</tr>
					</table>
					<br />
					<xsl:for-each select="promo">
						<xsl:choose>
							<xsl:when test="@imageOnly = 1">
								<table width="450" border="0" cellpadding="1" cellspacing="0" bgcolor="#FFFFFF">
									<tr>
										<td>
											<a href="{@url}" target="_blank">
												<img src="{concat($address, '/bin.aspx?ID=', @image)}" alt="{name}" border="0" />
											</a>
										</td>
									</tr>
								</table>
								<br />
							</xsl:when>
							<xsl:otherwise>
								<table width="450" border="0" cellpadding="19" cellspacing="1" bgcolor="#494949">
									<tr>
										<td bgcolor="#FFFFFF">
											<table width="410" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="229" valign="top">
														<p style="font-size: 18px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 13px;">
															<xsl:value-of select="name" />
														</p>
														<p style="font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px;">
															<xsl:value-of select="description" />
														</p>
														<p style="font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px;">
															<a href="{concat($address, @url, $must)}" target="_blank" style="text-decoration: none; color: #000000;">
																<strong>Подробнее...</strong>
															</a>
														</p>
														<xsl:if test="@price != 0">
															<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 18px; font-weight: normal; line-height: 18px; margin-top: 3px; margin-right: 0px; margin-bottom: 3px; margin-left: 0px; letter-spacing: -1px;"><xsl:value-of select="format-number(@price, '#,##0.00')" /> руб.</p>
														</xsl:if>
													</td>
													<td width="181" valign="top">
														<a href="{concat($address, @url, $must)}" target="_blank">
															<img src="{concat($address, '/bin.aspx?ID=', @image)}" alt="{name}" border="0" />
														</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<br />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:if>
				<xsl:if test="goods">
					<table width="100%" border="0" cellpadding="20" cellspacing="0" bgcolor="#343434">
						<tr>
							<td style="font-size: 24px; color: #FFFFFF;"><a name="sales"></a>Новинки и распродажи</td>
						</tr>
					</table>
					<table width="450" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="16" colspan="3" background="http://s60.radikal.ru/i168/0810/b9/407807b7364c.gif">
								<img src="http://s57.radikal.ru/i157/0810/d3/8363a2cf7f6d.gif" width="16" height="16" />
							</td>
						</tr>
						<xsl:for-each select="goods">
							<tr>
								<td width="90" rowspan="2" valign="top">
									<a href="{concat($address, '/goods/OID_', @OID, $must)}" target="_blank">
										<img src="{concat($address, '/bin.aspx?ID=', @smallImage)}" alt="{shortName}"
											width="73" height="54" border="0" />
									</a>
								</td>
								<td colspan="2">
									<p style="margin-top: 5px; margin-bottom: 0px;">
										<xsl:value-of select="shortName" />
									</p>
								</td>
							</tr>
							<tr>
								<td width="220" style="color: #737373; font-size: 14px;">
									<strong><xsl:value-of select="format-number(@price, '#,##0.00')" /> руб.</strong>
								</td>
								<td width="140" background="http://i002.radikal.ru/0810/dd/36417a718f5a.gif">
									<p style="margin-left: 18px; margin-top: 0px; margin-bottom: 0px; margin-right: 0px;">
										<a href="{concat($address, '/goods/OID_', @OID, $must)}" target="_blank" style="text-decoration: none; color: #bc0007;">Полное описание <br /> на сайте 123.ru</a>
									</p>
								</td>
							</tr>
							<tr>
								<td height="16" colspan="3" background="http://s60.radikal.ru/i168/0810/b9/407807b7364c.gif">
									<img src="http://s57.radikal.ru/i157/0810/d3/8363a2cf7f6d.gif" width="16" height="16" />
								</td>
							</tr>
						</xsl:for-each>
					</table>
					<br />
				</xsl:if>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="background-image:url(http://s58.radikal.ru/i162/0810/16/a1c7b3f53203.gif); background-repeat: repeat-x;">
							<xsl:for-each select="review">
								<table width="100%" border="0" cellspacing="30" cellpadding="0">
									<tr>
										<td align="left" valign="top">
											<p style="margin-top: 0px; margin-bottom: 0px; font-size: 24px;">
												<a name="{concat('review', @ordValue)}"></a>
												<xsl:value-of select="header" />
											</p>
											<p style="margin-top: 0px; margin-bottom: 5px;">
												<xsl:value-of select="@pubDate" />
											</p>
										</td>
										<td align="right" valign="top" style="font-size: 18px;">
											<a href="/reviews" target="_blank">Все 
									обзоры и статьи<br />
									на сайте 123.ru</a>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<xsl:value-of select="body" disable-output-escaping="yes" />
										</td>
									</tr>
								</table>
								<br />
							</xsl:for-each>
							<table width="100%" border="0" cellpadding="0" cellspacing="20" bgcolor="#000000">
								<tr>
									<td width="1" valign="top">&#160;</td>
									<td width="160" valign="top">
										<p style="margin-top: 10px;">
											<a href="http://www.123.ru/" target="_blank">
												<img src="http://s61.radikal.ru/i173/0810/e6/486daa24becb.gif" alt="интернет-магазин 123.ru"
													width="160" height="71" border="0" />
												<br />
											</a>
										</p>
									</td>
									<td valign="top">
										<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; color: #FFFFFF; margin-top: 5px; margin-bottom: 20px;"><span style="font-size: 12px;">Заказ по телефону: +7 (495) 933-9295</span><br />
									Бытовая техника и электроника с доставкой</p>
										<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; color: #FFFFFF; margin-top: 5px; margin-bottom: 20px;">Не 
									хотите больше получать новости от магазина 123.ru?</p>
										<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 18px; color: #FFFFFF; margin-top: 5px; margin-bottom: 0px;">
											<a href="http://www.123.ru/denySubscription/OID_%OID%" style="color: #FFFFFF;">Отказаться 
										навсегда!</a>
										</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
