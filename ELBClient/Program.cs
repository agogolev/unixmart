﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace ELBClient
{
    internal static class Program
    {
        //проверка на единственность копии программы и для обновлений
        public static Mutex mutexStock, mutexAU;

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            if (InstanceExistsStock() && InstanceNotExistsAU())
                return;
            if (mutexAU != null)
            {
                mutexAU.WaitOne(); //wait for AutoUpdate is closed
                mutexAU.Close(); //closing mutex
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainFrm());
        }

        private static bool InstanceExistsStock()
        {
            bool createdNew;
            mutexStock = new Mutex(true, "invMutexELBClient", out createdNew);
            return false;
            //return !createdNew;
        }

        private static bool InstanceNotExistsAU()
        {
            bool createdNew;
            mutexAU = new Mutex(true, "invMutexELBClientU", out createdNew);
            return createdNew;
        }
    }
}