﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ColumnMenuExtender;

namespace ELBClient.UserControls
{
	public partial class ExtDateTimePicker : DateTimePicker
	{

		private bool SetDate;

		private const int BTNWIDTH = 33;

		//public enum dtpCustomExtensions
		//{
		//    dtpLong = 0,
		//    dtpShort = 1,
		//    dtpTime = 2,
		//    dtpShortDateShortTimeAMPM = 3,
		//    dtpShortDateLongTimeAMPM = 4,
		//    dtpShortDateShortTime24Hour = 5,
		//    dtpShortDateLongTime24Hour = 6,
		//    dtpLongDateShortTimeAMPM = 7,
		//    dtpLongDateLongTimeAMPM = 8,
		//    dtpLongDateShortTime24Hour = 9,
		//    dtpLongDateLongTime24Hour = 10,
		//    dtpSortableDateAndTimeLocalTime = 11,
		//    dtpUTFLocalDateAndShortTimeAMPM = 12,
		//    dtpUTFLocalDateAndLongTimeAMPM = 13,
		//    dtpUTFLocalDateAndShortTime24Hour = 14,
		//    dtpUTFLocalDateAndLongTime24Hour = 15,
		//    dtpShortTimeAMPM = 16,
		//    dtpShortTime24Hour = 17,
		//    dtpLongTime24Hour = 18,
		//    dtpYearAndMonthName = 19,
		//    dtpMonthNameAndDay = 20,
		//    dtpYear4Digit = 21,
		//    dtpMonthFullName = 22,
		//    dtpMonthShortName = 23,
		//    dtpDayFullName = 24,
		//    dtpDayShortName = 25,
		//    dtpShortDateAMPM = 26,
		//    dtpShortDateMorningAfternoon = 27,
		//    dtpCustom = 28
		//}

		private string mvarLinkedTo;
		private bool bDroppedDown;
		private int ButtonWidth = BTNWIDTH;
		private bool mvarShowButtons = true;
		private dtpCustomExtensions mvarFormatEx;
		private string mvarCustomFormatMessage;
		private int CheckWidth;


		private readonly ArrayList LinkToArray = new ArrayList();
		private readonly ArrayList LinkedArray = new ArrayList();

		public ExtDateTimePicker()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			base.Format = DateTimePickerFormat.Custom;
			//base.Value = MinDateTime;
			DateTimePicker_Resize(this, null);

		}

		private bool isSetBoundValue;

		[Browsable(false)]
		public DateTime BoundValue
		{
			set
			{
				if (value == DateTime.MinValue)
				{
					Value = MinDateTime;
					isSetBoundValue = false;
					txtDateTime.Text = "";
				}
				else
				{
					Value = value;
					isSetBoundValue = true;
					txtDateTime.Text = Value.ToString();
					FormatTextBox();
				}
			}
			get
			{
				if (Value == MinDateTime)
					return DateTime.MinValue;
				return Value;
			}
		}

		[Browsable(false)]
		public new DateTime Value
		{
			get { return base.Value; }
			set
			{
				base.Value = value;
			}
		}

		//OverRide Format and hide it by setting Browsable false, make it read only
		//so it can't be written to, it will always be Custom anyway
		[Browsable(false)]
		[DefaultValue(DateTimePickerFormat.Custom)]
		public new DateTimePickerFormat Format
		{
			get
			{
				return base.Format;
			}
			//set
			//{
			//	base.Format = value;
			//}
		}

		//FormatEx, extends the formatting options by allowing additional selections
		//Replaces Format
		[Browsable(true), Category("Appearance"), Description("Format Extensions replaces Format gets sets display Formats")]
		[DefaultValue(dtpCustomExtensions.dtpLong)]
		public dtpCustomExtensions FormatEx
		{
			get
			{
				return mvarFormatEx;
			}
			set
			{
				mvarFormatEx = value;
				InitialiseCustomMessage();
			}
		}

		//New Property, allows hiding of DropDown Button and Updown Button
		[Browsable(true), Category("Appearance"), Description("Hides DropDown and Spin Buttons, Allows keyed entry only.")]
		public bool ShowButtons
		{
			get
			{
				return mvarShowButtons;
			}
			set
			{
				//Do not allow Set Show Buttons when ReadOnly is true
				//all Buttons and Chexkbox are hidden when Control is Read Only
				if (!ReadOnly)
				{
					mvarShowButtons = value;
					ButtonWidth = mvarShowButtons ? BTNWIDTH : 0;
					DateTimePicker_Resize(this, null);
				}
			}
		}

		//Overrides base.ShowCheckBox
		[Browsable(true), Category("Appearance"), Description("Hides DropDown and Spin Buttons, Allows keyed entry only.")]
		public new bool ShowCheckBox
		{
			get
			{
				return base.ShowCheckBox;
			}
			set
			{
				//Do not allow set ShowCheckBox when ReadOnly is True
				//all Buttons and Chexkbox are hidden when Control is Read Only
				if (!ReadOnly)
				{
					base.ShowCheckBox = value;
					CheckWidth = base.ShowCheckBox ? BTNWIDTH : 0;
					DateTimePicker_Resize(this, null);
				}
			}
		}

		//overrie Text, we want to set Get Textbox Text
		[Browsable(false)]
		public new string Text
		{
			get
			{
				return txtDateTime.Text;
			}
			set
			{
				txtDateTime.Text = value;
				//Don't bother Formatting the Textbox if it's value is NullString
				//It will cause problems if you do
				if (value != "")
				{
					FormatTextBox();
				}
			}
		}

		//Override bas.ShowUpDown
		[Browsable(true), Category("Appearance"), Description("Uses Updown control to select dates instead of Dropdown control")]
		public new bool ShowUpDown
		{
			get
			{
				return base.ShowUpDown;
			}
			set
			{
				//Do not allow set ShowUpDown when ReadOnly is True
				//all Buttons and Checkbox are hidden when Control is Read Only
				if (!ReadOnly)
				{
					base.ShowUpDown = value;
					txtDateTime.Text = "";
				}
			}
		}

		//Override Textbox back Colour so we can add it to the Appearance List
		//and use it to set the BG colour
		[Browsable(true), Category("Appearance"), Description("The Backround Colour user to display Text and Graphics in this Control")]
		public new Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		//New Property Read Only makes it possible to set Textbox to read only
		[Browsable(true), Category("Behavior"), Description("Used to set whether the control can be edited")]
		public bool ReadOnly
		{
			get
			{
				return txtDateTime.ReadOnly;
			}
			set
			{
				//If ReadOnly is true make sure ShowCheckBox, ShowUpDown and ShowButtons 
				//are false.
				//all Buttons and Checkbox are hidden when Control is Read Only
				//Be aware of the order these properties are set
				if (value)
				{
					ShowCheckBox = false;
					ShowUpDown = false;
					ShowButtons = false;
					txtDateTime.ReadOnly = true;
				}
				else
				{
					txtDateTime.ReadOnly = false;
					ShowButtons = true;
				}
			}
		}

		//New Property Makes it possible to link control to another Datetimepicker
		[Browsable(true), Category("Behavior"), Description("Set Get another Date Picker Control that this control receives data from.")]
		public string LinkedTo
		{
			get
			{
				return mvarLinkedTo;
			}
			set
			{
				mvarLinkedTo = value;
				LinkedArray.Clear();
				if (!string.IsNullOrEmpty(mvarLinkedTo))
				{
					var splitmvarLinkedTo = mvarLinkedTo.Split(",".ToCharArray());
					foreach (var t in splitmvarLinkedTo)
					{
						LinkedArray.Add(t.Trim());
					}
				}
			}
		}

		private void DateTimePicker_Resize(object sender, EventArgs e)
		{
			txtDateTime.Location = new Point(CheckWidth+2, 2);
			txtDateTime.Size = new Size(Width - ButtonWidth - CheckWidth, Height);
		}

		private void DateTimePicker_FontChanged(Object sender, EventArgs e)
		{
			//Make sure TextBox Font =  Dtp Font
			txtDateTime.Font = Font;
		}

		private void DateTimePicker_BackColorChanged(Object sender, EventArgs e)
		{
			//Make sure TextBox BackColour =  Dtp Back Colour
			txtDateTime.BackColor = BackColor;
		}

		private void txtDateTime_BackColorChanged(Object sender, EventArgs e)
		{
			//Make sure DTP BackColour =  TextBox Back Colour
			if (txtDateTime.BackColor != BackColor)
			{
				BackColor = txtDateTime.BackColor;
			}
		}

		private void DateTimePicker_ForeColorChanged(Object sender, EventArgs e)
		{
			//Make sure TextBox Fore Colour =  Dtp Fore Colour
			txtDateTime.ForeColor = ForeColor;
		}

		private void FormatOrValueChanged(Object sender, EventArgs e)
		{
			ErrorMessage.SetError(this, "");

			//if dtp Value changed 
			//Attempt to Format the TextBox String if Text is not NullString
			if (Text != "")
			{
				try
				{
					FormatTextBox();
				}
				catch
				{
					ErrorMessage.SetError(this, "Invalid Date - " + txtDateTime.Text + ", valid format is " + mvarCustomFormatMessage);
				}
			}
		}

		private void txtDateTime_Enter(Object sender, EventArgs e)
		{
			Tooltip.SetToolTip(txtDateTime, mvarCustomFormatMessage);

			if (txtDateTime.Text.Length > 0)
			{
				txtDateTime.SelectionStart = 0;
				txtDateTime.SelectionLength = txtDateTime.Text.Length;
			}

			if (!isSetBoundValue)
			{
				SetDate = true;
				Value = DateTime.Now;
				SetDate = false;
			}
		}

		private void txtDateTime_Leave(Object sender, EventArgs e)
		{
			if (!SetDate)
			{
				SetDate = true;

				ErrorMessage.SetError(this, "");

				//Attempt to Format the TextBox String if Text is not NullString
				if (Text != "")
				{
					try
					{
						FormatTextBox();
						//if Link To is Not nullString
						//Attempt to Link to the Specified LinkTo Controls
						LinkToArray.Clear();
						if (!string.IsNullOrEmpty(mvarLinkedTo))
						{
							foreach (object t in LinkedArray)
							{
								for (var i = 0; i < Parent.Controls.Count; i++)
								{
									if (Parent.Controls[i].Name != t.ToString() || !(Parent.Controls[i] is ExtDateTimePicker)) continue;
									LinkTo = (ExtDateTimePicker)Parent.Controls[i];
									LinkToArray.Add(LinkTo);
									break;
								}
							}
						}
					}
					catch
					{
						ErrorMessage.SetError(this, "Invalid Date - " + txtDateTime.Text + ", valid format is " + mvarCustomFormatMessage);
					}
				}

				//IF the LinkTo Object has been instantiated it's ok to attempt to set it's Text Value
				foreach (object t in from object t in LinkToArray where t != null select t)
				{
					LinkTo = (ExtDateTimePicker)t;
					LinkTo.Text = Text;
				}

				SetDate = false;
			}
		}

		private void DateTimePicker_Enter(Object sender, EventArgs e)
		{
			txtDateTime.Focus();
		}

		private void ExtDateTimePicker_Leave(object sender, EventArgs e)
		{
			if (txtDateTime.Text == "")
			{
				Value = MinDateTime;
				isSetBoundValue = false;
			}
		}

		private void DateTimePicker_DropDown(Object sender, EventArgs e)
		{
			bDroppedDown = true;
		}

		private void DateTimePicker_CloseUp(object sender, EventArgs e)
		{
			if (bDroppedDown || ShowUpDown)
			{
				if (!SetDate)
				{
					txtDateTime.Text = Value.ToString();
					FormatTextBox();
					bDroppedDown = false;
					txtDateTime.Focus();
				}
			}
		}

		protected override void OnValueChanged(EventArgs eventargs)
		{
			if (bDroppedDown || ShowUpDown)
			{
				if (!SetDate)
				{
					txtDateTime.Text = Value.ToString();
					FormatTextBox();
				}
			}
		}

		//Set up the message that will diplay in the Tooltip
		//when the mouse is hovered over the control
		private void InitialiseCustomMessage()
		{
			switch (mvarFormatEx)
			{
				case dtpCustomExtensions.dtpCustom:
					mvarCustomFormatMessage = CustomFormat;
					break;
				case dtpCustomExtensions.dtpLong:
					mvarCustomFormatMessage = "Long Date (" + DateTime.Now.ToLongDateString() + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShort:
					mvarCustomFormatMessage = "Short Date (" + DateTime.Now.ToShortDateString() + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpTime:
					mvarCustomFormatMessage = "Long Time AM/PM (" + DateTime.Now.ToLongTimeString() + ")";
					CustomFormat = "HH:mm:ss yyyy-MM-dd ";
					break;
				case dtpCustomExtensions.dtpDayFullName:
					mvarCustomFormatMessage = "Day of the Week Full Name (" + DateTime.Now.ToString("dddd", Application.CurrentCulture) + ")";
					CustomFormat = "dd-MM-yyyy HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpDayShortName:
					mvarCustomFormatMessage = "Day of the Week Short Name (" + DateTime.Now.ToString("ddd", Application.CurrentCulture) + ")";
					CustomFormat = "dd-MM-yyyy HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpLongDateLongTime24Hour:
					mvarCustomFormatMessage = "Long Date Long Time 24 Hour (" + DateTime.Now.ToString("D", Application.CurrentCulture) + " " + DateTime.Now.ToString("HH:mm:ss", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpLongDateLongTimeAMPM:
					mvarCustomFormatMessage = "Long Date Long Time AM/PM (" + DateTime.Now.ToString("D", Application.CurrentCulture) + " " + DateTime.Now.ToString("hh:mm:ss tt", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpLongDateShortTime24Hour:
					mvarCustomFormatMessage = "Long Date Short Time 24 Hour (" + DateTime.Now.ToString("D", Application.CurrentCulture) + " " + DateTime.Now.ToString("HH:mm", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpLongDateShortTimeAMPM:
					mvarCustomFormatMessage = "Long Date Short Time AM/PM (" + DateTime.Now.ToString("D", Application.CurrentCulture) + " " + DateTime.Now.ToString("hh:mm tt", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpLongTime24Hour:
					mvarCustomFormatMessage = "Long Time 24 Hour (" + DateTime.Now.ToString("HH:mm:ss", Application.CurrentCulture) + ")";
					CustomFormat = "HH:mm:ss yyyy-MM-dd ";
					break;
				case dtpCustomExtensions.dtpMonthFullName:
					mvarCustomFormatMessage = "Month Full Name (" + DateTime.Now.ToString("MMMM", Application.CurrentCulture) + ")";
					CustomFormat = "MM-dd-yyyy HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpMonthNameAndDay:
					mvarCustomFormatMessage = "Month Name and Day (" + DateTime.Now.ToString("M", Application.CurrentCulture) + ")";
					CustomFormat = "dd-MM-yyyy HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpMonthShortName:
					mvarCustomFormatMessage = "Month Short Name (" + DateTime.Now.ToString("MMM", Application.CurrentCulture) + ")";
					CustomFormat = "MM-dd-yyyy HH:mm:ss";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortDateLongTime24Hour:
					mvarCustomFormatMessage = "Short Date Long Time 24 Hour (" + DateTime.Now.ToString("d", Application.CurrentCulture) + " " + DateTime.Now.ToString("HH:mm:ss", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortDateLongTimeAMPM:
					mvarCustomFormatMessage = "Short Date Long Time AM/PM (" + DateTime.Now.ToString("G", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortDateShortTime24Hour:
					mvarCustomFormatMessage = " Short Date Short Time 24 Hour (" + DateTime.Now.ToString("d", Application.CurrentCulture) + " " + DateTime.Now.ToString("HH:mm", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortDateShortTimeAMPM:
					mvarCustomFormatMessage = " Short Date Short Time AM/PM (" + DateTime.Now.ToString("d", Application.CurrentCulture) + " " + DateTime.Now.ToString("hh:mmss tt", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortTime24Hour:
					mvarCustomFormatMessage = "Short Time 24 Hour (" + DateTime.Now.ToString("HH:mm", Application.CurrentCulture) + ")";
					CustomFormat = "HH:mm:ss yyyy-MM-dd ";
					break;
				case dtpCustomExtensions.dtpShortTimeAMPM:
					mvarCustomFormatMessage = "Short Time AM/PM (" + DateTime.Now.ToString("hh:mm tt", Application.CurrentCulture) + ")";
					CustomFormat = "HH:mm:ss yyyy-MM-dd ";
					break;
				case dtpCustomExtensions.dtpSortableDateAndTimeLocalTime:
					mvarCustomFormatMessage = "Sortable Date and Local Time (" + DateTime.Now.ToString("s", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndLongTime24Hour:
					mvarCustomFormatMessage = "UTF Local Date and Long Time 24 Hour (" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndLongTimeAMPM:
					mvarCustomFormatMessage = "UTF Local Date and Long Time AM/PM (" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss tt", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndShortTime24Hour:
					mvarCustomFormatMessage = "UTF Local Date and Short Time 24 Hour (" + DateTime.Now.ToString("yyyy-MM-dd HH:mm", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndShortTimeAMPM:
					mvarCustomFormatMessage = "UTF Local Date and Short Time AM/PM (" + DateTime.Now.ToString("yyyy-MM-dd HH:mm tt", Application.CurrentCulture) + ")";
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpYear4Digit:
					mvarCustomFormatMessage = "4 Digit Year (" + DateTime.Now.ToString("yyyy", Application.CurrentCulture);
					CustomFormat = "yyyy-MM-dd HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpYearAndMonthName:
					mvarCustomFormatMessage = "Year and Month Name (" + DateTime.Now.ToString("Y", Application.CurrentCulture) + ")";
					CustomFormat = "MM-dd-yyyy HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortDateAMPM:
					mvarCustomFormatMessage = "Short Date AM/PM (" + DateTime.Now.ToString("d", Application.CurrentCulture) + " " + DateTime.Now.ToString("tt", Application.CurrentCulture) + ")";
					CustomFormat = "MM-dd-yyyy HH:mm:ss";
					break;
				case dtpCustomExtensions.dtpShortDateMorningAfternoon:
					string AMPM = "AM";
					if (DateTime.Now.Hour >= 12)
					{
						AMPM = "Afternoon";
					}
					mvarCustomFormatMessage = "Short Date Morning/Afternoon (" + DateTime.Now.ToString("d", Application.CurrentCulture) + " " + AMPM + ")";
					CustomFormat = "MM-dd-yyyy HH:mm:ss";
					break;
			}
			Tooltip.SetToolTip(txtDateTime, mvarCustomFormatMessage);
		}

		//Dispplay dates Times etc, based on Format selected
		private void FormatTextBox()
		{

			switch (mvarFormatEx)
			{
				case dtpCustomExtensions.dtpCustom:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString(CustomFormat, Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpDayFullName:
					try
					{
						Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
						//Value = DateTime.Parse(txtDateTime.Text);
					}
					catch
					{
						int aDay;
						for (aDay = 1; aDay < 8; aDay++)
						{
							var aDate = DateTime.Parse(DateTime.Now.Year.ToString() + "-01-" + aDay.ToString());
							if (aDate.DayOfWeek.ToString().ToLower() != txtDateTime.Text.ToLower() &&
								aDate.DayOfWeek.ToString().Substring(0, 3).ToLower() != txtDateTime.Text.ToLower()) continue;
							Value = DateTime.Parse(DateTime.Now.Year.ToString() + "-01-" + aDay.ToString());
							break;
						}
					}
					txtDateTime.Text = Value.ToString("dddd", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpDayShortName:
					try
					{
						Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
						//Value = DateTime.Parse(txtDateTime.Text);
					}
					catch
					{
						int aDay;
						for (aDay = 1; aDay < 8; aDay++)
						{
							var aDate = DateTime.Parse(DateTime.Now.Year.ToString() + "-01-" + aDay.ToString());
							if (aDate.DayOfWeek.ToString().ToLower() != txtDateTime.Text.ToLower() &&
								aDate.DayOfWeek.ToString().Substring(0, 3).ToLower() != txtDateTime.Text.ToLower()) continue;
							Value = DateTime.Parse(DateTime.Now.Year.ToString() + "-01-" + aDay.ToString());
							break;
						}
					}
					txtDateTime.Text = Value.ToString("ddd", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpLongDateLongTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("D", Application.CurrentCulture) + " " + Value.ToString("HH:mm:ss", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpLongDateLongTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("D", Application.CurrentCulture) + " " + Value.ToString("hh:mm:ss tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpLongDateShortTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("D", Application.CurrentCulture) + " " + Value.ToString("HH:mm", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpLongDateShortTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("D", Application.CurrentCulture) + " " + Value.ToString("hh:mm tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpLongTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("HH:mm:ss", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpMonthFullName:
					try
					{
						Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
						//Value = DateTime.Parse(txtDateTime.Text);
					}
					catch
					{
						int aMonth;
						var sMonth = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
						for (aMonth = 0; aMonth < 12; aMonth++)
						{
							if (sMonth[aMonth].ToLower() != txtDateTime.Text.ToLower() &&
								sMonth[aMonth].ToLower() != txtDateTime.Text.Substring(0, 3).ToLower()) continue;
							Value = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + (aMonth + 1) + "-" + "01");
							break;
						}
					}
					txtDateTime.Text = Value.ToString("MMMM", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpMonthShortName:
					try
					{
						Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
						//Value = DateTime.Parse(txtDateTime.Text);
					}
					catch
					{
						int aMonth;
						var sMonth = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
						for (aMonth = 0; aMonth < 12; aMonth++)
						{
							if (sMonth[aMonth].ToLower() != txtDateTime.Text.ToLower() &&
								sMonth[aMonth].ToLower() != txtDateTime.Text.Substring(0, 3).ToLower()) continue;
							Value = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + (aMonth + 1) + "-" + "01");
							break;
						}
					}
					txtDateTime.Text = Value.ToString("MMM", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpMonthNameAndDay:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("M", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortDateLongTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("d", Application.CurrentCulture) + " " + Value.ToString("HH:mms:ss", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortDateLongTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("d", Application.CurrentCulture) + " " + Value.ToString("hh:mms:ss tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortDateShortTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("d", Application.CurrentCulture) + " " + Value.ToString("HH:mm", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortDateShortTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("d", Application.CurrentCulture) + " " + Value.ToString("hh:mms tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("HH:mm", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("hh:mm tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpSortableDateAndTimeLocalTime:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("s", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndLongTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("yyyy-MM-dd", Application.CurrentCulture) + " " + Value.ToString("HH:mm:ss", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndLongTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("yyyy-MM-dd", Application.CurrentCulture) + " " + Value.ToString("hh:mm:ss tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndShortTime24Hour:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("yyyy-MM-dd", Application.CurrentCulture) + " " + Value.ToString("HH:mm", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpUTFLocalDateAndShortTimeAMPM:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("yyyy-MM-dd", Application.CurrentCulture) + " " + Value.ToString("hh:mm tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpYear4Digit:
					try
					{
						Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
						//Value = DateTime.Parse(txtDateTime.Text);
					}
					catch
					{
						Value = DateTime.Parse("01 01 " + txtDateTime.Text);
					}
					txtDateTime.Text = Value.ToString("yyyy", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpYearAndMonthName:
					try
					{
						Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
						//Value = DateTime.Parse(txtDateTime.Text);
					}
					catch
					{
						try
						{
							txtDateTime.Text = DateTime.Now.Year.ToString() + " " + int.Parse(txtDateTime.Text, Application.CurrentCulture).ToString();
						}
						catch
						{
							Value = DateTime.Parse(txtDateTime.Text + " 01");
						}
					}
					txtDateTime.Text = Value.ToString("Y", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortDateAMPM:
					if (txtDateTime.Text.Substring(txtDateTime.Text.Length - 2, 2).ToLower() == "pm")
					{
						txtDateTime.Text = txtDateTime.Text.Substring(0, txtDateTime.Text.Length - 2);
						txtDateTime.Text = txtDateTime.Text + " 13:00";
					}
					else
					{
						if (txtDateTime.Text.Substring(txtDateTime.Text.Length - 2, 2).ToLower() == "am")
						{
							txtDateTime.Text = txtDateTime.Text.Substring(0, txtDateTime.Text.Length - 2);
						}
						txtDateTime.Text = txtDateTime.Text + " 01:00";
					}
					Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToString("d", Application.CurrentCulture) + " " + Value.ToString("tt", Application.CurrentCulture);
					break;
				case dtpCustomExtensions.dtpShortDateMorningAfternoon:
					string AMPM = "Morning";
					if (txtDateTime.Text.Substring(txtDateTime.Text.Length - 2, 2).ToLower() == "pm")
					{
						txtDateTime.Text = txtDateTime.Text.Substring(0, txtDateTime.Text.Length - 2);
						txtDateTime.Text = txtDateTime.Text + " 13:00";
					}
					else
					{
						if (txtDateTime.Text.Substring(txtDateTime.Text.Length - 2, 2).ToLower() == "am")
						{
							txtDateTime.Text = txtDateTime.Text.Substring(0, txtDateTime.Text.Length - 2);
						}
						txtDateTime.Text = txtDateTime.Text + " 01:00";
					}
					Value = DateTime.Parse(txtDateTime.Text);
					if (Value.Hour >= 12)
					{
						AMPM = "Afternoon";
					}
					txtDateTime.Text = Value.ToString("d", Application.CurrentCulture) + " " + AMPM;
					break;
				case dtpCustomExtensions.dtpLong:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToLongDateString();
					break;
				case dtpCustomExtensions.dtpShort:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToShortDateString();
					break;
				case dtpCustomExtensions.dtpTime:
					Value = txtDateTime.Text == "" ? MinDateTime : DateTime.Parse(txtDateTime.Text);
					//Value = DateTime.Parse(txtDateTime.Text);
					txtDateTime.Text = Value.ToLongTimeString();
					break;
			}
		}
	}
}
