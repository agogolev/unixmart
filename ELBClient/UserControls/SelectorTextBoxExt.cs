﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.UserControls
{
	public partial class SelectorTextBoxExt : UserControl
	{
		public event EventHandler ChooseClick;
		public event EventHandler DelClick;
		private ObjectItem _boundProp;
		[Browsable(false)]
		public ObjectItem BoundProp
		{
			get
			{
				return _boundProp;
			}
			set
			{
				_boundProp = value;
				if (_boundProp != null)
				{
					TextBoxView.Text = _boundProp.NK;
				}
				else
				{
					TextBoxView.Text = "";
				}
			}
		}
		[Category("Appearance")]
		public new Color BackColor
		{
			get
			{
				return TextBoxView.BackColor;
			}
			set
			{
				TextBoxView.BackColor = value;
			}
		}
		[Category("Appearance")]
		public Color ButtonBackColor
		{
			set
			{
				Choose.BackColor = value;
				Delete.BackColor = value;
			}
			get
			{
				return Choose.BackColor;
			}
		}
		[DefaultValue(FlatStyle.Standard),
		Category("Appearance")]
		public FlatStyle ButtonFlatStyle
		{
			set
			{
				Choose.FlatStyle = value;
				Delete.FlatStyle = value;
			}
			get
			{
				return Choose.FlatStyle;
			}
		}
		[DefaultValue(BorderStyle.Fixed3D),
		Category("Appearance")]
		public BorderStyle TextBoxBorderStyle
		{
			set
			{
				TextBoxView.BorderStyle = value;
			}
			get
			{
				return TextBoxView.BorderStyle;
			}
		}
		public SelectorTextBoxExt()
		{
			InitializeComponent();
		}

		private void Choose_Click(object sender, EventArgs e)
		{
			if (ChooseClick != null)
			{
				ChooseClick(sender, e);
			}
		}

		private void Delete_Click(object sender, EventArgs e)
		{
			if (DelClick != null)
			{
				DelClick(sender, e);
			}
		}
	}
}
