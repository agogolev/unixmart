using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.UserControls
{
	public partial class AddDelPositionDataGrid
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel pnlButtons;
private System.Windows.Forms.Button btnAdd;
private System.Windows.Forms.Button btnDel;
private System.Windows.Forms.Panel pnlDataGrid;
private System.Windows.Forms.ContextMenu contextMenu1;
private System.Windows.Forms.MenuItem miDel;
private System.Windows.Forms.ImageList ilImagesUpDown;
private System.Windows.Forms.Button btnUp;
private System.Windows.Forms.Button btnDown;
private ColumnMenuExtender.ExtendedDataGrid dataGrid;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AddDelPositionDataGrid));
			this.pnlButtons = new System.Windows.Forms.Panel();
			this.btnDown = new System.Windows.Forms.Button();
			this.ilImagesUpDown = new System.Windows.Forms.ImageList(this.components);
			this.btnUp = new System.Windows.Forms.Button();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.pnlDataGrid = new System.Windows.Forms.Panel();
			this.dataGrid = new ColumnMenuExtender.ExtendedDataGrid();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miDel = new System.Windows.Forms.MenuItem();
			this.pnlButtons.SuspendLayout();
			this.pnlDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlButtons
			// 
			this.pnlButtons.Controls.Add(this.btnDown);
			this.pnlButtons.Controls.Add(this.btnUp);
			this.pnlButtons.Controls.Add(this.btnDel);
			this.pnlButtons.Controls.Add(this.btnAdd);
			this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlButtons.Location = new System.Drawing.Point(280, 0);
			this.pnlButtons.Name = "pnlButtons";
			this.pnlButtons.Size = new System.Drawing.Size(32, 176);
			this.pnlButtons.TabIndex = 0;
			// 
			// btnDown
			// 
			this.btnDown.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnDown.ImageIndex = 0;
			this.btnDown.ImageList = this.ilImagesUpDown;
			this.btnDown.Location = new System.Drawing.Point(8, 112);
			this.btnDown.Name = "btnDown";
			this.btnDown.Size = new System.Drawing.Size(24, 23);
			this.btnDown.TabIndex = 4;
			this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
			// 
			// ilImagesUpDown
			// 
			this.ilImagesUpDown.ImageSize = new System.Drawing.Size(16, 16);
			this.ilImagesUpDown.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImagesUpDown.ImageStream")));
			this.ilImagesUpDown.TransparentColor = System.Drawing.Color.Magenta;
			// 
			// btnUp
			// 
			this.btnUp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnUp.ImageIndex = 1;
			this.btnUp.ImageList = this.ilImagesUpDown;
			this.btnUp.Location = new System.Drawing.Point(8, 77);
			this.btnUp.Name = "btnUp";
			this.btnUp.Size = new System.Drawing.Size(24, 23);
			this.btnUp.TabIndex = 3;
			this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
			// 
			// Delete
			// 
			this.btnDel.Location = new System.Drawing.Point(8, 32);
			this.btnDel.Name = "Delete";
			this.btnDel.Size = new System.Drawing.Size(24, 23);
			this.btnDel.TabIndex = 2;
			this.btnDel.Text = "-";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(8, 0);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(24, 23);
			this.btnAdd.TabIndex = 1;
			this.btnAdd.Text = "+";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// pnlDataGrid
			// 
			this.pnlDataGrid.Controls.Add(this.dataGrid);
			this.pnlDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlDataGrid.Location = new System.Drawing.Point(0, 0);
			this.pnlDataGrid.Name = "pnlDataGrid";
			this.pnlDataGrid.Size = new System.Drawing.Size(280, 176);
			this.pnlDataGrid.TabIndex = 2;
			// 
			// dataGrid
			// 
			this.dataGrid.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGrid.CaptionVisible = false;
			this.dataGrid.ColumnDragEnabled = false;
			this.dataGrid.DataMember = "";
			this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid.Location = new System.Drawing.Point(0, 0);
			this.dataGrid.Name = "dataGrid";
			this.dataGrid.ShowColumnHeaderWhileDragging = true;
			this.dataGrid.ShowColumnWhileDragging = true;
			this.dataGrid.Size = new System.Drawing.Size(280, 176);
			this.dataGrid.TabIndex = 0;
			this.dataGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																												 this.extendedDataGridTableStyle1});
			this.dataGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGrid_MouseDown);
			this.dataGrid.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGrid_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dataGrid;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																																																									this.formattableTextBoxColumn1,
																																																									this.formattableTextBoxColumn2});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.MappingName = "propValue";
			this.formattableTextBoxColumn1.Width = 0;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Название";
			this.formattableTextBoxColumn2.MappingName = "propValue_NK";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																																								 this.miDel});
			// 
			// miDel
			// 
			this.miDel.Index = 0;
			this.miDel.Text = "Удалить";
			this.miDel.Click += new System.EventHandler(this.miDel_Click);
			// 
			// AddDelPositionDataGrid
			// 
			this.Controls.Add(this.pnlDataGrid);
			this.Controls.Add(this.pnlButtons);
			this.Name = "AddDelPositionDataGrid";
			this.Size = new System.Drawing.Size(312, 176);
			this.pnlButtons.ResumeLayout(false);
			this.pnlDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
