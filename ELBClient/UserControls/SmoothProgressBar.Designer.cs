﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace Invento.SmoothProgressBar
{
	public partial class SmoothProgressBar
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			// 
			// SmoothProgressBar
			// 
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Name = "SmoothProgressBar";
			this.Size = new System.Drawing.Size(436, 24);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(components != null)
					components.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
