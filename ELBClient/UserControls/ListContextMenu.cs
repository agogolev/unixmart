﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ELBClient.Classes;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Summary description for ListContextMenu.
	/// </summary>
	public partial class ListContextMenu : System.Windows.Forms.ContextMenu
	{
		ObjectProvider.ObjectProvider objectProvider = null;
		private bool useFalseDel = false;
		public Guid SelectedOID;
		public ListContextMenu()
			: base()
		{
			miNew = new MenuItem();
			miEdit = new MenuItem();
			miDelete = new MenuItem();
			miReload = new MenuItem();
			miList = new MenuItem();
			miSeparator = new MenuItem();
			miSeparator1 = new MenuItem();

			miEdit.Index = 0;
			miEdit.Text = "Редактировать";
			miEdit.Click += new System.EventHandler(this.miEdit_Click);

			miNew.Index = 1;
			miNew.Text = "Новый...";
			miNew.Shortcut = Shortcut.Ins;
			miNew.ShowShortcut = true;
			miNew.Click += new System.EventHandler(this.miNew_Click);

			miReload.Index = 2;
			miReload.Text = "Обновить";
			miReload.Shortcut = Shortcut.F5;
			miReload.ShowShortcut = true;
			miReload.Click += new System.EventHandler(this.miReload_Click);

			miSeparator.Index = 3;
			miSeparator.Text = "-";

			miList.Index = 4;
			miList.Text = "Сохранить список";
			miList.Click += new EventHandler(miList_Click);

			miSeparator1.Index = 5;
			miSeparator1.Text = "-";

			miDelete.Index = 6;
			miDelete.Text = "Удалить";
			miDelete.Shortcut = Shortcut.Del;
			miDelete.ShowShortcut = true;
			miDelete.Click += new System.EventHandler(this.miDelete_Click);


			this.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {	 miEdit,
																			 miNew,
																			 miReload,
																			 miSeparator,
																			 miDelete,
																			 miSeparator1,
																			 miList
																		});
		}

		protected override void OnPopup(EventArgs e)
		{
			if (GetList == null)
			{
				miSeparator1.Visible = false;
				miList.Visible = false;
			}
			else
			{
				miSeparator1.Visible = true;
				miList.Visible = true;
			}
			base.OnPopup(e);
		}

		void miList_Click(object sender, EventArgs e)
		{
			OnGetList(e);
		}

		public void insertItem(int index, MenuItem item)
		{
			if (index >= 0 && index < this.MenuItems.Count)
			{
				this.MenuItems.Add(item);
				for (int i = this.MenuItems.Count - 2; i >= index; i--)
					this.MenuItems[i].Index = i + 1;
			}
			else throw new Exception("MenuIndex out of range!");
		}

		private void miDelete_Click(object sender, System.EventArgs e)
		{
			Form form = null;
			if (listForm != null)
				form = listForm;
			else if (mainDataGrid != null)
				form = mainDataGrid.ParentForm;

			DeleteObject(form, SelectedOID);
		}

		public void DeleteObject(Form form, Guid OID)
		{
			if (MessageBox.Show(form, "Вы уверены, что хотите удалить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				if (objectProvider == null)
					objectProvider = ServiceUtility.ObjectProvider;
				ELBClient.ObjectProvider.ExceptionISM e;
				if (!useFalseDel)
					e = objectProvider.DeleteObject(OID);//обычный DeleteObject
				else e = objectProvider.DeleteObject(OID, true);//DeleteObject с включённым псевдо удалением (бит isDeleted в t_Object) при возникновении SqlException номер ошибки 547 (ошибка из-за constraint)
				if (e != null)
				{
					if (e.LiteralExceptionType == "System.Data.SqlClient.SqlException")
						MessageBox.Show(form, "Невозможно удалить объект!\nВозможно он связан с другим объектом.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					else if (e.LiteralExceptionType == "MetaData.SystemOIDException")
						MessageBox.Show(form, "Невозможно удалить объект!\nДанный объект используется системой.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					else MessageBox.Show(form, e.LiteralMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
					ReloadData();
			}
		}

		private void ReloadData()
		{
			if (listForm != null)
				listForm.LoadData();
			else if (mainDataGrid != null)
				mainDataGrid.LoadData();
		}

		private void miReload_Click(object sender, System.EventArgs e)
		{
			ReloadData();
		}

		public void Bind(ListForm form)
		{
			listForm = form;
		}

		public void Bind(MainDataGrid userControl)
		{
			mainDataGrid = userControl;
		}

		[DefaultValue(false), Description(@"Указывает, нужно ли использовать псевдо удаление (бит isDeleted в t_Object) при возникновении SqlException номер ошибки 547 (ошибка из-за constraint)")]
		public bool UseFalseDel
		{
			get
			{
				return useFalseDel;
			}
			set
			{
				useFalseDel = value;
			}
		}

		[
		Category("Action"),
		Description("Raises when Edit menu item is clicked")
		]
		public event EventHandler EditClick;
		[
		Category("Action"),
		Description("Raises when GetList menu item is clicked")
		]
		public event EventHandler GetList;


		protected virtual void OnEditClick(EventArgs e)
		{
			if (EditClick != null)
				EditClick(this, e);
		}

		protected virtual void OnGetList(EventArgs e)
		{
			if (GetList != null)
				GetList(this, e);
		}

		private void miEdit_Click(object sender, System.EventArgs e)
		{
			OnEditClick(e);
		}

		[
		Category("Action"),
		Description("Raises when New menu item is clicked")
		]
		public event EventHandler NewClick;

		protected virtual void OnNewClick(EventArgs e)
		{
			if (NewClick != null)
				NewClick(this, e);
		}

		private void miNew_Click(object sender, System.EventArgs e)
		{
			OnNewClick(e);
		}
	}
}
