﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace ELBClient.UserControls
{
    public class DataBoundTreeViewControl : TreeView
    {
        public bool IsModified { get; set; }

        protected override void OnAfterCheck(TreeViewEventArgs e)
        {
            IsModified = true;
            base.OnAfterCheck(e);
        }

        public void AcceptChanges()
        {
            IsModified = false;
        }

        public IList<TreeNode> GetCheckedNodes()
        {
            var list = new List<TreeNode>();
            if (!CheckBoxes) return list;
            foreach (TreeNode node in this.Nodes)
            {
                if(node.Checked) list.Add(node);
                GetCheckedNodes(node, list);
            }
            return list;
        }

        private void GetCheckedNodes(TreeNode parentNode, List<TreeNode> list)
        {
            foreach (TreeNode node in parentNode.Nodes)
            {
                if (node.Checked) list.Add(node);
                GetCheckedNodes(node, list);
            }
        }
    }
}
