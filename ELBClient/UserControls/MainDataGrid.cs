﻿using System;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ELBClient.Classes;
using ColumnMenuExtender;
using MetaData;
using System.Drawing.Design;

namespace ELBClient.UserControls
{
	public partial class MainDataGrid : System.Windows.Forms.UserControl
	{
		public BeforeShowEditFormDelegate BeforeShowEditForm; 
		//ListProvider не делать статическим!!! - иначе при отключении\подключении новый Ticket не зачитается
		private ListProvider.ListProvider listProvider;// = ServiceUtility.ListProvider; - так тоже не делать, иначе в дизинг мод будет всё падать
		private System.Data.DataSet dataSet1;
		public ELBClient.UserControls.ListContextMenu listContextMenu1;
		private ColumnMenuExtender.ColumnMenuExtender cmExtender;
		[DefaultValue(null)]
		public ColumnMenuExtender.ColumnMenuExtender CMExtender
		{
			get
			{
				return cmExtender;
			}
			set {
				cmExtender = value;
				if (cmExtender != null) cmExtender.SetUseGridMenu(dataGridISM1, true);
			}
		}
		private bool allowNew = true;
		private bool allowDel = true;
		private bool allowEdit = true;
		private bool useFalseDel = false;
		private string additionalInfo = "";
		public Hashtable InfoFields = new Hashtable();
		public MainDataGrid()
		{
			InitializeComponent();
			pnlInfo.Height = this.FontHeight + 3;

		}

		#region Loading
		public void LoadData()
		{
			if(listProvider == null)
				listProvider = ServiceUtility.ListProvider;

			OnBeforeLoad(new EventArgs());

			if(!useFalseDel)
				dataSet1 = listProvider.GetList(dataGridISM1.GetDataXml().OuterXml);//обычный GetList
			else dataSet1 = listProvider.GetList(dataGridISM1.GetDataXml().OuterXml,false);//GetList с включённой фильтрацией псевдо удалённых объектов

			dataGridISM1.TableStyles["table"].GridColumnStyles
				.OfType<IExtendedQuery>()
				.Where(cs => string.Compare(cs.FieldName, "ignore", true) == 0)
				.ToList()
				.ForEach(cs => dataSet1.Tables["Table"].Columns.Add((cs as DataGridColumnStyle).MappingName));

			int cri = dataGridISM1.CurrentRowIndex;
			dataGridISM1.Enabled = false;
			dataGridISM1.SetDataBinding(dataSet1, "table");
			dataGridISM1.Enabled = true;

			
			DataTable dt = dataSet1.Tables["table"];
			if (cri == -1) 
			{
				if (dt.Rows.Count > 0) 
					cri = 0;
			}
			else if (cri > dt.Rows.Count-1) 
				cri = dt.Rows.Count-1;
			if (cri >= 0)
			{
				dataGridISM1.CurrentRowIndex = cri;
				dataGridISM1.Select(cri);
			}

			// корявый способ избавится от дизабленных скролбаров :)
//			ParentForm.Width++;ParentForm.Width--;
			dataGridISM1.Width++;dataGridISM1.Width--;

			OnAfterLoad(new EventArgs());
			//_dataGridISM.StorableFilters[_filter.ColumnName]
			//pnlInfo.RefreshButton();
			if (pnlInfo.Visible) {
				Hashtable newInfoFields = new Hashtable();
				foreach (DictionaryEntry de in InfoFields) 
				{
					//dataGridISM1.Filters[de.Key] = de.Value.ToString();
					//ищем колонки указанные для отображения агрегатных значений
					string key = de.Key.ToString();
					if(dt.Columns[key] != null && dt.Columns[key].DataType == typeof(decimal))
					{
						decimal sum = 0;
						foreach(DataRow row in dt.Rows)
							sum += row[key] != DBNull.Value ? (decimal) row[key] : 0;
						newInfoFields[key] = sum;
					}
				}		

				foreach (DataGridColumnStyle dgcs in dataGridISM1.TableStyles["table"].GridColumnStyles)
				{
					dgcs.WidthChanged += new EventHandler(MainDataGrid_ColumnWidthChanged);
				}
		
				InfoFields = newInfoFields;
				pnlInfo.Refresh();
			}
		}

		private void dataGridISM1_Reload(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void MainDataGrid_Load(object sender, System.EventArgs e)
		{
			if (!this.DesignMode)
			{
				OnBeforeLoad(new EventArgs());
				dataGridPager1.BindToDataGrid(dataGridISM1);
				LoadData();
				dataGridPager1.UpdatePager(sender, e);
				listContextMenu1.Bind(this);
			}
			if (!AllowNew)
			{
				btnNew.Visible = false;
				listContextMenu1.miNew.Visible = false;
			}
			if(!AllowDel)
			{
				listContextMenu1.miDelete.Visible = false;
				listContextMenu1.miSeparator.Visible = false;
			}
			else//если разрешено удалять, то надо передать useFalseDel в list context menu
				listContextMenu1.UseFalseDel = useFalseDel;

			if(!AllowEdit)
				listContextMenu1.miEdit.Visible = false;
		}
		public void RebindPager() {
			dataGridPager1.BindToDataGrid(dataGridISM1);
		}
		#endregion Loading

		#region EditNewHandlers
		private void btnNew_Click(object sender, System.EventArgs e)
		{
			showEditForm(Guid.Empty);
		}

		protected virtual void showEditForm(Guid OID)
		{
			var form = FormSelection.GetEditForm(this.ClassName);
			if (form == null) return;
			form.ObjectOID = OID;
			form.MdiParent = ParentForm.MdiParent;
			form.ReloadGrid += LoadData;
			OnBeforeShowEditForm(form);
			form.Show();
			form.Activate();
		}

		protected void OnBeforeShowEditForm(EditForm form)
		{
			if (BeforeShowEditForm != null) BeforeShowEditForm(form);
		}

		private void listContextMenu1_EditClick(object sender, System.EventArgs e)
		{
			showEditForm(listContextMenu1.SelectedOID);
		}

		private void listContextMenu1_NewClick(object sender, System.EventArgs e)
		{
			showEditForm(Guid.Empty);
		}

		private void listContextMenu1_EditRowColorsClick(object sender, System.EventArgs e)
		{
			this.dataGridISM1.ShowColorForm();
		}
		#endregion EditNewHandlers

		#region dataGridISM1 Handlers		
		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGridISM1.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGridISM1.CurrentRowIndex = rowNum;
					int colNum = hti.Column;
					DataTable dt = TableInGrid.GetTable(dataGridISM1);
					listContextMenu1.SelectedOID = (Guid)dt.Rows[rowNum]["OID"];
					OnBeforeListContextMenuShow();
					listContextMenu1.Show(dataGridISM1, new Point(e.X, e.Y));
				}				
			}
		}

		private void dataGridISM1_DoubleClick(object sender, System.EventArgs e)
		{
			if(AllowEdit)
			{
				if (dataGridISM1.Hit.Type == DataGrid.HitTestType.Cell || dataGridISM1.Hit.Type == DataGrid.HitTestType.RowHeader) 
				{
					DataRow dr = dataGridISM1.GetSelectedRow();
					showEditForm((Guid)dr["OID"]);
				}
			}
			if(pnlInfo.Visible)
				pnlInfo.Refresh();
		}

		private void dataGridISM1_ActionKeyPressed(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Insert)
			{
				if (AllowNew)
					showEditForm(Guid.Empty);
			}
			else if (e.KeyCode == Keys.Delete)
			{	
				if(AllowDel)
				{
					if(dataGridISM1.CurrentRowIndex >= 0)
					{
						DataTable dt = TableInGrid.GetTable(dataGridISM1);
						listContextMenu1.DeleteObject(ParentForm, (Guid)dt.Rows[dataGridISM1.CurrentRowIndex]["OID"]);
					}
				}
			}
			else if (e.KeyCode == Keys.Enter)
			{
				if(AllowEdit)
				{
					if(dataGridISM1.CurrentRowIndex >= 0)
					{
						DataTable dt = TableInGrid.GetTable(dataGridISM1);
						showEditForm((Guid)dt.Rows[dataGridISM1.CurrentRowIndex]["OID"]);
					}
				}
			}
		}
		#endregion dataGridISM1 Handlers

		#region Public Properties
		public string ClassName
		{
			get
			{
				return dataGridISM1.StockClass;
			}
			set
			{
				dataGridISM1.StockClass = value;
			}
		}

		public string AdditionalInfo
		{
			get
			{
				return additionalInfo;
			}
			set
			{
				additionalInfo = value;
			}
		}

		[
		DefaultValue(true)
		]
		public bool AllowNew
		{
			get
			{
				return allowNew;
			}
			set
			{
				allowNew = value;
			}
		}

		[
		DefaultValue(true)
		]
		public bool AllowDel
		{
			get
			{
				return allowDel;
			}
			set
			{
				allowDel = value;
			}
		}
		
		[DefaultValue(false),Description(@"Указывает, нужно ли использовать псевдо удаление, фильтрацию псевдо удалённых объектов (бит isDeleted в t_Object)")]
		public bool UseFalseDel
		{
			get
			{
				return useFalseDel;
			}
			set
			{
				useFalseDel = value;
			}
		}

		[
		DefaultValue(true)
		]
		public bool AllowEdit
		{
			get
			{
				return allowEdit;
			}
			set
			{
				allowEdit = value;
			}
		}

		[
		Localizable(true), 
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		Editor(typeof(DataGridISM.TableStylesCollectionEditor), typeof(UITypeEditor))
		]
		public GridTableStylesCollection TableStyles
		{
			get
			{
				return dataGridISM1.TableStyles;
			}
		}

		[
		DefaultValue("")
		]
		public string OrderColumnName
		{
			get 
			{
				return dataGridISM1.OrderColumnName;
			}
			set 
			{
				dataGridISM1.OrderColumnName = value;
			}
		}

		[DefaultValue(false), Browsable(true)]
		public bool ShowInfoString
		{
			get { return pnlInfo.Visible; }
			set { pnlInfo.Visible = value; }
		}	

		[
		DefaultValue(OrderDir.Asc)
		]
		public OrderDir OrderOrderDir
		{
			get { return dataGridISM1.OrderOrderDir;	}
			set { dataGridISM1.OrderOrderDir = value; }
		}
		#endregion Public Properties

		#region Public Methods
		public void SetMultiFilter(Guid OID, string classContainer, string propName)
		{
			dataGridISM1.SetMultiFilter(OID, classContainer, propName);
		}

		public DataRow GetSelectedRow()
		{
			return dataGridISM1.GetSelectedRow();
		}
		#endregion Public Methods
		
		#region Events
		public event EventHandler BeforeListContextMenuShow;
		protected static readonly object EventBeforeLoad = new object();
		protected static readonly object EventAfterLoad = new object();		

		[
		Category("Behavior"),
		Description("Fired before loading data for DataGrid (only at first time)")
		]
		public event EventHandler BeforeLoad 
		{
			add 
			{
				Events.AddHandler(EventBeforeLoad, value);
			}
			remove 
			{
				Events.RemoveHandler(EventBeforeLoad, value);
			}
		}

		[
		Category("Behavior"),
		Description("Fired after loading data for DataGrid (each time loading data)")
		]
		public event EventHandler AfterLoad 
		{
			add 
			{
				Events.AddHandler(EventAfterLoad, value);
			}
			remove 
			{
				Events.RemoveHandler(EventAfterLoad, value);
			}
		}

		public virtual void OnBeforeLoad(EventArgs e) 
		{       
			EventHandler initHandler = (EventHandler)Events[EventBeforeLoad];
			if (initHandler != null) 
			{
				initHandler(this, e);
			} 
		}

		public virtual void OnAfterLoad(EventArgs e) 
		{       
			EventHandler initHandler = (EventHandler)Events[EventAfterLoad];
			if (initHandler != null) 
			{
				initHandler(this, e);
			} 
		}

		
		private void OnBeforeListContextMenuShow()
		{
			if(BeforeListContextMenuShow != null && listContextMenu1 != null)			
				BeforeListContextMenuShow(listContextMenu1,new EventArgs());				
		}
		#endregion Events		

		private void pnlInfo_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{			
			if(pnlInfo.Visible)
			{			
				Graphics g = e.Graphics;	
				//g.Clear(pnlInfo.BackColor);
				foreach (DictionaryEntry de in InfoFields) 
				{					
					string key = de.Key.ToString();
					Point point = GetFieldSizes(key);
					if(point != Point.Empty)
						g.DrawRectangle(SystemPens.ControlText,point.X,0,point.Y,pnlInfo.Height);
					int charWidth = (int) Math.Ceiling(g.MeasureString("c", Font, 20, StringFormat.GenericTypographic).Width);
		
					string s = (Math.Round((decimal)InfoFields[key], 2)).ToString();					
					int maxChars = Math.Min(s.Length,  (point.Y / charWidth));
					
					g.DrawString(s.Substring(0, maxChars), Font, SystemBrushes.ControlText, point.X, 2);					
				}
			}
		}

		//получаем прямоугольник рисования ячейки
		protected Point GetFieldSizes(string field)
		{			
			DataGridColumnStyle columnStyle = dataGridISM1.TableStyles[0].GridColumnStyles[field];
			//получаем номер колонки
			int index = dataGridISM1.TableStyles[0].GridColumnStyles.IndexOf(columnStyle);
			int width = columnStyle.Width; //и ее ширину
			if(index != -1)
			{
				//получаем х-координату рисования прямоугольника ячейки
				int xCoordinate = ( dataGridISM1.RowHeadersVisible ) ? dataGridISM1.RowHeaderWidth : 0;
				for ( int i = 0; i < index; i++ ) 
				{
					DataGridColumnStyle dgcs = dataGridISM1.TableStyles[dataGridISM1.DataMember].GridColumnStyles[i];
					xCoordinate += dgcs.Width;
				}			

				xCoordinate = xCoordinate - dataGridISM1.HorizScrollBarPos;
				return new Point(xCoordinate,width);
			}

			return Point.Empty;
		}


		private void MainDataGrid_ColumnWidthChanged(object sender, EventArgs e)
		{
			if(pnlInfo.Visible)
				pnlInfo.Refresh();
		}

		private void dataGridISM1_Scroll(object sender, System.EventArgs e)
		{
			if(pnlInfo.Visible)
				pnlInfo.Refresh();
		}
	}
	public delegate void BeforeShowEditFormDelegate(EditForm form);
}