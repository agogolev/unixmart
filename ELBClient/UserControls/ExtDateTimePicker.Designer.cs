﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.UserControls
{
	public partial class ExtDateTimePicker
	{
		private System.Windows.Forms.TextBox txtDateTime;
		private System.Windows.Forms.ErrorProvider ErrorMessage;
		private System.Windows.Forms.ToolTip Tooltip;
		private ExtDateTimePicker LinkTo;
		private System.ComponentModel.IContainer components;

		#region Windows Form Designer generated code

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.txtDateTime = new System.Windows.Forms.TextBox();
			this.ErrorMessage = new System.Windows.Forms.ErrorProvider(this.components);
			this.Tooltip = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.ErrorMessage)).BeginInit();
			this.SuspendLayout();
			// 
			// txtDateTime
			// 
			this.txtDateTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtDateTime.ForeColor = System.Drawing.Color.Black;
			this.txtDateTime.Location = new System.Drawing.Point(0, 0);
			this.txtDateTime.MaxLength = 50;
			this.txtDateTime.Name = "txtDateTime";
			this.txtDateTime.Size = new System.Drawing.Size(100, 13);
			this.txtDateTime.TabIndex = 0;
			this.txtDateTime.BackColorChanged += new System.EventHandler(this.txtDateTime_BackColorChanged);
			this.txtDateTime.Enter += new System.EventHandler(this.txtDateTime_Enter);
			this.txtDateTime.Leave += new System.EventHandler(this.txtDateTime_Leave);
			// 
			// ErrorMessage
			// 
			this.ErrorMessage.DataMember = "";
			// 
			// ExtDateTimePicker
			// 
			this.Controls.Add(this.txtDateTime);
			this.BackColorChanged += new System.EventHandler(this.DateTimePicker_BackColorChanged);
			this.ForeColorChanged += new System.EventHandler(this.DateTimePicker_ForeColorChanged);
			this.FormatChanged += new System.EventHandler(this.FormatOrValueChanged);
			this.CloseUp += new System.EventHandler(this.DateTimePicker_CloseUp);
			this.ValueChanged += new System.EventHandler(this.FormatOrValueChanged);
			this.DropDown += new System.EventHandler(this.DateTimePicker_DropDown);
			this.FontChanged += new System.EventHandler(this.DateTimePicker_FontChanged);
			this.Enter += new System.EventHandler(this.DateTimePicker_Enter);
			this.Leave += new System.EventHandler(this.ExtDateTimePicker_Leave);
			this.Resize += new System.EventHandler(this.DateTimePicker_Resize);
			((System.ComponentModel.ISupportInitialize)(this.ErrorMessage)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
					components.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
