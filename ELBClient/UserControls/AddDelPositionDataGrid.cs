﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ELBClient.Forms.Dialogs;
using MetaData;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Summary description for AddDelDataGrid.
	/// </summary>
	public partial class AddDelPositionDataGrid : System.Windows.Forms.UserControl
	{
		
		
		
		
		
		
		private System.ComponentModel.IContainer components;

		public OrderInfo Order = null;
		
		
		
		
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		public Hashtable Filters = new Hashtable();

		public AddDelPositionDataGrid()
		{
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			string[] columnNames;
			string[] columnHeaderNames;

			if (this.ColumnNames != null)
				columnNames = this.ColumnNames.Split(',');
			else
				columnNames = null;
			if (this.ColumnHeaderNames != null)
				columnHeaderNames = this.ColumnHeaderNames.Split(',');
			else
				columnHeaderNames = null;

			ListDialog fm = new ListDialog(this.ClassName, columnNames, columnHeaderNames, Order, Filters);

			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				Guid o = fm.SelectedOID;
				string s = fm.SelectedNK;

				if(MultiContainer != null)
				{
					try
					{
						MultiContainer.Rows.Add(new object[] {o, s, this.ClassName});
					}
					catch
					{
						MessageBox.Show(this.ParentForm, "Дубли не разрешены!","Внимание!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
					}
				}
				else
				{
					DataRow dr = DT.NewRow();
					dr[0] = o; dr[1] = s;
					try
					{
						DT.Rows.Add(dr);
					}
					catch
					{
						MessageBox.Show(this.ParentForm, "Дубли не разрешены!","Внимание!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
					}
				}
			}
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			deleteSelectedItem();
		}

		private void deleteSelectedItem()
		{
			int CRI = dataGrid.CurrentRowIndex;
			if (CRI >= 0)
			{
				Guid g = new Guid(dataGrid[CRI,0].ToString());
				DataRow dr;
				if(MultiContainer != null)
					dr = MultiContainer.Rows.Find(g);
				else dr = DT.Rows.Find(g);

				if(dr != null) dr.Delete();
			}
		}


		private MultiContainer multiContainer;
		private DataTable dt;
		private string _className;
		private string _columnNames;
		private string _columnHeaderNames;
		private string _intField = "ordValue";
		private string _addDelDataGridColumnNames;	// для того чтобы при подсовывании DataTable можно было бы указать
													// назвнаия колонок, а не использовать по умолчанию PropValue и PropValue_NK
		[
		Browsable(false)
		]
		public MultiContainer MultiContainer
		{
			set 
			{
				multiContainer = value;
				dataGrid.DataSource = multiContainer;
			}
			get
			{
				return multiContainer;
			}
		}

		[
		Browsable(false)
		]
		public DataTable DT
		{
			set 
			{
				dt = value;
				dataGrid.DataSource = dt;
			}
			get
			{
				return dt;
			}
		}
		
		[DefaultValue("")]
		public string ClassName {
			set {
				_className = value;
			}
			get {
				return _className;
			}
		}
		[DefaultValue("ordValue")]
		public string IntField {
			set {
				_intField = value;
			}
			get {
				return _intField;
			}
		}

		[
		DefaultValue("")
		]
		public string AddDelDataGridColumnNames
		{
			set 
			{
				_addDelDataGridColumnNames = value;

				string [] arr = null;
				
				if(_addDelDataGridColumnNames != null && _addDelDataGridColumnNames != "")
				{
					arr = _addDelDataGridColumnNames.Split(',');

					if(arr != null && arr.Length > 1)
					{
						formattableTextBoxColumn1.MappingName = arr[0];
						formattableTextBoxColumn2.MappingName = arr[1];
					}
				}
			}
			get
			{
				return _addDelDataGridColumnNames;
			}
		}
		
		[
		DefaultValue("")
		]
		public string ColumnNames
		{
			set 
			{
				_columnNames = value;
			}
			get
			{
				return _columnNames;
			}
		}

		[DefaultValue(false), Browsable(true)]		
		public bool ColumnHeadersVisible
		{
			set { dataGrid.ColumnHeadersVisible = value; }			
		}

		[DefaultValue("")]
		public string ColumnHeaderNames
		{
			set 
			{
				_columnHeaderNames = value;
			}
			get
			{
				return _columnHeaderNames;
			}
		}


		private void miDel_Click(object sender, System.EventArgs e)
		{
			deleteSelectedItem();
		}

		private void dataGrid_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGrid.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGrid.UnSelect(dataGrid.CurrentRowIndex);
					dataGrid.CurrentRowIndex = rowNum;
					dataGrid.Select(rowNum);
					if (e.Button == MouseButtons.Right)
					{
						contextMenu1.Show(dataGrid, new Point(e.X, e.Y));
					}
				}
			}
		}

		private void dataGrid_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				DataGrid.HitTestInfo hti = dataGrid.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGrid.UnSelect(dataGrid.CurrentRowIndex);
					dataGrid.CurrentRowIndex = rowNum;
					dataGrid.Select(rowNum);
				}
			}
		}

		public DataRow GetAfterSelectedRow() {
			dataGrid.SuspendLayout();
			try {
				if(dataGrid.DataSource == null || dataGrid.DataMember == null)
					return null;

				CurrencyManager cm = (CurrencyManager) dataGrid.BindingContext[dataGrid.DataSource, dataGrid.DataMember];
			
				cm.Position += 1;
				try {
					if(cm != null && cm.Position != -1 && cm.Current is DataRowView) {
						DataRowView rv = (DataRowView)cm.Current;
						return rv.Row;
					}
					else
						return null;
				}
				finally{
					cm.Position -= 1;
				}
			}
			finally{
				dataGrid.ResumeLayout();
			}
		}
		public DataRow GetBeforeSelectedRow() {
			dataGrid.SuspendLayout();
			try {
				if(dataGrid.DataSource == null || dataGrid.DataMember == null)
					return null;

				CurrencyManager cm = (CurrencyManager) dataGrid.BindingContext[dataGrid.DataSource, dataGrid.DataMember];
			
				cm.Position -= 1;
				try {
					if(cm != null && cm.Position != -1 && cm.Current is DataRowView) {
						DataRowView rv = (DataRowView)cm.Current;
						return rv.Row;
					}
					else
						return null;
				}
				finally{
					cm.Position += 1;
				}
			}
			finally{
				dataGrid.ResumeLayout();
			}
		}

		private void btnUp_Click(object sender, System.EventArgs e) {
			DataRow dr = dataGrid.GetSelectedRow();
			DataRow before = GetBeforeSelectedRow();
			if(before != null) {
				object ord = dr[IntField];
				dr[IntField] = before[IntField];
				before[IntField] = ord;
			}
		}

		private void btnDown_Click(object sender, System.EventArgs e) {
			DataRow dr = dataGrid.GetSelectedRow();
			DataRow after = GetAfterSelectedRow();
			if(after != null) {
				object ord = dr[IntField];
				dr[IntField] = after[IntField];
				after[IntField] = ord;
			}
		}
	}
}
