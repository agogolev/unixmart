﻿namespace ELBClient.UserControls
{
	partial class SelectorTextBoxExt
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.TextBoxView = new ColumnMenuExtender.HintTextBox();
			this.Choose = new System.Windows.Forms.Button();
			this.Delete = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TextBoxView
			// 
			this.TextBoxView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.TextBoxView.BackColor = System.Drawing.Color.White;
			this.TextBoxView.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.TextBoxView.Location = new System.Drawing.Point(0, 0);
			this.TextBoxView.Name = "TextBoxView";
			this.TextBoxView.ReadOnly = true;
			this.TextBoxView.Size = new System.Drawing.Size(189, 20);
			this.TextBoxView.TabIndex = 6;
			this.TextBoxView.TabStop = false;
			// 
			// Choose
			// 
			this.Choose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.Choose.Location = new System.Drawing.Point(197, 0);
			this.Choose.Name = "Choose";
			this.Choose.Size = new System.Drawing.Size(24, 20);
			this.Choose.TabIndex = 4;
			this.Choose.Text = "...";
			this.Choose.Click += new System.EventHandler(this.Choose_Click);
			// 
			// Delete
			// 
			this.Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.Delete.Location = new System.Drawing.Point(229, 0);
			this.Delete.Name = "Delete";
			this.Delete.Size = new System.Drawing.Size(24, 20);
			this.Delete.TabIndex = 5;
			this.Delete.Text = "X";
			this.Delete.Click += new System.EventHandler(this.Delete_Click);
			// 
			// SelectorTextBoxExt
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.TextBoxView);
			this.Controls.Add(this.Choose);
			this.Controls.Add(this.Delete);
			this.Name = "SelectorTextBoxExt";
			this.Size = new System.Drawing.Size(254, 20);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ColumnMenuExtender.HintTextBox TextBoxView;
		private System.Windows.Forms.Button Choose;
		private System.Windows.Forms.Button Delete;
	}
}
