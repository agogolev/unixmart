using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.UserControls
{
	public partial class AddDelDataGridExt
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel pnlButtons;
private System.Windows.Forms.Button btnAdd;
private System.Windows.Forms.Button btnDel;
private System.Windows.Forms.DataGrid dataGrid;
private System.Windows.Forms.Panel pnlDataGrid;
private System.Windows.Forms.ContextMenu contextMenu1;
private System.Windows.Forms.MenuItem miDel;
private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.pnlButtons = new System.Windows.Forms.Panel();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.dataGrid = new System.Windows.Forms.DataGrid();
			this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
			this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.pnlDataGrid = new System.Windows.Forms.Panel();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miDel = new System.Windows.Forms.MenuItem();
			this.pnlButtons.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
			this.pnlDataGrid.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlButtons
			// 
			this.pnlButtons.Controls.Add(this.btnDel);
			this.pnlButtons.Controls.Add(this.btnAdd);
			this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlButtons.Location = new System.Drawing.Point(280, 0);
			this.pnlButtons.Name = "pnlButtons";
			this.pnlButtons.Size = new System.Drawing.Size(32, 176);
			this.pnlButtons.TabIndex = 0;
			// 
			// Delete
			// 
			this.btnDel.Location = new System.Drawing.Point(8, 32);
			this.btnDel.Name = "Delete";
			this.btnDel.Size = new System.Drawing.Size(24, 23);
			this.btnDel.TabIndex = 2;
			this.btnDel.Text = "-";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(8, 0);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(24, 23);
			this.btnAdd.TabIndex = 1;
			this.btnAdd.Text = "+";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// dataGrid
			// 
			this.dataGrid.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGrid.CaptionVisible = false;
			this.dataGrid.DataMember = "";
			this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid.Location = new System.Drawing.Point(0, 0);
			this.dataGrid.Name = "dataGrid";
			this.dataGrid.ReadOnly = true;
			this.dataGrid.Size = new System.Drawing.Size(280, 176);
			this.dataGrid.TabIndex = 1;
			this.dataGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																												 this.dataGridTableStyle1});
			this.dataGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGrid_MouseDown);
			this.dataGrid.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGrid_MouseUp);
			// 
			// dataGridTableStyle1
			// 
			this.dataGridTableStyle1.DataGrid = this.dataGrid;
			this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																																																					this.dataGridTextBoxColumn1,
																																																					this.dataGridTextBoxColumn2});
			this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridTableStyle1.MappingName = "table";
			this.dataGridTableStyle1.ReadOnly = true;
			// 
			// dataGridTextBoxColumn1
			// 
			this.dataGridTextBoxColumn1.Format = "";
			this.dataGridTextBoxColumn1.FormatInfo = null;
			this.dataGridTextBoxColumn1.MappingName = "propValue";
			this.dataGridTextBoxColumn1.NullText = "";
			this.dataGridTextBoxColumn1.ReadOnly = true;
			this.dataGridTextBoxColumn1.Width = 0;
			// 
			// dataGridTextBoxColumn2
			// 
			this.dataGridTextBoxColumn2.Format = "";
			this.dataGridTextBoxColumn2.FormatInfo = null;
			this.dataGridTextBoxColumn2.HeaderText = "Наименование";
			this.dataGridTextBoxColumn2.MappingName = "propValue_NK";
			this.dataGridTextBoxColumn2.NullText = "";
			this.dataGridTextBoxColumn2.Width = 75;
			// 
			// pnlDataGrid
			// 
			this.pnlDataGrid.Controls.Add(this.dataGrid);
			this.pnlDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlDataGrid.Location = new System.Drawing.Point(0, 0);
			this.pnlDataGrid.Name = "pnlDataGrid";
			this.pnlDataGrid.Size = new System.Drawing.Size(280, 176);
			this.pnlDataGrid.TabIndex = 2;
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																																								 this.miDel});
			// 
			// miDel
			// 
			this.miDel.Index = 0;
			this.miDel.Text = "Удалить";
			this.miDel.Click += new System.EventHandler(this.miDel_Click);
			// 
			// AddDelDataGrid
			// 
			this.Controls.Add(this.pnlDataGrid);
			this.Controls.Add(this.pnlButtons);
			this.Name = "AddDelDataGrid";
			this.Size = new System.Drawing.Size(312, 176);
			this.pnlButtons.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
			this.pnlDataGrid.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
