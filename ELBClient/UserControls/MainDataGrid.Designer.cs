using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.UserControls
{
	public partial class MainDataGrid
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		public System.Windows.Forms.Button btnNew;
		public ColumnMenuExtender.DataGridISM dataGridISM1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel pnlInfo;
		private System.Windows.Forms.Panel pnlDataGrid;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnNew = new System.Windows.Forms.Button();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.panel2 = new System.Windows.Forms.Panel();
			this.pnlDataGrid = new System.Windows.Forms.Panel();
			this.dataGridISM1 = new ColumnMenuExtender.DataGridISM();
			this.pnlInfo = new System.Windows.Forms.Panel();
			this.dataSet1 = new System.Data.DataSet();
			this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.pnlDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.dataGridPager1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 320);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(496, 40);
			this.panel1.TabIndex = 2;
			// 
			// btnNew
			// 
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNew.Location = new System.Drawing.Point(408, 8);
			this.btnNew.Name = "btnNew";
			this.btnNew.TabIndex = 3;
			this.btnNew.Text = "Новый";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.Batch = -1;
			this.dataGridPager1.Location = new System.Drawing.Point(8, 8);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.Size = new System.Drawing.Size(288, 24);
			this.dataGridPager1.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.pnlDataGrid);
			this.panel2.Controls.Add(this.pnlInfo);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(496, 320);
			this.panel2.TabIndex = 1;
			// 
			// pnlDataGrid
			// 
			this.pnlDataGrid.Controls.Add(this.dataGridISM1);
			this.pnlDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlDataGrid.Location = new System.Drawing.Point(0, 0);
			this.pnlDataGrid.Name = "pnlDataGrid";
			this.pnlDataGrid.Size = new System.Drawing.Size(494, 294);
			this.pnlDataGrid.TabIndex = 3;
			// 
			// dataGridISM1
			// 
			this.dataGridISM1.AllowNavigation = false;
			this.dataGridISM1.AllowSorting = false;
			this.dataGridISM1.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGridISM1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridISM1.CaptionVisible = false;
			this.dataGridISM1.ColumnDragEnabled = true;
			this.dataGridISM1.DataMember = "";
			this.dataGridISM1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridISM1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridISM1.Location = new System.Drawing.Point(0, 0);
			this.dataGridISM1.Name = "dataGridISM1";
			this.dataGridISM1.Order = null;
			this.dataGridISM1.ReadOnly = true;
			this.dataGridISM1.ShowColumnHeaderWhileDragging = true;
			this.dataGridISM1.ShowColumnWhileDragging = true;
			this.dataGridISM1.Size = new System.Drawing.Size(494, 294);
			this.dataGridISM1.TabIndex = 1;
			this.dataGridISM1.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dataGridISM1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
			this.dataGridISM1.Scroll += new System.EventHandler(this.dataGridISM1_Scroll);
			this.dataGridISM1.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			this.dataGridISM1.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dataGridISM1_ActionKeyPressed);
			// 
			// pnlInfo
			// 
			this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlInfo.Location = new System.Drawing.Point(0, 294);
			this.pnlInfo.Name = "pnlInfo";
			this.pnlInfo.Size = new System.Drawing.Size(494, 24);
			this.pnlInfo.TabIndex = 2;
			this.pnlInfo.Visible = false;
			this.pnlInfo.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlInfo_Paint);
			// 
			// dataSource
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// listContextMenu1
			// 
			this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
			this.listContextMenu1.NewClick += new System.EventHandler(this.listContextMenu1_NewClick);
			// 
			// MainDataGrid
			// 
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "MainDataGrid";
			this.Size = new System.Drawing.Size(496, 360);
			this.Load += new System.EventHandler(this.MainDataGrid_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.pnlDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
