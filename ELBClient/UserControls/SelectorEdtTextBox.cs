﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using ELBClient.Forms.Dialogs;
using ColumnMenuExtender.UserControls;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Summary description for SelectorTextBox.
	/// </summary>
	public partial class SelectorEdtTextBox : BaseSelectorEdtTextBox
	{
        public event EventHandler BeforeEdit;
		protected override BaseListDialog GetBaseListDialog(
		    string _className,
		    IList<string> _fieldNames,
		    IList<string> _columnNames,
		    IList<string> _columnHeaderNames,
		    IList<string> _formatRows, 
		    OrderInfo _Order, 
		    Hashtable _Filters, 
		    int[] _widths) {
			return new ListDialog(_className, _fieldNames, _columnNames, _columnHeaderNames, _formatRows, _Order, _Filters, _widths);
		}

		protected override BaseListDialog GetBaseListDialog(
		    string _className,
		    IList<string> _columnNames,
		    IList<string> _columnHeaderNames,
		    IList<string> _formatRows, 
			OrderInfo _Order, 
		    Hashtable _Filters, 
		    int[] _widths) {
			return new ListDialog(_className, _columnNames, _columnHeaderNames, _formatRows, _Order, _Filters, _widths);
		}
		protected override void btnChoose_Click(object sender, EventArgs e) {
			if(ClassName == "CBinaryData") 
			{
				ListImagesDialog fm = new ListImagesDialog();
				if(fm.ShowDialog(this) == DialogResult.OK) 
				{
					BoundProp = new ObjectItem(fm.SelectedOID, fm.SelectedName, "CBinaryData");
					OnChanged();
				}
			}
			else base.btnChoose_Click (sender, e);
		}
		public override void btnEdit_Click(object sender, EventArgs e)
		{
			EditForm form = Classes.FormSelection.GetEditForm(this.ClassName);
			if(form != null) {
				//switch (this.ClassName) {
				//	case "CArticle":
				//		if(BoundProp.OID == Guid.Empty) {
				//			Classes.CArticle art = new Classes.CArticle();
				//			art.ArticleType = 3;
				//			ObjectProvider.ObjectProvider provider = Classes.ServiceUtility.ObjectProvider;
				//			ObjectProvider.ExceptionISM exISM;
				//			string xml = provider.SaveObject(art.SaveXml(), out exISM);
				//			art.LoadXml(xml);
				//			BoundProp = new ObjectItem(art.OID, "", "CArticle");
				//		}
				//		break;
				//}
				form.ObjectOID = BoundProp.OID;
			    BeforeEdit?.Invoke(form, EventArgs.Empty);
			    if(form.ShowDialog(this) == DialogResult.OK) {
					BoundProp = new ObjectItem(form.ObjectOID, form.ObjectNK, form.ObjectClassName);
					OnChanged();
				}
			}
		}
	}
}
