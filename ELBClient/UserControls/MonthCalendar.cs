﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Event handler for SelectedDate changed
	/// </summary>
	public delegate void SelectedDateChangedEventHandler(object sender, SelectedDateChangedEventArgs e);

	public delegate void MarketingActionEventHandler(object sender, MarketingActionEventArgs e);
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public partial class MonthCalendar : System.Windows.Forms.UserControl
	{
		
		
		
		
		
		
		
		
		
		
		

		#region Private variables

		private int pWidth;
		private int pHeight;
		private int pPicWidth;
		private int pPicHeight;
		private int intX=0;
		private int intY=0;
		private int pintMonth=DateTime.Now.Month;
		private int pintYear=DateTime.Now.Year;
		private int pintDay=0;
		private int pintMonthPrev=0;
		private int pintMonthNext=0;
		private DateTime[] pBoldedDates=null;
		private DateTime p_SelectedDate=DateTime.Now;
		//private DateTime p_SelectedDatePrevMonth;//=DateTime.Now;
		//private DateTime p_SelectedDateNextMonth;//=DateTime.Now;
		private bool p_ShowGrid = true;
		private bool p_ShowPrevNextButton = true;
		private bool p_AbbreviateWeekDayHeader=true;
		private bool p_ShowCurrentMonthInDay=false;
		private bool p_DisplayWeekendsDarker = true;
		private Color p_BackgroundColor=Color.LightSteelBlue;
		private Color p_GridColor=Color.Black;
		private Color p_HeaderColor=Color.LightSteelBlue;
		private Color p_BoldedDateFontColor=Color.Red;
		private Color p_ActiveMonthColor=Color.White;
		private Color p_InactiveMonthColor=Color.Silver;
		private Color p_SelectedDayColor=Color.LightSteelBlue;
		private Color p_SelectedDayFontColor=Color.White;
		private Color p_NonselectedDayFontColor=Color.Black;
		private Font p_ApptFont = new Font(FontFamily.GenericSansSerif,10,FontStyle.Bold);
		private Font p_NoApptFont = new Font(FontFamily.GenericSansSerif,10,FontStyle.Regular);
		private Font p_HeaderFont = new Font(FontFamily.GenericSansSerif,9,FontStyle.Regular);
		private Rectangle[,] rects;
		private Rectangle[] rectDays;
		//private string[] strDays = new string[7] {"Воскресение","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"};
		private string[] strDays = new string[7] {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
		private string[] strAbbrDays = new string[7] {"Пн","Вт","Ср","Чт","Пт","Сб","Вс"};
		private string[] strMonths = new string[12] {"Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"};
		private string[] strAbbrMonths = new string[12] {"Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"};

//		private string[,] strDates;
//		private string[,] pstrMonth;
		private int[,][] intDateInfo;
		
		private bool bDesign = true;

		private Pen penGrid;
		//private float fFontSize1;
		//private float fFontSize2;
		private Color ActiveDarker;
		private Color InActiveDarker;
		private SolidBrush brushHeader;
		private SolidBrush brushActive;
		private SolidBrush brushInactive;
		private SolidBrush brushActiveDarker;
		private SolidBrush brushInactiveDarker;
		private SolidBrush brushSelectedDay;
		private SolidBrush brushSelectedDayFont;
		private SolidBrush brushNonselectedDayFont;
		private SolidBrush brushBoldedDateFont;

		private SolidBrush brushEvent = new SolidBrush(Color.Aqua);

		//private Font fontBody;
		private StringFormat sf;
		private StringFormat sfHeader;

		private EventMarketingAction[] marketingActions = null;

		#endregion Private variables

		#region Custom events
		/// <summary>
		/// Event handler definition
		/// </summary>
		public event SelectedDateChangedEventHandler SelectedDateChanged;

		/// <summary>
		/// Raises event by invoking delegate
		/// </summary>
		/// <param name="eventArgs"></param>
		protected virtual void OnSelectedDateChanged(SelectedDateChangedEventArgs eventArgs)
		{
			if(SelectedDateChanged!=null)
			{
				SelectedDateChanged(this,eventArgs);
			}
		}

		public event MarketingActionEventHandler MarketingActionClicked;

		private void OnMarketingActionClicked(MarketingActionEventArgs e)
		{
			if (MarketingActionClicked != null)
			{
				MarketingActionClicked(this, e);
			}
		}
		#endregion Custom events

		#region Properties

		[Category("Marketing"),
		Browsable(false)]
		public EventMarketingAction[] MarketingActions
		{
			get {return marketingActions;}
			set {marketingActions = value;}
		}
		/// <summary>
		/// The year of the active month
		/// </summary>
		[Description("The year portion of the month, year to display.")]
		[Category("MonthCalendar")]
		public int intYear
		{
			get
			{
				if(pintYear==0)
				{
					return DateTime.Now.Year;

				}
				else
				{
					return pintYear;
				}
			}
			set
			{
				pintYear=value;
				FillDates();
			}
		}
		
		/// <summary>
		/// The number of the active month.
		/// </summary>
		[Description("The month portion of the month, year to display.")]
		[Category("MonthCalendar")]
		public int intMonth
		{
			get
			{
				if(pintMonth==0)
				{
					return DateTime.Now.Month;

				}
				else
				{
					return pintMonth;
				}
			}
			set
			{
				if(value<1)
				{
					pintMonth=12;
					pintYear--;
				}
				else if(value>12)
				{
					pintMonth=1;
					pintYear++;
				}
				else
				{
					pintMonth=value;
				}

				if(pintMonth>1)
				{
					pintMonthPrev = pintMonth-1;
				}
				else
				{
					pintMonthPrev=12;
				}
				if(pintMonth<12)
				{
					pintMonthNext=pintMonth+1;
				}
				else
				{
					pintMonthNext=1;
				}


				//FillDates();
			}
		}
		
		/// <summary>
		/// Array of dates to be bolded.
		/// </summary>
		[Description("Bolded dates.")]
		[Category("MonthCalendar")]
		public DateTime[] BoldedDates
		{
			get
			{

					return pBoldedDates;

			}
			set
			{
				pBoldedDates = value;
				//look at each date and see if it corresponds with a date in the grid
				if(pBoldedDates!=null)
				{
					Array.Sort(pBoldedDates);
				}

				
			}

		}
			
		/// <summary>
		/// The day number of the selected date.
		/// </summary>
		[Description("The day number.")]
		[Category("MonthCalendar")]
		public int intDay
		{
			get
			{
				if(pintDay==0)
				{
					return DateTime.Now.Day;

				}
				else
				{
					return pintDay;
				}
			}
			set
			{
				pintDay = value;
			}

		}
		
		/// <summary>
		/// The selected date.
		/// </summary>
		[Description("The selected date.")]
		[Category("MonthCalendar")]
		public DateTime SelectedDate
		{
			get
			{
				return p_SelectedDate;
			}
			set
			{
				p_SelectedDate = value;
				intMonth = p_SelectedDate.Month;
				intYear = p_SelectedDate.Year;

			}

		}

		/// <summary>
		/// Property - Determines whether the Previous and Next buttons are visible.
		/// </summary>
		[Description("Determines whether the Previous and Next buttons are visible.")]
		[Category("MonthCalendar")]
		public bool ShowPrevNextButton
		{
			get
			{
				return p_ShowPrevNextButton;
			}
			set
			{
				p_ShowPrevNextButton = value;
				this.btnNext.Visible=p_ShowPrevNextButton;
				this.btnPrev.Visible=p_ShowPrevNextButton;
			}
		}

		/// <summary>
		/// Property - Determines whether the weekends are displayed darker.
		/// </summary>
		[Description("Determines whether the weekends are displayed darker.")]
		[Category("MonthCalendar")]
		public bool DisplayWeekendsDarker
		{
			get
			{
				return p_DisplayWeekendsDarker;
			}
			set
			{
				p_DisplayWeekendsDarker = value;
				picMPK_Cal.Invalidate();
			}
		}

		/// <summary>
		/// Property - Determines whether or not the grid is drawn.
		/// </summary>
		[Description("Determines whether or not the grid is drawn.")]
		[Category("MonthCalendar")]
		public bool ShowGrid
		{
			get
			{
				return p_ShowGrid;
			}
			set
			{
				p_ShowGrid = value;
				picMPK_Cal.Invalidate();
			}
		}

		/// <summary>
		/// Property - Determines whether or not to abbreviate the weekday in the header.
		/// </summary>
		[Description("Determines whether or not to abbreviate the weekday in the header.")]
		[Category("MonthCalendar")]
		public bool AbbreviateWeekDayHeader
		{
			get
			{
				return p_AbbreviateWeekDayHeader;
			}
			set
			{
				p_AbbreviateWeekDayHeader = value;
			}
		}

		/// <summary>
		/// Property - Determines whether or not to display the month name in each day of the current month.
		/// </summary>
		[Description("Determines whether or not to display the month name in each day of the current month.")]
		[Category("MonthCalendar")]
		public bool ShowCurrentMonthInDay
		{
			get
			{
				return p_ShowCurrentMonthInDay;
			}
			set
			{
				p_ShowCurrentMonthInDay = value;
			}
		}

		/// <summary>
		/// Property - Grid color.
		/// </summary>
		[Description("Grid color.")]
		[Category("MonthCalendar")]
		public Color GridColor
		{
			get
			{
				return p_GridColor;
			}
			set
			{
				p_GridColor=value;
				this.picMPK_Cal.Invalidate();
			}
		}

		/// <summary>
		/// Property - Header background color.
		/// </summary>
		[Description("Header background color.")]
		[Category("MonthCalendar")]
		public Color HeaderColor
		{
			get
			{
				return p_HeaderColor;
			}
			set
			{
				p_HeaderColor=value;
				this.picMPK_Cal.Invalidate();
			}
		}

		/// <summary>
		/// Property - Control background color.
		/// </summary>
		[Description("Background color.")]
		[Category("MonthCalendar")]
		public Color BackgroundColor
		{
			get
			{
				return p_BackgroundColor;
			}
			set
			{
				p_BackgroundColor=value;
				this.BackColor = p_BackgroundColor;
			}
		}

		/// <summary>
		/// Property - Color of the active month.
		/// </summary>
		[Description("Color of the active month.")]
		[Category("MonthCalendar")]
		public Color ActiveMonthColor
		{
			get
			{
				return p_ActiveMonthColor;
			}
			set
			{
				p_ActiveMonthColor = value;
				this.picMPK_Cal.Invalidate();
			}
		}

		/// <summary>
		/// Property - Color of the inactive month.
		/// </summary>
		[Description("Color of the inactive month.")]
		[Category("MonthCalendar")]
		public Color InactiveMonthColor
		{
			get
			{
				return p_InactiveMonthColor;
			}
			set
			{
				p_InactiveMonthColor = value;
				this.picMPK_Cal.Invalidate();
			}
		}

		/// <summary>
		/// Property - Color for bolded dates font.
		/// </summary>
		[Description("Color for bolded dates font.")]
		[Category("MonthCalendar")]
		public Color BoldedDateFontColor
		{
			get
			{
				return p_BoldedDateFontColor;
			}
			set
			{
				p_BoldedDateFontColor = value;
			}
		}

		/// <summary>
		/// Property - Color of the background for selected day.
		/// </summary>
		[Description("Color of the background for selected day.")]
		[Category("MonthCalendar")]
		public Color SelectedDayColor
		{
			get
			{
				return p_SelectedDayColor;
			}
			set
			{
				p_SelectedDayColor = value;
			}
		}

		/// <summary>
		/// Property - Color of the text for selected day.
		/// </summary>
		[Description("Color of the text for selected day.")]
		[Category("MonthCalendar")]
		public Color SelectedDayFontColor
		{
			get
			{
				return p_SelectedDayFontColor;
			}
			set
			{
				p_SelectedDayFontColor = value;
			}
		}

		/// <summary>
		/// Property - Color of the text for non-selected days.
		/// </summary>
		[Description("Color of the text for non-selected days.")]
		[Category("MonthCalendar")]
		public Color NonselectedDayFontColor
		{
			get
			{
				return p_NonselectedDayFontColor;
			}
			set
			{
				p_NonselectedDayFontColor = value;
			}
		}

		/// <summary>
		/// Property - Font for bolded days.
		/// </summary>
		[Description("Font for bolded days.")]
		[Category("MonthCalendar")]
		public Font ApptFont
		{
			get
			{
				return p_ApptFont;
			}
			set
			{
				p_ApptFont = value;
			}
		}

		/// <summary>
		/// Property - Font for not bolded days.
		/// </summary>
		[Description("Font for not bolded days.")]
		[Category("MonthCalendar")]
		public Font NoApptFont
		{
			get
			{
				return p_NoApptFont;
			}
			set
			{
				p_NoApptFont = value;
			}
		}

		/// <summary>
		/// Property - Header text font.
		/// </summary>
		[Description("Header text font.")]
		[Category("MonthCalendar")]
		public Font HeaderFont
		{
			get
			{
				return p_HeaderFont;
			}
			set
			{
				p_HeaderFont = value;
			}
		}



		#endregion Properties
		
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public MonthCalendar()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
			SelectedDate = DateTime.Now;
		}

		#endregion Constructor

		#region Control events



		private void MonthCalendar_SizeChanged(object sender, System.EventArgs e)
		{
			pWidth = this.Size.Width;
			pPicWidth=pWidth;
			pHeight = this.Size.Height;
			pPicHeight=pHeight-35;
			picMPK_Cal.Width=pPicWidth;
			picMPK_Cal.Height=pPicHeight;
			//keep the buttons in the same place
			btnPrev.Left = 5;
			btnNext.Left = this.Width - btnNext.Width - 5;
			lblMonth.Left = (this.Width - lblMonth.Width)/2;
			rects = CreateGrid(picMPK_Cal.Width,picMPK_Cal.Height);
			picMPK_Cal.Invalidate();
		}

		private void picMPK_Cal_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{

			intX = e.X;
			intY = e.Y;
			int inti = 0;
			int intj = 0;
			for(int j = 0;j<6;j++)
			{
				for(int i = 0;i<7;i++)
				{
					if(rects[i,j].Contains(intX,intY))
					{
						inti = i;
						intj = j;
						intDay = intDateInfo[i,j][1];
						//intMonth=intDateInfo[shopType,j][0];
						int y = 0;
						int m = intDateInfo[i,j][0]-intMonth;
						if (m > 1)
							y = -1;
						else if (m < -1)
							y = 1;
						//raise date changed event
						this.SelectedDate = new DateTime(intYear + y,intDateInfo[i,j][0],intDateInfo[i,j][1]);

						//show event hint
						if (m == 0 && eventsArray.ContainsKey(this.SelectedDate.ToShortDateString()))
						{
							OnMarketingActionClicked(new MarketingActionEventArgs((EventMarketingAction)((ArrayList)eventsArray[this.SelectedDate.ToShortDateString()])[0]));
							//MessageBox.Shows(((EventMarketingAction)((ArrayList)eventsArray[this.SelectedDate.ToShortDateString()])[0]).Name);
						}

						SelectedDateChangedEventArgs eventArgs = new SelectedDateChangedEventArgs(SelectedDate);
						OnSelectedDateChanged(eventArgs);
						
					}
				}

			}
			picMPK_Cal.Invalidate();
		}

		//this is not used when displaying appointment data
		private void btnPrev_Click(object sender, System.EventArgs e)
		{
			if(intMonth==1)
			{
				intMonth=12;
				intYear=intYear-1;
			}
			else
			{
				intMonth--;
			}
			picMPK_Cal.Invalidate();

		}

		//this is not used when displaying appointment data
		private void btnNext_Click(object sender, System.EventArgs e)
		{
			if(intMonth==12)
			{
				intMonth=1;
				intYear++;
			}
			else
			{
				intMonth++;
			}
			picMPK_Cal.Invalidate();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void picMPK_Cal_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			bool bActive = false;
			bool bSelected = false;
			bool bApptOnDate = false;
			bool bEvent = false;
			if(!bDesign)
			{
				//the calendar is created as a 7 x 6 grid drawn onto the picture box
				//the data to be displayed in the calendar is stored in a 7 x 6 array of arrays
				FillDates();
				CreateGraphicObjects();
				Rectangle rect = e.ClipRectangle;
				rect.Y +=20;
				rect.Height-=20;
				//graphics object for paint event
				Graphics g = e.Graphics;
				g.FillRectangle(Brushes.DarkGray,rect);
				string str;
				//day of week header
				for(int i = 0;i<7;i++)
				{
					//draw weekday header rectangle
					g.FillRectangle(brushHeader,rectDays[i]);
					//draw rectangle for grid if requested
					if(p_ShowGrid)
					{
						g.DrawRectangle(new Pen(Color.Black,2),rectDays[i]);
					}
					//draw weekday header
					if(p_AbbreviateWeekDayHeader)
					{
						g.DrawString( strAbbrDays[i],p_HeaderFont, Brushes.Black,rectDays[i],sfHeader);
					}
					else
					{
						g.DrawString( strDays[i], p_HeaderFont, Brushes.Black,rectDays[i],sfHeader);
					}

				}
				//actual calendar 
				for(int j = 0;j<6;j++)
				{
					for(int i = 0;i<7;i++)
					{
						//draw rectangle for grid if requested
						if(p_ShowGrid)
						{
							g.DrawRectangle(penGrid,rects[i,j]);
						}
						//rects for text
						Rectangle rectTop = new Rectangle(rects[i,j].X,rects[i,j].Y,rects[i,j].Width,(int)(rects[i,j].Height*0.2));
						Rectangle rectTopHalf = new Rectangle(rects[i,j].X,rects[i,j].Y,rects[i,j].Width,(int)(rects[i,j].Height*0.5));

						//check to see if day is in active month
						if(intDateInfo[i,j][0]==pintMonth)
						{
							bActive=true;
							//add month name to box if requested
							if(p_ShowCurrentMonthInDay)
							{
								//str=strAbbrMonths[intMonth-1] + " " + strDates[shopType,j];
								str=strAbbrMonths[intMonth-1] + " " + intDateInfo[i,j][1].ToString();
							}
							else
							{
								//str = strDates[shopType,j];
								str = intDateInfo[i,j][1].ToString();
							}

							DateTime dateTest = new DateTime(intYear,intDateInfo[i,j][0],intDateInfo[i,j][1]);
							//should box be filled as selected date

//							if(dateTest.Date==this.SelectedDate.Date)
//							{
//								bSelected = true;
//							}
//							else
//							{
								bSelected = false;
//							}

							//check to see if date is in BoldedDates
							//first, are there any BoldedDates?
							if(pBoldedDates!=null)//yes, there are BoldedDates
							{
								//do any dates match the date for this rectangle?
								if(Array.IndexOf(pBoldedDates,dateTest)>-1)
								{
									bApptOnDate = true;
								}
								else//no match
								{
									bApptOnDate=false;
								}
							}
							else//no BoldedDates
							{
								bApptOnDate=false;
							}

							if (eventsArray.ContainsKey(dateTest.ToShortDateString()))
								bEvent = true;
							else 
								bEvent = false;

						}
						else//not the active month
						{
							//str = strDates[shopType,j];
							str = intDateInfo[i,j][1].ToString();
							bActive=false;
							bSelected=false;
						}



						/////////////////////////////////////////////////////////////////////////////
						//finally, draw rectangle and text
	                    
						if(bSelected)//selected date
						{
							g.FillRectangle(brushSelectedDay,rects[i,j]);
							if(bApptOnDate)
							{
								g.DrawString( str, p_ApptFont, brushBoldedDateFont, rectTopHalf,sfHeader);

							}
							else
							{
								g.DrawString( str, p_NoApptFont, brushSelectedDayFont, rectTopHalf,sfHeader);

							}
						}
						else if(bActive)//not selected but active
						{
							if (bEvent)
							{
								g.FillRectangle(brushEvent,rects[i,j]);
								if(bApptOnDate)
								{
									g.DrawString( str, p_ApptFont, brushBoldedDateFont, rectTopHalf,sfHeader);

								}
								else
								{
									g.DrawString( str, p_NoApptFont, brushNonselectedDayFont, rectTopHalf,sfHeader);

								}
							}
							else
							{
							
								if(((i == 0) || (i==6)) && p_DisplayWeekendsDarker)//weekend
								{
									g.FillRectangle(brushActiveDarker,rects[i,j]);
								}
								else//weekday
								{
									g.FillRectangle(brushActive,rects[i,j]);
								}
							
								if(bApptOnDate)
								{
									g.DrawString( str, p_ApptFont, brushBoldedDateFont, rectTopHalf,sfHeader);

								}
								else
								{
									g.DrawString( str, p_NoApptFont, brushNonselectedDayFont, rectTopHalf,sfHeader);

								}
							}
						}
						else//not selected or active
						{
							if(((i == 0) || (i==6)) && p_DisplayWeekendsDarker)//weekend
							{
								g.FillRectangle(brushInactiveDarker,rects[i,j]);
							}
							else//weekday
							{
								g.FillRectangle(brushInactive,rects[i,j]);
							}
							
							if(bApptOnDate)
							{
								g.DrawString( str, p_NoApptFont, brushNonselectedDayFont, rectTopHalf,sfHeader);

							}
							else
							{
								g.DrawString( str, p_NoApptFont, brushNonselectedDayFont, rectTopHalf,sfHeader);
							}
						}
					}
				}

			}

		}

		#endregion Control events

		#region Private functions

		private void CreateGraphicObjects()
		{
			//these are the objects that will be used in the paint event
			//pens
			penGrid = new Pen(p_GridColor,3);

			//brushes
			brushHeader = new SolidBrush(p_HeaderColor);

			brushActive = new SolidBrush(p_ActiveMonthColor);
			ActiveDarker = Color.FromArgb((int)(p_ActiveMonthColor.R*0.8),(int)(p_ActiveMonthColor.G*0.8),(int)(p_ActiveMonthColor.B*0.8));
			brushActiveDarker = new SolidBrush(ActiveDarker);

			brushInactive = new SolidBrush(p_InactiveMonthColor);
			InActiveDarker = Color.FromArgb((int)(p_InactiveMonthColor.R*0.8),(int)(p_InactiveMonthColor.G*0.8),(int)(p_InactiveMonthColor.B*0.8));
			brushInactiveDarker = new SolidBrush(InActiveDarker);

			brushSelectedDay = new SolidBrush(p_SelectedDayColor);
			brushBoldedDateFont = new SolidBrush(p_BoldedDateFontColor);
			brushSelectedDayFont = new SolidBrush(p_SelectedDayFontColor);
			brushNonselectedDayFont = new SolidBrush(p_NonselectedDayFontColor);

			//stringformats for displaying text
			//this is used to display appointment text in each day box
			//the text is left aligned and trimmed to fit with ellipsis
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Near;//left
			sf.LineAlignment = StringAlignment.Center;
			sf.Trimming = StringTrimming.EllipsisCharacter;
			//this is used for day header and day numbers
			//the text is centered vertically and horizontally
			sfHeader = new StringFormat();
			sfHeader.Alignment = StringAlignment.Center;
			sfHeader.LineAlignment = StringAlignment.Center;

		}
		private Rectangle[,] CreateGrid(int intW,int intH)
		{
			//Array of rectangles representing the calendar
			Rectangle[,] rectTemp = new Rectangle[7,6];
			
			//header rectangles
			//
			rectDays = new Rectangle[7];
			
			int intXX=0;
			int intXSize = (int)Math.Floor((double)intW/7);
			int intYSize = (int)Math.Floor((double)(intH-20)/6);

			int intYY = 0;
			intXX=0;
			
			for(int i = 0;i<7;i++)
			{
				Rectangle r1 = new Rectangle(intXX,intYY,intXSize,20);
				intXX += intXSize;
				rectDays[i] = r1;

			}
			intYY = 20;
			for(int j = 0;j<6;j++)
			{
				intXX=0;
				for(int i = 0;i<7;i++)
				{
					Rectangle r1 = new Rectangle(intXX,intYY,intXSize,intYSize);
					intXX += intXSize;
					rectTemp[i,j] = r1;
				}
				intYY += intYSize;
			}
			return rectTemp;
		}

		private void FillDates()
		{
			//this is what will be displayed in each day box
			intDateInfo = new int[7,6][];

			//this is where the first day of the month falls in the grid
			int intFirstDay=0;

			//number of days in active month
			int intDays = DateTime.DaysInMonth(intYear,intMonth);

			//total day counter
			int intTotalDays=-1;

			//grid column
			int intDayofWeek=0;
			//grid row
			int intWeek = 0;
			
			//label at top
			this.lblMonth.Text = strMonths[intMonth-1] + ", " + intYear.ToString();

			//array representing the 7 x 6 grid of the calendar
			for(int j = 0;j<6;j++)
			{
				for(int i = 0;i<7;i++)
				{
					intDateInfo[i,j] = new int[2];
					intDateInfo[i,j][0]=intMonth;
					intDateInfo[i,j][1]=0;
				}
			}


			DateTime dateCheck;

			for(int intDay = 0;intDay<intDays;intDay++)
			{
				//populate array of dates for active month, this is used to tell what day of the week each day is

				dateCheck = new DateTime(intYear,intMonth,intDay+1);

				intDayofWeek = Array.IndexOf(strDays,dateCheck.DayOfWeek.ToString());

				//fill the array with the day numbers
				intDateInfo[intDayofWeek,intWeek][1]=intDay+1;
				if(intDayofWeek==6)
				{
					intWeek++;
				}

				//Back fill any days from the previous month
				//this is does here because I needed to know where the first day of the active month fell in the grid
				if(intDay==0)
				{
					intFirstDay=intDayofWeek;
					//Days in previous month
					int intDaysPrev = DateTime.DaysInMonth(intYear,pintMonthPrev);

					//if the first day of the active month is not sunday, count backwards and fill in day number
					if(intDayofWeek>0)
					{
						for(int i = intDayofWeek-1;i>=0;i--)
						{
							intDateInfo[i,0][0]=pintMonthPrev;
							intDateInfo[i,0][1]=intDaysPrev;
							intDaysPrev--;
							intTotalDays++;
						}
					}
				}
				intTotalDays++;
			}//for


			//fill in the remaining days of the grid with the beginning of the next month

			intTotalDays++;
			//what row did we leave off in for active month?
			int intRow = intTotalDays/7;

			int intCol;

			int intDayNext=1;

			for(int i = intRow;i<6;i++)
			{
				intCol = intTotalDays - (intRow*7);
				for(int j = intCol;j<7;j++)
				{
					intDateInfo[j,i][0]=pintMonthNext;
					intDateInfo[j,i][1]=intDayNext;
					intDayNext++;
					intTotalDays++;
				}	
				intRow++;
			}

		}

		#endregion Private functions

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#endregion

		#region Form events

		private void MonthCalendar_Load(object sender, System.EventArgs e)
		{
			pWidth = this.Size.Width;
			pPicWidth=pWidth;
			pHeight = this.Size.Height;
			pPicHeight=pHeight-35;
			picMPK_Cal.Width=pPicWidth;
			picMPK_Cal.Height=pPicHeight;
			rects = CreateGrid(picMPK_Cal.Width,picMPK_Cal.Height);
			//FillDates();
			btnPrev.Visible = p_ShowPrevNextButton;
			btnNext.Visible = p_ShowPrevNextButton;
			CreateGraphicObjects();
			bDesign = false;
		}

		#endregion Form events

		private Hashtable eventsArray = new Hashtable();

		public void CreateEventsArray()
		{
			if (this.MarketingActions != null)
			{
				for (int i = 0; i < MarketingActions.Length; i++)
				{
					EventMarketingAction ema = MarketingActions[i];
					for (DateTime dt = ema.DateBegin.Date; dt <= ema.DateEnd.Date; dt=dt.AddDays(1))
					{
						if (!eventsArray.ContainsKey(dt.ToShortDateString()))
						{
							ArrayList al = new ArrayList();
							al.Add(ema);
							eventsArray.Add(dt.ToShortDateString(),al);
						}
						else
						{
							((ArrayList)eventsArray[dt.ToShortDateString()]).Add(ema);
						}
					}
				}
			}
		}

	}

	public struct EventMarketingAction
	{
		public string Name;
		public string Comment;
		public DateTime DateBegin;
		public DateTime DateEnd;
		public string Type;

		public EventMarketingAction(string name, DateTime dateBegin, DateTime dateEnd, string comment, string type)
		{
			Name = name;
			Comment = comment;
			DateBegin = dateBegin;
			DateEnd = dateEnd;
			Type = type;
		}
	}

	public class MarketingActionEventArgs : EventArgs
	{
		private	EventMarketingAction marketingAction;

		public MarketingActionEventArgs(EventMarketingAction marketingAction)
		{
			this.marketingAction = marketingAction;
		}

		public EventMarketingAction MarketingAction
		{
			get 
			{
				return marketingAction;
			}
		}
	}
}
