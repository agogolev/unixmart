﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using ELBClient.Forms.Dialogs;
using ColumnMenuExtender.UserControls;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Summary description for SelectorTextBox.
	/// </summary>
	public partial class SelectorTextBox : BaseSelectorTextBox
	{
		protected override BaseListDialog GetBaseListDialog(string _className, IList<string> _fieldNames, IList<string> _columnNames, IList<string> _columnHeaderNames,
		    IList<string> _formatRows, OrderInfo _Order, Hashtable _Filters, int[] _widths) {
			return new ListDialog(_className, _fieldNames, _columnNames, _columnHeaderNames, _formatRows, _Order, _Filters, _widths);
		}

		protected override BaseListDialog GetBaseListDialog(string _className, IList<string> _columnNames, IList<string> _columnHeaderNames, IList<string> _formatRows, 
			OrderInfo _Order, Hashtable _Filters, int[] _widths) {
			return new ListDialog(_className, _columnNames, _columnHeaderNames, _formatRows, _Order, _Filters, _widths);
		}
		protected override void btnChoose_Click(object sender, EventArgs e) {
			if(ClassName == "CBinaryData") {
				var fm = new ListImagesDialog();
				if(fm.ShowDialog(this) == DialogResult.OK) {
					BoundProp = new ObjectItem(fm.SelectedOID, fm.SelectedName, "CBinaryData");
					OnChanged(new EventArgs());
				}
			}
			else if (ClassName == "CTheme")
			{
				var fm = new ListThemesDialog();
				if (fm.ShowDialog(this) == DialogResult.OK)
				{
					BoundProp = new ObjectItem(fm.SelectedOID, fm.SelectedName, "CTheme");
					OnChanged(new EventArgs());
				}
			}
			else if (ClassName == "CPeople")
			{
				var fm = new ListPeoplesDialog();
				if (fm.ShowDialog(this) == DialogResult.OK)
				{
					BoundProp = new ObjectItem((Guid)fm.SelectedRow["OID"], string.Format("{0} {1}", fm.SelectedRow["firstName"], fm.SelectedRow["lastName"]), "CPeople");
					OnChanged(new EventArgs());
				}
			}
            else if (ClassName == "CGoogleTaxonomy")
            {
                var fm = new ListGoogleTaxonomiesDialog();
                if (fm.ShowDialog(this) == DialogResult.OK)
                {
                    var selectedRow = fm.SelectedRow;
                    BoundProp = new ObjectItem((Guid)selectedRow["OID"], (string)selectedRow["name"], "CGoogleTaxonomy");
                    OnChanged(new EventArgs());
                }
            }
            else base.btnChoose_Click (sender, e);
		}
	}
}
