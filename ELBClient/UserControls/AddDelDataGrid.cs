﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ELBClient.Forms.Dialogs;
using MetaData;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Summary description for AddDelDataGrid.
	/// </summary>
	public partial class AddDelDataGrid : System.Windows.Forms.UserControl
	{
		public OrderInfo Order = null;
		public Hashtable Filters = new Hashtable();

		public AddDelDataGrid()
		{
			InitializeComponent();
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			string[] columnNames;
			string[] columnHeaderNames;

			if (this.ColumnNames != null)
				columnNames = this.ColumnNames.Split(',');
			else
				columnNames = null;
			if (this.ColumnHeaderNames != null)
				columnHeaderNames = this.ColumnHeaderNames.Split(',');
			else
				columnHeaderNames = null;

			ListDialog fm = new ListDialog(this.ClassName, columnNames, columnHeaderNames, Order, Filters);

			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				Guid o = fm.SelectedOID;
				string s = fm.SelectedNK;

				if (MultiContainer != null)
				{
					try
					{
						MultiContainer.Rows.Add(new object[] { o, s, this.ClassName });
					}
					catch
					{
						MessageBox.Show(this.ParentForm, "Дубли не разрешены!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					DataRow dr = DT.NewRow();
					dr[0] = o; dr[1] = s;
					try
					{
						DT.Rows.Add(dr);
					}
					catch
					{
						MessageBox.Show(this.ParentForm, "Дубли не разрешены!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}


		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			deleteSelectedItem();
		}

		private void deleteSelectedItem()
		{
			int CRI = dataGrid.CurrentRowIndex;
			if (CRI >= 0)
			{
				Guid g = new Guid(dataGrid[CRI, 0].ToString());
				DataRow dr;
				if (MultiContainer != null)
					dr = MultiContainer.Rows.Find(g);
				else dr = DT.Rows.Find(g);

				if (dr != null) dr.Delete();
			}
		}


		private MultiContainer multiContainer;
		private DataTable dt;
		private string className;
		private string columnNames;
		private string columnHeaderNames;
		private string addDelDataGridColumnNames;	// для того чтобы при подсовывании DataTable можно было бы указать
		// назвнаия колонок, а не использовать по умолчанию PropValue и PropValue_NK
		[
		Browsable(false)
		]
		public MultiContainer MultiContainer
		{
			set
			{
				multiContainer = value;
				dataGrid.DataSource = multiContainer;
			}
			get
			{
				return multiContainer;
			}
		}

		[
		Browsable(false)
		]
		public DataTable DT
		{
			set
			{
				dt = value;
				dataGrid.DataSource = dt;
			}
			get
			{
				return dt;
			}
		}

		[
		DefaultValue("")
		]
		public string ClassName
		{
			set
			{
				className = value;
			}
			get
			{
				return className;
			}
		}

		[
		DefaultValue("")
		]
		public string AddDelDataGridColumnNames
		{
			set
			{
				addDelDataGridColumnNames = value;

				string[] arr = null;

				if (addDelDataGridColumnNames != null && addDelDataGridColumnNames != "")
				{
					arr = this.addDelDataGridColumnNames.Split(',');

					if (arr != null && arr.Length > 1)
					{
						dataGridTextBoxColumn1.MappingName = arr[0];
						dataGridTextBoxColumn2.MappingName = arr[1];
					}
				}
			}
			get
			{
				return addDelDataGridColumnNames;
			}
		}

		[
		DefaultValue("")
		]
		public string ColumnNames
		{
			set
			{
				columnNames = value;
			}
			get
			{
				return columnNames;
			}
		}

		[DefaultValue(false), Browsable(true)]
		public bool ColumnHeadersVisible
		{
			set { dataGrid.ColumnHeadersVisible = value; }
		}

		[
		DefaultValue("")
		]
		public string ColumnHeaderNames
		{
			set
			{
				columnHeaderNames = value;
			}
			get
			{
				return columnHeaderNames;
			}
		}


		private void miDel_Click(object sender, System.EventArgs e)
		{
			deleteSelectedItem();
		}

		private void dataGrid_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGrid.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGrid.UnSelect(dataGrid.CurrentRowIndex);
					dataGrid.CurrentRowIndex = rowNum;
					dataGrid.Select(rowNum);
					if (e.Button == MouseButtons.Right)
					{
						contextMenu1.Show(dataGrid, new Point(e.X, e.Y));
					}
				}
			}
		}

		private void dataGrid_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				DataGrid.HitTestInfo hti = dataGrid.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGrid.UnSelect(dataGrid.CurrentRowIndex);
					dataGrid.CurrentRowIndex = rowNum;
					dataGrid.Select(rowNum);
				}
			}
		}

	}
}
