﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.UserControls
{
	/// <summary>
	/// Summary description for AddDelDataGridExt.
	/// </summary>
	public partial class AddDelDataGridExt : System.Windows.Forms.UserControl
	{
		public event EventHandler AddClick;
		public OrderInfo Order = null;
		public Hashtable Filters = new Hashtable();

		public AddDelDataGridExt()
		{
			InitializeComponent();
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			if (AddClick != null) AddClick(sender, e);
		}

		private void btnDel_Click(object sender, System.EventArgs e)
		{
			deleteSelectedItem();
		}

		private void deleteSelectedItem()
		{
			int CRI = dataGrid.CurrentRowIndex;
			if (CRI >= 0)
			{
				Guid g = new Guid(dataGrid[CRI,0].ToString());
				DataRow dr;
				if(MultiContainer != null)
					dr = MultiContainer.Rows.Find(g);
				else dr = DataTable.Rows.Find(g);

				if(dr != null) dr.Delete();
			}
		}


		private MultiContainer multiContainer;
		private DataTable dataTable;
		private string className;
		private string columnNames;
		private string columnHeaderNames;
		private string addDelDataGridColumnNames;	// для того чтобы при подсовывании DataTable можно было бы указать
													// назвнаия колонок, а не использовать по умолчанию PropValue и PropValue_NK
		[
		Browsable(false)
		]
		public MultiContainer MultiContainer
		{
			set 
			{
				multiContainer = value;
				dataGrid.DataSource = multiContainer;
			}
			get
			{
				return multiContainer;
			}
		}

		[
		Browsable(false)
		]
		public DataTable DataTable
		{
			set 
			{
				dataTable = value;
				dataGrid.DataSource = dataTable;
			}
			get
			{
				return dataTable;
			}
		}
		
		[
		DefaultValue("")
		]
		public string ClassName
		{
			set 
			{
				className = value;
			}
			get
			{
				return className;
			}
		}

		[
		DefaultValue("")
		]
		public string AddDelDataGridColumnNames
		{
			set 
			{
				addDelDataGridColumnNames = value;

				string [] arr = null;
				
				if(addDelDataGridColumnNames != null && addDelDataGridColumnNames != "")
				{
					arr = this.addDelDataGridColumnNames.Split(',');

					if(arr != null && arr.Length > 1)
					{
						dataGridTextBoxColumn1.MappingName = arr[0];
						dataGridTextBoxColumn2.MappingName = arr[1];
					}
				}
			}
			get
			{
				return addDelDataGridColumnNames;
			}
		}
		
		[
		DefaultValue("")
		]
		public string ColumnNames
		{
			set 
			{
				columnNames = value;
			}
			get
			{
				return columnNames;
			}
		}

		[DefaultValue(false), Browsable(true)]		
		public bool ColumnHeadersVisible
		{
			set { dataGrid.ColumnHeadersVisible = value; }			
		}

		[
		DefaultValue("")
		]
		public string ColumnHeaderNames
		{
			set 
			{
				columnHeaderNames = value;
			}
			get
			{
				return columnHeaderNames;
			}
		}


		private void miDel_Click(object sender, System.EventArgs e)
		{
			deleteSelectedItem();
		}

		private void dataGrid_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGrid.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGrid.UnSelect(dataGrid.CurrentRowIndex);
					dataGrid.CurrentRowIndex = rowNum;
					dataGrid.Select(rowNum);
					if (e.Button == MouseButtons.Right)
					{
						contextMenu1.Show(dataGrid, new Point(e.X, e.Y));
					}
				}
			}
		}

		private void dataGrid_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				DataGrid.HitTestInfo hti = dataGrid.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dataGrid.UnSelect(dataGrid.CurrentRowIndex);
					dataGrid.CurrentRowIndex = rowNum;
					dataGrid.Select(rowNum);
				}
			}
		}

	}
}
