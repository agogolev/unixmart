﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Invento.SmoothProgressBar
{
	public partial class SmoothProgressBar : System.Windows.Forms.UserControl
	{
		
		int _min = 0;
		int _max = 100;
		int _val = 0;
		Color _barColor = Color.MidnightBlue;

		public SmoothProgressBar()
		{
			InitializeComponent();
		}

		

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		protected override void OnResize(EventArgs e)
		{
			this.Invalidate();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			Graphics g = e.Graphics;
			SolidBrush brush = new SolidBrush(_barColor);
			float percent = (float)(_val - _min) / (float)(_max - _min);
			Rectangle rect = this.ClientRectangle;
			rect.Width = (int)((float)rect.Width * percent);
			g.FillRectangle(brush, rect);
			Draw3DBorder(g);
			brush.Dispose();
			g.Dispose();
		}

		public int Minimum
		{
			get
			{
				return _min;
			}
			set
			{
				if (value < 0)
				{
					_min = 0;
				}
				if (value > _max)
				{
					_min = value;
					_min = value;
				}
				if (_val < _min)
				{
					_val = _min;
				}
				this.Invalidate();
			}
		}

		public int Maximum
		{
			get
			{
				return _max;
			}
			set
			{
				if (value < _min)
				{
					_min = value;
				}
				_max = value;
				if (_val > _max)
				{
					_val = _max;
				}
				this.Invalidate();
			}
		}

		public int Value
		{
			get
			{
				return _val;
			}
			set
			{
				int oldValue = _val;
				if (value < _min)
				{
					_val = _min;
				}
				else if (value > _max)
				{
					_val = _max;
				}
				else
				{
					_val = value;
				}
				float percent;
				Rectangle newValueRect = this.ClientRectangle;
				Rectangle oldValueRect = this.ClientRectangle;
				percent = (float)(_val - _min) / (float)(_max - _min);
				newValueRect.Width = (int)((float)newValueRect.Width * percent);
				percent = (float)(oldValue - _min) / (float)(_max - _min);
				oldValueRect.Width = (int)((float)oldValueRect.Width * percent);
				Rectangle updateRect = new Rectangle();
				if (newValueRect.Width > oldValueRect.Width)
				{
					updateRect.X = oldValueRect.Size.Width;
					updateRect.Width = newValueRect.Width - oldValueRect.Width;
				}
				else
				{
					updateRect.X = newValueRect.Size.Width;
					updateRect.Width = oldValueRect.Width - newValueRect.Width;
				}
				updateRect.Height = this.Height;
				this.Invalidate(updateRect);
			}
		}

		public Color ProgressBarColor
		{
			get
			{
				return _barColor;
			}

			set
			{
				_barColor = value;
				this.Invalidate();
			}
		}

		private void Draw3DBorder(Graphics g)
		{
			int PenWidth = (int)Pens.White.Width;
			g.DrawLine(Pens.DarkGray,
				new Point(this.ClientRectangle.Left, this.ClientRectangle.Top),
				new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Top));
			g.DrawLine(Pens.DarkGray,
				new Point(this.ClientRectangle.Left, this.ClientRectangle.Top),
				new Point(this.ClientRectangle.Left, this.ClientRectangle.Height - PenWidth));
			g.DrawLine(Pens.White,
				new Point(this.ClientRectangle.Left, this.ClientRectangle.Height - PenWidth),
				new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Height - PenWidth));
			g.DrawLine(Pens.White,
				new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Top),
				new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Height - PenWidth));
		}
	}
}
