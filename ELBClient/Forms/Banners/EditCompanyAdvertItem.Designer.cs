﻿namespace ELBClient.Forms.Banners
{
	partial class EditCompanyAdvertItem
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.AdvertItemSelect = new ELBClient.UserControls.SelectorTextBox();
			this.AdvertPlaceSelect = new ELBClient.UserControls.SelectorTextBox();
			this.DateBeginPicker = new ELBClient.UserControls.ExtDateTimePicker();
			this.DateEndPicker = new ELBClient.UserControls.ExtDateTimePicker();
			this.MaxShowsText = new ColumnMenuExtender.DataBoundTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.MaxClicksText = new ColumnMenuExtender.DataBoundTextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.WeightText = new ColumnMenuExtender.DataBoundTextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// AdvertItemSelect
			// 
			this.AdvertItemSelect.BoundProp = null;
			this.AdvertItemSelect.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.AdvertItemSelect.ClassName = "CAdvertItem";
			this.AdvertItemSelect.FieldNames = "name,width,height";
			this.AdvertItemSelect.FormatRows = null;
			this.AdvertItemSelect.HeaderNames = "Название,Ширина,Высота";
			this.AdvertItemSelect.Location = new System.Drawing.Point(90, 13);
			this.AdvertItemSelect.Name = "AdvertItemSelect";
			this.AdvertItemSelect.RowNames = null;
			this.AdvertItemSelect.Size = new System.Drawing.Size(256, 24);
			this.AdvertItemSelect.TabIndex = 0;
			// 
			// AdvertPlaceSelect
			// 
			this.AdvertPlaceSelect.BoundProp = null;
			this.AdvertPlaceSelect.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.AdvertPlaceSelect.ClassName = "CAdvertPlace";
			this.AdvertPlaceSelect.FieldNames = "name";
			this.AdvertPlaceSelect.FormatRows = null;
			this.AdvertPlaceSelect.HeaderNames = "Название";
			this.AdvertPlaceSelect.Location = new System.Drawing.Point(90, 43);
			this.AdvertPlaceSelect.Name = "AdvertPlaceSelect";
			this.AdvertPlaceSelect.RowNames = null;
			this.AdvertPlaceSelect.Size = new System.Drawing.Size(256, 24);
			this.AdvertPlaceSelect.TabIndex = 1;
			// 
			// DateBeginPicker
			// 
			this.DateBeginPicker.BoundValue = new System.DateTime(((long)(0)));
			this.DateBeginPicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.DateBeginPicker.LinkedTo = null;
			this.DateBeginPicker.Location = new System.Drawing.Point(90, 73);
			this.DateBeginPicker.Name = "DateBeginPicker";
			this.DateBeginPicker.ReadOnly = false;
			this.DateBeginPicker.ShowButtons = true;
			this.DateBeginPicker.Size = new System.Drawing.Size(256, 20);
			this.DateBeginPicker.TabIndex = 2;
			this.DateBeginPicker.Value = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
			// 
			// DateEndPicker
			// 
			this.DateEndPicker.BoundValue = new System.DateTime(((long)(0)));
			this.DateEndPicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.DateEndPicker.LinkedTo = null;
			this.DateEndPicker.Location = new System.Drawing.Point(90, 99);
			this.DateEndPicker.Name = "DateEndPicker";
			this.DateEndPicker.ReadOnly = false;
			this.DateEndPicker.ShowButtons = true;
			this.DateEndPicker.Size = new System.Drawing.Size(256, 20);
			this.DateEndPicker.TabIndex = 3;
			this.DateEndPicker.Value = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
			// 
			// MaxShowsText
			// 
			this.MaxShowsText.BoundProp = null;
			this.MaxShowsText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.MaxShowsText.IsCurrency = true;
			this.MaxShowsText.Location = new System.Drawing.Point(90, 125);
			this.MaxShowsText.Name = "MaxShowsText";
			this.MaxShowsText.Size = new System.Drawing.Size(100, 20);
			this.MaxShowsText.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(2, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Банер:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(2, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(42, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Место:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(2, 77);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(47, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Начало:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(2, 103);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(41, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "Конец:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 129);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(81, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Макс. показы:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(3, 158);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(73, 13);
			this.label6.TabIndex = 10;
			this.label6.Text = "Макс. клики:";
			// 
			// MaxClicksText
			// 
			this.MaxClicksText.BoundProp = null;
			this.MaxClicksText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.MaxClicksText.IsCurrency = true;
			this.MaxClicksText.Location = new System.Drawing.Point(90, 154);
			this.MaxClicksText.Name = "MaxClicksText";
			this.MaxClicksText.Size = new System.Drawing.Size(100, 20);
			this.MaxClicksText.TabIndex = 11;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(5, 227);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 12;
			this.button1.Text = "Сохранить";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(271, 227);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 13;
			this.button2.Text = "Отказ";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// WeightText
			// 
			this.WeightText.BoundProp = null;
			this.WeightText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WeightText.IsCurrency = true;
			this.WeightText.Location = new System.Drawing.Point(90, 180);
			this.WeightText.Name = "WeightText";
			this.WeightText.Size = new System.Drawing.Size(100, 20);
			this.WeightText.TabIndex = 15;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(3, 184);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(77, 13);
			this.label7.TabIndex = 14;
			this.label7.Text = "Вес в списке:";
			// 
			// EditCompanyAdvertItem
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(358, 270);
			this.Controls.Add(this.WeightText);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.MaxClicksText);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.MaxShowsText);
			this.Controls.Add(this.DateEndPicker);
			this.Controls.Add(this.DateBeginPicker);
			this.Controls.Add(this.AdvertPlaceSelect);
			this.Controls.Add(this.AdvertItemSelect);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "EditCompanyAdvertItem";
			this.Text = "Редактирование элемента";
			this.Load += new System.EventHandler(this.fmEditCompanyAdvertItem_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private UserControls.SelectorTextBox AdvertItemSelect;
		private UserControls.SelectorTextBox AdvertPlaceSelect;
		private UserControls.ExtDateTimePicker DateBeginPicker;
		private UserControls.ExtDateTimePicker DateEndPicker;
		private ColumnMenuExtender.DataBoundTextBox MaxShowsText;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private ColumnMenuExtender.DataBoundTextBox MaxClicksText;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private ColumnMenuExtender.DataBoundTextBox WeightText;
		private System.Windows.Forms.Label label7;
	}
}