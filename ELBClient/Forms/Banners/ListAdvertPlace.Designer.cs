using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Banners
{
	public partial class ListAdvertPlace
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;

		private ColumnMenuExtender.ExtendedDataGridTableStyle dataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn dataGridTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.dataGridTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
			this.dataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridTextBoxColumn1
			// 
			this.dataGridTextBoxColumn1.FieldName = null;
			this.dataGridTextBoxColumn1.FilterFieldName = null;
			this.dataGridTextBoxColumn1.Format = "";
			this.dataGridTextBoxColumn1.FormatInfo = null;
			this.dataGridTextBoxColumn1.HeaderText = "Наименование";
			this.dataGridTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.dataGridTextBoxColumn1, this.menuFilterSort1);
			this.dataGridTextBoxColumn1.NullText = "";
			this.dataGridTextBoxColumn1.Width = 150;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Ширина";
			this.formattableTextBoxColumn1.MappingName = "width";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.NullText = "";
			this.formattableTextBoxColumn1.Width = 150;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Высота";
			this.formattableTextBoxColumn2.MappingName = "height";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// mainDataGrid1
			// 
			this.mainDataGrid1.AdditionalInfo = "";
			this.mainDataGrid1.AllowNew = false;
			this.mainDataGrid1.ClassName = "CAdvertPlace";
			this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
			this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.mainDataGrid1.Name = "mainDataGrid1";
			this.mainDataGrid1.Size = new System.Drawing.Size(542, 370);
			this.mainDataGrid1.TabIndex = 0;
			this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.dataGridTableStyle1});
			// 
			// dataGridTableStyle1
			// 
			this.dataGridTableStyle1.AllowSorting = false;
			this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.dataGridTextBoxColumn1,
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.formattableBooleanColumn1,
            this.formattableTextBoxColumn4});
			this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridTableStyle1.MappingName = "table";
			this.dataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Кол-во банеров";
			this.formattableTextBoxColumn3.MappingName = "ItemsCount";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableBooleanColumn1
			// 
			this.formattableBooleanColumn1.FieldName = null;
			this.formattableBooleanColumn1.FilterFieldName = null;
			this.formattableBooleanColumn1.HeaderText = "Вертикальный";
			this.formattableBooleanColumn1.MappingName = "isVertical";
			this.formattableBooleanColumn1.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = null;
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "ID";
			this.formattableTextBoxColumn4.MappingName = "objectID";
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// ListAdvertPlace
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 370);
			this.Controls.Add(this.mainDataGrid1);
			this.MinimumSize = new System.Drawing.Size(550, 404);
			this.Name = "ListAdvertPlace";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Список рекламны мест";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
	}
}
