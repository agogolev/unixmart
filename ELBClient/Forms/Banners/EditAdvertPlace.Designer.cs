﻿namespace ELBClient.Forms.Banners
{
	partial class EditAdvertPlace
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.NameText = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.WidthText = new ColumnMenuExtender.DataBoundTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.HeightText = new ColumnMenuExtender.DataBoundTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.ItemsCountText = new ColumnMenuExtender.DataBoundTextBox();
			this.IsVerticalCheck = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// NameText
			// 
			this.NameText.Location = new System.Drawing.Point(153, 20);
			this.NameText.Name = "NameText";
			this.NameText.Size = new System.Drawing.Size(360, 20);
			this.NameText.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Название:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 60);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(55, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Ширина:";
			// 
			// WidthText
			// 
			this.WidthText.BoundProp = null;
			this.WidthText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WidthText.IsCurrency = false;
			this.WidthText.Location = new System.Drawing.Point(153, 57);
			this.WidthText.Name = "WidthText";
			this.WidthText.Size = new System.Drawing.Size(126, 20);
			this.WidthText.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(13, 97);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(48, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Высота:";
			// 
			// HeightText
			// 
			this.HeightText.BoundProp = null;
			this.HeightText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.HeightText.IsCurrency = false;
			this.HeightText.Location = new System.Drawing.Point(153, 94);
			this.HeightText.Name = "HeightText";
			this.HeightText.Size = new System.Drawing.Size(126, 20);
			this.HeightText.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(13, 136);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(136, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Кол-во одновременных:";
			// 
			// ItemsCountText
			// 
			this.ItemsCountText.BoundProp = null;
			this.ItemsCountText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.ItemsCountText.IsCurrency = false;
			this.ItemsCountText.Location = new System.Drawing.Point(153, 133);
			this.ItemsCountText.Name = "ItemsCountText";
			this.ItemsCountText.Size = new System.Drawing.Size(126, 20);
			this.ItemsCountText.TabIndex = 6;
			// 
			// IsVerticalCheck
			// 
			this.IsVerticalCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.IsVerticalCheck.Location = new System.Drawing.Point(12, 172);
			this.IsVerticalCheck.Name = "IsVerticalCheck";
			this.IsVerticalCheck.Size = new System.Drawing.Size(158, 24);
			this.IsVerticalCheck.TabIndex = 9;
			this.IsVerticalCheck.Text = "Вертикальный:";
			this.IsVerticalCheck.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 216);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(619, 46);
			this.panel1.TabIndex = 10;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(537, 9);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 2;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// EditAdvertPlace
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(619, 262);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.IsVerticalCheck);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.ItemsCountText);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.HeightText);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.WidthText);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.NameText);
			this.Name = "EditAdvertPlace";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование рекламного места";
			this.Load += new System.EventHandler(this.EditAdvertPlace_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox NameText;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private ColumnMenuExtender.DataBoundTextBox WidthText;
		private System.Windows.Forms.Label label3;
		private ColumnMenuExtender.DataBoundTextBox HeightText;
		private System.Windows.Forms.Label label4;
		private ColumnMenuExtender.DataBoundTextBox ItemsCountText;
		private System.Windows.Forms.CheckBox IsVerticalCheck;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
	}
}