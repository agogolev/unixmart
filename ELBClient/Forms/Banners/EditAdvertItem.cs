﻿using System;
using System.Linq;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using System.Xml.Linq;
using ELBClient.Classes;
using System.Collections.Generic;
using ELBClient.Forms.Dialogs;

namespace ELBClient.Forms.Banners
{
	public partial class EditAdvertItem : EditForm
	{
		private readonly TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
		private readonly Tree regionsTree = new Tree(0, int.MaxValue);
		private readonly Tree catalogTree = new Tree(0, int.MaxValue);
		private DataSet regionsDataSet;
		private DataSet catalogDataSet;

		public EditAdvertItem()
		{
			InitializeComponent();
			Object = new CAdvertItem();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			SaveObject();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void BindFields()
		{
			tbName.DataBindings.Add("Text", Object, "Name");
			tbUrl.DataBindings.Add("Text", Object, "Url");
			WidthText.DataBindings.Add("BoundProp", Object, "Width");
			HeightText.DataBindings.Add("BoundProp", Object, "Height");
			stbImage.DataBindings.Add("BoundProp", Object, "BinData");
			HtmlText.DataBindings.Add("Text", Object, "Html");
			AdvertTypeLookup.DataBindings.Add("SelectedValue", Object, "advertType");
			ParentTypeLookup.DataBindings.Add("SelectedValue", Object, "parentType");
			if ((Object as CAdvertItem).GeoInclusive) GeoInclusive.Checked = true;
			else GeoExclusive.Checked = true;
			GeoInclusive.CheckedChanged += GeoInclusive_CheckedChanged;
		}

		private void EditAdvertItem_Load(object sender, EventArgs e)
		{
			LoadLookup();
			LoadRegions();
			LoadCatalog();
			BuildTrees();
			LoadObject("name,width,height,binData,html,advertType,url,parentType,geoInclusive", "locations,objects");
			BindFields();
			LoadTrees();
			LoadGoods();
			if (((CAdvertItem)Object).BinData != null && ((CAdvertItem)Object).BinData.OID != Guid.Empty)
			{
				stbImage_Changed(sender, e);
			}
		}

		private void LoadGoods()
		{
			var item = Object as CAdvertItem;
			item.Objects.AsEnumerable()
				.Where(row => string.Compare(row.Field<string>("propValue_className"), "CGoods", true) == 0)
				.ToList()
				.ForEach(row => GoodsDataSet.Table.Rows.Add(new object[]
				                                            	{
				                                            		row.Field<Guid>("propValue"), 
				                                            		row.Field<string>("propValue_NK"),
				                                            		row.Field<string>("propValue_className")
				                                            	}));
			GoodsDataSet.AcceptChanges();
			GoodsAddDelDataGrid.DataTable = GoodsDataSet.Table;
		}

		private void LoadRegions()
		{
			//Filling OID
			const string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree OID = \"3094c215-57e7-4623-b4b9-6c93914e8c88\" />";

			regionsDataSet = treeProvider.GetTreeContent(xml);
		}

		private void LoadCatalog()
		{
			var element = XElement.Parse("<tree />");
			var parentNode = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(1);
			element.Add(new XAttribute("nodeID", parentNode),
				new XAttribute("withParent", "true"));
			catalogDataSet = treeProvider.GetTreeContent(element.ToString());
		}

		private void BuildTrees()
		{
			DoBuildTree(regionsDataSet, regionsTree);
			DoBuildTree(catalogDataSet, catalogTree);
		}

		private void DoBuildTree(DataSet dataSet, Tree tree)
		{
			var nodes = new Stack();
			nodes.Push(tree);

			foreach (DataRow dr in dataSet.Tables["table"].Rows)
			{
				var nodeID = (int)dr["nodeID"];
				var node = new Node((int)dr["lft"], (int)dr["rgt"])
								{
									NodeId = nodeID,
									Name = dr["nodeName"].ToString(),
									objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"]
								};
				while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
				((NodeContainer)nodes.Peek()).Nodes.Add(node);
				if ((node.Left + 1) != node.Right)
				{
					nodes.Push(node);
				}
			}
		}

		private void LoadTrees()
		{
			DoLoadTree(GeoTargetingTreeView, regionsTree, (Object as CAdvertItem).Locations.AsEnumerable().ToList());
			DoLoadTree(CatalogTreeView, catalogTree, 
				(Object as CAdvertItem).Objects
				.AsEnumerable()
				.Where(row => string.Compare(row.Field<string>("propValue_className"), "CTheme", true) == 0)
				.ToList());
		}

		private void DoLoadTree(TreeView treeView, Tree tree, IList<DataRow> multiContainer)
		{
			treeView.Nodes.Clear();
			foreach (Node n in tree.Nodes)
			{
				var tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : "<Unknown>");
				var OID = n.objectOID;
				var nodeChecked = false;
				if (OID != Guid.Empty && multiContainer.Count(row => row.Field<Guid>("propValue") == OID) != 0) nodeChecked = true;
				else if (OID == Guid.Empty && !multiContainer.Any()) nodeChecked = true;
				tn.Checked = nodeChecked;
				tn.Tag = n;//n.objectOID;
				treeView.Nodes.Add(tn);
				LoadNode(tn, n, multiContainer);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node, IList<DataRow> locations)
		{
			foreach (Node n in node.Nodes)
			{
				var tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : "<Unknown>");
				var OID = n.objectOID;
				var nodeChecked = false;
				if (locations != null)
				{
					var count = locations.Count(row => row.Field<Guid>("propValue") == OID);
					if ((OID != Guid.Empty && count != 0)) nodeChecked = true;
					else if ((OID == Guid.Empty && !locations.Any())) nodeChecked = true;
				}
				tn.Checked = nodeChecked;
				tn.Tag = n;//n.objectOID;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n, locations);
			}
		}

		private void LoadLookup()
		{
			string xml = lp.GetLookupValues("CAdvertItem");
			AdvertTypeLookup.LoadXml(xml, "advertTypeN");
			ParentTypeLookup.LoadXml(xml, "parentTypeN");
		}

		private void stbImage_Changed(object sender, EventArgs e)
		{
			string adr = ServiceUtility.GetBrowseDir() + "BrowseImage.aspx?OID=" + stbImage.BoundProp.OID;
			webBrowser1.Navigate(adr);
		}

		protected override string AllowToSave()
		{
			var advertItem = (Object as CAdvertItem);
			if (advertItem.AdvertType == 3 && (advertItem.Width == int.MinValue || advertItem.Width == 0 || advertItem.Height == int.MinValue || advertItem.Height == 0)) return "для flash банера указание ширины и высоты обязательно";
			return base.AllowToSave();
		}
		protected override void SaveObject()
		{
			try
			{
				var item = Object as CAdvertItem;
				if (LocationIsModified())
				{
					for (var i = item.Locations.Rows.Count - 1; i >= 0; i--) item.Locations.Rows[i].Delete();
					var items = GetNewLocation();
					if (items.Count == 0 || (items.Count == 1 && items[0].objectOID == Guid.Empty)) { }
					else
					{
						items
							.ToList()
							.ForEach(n => item.Locations.Rows.Add(new object[] { n.objectOID }));
					}
				}
				if (CategoriesIsModified() || GoodsDataSet.IsModified)
				{
					for (var i = item.Objects.Rows.Count - 1; i >= 0; i--) item.Objects.Rows[i].Delete();
					var items = GetNewCategories();
					if (items.Count == 0 || (items.Count == 1 && items[0].objectOID == Guid.Empty)) { }
					else
					{
						items
							.ToList()
							.ForEach(n => item.Objects.Rows.Add(new object[] { n.objectOID, "", "CTheme" }));
					}
					GoodsDataSet.Table.AsEnumerable().Where(row => row.RowState != DataRowState.Deleted).Select(row => row.Field<Guid>("propValue"))
						.ToList()
						.ForEach(g => item.Objects.Rows.Add(new object[] { g, "", "CGoods" }));
				}
				base.SaveObject();
				GoodsDataSet.AcceptChanges();
			}
			catch (Exception e)
			{
				ShowError(e.Message);
			}
		}

		private void GeoInclusive_CheckedChanged(object sender, EventArgs e)
		{
			var item = Object as CAdvertItem;
			item.GeoInclusive = GeoInclusive.Checked;
		}

		private void GeoTargetingTree_AfterCheck(object sender, TreeViewEventArgs e)
		{
			var item = e.Node;
			if (item == null) return;
			if (item.Checked)
			{
				var parentNode = item.Parent;
				while (parentNode != null)
				{
					parentNode.Checked = false;
					parentNode = parentNode.Parent;
				}
				RemoveCheckedStateChild(item);
			}
		}
		private void RemoveCheckedStateChild(TreeNode item)
		{
			foreach (TreeNode tn in item.Nodes)
			{
				tn.Checked = false;
				RemoveCheckedStateChild(tn);
			}
		}
		protected override bool IsModified
		{
			get
			{
				return LocationIsModified() || CategoriesIsModified() || GoodsDataSet.IsModified || base.IsModified;
			}
		}
		private bool LocationIsModified()
		{
			var selectedItems = GetNewLocation();
			var obj = Object as CAdvertItem;

			if (selectedItems.Count(n => n.objectOID != Guid.Empty) != obj.Locations.Rows.Count) return true;
			var isModified = false;
			selectedItems.ToList().ForEach(n =>
				{
					if (obj.Locations.AsEnumerable().Count(row => row.Field<Guid>("propValue") == n.objectOID) == 0) isModified = true;
				});
			return isModified;
		}

		private bool CategoriesIsModified()
		{
			var selectedItems = GetNewCategories();
			var obj = Object as CAdvertItem;

			if (selectedItems.Count(n => n.objectOID != Guid.Empty) != obj.Objects.AsEnumerable().Where(row => string.Compare(row.Field<string>("propValue_className"), "CTheme", true) == 0).Count()) return true;
			var isModified = false;
			selectedItems.ToList().ForEach(n =>
			{
				if (obj.Objects.AsEnumerable().Count(row => row.Field<Guid>("propValue") == n.objectOID) == 0) isModified = true;
			});
			return isModified;
		}

		private IList<Node> GetNewLocation()
		{
			var selectedItems = new List<Node>();
			foreach (TreeNode n1 in GeoTargetingTreeView.Nodes)
			{
				if (n1.Checked && ((Node)n1.Tag).objectOID != Guid.Empty) selectedItems.Add((Node)n1.Tag);
				ProcessChild(selectedItems, n1);
			}
			return selectedItems;
		}

		private IList<Node> GetNewCategories()
		{
			var selectedItems = new List<Node>();
			foreach (TreeNode n1 in CatalogTreeView.Nodes)
			{
				if (n1.Checked && ((Node)n1.Tag).objectOID != Guid.Empty) selectedItems.Add((Node)n1.Tag);
				ProcessChild(selectedItems, n1);
			}
			return selectedItems;
		}

		private void ProcessChild(List<Node> selectedItems, TreeNode n)
		{
			foreach (TreeNode n1 in n.Nodes)
			{
				if (n1.Checked) selectedItems.Add((Node)n1.Tag);
				ProcessChild(selectedItems, n1);
			}
		}

		private void CatalogTreeView_AfterCheck(object sender, TreeViewEventArgs e)
		{
			var item = e.Node;
			if (item == null) return;
			if (item.Checked)
			{
				var parentNode = item.Parent;
				while (parentNode != null)
				{
					parentNode.Checked = false;
					parentNode = parentNode.Parent;
				}
				RemoveCheckedStateChild(item);
			}
		}

		private void addDelDataGridExt1_AddClick(object sender, EventArgs e)
		{
			var fm = new ListGoodsDialog { ShopType = 0 };
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			foreach (var dr in fm.SelectedRows)
			{
				try
				{
					if (dr == null) continue;
					var o = (Guid)dr["OID"];
					var s = dr["NK"].ToString();
					GoodsDataSet.Table.Rows.Add(new object[] { o, s, "CGoods" });
				}
				catch (ConstraintException)
				{
					//ShowError("Товар уже добавлен");
				}
			}
		}
	}
}
