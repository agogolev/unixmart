﻿using System;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.Forms.Banners
{
	public partial class EditCompanyAdvertItem : Form
	{
		//public bool PreserveKey { get; set; }
		public CompanyAdvertItem Item { get; set; }
		public EditCompanyAdvertItem()
		{
			InitializeComponent();
		}

		private void fmEditCompanyAdvertItem_Load(object sender, EventArgs e)
		{
			BindFields();
			//CheckKey();
		}

		//private void CheckKey()
		//{
		//    AdvertItemSelect.Enabled = !PreserveKey;
		//    AdvertPlaceSelect.Enabled = !PreserveKey;
		//    DateBeginPicker.Enabled = !PreserveKey;
		//}

		private void BindFields()
		{
			AdvertItemSelect.DataBindings.Add("BoundProp", Item, "AdvertItem");
			AdvertPlaceSelect.DataBindings.Add("BoundProp", Item, "AdvertPlace");
			DateBeginPicker.DataBindings.Add("BoundValue", Item, "DateBegin");
			DateEndPicker.DataBindings.Add("BoundValue", Item, "DateEnd");
			MaxShowsText.DataBindings.Add("BoundProp", Item, "MaxShows");
			MaxClicksText.DataBindings.Add("BoundProp", Item, "MaxClicks");
			WeightText.DataBindings.Add("BoundProp", Item, "Weight");
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (Item.AdvertItem == ObjectItem.Empty)
			{
				MessageBox.Show("Укажите банер");
				return;
			}
			if (Item.AdvertPlace == ObjectItem.Empty)
			{
				MessageBox.Show("Укажите место");
				return;
			}
			if (Item.DateBegin == DateTime.MinValue)
			{
				MessageBox.Show("Укажите начало публикации");
				return;
			}
			DialogResult = DialogResult.OK;

		}
	}
}
