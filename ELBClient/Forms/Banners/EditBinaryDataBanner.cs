﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using System.Data;
using ColumnMenuExtender;
using MetaData;

namespace ELBClient.Forms.Banners
{
	/// <summary>
	/// Summary description for fmEditBinaryData.
	/// </summary>
	public partial class EditBinaryDataBanner : EditForm
	{
		public int ExactWidth = -1, ExactHeight = -1;

		
		public EditBinaryDataBanner()
		{
			InitializeComponent();
			extendedDataGridSelectorColumn1.ButtonClick += new CellEventHandler(extendedDataGridSelectorColumn1_ButtonClick);
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveObject();
			//			this.Close();
		}
		protected override void SaveObject() 
		{
			if(ExactWidth != -1 || ExactHeight != -1) {
				if((Object as CBinaryData).Width != ExactWidth || (Object as CBinaryData).Height != ExactHeight) {
					MessageBox.Show(this, "Картинка не соответсвует размеру (ширина: "+ExactWidth+", высота: "+ExactHeight+") для выбранного типа картинки", "Внимание!");
					return;
				}
			}
			string fileName = tbFile.Text;
			if(fileName.IndexOf(@"\") != -1 || fileName.IndexOf("/") != -1) 
			{
				tbFile.Text = Path.GetFileName(fileName);
			}
			try
			{
				base.SaveObject ();
			}
			catch{}
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void fmEditBinaryData_Load(object sender, System.EventArgs e)
		{
			Object = new CBinaryData();
			LoadObject("name,mimeType,description,fileName,width,height", null);
			ListProvider.ListProvider provider = Classes.ServiceUtility.ListProvider;
			DataSetISM ds = new DataSetISM(provider.GetDataSetSql("upImageLinks '"+Object.OID+"'"));
			ds.Table.Columns.Add("button");
			dgObjects.DataSource = ds;
			dgObjects.DataMember = "table";
			BindFields();
		}
	
		private void BindFields()
		{
			tbName.DataBindings.Add("Text", Object, "Name");
			tbMimeType.DataBindings.Add("Text", Object, "MimeType");
			tbFile.DataBindings.Add("Text", Object, "FileName");
			tbWidth.DataBindings.Add("BoundProp", Object, "Width");
			tbHeight.DataBindings.Add("BoundProp", Object, "Height");
		}

		private void button1_Click(object sender, System.EventArgs e) 
		{
			if(ofdFile.ShowDialog(this) == DialogResult.OK) 
			{
				tbFile.Text = ofdFile.FileName;
				FileStream fs = new FileStream(tbFile.Text, FileMode.Open, FileAccess.Read, FileShare.Read);
				byte[] content = new byte[fs.Length];
				fs.Read(content, 0, (int)fs.Length);
				fs.Close();
				CBinaryData img = (CBinaryData)Object;
				img.Data = content;
				tbName.Text = Path.GetFileName(tbFile.Text);
				tbMimeType.Text = MetaData.MimeTypeUtil.CheckType(tbFile.Text);
				FillSize(tbFile.Text, tbMimeType.Text);
				BindingContext[Object].EndCurrentEdit();
			}
		}

		private void FillSize(string fileName, string mimeType) 
		{
			Size size = MetaData.Sizer.GetSize(fileName, mimeType);
			if(size.Width != -1) tbWidth.Text = size.Width.ToString();
			if(size.Height != -1) tbHeight.Text = size.Height.ToString();
		}
		
		private void btnView_Click(object sender, System.EventArgs e)
		{
			if(Object.OID != Guid.Empty)
			{
				Browse fm = new Browse(Object.OID) { BrowseBanners = true };
				fm.MdiParent = this.MdiParent;
				fm.Show();
			}
			else MessageBox.Show(this, "Сначала сохраните объект в базе!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}
		public override string ObjectNK
		{
			get
			{
				return (Object as CBinaryData).Name;
			}
		}

		private void extendedDataGridSelectorColumn1_ButtonClick(object sender, DataGridCellEventArgs e) {
			DataRow dr = dgObjects.GetSelectedRow();
			if(dr != null) {
				Guid OID = (Guid) dr["OID"];
				string className = (string) dr["className"];
				EditForm frm = FormSelection.GetEditForm(className);
				if(frm != null) {
					frm.ObjectOID = OID;
					if(!this.Modal) {
						frm.MdiParent = this.MdiParent;
						frm.Show();
					}
					else frm.ShowDialog(this);
				}
			}
		}
	}
}
