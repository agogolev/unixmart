﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using ColumnMenuExtender;
using ELBClient.Classes;
using MetaData;

namespace ELBClient.Forms.Banners
{
	public partial class EditAdvertCompany : EditForm
	{
		public EditAdvertCompany()
		{
			InitializeComponent();
			Object = new CAdvertCompany();
		}

		private void fmEditAdvertCompany_Load(object sender, EventArgs e)
		{
			LoadData();
			BindFields();
		}

		private void BindFields()
		{
			NameText.DataBindings.Add("Text", Object, "Name");
			BeginDate.DataBindings.Add("BoundValue", Object, "DateBegin");
			EndDate.DataBindings.Add("BoundValue", Object, "DateEnd");
		}

		private void LoadData()
		{
			string xml = GetResourceString("fmEditAdvertCompany.xml");
			Object.LoadMultiPropFlag = false;
			xml = LoadObject(xml, CAdvertCompany.ClassCID);
			LoadGrid(xml);
		}

		private void LoadGrid(string xml)
		{

			try
			{
				BannersData.LoadRowsFromXML("Table", xml, "banners");
				RefreshPlaceFilter();
			}
			catch (Exception e)
			{
				ShowError(e.Message);
			}

		}

		protected override void SaveObject()
		{
			string message = AllowToSave();
			if (message != "")
			{
				ShowError(message);
				return;
			}
			if ((Object as CAdvertCompany).Site.OID == Guid.Empty)
			{
				(Object as CAdvertCompany).Site = new ObjectItem(Constants.SiteOID, "electroburg.ru", "CSite");
			}
			Object.ExtMultiProp = BannersData.GetChangesXml("banners",
				new[] { "itemOID", "placeOID", "dateBegin", "dateEnd", "maxShows", "maxClicks", "weight" },
				new[] { "itemOID", "advertPlace", "dateBegin", "dateEnd", "maxShows", "maxClicks", "weight" });
			base.SaveObject();
			BannersData.AcceptChanges();
		}

		protected override string AllowToSave()
		{
			if ((Object as CAdvertCompany).DateBegin == DateTime.MinValue) return "Не задана дата начала публикации";
			return base.AllowToSave();
		}
		private void CloseButton_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			SaveObject();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			var fm = new EditCompanyAdvertItem
			{
				Item = new CompanyAdvertItem
				{
					AdvertItem = ObjectItem.Empty,
					AdvertPlace = ObjectItem.Empty,
					DateBegin = DateTime.MinValue,
					DateEnd = DateTime.MinValue,
					MaxShows = int.MinValue,
					MaxClicks = int.MinValue,
					Weight = 0
				}
			};
			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				var item = fm.Item;
				try
				{
					BannersData.Table.Rows.Add(new[] {
						item.AdvertItem.OID, 
						item.AdvertItem.NK, 
						item.AdvertPlace.OID,
						item.AdvertPlace.NK, 
						item.DateBegin,
						fm.Item.DateEnd != DateTime.MinValue ? (object)fm.Item.DateEnd : DBNull.Value,
						fm.Item.MaxShows != int.MinValue ? (object)fm.Item.MaxShows : DBNull.Value,
						fm.Item.MaxClicks != int.MinValue ? (object)fm.Item.MaxClicks : DBNull.Value,
						item.Weight
					});
					RefreshPlaceFilter();
				}
				catch
				{
					ShowError("Такая запись уже существует");
				}
			}
		}

		private void BannersGrid_DoubleClick(object sender, EventArgs e)
		{
			var dr = BannersGrid.GetSelectedRow();
			if (dr != null && (BannersGrid.Hit.Type == DataGrid.HitTestType.Cell || BannersGrid.Hit.Type == DataGrid.HitTestType.RowHeader))
			{
				var fm = new EditCompanyAdvertItem
				{
					Item = new CompanyAdvertItem
					{
						AdvertItem = dr["itemOID"] != DBNull.Value ? new ObjectItem((Guid)dr["itemOID"], (string)dr["itemName"], "CAdvertItem") : ObjectItem.Empty,
						AdvertPlace = dr["placeOID"] != DBNull.Value ? new ObjectItem((Guid)dr["placeOID"], (string)dr["placeName"], "CAdvertPlace") : ObjectItem.Empty,
						DateBegin = dr["dateBegin"] != DBNull.Value ? (DateTime)dr["dateBegin"] : DateTime.MinValue,
						DateEnd = dr["dateEnd"] != DBNull.Value ? (DateTime)dr["dateEnd"] : DateTime.MinValue,
						MaxShows = dr["maxShows"] != DBNull.Value ? (int)dr["maxShows"] : int.MinValue,
						MaxClicks = dr["maxClicks"] != DBNull.Value ? (int)dr["maxClicks"] : int.MinValue,
						Weight = (int)dr["weight"]
					}/*,
					PreserveKey = true*/
				};
				if (fm.ShowDialog(this) == DialogResult.OK)
				{
					dr["itemOID"] = fm.Item.AdvertItem.OID;
					dr["itemName"] = fm.Item.AdvertItem.NK;
					dr["placeOID"] = fm.Item.AdvertPlace.OID;
					dr["placeName"] = fm.Item.AdvertPlace.NK;
					dr["dateBegin"] = fm.Item.DateBegin != DateTime.MinValue ? (object)fm.Item.DateBegin : DBNull.Value;
					dr["dateEnd"] = fm.Item.DateEnd != DateTime.MinValue ? (object)fm.Item.DateEnd : DBNull.Value;
					dr["maxShows"] = fm.Item.MaxShows != int.MinValue ? (object)fm.Item.MaxShows : DBNull.Value;
					dr["maxClicks"] = fm.Item.MaxClicks != int.MinValue ? (object)fm.Item.MaxClicks : DBNull.Value;
					dr["weight"] = fm.Item.Weight;
					RefreshPlaceFilter();
				}
			}
		}

		private void btnDel_Click(object sender, EventArgs e)
		{
			DataRow dr = BannersGrid.GetSelectedRow();
			if (dr == null) return;
			dr.Delete();
			RefreshPlaceFilter();
		}
		protected override bool IsModified
		{
			get
			{
				return BannersData.IsModified || base.IsModified;
			}
		}

		private void RefreshPlaceFilter()
		{

			var selectedItem = AdvertPlaceSelect.SelectedIndex;
			AdvertPlaceSelect.SelectedIndexChanged -= AdvertPlaceSelect_SelectedIndexChanged;
			try
			{
				AdvertPlaceSelect.Items.Clear();
				var i = 1;
				AdvertPlaceSelect.AddItems
				(
					BannersData.Table.AsEnumerable()
						.Where(row => row.RowState != DataRowState.Deleted)
						.Select(row => new ListItem(0, row.Field<string>("placeName"))
						{
						    Tag = row.Field<Guid>("placeOID")
						})
                        .Distinct(new LambdaComparer<ListItem>((a, b) => (Guid)a.Tag == (Guid)b.Tag))
						.OrderBy(item => item.Text)
						.Select(item => new ListItem(i++, item.Text)
						{
						    Tag = item.Tag
						})
						.Union(new[] { new ListItem(0, "Все") { Tag = Guid.Empty } })
						.OrderBy(item => item.Value)
						.ToList()
				);
			}
			finally
			{
				AdvertPlaceSelect.SelectedIndexChanged += AdvertPlaceSelect_SelectedIndexChanged;
				AdvertPlaceSelect.SelectedIndex = selectedItem != -1 ? selectedItem : 0;
			}
		}

		private void AdvertPlaceSelect_SelectedIndexChanged(object sender, EventArgs e)
		{
			var placeOID = (Guid)(AdvertPlaceSelect.SelectedItem).Tag;
			if (placeOID == Guid.Empty)
			{
				var view = new DataView(BannersData.Table, null, "placeName, weight", DataViewRowState.CurrentRows);
				BannersGrid.DataSource = view;
				BannersGrid.DisableNewDel();
			}
			else
			{
				var view = new DataView(BannersData.Table, string.Format("placeOID = '{0}'", placeOID), "placeName, weight", DataViewRowState.CurrentRows);
				BannersGrid.DataSource = view;
				BannersGrid.DisableNewDel();
			}
		}
	}
	public class CompanyAdvertItem
	{
		public ObjectItem AdvertItem { get; set; }
		public ObjectItem AdvertPlace { get; set; }
		public DateTime DateBegin { get; set; }
		public DateTime DateEnd { get; set; }
		public int MaxShows { get; set; }
		public int MaxClicks { get; set; }
		public int Weight { get; set; }
	}
}
