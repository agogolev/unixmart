﻿using System;
using ELBClient.Classes;

namespace ELBClient.Forms.Banners
{
	public partial class EditAdvertPlace : EditForm
	{
		public EditAdvertPlace()
		{
			InitializeComponent();
			Object = new CAdvertPlace();
		}

		private void EditAdvertPlace_Load(object sender, EventArgs e)
		{
			LoadData();
			BindFields();
		}

		private void BindFields()
		{
			NameText.DataBindings.Add("Text", Object, "Name");
			WidthText.DataBindings.Add("BoundProp", Object, "Width");
			HeightText.DataBindings.Add("BoundProp", Object, "Height");
			ItemsCountText.DataBindings.Add("BoundProp", Object, "ItemsCount");
			IsVerticalCheck.DataBindings.Add("Checked", Object, "IsVertical");
		}

		private void LoadData()
		{
			LoadObject("name,width,height,isVertical,itemsCount", "");
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			SaveObject();
		}

	}
}
