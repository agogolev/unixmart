﻿using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Banners
{
	partial class EditAdvertCompany
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.NameText = new System.Windows.Forms.TextBox();
			this.BeginDate = new ELBClient.UserControls.ExtDateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.EndDate = new ELBClient.UserControls.ExtDateTimePicker();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.SaveButton = new System.Windows.Forms.Button();
			this.CloseButton = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.BannersGrid = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.AdvertPlaceSelect = new ColumnMenuExtender.DataBoundComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.BannersData = new MetaData.DataSetISM();
			this.dataTable1 = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn9 = new System.Data.DataColumn();
			this.panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BannersGrid)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BannersData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// NameText
			// 
			this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.NameText.Location = new System.Drawing.Point(169, 6);
			this.NameText.Name = "NameText";
			this.NameText.Size = new System.Drawing.Size(466, 20);
			this.NameText.TabIndex = 30;
			// 
			// BeginDate
			// 
			this.BeginDate.BoundValue = new System.DateTime(2012, 8, 28, 9, 18, 28, 466);
			this.BeginDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.BeginDate.LinkedTo = null;
			this.BeginDate.Location = new System.Drawing.Point(169, 32);
			this.BeginDate.Name = "BeginDate";
			this.BeginDate.ReadOnly = false;
			this.BeginDate.ShowButtons = true;
			this.BeginDate.Size = new System.Drawing.Size(208, 20);
			this.BeginDate.TabIndex = 31;
			this.BeginDate.Value = new System.DateTime(2012, 8, 28, 9, 18, 28, 466);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(139, 18);
			this.label1.TabIndex = 32;
			this.label1.Text = "Дата начала публикации:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 16);
			this.label2.TabIndex = 33;
			this.label2.Text = "Заголовок:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// EndDate
			// 
			this.EndDate.BoundValue = new System.DateTime(2012, 8, 28, 9, 18, 28, 462);
			this.EndDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.EndDate.LinkedTo = null;
			this.EndDate.Location = new System.Drawing.Point(169, 58);
			this.EndDate.Name = "EndDate";
			this.EndDate.ReadOnly = false;
			this.EndDate.ShowButtons = true;
			this.EndDate.Size = new System.Drawing.Size(208, 20);
			this.EndDate.TabIndex = 34;
			this.EndDate.Value = new System.DateTime(2012, 8, 28, 9, 18, 28, 462);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 58);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(155, 18);
			this.label3.TabIndex = 35;
			this.label3.Text = "Дата окончания публикации:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.SaveButton);
			this.panel1.Controls.Add(this.CloseButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 333);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(717, 34);
			this.panel1.TabIndex = 36;
			// 
			// SaveButton
			// 
			this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.SaveButton.Location = new System.Drawing.Point(611, 7);
			this.SaveButton.Name = "SaveButton";
			this.SaveButton.Size = new System.Drawing.Size(100, 23);
			this.SaveButton.TabIndex = 1;
			this.SaveButton.Text = "Сохранить";
			this.SaveButton.UseVisualStyleBackColor = true;
			this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
			// 
			// CloseButton
			// 
			this.CloseButton.Location = new System.Drawing.Point(4, 7);
			this.CloseButton.Name = "CloseButton";
			this.CloseButton.Size = new System.Drawing.Size(100, 23);
			this.CloseButton.TabIndex = 0;
			this.CloseButton.Text = "Закрыть";
			this.CloseButton.UseVisualStyleBackColor = true;
			this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(717, 333);
			this.tabControl1.TabIndex = 37;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.NameText);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.EndDate);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.BeginDate);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(709, 307);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Общие";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.BannersGrid);
			this.tabPage2.Controls.Add(this.panel2);
			this.tabPage2.Controls.Add(this.panel3);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(709, 307);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Рекламные баннеры";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// BannersGrid
			// 
			this.BannersGrid.BackgroundColor = System.Drawing.Color.White;
			this.BannersGrid.DataMember = "";
			this.BannersGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BannersGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.BannersGrid.Location = new System.Drawing.Point(3, 38);
			this.BannersGrid.Name = "BannersGrid";
			this.BannersGrid.Order = null;
			this.BannersGrid.Size = new System.Drawing.Size(667, 266);
			this.BannersGrid.TabIndex = 0;
			this.BannersGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.BannersGrid.DoubleClick += new System.EventHandler(this.BannersGrid_DoubleClick);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.BannersGrid;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Банер";
			this.formattableTextBoxColumn1.MappingName = "itemName";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Место";
			this.formattableTextBoxColumn2.MappingName = "placeName";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "dd.MM.yyyy";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Начало";
			this.formattableTextBoxColumn3.MappingName = "dateBegin";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = null;
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "dd.MM.yyyy";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "Конец";
			this.formattableTextBoxColumn4.MappingName = "dateEnd";
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// formattableTextBoxColumn5
			// 
			this.formattableTextBoxColumn5.FieldName = null;
			this.formattableTextBoxColumn5.FilterFieldName = null;
			this.formattableTextBoxColumn5.Format = "";
			this.formattableTextBoxColumn5.FormatInfo = null;
			this.formattableTextBoxColumn5.HeaderText = "Макс. кликов";
			this.formattableTextBoxColumn5.MappingName = "MaxClicks";
			this.formattableTextBoxColumn5.Width = 75;
			// 
			// formattableTextBoxColumn6
			// 
			this.formattableTextBoxColumn6.FieldName = null;
			this.formattableTextBoxColumn6.FilterFieldName = null;
			this.formattableTextBoxColumn6.Format = "";
			this.formattableTextBoxColumn6.FormatInfo = null;
			this.formattableTextBoxColumn6.HeaderText = "Макс. показов";
			this.formattableTextBoxColumn6.MappingName = "MaxShows";
			this.formattableTextBoxColumn6.Width = 75;
			// 
			// formattableTextBoxColumn7
			// 
			this.formattableTextBoxColumn7.FieldName = null;
			this.formattableTextBoxColumn7.FilterFieldName = null;
			this.formattableTextBoxColumn7.Format = "";
			this.formattableTextBoxColumn7.FormatInfo = null;
			this.formattableTextBoxColumn7.HeaderText = "Вес";
			this.formattableTextBoxColumn7.MappingName = "weight";
			this.formattableTextBoxColumn7.Width = 75;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnDel);
			this.panel2.Controls.Add(this.btnAdd);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(670, 38);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(36, 266);
			this.panel2.TabIndex = 1;
			// 
			// btnDel
			// 
			this.btnDel.Location = new System.Drawing.Point(6, 35);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(24, 23);
			this.btnDel.TabIndex = 4;
			this.btnDel.Text = "-";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(6, 3);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(24, 23);
			this.btnAdd.TabIndex = 3;
			this.btnAdd.Text = "+";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.AdvertPlaceSelect);
			this.panel3.Controls.Add(this.label4);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(3, 3);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(703, 35);
			this.panel3.TabIndex = 2;
			// 
			// AdvertPlaceSelect
			// 
			this.AdvertPlaceSelect.Location = new System.Drawing.Point(128, 8);
			this.AdvertPlaceSelect.Name = "AdvertPlaceSelect";
			this.AdvertPlaceSelect.SelectedValue = -1;
			this.AdvertPlaceSelect.Size = new System.Drawing.Size(208, 21);
			this.AdvertPlaceSelect.TabIndex = 29;
            this.AdvertPlaceSelect.SelectedIndexChanged += new PositionChangedEventHandler(this.AdvertPlaceSelect_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(114, 18);
			this.label4.TabIndex = 30;
			this.label4.Text = "Рекламное место:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BannersData
			// 
			this.BannersData.DataSetName = "NewDataSet";
			this.BannersData.OnlyRowsWithoutErrors = false;
			this.BannersData.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
			// 
			// dataTable1
			// 
			this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
			this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "dateBegin",
                        "itemOID",
                        "placeOID"}, true)});
			this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn5,
        this.dataColumn1,
        this.dataColumn3};
			this.dataTable1.TableName = "Table";
			// 
			// dataColumn1
			// 
			this.dataColumn1.AllowDBNull = false;
			this.dataColumn1.ColumnName = "itemOID";
			this.dataColumn1.DataType = typeof(System.Guid);
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "itemName";
			// 
			// dataColumn3
			// 
			this.dataColumn3.AllowDBNull = false;
			this.dataColumn3.ColumnName = "placeOID";
			this.dataColumn3.DataType = typeof(System.Guid);
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "placeName";
			// 
			// dataColumn5
			// 
			this.dataColumn5.AllowDBNull = false;
			this.dataColumn5.ColumnName = "dateBegin";
			this.dataColumn5.DataType = typeof(System.DateTime);
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "dateEnd";
			this.dataColumn6.DataType = typeof(System.DateTime);
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "maxShows";
			this.dataColumn7.DataType = typeof(int);
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "maxClicks";
			this.dataColumn8.DataType = typeof(int);
			// 
			// dataColumn9
			// 
			this.dataColumn9.Caption = "weight";
			this.dataColumn9.ColumnName = "weight";
			this.dataColumn9.DataType = typeof(int);
			// 
			// EditAdvertCompany
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(717, 367);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.panel1);
			this.Name = "EditAdvertCompany";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Рекламная компания";
			this.Load += new System.EventHandler(this.fmEditAdvertCompany_Load);
			this.panel1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BannersGrid)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BannersData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox NameText;
		private UserControls.ExtDateTimePicker BeginDate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private UserControls.ExtDateTimePicker EndDate;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button SaveButton;
		private System.Windows.Forms.Button CloseButton;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private ColumnMenuExtender.DataGridISM BannersGrid;
		private System.Windows.Forms.Panel panel2;
		private MetaData.DataSetISM BannersData;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataColumn dataColumn5;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;
		private System.Data.DataColumn dataColumn8;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.Button btnAdd;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private System.Windows.Forms.Panel panel3;
		private ColumnMenuExtender.DataBoundComboBox AdvertPlaceSelect;
		private System.Windows.Forms.Label label4;
		private System.Data.DataColumn dataColumn9;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
	}
}