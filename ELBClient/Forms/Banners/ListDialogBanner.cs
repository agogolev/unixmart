﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;
using ELBClient.Classes;

namespace ELBClient.Forms.Banners {
	/// <summary>
	/// Summary description for fmListDialog.
	/// </summary>
	public partial class ListDialogBanner : BaseListDialog {
		
		public ListDialogBanner(string className, string[] columnNames, string[] columnHeaderNames)
			: base (className, columnNames, columnHeaderNames) {
		 }

		public ListDialogBanner(string className, string[] columnNames, string[] columnHeaderNames, OrderInfo Order, 
			Hashtable Filters) 
			: base (className, columnNames, columnHeaderNames, Order, Filters) {
		 }
		
		public ListDialogBanner(string className, string[] columnNames, string[] columnHeaderNames, OrderInfo Order, 
			Hashtable Filters, int[] widths) 
			: base (className, columnNames, columnHeaderNames, Order, Filters, widths) {
		 }

		public ListDialogBanner(string className, string[] columnNames, string[] columnHeaderNames, string[] formatRows, 
			OrderInfo Order, Hashtable Filters, int[] widths) 
			: base (className, columnNames, columnHeaderNames, formatRows, Order, Filters, widths) {
		 }

		public ListDialogBanner(string className, ArrayList fieldNames, string[] columnNames, string[] columnHeaderNames, 
			string[] formatRows, OrderInfo Order, Hashtable Filters, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, formatRows, Order, Filters, widths) {
		 }

		public ListDialogBanner(string className, ArrayList fieldNames, string[] columnNames, string[] columnHeaderNames, 
			OrderInfo Order, Hashtable Filters, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, Order, Filters, widths) {
		 }

		public ListDialogBanner(string className, string[] fieldNames, string[] columnNames, string[] columnHeaderNames, 
			Hashtable DataGridColumnStyleArray, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, DataGridColumnStyleArray, widths) {
		 }

		public ListDialogBanner(string className, string[] fieldNames, string[] columnNames, string[] columnHeaderNames,  
			string[] formatRows, Hashtable DataGridColumnStyleArray, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, formatRows, DataGridColumnStyleArray, widths) {
		 }

		protected override DataSet GetDataSet() {
			return  ServiceUtility.ListProvider.GetList(dataGridISM1.GetDataXml().OuterXml);
		}

		

		//protected override string GetFormName(string ClassName) {
		//    return (string)GenericForm.ClassRUName[ClassName];
		//}

		protected override string FileName {
			get {
				return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
					@"\Invento\ELBClient\forms.xml";
			}
		}

		protected override string HintTextBoxFileName {	
			get {	
				return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + 
					@"\Invento\ELBClient\HintTextBox.xml";
			}
		}
	}
}
