using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Banners
{
	public partial class ReportBanners
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnShow;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.DataGridISM BannersGrid;
		private System.Windows.Forms.SaveFileDialog sfdExportFile;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnShow = new System.Windows.Forms.Button();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.BannersGrid = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.sfdExportFile = new System.Windows.Forms.SaveFileDialog();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BannersGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnShow);
			this.panel1.Controls.Add(this.dtpDateEnd);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.dtpDateBegin);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(776, 40);
			this.panel1.TabIndex = 0;
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(696, 7);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 5;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnShow
			// 
			this.btnShow.Location = new System.Drawing.Point(536, 7);
			this.btnShow.Name = "btnShow";
			this.btnShow.Size = new System.Drawing.Size(75, 23);
			this.btnShow.TabIndex = 4;
			this.btnShow.Text = "Показать";
			this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Location = new System.Drawing.Point(328, 8);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.Size = new System.Drawing.Size(200, 20);
			this.dtpDateEnd.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(272, 7);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(48, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "Конец:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Location = new System.Drawing.Point(72, 8);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(200, 20);
			this.dtpDateBegin.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Начало:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BannersGrid
			// 
			this.BannersGrid.AllowSorting = false;
			this.BannersGrid.BackgroundColor = System.Drawing.Color.White;
			this.BannersGrid.ColumnDragEnabled = true;
			this.BannersGrid.DataMember = "";
			this.BannersGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BannersGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.BannersGrid.Location = new System.Drawing.Point(0, 40);
			this.BannersGrid.Name = "BannersGrid";
			this.BannersGrid.Order = null;
			this.BannersGrid.Size = new System.Drawing.Size(776, 397);
			this.BannersGrid.TabIndex = 1;
			this.BannersGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
			this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.BannersGrid, false);
			this.BannersGrid.Reload += new System.EventHandler(this.dgOrders_Reload);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.BannersGrid;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
			this.formattableTextBoxColumn1,
			this.formattableTextBoxColumn2,
			this.formattableTextBoxColumn3,
			this.formattableTextBoxColumn4});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Показы";
			this.formattableTextBoxColumn2.MappingName = "showCount";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Клики";
			this.formattableTextBoxColumn3.MappingName = "ClickCount";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = null;
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "P";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "CTR";
			this.formattableTextBoxColumn4.MappingName = "CTR";
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// sfdExportFile
			// 
			this.sfdExportFile.DefaultExt = "xml";
			this.sfdExportFile.Filter = "XML файлы|*.xml|Все файлы|*.*";
			// 
			// fmReportBanners
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(776, 437);
			this.Controls.Add(this.BannersGrid);
			this.Controls.Add(this.panel1);
			this.Name = "ReportBanners";
			this.Text = "Отчёт по показам";
			this.Load += new System.EventHandler(this.fmExportOrders_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BannersGrid)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
	}
}
