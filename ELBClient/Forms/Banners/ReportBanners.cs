﻿using System;
using MetaData;

namespace ELBClient.Forms.Banners
{
	/// <summary>
	/// Summary description for fmReportBanners.
	/// </summary>
	public partial class ReportBanners : ListForm
	{
		public ReportBanners()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void fmExportOrders_Load(object sender, System.EventArgs e) {
			dtpDateBegin.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day) ;
			dtpDateEnd.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day).AddMonths(1);
		}

		public override void LoadData() {
			string sql = @"
SELECT DISTINCT
	ai.OID,
	ai.name,
	showCount = (SELECT Count(*) FROM t_AdvertShows WHERE OID = ai.OID),
	clickCount = (SELECT Count(*) FROM t_AdvertClicks WHERE OID = ai.OID)
FROM
	t_AdvertItem ai
WHERE
	EXISTS (SELECT * FROM t_AdvertShows WHERE dateShow BETWEEN {0} And {1} and OID = ai.OID)
";
			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(dtpDateBegin.Value), ToSqlString(dtpDateEnd.Value))));
			ds.Table.Columns.Add("CTR", typeof(decimal), "clickCount / showCount");
			BannersGrid.SetDataBinding(ds, "Table");
			BannersGrid.DisableNewDel();
		}

		private void btnShow_Click(object sender, System.EventArgs e) {
			LoadData();
		}

		private void dgOrders_Reload(object sender, System.EventArgs e) {
			LoadData();
		}
	}
}
