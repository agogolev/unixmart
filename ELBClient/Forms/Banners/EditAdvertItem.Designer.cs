using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Banners
{
	public partial class EditAdvertItem
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Panel pnlMain;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private ELBClient.UserControls.SelectorEdtTextBox stbImage;
		private System.Windows.Forms.TextBox tbUrl;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Label label4;
		private WebBrowser webBrowser1;
		private System.Windows.Forms.Label label1;

		private void InitializeComponent()
		{
			this.pnlMain = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.GeoExclusive = new System.Windows.Forms.RadioButton();
			this.GeoInclusive = new System.Windows.Forms.RadioButton();
			this.label8 = new System.Windows.Forms.Label();
			this.HeightText = new ColumnMenuExtender.DataBoundTextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.WidthText = new ColumnMenuExtender.DataBoundTextBox();
			this.ParentTypeLookup = new ColumnMenuExtender.DataBoundComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.AdvertTypeLookup = new ColumnMenuExtender.DataBoundComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tbUrl = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.stbImage = new ELBClient.UserControls.SelectorEdtTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.webBrowser1 = new System.Windows.Forms.WebBrowser();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.HtmlText = new System.Windows.Forms.TextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.GeoTargetingTreeView = new System.Windows.Forms.TreeView();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.CatalogTreeView = new System.Windows.Forms.TreeView();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.GoodsAddDelDataGrid = new ELBClient.UserControls.AddDelDataGridExt();
			this.GoodsDataSet = new MetaData.DataSetISM();
			this.dataTable1 = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.pnlMain.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.tabPage5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GoodsDataSet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlMain
			// 
			this.pnlMain.Controls.Add(this.groupBox1);
			this.pnlMain.Controls.Add(this.label8);
			this.pnlMain.Controls.Add(this.HeightText);
			this.pnlMain.Controls.Add(this.label7);
			this.pnlMain.Controls.Add(this.WidthText);
			this.pnlMain.Controls.Add(this.ParentTypeLookup);
			this.pnlMain.Controls.Add(this.label6);
			this.pnlMain.Controls.Add(this.AdvertTypeLookup);
			this.pnlMain.Controls.Add(this.label5);
			this.pnlMain.Controls.Add(this.label2);
			this.pnlMain.Controls.Add(this.tbUrl);
			this.pnlMain.Controls.Add(this.label1);
			this.pnlMain.Controls.Add(this.stbImage);
			this.pnlMain.Controls.Add(this.label3);
			this.pnlMain.Controls.Add(this.tbName);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlMain.Location = new System.Drawing.Point(0, 0);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(650, 275);
			this.pnlMain.TabIndex = 3;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.GeoExclusive);
			this.groupBox1.Controls.Add(this.GeoInclusive);
			this.groupBox1.Location = new System.Drawing.Point(189, 175);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(333, 85);
			this.groupBox1.TabIndex = 37;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Опции геотаргетинга";
			// 
			// GeoExclusive
			// 
			this.GeoExclusive.AutoSize = true;
			this.GeoExclusive.Location = new System.Drawing.Point(6, 58);
			this.GeoExclusive.Name = "GeoExclusive";
			this.GeoExclusive.Size = new System.Drawing.Size(188, 17);
			this.GeoExclusive.TabIndex = 1;
			this.GeoExclusive.TabStop = true;
			this.GeoExclusive.Text = "Все регионы кроме указанных";
			this.GeoExclusive.UseVisualStyleBackColor = true;
			// 
			// GeoInclusive
			// 
			this.GeoInclusive.AutoSize = true;
			this.GeoInclusive.Location = new System.Drawing.Point(6, 30);
			this.GeoInclusive.Name = "GeoInclusive";
			this.GeoInclusive.Size = new System.Drawing.Size(227, 17);
			this.GeoInclusive.TabIndex = 0;
			this.GeoInclusive.TabStop = true;
			this.GeoInclusive.Text = "Включить только указанные регионы";
			this.GeoInclusive.UseVisualStyleBackColor = true;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 205);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(72, 19);
			this.label8.TabIndex = 36;
			this.label8.Text = "Высота:";
			// 
			// HeightText
			// 
			this.HeightText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.HeightText.BoundProp = null;
			this.HeightText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.HeightText.IsCurrency = false;
			this.HeightText.Location = new System.Drawing.Point(88, 203);
			this.HeightText.Name = "HeightText";
			this.HeightText.Size = new System.Drawing.Size(95, 20);
			this.HeightText.TabIndex = 35;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 175);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(72, 19);
			this.label7.TabIndex = 34;
			this.label7.Text = "Ширина:";
			// 
			// WidthText
			// 
			this.WidthText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.WidthText.BoundProp = null;
			this.WidthText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.WidthText.IsCurrency = false;
			this.WidthText.Location = new System.Drawing.Point(88, 173);
			this.WidthText.Name = "WidthText";
			this.WidthText.Size = new System.Drawing.Size(95, 20);
			this.WidthText.TabIndex = 33;
			// 
			// ParentTypeLookup
			// 
			this.ParentTypeLookup.Location = new System.Drawing.Point(88, 142);
			this.ParentTypeLookup.Name = "ParentTypeLookup";
			this.ParentTypeLookup.SelectedValue = -1;
			this.ParentTypeLookup.Size = new System.Drawing.Size(208, 21);
			this.ParentTypeLookup.TabIndex = 31;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(5, 143);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(80, 19);
			this.label6.TabIndex = 32;
			this.label6.Text = "Показ:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// AdvertTypeLookup
			// 
			this.AdvertTypeLookup.Location = new System.Drawing.Point(88, 78);
			this.AdvertTypeLookup.Name = "AdvertTypeLookup";
			this.AdvertTypeLookup.SelectedValue = -1;
			this.AdvertTypeLookup.Size = new System.Drawing.Size(208, 21);
			this.AdvertTypeLookup.TabIndex = 29;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(5, 80);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 18);
			this.label5.TabIndex = 30;
			this.label5.Text = "Тип:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 18);
			this.label2.TabIndex = 18;
			this.label2.Text = "URL:";
			// 
			// tbUrl
			// 
			this.tbUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbUrl.Location = new System.Drawing.Point(88, 48);
			this.tbUrl.Name = "tbUrl";
			this.tbUrl.Size = new System.Drawing.Size(554, 20);
			this.tbUrl.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 112);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 18);
			this.label1.TabIndex = 16;
			this.label1.Text = "Картинка:";
			// 
			// stbImage
			// 
			this.stbImage.BoundProp = null;
			this.stbImage.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.stbImage.ClassName = "CBinaryData";
			this.stbImage.FieldNames = null;
			this.stbImage.FormatRows = null;
			this.stbImage.HeaderNames = "Название";
			this.stbImage.Location = new System.Drawing.Point(88, 107);
			this.stbImage.Name = "stbImage";
			this.stbImage.RowNames = "Name";
			this.stbImage.Size = new System.Drawing.Size(320, 28);
			this.stbImage.TabIndex = 4;
			this.stbImage.Changed += new System.EventHandler(this.stbImage_Changed);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 21);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 18);
			this.label3.TabIndex = 14;
			this.label3.Text = "Название:";
			// 
			// tbName
			// 
			this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(88, 18);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(554, 20);
			this.tbName.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(568, 9);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 2;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 562);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(650, 46);
			this.panel1.TabIndex = 4;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tabControl1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 275);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(650, 287);
			this.panel2.TabIndex = 5;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(650, 287);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.webBrowser1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(642, 261);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Картинка";
			// 
			// webBrowser1
			// 
			this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.webBrowser1.Location = new System.Drawing.Point(0, 0);
			this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new System.Drawing.Size(642, 261);
			this.webBrowser1.TabIndex = 0;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.HtmlText);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(642, 261);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "HTML";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// HtmlText
			// 
			this.HtmlText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.HtmlText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HtmlText.Location = new System.Drawing.Point(0, 0);
			this.HtmlText.Multiline = true;
			this.HtmlText.Name = "HtmlText";
			this.HtmlText.Size = new System.Drawing.Size(642, 261);
			this.HtmlText.TabIndex = 3;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.GeoTargetingTreeView);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(642, 261);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Геотаргетинг";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// GeoTargetingTreeView
			// 
			this.GeoTargetingTreeView.CheckBoxes = true;
			this.GeoTargetingTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GeoTargetingTreeView.Location = new System.Drawing.Point(0, 0);
			this.GeoTargetingTreeView.Name = "GeoTargetingTreeView";
			this.GeoTargetingTreeView.Size = new System.Drawing.Size(642, 261);
			this.GeoTargetingTreeView.TabIndex = 0;
			this.GeoTargetingTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.GeoTargetingTree_AfterCheck);
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.CatalogTreeView);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(642, 261);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Категории сайта";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// CatalogTreeView
			// 
			this.CatalogTreeView.CheckBoxes = true;
			this.CatalogTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CatalogTreeView.Location = new System.Drawing.Point(0, 0);
			this.CatalogTreeView.Name = "CatalogTreeView";
			this.CatalogTreeView.Size = new System.Drawing.Size(642, 261);
			this.CatalogTreeView.TabIndex = 1;
			this.CatalogTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.CatalogTreeView_AfterCheck);
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.GoodsAddDelDataGrid);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Size = new System.Drawing.Size(642, 261);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "Товары";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// GoodsAddDelDataGrid
			// 
			this.GoodsAddDelDataGrid.AddDelDataGridColumnNames = null;
			this.GoodsAddDelDataGrid.ClassName = null;
			this.GoodsAddDelDataGrid.ColumnHeaderNames = "Название";
			this.GoodsAddDelDataGrid.ColumnNames = "propValue_NK";
			this.GoodsAddDelDataGrid.DataTable = null;
			this.GoodsAddDelDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GoodsAddDelDataGrid.Location = new System.Drawing.Point(0, 0);
			this.GoodsAddDelDataGrid.MultiContainer = null;
			this.GoodsAddDelDataGrid.Name = "GoodsAddDelDataGrid";
			this.GoodsAddDelDataGrid.Size = new System.Drawing.Size(642, 261);
			this.GoodsAddDelDataGrid.TabIndex = 0;
			this.GoodsAddDelDataGrid.AddClick += new System.EventHandler(this.addDelDataGridExt1_AddClick);
			// 
			// GoodsDataSet
			// 
			this.GoodsDataSet.DataSetName = "NewDataSet";
			this.GoodsDataSet.OnlyRowsWithoutErrors = false;
			this.GoodsDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
			// 
			// dataTable1
			// 
			this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3});
			this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "propValue"}, true)});
			this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
			this.dataTable1.TableName = "Table";
			// 
			// dataColumn1
			// 
			this.dataColumn1.AllowDBNull = false;
			this.dataColumn1.ColumnName = "propValue";
			this.dataColumn1.DataType = typeof(System.Guid);
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "propValue_NK";
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "propValue_className";
			// 
			// EditAdvertItem
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(650, 608);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pnlMain);
			this.Name = "EditAdvertItem";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование промо-блока";
			this.Load += new System.EventHandler(this.EditAdvertItem_Load);
			this.pnlMain.ResumeLayout(false);
			this.pnlMain.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GoodsDataSet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.DataBoundComboBox ParentTypeLookup;
		private Label label6;
		private ColumnMenuExtender.DataBoundComboBox AdvertTypeLookup;
		private Label label5;
		private Label label8;
		private ColumnMenuExtender.DataBoundTextBox HeightText;
		private Label label7;
		private ColumnMenuExtender.DataBoundTextBox WidthText;
		private TabPage tabPage2;
		private TextBox HtmlText;
		private GroupBox groupBox1;
		private RadioButton GeoExclusive;
		private RadioButton GeoInclusive;
		private TabPage tabPage3;
		private TreeView GeoTargetingTreeView;
		private TabPage tabPage4;
		private TabPage tabPage5;
		private TreeView CatalogTreeView;
		private UserControls.AddDelDataGridExt GoodsAddDelDataGrid;
		private MetaData.DataSetISM GoodsDataSet;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn3;
	}
}
