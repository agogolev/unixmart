﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListPeoplesDialog.
	/// </summary>
	public partial class ListPeoplesDialog : EditForm
	{
		public DataRow SelectedRow {
			get {
				return dgPeoples.GetSelectedRow();
			}
		}
		
		public ListPeoplesDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			Object = new Classes.CPeople();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnClose_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
		}

		private void btnSearch_Click(object sender, EventArgs e) {
			if (string.IsNullOrWhiteSpace(EMailSearch.Text) && string.IsNullOrWhiteSpace(NameSearch.Text) && string.IsNullOrWhiteSpace(LastNameSearch.Text)) return;
			dgPeoples.Filters.Clear();
			if (!string.IsNullOrWhiteSpace(EMailSearch.Text)) dgPeoples.CreateAuxFilter("eMail", "eMailFilter", MetaData.FilterVerb.Like, false, new object[] { "*" + EMailSearch.Text.Trim() + "*" });
			if (!string.IsNullOrWhiteSpace(NameSearch.Text)) dgPeoples.CreateAuxFilter("firstName", "firstNameFilter", MetaData.FilterVerb.Like, false, new object[] { "*" + NameSearch.Text.Trim() + "*" });
			if (!string.IsNullOrWhiteSpace(LastNameSearch.Text)) dgPeoples.CreateAuxFilter("lastName", "nameFilter", MetaData.FilterVerb.Like, false, new object[] { "*" + LastNameSearch.Text.Trim() + "*" });
			var ds = lp.GetList(dgPeoples.GetDataXml().InnerXml);
			dgPeoples.SetDataBinding(ds, "table");
			dgPeoples.DisableNewDel();
		}

		private void dgGoods_DoubleClick(object sender, EventArgs e) {
			DialogResult = DialogResult.OK;
		}

		private void btnNew_Click(object sender, EventArgs e) {
			if(NameSearch.Text.Trim() == "") return;
			var p = (Object as Classes.CPeople);
			p.LastName = NameSearch.Text.Trim();
			SaveObject();
			var ds = new DataSet();
			var dt = new DataTable("table");
			dt.Columns.Add("OID", typeof(Guid));
			dt.Columns.Add("lastName", typeof(string));
			ds.Tables.Add(dt);
			dgPeoples.SetDataBinding(ds, "table");
			dt.Rows.Add(new object[]{p.OID, p.LastName});
			DialogResult = DialogResult.OK;
		}
	}
}
