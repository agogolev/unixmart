﻿using System;
using System.Windows.Forms;
using System.Data;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListImagesDialog.
	/// </summary>
	public partial class ListImagesDialog  : ListForm
	{
		private Guid _selectedOID;
		private int _selectedWidth, _selectedHeight;
		private string _selectedName;

		public Guid SelectedOID {
			get {
				return _selectedOID;
			}
			set {
				_selectedOID = value;
			}
		}
		public string SelectedName {
			get {
				return _selectedName;
			}
			set {
				_selectedName = value;
			}
		}
		public int SelectedWidth{
			get {
				return _selectedWidth;
			}
			set {
				_selectedWidth = value;
			}
		}
		public int SelectedHeight{
			get {
				return _selectedHeight;
			}
			set {
				_selectedHeight = value;
			}
		}

		
		


		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ListImagesDialog()
		{
			InitializeComponent();

			mainDataGrid1.AfterLoad += new EventHandler(mainDataGrid1_AfterLoad);
			mainDataGrid1.dataGridISM1.CurrentCellChanged += new EventHandler(dataGridISM1_CurrentCellChanged);
			mainDataGrid1.dataGridISM1.DoubleClick += new EventHandler(dataGridISM1_DoubleClick);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{

			DataRow dr = mainDataGrid1.dataGridISM1.GetSelectedRow();
			if(dr != null) {
				SelectedOID = (Guid) dr["OID"];
				SelectedName = dr["name"].ToString();
				SelectedWidth = dr["width"] != DBNull.Value ? (int)dr["width"] : 0;
				SelectedHeight = dr["height"] != DBNull.Value ? (int)dr["height"] : 0;
			}
			else SelectedOID = Guid.Empty;
			DialogResult = DialogResult.OK;
		}

		private void fmListGoodsImages_Load(object sender, System.EventArgs e)
		{
		}

		private void dataGridISM1_CurrentCellChanged(object sender, EventArgs e) {
			Guid prevSelected = SelectedOID;
			if(mainDataGrid1.dataGridISM1.GetSelectedRow() != null) {
				SelectedOID = (Guid)mainDataGrid1.dataGridISM1.GetSelectedRow()["OID"];
				if(prevSelected != SelectedOID){
					string adr = ELBClient.Classes.ServiceUtility.GetBrowseDir()+"BrowseImage.aspx?OID="+SelectedOID;
					webBrowser1.Navigate(adr);
				}
			}
		}

		private void dataGridISM1_DoubleClick(object sender, EventArgs e) {
			btnSave_Click(sender, e);
		}

		private void mainDataGrid1_AfterLoad(object sender, EventArgs e) {
			dataGridISM1_CurrentCellChanged(sender, e);
		}
	}
}
