﻿using System;
using ELBClient.Classes;


namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmHtmlEditDialog.
	/// </summary>
	public partial class HtmlEditDialog : EditForm
	{
		private bool _isLoaded = false;
		public string FieldString { get; set; }
		public string ClassName { get; set; }
		public bool NotNeedRoot { get; set; }
		public string AddrParams
		{
			get
			{
				return string.Format("&FieldString={0}&ClassName={1}&NotNeedRoot={2}&tm={3:yyyyMMddHHmmss}", FieldString, ClassName, NotNeedRoot, DateTime.Now);
			}
		}

		public HtmlEditDialog()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void fmHtmlEditDialog_Activated(object sender, System.EventArgs e)
		{
			if (!this.Disposing && !_isLoaded)
			{
				string adr = string.Format("{0}{1}?OID={2}{3}", ServiceUtility.GetBrowseDir().Replace("Browse", "CKBrowse"), "EditDescription.aspx",
					ObjectOID, AddrParams);
				webBrowser1.Navigate(adr);
				_isLoaded = true;
			}
		}
	}
}
