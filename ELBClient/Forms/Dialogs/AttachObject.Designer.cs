using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class AttachObject
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.ComboBox cbClasses;
		private ColumnMenuExtender.DataGridISM dataGridISM1;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Button btnCLose;
		private System.Windows.Forms.Button btnChoose;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem miChoose;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private System.Data.DataSet dataSet1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCLose = new System.Windows.Forms.Button();
			this.btnChoose = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.dataGridISM1 = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel4 = new System.Windows.Forms.Panel();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.cbClasses = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.dataSet1 = new System.Data.DataSet();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miChoose = new System.Windows.Forms.MenuItem();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).BeginInit();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCLose);
			this.panel1.Controls.Add(this.btnChoose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 283);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(542, 40);
			this.panel1.TabIndex = 0;
			// 
			// btnCLose
			// 
			this.btnCLose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCLose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCLose.Location = new System.Drawing.Point(8, 8);
			this.btnCLose.Name = "btnCLose";
			this.btnCLose.Size = new System.Drawing.Size(72, 23);
			this.btnCLose.TabIndex = 32;
			this.btnCLose.Text = "Закрыть";
			this.btnCLose.Click += new System.EventHandler(this.btnCLose_Click);
			// 
			// btnChoose
			// 
			this.btnChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnChoose.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnChoose.Location = new System.Drawing.Point(462, 8);
			this.btnChoose.Name = "btnChoose";
			this.btnChoose.Size = new System.Drawing.Size(72, 23);
			this.btnChoose.TabIndex = 31;
			this.btnChoose.Text = "Выбрать";
			this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(542, 283);
			this.panel2.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.dataGridISM1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(542, 251);
			this.panel3.TabIndex = 2;
			// 
			// dataGridISM1
			// 
			this.dataGridISM1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridISM1.DataMember = "";
			this.dataGridISM1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridISM1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridISM1.Location = new System.Drawing.Point(0, 0);
			this.dataGridISM1.Name = "dataGridISM1";
			this.dataGridISM1.Order = null;
			this.dataGridISM1.ReadOnly = true;
			this.dataGridISM1.Size = new System.Drawing.Size(542, 251);
			this.dataGridISM1.TabIndex = 0;
			this.dataGridISM1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
						this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dataGridISM1, true);
			this.dataGridISM1.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dataGridISM1.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			this.dataGridISM1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dataGridISM1;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
						this.formattableTextBoxColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.PreferredColumnWidth = 200;
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = "NK(OID)";
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название объекта";
			this.formattableTextBoxColumn1.MappingName = "OID_NK";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.NullText = "";
			this.formattableTextBoxColumn1.Width = 200;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.dataGridPager1);
			this.panel4.Controls.Add(this.cbClasses);
			this.panel4.Controls.Add(this.label6);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel4.Location = new System.Drawing.Point(0, 251);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(542, 32);
			this.panel4.TabIndex = 3;
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(8, 8);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(280, 24);
			this.dataGridPager1.TabIndex = 35;
			// 
			// cbClasses
			// 
			this.cbClasses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbClasses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbClasses.Location = new System.Drawing.Point(342, 8);
			this.cbClasses.Name = "cbClasses";
			this.cbClasses.Size = new System.Drawing.Size(192, 21);
			this.cbClasses.TabIndex = 33;
			this.cbClasses.SelectedIndexChanged += new System.EventHandler(this.cbClasses_SelectedIndexChanged);
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.Location = new System.Drawing.Point(302, 8);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(40, 16);
			this.label6.TabIndex = 34;
			this.label6.Text = "Класс:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dataSource
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
						this.miChoose});
			// 
			// miChoose
			// 
			this.miChoose.Index = 0;
			this.miChoose.Text = "Выбрать";
			this.miChoose.Click += new System.EventHandler(this.miChoose_Click);
			// 
			// fmAttachObject
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(542, 323);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(550, 350);
			this.Name = "fmAttachObject";
			this.Text = "Привязка объектов";
			this.BeforeClosing += new System.EventHandler(this.fmAttachObject_BeforeClosing);
			this.Load += new System.EventHandler(this.fmAttachObject_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).EndInit();
			this.panel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
