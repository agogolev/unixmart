using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListGoogleTaxonomiesDialog
    {
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.DataGridISM dgGooggleTaxonomies;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colCode;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dgGooggleTaxonomies = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.colCode = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGooggleTaxonomies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 51);
            this.panel1.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(456, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 27);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Закрыть";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(112, 9);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(240, 20);
            this.txtName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название:";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(376, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 27);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dgGooggleTaxonomies
            // 
            this.dgGooggleTaxonomies.BackgroundColor = System.Drawing.Color.White;
            this.dgGooggleTaxonomies.DataMember = "";
            this.dgGooggleTaxonomies.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgGooggleTaxonomies.FilterString = null;
            this.dgGooggleTaxonomies.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgGooggleTaxonomies.Location = new System.Drawing.Point(0, 51);
            this.dgGooggleTaxonomies.Name = "dgGooggleTaxonomies";
            this.dgGooggleTaxonomies.Order = null;
            this.dgGooggleTaxonomies.Size = new System.Drawing.Size(536, 362);
            this.dgGooggleTaxonomies.StockClass = "CGoogleTaxonomy";
            this.dgGooggleTaxonomies.StockNumInBatch = 0;
            this.dgGooggleTaxonomies.TabIndex = 1;
            this.dgGooggleTaxonomies.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.dgGooggleTaxonomies.DoubleClick += new System.EventHandler(this.dgGooggleTaxonomies_DoubleClick);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.dgGooggleTaxonomies;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colCode,
            this.colName});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            // 
            // colCode
            // 
            this.colCode.FieldName = null;
            this.colCode.FilterFieldName = null;
            this.colCode.Format = "";
            this.colCode.FormatInfo = null;
            this.colCode.HeaderText = "Код";
            this.colCode.MappingName = "ID";
            this.colCode.ReadOnly = true;
            this.colCode.Width = 75;
            // 
            // colName
            // 
            this.colName.FieldName = null;
            this.colName.FilterFieldName = null;
            this.colName.Format = "";
            this.colName.FormatInfo = null;
            this.colName.HeaderText = "Название";
            this.colName.MappingName = "Name";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // ListGoogleTaxonomiesDialog
            // 
            this.AcceptButton = this.btnSearch;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(536, 413);
            this.Controls.Add(this.dgGooggleTaxonomies);
            this.Controls.Add(this.panel1);
            this.Name = "ListGoogleTaxonomiesDialog";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Поиск товара";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGooggleTaxonomies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
