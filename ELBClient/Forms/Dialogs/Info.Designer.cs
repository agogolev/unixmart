﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class Info
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TextBox txtBody;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.txtBody = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// txtBody
			// 
			this.txtBody.BackColor = System.Drawing.Color.White;
			this.txtBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtBody.ForeColor = System.Drawing.Color.Black;
			this.txtBody.Location = new System.Drawing.Point(0, 0);
			this.txtBody.Multiline = true;
			this.txtBody.Name = "txtBody";
			this.txtBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtBody.Size = new System.Drawing.Size(568, 468);
			this.txtBody.TabIndex = 0;
			// 
			// Info
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(568, 468);
			this.Controls.Add(this.txtBody);
			this.Name = "Info";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
