using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class LinkedThemes
	{
		#region Windows Form Designer generated code
		private ColumnMenuExtender.DataGridISM dgThemes;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.dgThemes = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgThemes)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgThemes
			// 
			this.dgThemes.BackgroundColor = System.Drawing.Color.White;
			this.dgThemes.DataMember = "";
			this.dgThemes.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgThemes.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgThemes.Location = new System.Drawing.Point(0, 0);
			this.dgThemes.Name = "dgThemes";
			this.dgThemes.Order = null;
			this.dgThemes.Size = new System.Drawing.Size(360, 309);
			this.dgThemes.TabIndex = 0;
			this.dgThemes.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																												 this.extendedDataGridTableStyle1});
			this.dgThemes.DoubleClick += new System.EventHandler(this.dgThemes_DoubleClick);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgThemes;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																																																									this.formattableTextBoxColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "themeName";
			this.formattableTextBoxColumn1.Width = 200;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 309);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(360, 40);
			this.panel1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "Закрыть";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(280, 8);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "Отвязать";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// fmLinkedThemes
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(360, 349);
			this.Controls.Add(this.dgThemes);
			this.Controls.Add(this.panel1);
			this.Name = "fmLinkedThemes";
			this.Text = "Привязанные категории сайта";
			this.Load += new System.EventHandler(this.fmLinkedThemes_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgThemes)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
