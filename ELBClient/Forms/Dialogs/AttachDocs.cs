﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;
using System.Data;
using System.Xml;
using System.IO;
using ELBClient.Forms.Guides;
using ExceptionISM = ELBClient.ObjectProvider.ExceptionISM;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for AttachDocs.
	/// </summary>
	public partial class AttachDocs : EditForm
	{
		public string ClassName { get; private set; }
		CBinaryData _img = new CBinaryData();

		public MultiContainer AttachedDocs { get { return ((CObject)Object).Docs; } }

		public AttachDocs(string className, Guid objectOID)
		{
			InitializeComponent();
			ClassName = className;
			if (objectOID == Guid.Empty) throw new Exception("Сначала сохраните объект");
			if (className == "CGoods")
			{
				Object = new CGoods();
			}
			else throw new Exception("Unknown Type");
			ObjectOID = objectOID;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void AttachDocs_Load(object sender, EventArgs e)
		{
			loadTypes();

			LoadData();

			dataGridPager1.BindToDataGrid(dgImages);

			LoadObject("OID", "Docs");
			BindEmpty();
			FillDocs();
			ReloadGrid += LoadData;
		}

		private void BindEmpty()
		{

			tbName.DataBindings.Clear();
			tbMimeType.DataBindings.Clear();
			tbFile.DataBindings.Clear();

			tbName.DataBindings.Add("Text", _img, "Name");
			tbMimeType.DataBindings.Add("Text", _img, "MimeType");
			tbFile.DataBindings.Add("Text", _img, "FileName");

		}

		private void loadTypes()
		{
			XmlDocument doc = GetResource("fmAttachDocs.xml");

			XmlNode cNode = doc.SelectSingleNode("/root/class[@name='" + ClassName + "']");
			if (cNode == null)
				return;
			XmlAttribute attr;
			if (cNode.Attributes.Count > 0 && (attr = cNode.Attributes["allowCustom"]) != null)
			{
				if (attr.Value == "true")
				{
					cbType1.DropDownStyle = ComboBoxStyle.DropDown;
				}
				else if (attr.Value == "false")
				{
					cbType1.DropDownStyle = ComboBoxStyle.DropDownList;
				}
			}
			XmlNodeList nl = cNode.SelectNodes("item");

			foreach (XmlNode node in nl)
			{
				cbType.Items.Add(node.Attributes["name"].Value);
				cbType1.Items.Add(node.Attributes["name"].Value);
			}
			cbType.SelectedIndex = 0;
		}

		private void LoadData()
		{
			dataSet1 = lp.GetList(dgImages.GetDataXml().OuterXml);
			dataSet1.Tables[0].PrimaryKey = new[] { dataSet1.Tables[0].Columns["OID"] };
			dgImages.SetDataBinding(dataSet1, "table");
			dgImages.DisableNewDel();
		}

		private void FillDocs()
		{
			dataGrid1.DataSource = ((CObject)Object).Docs;
		}

		#region contextmenus functionality
		private void mItemAdd_Click(object sender, EventArgs e)
		{
			DataRow dr = dgImages.GetSelectedRow();
            var customType = (string)cbType.SelectedItem;
		    while (!CheckUnique(customType))
		    {
		        customType += "1";
		    }

		    var ordValue = customType;

            var g = (Guid)dr["OID"];
			var name = (string)dr["name"];
			try
			{
				((CObject)Object).Docs.Rows.Add(new object[] { g, name, "CBinaryData", ordValue });
			}
			catch
			{
				ShowWarning("Данные с таким типом уже существуют!");
				return;
			}
			SaveObject();
		}

		private void mItemDel_Click(object sender, EventArgs e)
		{
			var dr = dataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
			SaveObject();
		}

		private void mItemEdit_Click(object sender, EventArgs e)
		{
			var dr = dgImages.GetSelectedRow();
			var imageOID = (Guid)dr["OID"];
			EditForm form = new EditBinaryData();
			form.ObjectOID = imageOID;
			form.ReloadGrid += LoadData;
			form.ShowDialog(this);
		}

		private void mItemDelFromDB_Click(object sender, EventArgs e)
		{
			var dr = dgImages.GetSelectedRow();
			var OID = (Guid)dr["OID"];
			if (MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				var ex = op.DeleteObject(OID);
				if (ex != null)
				{
					if (ex.LiteralExceptionType == "System.Data.SqlClient.SqlException")
						MessageBox.Show(this, "Невозможно удалить объект!\nВозможно он связан с другим объектом.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					else if (ex.LiteralExceptionType == "MetaData.SystemOIDException")
						MessageBox.Show(this, "Невозможно удалить объект!\nДанный объект используется системой.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					else MessageBox.Show(this, ex.LiteralMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					//else MessageBox.Shows(this, e.LiteralExceptionType+": "+e.LiteralMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else LoadData();
			}
		}
		#endregion contextmenus functionality

		#region dataGrid1 functionality
		private void dataGrid1_MouseUp(object sender, MouseEventArgs e)
		{
			int CRI = dataGrid1.CurrentRowIndex;
			if (CRI >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					dataGrid1.Select(CRI);
				}
				else if (e.Button == MouseButtons.Right)
				{
					dataGrid1.Select(CRI);
					contextMenuAttImgs.Show(dataGrid1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGrid1_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGrid1.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					dataGrid1.UnSelect(dataGrid1.CurrentRowIndex);
					dataGrid1.CurrentRowIndex = hti.Row;
					dataGrid1.Select(dataGrid1.CurrentRowIndex);
				}
			}
		}

		#endregion dataGrid1 functionality

		#region dgImages functionality
		private void dataGridISM1_MouseUp(object sender, MouseEventArgs e)
		{
			int CRI = dgImages.CurrentRowIndex;
			if (CRI >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					dgImages.Select(CRI);
				}
				else if (e.Button == MouseButtons.Right)
				{
					dgImages.Select(CRI);
					contextMenuImages.Show(dgImages, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_Reload(object sender, EventArgs e)
		{
			LoadData();
		}

		private void dataGridISM1_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dgImages.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					dgImages.UnSelect(dgImages.CurrentRowIndex);
					dgImages.CurrentRowIndex = hti.Row;
					dgImages.Select(dgImages.CurrentRowIndex);
				}
			}
		}
		#endregion dgImages functionality

		private void button1_Click(object sender, EventArgs e)
		{
			if (ofdFile.ShowDialog(this) == DialogResult.OK)
			{
				tbFile.Text = ofdFile.FileName;
				var fs = new FileStream(tbFile.Text, FileMode.Open, FileAccess.Read, FileShare.Read);
				var content = new byte[fs.Length];
				fs.Read(content, 0, (int)fs.Length);
				fs.Close();
				_img.Data = content;
				tbMimeType.Text = MimeTypeUtil.CheckType(tbFile.Text);
				tbName.Text = Path.GetFileName(tbFile.Text);
				BindingContext[_img].EndCurrentEdit();
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			try
			{
			    var customType = cbType1.Text;
			    while (!CheckUnique(customType))
			    {
			        customType += "1";
			    }

			    var ordValue = customType;

                if (_img.Data != null && _img.Data.Length != 0)
				{
					if (_img.OID == Guid.Empty)
					{
						string fileName = tbFile.Text;
						if (fileName.IndexOf(@"\") != -1 || fileName.IndexOf("/") != -1)
						{
							tbFile.Text = Path.GetFileName(fileName);
						}
						BindingContext[_img].EndCurrentEdit();
						ExceptionISM exISM;
						var newobj = op.SaveObject(_img.SaveXml(), out exISM);
						_img.LoadXml(newobj);
						if (exISM != null)
						{
							if (exISM.LiteralExceptionType == "BackendService.Classes.DBException")
							{
								ShowError(this, "Ошибка сохранения: ошибка целостности");
								throw new Exception(exISM.LiteralMessage);
							}
							ShowError(this, "Ошибка сохранения: " + newobj);
							throw new Exception(exISM.LiteralMessage);
						}
						if (ReloadGrid != null)
							ReloadGrid();
					}
					string name = tbName.Text;
					try
					{
						((CObject)Object).Docs.Rows.Add(new object[] { _img.OID, name, "CBinaryData", ordValue });
					}
					catch
					{
						ShowWarning("Данные с таким типом уже существуют!");
						return;
					}
					SaveObject();
					_img = new CBinaryData();
					BindEmpty();
				}
			}
			catch (Exception exc)
			{
				//может возникнуть из-за разрыва соединения
				ShowError("Ошибка сохранения: " + exc.Message);
			}

		}

        private bool CheckUnique(string customType)
        {
            return ((CObject) Object).Docs.Rows.Find(customType) == null;
        }
    }
}
