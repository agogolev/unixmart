using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListThemesDialog
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TreeView tv1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.tv1 = new System.Windows.Forms.TreeView();
			this.SuspendLayout();
			// 
			// tv1
			// 
			this.tv1.AllowDrop = true;
			this.tv1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tv1.HideSelection = false;
			this.tv1.Location = new System.Drawing.Point(0, 0);
			this.tv1.Name = "tv1";
			this.tv1.Size = new System.Drawing.Size(396, 358);
			this.tv1.TabIndex = 3;
			this.tv1.DoubleClick += new System.EventHandler(this.fmListThemesDialog_DoubleClick);
			// 
			// ListThemesDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(396, 358);
			this.Controls.Add(this.tv1);
			this.Name = "ListThemesDialog";
			this.Text = "Каталог";
			this.Load += new System.EventHandler(this.fmListThemesDialog_Load);
			this.DoubleClick += new System.EventHandler(this.fmListThemesDialog_DoubleClick);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
