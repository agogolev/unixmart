using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListThemesMultipleDialog
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TreeView tv1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.tv1 = new System.Windows.Forms.TreeView();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// tv1
			// 
			this.tv1.AllowDrop = true;
			this.tv1.CheckBoxes = true;
			this.tv1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tv1.HideSelection = false;
			this.tv1.Location = new System.Drawing.Point(0, 0);
			this.tv1.Name = "tv1";
			this.tv1.Size = new System.Drawing.Size(396, 318);
			this.tv1.TabIndex = 3;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 318);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(396, 40);
			this.panel1.TabIndex = 4;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(318, 7);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 26);
			this.button1.TabIndex = 0;
			this.button1.Text = "Выбрать";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// ListThemesMultipleDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(396, 358);
			this.Controls.Add(this.tv1);
			this.Controls.Add(this.panel1);
			this.Name = "ListThemesMultipleDialog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Каталог";
			this.Load += new System.EventHandler(this.fmListThemesDialog_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private Panel panel1;
		private Button button1;
	}
}
