﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListGoodsCharacter.
	/// </summary>
	public partial class ListGoodsCharacterDialog : GenericForm
	{
		public Guid SelectedOID;
		public string SelectedNK;
		public string Description;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		
		
		

		public ListGoodsCharacterDialog()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void LoadData()
		{
			dataSet1 = lp.GetList(dataGridISM1.GetDataXml().OuterXml);
			dataGridISM1.Enabled = false;
			dataGridISM1.SetDataBinding(dataSet1, "table");
			dataGridISM1.Enabled = true;
			dataGridISM1.Width++;dataGridISM1.Width--;
		}

		private void fmListGoodsCharacter_Load(object sender, System.EventArgs e)
		{
			LoadData();
			dataGridPager1.BindToDataGrid(dataGridISM1);
		}

		private void miSelect_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			Close();
		}

		
		private void dataGridISM1_DoubleClick(object sender, System.EventArgs e)
		{
			System.Drawing.Point pt = dataGridISM1.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dataGridISM1.HitTest(pt); 
			if(hti.Type == DataGrid.HitTestType.Cell) 
			{
//				DataRow dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, pt.X, pt.Y);
				DataRow dr = dataGridISM1.GetSelectedRow();
				SelectedOID = (Guid)dr["OID"];
				SelectedNK = (String)dr["name"];
				Description = tbDescription.Text;

				this.DialogResult = DialogResult.OK;
				Close();
			}
		}

		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				DataRow dr = null;
//				if ((dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, e)) != null)
				if ((dr = dataGridISM1.GetSelectedRow()) != null)
				{
					SelectedOID = (Guid)dr["OID"];
					SelectedNK = (String)dr["name"];
					Description = tbDescription.Text;
					contextMenu1.Show(dataGridISM1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_Reload(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void btnChoose_Click(object sender, System.EventArgs e)
		{
			DataRow dr = null;
			//Get selected ROW
			if ((dr = dataSet1.Tables[0].Rows[dataGridISM1.CurrentRowIndex]) != null)
			{
				SelectedOID = (Guid)dr["OID"];
				SelectedNK = (String)dr["name"];
				Description = tbDescription.Text;
			}

			this.DialogResult = DialogResult.OK;
			this.Close();
		}

	}
}
