using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class MassAttachImages
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button btnSave;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox cbType;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.ContextMenu contextMenuImages;
		private System.Windows.Forms.MenuItem mItemAdd;
		private System.Windows.Forms.MenuItem mItemEdit;
		private System.Windows.Forms.MenuItem mItemDelFromDB;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbFile;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private ColumnMenuExtender.DataBoundTextBox tbHeight;
		private ColumnMenuExtender.DataBoundTextBox tbWidth;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbMimeType;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox cbType1;
		private System.Windows.Forms.OpenFileDialog ofdFile;
		private System.Windows.Forms.Button button3;
		private ColumnMenuExtender.DataGridISM dgImages;
		private ColumnMenuExtender.ExtendedDataGrid dgAllowedSize;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private System.Data.DataSet dataSet1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn9;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn10;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.btnSave = new System.Windows.Forms.Button();
			this.dgImages = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.dataSet1 = new System.Data.DataSet();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn9 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn10 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.dgAllowedSize = new ColumnMenuExtender.ExtendedDataGrid();
			this.extendedDataGridTableStyle3 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.button3 = new System.Windows.Forms.Button();
			this.cbType1 = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tbHeight = new ColumnMenuExtender.DataBoundTextBox();
			this.tbWidth = new ColumnMenuExtender.DataBoundTextBox();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbMimeType = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.tbFile = new System.Windows.Forms.TextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.cbType = new System.Windows.Forms.ComboBox();
			this.contextMenuImages = new System.Windows.Forms.ContextMenu();
			this.mItemEdit = new System.Windows.Forms.MenuItem();
			this.mItemDelFromDB = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.mItemAdd = new System.Windows.Forms.MenuItem();
			this.ofdFile = new System.Windows.Forms.OpenFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.dgImages)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgAllowedSize)).BeginInit();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(456, 8);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 24);
			this.btnSave.TabIndex = 32;
			this.btnSave.Text = "Ок";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// dgImages
			// 
			this.dgImages.AllowSorting = false;
			this.dgImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
									| System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgImages.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgImages.DataMember = "";
			this.dgImages.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgImages.Location = new System.Drawing.Point(8, 8);
			this.dgImages.Name = "dgImages";
			this.dgImages.Order = null;
			this.dgImages.ReadOnly = true;
			this.dgImages.Size = new System.Drawing.Size(512, 240);
			this.dgImages.StockClass = "CBinaryData";
			this.dgImages.TabIndex = 33;
			this.dgImages.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dgImages, true);
			this.dgImages.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dgImages.Click += new System.EventHandler(this.dgImages_Click);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dgImages;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7,
            this.formattableTextBoxColumn8});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.PreferredColumnWidth = 100;
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.NullText = "";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Имя файла";
			this.formattableTextBoxColumn2.MappingName = "fileName";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.NullText = "";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn6
			// 
			this.formattableTextBoxColumn6.FieldName = null;
			this.formattableTextBoxColumn6.FilterFieldName = null;
			this.formattableTextBoxColumn6.Format = "";
			this.formattableTextBoxColumn6.FormatInfo = null;
			this.formattableTextBoxColumn6.HeaderText = "Ширина";
			this.formattableTextBoxColumn6.MappingName = "width";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
			this.formattableTextBoxColumn6.Width = 75;
			// 
			// formattableTextBoxColumn7
			// 
			this.formattableTextBoxColumn7.FieldName = null;
			this.formattableTextBoxColumn7.FilterFieldName = null;
			this.formattableTextBoxColumn7.Format = "";
			this.formattableTextBoxColumn7.FormatInfo = null;
			this.formattableTextBoxColumn7.HeaderText = "Высота";
			this.formattableTextBoxColumn7.MappingName = "height";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
			this.formattableTextBoxColumn7.Width = 75;
			// 
			// formattableTextBoxColumn8
			// 
			this.formattableTextBoxColumn8.FieldName = null;
			this.formattableTextBoxColumn8.FilterFieldName = null;
			this.formattableTextBoxColumn8.Format = "";
			this.formattableTextBoxColumn8.FormatInfo = null;
			this.formattableTextBoxColumn8.HeaderText = "mime тип";
			this.formattableTextBoxColumn8.MappingName = "mimeType";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn8, this.menuFilterSort1);
			this.formattableTextBoxColumn8.Width = 75;
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(8, 256);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(288, 24);
			this.dataGridPager1.TabIndex = 34;
			// 
			// dataSource
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Тип";
			this.formattableTextBoxColumn3.MappingName = "type";
			this.formattableTextBoxColumn3.Width = 150;
			// 
			// formattableTextBoxColumn9
			// 
			this.formattableTextBoxColumn9.FieldName = null;
			this.formattableTextBoxColumn9.FilterFieldName = null;
			this.formattableTextBoxColumn9.Format = "";
			this.formattableTextBoxColumn9.FormatInfo = null;
			this.formattableTextBoxColumn9.HeaderText = "Ширина";
			this.formattableTextBoxColumn9.MappingName = "width";
			this.formattableTextBoxColumn9.Width = 50;
			// 
			// formattableTextBoxColumn10
			// 
			this.formattableTextBoxColumn10.FieldName = null;
			this.formattableTextBoxColumn10.FilterFieldName = null;
			this.formattableTextBoxColumn10.Format = "";
			this.formattableTextBoxColumn10.FormatInfo = null;
			this.formattableTextBoxColumn10.HeaderText = "Высота";
			this.formattableTextBoxColumn10.MappingName = "height";
			this.formattableTextBoxColumn10.Width = 50;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 309);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(536, 40);
			this.panel1.TabIndex = 37;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 8);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 33;
			this.btnCancel.Text = "Отказ";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tabControl1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(536, 309);
			this.panel2.TabIndex = 38;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(536, 309);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.dgAllowedSize);
			this.tabPage1.Controls.Add(this.button3);
			this.tabPage1.Controls.Add(this.cbType1);
			this.tabPage1.Controls.Add(this.label7);
			this.tabPage1.Controls.Add(this.label5);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Controls.Add(this.tbHeight);
			this.tabPage1.Controls.Add(this.tbWidth);
			this.tabPage1.Controls.Add(this.tbName);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.tbMimeType);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.button1);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.tbFile);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(528, 283);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Новая";
			// 
			// dgAllowedSize
			// 
			this.dgAllowedSize.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
									| System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.dgAllowedSize.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgAllowedSize.CaptionVisible = false;
			this.dgAllowedSize.DataMember = "";
			this.dgAllowedSize.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgAllowedSize.Location = new System.Drawing.Point(176, 120);
			this.dgAllowedSize.Name = "dgAllowedSize";
			this.dgAllowedSize.Size = new System.Drawing.Size(344, 160);
			this.dgAllowedSize.TabIndex = 60;
			this.dgAllowedSize.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle3});
			// 
			// extendedDataGridTableStyle3
			// 
			this.extendedDataGridTableStyle3.DataGrid = this.dgAllowedSize;
			this.extendedDataGridTableStyle3.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn9,
            this.formattableTextBoxColumn10});
			this.extendedDataGridTableStyle3.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle3.MappingName = "table";
			this.extendedDataGridTableStyle3.ReadOnly = true;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(256, 80);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(112, 20);
			this.button3.TabIndex = 59;
			this.button3.Text = "Добавить в базу";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// cbType1
			// 
			this.cbType1.Location = new System.Drawing.Point(80, 80);
			this.cbType1.Name = "cbType1";
			this.cbType1.Size = new System.Drawing.Size(168, 21);
			this.cbType1.TabIndex = 57;
			this.cbType1.SelectedIndexChanged += new System.EventHandler(this.cbType1_SelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 80);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(72, 16);
			this.label7.TabIndex = 56;
			this.label7.Text = "Тип:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 184);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 55;
			this.label5.Text = "Высота:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 152);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 16);
			this.label4.TabIndex = 54;
			this.label4.Text = "Ширина:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbHeight
			// 
			this.tbHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbHeight.BoundProp = null;
			this.tbHeight.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbHeight.IsCurrency = false;
			this.tbHeight.Location = new System.Drawing.Point(80, 184);
			this.tbHeight.Name = "tbHeight";
			this.tbHeight.ReadOnly = true;
			this.tbHeight.Size = new System.Drawing.Size(64, 20);
			this.tbHeight.TabIndex = 53;
			// 
			// tbWidth
			// 
			this.tbWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbWidth.BoundProp = null;
			this.tbWidth.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbWidth.IsCurrency = false;
			this.tbWidth.Location = new System.Drawing.Point(80, 152);
			this.tbWidth.Name = "tbWidth";
			this.tbWidth.ReadOnly = true;
			this.tbWidth.Size = new System.Drawing.Size(64, 20);
			this.tbWidth.TabIndex = 52;
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(80, 48);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(432, 20);
			this.tbName.TabIndex = 48;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 49;
			this.label1.Text = "Название:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbMimeType
			// 
			this.tbMimeType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbMimeType.Location = new System.Drawing.Point(80, 120);
			this.tbMimeType.Name = "tbMimeType";
			this.tbMimeType.ReadOnly = true;
			this.tbMimeType.Size = new System.Drawing.Size(88, 20);
			this.tbMimeType.TabIndex = 51;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 120);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(72, 16);
			this.label6.TabIndex = 50;
			this.label6.Text = "MIME тип:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(448, 16);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(72, 20);
			this.button1.TabIndex = 41;
			this.button1.Text = "Выбрать";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 42;
			this.label3.Text = "Файл:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbFile
			// 
			this.tbFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbFile.Location = new System.Drawing.Point(80, 16);
			this.tbFile.Name = "tbFile";
			this.tbFile.Size = new System.Drawing.Size(360, 20);
			this.tbFile.TabIndex = 40;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.dgImages);
			this.tabPage2.Controls.Add(this.dataGridPager1);
			this.tabPage2.Controls.Add(this.cbType);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(528, 283);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Картинка из базы";
			// 
			// cbType
			// 
			this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbType.Location = new System.Drawing.Point(336, 256);
			this.cbType.Name = "cbType";
			this.cbType.Size = new System.Drawing.Size(184, 21);
			this.cbType.TabIndex = 37;
			this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
			// 
			// contextMenuImages
			// 
			this.contextMenuImages.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mItemEdit,
            this.mItemDelFromDB,
            this.menuItem5,
            this.mItemAdd});
			// 
			// mItemEdit
			// 
			this.mItemEdit.Index = 0;
			this.mItemEdit.Text = "Редактировать";
			this.mItemEdit.Click += new System.EventHandler(this.mItemEdit_Click);
			// 
			// mItemDelFromDB
			// 
			this.mItemDelFromDB.Index = 1;
			this.mItemDelFromDB.Text = "Удалить";
			this.mItemDelFromDB.Click += new System.EventHandler(this.mItemDelFromDB_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 2;
			this.menuItem5.Text = "-";
			// 
			// mItemAdd
			// 
			this.mItemAdd.Index = 3;
			this.mItemAdd.Text = "Добавить";
			this.mItemAdd.Click += new System.EventHandler(this.mItemAdd_Click);
			// 
			// fmMassAttachImages
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(536, 349);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(0, 200);
			this.Name = "fmMassAttachImages";
			this.Text = "Привязка картинок";
			this.Load += new System.EventHandler(this.fmListGoodsImages_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgImages)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgAllowedSize)).EndInit();
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
