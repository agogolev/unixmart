﻿namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmInfo.
	/// </summary>
	public partial class Info : GenericForm
	{
		public bool ReadOnly
		{
			get
			{
				return txtBody.ReadOnly;
			}
			set
			{
				txtBody.ReadOnly = value;
			}
		}
		public string Body {
			get {
				return txtBody.Text;
			}
			set {
				txtBody.Text = value;
				txtBody.SelectionStart = 0;
				txtBody.SelectionLength = 0;
			}
		}

		public Info()
		{
			InitializeComponent();
		}
	}
}
