﻿using System;
using System.Drawing;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmViewTicket.
	/// </summary>
	public partial class ViewTicket : GenericForm {
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ViewTicket(string RawTicket, string Header, Icon HIcon) {
			InitializeComponent();
			this.Text = Header;
			this.Icon = HIcon;
			this.tbTicket.Text = RawTicket.Replace("<br>",Environment.NewLine.ToString());
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion
	}
}
