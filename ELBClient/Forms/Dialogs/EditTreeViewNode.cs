﻿using System;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmEditTreeViewNode.
	/// </summary>
	public partial class EditTreeViewNode : GenericForm
	{	
		public bool AllowObject = true;
		public int NodeID;
		public string[] AllowedClassName;
		public StringProperty NodeName = new StringProperty("");
		public GuidProperty NodeObject = new GuidProperty(Guid.Empty);
		public StringProperty NodeObjectNK = new StringProperty("");
		public string NodeObjectNKRus;

		public EditTreeViewNode()
		{
			InitializeComponent();
		}

		private void fmEditTreeNodeDialog_Load(object sender, System.EventArgs e)
		{
			DataBinding();
		}

		private void DataBinding()
		{
			if(!AllowObject) {
				btnAddObject.Enabled = false;
				btnDelObject.Enabled = false;
				btnEditObject.Enabled = false;
			}
			if(AllowedClassName == null || (AllowedClassName.Length != 1 && AllowedClassName.Length != 1)) {
				btnEditObject.Enabled = false;
			}
			tbID.Text = NodeID.ToString();
			tbName.DataBindings.Add("Text", NodeName, "Prop");
			tbObject.DataBindings.Add("Text", NodeObjectNK, "Prop");
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			BindingContext[NodeName].EndCurrentEdit();
			BindingContext[NodeObject].EndCurrentEdit();
			SaveObject();
		}

		private void SaveObject()
		{
			this.DialogResult = DialogResult.OK;
		}

		private void btnAddObject_Click(object sender, System.EventArgs e)
		{
			AttachObject fm = new AttachObject();
			if(AllowedClassName != null) fm.AllowedClassName = AllowedClassName;
			if(fm.ShowDialog(this) == DialogResult.OK)
			{
				NodeObject.Prop = fm.SelectedOID;
				tbObject.Text = fm.SelectedNK;
				if(tbName.Text == "")
					tbName.Text = fm.SelectedNK;
			}
		}

		private void btnEditObject_Click(object sender, System.EventArgs e) 
		{
			string className = AllowedClassName[0];
			EditForm frm = Classes.FormSelection.GetEditForm(className);
			if(frm != null) {
				switch (className) {
					case "CArticle":
						if(NodeObject.Prop == Guid.Empty) {
							Classes.CArticle art = new Classes.CArticle();
							ObjectProvider.ObjectProvider provider = Classes.ServiceUtility.ObjectProvider;
							ObjectProvider.ExceptionISM exISM;
							string xml = provider.SaveObject(art.SaveXml(), out exISM);
							art.LoadXml(xml);
							NodeObject.Prop = art.OID;
						}
						break;
				}
				frm.ObjectOID = NodeObject.Prop;
				if(frm.ShowDialog(this) == DialogResult.OK) {
					NodeObject.Prop = frm.ObjectOID;
					tbObject.Text = frm.ObjectNK;
					if(tbName.Text == "")
						tbName.Text = frm.ObjectNK;
					NodeObjectNK.Prop = frm.ObjectNK;
					NodeObjectNKRus = frm.ObjectClassName;
				}
			}
		}

		private void btnDelObject_Click(object sender, System.EventArgs e)
		{
			NodeObject.Prop = Guid.Empty;
			tbObject.Text = "";
		}

		private bool isModified()
		{
			return NodeObject.IsModified || NodeName.IsModified || NodeObjectNK.IsModified;
		}

		private void fmEditTreeNodeDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(DialogResult == DialogResult.OK) return;
			if(isModified())
			{
				DialogResult dr = MessageBox.Show(this, "Елемент был изменен, хотите сохранить?", "Внимание!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
				if (dr == DialogResult.Yes)
					SaveObject();
				else if (dr == DialogResult.Cancel)
					e.Cancel = true;
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindingContext[NodeName].EndCurrentEdit();
			BindingContext[NodeObject].EndCurrentEdit();		
		}
	}
}
