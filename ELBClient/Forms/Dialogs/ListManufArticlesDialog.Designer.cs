using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListManufArticlesDialog
	{
		#region Windows Form Designer generated code

		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.CompanyGrid = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel2 = new System.Windows.Forms.Panel();
			this.CompanyNameText = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnSearch = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.ArticleHeaderText = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.SearchButton = new System.Windows.Forms.Button();
			this.ArticlesGrid = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.CompanyGrid)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ArticlesGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 477);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(817, 40);
			this.panel1.TabIndex = 4;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(739, 7);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 26);
			this.button1.TabIndex = 0;
			this.button1.Text = "Выбрать";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.CompanyGrid);
			this.splitContainer1.Panel1.Controls.Add(this.panel2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.ArticlesGrid);
			this.splitContainer1.Panel2.Controls.Add(this.panel3);
			this.splitContainer1.Size = new System.Drawing.Size(817, 477);
			this.splitContainer1.SplitterDistance = 387;
			this.splitContainer1.TabIndex = 5;
			// 
			// CompanyGrid
			// 
			this.CompanyGrid.BackgroundColor = System.Drawing.Color.White;
			this.CompanyGrid.DataMember = "";
			this.CompanyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CompanyGrid.FilterString = null;
			this.CompanyGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.CompanyGrid.Location = new System.Drawing.Point(0, 47);
			this.CompanyGrid.Name = "CompanyGrid";
			this.CompanyGrid.Order = null;
			this.CompanyGrid.Size = new System.Drawing.Size(387, 430);
			this.CompanyGrid.StockClass = "CCompany";
			this.CompanyGrid.StockNumInBatch = 0;
			this.CompanyGrid.TabIndex = 2;
			this.CompanyGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.CompanyGrid;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableBooleanColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "companyName";
			this.formattableTextBoxColumn1.Width = 200;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.CompanyNameText);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.btnSearch);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(387, 47);
			this.panel2.TabIndex = 1;
			// 
			// CompanyNameText
			// 
			this.CompanyNameText.Location = new System.Drawing.Point(73, 13);
			this.CompanyNameText.Name = "CompanyNameText";
			this.CompanyNameText.Size = new System.Drawing.Size(240, 20);
			this.CompanyNameText.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Название:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnSearch
			// 
			this.btnSearch.Location = new System.Drawing.Point(314, 10);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(70, 27);
			this.btnSearch.TabIndex = 1;
			this.btnSearch.Text = "Поиск";
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.ArticleHeaderText);
			this.panel3.Controls.Add(this.label2);
			this.panel3.Controls.Add(this.SearchButton);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(426, 47);
			this.panel3.TabIndex = 2;
			// 
			// ArticleHeaderText
			// 
			this.ArticleHeaderText.Location = new System.Drawing.Point(73, 13);
			this.ArticleHeaderText.Name = "ArticleHeaderText";
			this.ArticleHeaderText.Size = new System.Drawing.Size(240, 20);
			this.ArticleHeaderText.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(8, 17);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Название:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// SearchButton
			// 
			this.SearchButton.Location = new System.Drawing.Point(314, 10);
			this.SearchButton.Name = "SearchButton";
			this.SearchButton.Size = new System.Drawing.Size(70, 27);
			this.SearchButton.TabIndex = 1;
			this.SearchButton.Text = "Поиск";
			this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
			// 
			// ArticlesGrid
			// 
			this.ArticlesGrid.BackgroundColor = System.Drawing.Color.White;
			this.ArticlesGrid.DataMember = "";
			this.ArticlesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ArticlesGrid.FilterString = null;
			this.ArticlesGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.ArticlesGrid.Location = new System.Drawing.Point(0, 47);
			this.ArticlesGrid.Name = "ArticlesGrid";
			this.ArticlesGrid.Order = null;
			this.ArticlesGrid.Size = new System.Drawing.Size(426, 430);
			this.ArticlesGrid.TabIndex = 3;
			this.ArticlesGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
			// 
			// extendedDataGridTableStyle2
			// 
			this.extendedDataGridTableStyle2.DataGrid = this.ArticlesGrid;
			this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn2});
			this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle2.MappingName = "Table";
			this.extendedDataGridTableStyle2.ReadOnly = true;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Название";
			this.formattableTextBoxColumn2.MappingName = "header";
			this.formattableTextBoxColumn2.Width = 200;
			// 
			// formattableBooleanColumn1
			// 
			this.formattableBooleanColumn1.FieldName = null;
			this.formattableBooleanColumn1.FilterFieldName = null;
			this.formattableBooleanColumn1.HeaderText = "Выбрать";
			this.formattableBooleanColumn1.MappingName = "selected";
			this.formattableBooleanColumn1.Width = 75;
			// 
			// ListManufArticlesDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(817, 517);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.panel1);
			this.Name = "ListManufArticlesDialog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Статьи";
			this.Load += new System.EventHandler(this.fmListThemesDialog_Load);
			this.panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.CompanyGrid)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ArticlesGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private Panel panel1;
		private Button button1;
		private SplitContainer splitContainer1;
		private Panel panel2;
		private TextBox CompanyNameText;
		private Label label1;
		private Button btnSearch;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.DataGridISM CompanyGrid;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private ColumnMenuExtender.DataGridISM ArticlesGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private Panel panel3;
		private TextBox ArticleHeaderText;
		private Label label2;
		private Button SearchButton;
	}
}
