using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListGoodsDialog
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.TextBox txtCode;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.DataGridISM dgGoods;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colCode;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.FormattableTextBoxColumn colManufacturer;
		private ColumnMenuExtender.FormattableTextBoxColumn colPrice;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.SelectButton = new System.Windows.Forms.Button();
			this.ObjectId = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btnClose = new System.Windows.Forms.Button();
			this.txtCode = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnSearch = new System.Windows.Forms.Button();
			this.dgGoods = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colCode = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colManufacturer = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colPrice = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.selectedColumn = new ColumnMenuExtender.FormattableBooleanColumn();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.SelectButton);
			this.panel1.Controls.Add(this.ObjectId);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.txtCode);
			this.panel1.Controls.Add(this.txtName);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnSearch);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(536, 93);
			this.panel1.TabIndex = 0;
			// 
			// SelectButton
			// 
			this.SelectButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SelectButton.Location = new System.Drawing.Point(456, 57);
			this.SelectButton.Name = "SelectButton";
			this.SelectButton.Size = new System.Drawing.Size(75, 27);
			this.SelectButton.TabIndex = 7;
			this.SelectButton.Text = "Выбрать";
			this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
			// 
			// ObjectId
			// 
			this.ObjectId.Location = new System.Drawing.Point(112, 63);
			this.ObjectId.Name = "ObjectId";
			this.ObjectId.Size = new System.Drawing.Size(100, 20);
			this.ObjectId.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 63);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 21);
			this.label3.TabIndex = 5;
			this.label3.Text = "Код сайта:";
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Location = new System.Drawing.Point(456, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 4;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// txtCode
			// 
			this.txtCode.Location = new System.Drawing.Point(112, 37);
			this.txtCode.Name = "txtCode";
			this.txtCode.Size = new System.Drawing.Size(100, 20);
			this.txtCode.TabIndex = 3;
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(112, 9);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(240, 20);
			this.txtName.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 37);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 21);
			this.label2.TabIndex = 1;
			this.label2.Text = "Код SV:";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "Название:";
			// 
			// btnSearch
			// 
			this.btnSearch.Location = new System.Drawing.Point(376, 9);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(75, 27);
			this.btnSearch.TabIndex = 1;
			this.btnSearch.Text = "Поиск";
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// dgGoods
			// 
			this.dgGoods.BackgroundColor = System.Drawing.Color.White;
			this.dgGoods.DataMember = "";
			this.dgGoods.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgGoods.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgGoods.Location = new System.Drawing.Point(0, 93);
			this.dgGoods.Name = "dgGoods";
			this.dgGoods.Order = null;
			this.dgGoods.Size = new System.Drawing.Size(536, 320);
			this.dgGoods.StockClass = "CGoods";
			this.dgGoods.StockNumInBatch = 0;
			this.dgGoods.TabIndex = 1;
			this.dgGoods.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgGoods;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colCode,
            this.formattableTextBoxColumn1,
            this.colName,
            this.colManufacturer,
            this.colPrice,
            this.selectedColumn});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			// 
			// colCode
			// 
			this.colCode.FieldName = null;
			this.colCode.FilterFieldName = null;
			this.colCode.Format = "";
			this.colCode.FormatInfo = null;
			this.colCode.HeaderText = "Код";
			this.colCode.MappingName = "ID";
			this.colCode.ReadOnly = true;
			this.colCode.Width = 75;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Код сайта";
			this.formattableTextBoxColumn1.MappingName = "objectId";
			this.formattableTextBoxColumn1.ReadOnly = true;
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// colName
			// 
			this.colName.FieldName = null;
			this.colName.FilterFieldName = null;
			this.colName.Format = "";
			this.colName.FormatInfo = null;
			this.colName.HeaderText = "Название";
			this.colName.MappingName = "ShortName";
			this.colName.ReadOnly = true;
			this.colName.Width = 200;
			// 
			// colManufacturer
			// 
			this.colManufacturer.FieldName = "NK(manufacturer)";
			this.colManufacturer.FilterFieldName = null;
			this.colManufacturer.Format = "";
			this.colManufacturer.FormatInfo = null;
			this.colManufacturer.HeaderText = "Производитель";
			this.colManufacturer.MappingName = "manufacturer_name";
			this.colManufacturer.ReadOnly = true;
			this.colManufacturer.Width = 75;
			// 
			// colPrice
			// 
			this.colPrice.FieldName = null;
			this.colPrice.FilterFieldName = null;
			this.colPrice.Format = "# ##0,00";
			this.colPrice.FormatInfo = null;
			this.colPrice.HeaderText = "Цена";
			this.colPrice.MappingName = "price";
			this.colPrice.ReadOnly = true;
			this.colPrice.Width = 75;
			// 
			// selectedColumn
			// 
			this.selectedColumn.AllowNull = false;
			this.selectedColumn.FieldName = null;
			this.selectedColumn.FilterFieldName = null;
			this.selectedColumn.MappingName = "isSelected";
			this.selectedColumn.Width = 75;
			// 
			// ListGoodsDialog
			// 
			this.AcceptButton = this.btnSearch;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(536, 413);
			this.Controls.Add(this.dgGoods);
			this.Controls.Add(this.panel1);
			this.Name = "ListGoodsDialog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Поиск товара";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private TextBox ObjectId;
		private Label label3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private Button SelectButton;
		private ColumnMenuExtender.FormattableBooleanColumn selectedColumn;
	}
}
