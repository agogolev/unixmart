using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class EditTreeViewNode
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbObject;
		private System.Windows.Forms.Button btnAddObject;
		private System.Windows.Forms.Button btnDelObject;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbID;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button btnEditObject;
		private System.ComponentModel.IContainer components;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTreeViewNode));
			this.tbName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.tbObject = new System.Windows.Forms.TextBox();
			this.btnAddObject = new System.Windows.Forms.Button();
			this.btnDelObject = new System.Windows.Forms.Button();
			this.tbID = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.btnEditObject = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// tbName
			// 
			this.tbName.AcceptsReturn = true;
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(72, 9);
			this.tbName.Multiline = true;
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(256, 102);
			this.tbName.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 19);
			this.label1.TabIndex = 25;
			this.label1.Text = "Название:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 184);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(72, 26);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(264, 184);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 26);
			this.btnSave.TabIndex = 6;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 125);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 18);
			this.label3.TabIndex = 31;
			this.label3.Text = "Объект:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbObject
			// 
			this.tbObject.Enabled = false;
			this.tbObject.Location = new System.Drawing.Point(72, 122);
			this.tbObject.Name = "tbObject";
			this.tbObject.Size = new System.Drawing.Size(192, 20);
			this.tbObject.TabIndex = 7;
			// 
			// btnAddObject
			// 
			this.btnAddObject.Location = new System.Drawing.Point(288, 120);
			this.btnAddObject.Name = "btnAddObject";
			this.btnAddObject.Size = new System.Drawing.Size(24, 28);
			this.btnAddObject.TabIndex = 3;
			this.btnAddObject.Text = "...";
			this.btnAddObject.Click += new System.EventHandler(this.btnAddObject_Click);
			// 
			// btnDelObject
			// 
			this.btnDelObject.Location = new System.Drawing.Point(312, 120);
			this.btnDelObject.Name = "btnDelObject";
			this.btnDelObject.Size = new System.Drawing.Size(25, 28);
			this.btnDelObject.TabIndex = 4;
			this.btnDelObject.Text = "X";
			this.btnDelObject.Click += new System.EventHandler(this.btnDelObject_Click);
			// 
			// tbID
			// 
			this.tbID.BackColor = System.Drawing.Color.White;
			this.tbID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbID.Location = new System.Drawing.Point(72, 157);
			this.tbID.Name = "tbID";
			this.tbID.ReadOnly = true;
			this.tbID.Size = new System.Drawing.Size(56, 20);
			this.tbID.TabIndex = 8;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 159);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 19);
			this.label5.TabIndex = 37;
			this.label5.Text = "ID:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnEditObject
			// 
			this.btnEditObject.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
			this.btnEditObject.ImageIndex = 0;
			this.btnEditObject.ImageList = this.imageList1;
			this.btnEditObject.Location = new System.Drawing.Point(264, 120);
			this.btnEditObject.Name = "btnEditObject";
			this.btnEditObject.Size = new System.Drawing.Size(24, 28);
			this.btnEditObject.TabIndex = 2;
			this.btnEditObject.Click += new System.EventHandler(this.btnEditObject_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
			this.imageList1.Images.SetKeyName(0, "");
			// 
			// EditTreeViewNode
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(346, 220);
			this.Controls.Add(this.btnEditObject);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.tbID);
			this.Controls.Add(this.tbObject);
			this.Controls.Add(this.tbName);
			this.Controls.Add(this.btnDelObject);
			this.Controls.Add(this.btnAddObject);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(344, 151);
			this.Name = "EditTreeViewNode";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование листа";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.fmEditTreeNodeDialog_Closing);
			this.Load += new System.EventHandler(this.fmEditTreeNodeDialog_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
