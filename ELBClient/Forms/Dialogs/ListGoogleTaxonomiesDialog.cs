﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
    /// <summary>
    /// Summary description for ListGoogleTaxonomiesDialog.
    /// </summary>
    public partial class ListGoogleTaxonomiesDialog : GenericForm
	{
		public DataSetISM DataSource { get; set; }
        public DataRow SelectedRow
        {
            get
            {
                return dgGooggleTaxonomies.GetSelectedRow();
            }
        }

        public int ShopType { get; set; }

		public ListGoogleTaxonomiesDialog()
		{
			InitializeComponent();
		}


		private void btnClose_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
		}

		private void btnSearch_Click(object sender, EventArgs e) {
			if(string.IsNullOrWhiteSpace(txtName.Text)) return;
		    var name = txtName.Text;
            var sb = new StringBuilder();
			sb.Append(@"
SELECT
	g.OID,
	name = g.name
FROM
	t_GoogleTaxonomy g");
			sb.Append(@"
WHERE
	g.name like '%" + name.Replace("'", "''") + @"%'
ORDER BY
    g.name");
			DataSource = new DataSetISM(lp.GetDataSetSql(sb.ToString()));

			dgGooggleTaxonomies.SetDataBinding(DataSource, "table");
			dgGooggleTaxonomies.DisableNewDel();
		}

        private void dgGooggleTaxonomies_DoubleClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
