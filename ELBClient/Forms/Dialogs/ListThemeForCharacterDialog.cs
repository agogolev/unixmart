﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListThemeForCharacterDialog.
	/// </summary>
	public partial class ListThemeForCharacterDialog : GenericForm
	{
		private ListProvider.ListProvider listProvider = ELBClient.Classes.ServiceUtility.ListProvider;
		public Guid SelectedOID;
		public string SelectedName;
		public bool SelectPressed = false;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		
		
		
		
		
		
		
		

		public ListThemeForCharacterDialog()
		{
			InitializeComponent();
			dataGridISM1.CreateAuxFilter("themeType","ThemeType",FilterVerb.Equal,false,new Object[]{1});//1 - Категории товаров
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void LoadData()
		{
			dataSet1 = listProvider.GetList(dataGridISM1.GetDataXml().OuterXml);
			dataGridISM1.Enabled = false;
			dataGridISM1.SetDataBinding(dataSet1, "table");
			dataGridISM1.Enabled = true;
			dataGridISM1.Width++;dataGridISM1.Width--;
		}

		private void fmListThemeForCharacterDialog_Load(object sender, System.EventArgs e)
		{
			LoadData();
			dataGridPager1.BindToDataGrid(dataGridISM1);
		}

		private void miSelect_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			Close();
		}

		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				DataRow dr = null;
//				if ((dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, e)) != null)
				if ((dr = dataGridISM1.GetSelectedRow()) != null)
				{
					SelectedOID = (Guid)dr["OID"];
					SelectedName = (String)dr["ThemeName"];
					contextMenu1.Show(dataGridISM1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_Reload(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void btnChoose_Click(object sender, System.EventArgs e)
		{
			DataRow dr = null;
			//Get selected ROW
			if ((dr = dataSet1.Tables[0].Rows[dataGridISM1.CurrentRowIndex]) != null)
			{
				SelectedOID = (Guid)dr["OID"];
				SelectedName = (String)dr["ThemeName"];
			}

			SelectPressed = true;
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

	}
}
