﻿using System;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using MetaData;
using System.Data;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmAttachObject.
	/// </summary>
	public partial class AttachObject : GenericForm {
		public string[] AllowedClassName;
		public Guid SelectedOID = Guid.Empty;
		public string SelectedNK = "";
		public string SelectedNKRus = "";
		/// <summary>
		/// Required designer variable.
		/// </summary>
		public AttachObject() {
			InitializeComponent();
		}

		private void fmAttachObject_Load(object sender, System.EventArgs e) {
			LoadClasses();
			LoadData();
			dataGridPager1.BindToDataGrid(dataGridISM1);
			if (this.Additionals.ContainsKey("cbClasses")) {
				Guid lastClass = new Guid(this.Additionals["cbClasses"]);
				foreach (ObjectItem o in cbClasses.Items) {
					if (lastClass == o.OID) {
						cbClasses.SelectedItem = o;
						break;
					}
				}
			}

		}

		private void LoadData() {
			ObjectItem item = (ObjectItem)cbClasses.Items[cbClasses.SelectedIndex];
			if(item != null && item.OID != Guid.Empty) {
				BindGrid();
			}
		}
		private void BindGrid() {
			dataSet1 = lp.GetList(dataGridISM1.GetDataXml().OuterXml);
			int cri = dataGridISM1.CurrentRowIndex;
			dataGridISM1.Enabled = false;
			dataGridISM1.SetDataBinding(dataSet1, "table");
			dataGridISM1.Enabled = true;
			dataGridISM1.Width++;dataGridISM1.Width--;
			if (cri >= 0 && dataGridISM1.VisibleRowCount > cri) {
				dataGridISM1.CurrentRowIndex = cri;
				dataGridISM1.Select(cri);
			}
		}

		private void LoadClasses() {
			if(AllowedClassName == null || AllowedClassName.Length != 1)
				cbClasses.Items.Add(new ObjectItem(Guid.Empty, "Выберите класс", ""));
			StringBuilder sql = new StringBuilder();
			if (AllowedClassName == null || AllowedClassName.Length == 0) {
				sql.Append(@"SELECT label, cid, className
FROM t_classname WHERE isCVS = 1
ORDER BY label");
			}
			else {
				sql.Append (@"SELECT label, cid, className
FROM t_classname WHERE className in (");
				foreach (string className in AllowedClassName) {
					sql.Append("'"+className+"', ");
				}
				sql.Length -= 2;
				sql.Append(@")
ORDER BY label");
			}

			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql.ToString()));
			foreach(DataRow dr in ds.Tables["table"].Rows) {
				cbClasses.Items.Add(new ObjectItem((Guid)dr["CID"], (string)dr["label"], (string)dr["className"]));
			}
			cbClasses.SelectedIndex = 0;
		}

		private void cbClasses_SelectedIndexChanged(object sender, System.EventArgs e) {
			if(((ObjectItem)cbClasses.SelectedItem).ClassName != "") {
				dataGridISM1.StockClass = ((ObjectItem)cbClasses.SelectedItem).ClassName;
				dataGridISM1.StockPageNum = 1;
				LoadData();
			}
		}

		#region Working with DataGrid

		private void dataGridISM1_Reload(object sender, System.EventArgs e) {
			BindGrid();
		}

		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e) {
			if (e.Button == MouseButtons.Right) {
				DataRow dr;
				//				if ((dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, e)) != null)
				if ((dr = dataGridISM1.GetSelectedRow()) != null) {
					SelectedOID = (Guid)dr["OID"];
					SelectedNK = (string)dr["OID_NK"];
					contextMenu1.Show(dataGridISM1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_DoubleClick(object sender, System.EventArgs e) {
			System.Drawing.Point pt = dataGridISM1.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dataGridISM1.HitTest(pt); 
			if(hti.Type == DataGrid.HitTestType.Cell) {
				//				DataRow dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, pt.X, pt.Y);
				DataRow dr = dataGridISM1.GetSelectedRow();
				SelectedOID = (Guid)dr["OID"];
				SelectedNK = (string)dr["OID_NK"];
				OKClose();
			}
		}
		#endregion

		private void btnCLose_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private void btnChoose_Click(object sender, System.EventArgs e) {
			DataRow dr = dataGridISM1.GetSelectedRow();
			if(dr != null) {
				SelectedOID = (Guid)dr["OID"];
				SelectedNK = (string)dr["OID_NK"];
				OKClose();
			}
		}

		private void OKClose() {
			SelectedNKRus = this.cbClasses.Text;
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void miChoose_Click(object sender, System.EventArgs e) {
			OKClose();
		}

		private void fmAttachObject_BeforeClosing(object sender, EventArgs e)
		{
			//Сохраним последний выбранный класс
			this.Additionals["cbClasses"] = ((ObjectItem)cbClasses.SelectedItem).OID.ToString();
		}
	}
}
