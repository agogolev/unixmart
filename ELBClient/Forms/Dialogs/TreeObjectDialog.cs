﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class TreeObjectDialog : GenericForm
	{
		public ObjectItem SelectedObject { get; set; }
		public int SelectedNode { get; set; }
		public bool CanAssignAny { get; set; }
		public int RootNode { get; set; }
		private TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
		private Classes.Tree tree;
		public DataSet NodesDataSet { get; set; }
		public TreeObjectDialog()
		{
			InitializeComponent();
			tree = new Tree(0, int.MaxValue);
        }

		private void TreeObjectDialog_Load(object sender, EventArgs e)
		{
			LoadObjects();
			BuildTree();
			LoadTree();
		}
		private void LoadObjects()
		{
			//LoadObject("treeName", null);

			//Filling OID
			string xml;
			if (RootNode == 0)
			{
				throw new Exception("Не задан корневой элемент");
			}
			else
			{
				xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\"" + RootNode + "\" withParent=\"true\"/>";
			}
			NodesDataSet = treeProvider.GetTreeContent(xml);

		}

		//Filling Tree
		private void BuildTree()
		{
			Stack nodes = new Stack();
			nodes.Push(tree);

			int prevNode = 0;
			foreach (DataRow dr in NodesDataSet.Tables["table"].Rows)
			{
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if (nodeID != prevNode)
				{
					node = new Node((int)dr["lft"], (int)dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString().RestoreCr();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"];
					node.objectNK = dr["NK"].ToString().RestoreCr();// == DBNull.Value ? null: (string) dr["NK"];
					node.objectNKRus = dr["label"].ToString().RestoreCr();
					node.className = dr["className"].ToString();
					while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer)nodes.Peek()).Nodes.Add(node);
					if ((node.Left + 1) != node.Right)
					{
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else
				{
					if (node != null)
					{
						node.AddString.Rows.Add(new string[3] { (string)dr["ordValue"], (string)dr["value"], (string)dr["url"] });
						node.AddString.AcceptChanges();
					}
				}
			}
		}

		private void LoadTree()
		{
			objectTree.Nodes.Clear();
			foreach (Node n in tree.Nodes)
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				objectTree.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node)
		{
			foreach (Node n in node.Nodes)
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void objectTree_DoubleClick(object sender, EventArgs e)
		{
			if (!CanAssignAny && objectTree.SelectedNode.Nodes.Count != 0)
			{
				ShowError("Можно назначать только категорию, у которой нет подкатегорий");
				return;
			}
			List<TreeNode> items = new List<TreeNode>();
			TreeNode node = objectTree.SelectedNode;
			while (node.Parent != null)
			{
				items.Add(node);
				node = node.Parent;
			}
			StringBuilder sb = new StringBuilder();
			for (int i = items.Count - 1; i >= 0; i--)
			{
				sb.AppendFormat("{0} / ", items[i].Text);
			}
			if (sb.Length != 0) sb.Length -= 3;
			Node n = (objectTree.SelectedNode.Tag as Node);
			SelectedObject = n.objectOID == Guid.Empty ? ObjectItem.Empty : new ObjectItem(n.objectOID, sb.ToString(), "");
			SelectedNode = n.NodeId;
			DialogResult = DialogResult.OK;
		}
	}
}
