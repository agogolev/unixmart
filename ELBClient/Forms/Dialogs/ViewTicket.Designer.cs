﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ViewTicket
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TextBox tbTicket;
private System.ComponentModel.Container components = null;

		private void InitializeComponent() {
			this.tbTicket = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// tbTicket
			// 
			this.tbTicket.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbTicket.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbTicket.Location = new System.Drawing.Point(0, 0);
			this.tbTicket.Multiline = true;
			this.tbTicket.Name = "tbTicket";
			this.tbTicket.ReadOnly = true;
			this.tbTicket.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.tbTicket.Size = new System.Drawing.Size(504, 192);
			this.tbTicket.TabIndex = 2;
			this.tbTicket.Text = "";
			// 
			// fmViewTicket
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(504, 192);
			this.Controls.Add(this.tbTicket);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(328, 104);
			this.Name = "fmViewTicket";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
