﻿using System;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmLinkedThemes.
	/// </summary>
	public partial class LinkedThemes : ListForm
	{
		private Guid selectedMaster;
		public Guid SelectedMaster {
			get {
				return this.selectedMaster;
			}
			set {
				this.selectedMaster = value;
			}
		}
		
		
		
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public LinkedThemes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmLinkedThemes_Load(object sender, System.EventArgs e) {
			LoadData();
		}
		public override void LoadData() {
			ListProvider.ListProvider prov = Classes.ServiceUtility.ListProvider;
			string sql = @"
SELECT
	t.OID,
	t.themeName
FROM 
	t_ThemeMasters tm
	inner join t_Theme t on tm.OID = t.OID and tm.masterOID = '"+SelectedMaster+@"'
ORDER BY 
	t.themeName
";
			DataSetISM ds = new DataSetISM(prov.GetDataSetSql(sql));
			dgThemes.SetDataBinding(ds, "table");
			dgThemes.DisableNewDel();
		}

		private void dgThemes_DoubleClick(object sender, System.EventArgs e) {
			DataRow dr = dgThemes.GetSelectedRow();
			if(dr != null) {
				Guid themeOID = (Guid)dr["OID"];
				ELBClient.Forms.Guides.EditTheme fm = new ELBClient.Forms.Guides.EditTheme();
				fm.ObjectOID = themeOID;
				fm.MdiParent = this.MdiParent;
				fm.Show();
			}
		}

		private void button1_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void button2_Click(object sender, System.EventArgs e) {
			DataRow dr = dgThemes.GetSelectedRow();
			string sql = "DELETE FROM t_ThemeMasters WHERE OID = '"+dr["OID"]+"' and masterOID = '"+SelectedMaster+@"'";
			ObjectProvider.ObjectProvider prov = Classes.ServiceUtility.ObjectProvider;
			try {
				prov.ExecuteCommand(sql);
				LoadData();
			}
			catch(Exception ex) {
				ShowError(ex.Message);
			}
		}

	}
}
