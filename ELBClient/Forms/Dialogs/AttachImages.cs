﻿using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;
using System.Data;
using System.Xml;
using System.IO;
using ELBClient.Forms.Guides;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListGoodsImages.
	/// </summary>
	public partial class AttachImages : EditForm
	{

		Types _ordValues = new Types();
		CBinaryData _img = new CBinaryData();

		private Hashtable _sizes = new Hashtable();







		/// <summary>
		/// Required designer variable.
		/// </summary>


		public AttachImages(DBObject _object)
		{
			InitializeComponent();
			if (_object.OID == Guid.Empty)
				throw new ApplicationException("Fatal Error: fmAttachImages - OID cannot be Empty");

			ObjectOID = _object.OID;
			if (_object is CArticle)
				Object = new CArticle();
			else if (_object is CPeople)
				Object = new CPeople();
			else if (_object is CTheme)
				Object = new CTheme();
            else if (_object is CGoods)
                Object = new CGoods();
            else if (_object is CCompany)
                Object = new CCompany();
            else throw new ApplicationException("Fatal Error: fmAttachImages - Undefined class");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveObject();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void fmListGoodsImages_Load(object sender, System.EventArgs e)
		{
			loadTypes();

			dataGridPager1.BindToDataGrid(dgImages);

			LoadObject("OID", "Images");
			BindEmpty();
			FillImages();
			ReloadGrid += new ReloadDelegate(LoadData);
		}

		private void BindEmpty()
		{
			if (BindingContext[_img] != null)
			{
				tbName.DataBindings.Clear();
				tbMimeType.DataBindings.Clear();
				tbFile.DataBindings.Clear();
				tbWidth.DataBindings.Clear();
				tbHeight.DataBindings.Clear();
			}
			tbName.DataBindings.Add("Text", _img, "Name");
			tbMimeType.DataBindings.Add("Text", _img, "MimeType");
			tbFile.DataBindings.Add("Text", _img, "FileName");
			tbWidth.DataBindings.Add("BoundProp", _img, "Width");
			tbHeight.DataBindings.Add("BoundProp", _img, "Height");

		}

		private void loadTypes()
		{
			XmlDocument doc = GetResource("fmAttachImages.xml");

			XmlNode cNode = doc.SelectSingleNode("/root/class[@name='" + Object.ClassName + "']");
			if (cNode == null)
				return;
			XmlAttribute attr;
			if (cNode.Attributes.Count > 0 && (attr = cNode.Attributes["allowCustom"]) != null)
			{
				if (attr.Value == "true")
				{
					cbType1.DropDownStyle = ComboBoxStyle.DropDown;
				}
				else if (attr.Value == "false")
				{
					cbType1.DropDownStyle = ComboBoxStyle.DropDownList;
				}
			}
			XmlNodeList nl = cNode.SelectNodes("item");

			_ordValues.Clear();
			DataSet ds = new DataSet();
			DataTable dt = ds.Tables.Add("table");
			dt.Columns.Add("type", typeof(string));
			dt.Columns.Add("width", typeof(int));
			dt.Columns.Add("height", typeof(int));

			foreach (XmlNode node in nl)
			{
				ImageDescription imgDesc = new ImageDescription();
				imgDesc.OrdValue = node.Attributes["name"].Value;
				if (node.Attributes["width"] != null) imgDesc.Width = int.Parse(node.Attributes["width"].Value);
				if (node.Attributes["height"] != null) imgDesc.Height = int.Parse(node.Attributes["height"].Value);
				_ordValues.Add(imgDesc);
				cbType.Items.Add(node.Attributes["name"].Value);
				cbType1.Items.Add(node.Attributes["name"].Value);
				_sizes[imgDesc.OrdValue] = imgDesc;
				dt.Rows.Add(new object[] { imgDesc.OrdValue, imgDesc.Width, imgDesc.Height });
			}
			cbType.SelectedIndex = 0;
		}

		private void LoadData()
		{
			dataSet1 = lp.GetList(dgImages.GetDataXml().OuterXml);
			dataSet1.Tables[0].PrimaryKey = new DataColumn[] { dataSet1.Tables[0].Columns["OID"] };
			dgImages.Enabled = false;
			dgImages.SetDataBinding(dataSet1, "table");
			dgImages.Enabled = true;
			dgImages.Width++; dgImages.Width--;
		}

		private void FillImages()
		{
			dataGrid1.DataSource = ((CObject)Object).Images;
		}

		#region contextmenus functionality
		private void mItemAdd_Click(object sender, System.EventArgs e)
		{
			DataRow dr = dgImages.GetSelectedRow();
			string ordValue = (string)cbType.SelectedItem;
			int width = (dr["width"] != DBNull.Value ? (int)dr["width"] : -1);
			int height = (dr["height"] != DBNull.Value ? (int)dr["height"] : -1);
			Size sz = _ordValues.GetImageSize(ordValue);
			if ((sz.Height != -1 && sz.Height != height) || (sz.Width != -1 && sz.Width != width))
			{
				MessageBox.Show(this, "Картинка не соответсвует размеру (ширина: " + sz.Width + ", высота: " + sz.Height + ") для выбранного типа картинки", "Внимание!");
				return;
			}
			Guid g = (Guid)dr["OID"];
			string name = (string)dr["name"];
			try
			{
				((CObject)Object).Images.Rows.Add(new object[] { g, name, "CBinaryData", cbType.Text });
			}
			catch
			{
				this.ShowWarning("Изображение с таким типом уже существует!");
				return;
			}
			SaveObject();
		}

		private void mItemDel_Click(object sender, System.EventArgs e)
		{
			DataRow dr = dataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
			SaveObject();
		}

		private void mItemEdit_Click(object sender, System.EventArgs e)
		{
			DataRow dr = dgImages.GetSelectedRow();
			Guid imageOID = (Guid)dr["OID"];
			EditForm form = new EditBinaryData();
			form.ObjectOID = imageOID;
			form.ReloadGrid += new ReloadDelegate(LoadData);
			form.ShowDialog(this);
		}

		private void mItemDelFromDB_Click(object sender, System.EventArgs e)
		{
			DataRow dr = dgImages.GetSelectedRow();
			Guid OID = (Guid)dr["OID"];
			if (MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				ELBClient.ObjectProvider.ExceptionISM ex = null;
				ex = op.DeleteObject(OID);
				if (ex != null)
				{
					if (ex.LiteralExceptionType == "System.Data.SqlClient.SqlException")
						MessageBox.Show(this, "Невозможно удалить объект!\nВозможно он связан с другим объектом.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					else if (ex.LiteralExceptionType == "MetaData.SystemOIDException")
						MessageBox.Show(this, "Невозможно удалить объект!\nДанный объект используется системой.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					else MessageBox.Show(this, ex.LiteralMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					//else MessageBox.Shows(this, e.LiteralExceptionType+": "+e.LiteralMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else LoadData();
			}
		}
		#endregion contextmenus functionality

		#region dataGrid1 functionality
		private void dataGrid1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			int CRI = dataGrid1.CurrentRowIndex;
			if (CRI >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					dataGrid1.Select(CRI);
				}
				else if (e.Button == MouseButtons.Right)
				{
					dataGrid1.Select(CRI);
					contextMenuAttImgs.Show(dataGrid1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGrid1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGrid1.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					dataGrid1.UnSelect(dataGrid1.CurrentRowIndex);
					dataGrid1.CurrentRowIndex = hti.Row;
					dataGrid1.Select(dataGrid1.CurrentRowIndex);
				}
			}
		}

		#endregion dataGrid1 functionality

		#region dgImages functionality
		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			int CRI = dgImages.CurrentRowIndex;
			if (CRI >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					dgImages.Select(CRI);
				}
				else if (e.Button == MouseButtons.Right)
				{
					dgImages.Select(CRI);
					contextMenuImages.Show(dgImages, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_Reload(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void dataGridISM1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dgImages.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					dgImages.UnSelect(dgImages.CurrentRowIndex);
					dgImages.CurrentRowIndex = hti.Row;
					dgImages.Select(dgImages.CurrentRowIndex);
				}
			}
		}
		#endregion dgImages functionality

		private void button1_Click(object sender, System.EventArgs e)
		{
			if (ofdFile.ShowDialog(this) == DialogResult.OK)
			{
				tbFile.Text = ofdFile.FileName;
				FileStream fs = new FileStream(tbFile.Text, FileMode.Open, FileAccess.Read, FileShare.Read);
				byte[] content = new byte[fs.Length];
				fs.Read(content, 0, (int)fs.Length);
				fs.Close();
				_img.Data = content;
				tbMimeType.Text = MetaData.MimeTypeUtil.CheckType(tbFile.Text);
				tbName.Text = Path.GetFileName(tbFile.Text);
				FillSize(tbFile.Text, tbMimeType.Text);
				BindingContext[_img].EndCurrentEdit();
			}
		}
		private void FillSize(string fileName, string mimeType)
		{
			Size size = MetaData.Sizer.GetSize(fileName, mimeType);
			if (size.Width != -1) tbWidth.Text = size.Width.ToString();
			if (size.Height != -1) tbHeight.Text = size.Height.ToString();
			string ordValue = _ordValues.GetCandidate(size.Width, size.Height);
			if (ordValue != null)
			{
				ImageDescription imgDescr = null;
				if (cbType1.SelectedItem != null) imgDescr = _sizes[cbType1.SelectedItem] as ImageDescription;
				if (imgDescr != null)
				{
					int prevW = imgDescr.Width;
					int prevH = imgDescr.Height;
					if (size.Width == prevW && size.Height == prevH) return;
				}
				string prevSelected = (string)cbType1.SelectedItem;
				foreach (string li in cbType1.Items)
				{
					if (ordValue == li)
					{
						cbType1.SelectedItem = li;
						break;
					}
				}
				if (imgDescr != null)
				{
					int prevW = imgDescr.Width;
					int prevH = imgDescr.Height;
					if ((prevW != -1 && size.Width != prevW) || (prevH != -1 && size.Height != prevH))
					{
						MessageBox.Show(this, "Вы хотели загрузить картинку с типом \"" + prevSelected + "\", но размеры данной картинки не сответствуют желаемым размерам");
					}
				}
			}
			else
			{
				MessageBox.Show(this, "Картинка не соответсвует разрешённым размерам для редактируемого объекта", "Внимание!");
			}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (((CObject)Object).Images.Rows.Find(cbType1.Text) != null)
				{
					this.ShowWarning("Изображение с таким типом уже существует!");
					return;
				}
				string ordValue = (string)cbType1.SelectedItem;
				Size sz = _ordValues.GetImageSize(ordValue);
				if (sz.Width == -2 && sz.Height == -2)
				{
					MessageBox.Show(this, "Картинка не соответсвует разрешённым размерам для редактируемого объекта", "Внимание!");
					return;
				}
				else if ((sz.Height != -1 && sz.Height != (_img as CBinaryData).Height) || (sz.Width != -1 && sz.Width != (_img as CBinaryData).Width))
				{
					MessageBox.Show(this, "Картинка не соответсвует размеру (ширина: " + sz.Width + ", высота: " + sz.Height + ") для выбранного типа картинки", "Внимание!");
					return;
				}
				if (_img.Data != null && _img.Data.Length != 0)
				{
					if (_img.OID == Guid.Empty)
					{
						string fileName = tbFile.Text;
						if (fileName.IndexOf(@"\") != -1 || fileName.IndexOf("/") != -1)
						{
							tbFile.Text = Path.GetFileName(fileName);
						}
						BindingContext[_img].EndCurrentEdit();
						ELBClient.ObjectProvider.ExceptionISM exISM = null;
						string newobj = op.SaveObject(_img.SaveXml(), out exISM);
						_img.LoadXml(newobj);
						if (exISM != null)
						{
							if (exISM.LiteralExceptionType == "BackendService.Classes.DBException")
							{
								this.ShowError(this, "Ошибка сохранения: ошибка целостности");
								throw new Exception(exISM.LiteralMessage);
							}
							else
							{
								this.ShowError(this, "Ошибка сохранения: " + newobj);
								throw new Exception(exISM.LiteralMessage);
							}
						}
						if (ReloadGrid != null)
							ReloadGrid();
					}
					string name = tbName.Text;
					try
					{
						((CObject)Object).Images.Rows.Add(new object[] { _img.OID, name, "CBinaryData", cbType1.Text });
					}
					catch
					{
						this.ShowWarning("Изображение с таким типом уже существует!");
						return;
					}
					SaveObject();
					_img = new CBinaryData();
					BindEmpty();
				}
			}
			catch (Exception exc)
			{
				//может возникнуть из-за разрыва соединения
				this.ShowError("Ошибка сохранения: " + exc.Message);
			}

		}

		private void cbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ImageDescription imgDescr = (_sizes[cbType.SelectedItem] as ImageDescription);
			if (imgDescr != null)
			{
				int width = imgDescr.Width;
				int height = imgDescr.Height;
				if (width != -1 && height != -1)
				{
					dgImages.CreateAuxFilter("width", "width_filter", FilterVerb.Equal, false, new object[] { width });
					dgImages.CreateAuxFilter("height", "height_filter", FilterVerb.Equal, false, new object[] { height });
				}
				else {
					dgImages.StorableFilters.Remove("width_filter");
					dgImages.StorableFilters.Remove("height_filter");
				}

			}
			else
			{
			}
			LoadData();
		}
	}

	public class ImageDescription
	{
		private int _width = -1, _height = -1;
		private string _ordValue = "";
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}
		public int Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}
		public string OrdValue
		{
			get
			{
				return _ordValue;
			}
			set
			{
				_ordValue = value;
			}
		}
	}
	public class Types : ArrayList
	{
		public string GetCandidate(int width, int height)
		{
			Sort(new ComparerClass());
			ArrayList tempArr = new ArrayList();
			foreach (ImageDescription img in this)
			{
				if ((img.Width == width && img.Height == height)
					|| (img.Width == width && img.Height == -1)
					|| (img.Width == -1 && img.Height == height)
					|| (img.Width == -1 && img.Height == -1)) return img.OrdValue;
			}
			return null;
		}
		public Size GetImageSize(string ordValue)
		{
			if (ordValue == null) return new Size(-1, -1);
			foreach (ImageDescription img in this)
			{
				if (ordValue == img.OrdValue) return new Size(img.Width, img.Height);
			}
			return new Size(-2, -2);
		}
	}
	public class ComparerClass : IComparer
	{
		int IComparer.Compare(Object x, Object y)
		{
			if ((x as ImageDescription).Width.CompareTo((y as ImageDescription).Width) == 0)
			{
				if ((x as ImageDescription).Height.CompareTo((y as ImageDescription).Height) == 0)
					return (x as ImageDescription).OrdValue.CompareTo((y as ImageDescription).OrdValue);
				else return (x as ImageDescription).Height.CompareTo((y as ImageDescription).Height);
			}
			else return (x as ImageDescription).Width.CompareTo((y as ImageDescription).Width);
		}
	}
}
