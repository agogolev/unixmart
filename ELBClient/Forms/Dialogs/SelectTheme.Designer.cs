using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class SelectTheme
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
private System.Windows.Forms.Button btnSelect;
private System.Windows.Forms.CheckedListBox lbItems;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.lbItems = new System.Windows.Forms.CheckedListBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSelect = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lbItems
			// 
			this.lbItems.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbItems.Location = new System.Drawing.Point(0, 0);
			this.lbItems.Name = "lbItems";
			this.lbItems.Size = new System.Drawing.Size(328, 224);
			this.lbItems.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSelect);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 224);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(328, 40);
			this.panel1.TabIndex = 1;
			// 
			// btnSelect
			// 
			this.btnSelect.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSelect.Location = new System.Drawing.Point(8, 8);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(88, 23);
			this.btnSelect.TabIndex = 0;
			this.btnSelect.Text = "Продолжить";
			// 
			// fmSelectTheme
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(328, 264);
			this.Controls.Add(this.lbItems);
			this.Controls.Add(this.panel1);
			this.Name = "fmSelectTheme";
			this.Text = "Выбор категорий каталога";
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
