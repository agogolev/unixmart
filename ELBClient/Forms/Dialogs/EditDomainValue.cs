﻿using System;
using ELBClient.Classes;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmEditDomainValue.
	/// </summary>
	public partial class EditDomainValue : EditForm
	{
		public MetaData.ObjectItem ParamType {
			get{
				return ((CStringItem)Object).ParamType;
			}
			set {
				((CStringItem)Object).ParamType = value;
			}
		}
		
		
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public EditDomainValue()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			Object = new CStringItem();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmEditDomainValue_Load(object sender, System.EventArgs e) {
			LoadData();
			BindFields();
		}
		private void LoadData() {
			if(ObjectOID != Guid.Empty) {
				LoadObject("value,paramType", null);
			}
		}
		private void BindFields() {
			txtValue.DataBindings.Add("Text", Object, "Value");
		}

		private void button2_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void button3_Click(object sender, System.EventArgs e) {
			SaveObject(true);
		}
	}
}
