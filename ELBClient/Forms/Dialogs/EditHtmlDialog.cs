﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using ELBClient.Classes;

namespace ELBClient.Forms.Dialogs
{
    /// <summary>
    ///     Summary description for fmEditArticleDialog.
    /// </summary>
    public class EditHtmlDialog : EditForm
    {
        public int MaxChars { get; private set; }
        public string ClassName { get; private set; }
        public string FieldName { get; private set; }
        private Panel BrowserPanel;
    
        private string Address
        {
            get
            {
                return string.Format("{0}EditDescription.aspx?OID={1}&FieldString={4}&ClassName={3}&ts={2:yyyyMMddhhmmss}", ServiceUtility.GetBrowseDir(), ObjectOID, DateTime.Now, ClassName, FieldName);
            }
        }
        public IWebBrowser Browser { get; private set; }

        /// <summary>
        ///     Required designer variable.
        /// </summary>
        private readonly Container components = null;

        public EditHtmlDialog(Guid oid, int maxChars, string className = "CArticle", string fieldName = "body")
        {
            //ArticleType = 0;
            MaxChars = maxChars;
            ClassName = className;
            FieldName = fieldName;
            InitializeComponent();
            ObjectOID = oid;
            var browser = new ChromiumWebBrowser(Address) { Dock = DockStyle.Fill };
            BrowserPanel.Controls.Add(browser);
            Browser = browser;
            browser.FrameLoadEnd += browser_FrameLoadEnd;
            Closing += OnClosing;
            Disposed += FormDisposed;
        }

        void browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            Browser.ExecuteScriptAsync($"maxLength = {MaxChars};");
        }

        private void FormDisposed(object sender, EventArgs e)
        {
            Disposed -= FormDisposed;

            Browser.Dispose();
        }

        //public int ArticleType { get; set; }

        public string Header
        {
            set
            {
                Text = @"Содержание статьи - ";
                Text += !string.IsNullOrEmpty(value) ? value : "пусто";
            }
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            var result = false;
            var task = Browser.EvaluateScriptAsync("textChanged");
            task.ContinueWith(t =>
            {
                if (t.Result.Success)
                {
                    result = (bool) t.Result.Result;
                }
                if (result)
                {
                    if (MessageBox.Show(this, @"Данные изменены, Вы действительно хотите закончить редактирование статьи?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        cancelEventArgs.Cancel = true;
                        return;
                    }
                }
                Closing -= OnClosing;
                Close();
            },
                TaskScheduler.FromCurrentSynchronizationContext());
            cancelEventArgs.Cancel = true;
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrowserPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // BrowserPanel
            // 
            this.BrowserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowserPanel.Location = new System.Drawing.Point(0, 0);
            this.BrowserPanel.Name = "BrowserPanel";
            this.BrowserPanel.Size = new System.Drawing.Size(642, 423);
            this.BrowserPanel.TabIndex = 0;
            // 
            // fmEditArticleDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(642, 423);
            this.Controls.Add(this.BrowserPanel);
            this.MinimumSize = new System.Drawing.Size(350, 150);
            this.Name = "fmEditArticleDialog";
            this.Text = "Содержание статьи - ";
            this.ResumeLayout(false);

        }

        #endregion
    }
}