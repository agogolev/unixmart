﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ColumnMenuExtender.Forms.Dialogs;
using MetaData;

namespace ELBClient.Forms.Dialogs {
	/// <summary>
	/// Summary description for fmListDialog.
	/// </summary>
	public partial class ListDialog : BaseListDialog {
		private ListProvider.ListProvider listProvider = ELBClient.Classes.ServiceUtility.ListProvider;
		
		public ListDialog(string className, string[] columnNames, string[] columnHeaderNames)
			: base (className, columnNames, columnHeaderNames) {
		 }

		public ListDialog(string className, string[] columnNames, string[] columnHeaderNames, OrderInfo Order, 
			Hashtable Filters) 
			: base (className, columnNames, columnHeaderNames, Order, Filters) {
		 }
		
		public ListDialog(string className, string[] columnNames, string[] columnHeaderNames, OrderInfo Order, 
			Hashtable Filters, int[] widths) 
			: base (className, columnNames, columnHeaderNames, Order, Filters, widths) {
		 }

		public ListDialog(string className, string[] columnNames, string[] columnHeaderNames, string[] formatRows, 
			OrderInfo Order, Hashtable Filters, int[] widths) 
			: base (className, columnNames, columnHeaderNames, formatRows, Order, Filters, widths) {
		 }

		public ListDialog(string className, IList<string> fieldNames, IList<string> columnNames, IList<string> columnHeaderNames,
		    IList<string> formatRows, OrderInfo Order, Hashtable Filters, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, formatRows, Order, Filters, widths) {
		 }

		public ListDialog(string className, IList<string> fieldNames, IList<string> columnNames, IList<string> columnHeaderNames, 
			OrderInfo Order, Hashtable Filters, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, Order, Filters, widths) {
		 }

		public ListDialog(string className, string[] fieldNames, string[] columnNames, string[] columnHeaderNames, 
			Hashtable DataGridColumnStyleArray, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, DataGridColumnStyleArray, widths) {
		 }

		public ListDialog(string className, string[] fieldNames, string[] columnNames, string[] columnHeaderNames,  
			string[] formatRows, Hashtable DataGridColumnStyleArray, int[] widths)
			: base (className, fieldNames, columnNames, columnHeaderNames, formatRows, DataGridColumnStyleArray, widths) {
		 }

		protected override DataSet GetDataSet() {
			return listProvider.GetList(dataGridISM1.GetDataXml().OuterXml);
		}

		

		//protected override string GetFormName(string ClassName) {
		//  return (string)GenericForm.ClassRUName[ClassName];
		//}

		protected override string FileName {
			get {
				return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
					@"\Invento\ELBClient\forms.xml";
			}
		}

		protected override string HintTextBoxFileName {	
			get {	
				return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + 
					@"\Invento\ELBClient\HintTextBox.xml";
			}
		}
	}
}
