﻿namespace ELBClient.Forms.Dialogs
{
	partial class TreeObjectDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.objectTree = new System.Windows.Forms.TreeView();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 444);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(500, 40);
			this.panel1.TabIndex = 4;
			// 
			// objectTree
			// 
			this.objectTree.AllowDrop = true;
			this.objectTree.Dock = System.Windows.Forms.DockStyle.Fill;
			this.objectTree.HideSelection = false;
			this.objectTree.Location = new System.Drawing.Point(0, 0);
			this.objectTree.Name = "objectTree";
			this.objectTree.Size = new System.Drawing.Size(500, 444);
			this.objectTree.TabIndex = 5;
			this.objectTree.DoubleClick += new System.EventHandler(this.objectTree_DoubleClick);
			// 
			// TreeObjectDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(500, 484);
			this.Controls.Add(this.objectTree);
			this.Controls.Add(this.panel1);
			this.Name = "TreeObjectDialog";
			this.Text = "Иерархия объектов";
			this.Load += new System.EventHandler(this.TreeObjectDialog_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TreeView objectTree;
	}
}