﻿using System;
using System.Linq;
using System.Data;
using ELBClient.ListProvider;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for ListManufThemesDialog.
	/// </summary>
	public partial class ListManufArticlesDialog : ListForm
	{
		public DataRow SelectedArticle
		{
			get { return ArticlesGrid.GetSelectedRow(); }
		}

		public DataRow SelectedCompany
		{
			get
			{
				return (CompanyGrid.DataSource as DataSet).Tables[0].AsEnumerable().FirstOrDefault(row => row.Field<bool>("selected"));
			}
		}

		public ListManufArticlesDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		private void fmListThemesDialog_Load(object sender, System.EventArgs e)
		{
			LoadData();
		}
		public override void LoadData()
		{
			LoadCompanies();
			LoadArticles();
		}

		private void LoadCompanies ()
		{
			var sql = @"
SELECT c.OID, c.companyName FROM t_Company c inner join t_Object o on c.OID = o.OID";
			if (CompanyNameText.Text.Trim() != "") sql += " WHERE companyName like @companyName ";
			else sql += " WHERE c.OID = '00000000-0000-0000-0000-000000000000'";
			sql += " ORDER BY o.dateCreate desc";
			var ds =
				new DataSetISM(lp.GetDataSetSqlParams(sql,
				                                      new[]
				                                      	{
				                                      		new NameValuePair
				                                      			{
				                                      				Name = "companyName",
				                                      				Value = string.Format("%{0}%", CompanyNameText.Text.Trim())
				                                      			}
				                                      	}));
			var dc = new DataColumn("selected", typeof (bool)) {AllowDBNull = false, DefaultValue = false};
			ds.Tables[0].Columns.Add(dc);
			CompanyGrid.SetDataBinding(ds, "table");
			CompanyGrid.DisableNewDel();
		}

		private void LoadArticles()
		{
			var sql = @"
SELECT TOP 25 a.OID, a.header FROM t_Article a inner join t_Object o on a.OID = o.OID";
			if (ArticleHeaderText.Text.Trim() != "") sql += " WHERE a.header like @header ";
			sql += " ORDER BY o.dateCreate desc";
			var ds =
				new DataSetISM(lp.GetDataSetSqlParams(sql,
				                                      new[]
				                                      	{
				                                      		new NameValuePair
				                                      			{
				                                      				Name = "header",
				                                      				Value = string.Format("%{0}%", ArticleHeaderText.Text.Trim())
				                                      			}
				                                      	}));
			ArticlesGrid.SetDataBinding(ds, "table");
			ArticlesGrid.DisableNewDel();
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			LoadCompanies();
		}

		private void SearchButton_Click(object sender, EventArgs e)
		{
			LoadArticles();
		}
	}
}
