using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class HtmlEditDialog
	{
		#region Windows Form Designer generated code
		private WebBrowser webBrowser1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.webBrowser1 = new System.Windows.Forms.WebBrowser();
			this.SuspendLayout();
			// 
			// webBrowser1
			// 
			this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.webBrowser1.Location = new System.Drawing.Point(0, 0);
			this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new System.Drawing.Size(642, 423);
			this.webBrowser1.TabIndex = 0;
			// 
			// fmHtmlEditDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(642, 423);
			this.Controls.Add(this.webBrowser1);
			this.MinimumSize = new System.Drawing.Size(350, 150);
			this.Name = "fmHtmlEditDialog";
			this.Text = "Содержание";
			this.Activated += new System.EventHandler(this.fmHtmlEditDialog_Activated);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
