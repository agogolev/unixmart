﻿namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmWaiting.
	/// </summary>
	public partial class Waiting : GenericForm
	{
		public int MaxStep
		{
			get
			{
				return ProgressBar.Maximum;
			}
			set
			{
				ProgressBar.Maximum = value;
			}
		}
		public Waiting()
		{
			InitializeComponent();
		}

		public void NextStep()
		{
			ProgressBar.Increment(1);
		}
	}
}
