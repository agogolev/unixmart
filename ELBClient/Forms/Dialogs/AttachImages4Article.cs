﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using ELBClient.Classes;
using System.Data;
using System.IO;
using ELBClient.Forms.Guides;
using System.Text.RegularExpressions;
using ELBClient.ObjectProvider;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListGoodsImages.
	/// </summary>
	public partial class AttachImages4Article : EditForm
	{
		CBinaryData _img = new CBinaryData();
		/// <summary>
		/// Required designer variable.
		/// </summary>
		public AttachImages4Article(DBObject _object)
		{
			InitializeComponent();
			if (_object.OID == Guid.Empty)
				throw new ApplicationException("Fatal Error: fmAttachImages - OID cannot be Empty");

			ObjectOID = _object.OID;
			if (_object is CArticle)
				Object = new CArticle();
			else throw new ApplicationException("Fatal Error: fmAttachImages4Article - Undefined class");
		}


		private void btnSave_Click(object sender, EventArgs e)
		{
			SaveObject();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void fmListGoodsImages_Load(object sender, EventArgs e)
		{
			dataGridPager1.BindToDataGrid(dgImages);
			LoadData();

			LoadObject("OID", "Images");
			BindEmpty();
			FillImages();
			ReloadGrid += LoadData;
		}

		private void BindEmpty()
		{
			tbName.DataBindings.Clear();
			tbMimeType.DataBindings.Clear();
			tbFile.DataBindings.Clear();
			tbWidth.DataBindings.Clear();
			tbHeight.DataBindings.Clear();
			tbName.DataBindings.Add("Text", _img, "Name");
			tbMimeType.DataBindings.Add("Text", _img, "MimeType");
			tbFile.DataBindings.Add("Text", _img, "FileName");
			tbWidth.DataBindings.Add("BoundProp", _img, "Width");
			tbHeight.DataBindings.Add("BoundProp", _img, "Height");

		}

		private void LoadData()
		{
			dataSet1 = lp.GetList(dgImages.GetDataXml().OuterXml);
			dataSet1.Tables[0].PrimaryKey = new[] { dataSet1.Tables[0].Columns["OID"] };
			dgImages.Enabled = false;
			dgImages.SetDataBinding(dataSet1, "table");
			dgImages.Enabled = true;
			dgImages.Width++; dgImages.Width--;
		}

		private void FillImages()
		{
			dataGrid1.DataSource = ((CObject)Object).Images;
		}

		#region contextmenus functionality
		private void mItemAdd_Click(object sender, EventArgs e)
		{
			var dr = dgImages.GetSelectedRow();

			var g = (Guid)dr["OID"];
			var name = (string)dr["name"];
			var ordValue = DefineType();
			try
			{
				var row = ((CObject)Object).Images.Rows.Find(ordValue);
				if (row == null)
					((CObject)Object).Images.Rows.Add(new object[] { g, name, "CBinaryData", ordValue });
				else
				{
					row["propValue"] = g;
					row["propValue_NK"] = name;
				}
			}
			catch
			{
				ShowWarning("Изображение с таким типом уже существует!");
				return;
			}
			SaveObject();
		}

		private string DefineType()
		{
			if (!ForListCheckBox.Checked)
			{
				var re = new Regex(@"\d+$");
				var counter = (from row in ((CObject)Object).Images.AsEnumerable()
							   where row.Field<string>("ordValue").StartsWith("image") && re.IsMatch(row.Field<string>("ordValue"))
							   select int.Parse(re.Match(row.Field<string>("ordValue")).Value)).OrderByDescending(i => i).DefaultIfEmpty(1).First();
				counter++;
				return string.Format("{0} {1}", "image", counter);
			}
			return "headerImage";
		}

		private void mItemDel_Click(object sender, EventArgs e)
		{
			DataRow dr = dataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
			SaveObject();
		}

		private void mItemEdit_Click(object sender, EventArgs e)
		{
			var dr = dgImages.GetSelectedRow();
			var imageOID = (Guid)dr["OID"];
			var form = new EditBinaryData {ObjectOID = imageOID};
			form.ReloadGrid += LoadData;
			form.ShowDialog(this);
		}

		private void mItemDelFromDB_Click(object sender, EventArgs e)
		{
			var dr = dgImages.GetSelectedRow();
			var OID = (Guid)dr["OID"];
			if (
				MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание!", MessageBoxButtons.YesNo,
				                MessageBoxIcon.Warning) != DialogResult.Yes) return;
			var ex = op.DeleteObject(OID);
			if (ex != null)
			{
				switch (ex.LiteralExceptionType)
				{
					case "System.Data.SqlClient.SqlException":
						MessageBox.Show(this, "Невозможно удалить объект!\nВозможно он связан с другим объектом.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
						break;
					case "MetaData.SystemOIDException":
						MessageBox.Show(this, "Невозможно удалить объект!\nДанный объект используется системой.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
						break;
					default:
						MessageBox.Show(this, ex.LiteralMessage, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
						break;
				}
			}
			else LoadData();
		}
		#endregion contextmenus functionality

		#region dataGrid1 functionality
		private void dataGrid1_MouseUp(object sender, MouseEventArgs e)
		{
			int CRI = dataGrid1.CurrentRowIndex;
			if (CRI >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					dataGrid1.Select(CRI);
				}
				else if (e.Button == MouseButtons.Right)
				{
					dataGrid1.Select(CRI);
					contextMenuAttImgs.Show(dataGrid1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGrid1_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dataGrid1.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					dataGrid1.UnSelect(dataGrid1.CurrentRowIndex);
					dataGrid1.CurrentRowIndex = hti.Row;
					dataGrid1.Select(dataGrid1.CurrentRowIndex);
				}
			}
		}

		#endregion dataGrid1 functionality

		#region dgImages functionality
		private void dataGridISM1_MouseUp(object sender, MouseEventArgs e)
		{
			int CRI = dgImages.CurrentRowIndex;
			if (CRI >= 0)
			{
				if (e.Button == MouseButtons.Left)
				{
					dgImages.Select(CRI);
				}
				else if (e.Button == MouseButtons.Right)
				{
					dgImages.Select(CRI);
					contextMenuImages.Show(dgImages, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_Reload(object sender, EventArgs e)
		{
			LoadData();
		}

		private void dataGridISM1_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dgImages.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell || hti.Type == DataGrid.HitTestType.RowHeader)
				{
					dgImages.UnSelect(dgImages.CurrentRowIndex);
					dgImages.CurrentRowIndex = hti.Row;
					dgImages.Select(dgImages.CurrentRowIndex);
				}
			}
		}
		#endregion dgImages functionality

		private void button1_Click(object sender, EventArgs e)
		{
			if (ofdFile.ShowDialog(this) == DialogResult.OK)
			{
				tbFile.Text = ofdFile.FileName;
				var fs = new FileStream(tbFile.Text, FileMode.Open, FileAccess.Read, FileShare.Read);
				var content = new byte[fs.Length];
				fs.Read(content, 0, (int)fs.Length);
				fs.Close();
				_img.Data = content;
				tbMimeType.Text = MetaData.MimeTypeUtil.CheckType(tbFile.Text);
				tbName.Text = Path.GetFileName(tbFile.Text);
				FillSize(tbFile.Text, tbMimeType.Text);
				BindingContext[_img].EndCurrentEdit();
			}
		}
		private void FillSize(string fileName, string mimeType)
		{
			Size size = MetaData.Sizer.GetSize(fileName, mimeType);
			if (size.Width != -1) tbWidth.Text = size.Width.ToString();
			if (size.Height != -1) tbHeight.Text = size.Height.ToString();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			try
			{
				if (_img.Data != null && _img.Data.Length != 0)
				{
					if (_img.OID == Guid.Empty)
					{
						string fileName = tbFile.Text;
						if (fileName.IndexOf(@"\") != -1 || fileName.IndexOf("/") != -1)
						{
							tbFile.Text = Path.GetFileName(fileName);
						}
						BindingContext[_img].EndCurrentEdit();
						ExceptionISM exISM;
						var newobj = op.SaveObject(_img.SaveXml(), out exISM);
						_img.LoadXml(newobj);
						if (exISM != null)
						{
							if (exISM.LiteralExceptionType == "BackendService.Classes.DBException")
							{
								ShowError(this, "Ошибка сохранения: ошибка целостности");
								throw new Exception(exISM.LiteralMessage);
							}
							ShowError(this, "Ошибка сохранения: " + newobj);
							throw new Exception(exISM.LiteralMessage);
						}
						if (ReloadGrid != null)
							ReloadGrid();
					}
					var name = tbName.Text;
					var ordValue = DefineType();
					try
					{
						var dr = ((CObject) Object).Images.Rows.Find(ordValue);
						if (dr == null)
							((CObject)Object).Images.Rows.Add(new object[] { _img.OID, name, "CBinaryData", ordValue });
						else
						{
							dr["propValue"] = _img.OID;
							dr["propValue_NK"] = name;
						}
					}
					catch
					{
						ShowWarning("Изображение с таким типом уже существует!");
						return;
					}
					SaveObject();
					_img = new CBinaryData();
					BindEmpty();
				}
			}
			catch (Exception exc)
			{
				//может возникнуть из-за разрыва соединения
				ShowError("Ошибка сохранения: " + exc.Message);
			}

		}
	}
}
