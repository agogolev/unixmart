﻿using System;
using System.Windows.Forms;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmBrowse.
	/// </summary>
	public partial class Browse : GenericForm
	{
		public bool BrowseBanners { get; set; }
		//private AxSHDocVw.AxWebBrowser wb;
		private Guid _oid;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public Browse(Guid oid)
		{
			InitializeComponent();
			_oid = oid;
		}

		private void fmBrowse_Activated(object sender, System.EventArgs e)
		{
			if (!this.Disposing)
			{
				if (_oid != Guid.Empty)
				{
					string adr = (BrowseBanners ? Classes.ServiceUtility.GetBannersBrowseDir() : Classes.ServiceUtility.GetBrowseDir()) + "BrowseImage.aspx?OID=" + _oid;
					webBrowser1.Navigate(adr);
				}
				else MessageBox.Show(this, "Ошибка: OID картинки не может быть равен значению Guid.Empty!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
	}
}
