﻿using System;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using MetaData;
using ELBClient.Classes;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListThemesDialog.
	/// </summary>
	public partial class ListThemesDialog : ListForm
	{
		public int ShopType { get; set; }
		private DataSetISM ShopTypeDescription;
		protected ListProvider.ListProvider ListProv = ServiceUtility.ListProvider;
		private readonly Tree tree = new Tree(0, int.MaxValue);
		public Guid SelectedOID
		{
			get
			{
				var node = tv1.SelectedNode;
				return node == null ? Guid.Empty : ((Node)node.Tag).objectOID;
			}
		}
		public string SelectedName
		{
			get
			{
				var node = tv1.SelectedNode;
				return node.FullName();
			}
		}
		public bool IsLeaf
		{
			get
			{
				var node = tv1.SelectedNode;
				return node != null && ((Node)node.Tag).Nodes.Count == 0;
			}
		}




		/// <summary>
		/// Required designer variable.
		/// </summary>


		public ListThemesDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion


		private void fmListThemesDialog_Load(object sender, EventArgs e)
		{
			ShopTypeDescription = new DataSetISM(lp.GetDataSetSql("SELECT * FROM t_TypeShop"));
			LoadData();
		}
		public override void LoadData()
		{
			var catalogNode = ShopTypeDescription.Table.AsEnumerable().Where(row => row.Field<int>("shopType") == ShopType).Select(row => row.Field<int>("themeRootNode")).Single();
			BuildTree(catalogNode);
			LoadTree();
		}
		private void BuildTree(int rootNode)
		{
			var treeProv = ServiceUtility.TreeProvider;
			var xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\"" + rootNode + "\" />";
			var dataSet1 = treeProv.GetTreeContent(xml);
			var nodes = new Stack();
			nodes.Push(tree);

			var prevNode = 0;
			foreach (DataRow dr in dataSet1.Tables["table"].Rows)
			{
				var nodeID = (int)dr["nodeID"];
				if (nodeID == prevNode) continue;
				var node = new Node((int) dr["lft"], (int) dr["rgt"])
				           	{
				           		NodeId = nodeID,
				           		Name = dr["nodeName"].ToString(),
				           		objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid) dr["OID"],
				           		objectNK = dr["NK"].ToString(),
				           		objectNKRus = dr["label"].ToString(),
				           		className = dr["className"].ToString()
				           	};
				// == DBNull.Value ? null: (string) dr["NK"];
				while (nodes.Peek() != null && ((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
				((NodeContainer)nodes.Peek()).Nodes.Add(node);
				if ((node.Left + 1) != node.Right)
				{
					nodes.Push(node);
				}
				prevNode = nodeID;
				//else
				//{
				//    if (node != null)
				//    {
				//        node.AddString.Rows.Add(new string[3] { (string)dr["ordValue"], (string)dr["value"], (string)dr["url"] });
				//        node.AddString.AcceptChanges();
				//    }
				//}
			}
		}

		private void LoadTree()
		{
			tv1.Nodes.Clear();
			foreach (Node n in tree.Nodes)
			{
				var tn =
					new TreeNode(n.Name != ""
					             	? n.Name.Replace("\r\n", " ")
					             	: (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>"))) {Tag = n};
				//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tv1.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node)
		{
			foreach (Node n in node.Nodes)
			{
				var tn =
					new TreeNode(n.Name != ""
					             	? n.Name.Replace("\r\n", " ")
					             	: (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>"))) {Tag = n};
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void fmListThemesDialog_DoubleClick(object sender, EventArgs e)
		{
			if (SelectedOID != Guid.Empty) DialogResult = DialogResult.OK;
		}
	}
}
