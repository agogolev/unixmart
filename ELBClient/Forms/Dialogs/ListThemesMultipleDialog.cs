﻿using System;
using System.Linq;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using MetaData;
using ELBClient.Classes;
using System.Collections.Generic;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListThemesDialog.
	/// </summary>
	public partial class ListThemesMultipleDialog : ListForm
	{
		public int ShopType { get; set; }
		private DataSetISM ShopTypeDescription;
		private readonly Tree tree = new Tree(0, int.MaxValue);
		public IList<TreeNode> SelectedNodes
		{
			get
			{
				return tv1.AllNodes().Where(node => node.Checked).ToList();
			}
		}

		public ListThemesMultipleDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void fmListThemesDialog_Load(object sender, System.EventArgs e)
		{
			ShopTypeDescription = new DataSetISM(lp.GetDataSetSql("SELECT * FROM t_TypeShop"));
			LoadData();
		}
		public override void LoadData()
		{
			Tree currentTree = this.tree;
			int catalogNode = ShopTypeDescription.Table.AsEnumerable().Where(row => row.Field<int>("shopType") == ShopType).Select(row => row.Field<int>("themeRootNode")).Single();
			BuildTree(catalogNode);
			LoadTree();
		}
		private void BuildTree(int rootNode)
		{
			TreeProvider.TreeProvider treeProv = Classes.ServiceUtility.TreeProvider;
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\"" + rootNode + "\" />";
			DataSet dataSet1 = treeProv.GetTreeContent(xml);
			Stack nodes = new Stack();
			nodes.Push(tree);

			int prevNode = 0;
			foreach (DataRow dr in dataSet1.Tables["table"].Rows)
			{
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if (nodeID != prevNode)
				{
					node = new Node((int)dr["lft"], (int)dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"];
					node.objectNK = dr["NK"].ToString();// == DBNull.Value ? null: (string) dr["NK"];
					node.objectNKRus = dr["label"].ToString();
					node.className = dr["className"].ToString();
					while (nodes.Peek() != null && ((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer)nodes.Peek()).Nodes.Add(node);
					if ((node.Left + 1) != node.Right)
					{
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else
				{
					if (node != null)
					{
						node.AddString.Rows.Add(new string[3] { (string)dr["ordValue"], (string)dr["value"], (string)dr["url"] });
						node.AddString.AcceptChanges();
					}
				}
			}
		}

		private void LoadTree()
		{
			tv1.Nodes.Clear();
			foreach (Node n in tree.Nodes)
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tv1.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node)
		{
			foreach (Node n in node.Nodes)
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}
	}
}
