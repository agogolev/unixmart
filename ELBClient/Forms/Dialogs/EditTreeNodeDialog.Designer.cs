using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class EditTreeNodeDialog
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TextBox tbName;
private System.Windows.Forms.Label label1;
private System.Windows.Forms.TextBox tbUrl;
private System.Windows.Forms.Button btnCancel;
private System.Windows.Forms.Button btnSave;
private System.Windows.Forms.Label label3;
private System.Windows.Forms.TextBox tbObject;
private System.Windows.Forms.Button btnAddObject;
private System.Windows.Forms.Button btnDelObject;
private System.Windows.Forms.Label label5;
private System.Windows.Forms.TextBox tbID;
private System.Windows.Forms.Label label2;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.tbName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbUrl = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.tbObject = new System.Windows.Forms.TextBox();
			this.btnAddObject = new System.Windows.Forms.Button();
			this.btnDelObject = new System.Windows.Forms.Button();
			this.tbID = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// tbName
			// 
			this.tbName.AcceptsReturn = true;
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(72, 9);
			this.tbName.Multiline = true;
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(256, 102);
			this.tbName.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 19);
			this.label1.TabIndex = 25;
			this.label1.Text = "Название:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbUrl
			// 
			this.tbUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbUrl.Location = new System.Drawing.Point(72, 120);
			this.tbUrl.Name = "tbUrl";
			this.tbUrl.Size = new System.Drawing.Size(256, 20);
			this.tbUrl.TabIndex = 3;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 225);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(72, 26);
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "Закрыть";
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(256, 225);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(72, 26);
			this.btnSave.TabIndex = 8;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 157);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 18);
			this.label3.TabIndex = 31;
			this.label3.Text = "Объект:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbObject
			// 
			this.tbObject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbObject.Enabled = false;
			this.tbObject.Location = new System.Drawing.Point(72, 157);
			this.tbObject.Name = "tbObject";
			this.tbObject.Size = new System.Drawing.Size(192, 20);
			this.tbObject.TabIndex = 4;
			// 
			// btnAddObject
			// 
			this.btnAddObject.Location = new System.Drawing.Point(272, 157);
			this.btnAddObject.Name = "btnAddObject";
			this.btnAddObject.Size = new System.Drawing.Size(25, 23);
			this.btnAddObject.TabIndex = 5;
			this.btnAddObject.Text = "...";
			this.btnAddObject.Click += new System.EventHandler(this.btnAddObject_Click);
			// 
			// btnDelObject
			// 
			this.btnDelObject.Location = new System.Drawing.Point(304, 157);
			this.btnDelObject.Name = "btnDelObject";
			this.btnDelObject.Size = new System.Drawing.Size(25, 23);
			this.btnDelObject.TabIndex = 6;
			this.btnDelObject.Text = "X";
			this.btnDelObject.Click += new System.EventHandler(this.btnDelObject_Click);
			// 
			// tbID
			// 
			this.tbID.BackColor = System.Drawing.Color.White;
			this.tbID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbID.Location = new System.Drawing.Point(72, 194);
			this.tbID.Name = "tbID";
			this.tbID.ReadOnly = true;
			this.tbID.Size = new System.Drawing.Size(56, 20);
			this.tbID.TabIndex = 36;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 196);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 19);
			this.label5.TabIndex = 37;
			this.label5.Text = "ID:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 122);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 19);
			this.label2.TabIndex = 27;
			this.label2.Text = "URL:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// EditTreeNodeDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(338, 260);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.tbID);
			this.Controls.Add(this.tbObject);
			this.Controls.Add(this.tbUrl);
			this.Controls.Add(this.tbName);
			this.Controls.Add(this.btnDelObject);
			this.Controls.Add(this.btnAddObject);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(344, 151);
			this.Name = "EditTreeNodeDialog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование листа";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.fmEditTreeNodeDialog_Closing);
			this.Load += new System.EventHandler(this.fmEditTreeNodeDialog_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
