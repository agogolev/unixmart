﻿using System;
using ColumnMenuExtender;

namespace ELBClient.Forms.Dialogs
{
    /// <summary>
    ///     Summary description for fmSelectTheme.
    /// </summary>
    public partial class SelectTheme : GenericForm
    {
        /// <summary>
        ///     Required designer variable.
        /// </summary>
        public SelectTheme()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        public Guid[] GetSelectedItems
        {
            get
            {
                var array = new Guid[lbItems.CheckedItems.Count];
                for (int i = 0; i < lbItems.CheckedItems.Count; i++)
                {
                    var li = (ListItem) lbItems.CheckedItems[i];
                    array[i] = (Guid) li.Tag;
                }
                return array;
            }
        }

        public void AddCheckItem(Guid OID, string name)
        {
            var li = new ListItem(0, name) {Tag = OID};
            lbItems.Items.Add(li);
        }
    }
}