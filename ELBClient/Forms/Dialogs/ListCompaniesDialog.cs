﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListPeoplesDialog.
	/// </summary>
	public partial class ListCompaniesDialog : ListForm
	{
		public IList<DataRow> SelectedRows {
			get {
				return (CompanyGrid.DataSource as DataSet).Tables[0].AsEnumerable().Where(row => row.Field<bool>("isSelected")).ToList();
			}
		}

		public ListCompaniesDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e) {
			DialogResult = DialogResult.OK;
		}

		private void btnSearch_Click(object sender, System.EventArgs e) {
			LoadData();
		}

		public override void LoadData()
		{
			CompanyGrid.Filters.Clear();
			if (CompanyNameText.Text.Trim() != "") CompanyGrid.CreateAuxFilter("companyName", "nameFilter", MetaData.FilterVerb.Like, false, new object[] { "*" + CompanyNameText.Text.Trim() + "*" });
			DataSet ds = lp.GetList(CompanyGrid.GetDataXml().InnerXml);
			DataColumn dc = new DataColumn("isSelected", typeof(bool));
			dc.AllowDBNull = false;
			dc.DefaultValue = false;
			ds.Tables[0].Columns.Add(dc);
			CompanyGrid.SetDataBinding(ds, "table");
			CompanyGrid.DisableNewDel();
		}

		private void ListCompaniesDialog_Load(object sender, EventArgs e)
		{
			LoadData();
		}
	}
}
