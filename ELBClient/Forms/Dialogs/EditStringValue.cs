﻿namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for EditParamType.
	/// </summary>
	public partial class EditStringValue : EditForm
	{
		public string ParamName { get; set; }
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public EditStringValue()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmEditParam_Load(object sender, System.EventArgs e)
		{
			NameText.DataBindings.Add("Text", this, "ParamName");
		}
	
	}
}
