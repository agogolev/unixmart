﻿using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmShowVersions.
	/// </summary>
	public partial class ShowVersions : EditForm
	{
		
		
		
		
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ShowVersions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion
		private void LoadLookup() {
			string xml = lp.GetLookupValues("COrder");
			colStatus.ComboBox.LoadXml(xml, "statusType");
		}
		private void LoadData() {
			string sql = @"
SELECT
	*,
	authorName = dbo.NK(author) 
FROM
	t_ObjectVersions
WHERE
	OID = "+ToSqlString(ObjectOID)+@"

SELECT
	status 
FROM
	t_Order
WHERE
	OID = "+ToSqlString(ObjectOID);

			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			ds.Table.Columns.Add("typeStatus", typeof(int));
			Regex re = new Regex(@"\<status\>(\d+)\</status\>");
			int status = (int)ds.Tables[1].Rows[0]["status"];
			for (int i = ds.Table.Rows.Count - 1; i >= 0; i--) {
				DataRow dr = ds.Table.Rows[i];
				dr["typeStatus"] = status;
				string xmlContent = (string)dr["xmlContent"];
				Match m = re.Match(xmlContent);
				if(m.Success) {
					status = int.Parse(m.Groups[1].Value);
				}
				else break;
			}
			ds.AcceptChanges();
			dgVersions.SetDataBinding(ds, "Table");
			dgVersions.DisableNewDel();
		}

		private void fmShowVersions_Load(object sender, System.EventArgs e) {
			LoadLookup();
			LoadData();
		}

		private void dgVersions_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) {
			if(e.Button == MouseButtons.Right && (dgVersions.Hit.Type == DataGrid.HitTestType.Cell || dgVersions.Hit.Type == DataGrid.HitTestType.RowHeader)) {
				dgVersions.UnSelect(dgVersions.CurrentRowIndex);
				dgVersions.CurrentRowIndex = dgVersions.Hit.Row;
				dgVersions.Select(dgVersions.CurrentRowIndex);
				contextMenu1.Show(dgVersions, new Point(e.X, e.Y));
			}

		}

		private void miShowDifference_Click(object sender, System.EventArgs e) {
			DataRow dr = dgVersions.GetSelectedRow();
			if(dr == null) return;
			string sql = "upGetFullOrderInfo "+ToSqlString(ObjectOID);
			DataSetISM dsCurrent = new DataSetISM(lp.GetDataSetSql(sql));
			Hashtable aliases = BuildTableAliases();
			string xmlContent = (string)dr["xmlContent"];
			SetPrimaryKeys(dsCurrent);
			DataSetISM dsPrev = new DataSetISM(xmlContent);
			SetPrimaryKeys(dsPrev);
			GetDiffOrderInfo(dsCurrent, dsPrev);
			DataSet dsDiff = dsCurrent.GetChanges();
			StringBuilder sb = new StringBuilder();
			foreach(DataTable dt in dsDiff.Tables) {
				if(dt.Rows.Count == 0) continue;
				sb.Append("Таблица: "+aliases[dt.TableName]+"\r\n");
				foreach(DataRow row in dt.Rows) {
					if(row.RowState == DataRowState.Modified) {
						string changeStr = " Изменена строка\r\n";
						sb.Append(changeStr);
						StringBuilder sbTmp = new StringBuilder();
						foreach(DataColumn col in dt.Columns) {
							if(col.ColumnName.ToLower() == "owner" || col.ColumnName.ToLower() == "datemodify" || col.ColumnName.ToLower() == "oid" || col.ColumnName.ToLower() == "goodsOID" || col.ColumnName.ToLower() == "operator" || col.ColumnName.ToLower() == "buyer") continue;
							string prev = row[col.ColumnName].ToString();
							string current = row[col.ColumnName, DataRowVersion.Original].ToString();
							if(current != prev) sbTmp.Append("  Поле "+col.ColumnName+" было: "+prev+" стало: "+current+"\r\n");
						}
						if (sbTmp.Length == 0) sb.Length -= changeStr.Length;
						else sb.Append(sbTmp.ToString());
					}
					else if(row.RowState == DataRowState.Added) {
						sb.Append(" Удалена строка\r\n");
						foreach(DataColumn col in dt.Columns) {
							string val = row[col.ColumnName].ToString();
							sb.Append("  "+col.ColumnName+" = "+val+"\r\n");
						}
					}
					else if(row.RowState == DataRowState.Deleted) {
						sb.Append(" Добавлена строка\r\n");
						foreach(DataColumn col in dt.Columns) {
							string val = row[col.ColumnName, DataRowVersion.Original].ToString();
							sb.Append("  "+col.ColumnName+" = "+val+"\r\n");
						}
					}
				}
			}
			MessageBox.Show(sb.ToString());
		}
		private Hashtable BuildTableAliases() {
			Hashtable aliases = new Hashtable();
			aliases["table"] = "\"Заказ\"";
			aliases["table1"] = "\"Содержимое заказа\"";
			return aliases;
		}
		private void SetPrimaryKeys(DataSet ds) {
			ds.Tables[0].PrimaryKey = new DataColumn[]{ds.Tables[0].Columns[0]}; 
			ds.Tables[1].PrimaryKey = new DataColumn[]{ds.Tables[1].Columns[0], ds.Tables[1].Columns[1]}; 
		}
		public void GetDiffOrderInfo(DataSetISM dsPrev, DataSetISM dsNew) {
			foreach(DataTable dt in dsNew.Tables) {
				string tableName = dt.TableName;
				DataTable prevTbl = dsPrev.Tables[tableName];
				object[] key = new object[dt.PrimaryKey.Length];
				foreach(DataRow dr in dt.Rows) {
					for(int i = 0; i < dt.PrimaryKey.Length; i++) {
						key[i] = dr[dt.PrimaryKey[i].ColumnName];
					}
					DataRow drPrev = prevTbl.Rows.Find(key);
					if(drPrev == null) prevTbl.Rows.Add(dr.ItemArray);
					else {
						foreach(DataColumn dc in dt.Columns) {
							object valPrev = drPrev[dc.ColumnName];
							object valNew = dr[dc.ColumnName];
							if(valPrev.ToString() != valNew.ToString()) drPrev[dc.ColumnName] = valNew;
						}
					}
				}

				for(int i = prevTbl.Rows.Count - 1; i >= 0; i--) {
					DataRow dr = prevTbl.Rows[i];
					if(dr.RowState == DataRowState.Unchanged) {
						for(int j = 0; j < prevTbl.PrimaryKey.Length; j++) {
							key[j] = dr[prevTbl.PrimaryKey[j].ColumnName];
						}
						DataRow chkDelRow = dt.Rows.Find(key);
						if(chkDelRow == null) dr.Delete();
					}
				}
			}
		}
	}
}
