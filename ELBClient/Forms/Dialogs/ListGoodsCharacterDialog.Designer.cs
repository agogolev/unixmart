using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListGoodsCharacterDialog
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
		private ColumnMenuExtender.DataGridISM dataGridISM1;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem miSelect;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbDescription;
		private System.Windows.Forms.Button btnChoose;
		private System.Windows.Forms.Button btnCancel;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Data.DataSet dataSet1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;

		private void InitializeComponent()
		{
			this.dataGridISM1 = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.dataSet1 = new System.Data.DataSet();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miSelect = new System.Windows.Forms.MenuItem();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.label1 = new System.Windows.Forms.Label();
			this.tbDescription = new System.Windows.Forms.TextBox();
			this.btnChoose = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridISM1
			// 
			this.dataGridISM1.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGridISM1.DataMember = "";
			this.dataGridISM1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridISM1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridISM1.Location = new System.Drawing.Point(0, 0);
			this.dataGridISM1.Name = "dataGridISM1";
			this.dataGridISM1.Order = null;
			this.dataGridISM1.ReadOnly = true;
			this.dataGridISM1.Size = new System.Drawing.Size(492, 251);
			this.dataGridISM1.StockClass = "CGoodsCharacter";
			this.dataGridISM1.TabIndex = 0;
			this.dataGridISM1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dataGridISM1, true);
			this.dataGridISM1.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dataGridISM1.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			this.dataGridISM1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dataGridISM1;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Наименование";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.NullText = "";
			this.formattableTextBoxColumn1.Width = 150;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.MappingName = "OID";
			this.formattableTextBoxColumn2.NullText = "";
			this.formattableTextBoxColumn2.Width = 0;
			// 
			// dataSource
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miSelect});
			// 
			// miSelect
			// 
			this.miSelect.Index = 0;
			this.miSelect.Text = "Выбрать";
			this.miSelect.Click += new System.EventHandler(this.miSelect_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Описание:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbDescription
			// 
			this.tbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.tbDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDescription.Location = new System.Drawing.Point(72, 8);
			this.tbDescription.Name = "tbDescription";
			this.tbDescription.Size = new System.Drawing.Size(412, 20);
			this.tbDescription.TabIndex = 2;
			// 
			// btnChoose
			// 
			this.btnChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnChoose.Location = new System.Drawing.Point(412, 40);
			this.btnChoose.Name = "btnChoose";
			this.btnChoose.Size = new System.Drawing.Size(75, 23);
			this.btnChoose.TabIndex = 3;
			this.btnChoose.Text = "Выбрать";
			this.btnChoose.Click += new System.EventHandler(this.btnChoose_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.Location = new System.Drawing.Point(8, 40);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "Отмена";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(110, 40);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(280, 24);
			this.dataGridPager1.TabIndex = 13;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnChoose);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.tbDescription);
			this.panel1.Controls.Add(this.dataGridPager1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 251);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(492, 72);
			this.panel1.TabIndex = 14;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dataGridISM1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(492, 251);
			this.panel2.TabIndex = 15;
			// 
			// fmListGoodsCharacterDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(492, 323);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(464, 160);
			this.Name = "fmListGoodsCharacterDialog";
			this.Text = "Характеристики товара";
			this.Load += new System.EventHandler(this.fmListGoodsCharacter_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
