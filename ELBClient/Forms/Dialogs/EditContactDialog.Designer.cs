using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class EditContactDialog
	{
		#region Windows Form Designer generated code
		public System.Windows.Forms.TextBox tbContact;
private System.Windows.Forms.Label label1;
private System.Windows.Forms.Label label2;
private System.Windows.Forms.Button btnCancel;
private System.Windows.Forms.Button btnOk;
public System.Windows.Forms.ComboBox cbContactType;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.cbContactType = new System.Windows.Forms.ComboBox();
			this.tbContact = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOk = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// cbContactType
			// 
			this.cbContactType.Items.AddRange(new object[] {
            "Раб. телефон",
            "Дом. телефон",
            "Раб. адрес",
            "Дом. адрес"});
			this.cbContactType.Location = new System.Drawing.Point(128, 18);
			this.cbContactType.Name = "cbContactType";
			this.cbContactType.Size = new System.Drawing.Size(192, 21);
			this.cbContactType.TabIndex = 0;
			// 
			// tbContact
			// 
			this.tbContact.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbContact.Location = new System.Drawing.Point(128, 65);
			this.tbContact.Name = "tbContact";
			this.tbContact.Size = new System.Drawing.Size(192, 20);
			this.tbContact.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 19);
			this.label1.TabIndex = 2;
			this.label1.Text = "Тип контакта";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 18);
			this.label2.TabIndex = 3;
			this.label2.Text = "Контакт";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 97);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 26);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "Закрыть";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(248, 97);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 26);
			this.btnOk.TabIndex = 5;
			this.btnOk.Text = "Сохранить";
			// 
			// EditContactDialog
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(330, 136);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tbContact);
			this.Controls.Add(this.cbContactType);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(336, 170);
			this.Name = "EditContactDialog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Контакт";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
