﻿using System;
using System.Windows.Forms;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmEditTreeNodeDialog.
	/// </summary>
	public partial class EditTreeNodeDialog : GenericForm
	{	
		public string NodeID="";
		public string NodeName="";
		public string NodeUrl="";
		public Guid NodeObjectNew;		//новые
		public string NodeObjectNKNew;
		public Guid NodeObject = Guid.Empty;	//текущие
		public string NodeObjectNK = "";
		public string NodeObjectNKRus = "";

		
		
		
		
		
		
		
		
		
		
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public EditTreeNodeDialog()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmEditTreeNodeDialog_Load(object sender, System.EventArgs e)
		{
			DataBinding();
		}

		private void DataBinding()
		{
			tbID.Text = NodeID;
			tbName.Text = NodeName;
			tbUrl.Text = NodeUrl;
			NodeObjectNew = NodeObject;
			tbObject.Text = NodeObjectNKNew = NodeObjectNK;
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveObject();
		}

		private void SaveObject()
		{
			NodeName = tbName.Text;
			NodeUrl = tbUrl.Text;
			NodeObject = NodeObjectNew;
			NodeObjectNK = NodeObjectNKNew;
			this.DialogResult = DialogResult.OK;
		}

		private void btnAddObject_Click(object sender, System.EventArgs e)
		{
			AttachObject fm = new AttachObject();
			if(fm.ShowDialog(this) == DialogResult.OK)
			{
				NodeObjectNew = fm.SelectedOID;
				NodeObjectNKNew = fm.SelectedNK;
				tbObject.Text = fm.SelectedNK;
				if(tbName.Text == "")
					tbName.Text = fm.SelectedNK;

				NodeObjectNKRus = fm.SelectedNKRus;
			}
		}

		private void btnDelObject_Click(object sender, System.EventArgs e)
		{
			NodeObjectNew = Guid.Empty;
			NodeObjectNKNew = "";
			tbObject.Text = "";
			NodeObjectNKRus = "";
		}

		private bool isModified()
		{
			if(tbName.Text != NodeName || tbUrl.Text != NodeUrl || NodeObjectNew != NodeObject)
				return true;
			else return false;
		}

		private void fmEditTreeNodeDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(isModified())
			{
				DialogResult dr = MessageBox.Show(this, "Елемент был изменен, хотите сохранить?", "Внимание!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
				if (dr == DialogResult.Yes)
					SaveObject();
				else if (dr == DialogResult.Cancel)
					e.Cancel = true;
			}
		}

//	//	SelectPressed = true;
//		this.DialogResult = DialogResult.OK;
//		Close();
	}
}
