﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.Forms.Dialogs
{
	/// <summary>
	/// Summary description for fmListGoodsDialog.
	/// </summary>
	public partial class ListGoodsDialog : GenericForm
	{
		public DataSetISM DataSource { get; set; }
		public IEnumerable<DataRow> SelectedRows {
			get {
				if (DataSource == null || DataSource.Table.AsEnumerable().Count(row => row.Field<bool>("isSelected")) == 0)
				{
					return (new[] { dgGoods.GetSelectedRow() });
				}
				return DataSource.Table.AsEnumerable().Where(row => row.Field<bool>("isSelected"));
			}
		}

		public int ShopType { get; set; }

		public ListGoodsDialog()
		{
			InitializeComponent();
		}


		private void btnClose_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.Cancel;
		}

		private void btnSearch_Click(object sender, EventArgs e) {
			if(string.IsNullOrWhiteSpace(txtCode.Text) && string.IsNullOrWhiteSpace(txtName.Text) && string.IsNullOrWhiteSpace(ObjectId.Text)) return;
			var sb = new StringBuilder();
			sb.Append(@"
SELECT
	g.OID,
	ID = g.pseudoID,
	shortName = g.productType + ' ' + g.model,
	manufacturer_name = dbo.NK(g.manufacturer),
	NK = dbo.NK(g.OID),
	g.description,
	o.objectId,
	isSelected = convert(bit, 0)");
			if (ShopType != 0)
			{
				sb.Append(@",
	gp.price");
			}
			sb.Append(@"
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID");
			if (ShopType != 0)
			{
				sb.AppendFormat(@"
	inner join t_GoodsPrices gp on g.OID = gp.OID And gp.shopType = {0} And gp.price > 0", ShopType);
			}
			sb.Append(@"
WHERE
	g.postavshik_id is not null And %SEARCH_COND%");
			string sql = sb.ToString();

			string SEARCH_COND;
			int id;

			if(txtCode.Text.Trim() != "") {
				if (!int.TryParse(txtCode.Text.Trim(), out id))
				{
					ShowError("Код склада не числовой");
					return;
				}
				SEARCH_COND = "g.pseudoID = " + id;
			}
			else if (ObjectId.Text != "")
			{
				if (!int.TryParse(ObjectId.Text, out id))
				{
					ShowError("Код на сайте не числовой");
					return;
				}
				SEARCH_COND = "o.objectId = " + id;
			}
			else {
				string shortName = txtName.Text.Trim();
				SEARCH_COND = "g.model like '%"+shortName.Replace("'", "''")+"%'";
			}
			sql = sql.Replace("%SEARCH_COND%", SEARCH_COND);
			DataSource = new DataSetISM(lp.GetDataSetSql(sql));

			dgGoods.SetDataBinding(DataSource, "table");
			dgGoods.DisableNewDel();
		}

		private void SelectButton_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}
	}
}
