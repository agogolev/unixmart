using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class AttachDocs
	{
		#region Windows Form Designer generated code
		private ColumnMenuExtender.ExtendedDataGrid dataGrid1;
		private System.Windows.Forms.MenuItem mItemDel;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ContextMenu contextMenuAttImgs;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ComboBox cbType;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.ContextMenu contextMenuImages;
		private System.Windows.Forms.MenuItem mItemAdd;
		private System.Windows.Forms.MenuItem mItemEdit;
		private System.Windows.Forms.MenuItem mItemDelFromDB;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbFile;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbMimeType;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox cbType1;
		private System.Windows.Forms.OpenFileDialog ofdFile;
		private System.Windows.Forms.Button button3;
		private ColumnMenuExtender.DataGridISM dgImages;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private System.Data.DataSet dataSet1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
            this.dataGrid1 = new ColumnMenuExtender.ExtendedDataGrid();
            this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.contextMenuAttImgs = new System.Windows.Forms.ContextMenu();
            this.mItemDel = new System.Windows.Forms.MenuItem();
            this.dgImages = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
            this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
            this.dataSet1 = new System.Data.DataSet();
            this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.cbType1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbMimeType = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFile = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.contextMenuImages = new System.Windows.Forms.ContextMenu();
            this.mItemEdit = new System.Windows.Forms.MenuItem();
            this.mItemDelFromDB = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.mItemAdd = new System.Windows.Forms.MenuItem();
            this.ofdFile = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGrid1
            // 
            this.dataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGrid1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGrid1.CaptionVisible = false;
            this.dataGrid1.DataMember = "";
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Location = new System.Drawing.Point(8, 28);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.ReadOnly = true;
            this.dataGrid1.Size = new System.Drawing.Size(522, 248);
            this.dataGrid1.TabIndex = 3;
            this.dataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
            this.dataGrid1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGrid1_MouseDown);
            this.dataGrid1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGrid1_MouseUp);
            // 
            // extendedDataGridTableStyle2
            // 
            this.extendedDataGridTableStyle2.DataGrid = this.dataGrid1;
            this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5});
            this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle2.MappingName = "table";
            this.extendedDataGridTableStyle2.PreferredColumnWidth = 150;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Тип";
            this.formattableTextBoxColumn4.MappingName = "ordValue";
            this.formattableTextBoxColumn4.NullText = "";
            this.formattableTextBoxColumn4.Width = 150;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "Название";
            this.formattableTextBoxColumn5.MappingName = "propValue_NK";
            this.formattableTextBoxColumn5.NullText = "";
            this.formattableTextBoxColumn5.Width = 150;
            // 
            // contextMenuAttImgs
            // 
            this.contextMenuAttImgs.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mItemDel});
            // 
            // mItemDel
            // 
            this.mItemDel.Index = 0;
            this.mItemDel.Text = "Удалить";
            this.mItemDel.Click += new System.EventHandler(this.mItemDel_Click);
            // 
            // dgImages
            // 
            this.dgImages.AllowSorting = false;
            this.dgImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgImages.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgImages.DataMember = "";
            this.dgImages.FilterString = null;
            this.dgImages.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgImages.Location = new System.Drawing.Point(8, 9);
            this.dgImages.Name = "dgImages";
            this.dgImages.Order = null;
            this.dgImages.ReadOnly = true;
            this.dgImages.Size = new System.Drawing.Size(514, 141);
            this.dgImages.StockClass = "CBinaryData";
            this.dgImages.TabIndex = 33;
            this.dgImages.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.columnMenuExtender1.SetUseGridMenu(this.dgImages, true);
            this.dgImages.Reload += new System.EventHandler(this.dataGridISM1_Reload);
            this.dgImages.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseDown);
            this.dgImages.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.AllowSorting = false;
            this.extendedDataGridTableStyle1.DataGrid = this.dgImages;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7,
            this.formattableTextBoxColumn8});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            this.extendedDataGridTableStyle1.PreferredColumnWidth = 100;
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Название";
            this.formattableTextBoxColumn1.MappingName = "name";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
            this.formattableTextBoxColumn1.NullText = "";
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Имя файла";
            this.formattableTextBoxColumn2.MappingName = "fileName";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
            this.formattableTextBoxColumn2.NullText = "";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = null;
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "Ширина";
            this.formattableTextBoxColumn6.MappingName = "width";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
            this.formattableTextBoxColumn6.Width = 75;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = null;
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "Высота";
            this.formattableTextBoxColumn7.MappingName = "height";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
            this.formattableTextBoxColumn7.Width = 75;
            // 
            // formattableTextBoxColumn8
            // 
            this.formattableTextBoxColumn8.FieldName = null;
            this.formattableTextBoxColumn8.FilterFieldName = null;
            this.formattableTextBoxColumn8.Format = "";
            this.formattableTextBoxColumn8.FormatInfo = null;
            this.formattableTextBoxColumn8.HeaderText = "mime тип";
            this.formattableTextBoxColumn8.MappingName = "mimeType";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn8, this.menuFilterSort1);
            this.formattableTextBoxColumn8.Width = 75;
            // 
            // dataGridPager1
            // 
            this.dataGridPager1.AllowAll = true;
            this.dataGridPager1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridPager1.Batch = 30;
            this.dataGridPager1.Location = new System.Drawing.Point(8, 160);
            this.dataGridPager1.Name = "dataGridPager1";
            this.dataGridPager1.PageCount = 0;
            this.dataGridPager1.PageNum = 1;
            this.dataGridPager1.Size = new System.Drawing.Size(288, 27);
            this.dataGridPager1.TabIndex = 34;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
            // 
            // label2
            // 
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(8, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 19);
            this.label2.TabIndex = 36;
            this.label2.Text = "Привязанные данные";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 503);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(538, 46);
            this.panel1.TabIndex = 37;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCancel.Location = new System.Drawing.Point(8, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 33;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(538, 503);
            this.panel2.TabIndex = 38;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(538, 221);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.cbType1);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.tbName);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.tbMimeType);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.tbFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(536, 179);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Новая";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(256, 92);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(72, 23);
            this.button3.TabIndex = 59;
            this.button3.Text = "Добавить";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cbType1
            // 
            this.cbType1.Location = new System.Drawing.Point(80, 92);
            this.cbType1.Name = "cbType1";
            this.cbType1.Size = new System.Drawing.Size(168, 21);
            this.cbType1.TabIndex = 57;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 19);
            this.label7.TabIndex = 56;
            this.label7.Text = "Тип:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbName
            // 
            this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbName.Location = new System.Drawing.Point(80, 55);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(432, 20);
            this.tbName.TabIndex = 48;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 19);
            this.label1.TabIndex = 49;
            this.label1.Text = "Название:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMimeType
            // 
            this.tbMimeType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbMimeType.Location = new System.Drawing.Point(80, 138);
            this.tbMimeType.Name = "tbMimeType";
            this.tbMimeType.ReadOnly = true;
            this.tbMimeType.Size = new System.Drawing.Size(88, 20);
            this.tbMimeType.TabIndex = 51;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 19);
            this.label6.TabIndex = 50;
            this.label6.Text = "MIME тип:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(448, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 24);
            this.button1.TabIndex = 41;
            this.button1.Text = "Выбрать";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 19);
            this.label3.TabIndex = 42;
            this.label3.Text = "Файл:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbFile
            // 
            this.tbFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFile.Location = new System.Drawing.Point(80, 18);
            this.tbFile.Name = "tbFile";
            this.tbFile.Size = new System.Drawing.Size(360, 20);
            this.tbFile.TabIndex = 40;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgImages);
            this.tabPage2.Controls.Add(this.dataGridPager1);
            this.tabPage2.Controls.Add(this.cbType);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(530, 195);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Из базы";
            // 
            // cbType
            // 
            this.cbType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.Location = new System.Drawing.Point(338, 160);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(184, 21);
            this.cbType.TabIndex = 37;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.dataGrid1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 221);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(538, 282);
            this.panel4.TabIndex = 0;
            // 
            // contextMenuImages
            // 
            this.contextMenuImages.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mItemEdit,
            this.mItemDelFromDB,
            this.menuItem5,
            this.mItemAdd});
            // 
            // mItemEdit
            // 
            this.mItemEdit.Index = 0;
            this.mItemEdit.Text = "Редактировать";
            this.mItemEdit.Click += new System.EventHandler(this.mItemEdit_Click);
            // 
            // mItemDelFromDB
            // 
            this.mItemDelFromDB.Index = 1;
            this.mItemDelFromDB.Text = "Удалить";
            this.mItemDelFromDB.Click += new System.EventHandler(this.mItemDelFromDB_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 2;
            this.menuItem5.Text = "-";
            // 
            // mItemAdd
            // 
            this.mItemAdd.Index = 3;
            this.mItemAdd.Text = "Добавить";
            this.mItemAdd.Click += new System.EventHandler(this.mItemAdd_Click);
            // 
            // AttachDocs
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(538, 549);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(0, 563);
            this.Name = "AttachDocs";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Привязка двоичных данных";
            this.Load += new System.EventHandler(this.AttachDocs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
