using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ShowVersions
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.DataGridISM dgVersions;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem miShowDifference;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colDateOccur;
		private ColumnMenuExtender.FormattableTextBoxColumn colAuthor;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colStatus;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.dgVersions = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colDateOccur = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colAuthor = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colStatus = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miShowDifference = new System.Windows.Forms.MenuItem();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgVersions)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 253);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(424, 40);
			this.panel1.TabIndex = 0;
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnClose.Location = new System.Drawing.Point(184, 8);
			this.btnClose.Name = "btnClose";
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Закрыть";
			// 
			// dgVersions
			// 
			this.dgVersions.BackgroundColor = System.Drawing.Color.White;
			this.dgVersions.DataMember = "";
			this.dgVersions.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgVersions.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgVersions.Location = new System.Drawing.Point(0, 0);
			this.dgVersions.Name = "dgVersions";
			this.dgVersions.Order = null;
			this.dgVersions.Size = new System.Drawing.Size(424, 253);
			this.dgVersions.TabIndex = 1;
			this.dgVersions.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																													 this.extendedDataGridTableStyle1});
			this.dgVersions.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgVersions_MouseDown);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgVersions;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																																																									this.colDateOccur,
																																																									this.colAuthor,
																																																									this.colStatus});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// colDateOccur
			// 
			this.colDateOccur.FieldName = null;
			this.colDateOccur.FilterFieldName = null;
			this.colDateOccur.Format = "";
			this.colDateOccur.FormatInfo = null;
			this.colDateOccur.HeaderText = "Дата изменения";
			this.colDateOccur.MappingName = "dateOccur";
			this.colDateOccur.Width = 75;
			// 
			// colAuthor
			// 
			this.colAuthor.FieldName = null;
			this.colAuthor.FilterFieldName = null;
			this.colAuthor.Format = "";
			this.colAuthor.FormatInfo = null;
			this.colAuthor.HeaderText = "Кто изменил";
			this.colAuthor.MappingName = "authorName";
			this.colAuthor.Width = 75;
			// 
			// colStatus
			// 
			this.colStatus.FieldName = null;
			this.colStatus.FilterFieldName = null;
			this.colStatus.Format = "";
			this.colStatus.FormatInfo = null;
			this.colStatus.HeaderText = "Статус заказа";
			this.colStatus.MappingName = "typeStatus";
			this.colStatus.Width = 75;
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																																								 this.miShowDifference});
			// 
			// miShowDifference
			// 
			this.miShowDifference.Index = 0;
			this.miShowDifference.Text = "Посмотреть различия";
			this.miShowDifference.Click += new System.EventHandler(this.miShowDifference_Click);
			// 
			// fmShowVersions
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(424, 293);
			this.Controls.Add(this.dgVersions);
			this.Controls.Add(this.panel1);
			this.Name = "fmShowVersions";
			this.Text = "fmShowVersions";
			this.Load += new System.EventHandler(this.fmShowVersions_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgVersions)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
