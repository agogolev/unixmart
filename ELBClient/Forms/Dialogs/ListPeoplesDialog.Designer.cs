using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Dialogs
{
	public partial class ListPeoplesDialog
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox NameSearch;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TextBox EMailSearch;
		private ColumnMenuExtender.DataGridISM dgPeoples;
		private System.Windows.Forms.Button btnNew;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.FormattableTextBoxColumn colEMail;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.EMailSearch = new System.Windows.Forms.TextBox();
			this.NameSearch = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnSearch = new System.Windows.Forms.Button();
			this.dgPeoples = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colEMail = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.LastNameSearch = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgPeoples)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.LastNameSearch);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.EMailSearch);
			this.panel1.Controls.Add(this.NameSearch);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnSearch);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(536, 98);
			this.panel1.TabIndex = 0;
			// 
			// btnNew
			// 
			this.btnNew.Location = new System.Drawing.Point(376, 36);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(75, 26);
			this.btnNew.TabIndex = 5;
			this.btnNew.Text = "Новый";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(456, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 4;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// EMailSearch
			// 
			this.EMailSearch.Location = new System.Drawing.Point(112, 63);
			this.EMailSearch.Name = "EMailSearch";
			this.EMailSearch.Size = new System.Drawing.Size(100, 20);
			this.EMailSearch.TabIndex = 3;
			// 
			// NameSearch
			// 
			this.NameSearch.Location = new System.Drawing.Point(112, 9);
			this.NameSearch.Name = "NameSearch";
			this.NameSearch.Size = new System.Drawing.Size(240, 20);
			this.NameSearch.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 62);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 26);
			this.label2.TabIndex = 1;
			this.label2.Text = "eMail:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "Имя:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnSearch
			// 
			this.btnSearch.Location = new System.Drawing.Point(376, 9);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(75, 27);
			this.btnSearch.TabIndex = 1;
			this.btnSearch.Text = "Поиск";
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// dgPeoples
			// 
			this.dgPeoples.BackgroundColor = System.Drawing.Color.White;
			this.dgPeoples.DataMember = "";
			this.dgPeoples.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgPeoples.FilterString = null;
			this.dgPeoples.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgPeoples.Location = new System.Drawing.Point(0, 98);
			this.dgPeoples.Name = "dgPeoples";
			this.dgPeoples.Order = null;
			this.dgPeoples.Size = new System.Drawing.Size(536, 315);
			this.dgPeoples.StockClass = "CPeople";
			this.dgPeoples.StockNumInBatch = 0;
			this.dgPeoples.TabIndex = 1;
			this.dgPeoples.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.dgPeoples.DoubleClick += new System.EventHandler(this.dgGoods_DoubleClick);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgPeoples;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colName,
            this.formattableTextBoxColumn1,
            this.colEMail});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// colName
			// 
			this.colName.FieldName = null;
			this.colName.FilterFieldName = null;
			this.colName.Format = "";
			this.colName.FormatInfo = null;
			this.colName.HeaderText = "Фамилия";
			this.colName.MappingName = "lastName";
			this.colName.Width = 200;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Имя";
			this.formattableTextBoxColumn1.MappingName = "firstName";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// colEMail
			// 
			this.colEMail.FieldName = null;
			this.colEMail.FilterFieldName = null;
			this.colEMail.Format = "";
			this.colEMail.FormatInfo = null;
			this.colEMail.HeaderText = "EMail";
			this.colEMail.MappingName = "eMail";
			this.colEMail.Width = 75;
			// 
			// LastNameSearch
			// 
			this.LastNameSearch.Location = new System.Drawing.Point(112, 37);
			this.LastNameSearch.Name = "LastNameSearch";
			this.LastNameSearch.Size = new System.Drawing.Size(240, 20);
			this.LastNameSearch.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 36);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 27);
			this.label3.TabIndex = 6;
			this.label3.Text = "Фамилия:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ListPeoplesDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(536, 413);
			this.Controls.Add(this.dgPeoples);
			this.Controls.Add(this.panel1);
			this.Name = "ListPeoplesDialog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Персоны";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgPeoples)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private TextBox LastNameSearch;
		private Label label3;
	}
}
