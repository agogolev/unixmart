using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListGoods
	{

		private System.Windows.Forms.Button NewGoods;
		private System.Windows.Forms.Panel panel3;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridISM dgGoods;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ContextMenu treeMenu;
		private System.Windows.Forms.MenuItem miUpdateTree;
		private System.Windows.Forms.TreeView tvMasterMos;
		private Panel panel2;
		private Button RecalcPrices;
		private Button RecalcThisCategory;
		private System.Windows.Forms.Splitter splitter1;
		private System.Data.DataSet dataSet1;
		private ELBClient.UserControls.ListContextMenu listContextMenu1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle ListGoodsTableStyle;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn ParamCountColumn;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
		private ColumnMenuExtender.FormattableTextBoxColumn ObjectIDColumn;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private System.ComponentModel.IContainer components;

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListGoods));
            this.dgGoods = new ColumnMenuExtender.DataGridISM();
            this.ListGoodsTableStyle = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.ObjectIDColumn = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.ParamCountColumn = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn3 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn4 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn5 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn6 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.extendedDataGridSelectorColumn2 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.formattableTextBoxColumn10 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn11 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn12 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn9 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.NewGoods = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
            this.tvMasterMos = new System.Windows.Forms.TreeView();
            this.treeMenu = new System.Windows.Forms.ContextMenu();
            this.miUpdateTree = new System.Windows.Forms.MenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.KdrRadio = new System.Windows.Forms.RadioButton();
            this.NskRadio = new System.Windows.Forms.RadioButton();
            this.HasDescriptionCheck = new System.Windows.Forms.CheckBox();
            this.SpbRadio = new System.Windows.Forms.RadioButton();
            this.MoscowRadio = new System.Windows.Forms.RadioButton();
            this.ShowPublished = new System.Windows.Forms.CheckBox();
            this.RecalcThisCategory = new System.Windows.Forms.Button();
            this.RecalcPrices = new System.Windows.Forms.Button();
            this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
            this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
            this.formattableBooleanColumn2 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.SaveList = new System.Windows.Forms.SaveFileDialog();
            this.formattableTextBoxColumn13 = new ColumnMenuExtender.FormattableTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgGoods
            // 
            this.dgGoods.AllowSorting = false;
            this.dgGoods.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgGoods.ColumnDragEnabled = true;
            this.dgGoods.DataMember = "";
            this.dgGoods.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgGoods.FilterString = null;
            this.dgGoods.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgGoods.Location = new System.Drawing.Point(0, 0);
            this.dgGoods.Name = "dgGoods";
            this.dgGoods.Order = null;
            this.dgGoods.ReadOnly = true;
            this.dgGoods.Size = new System.Drawing.Size(641, 352);
            this.dgGoods.StockClass = "CGoods";
            this.dgGoods.TabIndex = 15;
            this.dgGoods.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.ListGoodsTableStyle});
            this.columnMenuExtender1.SetUseGridMenu(this.dgGoods, true);
            this.dgGoods.Reload += new System.EventHandler(this.dataGridISM1_Reload);
            this.dgGoods.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dataGridISM1_ActionKeyPressed);
            this.dgGoods.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
            this.dgGoods.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
            // 
            // ListGoodsTableStyle
            // 
            this.ListGoodsTableStyle.AllowSorting = false;
            this.ListGoodsTableStyle.DataGrid = this.dgGoods;
            this.ListGoodsTableStyle.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.ObjectIDColumn,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn6,
            this.ParamCountColumn,
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn7,
            this.formattableBooleanColumn1,
            this.formattableTextBoxColumn8,
            this.formattableBooleanColumn3,
            this.formattableBooleanColumn4,
            this.formattableBooleanColumn5,
            this.formattableBooleanColumn6,
            this.extendedDataGridSelectorColumn1,
            this.extendedDataGridSelectorColumn2,
            this.formattableTextBoxColumn10,
            this.formattableTextBoxColumn11,
            this.formattableTextBoxColumn12,
            this.formattableTextBoxColumn9,
            this.formattableTextBoxColumn13});
            this.ListGoodsTableStyle.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.ListGoodsTableStyle.MappingName = "table";
            this.ListGoodsTableStyle.PreferredColumnWidth = 100;
            this.ListGoodsTableStyle.ReadOnly = true;
            // 
            // ObjectIDColumn
            // 
            this.ObjectIDColumn.FieldName = null;
            this.ObjectIDColumn.FilterFieldName = null;
            this.ObjectIDColumn.Format = "";
            this.ObjectIDColumn.FormatInfo = null;
            this.ObjectIDColumn.HeaderText = "Код магазина";
            this.ObjectIDColumn.MappingName = "objectID";
            this.columnMenuExtender1.SetMenu(this.ObjectIDColumn, this.menuFilterSort1);
            this.ObjectIDColumn.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Код поставщика";
            this.formattableTextBoxColumn2.MappingName = "postavshik_id";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Модель";
            this.formattableTextBoxColumn1.MappingName = "model";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = "NK(category)";
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Категория";
            this.formattableTextBoxColumn4.MappingName = "categoryName";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn4, this.menuFilterSort1);
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = "NK(manufacturer)";
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "Производитель";
            this.formattableTextBoxColumn6.MappingName = "manufacturer";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
            this.formattableTextBoxColumn6.Width = 75;
            // 
            // ParamCountColumn
            // 
            this.ParamCountColumn.FieldName = "fHasDescr(OID)";
            this.ParamCountColumn.FilterFieldName = null;
            this.ParamCountColumn.Format = "";
            this.ParamCountColumn.FormatInfo = null;
            this.ParamCountColumn.HeaderText = "Кол-во параметров";
            this.ParamCountColumn.MappingName = "hasDescr";
            this.columnMenuExtender1.SetMenu(this.ParamCountColumn, this.menuFilterSort1);
            this.ParamCountColumn.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "#,0.##";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Цена производителя";
            this.formattableTextBoxColumn3.MappingName = "manufPrice";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "#,0.##";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "Минимальная цена";
            this.formattableTextBoxColumn5.MappingName = "minSellPrice";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn5, this.menuFilterSort1);
            this.formattableTextBoxColumn5.Width = 75;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = "fPrice(OID, @shopType)";
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "#,0.##";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "Цена на сайте";
            this.formattableTextBoxColumn7.MappingName = "sitePrice";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
            this.formattableTextBoxColumn7.Width = 75;
            // 
            // formattableBooleanColumn1
            // 
            this.formattableBooleanColumn1.FieldName = "fIsManual(OID, @shopType)";
            this.formattableBooleanColumn1.FilterFieldName = null;
            this.formattableBooleanColumn1.HeaderText = "Ручной ввод";
            this.formattableBooleanColumn1.MappingName = "priceIsManual";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn1, this.menuFilterSort1);
            this.formattableBooleanColumn1.Width = 75;
            // 
            // formattableTextBoxColumn8
            // 
            this.formattableTextBoxColumn8.FieldName = "ignore";
            this.formattableTextBoxColumn8.FilterFieldName = null;
            this.formattableTextBoxColumn8.Format = "P1";
            this.formattableTextBoxColumn8.FormatInfo = null;
            this.formattableTextBoxColumn8.HeaderText = "Наценка (%)";
            this.formattableTextBoxColumn8.MappingName = "calcAddPrice";
            this.formattableTextBoxColumn8.Width = 75;
            // 
            // formattableBooleanColumn3
            // 
            this.formattableBooleanColumn3.FieldName = "fisHit(OID, @shopType)";
            this.formattableBooleanColumn3.FilterFieldName = null;
            this.formattableBooleanColumn3.HeaderText = "Хит";
            this.formattableBooleanColumn3.MappingName = "isHit";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn3, this.menuFilterSort1);
            this.formattableBooleanColumn3.Width = 75;
            // 
            // formattableBooleanColumn4
            // 
            this.formattableBooleanColumn4.FieldName = "fIsNew(OID, @shopType)";
            this.formattableBooleanColumn4.FilterFieldName = null;
            this.formattableBooleanColumn4.HeaderText = "Новинка";
            this.formattableBooleanColumn4.MappingName = "isNew";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn4, this.menuFilterSort1);
            this.formattableBooleanColumn4.Width = 75;
            // 
            // formattableBooleanColumn5
            // 
            this.formattableBooleanColumn5.FieldName = "fIsSale(OID, @shopType)";
            this.formattableBooleanColumn5.FilterFieldName = null;
            this.formattableBooleanColumn5.HeaderText = "Распродажа";
            this.formattableBooleanColumn5.MappingName = "isSale";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn5, this.menuFilterSort1);
            this.formattableBooleanColumn5.Width = 75;
            // 
            // formattableBooleanColumn6
            // 
            this.formattableBooleanColumn6.FieldName = "fInstantDelivery(OID, @shopType)";
            this.formattableBooleanColumn6.FilterFieldName = null;
            this.formattableBooleanColumn6.HeaderText = "Экспресс доставка";
            this.formattableBooleanColumn6.MappingName = "instantDelivery";
            this.formattableBooleanColumn6.Width = 75;
            // 
            // extendedDataGridSelectorColumn1
            // 
            this.extendedDataGridSelectorColumn1.FieldName = null;
            this.extendedDataGridSelectorColumn1.FilterFieldName = null;
            this.extendedDataGridSelectorColumn1.Format = "";
            this.extendedDataGridSelectorColumn1.FormatInfo = null;
            this.extendedDataGridSelectorColumn1.HeaderText = "Яндекс";
            this.extendedDataGridSelectorColumn1.MappingName = "yandexUrl";
            this.columnMenuExtender1.SetMenu(this.extendedDataGridSelectorColumn1, this.menuFilterSort1);
            this.extendedDataGridSelectorColumn1.Width = 75;
            // 
            // extendedDataGridSelectorColumn2
            // 
            this.extendedDataGridSelectorColumn2.FieldName = "ignore";
            this.extendedDataGridSelectorColumn2.FilterFieldName = null;
            this.extendedDataGridSelectorColumn2.Format = "";
            this.extendedDataGridSelectorColumn2.FormatInfo = null;
            this.extendedDataGridSelectorColumn2.MappingName = "goodsUrl";
            this.extendedDataGridSelectorColumn2.Width = 75;
            // 
            // formattableTextBoxColumn10
            // 
            this.formattableTextBoxColumn10.FieldName = "fFirstCategoryId(OID, @rootNode)";
            this.formattableTextBoxColumn10.FilterFieldName = null;
            this.formattableTextBoxColumn10.Format = "";
            this.formattableTextBoxColumn10.FormatInfo = null;
            this.formattableTextBoxColumn10.MappingName = "firstCategoryId";
            this.formattableTextBoxColumn10.Width = 0;
            // 
            // formattableTextBoxColumn11
            // 
            this.formattableTextBoxColumn11.FieldName = null;
            this.formattableTextBoxColumn11.FilterFieldName = null;
            this.formattableTextBoxColumn11.Format = "";
            this.formattableTextBoxColumn11.FormatInfo = null;
            this.formattableTextBoxColumn11.HeaderText = "Тип товара";
            this.formattableTextBoxColumn11.MappingName = "productType";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn11, this.menuFilterSort1);
            this.formattableTextBoxColumn11.Width = 50;
            // 
            // formattableTextBoxColumn12
            // 
            this.formattableTextBoxColumn12.FieldName = "fGetImageCount(OID)";
            this.formattableTextBoxColumn12.FilterFieldName = null;
            this.formattableTextBoxColumn12.Format = "";
            this.formattableTextBoxColumn12.FormatInfo = null;
            this.formattableTextBoxColumn12.HeaderText = "Кол-во фото";
            this.formattableTextBoxColumn12.MappingName = "imageCount";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn12, this.menuFilterSort1);
            this.formattableTextBoxColumn12.Width = 75;
            // 
            // formattableTextBoxColumn9
            // 
            this.formattableTextBoxColumn9.FieldName = "fGetInstrCount(OID)";
            this.formattableTextBoxColumn9.FilterFieldName = null;
            this.formattableTextBoxColumn9.Format = "";
            this.formattableTextBoxColumn9.FormatInfo = null;
            this.formattableTextBoxColumn9.HeaderText = "Кол-во инструкций";
            this.formattableTextBoxColumn9.MappingName = "instrCount";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn9, this.menuFilterSort1);
            this.formattableTextBoxColumn9.Width = 75;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            // 
            // NewGoods
            // 
            this.NewGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NewGoods.BackColor = System.Drawing.SystemColors.Control;
            this.NewGoods.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.NewGoods.Location = new System.Drawing.Point(558, 50);
            this.NewGoods.Name = "NewGoods";
            this.NewGoods.Size = new System.Drawing.Size(75, 27);
            this.NewGoods.TabIndex = 17;
            this.NewGoods.Text = "Новый";
            this.NewGoods.UseVisualStyleBackColor = false;
            this.NewGoods.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
            // 
            // listContextMenu1
            // 
            this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
            this.listContextMenu1.GetList += new System.EventHandler(this.listContextMenu1_GetList);
            this.listContextMenu1.NewClick += new System.EventHandler(this.btnNew_Click);
            // 
            // tvMasterMos
            // 
            this.tvMasterMos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMasterMos.HideSelection = false;
            this.tvMasterMos.Location = new System.Drawing.Point(0, 0);
            this.tvMasterMos.Name = "tvMasterMos";
            this.tvMasterMos.Size = new System.Drawing.Size(248, 443);
            this.tvMasterMos.TabIndex = 0;
            this.tvMasterMos.Tag = "1";
            this.tvMasterMos.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMaster_AfterSelect);
            // 
            // treeMenu
            // 
            this.treeMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miUpdateTree});
            // 
            // miUpdateTree
            // 
            this.miUpdateTree.Index = 0;
            this.miUpdateTree.Text = "Обновить";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgGoods);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(251, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(641, 443);
            this.panel3.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.KdrRadio);
            this.panel1.Controls.Add(this.NskRadio);
            this.panel1.Controls.Add(this.HasDescriptionCheck);
            this.panel1.Controls.Add(this.SpbRadio);
            this.panel1.Controls.Add(this.MoscowRadio);
            this.panel1.Controls.Add(this.ShowPublished);
            this.panel1.Controls.Add(this.RecalcThisCategory);
            this.panel1.Controls.Add(this.RecalcPrices);
            this.panel1.Controls.Add(this.dataGridPager1);
            this.panel1.Controls.Add(this.NewGoods);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 352);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(641, 91);
            this.panel1.TabIndex = 20;
            // 
            // KdrRadio
            // 
            this.KdrRadio.AutoSize = true;
            this.KdrRadio.Location = new System.Drawing.Point(324, 7);
            this.KdrRadio.Name = "KdrRadio";
            this.KdrRadio.Size = new System.Drawing.Size(71, 17);
            this.KdrRadio.TabIndex = 27;
            this.KdrRadio.Tag = "4";
            this.KdrRadio.Text = "Регионы";
            this.KdrRadio.UseVisualStyleBackColor = true;
            this.KdrRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
            // 
            // NskRadio
            // 
            this.NskRadio.AutoSize = true;
            this.NskRadio.Location = new System.Drawing.Point(211, 7);
            this.NskRadio.Name = "NskRadio";
            this.NskRadio.Size = new System.Drawing.Size(97, 17);
            this.NskRadio.TabIndex = 26;
            this.NskRadio.Tag = "3";
            this.NskRadio.Text = "Новосибирск";
            this.NskRadio.UseVisualStyleBackColor = true;
            this.NskRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
            // 
            // HasDescriptionCheck
            // 
            this.HasDescriptionCheck.AutoSize = true;
            this.HasDescriptionCheck.Location = new System.Drawing.Point(455, 7);
            this.HasDescriptionCheck.Name = "HasDescriptionCheck";
            this.HasDescriptionCheck.Size = new System.Drawing.Size(190, 17);
            this.HasDescriptionCheck.TabIndex = 25;
            this.HasDescriptionCheck.Text = "Без маркетингового описания";
            this.HasDescriptionCheck.UseVisualStyleBackColor = true;
            this.HasDescriptionCheck.CheckedChanged += new System.EventHandler(this.HasDescriptionCheck_CheckedChanged);
            // 
            // SpbRadio
            // 
            this.SpbRadio.AutoSize = true;
            this.SpbRadio.Location = new System.Drawing.Point(86, 7);
            this.SpbRadio.Name = "SpbRadio";
            this.SpbRadio.Size = new System.Drawing.Size(116, 17);
            this.SpbRadio.TabIndex = 24;
            this.SpbRadio.Tag = "2";
            this.SpbRadio.Text = "Санкт-Петербург";
            this.SpbRadio.UseVisualStyleBackColor = true;
            this.SpbRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
            // 
            // MoscowRadio
            // 
            this.MoscowRadio.AutoSize = true;
            this.MoscowRadio.Checked = true;
            this.MoscowRadio.Location = new System.Drawing.Point(16, 7);
            this.MoscowRadio.Name = "MoscowRadio";
            this.MoscowRadio.Size = new System.Drawing.Size(65, 17);
            this.MoscowRadio.TabIndex = 23;
            this.MoscowRadio.TabStop = true;
            this.MoscowRadio.Tag = "1";
            this.MoscowRadio.Text = "Москва";
            this.MoscowRadio.UseVisualStyleBackColor = true;
            this.MoscowRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
            // 
            // ShowPublished
            // 
            this.ShowPublished.AutoSize = true;
            this.ShowPublished.Checked = true;
            this.ShowPublished.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowPublished.Location = new System.Drawing.Point(16, 65);
            this.ShowPublished.Name = "ShowPublished";
            this.ShowPublished.Size = new System.Drawing.Size(238, 17);
            this.ShowPublished.TabIndex = 22;
            this.ShowPublished.Text = "Только товары, показываемые на сайте";
            this.ShowPublished.UseVisualStyleBackColor = true;
            this.ShowPublished.CheckedChanged += new System.EventHandler(this.ShowPublished_CheckedChanged);
            // 
            // RecalcThisCategory
            // 
            this.RecalcThisCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RecalcThisCategory.BackColor = System.Drawing.SystemColors.Control;
            this.RecalcThisCategory.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.RecalcThisCategory.Location = new System.Drawing.Point(313, 31);
            this.RecalcThisCategory.Name = "RecalcThisCategory";
            this.RecalcThisCategory.Size = new System.Drawing.Size(117, 46);
            this.RecalcThisCategory.TabIndex = 21;
            this.RecalcThisCategory.Text = "Пересчитать цены этой категории";
            this.RecalcThisCategory.UseVisualStyleBackColor = false;
            this.RecalcThisCategory.Click += new System.EventHandler(this.RecalcThisCategory_Click);
            // 
            // RecalcPrices
            // 
            this.RecalcPrices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RecalcPrices.BackColor = System.Drawing.SystemColors.Control;
            this.RecalcPrices.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.RecalcPrices.Location = new System.Drawing.Point(436, 31);
            this.RecalcPrices.Name = "RecalcPrices";
            this.RecalcPrices.Size = new System.Drawing.Size(116, 46);
            this.RecalcPrices.TabIndex = 20;
            this.RecalcPrices.Text = "Пересчитать все цены";
            this.RecalcPrices.UseVisualStyleBackColor = false;
            this.RecalcPrices.Click += new System.EventHandler(this.RecalcPrices_Click);
            // 
            // dataGridPager1
            // 
            this.dataGridPager1.AllowAll = true;
            this.dataGridPager1.Batch = 30;
            this.dataGridPager1.Location = new System.Drawing.Point(16, 31);
            this.dataGridPager1.Name = "dataGridPager1";
            this.dataGridPager1.PageCount = 0;
            this.dataGridPager1.PageNum = 1;
            this.dataGridPager1.Size = new System.Drawing.Size(288, 28);
            this.dataGridPager1.TabIndex = 19;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(248, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 443);
            this.splitter1.TabIndex = 22;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tvMasterMos);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(248, 443);
            this.panel2.TabIndex = 23;
            // 
            // formattableBooleanColumn2
            // 
            this.formattableBooleanColumn2.FieldName = "fHasDescr(OID)";
            this.formattableBooleanColumn2.FilterFieldName = null;
            this.formattableBooleanColumn2.HeaderText = "Есть описания";
            this.formattableBooleanColumn2.MappingName = "hasDescr";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn2, this.menuFilterSort1);
            this.formattableBooleanColumn2.Width = 75;
            // 
            // SaveList
            // 
            this.SaveList.DefaultExt = "xls";
            this.SaveList.Filter = "Файлы Excel|*.xls|Все файлы|*.*";
            this.SaveList.Title = "Сохранить список";
            // 
            // formattableTextBoxColumn13
            // 
            this.formattableTextBoxColumn13.FieldName = null;
            this.formattableTextBoxColumn13.FilterFieldName = null;
            this.formattableTextBoxColumn13.Format = "dd.MM.yyyy HH:mm";
            this.formattableTextBoxColumn13.FormatInfo = null;
            this.formattableTextBoxColumn13.HeaderText = "Дата создания";
            this.formattableTextBoxColumn13.MappingName = "dateCreate";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn13, this.menuFilterSort1);
            this.formattableTextBoxColumn13.Width = 150;
            // 
            // ListGoods
            // 
            this.ClientSize = new System.Drawing.Size(892, 443);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel2);
            this.MinimumSize = new System.Drawing.Size(560, 115);
            this.Name = "ListGoods";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список товаров";
            this.Load += new System.EventHandler(this.ListGoods_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private CheckBox ShowPublished;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn3;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn4;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn5;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
		private RadioButton SpbRadio;
		private RadioButton MoscowRadio;
		private CheckBox HasDescriptionCheck;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn10;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn11;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn2;
		private RadioButton NskRadio;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn6;
		private RadioButton KdrRadio;
		private SaveFileDialog SaveList;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn12;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn2;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn9;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn13;
    }
}
