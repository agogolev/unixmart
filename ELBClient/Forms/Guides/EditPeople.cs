﻿using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditNMA.
	/// </summary>
	public partial class EditPeople : EditForm
	{
		public bool HasWholesale { get; set; }

		private const string passString = "*********************";

		public EditPeople()
		{
			InitializeComponent();
			Object = new CPeople();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveObject();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void BindFields()
		{
			tbFirstName.DataBindings.Add("Text", Object, "FirstName");
			tbLastName.DataBindings.Add("Text", Object, "LastName");
			tbMiddleName.DataBindings.Add("Text", Object, "MiddleName");
			tbComment.DataBindings.Add("Text", Object, "SelfComment");
			IDText.DataBindings.Add("BoundProp", Object, "ID");

			if (HasWholesale)
			{
				CompanyText.DataBindings.Add("Text", Object, "Company");
				ZipCodeText.DataBindings.Add("Text", Object, "ZipCode");
				CityText.DataBindings.Add("Text", Object, "City");
				AddressText.DataBindings.Add("Text", Object, "Address");
				JurAddressText.DataBindings.Add("Text", Object, "JurAddress");
				PhoneText.DataBindings.Add("Text", Object, "Phone");
				FaxText.DataBindings.Add("Text", Object, "Fax");
				UrlText.DataBindings.Add("Text", Object, "Url");
				BankText.DataBindings.Add("Text", Object, "Bank");
				BankAccountText.DataBindings.Add("Text", Object, "BankAccount");
				BikText.DataBindings.Add("Text", Object, "Bik");
				InnText.DataBindings.Add("Text", Object, "Inn");
				OkpoText.DataBindings.Add("Text", Object, "Okpo");
				ManagerSelection.DataBindings.Add("BoundProp", Object, "Manager");
			}
			else
			{
				CompanyText.Enabled = false;
				ZipCodeText.Enabled = false;
				CityText.Enabled = false;
				AddressText.Enabled = false;
				JurAddressText.Enabled = false;
				PhoneText.Enabled = false;
				FaxText.Enabled = false;
				UrlText.Enabled = false;
				BankText.Enabled = false;
				BankAccountText.Enabled = false;
				BikText.Enabled = false;
				InnText.Enabled = false;
				OkpoText.Enabled = false;
				ManagerSelection.Enabled = false;
			}
			
			tbLogin.DataBindings.Add("Text", Object, "LoginID");
			tbPass.Text = passString;
			tbPass2.Text = passString;
			tbEmail.DataBindings.Add("Text", Object, "EMail");
		}

		private bool passFirst = true;

		private void tbPass_Enter(object sender, System.EventArgs e)
		{
			if (passFirst)
			{
				tbPass.Text = "";
				tbPass2.Text = "";
				passFirst = false;
			}
		}

		private void fmEditPeople_Load(object sender, System.EventArgs e)
		{
			if(HasWholesale)
				LoadObject("FirstName,LastName,MiddleName,SelfComment,LoginID,EMail,frontPassword,Id,company,zipCode,city,address,jurAddress,phone,fax,url,bank,bankAccount,bik,inn,okpo,manager","Groups");
			else
				LoadObject("FirstName,LastName,MiddleName,SelfComment,LoginID,EMail,frontPassword,Id", "Groups");

			BindFields();
			FillGridGroup();
		}

		private void FillGridGroup()
		{
			addDelDataGrid1.ClassName = "CGroup";
			addDelDataGrid1.ColumnNames = "name";
			addDelDataGrid1.ColumnHeaderNames = "Название";
			addDelDataGrid1.MultiContainer = ((CPeople)Object).Groups;
		}

		protected override void SaveObject()
		{
			if (tbPass.Text != passString)
				((CPeople)Object).FrontPassword = tbPass.Text;

			base.SaveObject();
		}

		protected override string AllowToSave()
		{
			if (tbPass.Text != tbPass2.Text)
				return "Пароли не совпадают!";
			return "";
		}
	}
}
