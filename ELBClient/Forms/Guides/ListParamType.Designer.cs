using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListParamType
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button btnNew;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox txtSearch;
		private ColumnMenuExtender.DataGridISM dgParams;
		private System.Windows.Forms.Button button3;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private System.Data.DataSet dataSet1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ELBClient.UserControls.ListContextMenu listContextMenu1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colParamType;
		private ColumnMenuExtender.FormattableBooleanColumn colIsDomain;
		private ColumnMenuExtender.FormattableTextBoxColumn colUnit;
		private ColumnMenuExtender.FormattableBooleanColumn colIsMandatory;
		private ColumnMenuExtender.FormattableBooleanColumn colIsFullName;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.colParamType = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.colUnit = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colIsDomain = new ColumnMenuExtender.FormattableBooleanColumn();
			this.dgParams = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colIsMandatory = new ColumnMenuExtender.FormattableBooleanColumn();
			this.colIsFullName = new ColumnMenuExtender.FormattableBooleanColumn();
			this.dataSet1 = new System.Data.DataSet();
			this.btnNew = new System.Windows.Forms.Button();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.txtSearch = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dgParams)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// colParamType
			// 
			this.colParamType.FieldName = null;
			this.colParamType.FilterFieldName = null;
			this.colParamType.Format = "";
			this.colParamType.FormatInfo = null;
			this.colParamType.HeaderText = "Тип";
			this.colParamType.MappingName = "paramType";
			this.columnMenuExtender1.SetMenu(this.colParamType, this.menuFilterSort1);
			this.colParamType.Width = 75;
			// 
			// colUnit
			// 
			this.colUnit.FieldName = null;
			this.colUnit.FilterFieldName = null;
			this.colUnit.Format = "";
			this.colUnit.FormatInfo = null;
			this.colUnit.HeaderText = "Ед. изм.";
			this.colUnit.MappingName = "unit";
			this.columnMenuExtender1.SetMenu(this.colUnit, this.menuFilterSort1);
			this.colUnit.Width = 75;
			// 
			// colIsDomain
			// 
			this.colIsDomain.FieldName = null;
			this.colIsDomain.FilterFieldName = null;
			this.colIsDomain.HeaderText = "Список";
			this.colIsDomain.MappingName = "isDomain";
			this.columnMenuExtender1.SetMenu(this.colIsDomain, this.menuFilterSort1);
			this.colIsDomain.Width = 75;
			// 
			// dgParams
			// 
			this.dgParams.AllowSorting = false;
			this.dgParams.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgParams.DataMember = "";
			this.dgParams.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgParams.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgParams.Location = new System.Drawing.Point(0, 46);
			this.dgParams.Name = "dgParams";
			this.dgParams.Order = null;
			this.dgParams.ReadOnly = true;
			this.dgParams.Size = new System.Drawing.Size(608, 329);
			this.dgParams.StockClass = "CParamType";
			this.dgParams.TabIndex = 2;
			this.dgParams.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dgParams, true);
			this.dgParams.Reload += new System.EventHandler(this.dgParams_Reload);
			this.dgParams.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dgParams_ActionKeyPressed);
			this.dgParams.DoubleClick += new System.EventHandler(this.dgParams_DoubleClick);
			this.dgParams.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgParams_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgParams;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colName,
            this.colParamType,
            this.colUnit,
            this.colIsDomain,
            this.colIsMandatory,
            this.colIsFullName});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// colName
			// 
			this.colName.FieldName = null;
			this.colName.FilterFieldName = null;
			this.colName.Format = "";
			this.colName.FormatInfo = null;
			this.colName.HeaderText = "Название";
			this.colName.MappingName = "name";
			this.colName.Width = 150;
			// 
			// colIsMandatory
			// 
			this.colIsMandatory.FieldName = null;
			this.colIsMandatory.FilterFieldName = null;
			this.colIsMandatory.HeaderText = "Обязательность";
			this.colIsMandatory.MappingName = "isMandatory";
			this.colIsMandatory.Width = 150;
			// 
			// colIsFullName
			// 
			this.colIsFullName.FieldName = null;
			this.colIsFullName.FilterFieldName = null;
			this.colIsFullName.HeaderText = "Полное название";
			this.colIsFullName.MappingName = "isFullName";
			this.colIsFullName.Width = 75;
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// btnNew
			// 
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNew.Location = new System.Drawing.Point(528, 9);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(75, 27);
			this.btnNew.TabIndex = 11;
			this.btnNew.Text = "Новый";
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(8, 9);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(280, 28);
			this.dataGridPager1.TabIndex = 12;
			// 
			// listContextMenu1
			// 
			this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
			this.listContextMenu1.NewClick += new System.EventHandler(this.btnNew_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dataGridPager1);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 375);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(608, 46);
			this.panel1.TabIndex = 15;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dgParams);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(608, 375);
			this.panel2.TabIndex = 16;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.button3);
			this.panel3.Controls.Add(this.button2);
			this.panel3.Controls.Add(this.button1);
			this.panel3.Controls.Add(this.txtSearch);
			this.panel3.Controls.Add(this.label1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(608, 46);
			this.panel3.TabIndex = 3;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(360, 8);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 27);
			this.button3.TabIndex = 13;
			this.button3.Text = "Все";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(528, 10);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 27);
			this.button2.TabIndex = 12;
			this.button2.Text = "Закрыть";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(280, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 27);
			this.button1.TabIndex = 2;
			this.button1.Text = "Поиск";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtSearch
			// 
			this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSearch.Location = new System.Drawing.Point(72, 9);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(208, 20);
			this.txtSearch.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "Название:";
			// 
			// ListParamType
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(608, 421);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(550, 404);
			this.Name = "ListParamType";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Шаблоны параметров";
			this.Load += new System.EventHandler(this.fmTheme_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgParams)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
