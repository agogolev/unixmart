﻿using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using MetaData;

namespace ELBClient.Forms.Guides
{
	public partial class EditDefaultTitle : EditForm
	{
		public EditDefaultTitle()
		{
			InitializeComponent();
			Object = new CDefaultTitle();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch{}
			//this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void BindFields()
		{
            NameText.DataBindings.Add("Text", Object, "Name");
            TitleText.DataBindings.Add("Text", Object, "Title");
            KeywordsText.DataBindings.Add("Text", Object, "Keywords");
            PageDescriptionText.DataBindings.Add("Text", Object, "PageDescription");
            DefaultTypeSelect.DataBindings.Add("SelectedValue", Object, "DefaultType");
            IsActiveCheck.DataBindings.Add("Checked", Object, "IsActive");
            Category.DataBindings.Add("BoundProp", Object, "Theme");
        }

        private void fmEditTheme_Load(object sender, System.EventArgs e)
		{
            LoadLookup();
			LoadObject("name,defaultType,title,keywords,pageDescription,isActive,theme", null);
			BindFields();
		}

        private void LoadLookup()
        {
            string xml = lp.GetLookupValues("CDefaultTitle");
            DefaultTypeSelect.LoadXml(xml, "defaultTypeN");
        }

        private void Category_ChooseClick(object sender, System.EventArgs e)
        {
            var fm = new TreeObjectDialog { Owner = MdiParent, RootNode = 22427, CanAssignAny = true };
            if (fm.ShowDialog(this) != DialogResult.OK || fm.SelectedObject == null) return;
            ((CDefaultTitle) Object).Theme = fm.SelectedObject;
            Category.BoundProp = fm.SelectedObject;
        }

        private void Category_DelClick(object sender, System.EventArgs e)
        {
            ((CDefaultTitle) Object).Theme = ObjectItem.Empty;
            Category.BoundProp = ObjectItem.Empty;
        }
    }
}
