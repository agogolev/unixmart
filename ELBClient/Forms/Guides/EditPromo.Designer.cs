using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditPromo
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Panel pnlMain;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private ELBClient.UserControls.SelectorEdtTextBox stbImage;
		private System.Windows.Forms.TextBox tbUrl;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TextBox tbDescription;
		private ELBClient.UserControls.SelectorEdtTextBox stbArticle;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private ColumnMenuExtender.DataBoundTextBox txtPrice;
		private System.Windows.Forms.CheckBox cbImageOnly;
		private WebBrowser webBrowser1;
		private System.Windows.Forms.Label label1;

		private void InitializeComponent()
		{
			this.pnlMain = new System.Windows.Forms.Panel();
			this.cbImageOnly = new System.Windows.Forms.CheckBox();
			this.txtPrice = new ColumnMenuExtender.DataBoundTextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.stbArticle = new ELBClient.UserControls.SelectorEdtTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbUrl = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.stbImage = new ELBClient.UserControls.SelectorEdtTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.webBrowser1 = new System.Windows.Forms.WebBrowser();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tbDescription = new System.Windows.Forms.TextBox();
			this.pnlMain.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlMain
			// 
			this.pnlMain.Controls.Add(this.cbImageOnly);
			this.pnlMain.Controls.Add(this.txtPrice);
			this.pnlMain.Controls.Add(this.label6);
			this.pnlMain.Controls.Add(this.label5);
			this.pnlMain.Controls.Add(this.stbArticle);
			this.pnlMain.Controls.Add(this.label2);
			this.pnlMain.Controls.Add(this.tbUrl);
			this.pnlMain.Controls.Add(this.label1);
			this.pnlMain.Controls.Add(this.stbImage);
			this.pnlMain.Controls.Add(this.label3);
			this.pnlMain.Controls.Add(this.tbName);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlMain.Location = new System.Drawing.Point(0, 0);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(530, 185);
			this.pnlMain.TabIndex = 3;
			// 
			// cbImageOnly
			// 
			this.cbImageOnly.Location = new System.Drawing.Point(192, 155);
			this.cbImageOnly.Name = "cbImageOnly";
			this.cbImageOnly.Size = new System.Drawing.Size(184, 27);
			this.cbImageOnly.TabIndex = 22;
			this.cbImageOnly.Text = "только картинка";
			// 
			// txtPrice
			// 
			this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPrice.BoundProp = null;
			this.txtPrice.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtPrice.IsCurrency = true;
			this.txtPrice.Location = new System.Drawing.Point(88, 157);
			this.txtPrice.Name = "txtPrice";
			this.txtPrice.Size = new System.Drawing.Size(100, 20);
			this.txtPrice.TabIndex = 5;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 157);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 18);
			this.label6.TabIndex = 21;
			this.label6.Text = "Цена:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 88);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 18);
			this.label5.TabIndex = 20;
			this.label5.Text = "Статья:";
			// 
			// stbArticle
			// 
			this.stbArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.stbArticle.BoundProp = null;
			this.stbArticle.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.stbArticle.ClassName = "CArticle";
			this.stbArticle.FieldNames = null;
			this.stbArticle.FormatRows = null;
			this.stbArticle.HeaderNames = "Название";
			this.stbArticle.Location = new System.Drawing.Point(88, 83);
			this.stbArticle.Name = "stbArticle";
			this.stbArticle.RowNames = "header";
			this.stbArticle.Size = new System.Drawing.Size(432, 28);
			this.stbArticle.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 18);
			this.label2.TabIndex = 18;
			this.label2.Text = "URL:";
			// 
			// tbUrl
			// 
			this.tbUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbUrl.Location = new System.Drawing.Point(88, 48);
			this.tbUrl.Name = "tbUrl";
			this.tbUrl.Size = new System.Drawing.Size(434, 20);
			this.tbUrl.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 125);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 18);
			this.label1.TabIndex = 16;
			this.label1.Text = "Картинка:";
			// 
			// stbImage
			// 
			this.stbImage.BoundProp = null;
			this.stbImage.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.stbImage.ClassName = "CBinaryData";
			this.stbImage.FieldNames = null;
			this.stbImage.FormatRows = null;
			this.stbImage.HeaderNames = "Название";
			this.stbImage.Location = new System.Drawing.Point(88, 120);
			this.stbImage.Name = "stbImage";
			this.stbImage.RowNames = "Name";
			this.stbImage.Size = new System.Drawing.Size(320, 28);
			this.stbImage.TabIndex = 4;
			this.stbImage.Changed += new System.EventHandler(this.stbImage_Changed);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 21);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 18);
			this.label3.TabIndex = 14;
			this.label3.Text = "Название:";
			// 
			// tbName
			// 
			this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(88, 18);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(434, 20);
			this.tbName.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(448, 9);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 2;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 334);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(530, 47);
			this.panel1.TabIndex = 4;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tabControl1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 185);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(530, 149);
			this.panel2.TabIndex = 5;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(530, 149);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.webBrowser1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(522, 123);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Картинка";
			// 
			// webBrowser1
			// 
			this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.webBrowser1.Location = new System.Drawing.Point(0, 0);
			this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new System.Drawing.Size(522, 123);
			this.webBrowser1.TabIndex = 0;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.tbDescription);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(522, 183);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Описание";
			// 
			// tbDescription
			// 
			this.tbDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbDescription.Location = new System.Drawing.Point(0, 0);
			this.tbDescription.Multiline = true;
			this.tbDescription.Name = "tbDescription";
			this.tbDescription.Size = new System.Drawing.Size(522, 183);
			this.tbDescription.TabIndex = 0;
			// 
			// EditPromo
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(530, 381);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pnlMain);
			this.Name = "EditPromo";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование промо-блока";
			this.Load += new System.EventHandler(this.fmEditPromo_Load);
			this.pnlMain.ResumeLayout(false);
			this.pnlMain.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
