﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MetaData;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides {
	/// <summary>
	/// Summary description for fmListParamType.
	/// </summary>
	public partial class ListParamType : ListForm
	{
		private bool IsDialog {
			get {
				return this.MdiParent == null;
			}
		}
		public Guid SelectedOID {
			get {
				DataRow dr = dgParams.GetSelectedRow();
				return dr == null ? Guid.Empty : (Guid)dr["OID"];
			}
		}
		public string SelectedName {
			get {
				DataRow dr = dgParams.GetSelectedRow();
				return dr == null ? "" : (string)dr["name"];
			}
		}
		public object ParamType {
			get {
				DataRow dr = dgParams.GetSelectedRow();
				return dr == null  ? DBNull.Value : dr["paramType"];
			}
		}
		//public string Unit {
		//  get {
		//    DataRow dr = dgParams.GetSelectedRow();
		//    return dr == null ? "" : (string)dr["unit"];
		//  }
		//}
		//public bool IsDomain {
		//  get {
		//    DataRow dr = dgParams.GetSelectedRow();
		//    return dr == null ? false : (bool)dr["isDomain"];
		//  }
		//}
		//public bool IsMandatory
		//{
		//  get
		//  {
		//    DataRow dr = dgParams.GetSelectedRow();
		//    return dr == null ? false : (bool)dr["IsMandatory"];
		//  }
		//}
		//public bool IsFullName {
		//  get {
		//    DataRow dr = dgParams.GetSelectedRow();
		//    return dr == null ? false : (bool)dr["IsFullName"];
		//  }
		//}

		

		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ListParamType() 
		{
			InitializeComponent();
		}

		private void LoadLookup() {
			string xml = lp.GetLookupValues("CParamType");
			colParamType.ComboBox.LoadXml(xml, "paramTypeN");
		}
		public override void LoadData() 
		{
			if (txtSearch.Text.Trim() != "") {
				string searchName = "*"+txtSearch.Text.Trim()+"*";
				dgParams.CreateAuxFilter("name", "name_filter", FilterVerb.Like, false, new object[]{searchName});
			}
			else {
				if (dgParams.Filters.Count != 0) dgParams.Filters.Clear();
			}
			dataSet1 = lp.GetList(dgParams.GetDataXml().OuterXml);

			int cri = dgParams.CurrentRowIndex;
			dgParams.SetDataBinding(dataSet1, "table");
			
			DataTable dt = dataSet1.Tables["table"];
			if (cri == -1) 
			{
				if (dt.Rows.Count > 0) 
					cri = 0;
			}
			else if (cri > dt.Rows.Count-1) 
				cri = dt.Rows.Count-1;
			if (cri >= 0)
			{
				dgParams.CurrentRowIndex = cri;
				dgParams.Select(cri);
			}

			// корявый способ избавится от дизабленных скролбаров :)
			dgParams.Width++;dgParams.Width--;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void dgParams_Reload(object sender, System.EventArgs e) 
		{
			LoadData();
		}
		
		private void fmTheme_Load(object sender, System.EventArgs e) 
		{
			LoadLookup();
			LoadData();
			dataGridPager1.BindToDataGrid(dgParams);
			listContextMenu1.Bind(this);
		}

		private void btnNew_Click(object sender, System.EventArgs e) 
		{
			showEditForm(Guid.Empty);
		}

		private void listContextMenu1_EditClick(object sender, System.EventArgs e) 
		{
			showEditForm(listContextMenu1.SelectedOID);
		}

		private void showEditForm(Guid OID) 
		{
			EditParamType form = new EditParamType();
			form.ObjectOID = OID;
			if(!IsDialog) {
				form.MdiParent = this.MdiParent;
				form.ReloadGrid += new ReloadDelegate(LoadData);
				form.Show();
			}
			else {
				form.ReloadGrid += new ReloadDelegate(LoadData);
				form.ShowDialog(this);
			}
		}


		private void dgParams_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e) 
		{
			if (e.Button == MouseButtons.Right) 
			{
				DataRow dr = null;
				if ((dr = dgParams.GetSelectedRow()) != null) 
				{
					listContextMenu1.SelectedOID = (Guid)dr["OID"];
					listContextMenu1.Show(dgParams, new Point(e.X, e.Y));
				}
			}
		}

		private void dgParams_DoubleClick(object sender, System.EventArgs e)
		{
			System.Drawing.Point pt = dgParams.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dgParams.HitTest(pt); 
			if(hti.Type == DataGrid.HitTestType.Cell) 
			{
				DataRow dr = dgParams.GetSelectedRow();
				if(!IsDialog) {
					showEditForm((Guid)dr["OID"]);
				}
				else {
					DialogResult = DialogResult.OK;
				}
			}
		}

		private void dgParams_ActionKeyPressed(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Insert)
			{
				showEditForm(Guid.Empty);
			}
			else if (e.KeyCode == Keys.Delete)
			{
				DataTable dt = TableInGrid.GetTable(dgParams);
				Form form = null;
				if (this.Parent is Form)
					form = this.Parent as Form;
				listContextMenu1.DeleteObject(form, (Guid)dt.Rows[dgParams.CurrentRowIndex]["OID"]);
			}
			else if (e.KeyCode == Keys.Enter)
			{
				DataTable dt = TableInGrid.GetTable(dgParams);
				showEditForm((Guid)dt.Rows[dgParams.CurrentRowIndex]["OID"]);
			}
		}

		private void button1_Click(object sender, System.EventArgs e) {
			if(txtSearch.Text.Trim() != "") {
				LoadData();
			}
		}

		private void button3_Click(object sender, System.EventArgs e) {
			txtSearch.Text = "";
			LoadData();
		}
	}
}
