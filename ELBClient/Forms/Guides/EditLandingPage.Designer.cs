using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
    public partial class EditLandingPage
    {
        #region Windows Form Designer generated code
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private ColumnMenuExtender.DataBoundTextBox NameText;
        private TextBox ShortLinkText;

        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.NameText = new ColumnMenuExtender.DataBoundTextBox();
            this.ShortLinkText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CommodityGroupSelect = new ELBClient.UserControls.SelectorEdtTextBox();
            this.TranslitButton = new System.Windows.Forms.Button();
            this.ArticleSelector = new ELBClient.UserControls.SelectorEdtTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ActualGoodsCountText = new ColumnMenuExtender.DataBoundTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPageDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.H1Text = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 19);
            this.label1.TabIndex = 16;
            this.label1.Text = "Название:";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(8, 347);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(969, 347);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 27);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Красивая ссылка:";
            // 
            // NameText
            // 
            this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameText.BoundProp = null;
            this.NameText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameText.IsCurrency = false;
            this.NameText.IsHinted = true;
            this.NameText.Location = new System.Drawing.Point(111, 9);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(930, 20);
            this.NameText.TabIndex = 0;
            // 
            // ShortLinkText
            // 
            this.ShortLinkText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShortLinkText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ShortLinkText.Location = new System.Drawing.Point(111, 46);
            this.ShortLinkText.Name = "ShortLinkText";
            this.ShortLinkText.Size = new System.Drawing.Size(829, 20);
            this.ShortLinkText.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(5, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 59;
            this.label7.Text = "Группа:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CommodityGroupSelect
            // 
            this.CommodityGroupSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CommodityGroupSelect.BoundProp = null;
            this.CommodityGroupSelect.ButtonBackColor = System.Drawing.SystemColors.Control;
            this.CommodityGroupSelect.ClassName = "CCommodityGroup";
            this.CommodityGroupSelect.FieldNames = "Name";
            this.CommodityGroupSelect.FormatRows = null;
            this.CommodityGroupSelect.HeaderNames = "Название";
            this.CommodityGroupSelect.Location = new System.Drawing.Point(111, 115);
            this.CommodityGroupSelect.Name = "CommodityGroupSelect";
            this.CommodityGroupSelect.RowNames = "Name";
            this.CommodityGroupSelect.Size = new System.Drawing.Size(929, 28);
            this.CommodityGroupSelect.TabIndex = 60;
            this.CommodityGroupSelect.BeforeEdit += new System.EventHandler(this.CommodityGroupSelect_BeforeEdit);
            // 
            // TranslitButton
            // 
            this.TranslitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TranslitButton.Location = new System.Drawing.Point(937, 44);
            this.TranslitButton.Name = "TranslitButton";
            this.TranslitButton.Size = new System.Drawing.Size(104, 24);
            this.TranslitButton.TabIndex = 61;
            this.TranslitButton.Text = "Транслитерация";
            this.TranslitButton.Click += new System.EventHandler(this.TranslitButton_Click);
            // 
            // ArticleSelector
            // 
            this.ArticleSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ArticleSelector.BoundProp = null;
            this.ArticleSelector.ButtonBackColor = System.Drawing.SystemColors.Control;
            this.ArticleSelector.ClassName = "CArticle";
            this.ArticleSelector.FieldNames = "header,pubDate";
            this.ArticleSelector.FormatRows = null;
            this.ArticleSelector.HeaderNames = "Название,Дата публикации";
            this.ArticleSelector.Location = new System.Drawing.Point(111, 149);
            this.ArticleSelector.Name = "ArticleSelector";
            this.ArticleSelector.RowNames = "Name";
            this.ArticleSelector.Size = new System.Drawing.Size(929, 28);
            this.ArticleSelector.TabIndex = 63;
            this.ArticleSelector.BeforeEdit += new System.EventHandler(this.ArticleSelector_BeforeEdit);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 18);
            this.label2.TabIndex = 62;
            this.label2.Text = "СЕО текст:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ActualGoodsCountText
            // 
            this.ActualGoodsCountText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ActualGoodsCountText.BoundProp = null;
            this.ActualGoodsCountText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ActualGoodsCountText.IsCurrency = false;
            this.ActualGoodsCountText.IsHinted = true;
            this.ActualGoodsCountText.Location = new System.Drawing.Point(110, 183);
            this.ActualGoodsCountText.Name = "ActualGoodsCountText";
            this.ActualGoodsCountText.Size = new System.Drawing.Size(130, 20);
            this.ActualGoodsCountText.TabIndex = 64;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(7, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 19);
            this.label4.TabIndex = 65;
            this.label4.Text = "Товаров в группе:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(4, 276);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 71;
            this.label6.Text = "Description:";
            // 
            // txtPageDescription
            // 
            this.txtPageDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPageDescription.Location = new System.Drawing.Point(111, 273);
            this.txtPageDescription.Name = "txtPageDescription";
            this.txtPageDescription.Size = new System.Drawing.Size(929, 20);
            this.txtPageDescription.TabIndex = 68;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(4, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 18);
            this.label5.TabIndex = 70;
            this.label5.Text = "Keywords:";
            // 
            // txtKeywords
            // 
            this.txtKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeywords.Location = new System.Drawing.Point(111, 246);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(929, 20);
            this.txtKeywords.TabIndex = 67;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(4, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 19);
            this.label8.TabIndex = 69;
            this.label8.Text = "Title:";
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.Location = new System.Drawing.Point(111, 218);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(929, 20);
            this.txtTitle.TabIndex = 66;
            // 
            // H1Text
            // 
            this.H1Text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.H1Text.BorderStyle = BorderStyle.FixedSingle;
            this.H1Text.Location = new Point(110, 80);
            this.H1Text.Name = "H1Text";
            this.H1Text.Size = new System.Drawing.Size(930, 20);
            this.H1Text.TabIndex = 72;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(7, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 19);
            this.label9.TabIndex = 73;
            this.label9.Text = "H1:";
            // 
            // EditLandingPage
            // 
            this.ClientSize = new Size(1059, 383);
            this.Controls.Add(this.H1Text);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPageDescription);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtKeywords);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.ActualGoodsCountText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ArticleSelector);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TranslitButton);
            this.Controls.Add(this.CommodityGroupSelect);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ShortLinkText);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(464, 120);
            this.Name = "EditLandingPage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Редактирование страницы";
            this.Load += new System.EventHandler(this.EditLandingPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        private Label label7;
        private UserControls.SelectorEdtTextBox CommodityGroupSelect;
        private Button TranslitButton;
        private UserControls.SelectorEdtTextBox ArticleSelector;
        private Label label2;
        private ColumnMenuExtender.DataBoundTextBox ActualGoodsCountText;
        private Label label4;
        private Label label6;
        private TextBox txtPageDescription;
        private Label label5;
        private TextBox txtKeywords;
        private Label label8;
        private TextBox txtTitle;
        private TextBox H1Text;
        private Label label9;
    }
}
