using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListMasterCatalog
	{
		#region Windows Form Designer generated code
		private ColumnMenuExtender.DataGridISM dataGridISM1;
		private System.Windows.Forms.Button btnNew;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private System.Data.DataSet dataSet1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ELBClient.UserControls.ListContextMenu listContextMenu1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
            this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.dataGridISM1 = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.dataSet1 = new System.Data.DataSet();
            this.btnNew = new System.Windows.Forms.Button();
            this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
            this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Название";
            this.formattableTextBoxColumn1.MappingName = "name";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
            this.formattableTextBoxColumn1.Width = 150;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.AllowExpand = true;
            this.formattableTextBoxColumn3.FieldName = "GetFullTheme(OID, 22464)";
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Путь";
            this.formattableTextBoxColumn3.MappingName = "FullTheme";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // dataGridISM1
            // 
            this.dataGridISM1.AllowSorting = false;
            this.dataGridISM1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridISM1.DataMember = "";
            this.dataGridISM1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridISM1.FilterString = null;
            this.dataGridISM1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridISM1.Location = new System.Drawing.Point(0, 0);
            this.dataGridISM1.Name = "dataGridISM1";
            this.dataGridISM1.Order = null;
            this.dataGridISM1.ReadOnly = true;
            this.dataGridISM1.Size = new System.Drawing.Size(542, 328);
            this.dataGridISM1.StockClass = "CMasterCatalog";
            this.dataGridISM1.TabIndex = 2;
            this.dataGridISM1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.columnMenuExtender1.SetUseGridMenu(this.dataGridISM1, true);
            this.dataGridISM1.Reload += new System.EventHandler(this.dataGridISM1_Reload);
            this.dataGridISM1.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dataGridISM1_ActionKeyPressed);
            this.dataGridISM1.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
            this.dataGridISM1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.dataGridISM1;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn2});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnNew.Location = new System.Drawing.Point(462, 9);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 27);
            this.btnNew.TabIndex = 11;
            this.btnNew.Text = "Новый";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // dataGridPager1
            // 
            this.dataGridPager1.AllowAll = true;
            this.dataGridPager1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridPager1.Batch = 30;
            this.dataGridPager1.Location = new System.Drawing.Point(8, 9);
            this.dataGridPager1.Name = "dataGridPager1";
            this.dataGridPager1.PageCount = 0;
            this.dataGridPager1.PageNum = 1;
            this.dataGridPager1.Size = new System.Drawing.Size(280, 28);
            this.dataGridPager1.TabIndex = 12;
            // 
            // listContextMenu1
            // 
            this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
            this.listContextMenu1.NewClick += new System.EventHandler(this.btnNew_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridPager1);
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 328);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(542, 46);
            this.panel1.TabIndex = 15;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridISM1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(542, 328);
            this.panel2.TabIndex = 16;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "id";
            this.formattableTextBoxColumn2.MappingName = "objectID";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // ListMasterCatalog
            // 
            this.ClientSize = new System.Drawing.Size(542, 374);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(550, 404);
            this.Name = "ListMasterCatalog";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Категории мастер каталога";
            this.Load += new System.EventHandler(this.fmListMasterCatalog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
	}
}
