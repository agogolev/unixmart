﻿namespace ELBClient.Forms.Guides
{
    partial class BrowseDefaultTitle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CatalogTreeView = new System.Windows.Forms.TreeView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnNew = new System.Windows.Forms.Button();
            this.DefaultTypeSelect = new ColumnMenuExtender.DataBoundComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TitlesGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultTypeSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TitlesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // CatalogTreeView
            // 
            this.CatalogTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.CatalogTreeView.HideSelection = false;
            this.CatalogTreeView.Location = new System.Drawing.Point(0, 0);
            this.CatalogTreeView.Name = "CatalogTreeView";
            this.CatalogTreeView.Size = new System.Drawing.Size(318, 518);
            this.CatalogTreeView.TabIndex = 1;
            this.CatalogTreeView.Tag = "1";
            this.CatalogTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.CatalogTreeView_AfterSelect);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(318, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 518);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnNew);
            this.panel1.Controls.Add(this.DefaultTypeSelect);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(321, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(757, 56);
            this.panel1.TabIndex = 3;
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.Location = new System.Drawing.Point(344, 16);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 33;
            this.btnNew.Text = "Новый";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // DefaultTypeSelect
            // 
            this.DefaultTypeSelect.DefaultItemsCountInDropDown = 10;
            this.DefaultTypeSelect.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DefaultTypeSelect.Location = new System.Drawing.Point(133, 17);
            this.DefaultTypeSelect.Name = "DefaultTypeSelect";
            this.DefaultTypeSelect.SelectedValue = -1;
            this.DefaultTypeSelect.Size = new System.Drawing.Size(205, 20);
            this.DefaultTypeSelect.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(13, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 18);
            this.label4.TabIndex = 32;
            this.label4.Text = "Для какого типа:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TitlesGrid
            // 
            this.TitlesGrid.BackgroundColor = System.Drawing.Color.White;
            this.TitlesGrid.DataMember = "";
            this.TitlesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TitlesGrid.FilterString = null;
            this.TitlesGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.TitlesGrid.Location = new System.Drawing.Point(321, 56);
            this.TitlesGrid.Name = "TitlesGrid";
            this.TitlesGrid.Order = null;
            this.TitlesGrid.Size = new System.Drawing.Size(757, 462);
            this.TitlesGrid.StockClass = "CDefaultTitle";
            this.TitlesGrid.TabIndex = 4;
            this.TitlesGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.TitlesGrid.DoubleClick += new System.EventHandler(this.TitlesGrid_DoubleClick);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.AllowSorting = false;
            this.extendedDataGridTableStyle1.DataGrid = this.TitlesGrid;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn3,
            this.formattableBooleanColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Название";
            this.formattableTextBoxColumn1.MappingName = "name";
            this.formattableTextBoxColumn1.NullText = "";
            this.formattableTextBoxColumn1.Width = 150;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = "defaultType?name";
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Для чего";
            this.formattableTextBoxColumn3.MappingName = "defaultTypeName";
            this.formattableTextBoxColumn3.NullText = "";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableBooleanColumn1
            // 
            this.formattableBooleanColumn1.FieldName = null;
            this.formattableBooleanColumn1.FilterFieldName = null;
            this.formattableBooleanColumn1.HeaderText = "Активность";
            this.formattableBooleanColumn1.MappingName = "isActive";
            this.formattableBooleanColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Title";
            this.formattableTextBoxColumn2.MappingName = "title";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Keywords";
            this.formattableTextBoxColumn4.MappingName = "keywords";
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "Description";
            this.formattableTextBoxColumn5.MappingName = "pageDescription";
            this.formattableTextBoxColumn5.Width = 75;
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = "theme?themeName";
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "Категория";
            this.formattableTextBoxColumn6.MappingName = "themeName";
            this.formattableTextBoxColumn6.Width = 150;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = null;
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "dd.MM.yyyy HH:mm";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "Создан";
            this.formattableTextBoxColumn7.MappingName = "dateCreate";
            this.formattableTextBoxColumn7.Width = 75;
            // 
            // BrowseDefaultTitle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 518);
            this.Controls.Add(this.TitlesGrid);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.CatalogTreeView);
            this.Name = "BrowseDefaultTitle";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Список тайтлов по умолчанию";
            this.BeforeClosing += new System.EventHandler(this.BrowseDefaultTitle_BeforeClosing);
            this.Load += new System.EventHandler(this.BrowseDefaultTitle_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultTypeSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TitlesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView CatalogTreeView;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private ColumnMenuExtender.DataBoundComboBox DefaultTypeSelect;
        private System.Windows.Forms.Label label4;
        private ColumnMenuExtender.DataGridISM TitlesGrid;
        private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
        private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
        public System.Windows.Forms.Button btnNew;
    }
}