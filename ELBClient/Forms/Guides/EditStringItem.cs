﻿using System;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;

namespace ELBClient.Forms.Guides
{
	public partial class EditStringItem : EditForm
	{
		private ObjectProvider.ObjectProvider objectProvider = ServiceUtility.ObjectProvider;

		
		
		
		
		

		private string _objectName = "";
		public string ObjectName
		{
			get
			{
				return _objectName;
			}
			set
			{
				_objectName = value;
			}
		}
		private ObjectItem initParamType;
		public ObjectItem ParamType {
			get { 
				return this.initParamType; 
			} 
			set { 
				this.initParamType = value; 
			}
		}

		public EditStringItem()
		{
			InitializeComponent();
			Object = new CStringItem();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
				if(this.Modal) DialogResult = DialogResult.OK;
			}
			catch{}
			//this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void BindFields()
		{
			tbName.DataBindings.Add("BoundProp", Object, "Value");
		}

		private void fmEditName_Load(object sender, System.EventArgs e)
		{
			if(ObjectOID != Guid.Empty) {
				LoadObject("value,paramType", null);
			}
			else {
				(Object as CStringItem).ParamType = initParamType;
			}
			BindFields();
		}
	}
}
