using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListGoodsItems
	{
		private System.Windows.Forms.Panel panel3;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridISM dgGoods;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ContextMenu treeMenu;
		private System.Windows.Forms.MenuItem miUpdateTree;
		private System.Windows.Forms.TreeView tvMasterMos;
		private Panel panel2;
		private System.Windows.Forms.Splitter splitter1;
		private System.Data.DataSet dataSet1;
		private ELBClient.UserControls.ListContextMenu listContextMenu1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private System.ComponentModel.IContainer components;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListGoodsItems));
			this.dgGoods = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn11 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.dataSet1 = new System.Data.DataSet();
			this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
			this.tvMasterMos = new System.Windows.Forms.TreeView();
			this.treeMenu = new System.Windows.Forms.ContextMenu();
			this.miUpdateTree = new System.Windows.Forms.MenuItem();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.KdrRadio = new System.Windows.Forms.RadioButton();
			this.NskRadio = new System.Windows.Forms.RadioButton();
			this.HasDescriptionCheck = new System.Windows.Forms.CheckBox();
			this.SpbRadio = new System.Windows.Forms.RadioButton();
			this.MoscowRadio = new System.Windows.Forms.RadioButton();
			this.ShowPublished = new System.Windows.Forms.CheckBox();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel2 = new System.Windows.Forms.Panel();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.formattableBooleanColumn2 = new ColumnMenuExtender.FormattableBooleanColumn();
			this.SaveList = new System.Windows.Forms.SaveFileDialog();
			this.formattableTextBoxColumn13 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableBooleanColumn7 = new ColumnMenuExtender.FormattableBooleanColumn();
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel3.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dgGoods
			// 
			this.dgGoods.AllowSorting = false;
			this.dgGoods.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgGoods.ColumnDragEnabled = true;
			this.dgGoods.DataMember = "";
			this.dgGoods.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgGoods.FilterString = null;
			this.dgGoods.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgGoods.Location = new System.Drawing.Point(0, 0);
			this.dgGoods.Name = "dgGoods";
			this.dgGoods.Order = null;
			this.dgGoods.ReadOnly = true;
			this.dgGoods.Size = new System.Drawing.Size(641, 352);
			this.dgGoods.StockClass = "CGoods";
			this.dgGoods.TabIndex = 15;
			this.dgGoods.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dgGoods, true);
			this.dgGoods.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dgGoods.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dataGridISM1_ActionKeyPressed);
			this.dgGoods.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			this.dgGoods.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dgGoods;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn13,
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7,
            this.formattableTextBoxColumn11,
            this.formattableBooleanColumn7});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.PreferredColumnWidth = 100;
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Код поставщика";
			this.formattableTextBoxColumn2.MappingName = "postavshik_id";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Модель";
			this.formattableTextBoxColumn1.MappingName = "model";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = "NK(category)";
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "Категория";
			this.formattableTextBoxColumn4.MappingName = "categoryName";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn4, this.menuFilterSort1);
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// formattableTextBoxColumn6
			// 
			this.formattableTextBoxColumn6.FieldName = "NK(manufacturer)";
			this.formattableTextBoxColumn6.FilterFieldName = null;
			this.formattableTextBoxColumn6.Format = "";
			this.formattableTextBoxColumn6.FormatInfo = null;
			this.formattableTextBoxColumn6.HeaderText = "Производитель";
			this.formattableTextBoxColumn6.MappingName = "manufacturer";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
			this.formattableTextBoxColumn6.Width = 75;
			// 
			// formattableTextBoxColumn7
			// 
			this.formattableTextBoxColumn7.FieldName = "items.price";
			this.formattableTextBoxColumn7.FilterFieldName = null;
			this.formattableTextBoxColumn7.Format = "#,0.##";
			this.formattableTextBoxColumn7.FormatInfo = null;
			this.formattableTextBoxColumn7.HeaderText = "Цена на сайте";
			this.formattableTextBoxColumn7.MappingName = "price";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
			this.formattableTextBoxColumn7.Width = 75;
			// 
			// formattableTextBoxColumn11
			// 
			this.formattableTextBoxColumn11.FieldName = null;
			this.formattableTextBoxColumn11.FilterFieldName = null;
			this.formattableTextBoxColumn11.Format = "";
			this.formattableTextBoxColumn11.FormatInfo = null;
			this.formattableTextBoxColumn11.HeaderText = "Тип товара";
			this.formattableTextBoxColumn11.MappingName = "productType";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn11, this.menuFilterSort1);
			this.formattableTextBoxColumn11.Width = 50;
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "");
			this.imageList1.Images.SetKeyName(1, "");
			this.imageList1.Images.SetKeyName(2, "");
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// listContextMenu1
			// 
			this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
			this.listContextMenu1.GetList += new System.EventHandler(this.listContextMenu1_GetList);
			this.listContextMenu1.NewClick += new System.EventHandler(this.btnNew_Click);
			// 
			// tvMasterMos
			// 
			this.tvMasterMos.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvMasterMos.HideSelection = false;
			this.tvMasterMos.Location = new System.Drawing.Point(0, 0);
			this.tvMasterMos.Name = "tvMasterMos";
			this.tvMasterMos.Size = new System.Drawing.Size(248, 443);
			this.tvMasterMos.TabIndex = 0;
			this.tvMasterMos.Tag = "1";
			this.tvMasterMos.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMaster_AfterSelect);
			// 
			// treeMenu
			// 
			this.treeMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miUpdateTree});
			// 
			// miUpdateTree
			// 
			this.miUpdateTree.Index = 0;
			this.miUpdateTree.Text = "Обновить";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.dgGoods);
			this.panel3.Controls.Add(this.panel1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(251, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(641, 443);
			this.panel3.TabIndex = 20;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.KdrRadio);
			this.panel1.Controls.Add(this.NskRadio);
			this.panel1.Controls.Add(this.HasDescriptionCheck);
			this.panel1.Controls.Add(this.SpbRadio);
			this.panel1.Controls.Add(this.MoscowRadio);
			this.panel1.Controls.Add(this.ShowPublished);
			this.panel1.Controls.Add(this.dataGridPager1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 352);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(641, 91);
			this.panel1.TabIndex = 20;
			// 
			// KdrRadio
			// 
			this.KdrRadio.AutoSize = true;
			this.KdrRadio.Location = new System.Drawing.Point(324, 7);
			this.KdrRadio.Name = "KdrRadio";
			this.KdrRadio.Size = new System.Drawing.Size(71, 17);
			this.KdrRadio.TabIndex = 27;
			this.KdrRadio.Tag = "4";
			this.KdrRadio.Text = "Регионы";
			this.KdrRadio.UseVisualStyleBackColor = true;
			this.KdrRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
			// 
			// NskRadio
			// 
			this.NskRadio.AutoSize = true;
			this.NskRadio.Location = new System.Drawing.Point(211, 7);
			this.NskRadio.Name = "NskRadio";
			this.NskRadio.Size = new System.Drawing.Size(97, 17);
			this.NskRadio.TabIndex = 26;
			this.NskRadio.Tag = "3";
			this.NskRadio.Text = "Новосибирск";
			this.NskRadio.UseVisualStyleBackColor = true;
			this.NskRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
			// 
			// HasDescriptionCheck
			// 
			this.HasDescriptionCheck.AutoSize = true;
			this.HasDescriptionCheck.Location = new System.Drawing.Point(455, 7);
			this.HasDescriptionCheck.Name = "HasDescriptionCheck";
			this.HasDescriptionCheck.Size = new System.Drawing.Size(190, 17);
			this.HasDescriptionCheck.TabIndex = 25;
			this.HasDescriptionCheck.Text = "Без маркетингового описания";
			this.HasDescriptionCheck.UseVisualStyleBackColor = true;
			this.HasDescriptionCheck.CheckedChanged += new System.EventHandler(this.HasDescriptionCheck_CheckedChanged);
			// 
			// SpbRadio
			// 
			this.SpbRadio.AutoSize = true;
			this.SpbRadio.Location = new System.Drawing.Point(86, 7);
			this.SpbRadio.Name = "SpbRadio";
			this.SpbRadio.Size = new System.Drawing.Size(116, 17);
			this.SpbRadio.TabIndex = 24;
			this.SpbRadio.Tag = "2";
			this.SpbRadio.Text = "Санкт-Петербург";
			this.SpbRadio.UseVisualStyleBackColor = true;
			this.SpbRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
			// 
			// MoscowRadio
			// 
			this.MoscowRadio.AutoSize = true;
			this.MoscowRadio.Checked = true;
			this.MoscowRadio.Location = new System.Drawing.Point(16, 7);
			this.MoscowRadio.Name = "MoscowRadio";
			this.MoscowRadio.Size = new System.Drawing.Size(65, 17);
			this.MoscowRadio.TabIndex = 23;
			this.MoscowRadio.TabStop = true;
			this.MoscowRadio.Tag = "1";
			this.MoscowRadio.Text = "Москва";
			this.MoscowRadio.UseVisualStyleBackColor = true;
			this.MoscowRadio.CheckedChanged += new System.EventHandler(this.MoscowRadio_CheckedChanged);
			// 
			// ShowPublished
			// 
			this.ShowPublished.AutoSize = true;
			this.ShowPublished.Checked = true;
			this.ShowPublished.CheckState = System.Windows.Forms.CheckState.Checked;
			this.ShowPublished.Location = new System.Drawing.Point(16, 65);
			this.ShowPublished.Name = "ShowPublished";
			this.ShowPublished.Size = new System.Drawing.Size(238, 17);
			this.ShowPublished.TabIndex = 22;
			this.ShowPublished.Text = "Только товары, показываемые на сайте";
			this.ShowPublished.UseVisualStyleBackColor = true;
			this.ShowPublished.CheckedChanged += new System.EventHandler(this.ShowPublished_CheckedChanged);
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(16, 31);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(288, 28);
			this.dataGridPager1.TabIndex = 19;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(248, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 443);
			this.splitter1.TabIndex = 22;
			this.splitter1.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tvMasterMos);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(248, 443);
			this.panel2.TabIndex = 23;
			// 
			// formattableBooleanColumn2
			// 
			this.formattableBooleanColumn2.FieldName = "fHasDescr(OID)";
			this.formattableBooleanColumn2.FilterFieldName = null;
			this.formattableBooleanColumn2.HeaderText = "Есть описания";
			this.formattableBooleanColumn2.MappingName = "hasDescr";
			this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn2, this.menuFilterSort1);
			this.formattableBooleanColumn2.Width = 75;
			// 
			// SaveList
			// 
			this.SaveList.DefaultExt = "xls";
			this.SaveList.Filter = "Файлы Excel|*.xls|Все файлы|*.*";
			this.SaveList.Title = "Сохранить список";
			// 
			// formattableTextBoxColumn13
			// 
			this.formattableTextBoxColumn13.FieldName = "items.subCode";
			this.formattableTextBoxColumn13.FilterFieldName = null;
			this.formattableTextBoxColumn13.Format = "";
			this.formattableTextBoxColumn13.FormatInfo = null;
			this.formattableTextBoxColumn13.HeaderText = "Код уценки";
			this.formattableTextBoxColumn13.MappingName = "subCode";
			// 
			// formattableBooleanColumn7
			// 
			this.formattableBooleanColumn7.FieldName = "items.inStock";
			this.formattableBooleanColumn7.FilterFieldName = null;
			this.formattableBooleanColumn7.HeaderText = "На складе";
			this.formattableBooleanColumn7.MappingName = "inStock";
			// 
			// ListGoodsItems
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(892, 443);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel2);
			this.MinimumSize = new System.Drawing.Size(560, 115);
			this.Name = "ListGoodsItems";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Список уценённых товаров";
			this.Load += new System.EventHandler(this.ListGoodsItems_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private CheckBox ShowPublished;
		private RadioButton SpbRadio;
		private RadioButton MoscowRadio;
		private CheckBox HasDescriptionCheck;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn11;
		private RadioButton NskRadio;
		private RadioButton KdrRadio;
		private SaveFileDialog SaveList;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn13;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn7;


	}
}
