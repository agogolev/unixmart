﻿namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListArticle.
	/// </summary>
	public partial class ListPage : ListForm
	{
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ListPage()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmListPage_Load(object sender, System.EventArgs e) {
			mainDataGrid1.LoadData();
		}
	}
}
