﻿using System;
using System.Windows.Forms;
using Ecommerce.Extensions;
using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
    public partial class EditLandingPage : EditForm
    {
        private Binding shortLinkBind;
        public EditLandingPage()
        {
            InitializeComponent();
            Object = new CLandingPage();
        }

        public string ObjectName { get; set; } = "";

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveObject();
            }
            catch
            {
            }
            //this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BindFields()
        {
            NameText.DataBindings.Add("BoundProp", Object, "Name");
            shortLinkBind = new Binding("Text", Object, "ShortLink");
            ShortLinkText.DataBindings.Add(shortLinkBind);
            CommodityGroupSelect.DataBindings.Add("BoundProp", Object, "CommodityGroup");
            ArticleSelector.DataBindings.Add("BoundProp", Object, "Article");
            ActualGoodsCountText.DataBindings.Add("BoundProp", Object, "ActualGoodsCount");
            H1Text.DataBindings.Add("Text", Object, "H1");
            txtTitle.DataBindings.Add("Text", Object, "Title");
            txtKeywords.DataBindings.Add("Text", Object, "Keywords");
            txtPageDescription.DataBindings.Add("Text", Object, "PageDescription");
        }

        private void EditLandingPage_Load(object sender, EventArgs e)
        {
            LoadObject("name,shortLink,commodityGroup,article,actualGoodsCount,title,keywords,pageDescription,h1", null);
            BindFields();
        }

        private void CommodityGroupSelect_BeforeEdit(object sender, EventArgs e)
        {
            var form = (EditCommodityGroup) sender;
            if (form != null)
            {
                form.GroupType = 2;
                form.Name = ((CLandingPage) Object).Name;
            }
        }

        private void TranslitButton_Click(object sender, EventArgs e)
        {
            string shortLink = ShortLinkText.Text.TrimInner().Trim().TranslitEncode(true).ToLower();
            ((CObject)Object).ShortLink = shortLink;
            shortLinkBind.ReadValue();
        }

        private void ArticleSelector_BeforeEdit(object sender, EventArgs e)
        {
            var form = (EditArticle)sender;
            if (form != null)
            {
                form.Header = ((CLandingPage)Object).Name;
            }
        }
    }
}