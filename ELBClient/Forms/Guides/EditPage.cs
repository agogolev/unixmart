﻿using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	public partial class EditPage : EditForm
	{
		private ObjectProvider.ObjectProvider objectProvider = ServiceUtility.ObjectProvider;

		
		
		
		
		
		
		

		private string _objectName = "";
		public string ObjectName
		{
			get
			{
				return _objectName;
			}
			set
			{
				_objectName = value;
			}
		}

		public EditPage()
		{
			InitializeComponent();
			Object = new CPage();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch{}
			//this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void BindFields()
		{
			tbName.DataBindings.Add("BoundProp", Object, "Name");
			tbSite.DataBindings.Add("BoundProp", Object, "Site");
		}

		private void fmEditName_Load(object sender, System.EventArgs e)
		{
			LoadObject("name,site", null);
			BindFields();
		}
	}
}
