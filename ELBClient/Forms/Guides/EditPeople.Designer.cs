using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditPeople
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TextBox tbFirstName;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbMiddleName;
		private System.Windows.Forms.TextBox tbLastName;
		private System.Windows.Forms.TextBox tbPass2;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox tbEmail;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbPass;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbLogin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage4;
		private ELBClient.UserControls.AddDelDataGrid addDelDataGrid1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox tbComment;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TextBox CompanyText;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox JurAddressText;
		private System.Windows.Forms.TextBox InnText;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox BikText;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox OkpoText;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label8;
		private ColumnMenuExtender.DataBoundTextBox IDText;

		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label8 = new System.Windows.Forms.Label();
			this.IDText = new ColumnMenuExtender.DataBoundTextBox();
			this.tbComment = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.tbFirstName = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tbMiddleName = new System.Windows.Forms.TextBox();
			this.tbLastName = new System.Windows.Forms.TextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tbPass2 = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.tbEmail = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbPass = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbLogin = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.addDelDataGrid1 = new ELBClient.UserControls.AddDelDataGrid();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.label9 = new System.Windows.Forms.Label();
			this.ManagerSelection = new ELBClient.UserControls.SelectorTextBox();
			this.BankAccountText = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.BankText = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.UrlText = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.FaxText = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.PhoneText = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.AddressText = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.CityText = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.ZipCodeText = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.OkpoText = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.BikText = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.InnText = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.JurAddressText = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.CompanyText = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 28);
			this.btnCancel.TabIndex = 10;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(318, 9);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 28);
			this.btnSave.TabIndex = 9;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.label8);
			this.tabPage1.Controls.Add(this.IDText);
			this.tabPage1.Controls.Add(this.tbComment);
			this.tabPage1.Controls.Add(this.label7);
			this.tabPage1.Controls.Add(this.tbFirstName);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.label5);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.tbMiddleName);
			this.tabPage1.Controls.Add(this.tbLastName);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(392, 393);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Общее";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 128);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(104, 19);
			this.label8.TabIndex = 17;
			this.label8.Text = "Код SV:";
			// 
			// IDText
			// 
			this.IDText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.IDText.BoundProp = null;
			this.IDText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.IDText.IsCurrency = false;
			this.IDText.Location = new System.Drawing.Point(144, 128);
			this.IDText.Name = "IDText";
			this.IDText.Size = new System.Drawing.Size(240, 20);
			this.IDText.TabIndex = 16;
			// 
			// tbComment
			// 
			this.tbComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbComment.Location = new System.Drawing.Point(144, 168);
			this.tbComment.MaxLength = 5000;
			this.tbComment.Multiline = true;
			this.tbComment.Name = "tbComment";
			this.tbComment.Size = new System.Drawing.Size(240, 130);
			this.tbComment.TabIndex = 15;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 168);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(104, 19);
			this.label7.TabIndex = 14;
			this.label7.Text = "Комментарий:";
			// 
			// tbFirstName
			// 
			this.tbFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbFirstName.Location = new System.Drawing.Point(144, 55);
			this.tbFirstName.Name = "tbFirstName";
			this.tbFirstName.Size = new System.Drawing.Size(240, 20);
			this.tbFirstName.TabIndex = 2;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 18);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(120, 19);
			this.label6.TabIndex = 13;
			this.label6.Text = "Фамилия:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 92);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 19);
			this.label5.TabIndex = 12;
			this.label5.Text = "Отчество:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 55);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 19);
			this.label2.TabIndex = 2;
			this.label2.Text = "Имя:";
			// 
			// tbMiddleName
			// 
			this.tbMiddleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbMiddleName.Location = new System.Drawing.Point(144, 92);
			this.tbMiddleName.Name = "tbMiddleName";
			this.tbMiddleName.Size = new System.Drawing.Size(240, 20);
			this.tbMiddleName.TabIndex = 3;
			// 
			// tbLastName
			// 
			this.tbLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbLastName.Location = new System.Drawing.Point(144, 18);
			this.tbLastName.Name = "tbLastName";
			this.tbLastName.Size = new System.Drawing.Size(240, 20);
			this.tbLastName.TabIndex = 1;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.tbPass2);
			this.tabPage2.Controls.Add(this.label10);
			this.tabPage2.Controls.Add(this.tbEmail);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Controls.Add(this.tbPass);
			this.tabPage2.Controls.Add(this.label3);
			this.tabPage2.Controls.Add(this.tbLogin);
			this.tabPage2.Controls.Add(this.label1);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(392, 393);
			this.tabPage2.TabIndex = 4;
			this.tabPage2.Text = "Логин";
			// 
			// tbPass2
			// 
			this.tbPass2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPass2.Location = new System.Drawing.Point(144, 92);
			this.tbPass2.Name = "tbPass2";
			this.tbPass2.PasswordChar = '*';
			this.tbPass2.Size = new System.Drawing.Size(240, 20);
			this.tbPass2.TabIndex = 3;
			this.tbPass2.Enter += new System.EventHandler(this.tbPass_Enter);
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(8, 92);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(100, 19);
			this.label10.TabIndex = 6;
			this.label10.Text = "Повтор пароля";
			// 
			// tbEmail
			// 
			this.tbEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbEmail.Location = new System.Drawing.Point(144, 129);
			this.tbEmail.Name = "tbEmail";
			this.tbEmail.Size = new System.Drawing.Size(240, 20);
			this.tbEmail.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 129);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 19);
			this.label4.TabIndex = 4;
			this.label4.Text = "e-mail";
			// 
			// tbPass
			// 
			this.tbPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPass.Location = new System.Drawing.Point(144, 55);
			this.tbPass.Name = "tbPass";
			this.tbPass.PasswordChar = '*';
			this.tbPass.Size = new System.Drawing.Size(240, 20);
			this.tbPass.TabIndex = 2;
			this.tbPass.Enter += new System.EventHandler(this.tbPass_Enter);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 55);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 19);
			this.label3.TabIndex = 2;
			this.label3.Text = "Пароль";
			// 
			// tbLogin
			// 
			this.tbLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbLogin.Location = new System.Drawing.Point(144, 18);
			this.tbLogin.Name = "tbLogin";
			this.tbLogin.Size = new System.Drawing.Size(240, 20);
			this.tbLogin.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 19);
			this.label1.TabIndex = 0;
			this.label1.Text = "Логин";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.ItemSize = new System.Drawing.Size(47, 18);
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(400, 419);
			this.tabControl1.TabIndex = 30;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.addDelDataGrid1);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(392, 393);
			this.tabPage4.TabIndex = 6;
			this.tabPage4.Text = "Группы";
			// 
			// addDelDataGrid1
			// 
			this.addDelDataGrid1.AddDelDataGridColumnNames = null;
			this.addDelDataGrid1.ClassName = null;
			this.addDelDataGrid1.ColumnHeaderNames = null;
			this.addDelDataGrid1.ColumnNames = null;
			this.addDelDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addDelDataGrid1.DT = null;
			this.addDelDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.addDelDataGrid1.MultiContainer = null;
			this.addDelDataGrid1.Name = "addDelDataGrid1";
			this.addDelDataGrid1.Size = new System.Drawing.Size(392, 393);
			this.addDelDataGrid1.TabIndex = 8;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.label9);
			this.tabPage3.Controls.Add(this.ManagerSelection);
			this.tabPage3.Controls.Add(this.BankAccountText);
			this.tabPage3.Controls.Add(this.label24);
			this.tabPage3.Controls.Add(this.BankText);
			this.tabPage3.Controls.Add(this.label23);
			this.tabPage3.Controls.Add(this.UrlText);
			this.tabPage3.Controls.Add(this.label22);
			this.tabPage3.Controls.Add(this.FaxText);
			this.tabPage3.Controls.Add(this.label21);
			this.tabPage3.Controls.Add(this.PhoneText);
			this.tabPage3.Controls.Add(this.label20);
			this.tabPage3.Controls.Add(this.AddressText);
			this.tabPage3.Controls.Add(this.label19);
			this.tabPage3.Controls.Add(this.CityText);
			this.tabPage3.Controls.Add(this.label18);
			this.tabPage3.Controls.Add(this.ZipCodeText);
			this.tabPage3.Controls.Add(this.label13);
			this.tabPage3.Controls.Add(this.label12);
			this.tabPage3.Controls.Add(this.OkpoText);
			this.tabPage3.Controls.Add(this.label17);
			this.tabPage3.Controls.Add(this.BikText);
			this.tabPage3.Controls.Add(this.label16);
			this.tabPage3.Controls.Add(this.InnText);
			this.tabPage3.Controls.Add(this.label15);
			this.tabPage3.Controls.Add(this.JurAddressText);
			this.tabPage3.Controls.Add(this.label14);
			this.tabPage3.Controls.Add(this.CompanyText);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(392, 393);
			this.tabPage3.TabIndex = 7;
			this.tabPage3.Text = "Юр. лицо";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(12, 352);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(122, 27);
			this.label9.TabIndex = 30;
			this.label9.Text = "Менеджер:";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ManagerSelection
			// 
			this.ManagerSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.ManagerSelection.BoundProp = null;
			this.ManagerSelection.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.ManagerSelection.ClassName = "CPeople";
			this.ManagerSelection.FieldNames = null;
			this.ManagerSelection.FormatRows = null;
			this.ManagerSelection.HeaderNames = null;
			this.ManagerSelection.Location = new System.Drawing.Point(140, 354);
			this.ManagerSelection.Name = "ManagerSelection";
			this.ManagerSelection.RowNames = null;
			this.ManagerSelection.Size = new System.Drawing.Size(232, 24);
			this.ManagerSelection.TabIndex = 29;
			// 
			// BankAccountText
			// 
			this.BankAccountText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.BankAccountText.Location = new System.Drawing.Point(140, 249);
			this.BankAccountText.Name = "BankAccountText";
			this.BankAccountText.Size = new System.Drawing.Size(118, 20);
			this.BankAccountText.TabIndex = 11;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(12, 248);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(122, 26);
			this.label24.TabIndex = 28;
			this.label24.Text = "Р/счёт:";
			this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BankText
			// 
			this.BankText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.BankText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.BankText.Location = new System.Drawing.Point(140, 223);
			this.BankText.Name = "BankText";
			this.BankText.Size = new System.Drawing.Size(232, 20);
			this.BankText.TabIndex = 10;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(12, 222);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(122, 26);
			this.label23.TabIndex = 26;
			this.label23.Text = "Банк:";
			this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// UrlText
			// 
			this.UrlText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.UrlText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.UrlText.Location = new System.Drawing.Point(140, 197);
			this.UrlText.Name = "UrlText";
			this.UrlText.Size = new System.Drawing.Size(232, 20);
			this.UrlText.TabIndex = 9;
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(12, 196);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(122, 26);
			this.label22.TabIndex = 24;
			this.label22.Text = "WWW адрес:";
			this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// FaxText
			// 
			this.FaxText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.FaxText.Location = new System.Drawing.Point(140, 171);
			this.FaxText.Name = "FaxText";
			this.FaxText.Size = new System.Drawing.Size(118, 20);
			this.FaxText.TabIndex = 8;
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(12, 170);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(122, 26);
			this.label21.TabIndex = 22;
			this.label21.Text = "Факс:";
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// PhoneText
			// 
			this.PhoneText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PhoneText.Location = new System.Drawing.Point(140, 145);
			this.PhoneText.Name = "PhoneText";
			this.PhoneText.Size = new System.Drawing.Size(118, 20);
			this.PhoneText.TabIndex = 7;
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(12, 144);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(122, 26);
			this.label20.TabIndex = 20;
			this.label20.Text = "Телефон:";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// AddressText
			// 
			this.AddressText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.AddressText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.AddressText.Location = new System.Drawing.Point(140, 93);
			this.AddressText.Name = "AddressText";
			this.AddressText.Size = new System.Drawing.Size(232, 20);
			this.AddressText.TabIndex = 5;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(12, 92);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(122, 26);
			this.label19.TabIndex = 18;
			this.label19.Text = "Адрес:";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CityText
			// 
			this.CityText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.CityText.Location = new System.Drawing.Point(140, 67);
			this.CityText.Name = "CityText";
			this.CityText.Size = new System.Drawing.Size(118, 20);
			this.CityText.TabIndex = 4;
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(12, 66);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(122, 26);
			this.label18.TabIndex = 16;
			this.label18.Text = "Город:";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ZipCodeText
			// 
			this.ZipCodeText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ZipCodeText.Location = new System.Drawing.Point(140, 41);
			this.ZipCodeText.Name = "ZipCodeText";
			this.ZipCodeText.Size = new System.Drawing.Size(118, 20);
			this.ZipCodeText.TabIndex = 3;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(12, 40);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(122, 26);
			this.label13.TabIndex = 14;
			this.label13.Text = "Индекс:";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(12, 10);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(122, 26);
			this.label12.TabIndex = 13;
			this.label12.Text = "Название:";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// OkpoText
			// 
			this.OkpoText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.OkpoText.Location = new System.Drawing.Point(140, 327);
			this.OkpoText.Name = "OkpoText";
			this.OkpoText.Size = new System.Drawing.Size(118, 20);
			this.OkpoText.TabIndex = 14;
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(12, 325);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(122, 27);
			this.label17.TabIndex = 11;
			this.label17.Text = "ОКПО:";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BikText
			// 
			this.BikText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.BikText.Location = new System.Drawing.Point(140, 275);
			this.BikText.Name = "BikText";
			this.BikText.Size = new System.Drawing.Size(118, 20);
			this.BikText.TabIndex = 12;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(12, 274);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(122, 26);
			this.label16.TabIndex = 9;
			this.label16.Text = "БИК:";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// InnText
			// 
			this.InnText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.InnText.Location = new System.Drawing.Point(140, 301);
			this.InnText.Name = "InnText";
			this.InnText.Size = new System.Drawing.Size(118, 20);
			this.InnText.TabIndex = 13;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(12, 300);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(122, 26);
			this.label15.TabIndex = 7;
			this.label15.Text = "ИНН:";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// JurAddressText
			// 
			this.JurAddressText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.JurAddressText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.JurAddressText.Location = new System.Drawing.Point(140, 119);
			this.JurAddressText.Name = "JurAddressText";
			this.JurAddressText.Size = new System.Drawing.Size(232, 20);
			this.JurAddressText.TabIndex = 6;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(12, 118);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(122, 26);
			this.label14.TabIndex = 5;
			this.label14.Text = "Юр. адрес:";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CompanyText
			// 
			this.CompanyText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.CompanyText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.CompanyText.Location = new System.Drawing.Point(140, 15);
			this.CompanyText.Name = "CompanyText";
			this.CompanyText.Size = new System.Drawing.Size(232, 20);
			this.CompanyText.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnCancel);
			this.panel2.Controls.Add(this.btnSave);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 419);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(400, 46);
			this.panel2.TabIndex = 31;
			// 
			// EditPeople
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(400, 465);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.panel2);
			this.MinimumSize = new System.Drawing.Size(408, 351);
			this.Name = "EditPeople";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Персона";
			this.Load += new System.EventHandler(this.fmEditPeople_Load);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private TextBox BankAccountText;
		private Label label24;
		private TextBox BankText;
		private Label label23;
		private TextBox UrlText;
		private Label label22;
		private TextBox FaxText;
		private Label label21;
		private TextBox PhoneText;
		private Label label20;
		private TextBox AddressText;
		private Label label19;
		private TextBox CityText;
		private Label label18;
		private TextBox ZipCodeText;
		private Label label13;
		private Label label12;
		private Label label9;
		private UserControls.SelectorTextBox ManagerSelection;


	}
}
