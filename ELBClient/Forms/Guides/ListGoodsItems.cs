﻿using System;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using ELBClient.Classes;
using MetaData;
using ELBClient.Forms.Dialogs;
using ColumnMenuExtender;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using Ecommerce.Extensions;


namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListGoods.
	/// </summary>
	public partial class ListGoodsItems : ListForm
	{
		public IList<ShopMenu> Shops { get; set; }
		int _shopType;
		int _catalogRootNode;
		private const int MasterNodeMos = ELBExpImp.Constants.MASTER_NODE_ROOT;
		private readonly ArrayList _filterCategories = new ArrayList();
		private readonly Tree _treeMasterMos = new Tree(0, int.MaxValue);

		public ListGoodsItems()
		{
			InitializeComponent();
			listContextMenu1.miDelete.Visible = false;
			var mnuDelete = new MenuItem("Удалить");
			mnuDelete.Click += mnuDelete_Click;
			listContextMenu1.insertItem(4, mnuDelete);
			var mnuAttachImages = new MenuItem("Привязка картинок");
			mnuAttachImages.Click += mnuAttachImages_Click;
			listContextMenu1.insertItem(2, mnuAttachImages);
		}

		public override void LoadData()
		{
			dataSet1 = lp.GetList(dgGoods.GetDataXml().OuterXml);

			int rowNum = dgGoods.CurrentRowIndex;
			dgGoods.SetDataBinding(dataSet1, "table");

			if (rowNum > dataSet1.Tables["table"].Rows.Count - 1) rowNum = dataSet1.Tables["table"].Rows.Count - 1;
			if (rowNum >= 0) dgGoods.CurrentRowIndex = rowNum;
		}

		private void ListGoodsItems_Load(object sender, EventArgs e)
		{
			LoadShops();
			//shopType = 1;
			//siteUrl = "http://www.electroburg.ru";
			_catalogRootNode = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(_shopType);
			LoadThemePath();
			dgGoods.Filters.Clear();
			dgGoods.QueryParams["shopType"] = _shopType;
			dgGoods.QueryParams["rootNode"] = _catalogRootNode;

			BuildMasterTree();
			LoadMasterTree();
			
			BuildStockFilter(ShowPublished.Checked);

			dataGridPager1.BindToDataGrid(dgGoods);


			listContextMenu1.Bind(this);
		}

		private void LoadShops()
		{
			Shops = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).Shops;
			MoscowRadio.Visible = Shops.Single(s => s.ShopType == 1).IsActive;
			SpbRadio.Visible = Shops.Single(s => s.ShopType == 2).IsActive;
			NskRadio.Visible = Shops.Single(s => s.ShopType == 3).IsActive;
			KdrRadio.Visible = Shops.Single(s => s.ShopType == 4).IsActive;
			var shop = Shops.Where(s => s.IsActive).OrderBy(s => s.ShopType).First();
			_shopType = shop.ShopType;
			MoscowRadio.Checked = _shopType == 1;
			SpbRadio.Checked = _shopType == 2;
			NskRadio.Checked = _shopType == 3;
			KdrRadio.Checked = _shopType == 4;
		}

		private void FullReload()
		{
			LoadData();
			dataGridPager1.BindToDataGrid(dgGoods);
		}

		private void dataGridISM1_Reload(object sender, EventArgs e)
		{
			LoadData();
		}

		private void btnNew_Click(object sender, EventArgs e)
		{
			showEditForm(Guid.Empty);
		}

		private void listContextMenu1_EditClick(object sender, EventArgs e)
		{
			showEditForm(listContextMenu1.SelectedOID);
		}

		private void showEditForm(Guid OID)
		{
			//var form = new EditGoods { ObjectOID = OID, MdiParent = MdiParent };
			//form.ReloadGrid += LoadData;
			//form.Show();
		}

		private void dataGridISM1_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				DataGrid.HitTestInfo hti = dgGoods.HitTest(e.X, e.Y);
				if (hti.Type == DataGrid.HitTestType.Cell)
				{
					int rowNum = hti.Row;
					dgGoods.CurrentRowIndex = rowNum;
					DataTable dt = TableInGrid.GetTable(dgGoods);
					listContextMenu1.SelectedOID = (Guid)dt.Rows[rowNum]["OID"];
					listContextMenu1.Show(dgGoods, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_DoubleClick(object sender, EventArgs e)
		{
			Point pt = dgGoods.PointToClient(Cursor.Position);
			DataGrid.HitTestInfo hti = dgGoods.HitTest(pt);
			if (hti.Type == DataGrid.HitTestType.Cell)
			{
				DataRow dr = dgGoods.GetSelectedRow();
				showEditForm((Guid)dr["OID"]);
			}
		}

		private void dataGridISM1_ActionKeyPressed(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Insert)
			{
				showEditForm(Guid.Empty);
			}
			else if (e.KeyCode == Keys.Delete)
			{
				if (dgGoods.CurrentRowIndex >= 0)
				{
					DataTable dt = TableInGrid.GetTable(dgGoods);
					listContextMenu1.DeleteObject(ParentForm, (Guid)dt.Rows[dgGoods.CurrentRowIndex]["OID"]);
				}
			}
			else if (e.KeyCode == Keys.Enter)
			{
				if (dgGoods.CurrentRowIndex >= 0)
				{
					DataTable dt = TableInGrid.GetTable(dgGoods);
					showEditForm((Guid)dt.Rows[dgGoods.CurrentRowIndex]["OID"]);
				}
			}
		}

		private void mnuAttachImages_Click(object sender, EventArgs e)
		{
			var rows = dgGoods.GetMultiSelectedRows();
			if (rows.Count == 1)
			{
				var dr = (DataRow)rows[0];
				var OID = (Guid)dr["OID"];
				var name = dr["name"].ToString();
				var goods = new CGoods { OID = OID };
				var frm = new AttachImages(goods);
				frm.Text += " для товара: " + name;
				frm.ReloadGrid += LoadData;
				frm.ShowDialog(this);
			}
			else
			{
				var goods = new CGoods();
				var frm = new MassAttachImages(goods);
				if (frm.ShowDialog(this) == DialogResult.OK)
				{
					try
					{
						var imageOID = frm.SelectedImage.OID;
						var ordValue = frm.OrdValue;
						const string sql = @"
if EXISTS (SELECT * FROM t_ObjectImage WHERE OID = '%OID%' And ordValue = '%ordValue%') 
	UPDATE t_ObjectImage SET binData = '%imageOID%' WHERE OID = '%OID%' And ordValue = '%ordValue%'
else 
	INSERT INTO t_ObjectImage VALUES('%OID%', '%ordValue%', '%imageOID%')
";
						if (imageOID != Guid.Empty)
						{
							foreach (DataRow dr in rows)
							{
								var OID = (Guid)dr["OID"];
								var prov = ServiceUtility.ObjectProvider;
								var curSql = sql.Replace("%OID%", OID.ToString()).Replace("%ordValue%", ordValue.Replace("'", "''")).Replace("%imageOID%", imageOID.ToString());
								prov.ExecuteCommand(curSql);
							}
						}
					}
					catch (Exception ex)
					{
						ShowError("Ошибка привязки картинки: " + ex.Message);
					}
					LoadData();
				}
			}
		}

		private void mnuDelete_Click(object sender, EventArgs e)
		{
			ArrayList rows = dgGoods.GetMultiSelectedRows();
			if (MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				foreach (DataRow dr in rows)
				{
					var OID = (Guid)dr["OID"];
					var prov = ServiceUtility.ObjectProvider;
					var ex = prov.DeleteObject(OID);
					if (ex != null)
					{
						switch (ex.LiteralExceptionType)
						{
							case "System.Data.SqlClient.SqlException":
								ShowError("Невозможно удалить объект!\nВозможно он связан с другим объектом.");
								break;
							case "MetaData.SystemOIDException":
								ShowError("Невозможно удалить объект!\nВозможно он связан с другим объектом.");
								break;
							default:
								ShowError(ex.LiteralMessage);
								break;
						}
					}
				}
				LoadData();
			}
		}

		private void BuildMasterTree()
		{
			BuildMasterTree(_treeMasterMos, new Guid("20e1ac79-5a3c-4b49-94e0-9e640842fe81"), MasterNodeMos);
		}

		private void BuildMasterTree(Tree tree, Guid treeOID, int masterNode)
		{
			var treeProv = ServiceUtility.TreeProvider;
			var xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree OID = \"" + treeOID + "\" nodeID=\"" + masterNode + "\" />";
			var treeContent = treeProv.GetTreeContent(xml);
			var nodes = new Stack();
			nodes.Push(tree);

			var prevNode = 0;
			foreach (DataRow dr in treeContent.Tables["table"].Rows)
			{
				var nodeID = (int)dr["nodeID"];
				if (nodeID == prevNode) continue;
				var node = new Node((int)dr["lft"], (int)dr["rgt"])
							{
								NodeId = nodeID,
								Name = dr["nodeName"].ToString(),
								objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"],
								objectNK = dr["NK"] == DBNull.Value ? "" : (string)dr["NK"],
								objectNKRus = dr["label"] == DBNull.Value ? "" : (string)dr["label"],
								className = dr["className"] == DBNull.Value ? "" : (string)dr["className"]
							};
				while (nodes.Peek() != null && ((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
				((NodeContainer)nodes.Peek()).Nodes.Add(node);
				if ((node.Left + 1) != node.Right)
				{
					nodes.Push(node);
				}
				prevNode = nodeID;
			}
		}

		private void LoadMasterTree()
		{
			LoadMasterTree(tvMasterMos, _treeMasterMos);
		}

		private void LoadMasterTree(TreeView treeView, Tree tree)
		{
			treeView.Nodes.Clear();
			var root = new TreeNode("Все товары");
			treeView.Nodes.Add(root);
			foreach (Node n in tree.Nodes)
			{
				var tn =
					new TreeNode(n.Name != ""
									? n.Name.Replace("\r\n", " ")
									: (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>"))) { Tag = n };
				//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				root.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node)
		{
			foreach (Node n in node.Nodes)
			{
				var tn =
					new TreeNode(n.Name != ""
									? n.Name.Replace("\r\n", " ")
									: (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>"))) { Tag = n };
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void tvMaster_AfterSelect(object sender, TreeViewEventArgs e)
		{
			var tvSelect = (TreeView)sender;
			if (tvSelect.SelectedNode == null) return;
			tvSelect.SelectedNode.Expand();

			dgGoods.Filters.Remove("filter_category");
			//dgGoods.Filters.Remove("filter_shopType");


			_filterCategories.Clear();
			if (tvSelect.SelectedNode != tvSelect.Nodes[0] && (tvSelect.SelectedNode.Tag as Node).objectOID != Guid.Empty)
			{
				_filterCategories.Add((tvSelect.SelectedNode.Tag as Node).objectOID);
				LoadChildrenCategories(tvSelect.SelectedNode);
			}
			BuildFilter();

			dgGoods.OnReload(new EventArgs());
		}

		private void LoadChildrenCategories(TreeNode node)
		{
			foreach (TreeNode nd in node.Nodes)
			{
				Guid catOID = (nd.Tag as Node).objectOID;
				if (catOID != Guid.Empty) _filterCategories.Add(catOID);
				LoadChildrenCategories(nd);
			}
		}

		private void BuildFilter()
		{
			if (_filterCategories.Count != 0)
				dgGoods.CreateAuxFilter("category", "filter_category", FilterVerb.In, false, _filterCategories.ToArray());
			//dgGoods.FilterString = CreateFilter();
		}

		private void BuildStockFilter(bool doFilter)
		{
			if (doFilter)
			{
				dgGoods.CreateAuxFilter("items.inStock", "itemsinstock_filter", FilterVerb.Equal, false, new object[] { 1 });
				dgGoods.CreateAuxFilter("items.shopType", "itemsshoptype_filter", FilterVerb.Equal, false, new object[] { _shopType });
			}
			else
			{
				dgGoods.DeleteFilter("itemsinstock_filter");
				dgGoods.DeleteFilter("itemsinstock_filter");
			}
			//dgGoods.FilterString = CreateFilter();
		}

		private void BuildDescriptionFilter(bool doFilter)
		{
			if (doFilter)
			{
				dgGoods.CreateAuxFilter("fHasMarketDescr(OID, @shopType)", "description_filter", FilterVerb.Equal, false, new object[] { 0 });
			}
			else
			{
				dgGoods.DeleteFilter("description_filter");
			}
			//dgGoods.FilterString = CreateFilter();
		}

		private string CreateFilter()
		{
			var sb = new StringBuilder();
			foreach (DictionaryEntry entry in dgGoods.Filters)
			{
				if ((string)entry.Key == "instockinstock_filter")
					sb.Append("((instockinstock_filter or instockinpath_filter or instockondemand_filter) and instockshoptype_filter) and ");
			}
			if (sb.Length > 0) sb.Length -= 5;
			return sb.ToString();
		}

		private void LoadThemePath()
		{
			const string sql = @"
SELECT
	n.nodeID,
	t.themeName,
	n.lft,
	n.rgt
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = {0}
	inner join t_Theme t on n.object = t.OID
ORDER BY
	n.lft
";
			var ds = new DataSetISM(lp.GetDataSetSql(string.Format(sql, _catalogRootNode)));
			ds.Table.AsEnumerable().Select(k => new
													{
														nodeId = k.Field<int>("nodeID"),
														fullName = ds.Table.AsEnumerable().Where(m => k.Field<int>("lft") >= m.Field<int>("lft") && k.Field<int>("lft") <= m.Field<int>("rgt")).OrderBy(m => m.Field<int>("lft")).Select(m => m.Field<string>("themeName").TranslitEncode(true)).Intersperse("_").Aggregate((acc, p) => acc += p)
													}).ToDictionary(k => k.nodeId, v => v.fullName);
		}

		private void ShowPublished_CheckedChanged(object sender, EventArgs e)
		{
			BuildStockFilter(ShowPublished.Checked);
			FullReload();
		}

		private void MoscowRadio_CheckedChanged(object sender, EventArgs e)
		{
			if (((RadioButton)sender).Checked)
			{
				_shopType = int.Parse(((Control)sender).Tag.ToString());

				if (_shopType == 1)
				{
					_catalogRootNode = ((MainFrm)MdiParent).CatalogRoot(1);
				}
				else if (_shopType == 2)
				{
					_catalogRootNode = ((MainFrm)MdiParent).CatalogRoot(2);
				}
				else if (_shopType == 3)
				{
					_catalogRootNode = ((MainFrm)MdiParent).CatalogRoot(3);
				}
				else
				{
					_catalogRootNode = ((MainFrm)MdiParent).CatalogRoot(4);
				}
				dgGoods.QueryParams["shopType"] = _shopType;
				dgGoods.QueryParams["rootNode"] = _catalogRootNode;
				LoadThemePath();
				BuildStockFilter(ShowPublished.Checked);
				FullReload();
			}
		}

		private void HasDescriptionCheck_CheckedChanged(object sender, EventArgs e)
		{
			BuildDescriptionFilter(HasDescriptionCheck.Checked);
			FullReload();
		}

		private void listContextMenu1_GetList(object sender, EventArgs e)
		{
			if (SaveList.ShowDialog(this) == DialogResult.OK)
			{
				var re = new Regex(@"\s+", RegexOptions.Singleline);
				var sb = new StringBuilder();
				var items = (DataSet)dgGoods.DataSource;
				using (var wrt = new StreamWriter(SaveList.FileName, false, Encoding.GetEncoding(1251)))
				{
					wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
							.Select(column => re.Replace(column.HeaderText, " "))
							.Intersperse("\t")
							.Aggregate(sb, (builder, str) => { builder.Append(str); return builder; }, builder => { string result = builder.ToString(); builder.Length = 0; return result; }));
					items.Tables[0].AsEnumerable().ToList().ForEach(row => wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
																							.Select(column => re.Replace(row[column.MappingName].ToString(), " "))
																							.Intersperse("\t")
																							.Aggregate(sb, (builder, str) => { builder.Append(str); return builder; }, builder => { string result = builder.ToString(); builder.Length = 0; return result; })));
				}
			}
		}
	}
}
