using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	public partial class EditParamType
	{
		#region Windows Form Designer generated code

		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private DataBoundComboBox cbParamType;
		private System.Windows.Forms.TextBox txtUnit;
		private System.Windows.Forms.CheckBox cbIsDomain;
		private System.Windows.Forms.CheckBox cbIsMandatory;
		private ColumnMenuExtender.DataGridISM dgList;
		private System.Windows.Forms.Label lbList;
		private System.Windows.Forms.Button btnDelValue;
		private System.Windows.Forms.Button btnAddValue;
		private System.Windows.Forms.Button btnCheckValues;
		private System.Windows.Forms.CheckBox cbIsFullName;
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colValue;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.cbParamType = new DataBoundComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtUnit = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.cbIsDomain = new System.Windows.Forms.CheckBox();
			this.cbIsMandatory = new System.Windows.Forms.CheckBox();
			this.dgList = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colValue = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.lbList = new System.Windows.Forms.Label();
			this.btnDelValue = new System.Windows.Forms.Button();
			this.btnAddValue = new System.Windows.Forms.Button();
			this.btnCheckValues = new System.Windows.Forms.Button();
			this.cbIsFullName = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.Location = new System.Drawing.Point(490, 9);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 4;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 18);
			this.label3.TabIndex = 14;
			this.label3.Text = "Название:";
			// 
			// txtName
			// 
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtName.Location = new System.Drawing.Point(96, 9);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(466, 20);
			this.txtName.TabIndex = 1;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 288);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(572, 46);
			this.panel1.TabIndex = 15;
			// 
			// cbParamType
			// 
			this.cbParamType.Location = new System.Drawing.Point(96, 46);
			this.cbParamType.Name = "cbParamType";
			this.cbParamType.SelectedValue = -1;
			this.cbParamType.Size = new System.Drawing.Size(200, 21);
			this.cbParamType.TabIndex = 16;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 19);
			this.label1.TabIndex = 17;
			this.label1.Text = "Тип:";
			// 
			// txtUnit
			// 
			this.txtUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtUnit.Location = new System.Drawing.Point(96, 83);
			this.txtUnit.Name = "txtUnit";
			this.txtUnit.Size = new System.Drawing.Size(104, 20);
			this.txtUnit.TabIndex = 18;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 83);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 19);
			this.label2.TabIndex = 19;
			this.label2.Text = "Ед. изм.:";
			// 
			// cbIsDomain
			// 
			this.cbIsDomain.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbIsDomain.Location = new System.Drawing.Point(8, 120);
			this.cbIsDomain.Name = "cbIsDomain";
			this.cbIsDomain.Size = new System.Drawing.Size(136, 28);
			this.cbIsDomain.TabIndex = 25;
			this.cbIsDomain.Text = "Список";
			this.cbIsDomain.CheckedChanged += new System.EventHandler(this.cbIsDomain_CheckedChanged);
			// 
			// cbIsMandatory
			// 
			this.cbIsMandatory.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbIsMandatory.Location = new System.Drawing.Point(8, 150);
			this.cbIsMandatory.Name = "cbIsMandatory";
			this.cbIsMandatory.Size = new System.Drawing.Size(136, 28);
			this.cbIsMandatory.TabIndex = 26;
			this.cbIsMandatory.Text = "Обязательность";
			// 
			// dgList
			// 
			this.dgList.BackgroundColor = System.Drawing.Color.White;
			this.dgList.DataMember = "";
			this.dgList.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgList.Location = new System.Drawing.Point(224, 120);
			this.dgList.Name = "dgList";
			this.dgList.Order = null;
			this.dgList.Size = new System.Drawing.Size(280, 148);
			this.dgList.StockClass = "CStringItem";
			this.dgList.StockNumInBatch = 0;
			this.dgList.TabIndex = 27;
			this.dgList.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.dgList.Visible = false;
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgList;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colValue});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// colValue
			// 
			this.colValue.FieldName = null;
			this.colValue.FilterFieldName = null;
			this.colValue.Format = "";
			this.colValue.FormatInfo = null;
			this.colValue.HeaderText = "Значение";
			this.colValue.MappingName = "value";
			this.colValue.Width = 200;
			// 
			// lbList
			// 
			this.lbList.Location = new System.Drawing.Point(224, 92);
			this.lbList.Name = "lbList";
			this.lbList.Size = new System.Drawing.Size(160, 27);
			this.lbList.TabIndex = 28;
			this.lbList.Text = "Списочные значения";
			this.lbList.Visible = false;
			// 
			// btnDelValue
			// 
			this.btnDelValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDelValue.Location = new System.Drawing.Point(546, 157);
			this.btnDelValue.Name = "btnDelValue";
			this.btnDelValue.Size = new System.Drawing.Size(24, 26);
			this.btnDelValue.TabIndex = 39;
			this.btnDelValue.Text = "-";
			this.btnDelValue.Click += new System.EventHandler(this.btnDelValue_Click);
			// 
			// btnAddValue
			// 
			this.btnAddValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAddValue.Location = new System.Drawing.Point(546, 120);
			this.btnAddValue.Name = "btnAddValue";
			this.btnAddValue.Size = new System.Drawing.Size(24, 27);
			this.btnAddValue.TabIndex = 38;
			this.btnAddValue.Text = "+";
			this.btnAddValue.Click += new System.EventHandler(this.btnAddValue_Click);
			// 
			// btnCheckValues
			// 
			this.btnCheckValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCheckValues.Location = new System.Drawing.Point(546, 194);
			this.btnCheckValues.Name = "btnCheckValues";
			this.btnCheckValues.Size = new System.Drawing.Size(24, 26);
			this.btnCheckValues.TabIndex = 40;
			this.btnCheckValues.Text = "?";
			this.btnCheckValues.Click += new System.EventHandler(this.btnCheckValues_Click);
			// 
			// cbIsFullName
			// 
			this.cbIsFullName.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cbIsFullName.Location = new System.Drawing.Point(8, 185);
			this.cbIsFullName.Name = "cbIsFullName";
			this.cbIsFullName.Size = new System.Drawing.Size(136, 27);
			this.cbIsFullName.TabIndex = 41;
			this.cbIsFullName.Text = "В полном названии";
			// 
			// EditParamType
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(572, 334);
			this.Controls.Add(this.cbIsFullName);
			this.Controls.Add(this.btnCheckValues);
			this.Controls.Add(this.btnDelValue);
			this.Controls.Add(this.btnAddValue);
			this.Controls.Add(this.dgList);
			this.Controls.Add(this.cbIsMandatory);
			this.Controls.Add(this.cbIsDomain);
			this.Controls.Add(this.txtUnit);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbParamType);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lbList);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(464, 111);
			this.Name = "EditParamType";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Шаблон параметра";
			this.Load += new System.EventHandler(this.fmEditTheme_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
