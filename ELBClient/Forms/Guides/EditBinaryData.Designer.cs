using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditBinaryData
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.OpenFileDialog ofdFile;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbMimeType;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbFile;
		private System.Windows.Forms.Button btnView;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private ColumnMenuExtender.DataBoundTextBox tbHeight;
		private ColumnMenuExtender.DataBoundTextBox tbWidth;
		private System.Windows.Forms.Label label1;
		private ColumnMenuExtender.ExtendedDataGrid dgObjects;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.ofdFile = new System.Windows.Forms.OpenFileDialog();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dgObjects = new ColumnMenuExtender.ExtendedDataGrid();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tbHeight = new ColumnMenuExtender.DataBoundTextBox();
			this.tbWidth = new ColumnMenuExtender.DataBoundTextBox();
			this.tbName = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.tbMimeType = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.tbFile = new System.Windows.Forms.TextBox();
			this.btnView = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgObjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 5);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 26);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(487, 5);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 26);
			this.btnSave.TabIndex = 7;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(0, 0);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 311);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(567, 37);
			this.panel1.TabIndex = 7;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dgObjects);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.tbHeight);
			this.panel2.Controls.Add(this.tbWidth);
			this.panel2.Controls.Add(this.tbName);
			this.panel2.Controls.Add(this.button1);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.tbMimeType);
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.tbFile);
			this.panel2.Controls.Add(this.btnView);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(567, 311);
			this.panel2.TabIndex = 8;
			// 
			// dgObjects
			// 
			this.dgObjects.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgObjects.CaptionVisible = false;
			this.dgObjects.DataMember = "";
			this.dgObjects.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgObjects.Location = new System.Drawing.Point(240, 111);
			this.dgObjects.Name = "dgObjects";
			this.dgObjects.Size = new System.Drawing.Size(384, 120);
			this.dgObjects.TabIndex = 48;
			this.dgObjects.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgObjects;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.extendedDataGridSelectorColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.formattableTextBoxColumn1.Width = 150;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Тип объекта";
			this.formattableTextBoxColumn2.MappingName = "label";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// extendedDataGridSelectorColumn1
			// 
			this.extendedDataGridSelectorColumn1.FieldName = null;
			this.extendedDataGridSelectorColumn1.FilterFieldName = null;
			this.extendedDataGridSelectorColumn1.MappingName = "button";
			this.extendedDataGridSelectorColumn1.NullText = "";
			this.extendedDataGridSelectorColumn1.Width = 20;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 166);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 19);
			this.label5.TabIndex = 47;
			this.label5.Text = "Высота:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 129);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 19);
			this.label4.TabIndex = 46;
			this.label4.Text = "Ширина:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbHeight
			// 
			this.tbHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbHeight.BoundProp = null;
			this.tbHeight.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbHeight.IsCurrency = false;
			this.tbHeight.Location = new System.Drawing.Point(80, 166);
			this.tbHeight.Name = "tbHeight";
			this.tbHeight.ReadOnly = true;
			this.tbHeight.Size = new System.Drawing.Size(64, 20);
			this.tbHeight.TabIndex = 45;
			// 
			// tbWidth
			// 
			this.tbWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbWidth.BoundProp = null;
			this.tbWidth.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbWidth.IsCurrency = false;
			this.tbWidth.Location = new System.Drawing.Point(80, 129);
			this.tbWidth.Name = "tbWidth";
			this.tbWidth.ReadOnly = true;
			this.tbWidth.Size = new System.Drawing.Size(64, 20);
			this.tbWidth.TabIndex = 44;
			// 
			// tbName
			// 
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(80, 55);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(544, 20);
			this.tbName.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(552, 18);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(72, 24);
			this.button1.TabIndex = 4;
			this.button1.Text = "Выбрать";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 55);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 19);
			this.label2.TabIndex = 31;
			this.label2.Text = "Название:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbMimeType
			// 
			this.tbMimeType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbMimeType.Location = new System.Drawing.Point(80, 92);
			this.tbMimeType.Name = "tbMimeType";
			this.tbMimeType.ReadOnly = true;
			this.tbMimeType.Size = new System.Drawing.Size(144, 20);
			this.tbMimeType.TabIndex = 40;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 92);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(72, 19);
			this.label6.TabIndex = 36;
			this.label6.Text = "MIME тип:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 18);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 19);
			this.label3.TabIndex = 39;
			this.label3.Text = "Файл:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tbFile
			// 
			this.tbFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbFile.Location = new System.Drawing.Point(80, 18);
			this.tbFile.Name = "tbFile";
			this.tbFile.Size = new System.Drawing.Size(472, 20);
			this.tbFile.TabIndex = 3;
			// 
			// btnView
			// 
			this.btnView.Location = new System.Drawing.Point(80, 203);
			this.btnView.Name = "btnView";
			this.btnView.Size = new System.Drawing.Size(152, 27);
			this.btnView.TabIndex = 5;
			this.btnView.Text = "Просмотр";
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(240, 92);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(128, 27);
			this.label1.TabIndex = 49;
			this.label1.Text = "Объекты картинки";
			// 
			// EditBinaryData
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(567, 348);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "EditBinaryData";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Картинка";
			this.Load += new System.EventHandler(this.fmEditBinaryData_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgObjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
