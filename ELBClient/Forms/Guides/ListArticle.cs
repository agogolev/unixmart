﻿using System;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListArticle.
	/// </summary>
	public partial class ListArticle : ListForm
	{


		/// <summary>
		/// Required designer variable.
		/// </summary>


		public ListArticle()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void LoadRights()
		{
            if (MainFrm.IsGroupMember(ELBExpImp.Constants.AdminOID))
			{
			}
            else if (MainFrm.IsGroupMember(ELBExpImp.Constants.ContentManagerOID))
			{
				mainDataGrid1.dataGridISM1.CreateAuxFilter("articleType", "articleType_filter", MetaData.FilterVerb.Equal, false, new object[] { ELBExpImp.Constants.ReviewTypeArticle });
			}
		}

		private void mainDataGrid1_BeforeLoad(object sender, EventArgs e)
		{
			LoadRights();
		}
	}
}
