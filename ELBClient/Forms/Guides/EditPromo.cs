﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	public partial class EditPromo : EditForm
	{
		private Dialogs.Types _imgSize = new Dialogs.Types();

		public EditPromo()
		{
			InitializeComponent();
			Object = new CPromo();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveObject();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			if (CheckImageSize())
				this.Close();
		}

		private void BindFields()
		{
			tbName.DataBindings.Add("Text", Object, "Name");
			tbUrl.DataBindings.Add("Text", Object, "Url");
			tbDescription.DataBindings.Add("Text", Object, "Description");
			txtPrice.DataBindings.Add("BoundProp", Object, "Price");
			stbImage.DataBindings.Add("BoundProp", Object, "Image");
			stbArticle.DataBindings.Add("BoundProp", Object, "Article");
			cbImageOnly.DataBindings.Add("Checked", Object, "ImageOnly");
		}

		private void fmEditPromo_Load(object sender, System.EventArgs e)
		{
			XmlDocument doc = GetResource("fmEditPromo.xml");
			XmlNodeList nl = doc.DocumentElement.SelectNodes("images/image");
			foreach (XmlNode n in nl)
			{
				Dialogs.ImageDescription descr = new Dialogs.ImageDescription();
				descr.Width = int.Parse(n.Attributes["width"].Value);
				descr.Height = int.Parse(n.Attributes["height"].Value);
				descr.OrdValue = n.InnerText;
				_imgSize.Add(descr);
			}
			LoadObject("name,url,image,article,description,price,imageOnly", "pressReleases");
			BindFields();
			if (((CPromo)Object).Image != null && ((CPromo)Object).Image.OID != Guid.Empty)
			{
				stbImage_Changed(sender, e);
			}
		}

		private void stbImage_Changed(object sender, System.EventArgs e)
		{
			string adr = ELBClient.Classes.ServiceUtility.GetBrowseDir() + "BrowseImage.aspx?OID=" + stbImage.BoundProp.OID;
			webBrowser1.Navigate(adr);
		}

		private void dgArticles_AddClick(object sender, System.EventArgs e)
		{
			Forms.Dialogs.ListDialog fm = new Forms.Dialogs.ListDialog("CArticle", new string[] { "header", "pubDate" }, new string[] { "Название", "Дата" });
			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				Guid o = fm.SelectedOID;
				string s = fm.SelectedNK;
				int ordValue = GetLastValue();

				((CObject)Object).PressReleases.Rows.Add(new object[] { o, s, "CArticle", ordValue });
			}
		}
		private int GetLastValue()
		{
			int ordValue = 0;
			foreach (DataRow dr in ((CObject)Object).PressReleases.Rows)
			{
				if (dr.RowState != DataRowState.Deleted && dr["ordValue"] != DBNull.Value
					&& (int)dr["ordValue"] > ordValue)
					ordValue = (int)dr["ordValue"];
			}
			return ++ordValue;
		}
		private bool CheckImageSize()
		{
			if (stbImage.BoundProp.OID == Guid.Empty)
			{
				//MessageBox.Shows(this, "Промо-блок без картинки можен некорректно отображаться на сайте", "Внимание!");
				return true;
			}
			CBinaryData img = new CBinaryData();
			img.LoadXml(op.GetObject(stbImage.BoundProp.OID.ToString(), "width,height", null));
			string ordValue = _imgSize.GetCandidate(img.Width, img.Height);
			if (ordValue == null)
			{
				MessageBox.Show(this, "Картинка не соответсвует разрешённому размеру", "Внимание!");
				return false;
			}
			else return true;

		}
		protected override void SaveObject()
		{
			if (!CheckImageSize())
			{
				return;
			}
			base.SaveObject();
		}
	}
}
