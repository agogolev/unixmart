﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using ColumnMenuExtender;
using ELBClient.BusinessProvider;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using ELBClient.ListProvider;
using MetaData;
using Telerik.WinControls.UI;
using Constants = ELBExpImp.Constants;
using ExceptionISM = ELBClient.BusinessProvider.ExceptionISM;

namespace ELBClient.Forms.Guides
{
    /// <summary>
    ///     Summary description for fmEditOrder.
    /// </summary>
    public partial class EditOrder : EditForm
    {
        private readonly BusinessProvider.BusinessProvider bp = ServiceUtility.BusinessProvider;

        public EditOrder()
        {
            InitializeComponent();
            Object = new COrder();
            GoodsGrid.ThemeName = ThemeName;
        }

        public int ShopType { get; set; }
        public bool DeliveryPriceChanged { get; set; }
        public bool HasWholesale { get; set; }
        public bool IsDiscount { get; set; }

        protected override bool IsModified
        {
            get { return dataSetGoods.IsModified || base.IsModified; }
        }

        private void EditOrder_Load(object sender, EventArgs e)
        {
            try
            {
                DetermineWholesalePossibility();
                if (HasWholesale)
                {
                    var dt = new DataTable("Table1");
                    dt.Columns.Add("ordValue", typeof (int));
                    dt.Columns.Add("dateOccur", typeof (DateTime));
                    dt.Columns.Add("amount", typeof (int));
                    dt.Columns.Add("price", typeof (decimal));
                    dt.Columns.Add("yourPrice", typeof (decimal));
                    dataSetGoods.Tables.Add(dt);
                }
                else
                {
                    GoodsGrid.Templates.Clear();
                }
                ProcessRights();
                if (ObjectOID == Guid.Empty) InitEmptyObject();
                else LoadObjects();

                LoadLookupFields();

                if ((Object as COrder).Operator.OID == Guid.Empty)
                    (Object as COrder).Operator = new ObjectItem(((MainFrm) MdiParent ?? (MainFrm) Owner).Manager,
                        ((MainFrm) MdiParent ?? (MainFrm) Owner).ManagerName, "CPeople");
                BindFields();
                RecalcTotalSum();
                if ((Object as COrder).IsSendToSV)
                {
                    DisableSentOrder();
                }
                dataSetGoods.Table.ColumnChanged += Table_ColumnChanged;
                dataSetGoods.Table.RowDeleted += Table_RowDeleted;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                throw;
            }
        }

        private void DetermineWholesalePossibility()
        {
            const string sql = "SELECT optionValue FROM t_Option WHERE optionId = 3";
            var value =
                new DataSetISM(lp.GetDataSetSql(sql)).Table.AsEnumerable()
                    .Select(row => row.Field<string>("optionValue"))
                    .SingleOrDefault();
            HasWholesale = value == "1";
        }

        private void ProcessRights()
        {
            if (!MainFrm.IsGroupMember(Constants.AdminOID)) EMailText.ReadOnly = true;
        }

        private void DisableSentOrder()
        {
            GoodsGrid.Enabled = false;
            btnAdd.Enabled = false;
            btnDel.Enabled = false;
            DeliveryPriceText.Enabled = false;
            DeliveryList.Enabled = false;
            DocType.Enabled = false;
            DeliveryDate.Enabled = false;
            DeliveryPeriod.Enabled = false;
            TypePay.Enabled = false;
        }

        private void InitEmptyObject()
        {
            var item = Object as COrder;
            item.ShopType = ShopType;
            item.SourceType = 2; // Создан в офисе
            item.DeliveryPrice = 0;
        }

        private void Table_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
            RecalcDeliveryPrice();
            RecalcTotalSum();
        }

        //		private Hashtable objLookupFields;
        private void LoadLookupFields()
        {
            var xml = lp.GetLookupValues("COrder");
            StatusList.LoadXml(xml, "statusType");
            LiftList.LoadXml(xml, "liftTypeN");
            //DeliveryList.LoadXml(xml, "deliveryTypeN");
            TypePay.LoadXml(xml, "typePaySV");
            DeliveryPeriod.LoadXml(xml, "deliveryPeriodN");
            DocType.LoadXml(xml, "docTypeN");
            SourceTypeSelect.LoadXml(xml, "sourceTypeN");
            const string sql = @"
SELECT
	*
FROM
	t_Metro
WHERE
	shopType = {0}

SELECT
	*
FROM
	t_TypePay
WHERE
	shopType = {0}

SELECT
	*
FROM
	t_TypeDelivery
WHERE
	shopTypeCMS = {0}
";
            var ds = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ShopType)));
            MetroList.AddItems(
                ds.Table.AsEnumerable()
                    .Select(row => new ListItem {Value = row.Field<int>("metroID"), Text = row.Field<string>("name")})
                    .ToArray());
            PayType.AddItems(
                ds.Tables[1].AsEnumerable()
                    .Select(row => new ListItem {Value = row.Field<int>("payType"), Text = row.Field<string>("name")})
                    .ToArray());
            DeliveryList.AddItems(
                ds.Tables[2].AsEnumerable()
                    .Select(
                        row => new ListItem {Value = row.Field<int>("deliveryType"), Text = row.Field<string>("name")})
                    .ToArray());
        }

        private void BindFields()
        {
            OperatorText.DataBindings.Add("BoundProp", Object, "Operator");
            PersonText.DataBindings.Add("Text", Object, "Person");
            PhoneText.DataBindings.Add("Text", Object, "Phone");
            SecondPhone.DataBindings.Add("Text", Object, "SecondPhone");
            EMailText.DataBindings.Add("Text", Object, "EMail");
            City.DataBindings.Add("Text", Object, "City");
            StreetText.DataBindings.Add("Text", Object, "StreetName");
            HouseText.DataBindings.Add("Text", Object, "House");
            BuildText.DataBindings.Add("Text", Object, "Build");
            KorpText.DataBindings.Add("Text", Object, "Korp");
            FlatText.DataBindings.Add("Text", Object, "Flat");
            CodeText.DataBindings.Add("Text", Object, "Code");
            LevelText.DataBindings.Add("Text", Object, "Level");
            PodyezdText.DataBindings.Add("Text", Object, "Podyezd");
            MetroList.DataBindings.Add("SelectedValue", Object, "metroID");
            LiftList.DataBindings.Add("SelectedValue", Object, "liftType");
            PayType.DataBindings.Add("SelectedValue", Object, "PayType");
            TypePay.DataBindings.Add("SelectedValue", Object, "TypePay");
            DeliveryList.DataBindings.Add("SelectedValue", Object, "deliveryType");
            var deliveryPriceBinding = new Binding("BoundProp", Object, "DeliveryPrice");
            DeliveryPriceText.DataBindings.Add(deliveryPriceBinding);
            deliveryPriceBinding.Parse += deliveryPriceBinding_Parse;
            StatusList.DataBindings.Add("SelectedValue", Object, "Status");
            Comment.DataBindings.Add("Text", Object, "Comment");
            DeliveryDate.DataBindings.Add("BoundValue", Object, "DeliveryDate");
            DeliveryPeriod.DataBindings.Add("SelectedValue", Object, "DeliveryPeriod");
            SourceTypeSelect.DataBindings.Add("SelectedValue", Object, "SourceType");
            DocType.DataBindings.Add("SelectedValue", Object, "DocType");

            Text += (Object as COrder).OrderNum.ToString();
        }

        private void LoadObjects()
        {
            var sql = "SELECT shopType FROM t_Order WHERE OID = '" + ObjectOID + "'";
            var ds = new DataSetISM(lp.GetDataSetSql(sql));
            if (ds.Table.Rows.Count != 0) ShopType = (int) ds.Table.Rows[0][0];

            //Loading Embedded Resource
            var xml = GetResource("fmEditOrder.xml").OuterXml;
            xml = xml.Replace("%SHOPTYPE%", ShopType.ToString());

            if (HasWholesale)
            {
                var xdoc = XDocument.Parse(xml);
                xdoc.Descendants("multi")
                    .Single()
                    .Add(new XElement("field", new XAttribute("name", "goods.yourPrice"),
                        new XAttribute("columnName", "yourPrice")));
                xml = xdoc.ToString();
            }
            Object.LoadMultiPropFlag = false;
            xml = LoadObject(xml, COrder.ClassCID);
            if (dataSetGoods.Table.Columns.Contains("orderNum"))
            {
                dataSetGoods.Table.Columns["orderNum"].Expression = (Object as COrder).OrderNum.ToString();
            }
            dataSetGoods.LoadRowsFromXML("Table", xml, "goods");
            if (HasWholesale)
            {
                ds = new DataSetISM(
                    lp.GetDataSetSqlParams("SELECT * FROM t_OrderPriceHistory WHERE OID = @OID",
                        new[]
                        {
                            new NameValuePair {Name = "OID", Value = Object.OID.ToString()}
                        }));
                ds.Table.AsEnumerable()
                    .ToList()
                    .ForEach(row =>
                    {
                        var dr = dataSetGoods.Tables[1].NewRow();
                        dr["ordValue"] = row.Field<int>("ordValue");
                        dr["amount"] = row.Field<int>("amount");
                        dr["dateOccur"] = row.Field<DateTime>("dateOccur");
                        dr["price"] = row.Field<decimal>("price");
                        if (row.Field<decimal?>("yourPrice").HasValue)
                            dr["yourPrice"] = row.Field<decimal>("yourPrice");
                        dataSetGoods.Tables[1].Rows.Add(dr);
                    });
                dataSetGoods.AcceptChanges();
            }
            GoodsGrid.DataSource = dataSetGoods;
            GoodsGrid.DataMember = "Table";
            if (HasWholesale)
            {
                gridViewTemplate1.DataSource = dataSetGoods;
                gridViewTemplate1.DataMember = "Table1";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveObject();
        }

        protected override void SaveObject()
        {
            if (dataSetGoods.IsModified)
                Object.ExtMultiProp = dataSetGoods.GetChangesXml("goods");
            ((CObject) Object).Owner = new ObjectItem(Guid.Empty, "", "CPeople");
            ((CObject) Object).Owner = new ObjectItem(((MainFrm) MdiParent ?? (MainFrm) Owner).Manager,
                ((MainFrm) MdiParent ?? (MainFrm) Owner).ManagerName, "CPeople");
            base.SaveObject();
            dataSetGoods.AcceptChanges();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnShowVersions_Click(object sender, EventArgs e)
        {
            var fm = new ShowVersions {ObjectOID = ObjectOID};
            fm.ShowDialog(this);
        }

        //Customizing DataGrid

        private void btnDel_Click(object sender, EventArgs e)
        {
            var dr = GoodsGrid.SelectedRows.First();
            if (dr != null) dr.Delete();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var fm = new ListGoodsDialog {ShopType = ShopType};
            if (fm.ShowDialog(this) == DialogResult.OK)
            {
                foreach (var dr in fm.SelectedRows)
                {
                    if (dr != null)
                    {
                        var o = (Guid) dr["OID"];
                        var id = (int) dr["ID"];
                        var s = dr["shortName"].ToString();
                        var price = (decimal) dr["price"];
                        var ordValue = newOrdValue();
                        dataSetGoods.Table.Rows.Add(ordValue, o, s, 1, id, price);
                    }
                }

                RecalcDeliveryPrice();
                RecalcTotalSum();
            }
        }

        private int newOrdValue()
        {
            var i = 0;
            var dv = new DataView(dataSetGoods.Table, null, "ordValue DESC", DataViewRowState.CurrentRows);
            if (dv.Count != 0)
            {
                if (dv.Count >= 2 && (int) dv[0]["ordValue"] > 999) i = (int) dv[1]["ordValue"];
                else if (dv.Count == 1 && (int) dv[0]["ordValue"] > 999) i = 0;
                else i = (int) dv[0]["ordValue"];
            }
            i++;
            return i;
        }

        private void RecalcTotalSum()
        {
            var deliveryPrice = (Object as COrder).DeliveryPrice;
            var totalSum =
                dataSetGoods.Table.Rows.Cast<DataRow>()
                    .Where(dr => dr.RowState != DataRowState.Deleted)
                    .Aggregate(0, (current, dr) => (int) (current + (decimal) dr["price"]*(int) dr["amount"]));
            TotalPriceLabel.Text = string.Format("Общая стоимость заказа: {0:#,0.00}", totalSum + deliveryPrice);
        }

        private void Table_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            RecalcDeliveryPrice();
        }

        private void RecalcDeliveryPrice()
        {
            ExceptionISM ex;
            var content = (from row in dataSetGoods.Table.AsEnumerable()
                where row.RowState != DataRowState.Deleted
                select new BasketContent
                {
                    Goods = row.Field<Guid>("goodsOID"),
                    OrdValue = row.Field<int>("ordValue"),
                    Amount = row.Field<int>("amount"),
                    Price = row.Field<decimal>("price")
                }).ToArray();
            var newDeliveryPrice = bp.CalcDeliveryPrice((Object as COrder).DeliveryType, content, out ex);
            if (ex != null)
            {
                ShowError("Ошибка расчёта текущей цены доставки, попробуйте установить вручную");
                return;
            }

            (Object as COrder).DeliveryPrice = newDeliveryPrice;
            DeliveryPriceText.BoundProp = newDeliveryPrice;
        }

        private void ExportToSV_Click(object sender, EventArgs e)
        {
            var ord = Object as COrder;
            if (ord.DeliveryDate == DateTime.MinValue)
            {
                ShowError("Выберите дату доставки");
                return;
            }
            if (ord.DeliveryPeriod == int.MinValue)
            {
                ShowError("Выберите интервал доставки");
                return;
            }
            if (Object.IsModified)
            {
                ShowError("Сначала сохраните объект");
                return;
            }

            var ex = bp.ExportOrder((Object as COrder).OrderNum, (Object as COrder).ShopType);
            if (ex != null)
            {
                ShowError(ex.LiteralMessage);
                return;
            }

            DisableSentOrder();
            MessageBox.Show(this, @"Экспорт в SV завершён");
            ReloadGrid();
        }

        private void deliveryPriceBinding_Parse(object sender, ConvertEventArgs e)
        {
            DeliveryPriceChanged = true;
        }

        private void DeliveryPriceText_Validated(object sender, EventArgs e)
        {
            if (DeliveryPriceChanged)
            {
                RecalcTotalSum();
                DeliveryPriceChanged = false;
            }
        }

        private void GoodsGrid1_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            var row = e.RowElement.RowInfo;
            if (row.Cells["price"] == null) return;
            var price = (decimal) row.Cells["price"].Value;
            var yourPrice = row.Cells["yourPrice"].Value != DBNull.Value
                ? (decimal) row.Cells["yourPrice"].Value
                : (decimal?) null;
            var amount = (int) row.Cells["amount"].Value;
            if (yourPrice.HasValue && price == yourPrice)
            {
                e.RowElement.ForeColor = Color.Green;
                return;
            }
            if (!row.HasChildRows()) return;
            var child = row.ChildRows;
            var lastChild =
                child.OrderByDescending(row1 => (DateTime) row1.Cells["dateOccur"].Value).First();
            var yourPriceOld = lastChild.Cells["yourPrice"].Value != DBNull.Value
                ? (decimal) lastChild.Cells["yourPrice"].Value
                : (decimal?) null;
            var priceOld = (decimal) lastChild.Cells["price"].Value;
            var amountOld = (int) lastChild.Cells["amount"].Value;
            if (yourPrice != yourPriceOld || amount != amountOld || price != priceOld)
            {
                e.RowElement.ForeColor = Color.Red;
            }
        }
    }
}