﻿using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Guides
{
	partial class AppSettingsEdit
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbShopType = new System.Windows.Forms.Label();
			this.cbShopType = new ColumnMenuExtender.DataBoundComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.Sales_NotesText = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// lbShopType
			// 
			this.lbShopType.Location = new System.Drawing.Point(14, 11);
			this.lbShopType.Name = "lbShopType";
			this.lbShopType.Size = new System.Drawing.Size(64, 23);
			this.lbShopType.TabIndex = 7;
			this.lbShopType.Text = "Магазин:";
			this.lbShopType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbShopType
			// 
			this.cbShopType.Location = new System.Drawing.Point(86, 12);
			this.cbShopType.Name = "cbShopType";
			this.cbShopType.SelectedValue = -1;
			this.cbShopType.Size = new System.Drawing.Size(184, 21);
			this.cbShopType.TabIndex = 6;
            this.cbShopType.SelectedIndexChanged += new PositionChangedEventHandler(this.cbShopType_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(14, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(85, 23);
			this.label1.TabIndex = 8;
			this.label1.Text = "Sales Notes:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Sales_NotesText
			// 
			this.Sales_NotesText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Sales_NotesText.Location = new System.Drawing.Point(86, 50);
			this.Sales_NotesText.Name = "Sales_NotesText";
			this.Sales_NotesText.Size = new System.Drawing.Size(542, 20);
			this.Sales_NotesText.TabIndex = 9;
			this.Sales_NotesText.TextChanged += new System.EventHandler(this.Sales_NotesText_TextChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 236);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(640, 37);
			this.panel1.TabIndex = 33;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(4, 5);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(562, 5);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 9;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// AppSettingsEdit
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(640, 273);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.Sales_NotesText);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lbShopType);
			this.Controls.Add(this.cbShopType);
			this.Name = "AppSettingsEdit";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Настройки магазина";
			this.Load += new System.EventHandler(this.AppSettingsEdit_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lbShopType;
		private ColumnMenuExtender.DataBoundComboBox cbShopType;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox Sales_NotesText;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
	}
}