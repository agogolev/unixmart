using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	public partial class EditOrder
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TextBox PhoneText;
		private System.Windows.Forms.TextBox StreetText;
		private System.Windows.Forms.TextBox EMailText;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox PersonText;
		private ColumnMenuExtender.DataBoundComboBox PayType;
		private System.Windows.Forms.Label label10;
		private ELBClient.UserControls.SelectorTextBox OperatorText;
		private System.Windows.Forms.Button btnDel;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Label label13;
		private ColumnMenuExtender.DataBoundComboBox StatusList;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox HouseText;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox KorpText;
		private System.Windows.Forms.TextBox FlatText;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button btnShowVersions;
		private TextBox BuildText;
		private Label label1;
		private TextBox PodyezdText;
		private TextBox LevelText;
		private Label label5;
		private TextBox CodeText;
		private Label label7;
		private Label label3;
		private Label label14;
		private Label label12;
		private ColumnMenuExtender.DataBoundComboBox LiftList;
		private DataBoundComboBox MetroList;
		private ColumnMenuExtender.DataBoundTextBox DeliveryPriceText;
		private Label label19;
		private DataBoundComboBox DeliveryList;
		private Label label15;
		private TextBox SecondPhone;
		private TextBox City;
		private Label label20;
		private MetaData.DataSetISM dataSetGoods;
		private System.Data.DataTable dataTableGoods;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataColumn dataColumn5;
		private System.Data.DataColumn dataColumn6;
		private Button ExportToSV;
		private TextBox Comment;
		private Label label21;
		private Label TotalPriceLabel;
		private UserControls.ExtDateTimePicker DeliveryDate;
		private Label label22;
		private System.Data.DataColumn dataColumn7;
		private DataBoundComboBox TypePay;
		private Label label24;
		private DataBoundComboBox DeliveryPeriod;
		private Label label23;
		private DataBoundComboBox DocType;
		private Label label25;
		private DataBoundComboBox SourceTypeSelect;
		private Label label27;

		private void InitializeComponent()
		{
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
			Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
			Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
			Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn5 = new Telerik.WinControls.UI.GridViewDecimalColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewRelation gridViewRelation1 = new Telerik.WinControls.UI.GridViewRelation();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditOrder));
			this.gridViewTemplate1 = new Telerik.WinControls.UI.GridViewTemplate();
			this.GoodsGrid = new Telerik.WinControls.UI.RadGridView();
			this.dataSetGoods = new MetaData.DataSetISM();
			this.dataTableGoods = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.TotalPriceLabel = new System.Windows.Forms.Label();
			this.SourceTypeSelect = new ColumnMenuExtender.DataBoundComboBox();
			this.label27 = new System.Windows.Forms.Label();
			this.DocType = new ColumnMenuExtender.DataBoundComboBox();
			this.City = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.TypePay = new ColumnMenuExtender.DataBoundComboBox();
			this.label24 = new System.Windows.Forms.Label();
			this.DeliveryPeriod = new ColumnMenuExtender.DataBoundComboBox();
			this.label23 = new System.Windows.Forms.Label();
			this.DeliveryDate = new ELBClient.UserControls.ExtDateTimePicker();
			this.label22 = new System.Windows.Forms.Label();
			this.Comment = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.SecondPhone = new System.Windows.Forms.TextBox();
			this.DeliveryPriceText = new ColumnMenuExtender.DataBoundTextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.DeliveryList = new ColumnMenuExtender.DataBoundComboBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.LiftList = new ColumnMenuExtender.DataBoundComboBox();
			this.MetroList = new ColumnMenuExtender.DataBoundComboBox();
			this.PodyezdText = new System.Windows.Forms.TextBox();
			this.LevelText = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.CodeText = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.BuildText = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.FlatText = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.KorpText = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.HouseText = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.StatusList = new ColumnMenuExtender.DataBoundComboBox();
			this.label13 = new System.Windows.Forms.Label();
			this.OperatorText = new ELBClient.UserControls.SelectorTextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.PayType = new ColumnMenuExtender.DataBoundComboBox();
			this.PersonText = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.EMailText = new System.Windows.Forms.TextBox();
			this.PhoneText = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.StreetText = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnShowVersions = new System.Windows.Forms.Button();
			this.ExportToSV = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.btnDel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			((System.ComponentModel.ISupportInitialize)(this.gridViewTemplate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetGoods)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableGoods)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel5.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// gridViewTemplate1
			// 
			this.gridViewTemplate1.AllowAddNewRow = false;
			this.gridViewTemplate1.AutoGenerateColumns = false;
			gridViewTextBoxColumn1.AllowResize = false;
			gridViewTextBoxColumn1.EnableExpressionEditor = false;
			gridViewTextBoxColumn1.FieldName = "ordValue";
			gridViewTextBoxColumn1.HeaderText = "№";
			gridViewTextBoxColumn1.IsVisible = false;
			gridViewTextBoxColumn1.MinWidth = 0;
			gridViewTextBoxColumn1.Name = "ordValue";
			gridViewTextBoxColumn2.EnableExpressionEditor = false;
			gridViewTextBoxColumn2.FieldName = "dateOccur";
			gridViewTextBoxColumn2.HeaderText = "Дата";
			gridViewTextBoxColumn2.Name = "dateOccur";
			gridViewTextBoxColumn2.Width = 150;
			gridViewTextBoxColumn3.EnableExpressionEditor = false;
			gridViewTextBoxColumn3.FieldName = "amount";
			gridViewTextBoxColumn3.HeaderText = "Кол-во";
			gridViewTextBoxColumn3.Name = "amount";
			gridViewTextBoxColumn4.EnableExpressionEditor = false;
			gridViewTextBoxColumn4.FieldName = "price";
			gridViewTextBoxColumn4.FormatString = "{0:#,#0}";
			gridViewTextBoxColumn4.HeaderText = "Цена";
			gridViewTextBoxColumn4.Name = "price";
			gridViewTextBoxColumn4.Width = 100;
			gridViewTextBoxColumn5.EnableExpressionEditor = false;
			gridViewTextBoxColumn5.FieldName = "yourPrice";
			gridViewTextBoxColumn5.FormatString = "{0:#,#0}";
			gridViewTextBoxColumn5.HeaderText = "Цена клиента";
			gridViewTextBoxColumn5.Name = "yourPrice";
			gridViewTextBoxColumn5.Width = 100;
			this.gridViewTemplate1.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
			this.gridViewTemplate1.ShowRowHeaderColumn = false;
			// 
			// GoodsGrid
			// 
			this.GoodsGrid.BackColor = System.Drawing.SystemColors.Control;
			this.GoodsGrid.Cursor = System.Windows.Forms.Cursors.Default;
			this.GoodsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GoodsGrid.Font = new System.Drawing.Font("Segoe UI", 8.25F);
			this.GoodsGrid.ForeColor = System.Drawing.Color.Black;
			this.GoodsGrid.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.GoodsGrid.Location = new System.Drawing.Point(3, 3);
			// 
			// GoodsGrid
			// 
			this.GoodsGrid.MasterTemplate.AllowAddNewRow = false;
			this.GoodsGrid.MasterTemplate.AutoGenerateColumns = false;
			gridViewDecimalColumn1.EnableExpressionEditor = false;
			gridViewDecimalColumn1.FieldName = "ordValue";
			gridViewDecimalColumn1.HeaderText = "№";
			gridViewDecimalColumn1.Name = "ordValue";
			gridViewDecimalColumn1.ReadOnly = true;
			gridViewDecimalColumn2.EnableExpressionEditor = false;
			gridViewDecimalColumn2.FieldName = "ID";
			gridViewDecimalColumn2.HeaderText = "Код";
			gridViewDecimalColumn2.Name = "ID";
			gridViewDecimalColumn2.ReadOnly = true;
			gridViewDecimalColumn2.Width = 100;
			gridViewTextBoxColumn6.EnableExpressionEditor = false;
			gridViewTextBoxColumn6.FieldName = "shortName";
			gridViewTextBoxColumn6.HeaderText = "Название";
			gridViewTextBoxColumn6.Name = "shortName";
			gridViewTextBoxColumn6.ReadOnly = true;
			gridViewTextBoxColumn6.Width = 200;
			gridViewDecimalColumn3.EnableExpressionEditor = false;
			gridViewDecimalColumn3.FieldName = "amount";
			gridViewDecimalColumn3.HeaderText = "Кол-во";
			gridViewDecimalColumn3.Name = "amount";
			gridViewDecimalColumn4.EnableExpressionEditor = false;
			gridViewDecimalColumn4.FieldName = "price";
			gridViewDecimalColumn4.FormatString = "{0:#,#0}";
			gridViewDecimalColumn4.HeaderText = "Цена";
			gridViewDecimalColumn4.Name = "price";
			gridViewDecimalColumn4.Width = 100;
			gridViewDecimalColumn5.EnableExpressionEditor = false;
			gridViewDecimalColumn5.FieldName = "yourPrice";
			gridViewDecimalColumn5.FormatString = "{0:#,#0}";
			gridViewDecimalColumn5.HeaderText = "Цена клиента";
			gridViewDecimalColumn5.Name = "yourPrice";
			gridViewDecimalColumn5.ReadOnly = true;
			gridViewDecimalColumn5.Width = 100;
			gridViewTextBoxColumn7.EnableExpressionEditor = false;
			gridViewTextBoxColumn7.FieldName = "masterCategory";
			gridViewTextBoxColumn7.HeaderText = "Категория";
			gridViewTextBoxColumn7.Name = "masterCategory";
			gridViewTextBoxColumn7.ReadOnly = true;
			gridViewTextBoxColumn7.Width = 300;
			this.GoodsGrid.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewTextBoxColumn6,
            gridViewDecimalColumn3,
            gridViewDecimalColumn4,
            gridViewDecimalColumn5,
            gridViewTextBoxColumn7});
			this.GoodsGrid.MasterTemplate.EnableGrouping = false;
			this.GoodsGrid.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridViewTemplate1});
			this.GoodsGrid.Name = "GoodsGrid";
			gridViewRelation1.ChildColumnNames = ((System.Collections.Specialized.StringCollection)(resources.GetObject("gridViewRelation1.ChildColumnNames")));
			gridViewRelation1.ChildTemplate = this.gridViewTemplate1;
			gridViewRelation1.ParentColumnNames = ((System.Collections.Specialized.StringCollection)(resources.GetObject("gridViewRelation1.ParentColumnNames")));
			gridViewRelation1.ParentTemplate = this.GoodsGrid.MasterTemplate;
			this.GoodsGrid.Relations.AddRange(new Telerik.WinControls.UI.GridViewRelation[] {
            gridViewRelation1});
			this.GoodsGrid.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.GoodsGrid.Size = new System.Drawing.Size(741, 454);
			this.GoodsGrid.TabIndex = 29;
			this.GoodsGrid.Text = "radGridView1";
			this.GoodsGrid.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.GoodsGrid1_RowFormatting);
			// 
			// dataSetGoods
			// 
			this.dataSetGoods.DataSetName = "NewDataSet";
			this.dataSetGoods.Locale = new System.Globalization.CultureInfo("ru-RU");
			this.dataSetGoods.OnlyRowsWithoutErrors = false;
			this.dataSetGoods.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableGoods});
			// 
			// dataTableGoods
			// 
			this.dataTableGoods.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8});
			this.dataTableGoods.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ordValue"}, true)});
			this.dataTableGoods.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
			this.dataTableGoods.TableName = "Table";
			// 
			// dataColumn1
			// 
			this.dataColumn1.AllowDBNull = false;
			this.dataColumn1.ColumnName = "ordValue";
			this.dataColumn1.DataType = typeof(int);
			this.dataColumn1.ReadOnly = true;
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "goodsOID";
			this.dataColumn2.DataType = typeof(System.Guid);
			this.dataColumn2.ReadOnly = true;
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "shortName";
			this.dataColumn3.ReadOnly = true;
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "amount";
			this.dataColumn4.DataType = typeof(int);
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "ID";
			this.dataColumn5.DataType = typeof(long);
			this.dataColumn5.ReadOnly = true;
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "price";
			this.dataColumn6.DataType = typeof(decimal);
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "masterCategory";
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "yourPrice";
			this.dataColumn8.DataType = typeof(decimal);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.TotalPriceLabel);
			this.panel1.Controls.Add(this.SourceTypeSelect);
			this.panel1.Controls.Add(this.label27);
			this.panel1.Controls.Add(this.DocType);
			this.panel1.Controls.Add(this.City);
			this.panel1.Controls.Add(this.label25);
			this.panel1.Controls.Add(this.TypePay);
			this.panel1.Controls.Add(this.label24);
			this.panel1.Controls.Add(this.DeliveryPeriod);
			this.panel1.Controls.Add(this.label23);
			this.panel1.Controls.Add(this.DeliveryDate);
			this.panel1.Controls.Add(this.label22);
			this.panel1.Controls.Add(this.Comment);
			this.panel1.Controls.Add(this.label21);
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.SecondPhone);
			this.panel1.Controls.Add(this.DeliveryPriceText);
			this.panel1.Controls.Add(this.label19);
			this.panel1.Controls.Add(this.DeliveryList);
			this.panel1.Controls.Add(this.label15);
			this.panel1.Controls.Add(this.label14);
			this.panel1.Controls.Add(this.label12);
			this.panel1.Controls.Add(this.LiftList);
			this.panel1.Controls.Add(this.MetroList);
			this.panel1.Controls.Add(this.PodyezdText);
			this.panel1.Controls.Add(this.LevelText);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.CodeText);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.BuildText);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.FlatText);
			this.panel1.Controls.Add(this.label18);
			this.panel1.Controls.Add(this.KorpText);
			this.panel1.Controls.Add(this.label17);
			this.panel1.Controls.Add(this.HouseText);
			this.panel1.Controls.Add(this.label16);
			this.panel1.Controls.Add(this.StatusList);
			this.panel1.Controls.Add(this.label13);
			this.panel1.Controls.Add(this.OperatorText);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.PayType);
			this.panel1.Controls.Add(this.PersonText);
			this.panel1.Controls.Add(this.label11);
			this.panel1.Controls.Add(this.EMailText);
			this.panel1.Controls.Add(this.PhoneText);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.StreetText);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(771, 460);
			this.panel1.TabIndex = 17;
			// 
			// TotalPriceLabel
			// 
			this.TotalPriceLabel.Location = new System.Drawing.Point(9, 498);
			this.TotalPriceLabel.Name = "TotalPriceLabel";
			this.TotalPriceLabel.Size = new System.Drawing.Size(138, 75);
			this.TotalPriceLabel.TabIndex = 72;
			this.TotalPriceLabel.Text = "Общая стоимость заказа:";
			// 
			// SourceTypeSelect
			// 
			this.SourceTypeSelect.BackColor = System.Drawing.Color.White;
			this.SourceTypeSelect.Enabled = false;
			this.SourceTypeSelect.ForeColor = System.Drawing.Color.Black;
			this.SourceTypeSelect.Location = new System.Drawing.Point(440, 258);
			this.SourceTypeSelect.Name = "SourceTypeSelect";
			this.SourceTypeSelect.SelectedValue = -1;
			this.SourceTypeSelect.Size = new System.Drawing.Size(120, 21);
			this.SourceTypeSelect.TabIndex = 17;
			// 
			// label27
			// 
			this.label27.Location = new System.Drawing.Point(296, 261);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(144, 18);
			this.label27.TabIndex = 84;
			this.label27.Text = "Источник заказа:";
			this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// DocType
			// 
			this.DocType.BackColor = System.Drawing.Color.White;
			this.DocType.Location = new System.Drawing.Point(338, 352);
			this.DocType.Name = "DocType";
			this.DocType.SelectedValue = -1;
			this.DocType.Size = new System.Drawing.Size(222, 21);
			this.DocType.TabIndex = 22;
			// 
			// City
			// 
			this.City.Location = new System.Drawing.Point(152, 106);
			this.City.Name = "City";
			this.City.Size = new System.Drawing.Size(408, 20);
			this.City.TabIndex = 5;
			// 
			// label25
			// 
			this.label25.Location = new System.Drawing.Point(239, 354);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(93, 20);
			this.label25.TabIndex = 80;
			this.label25.Text = "Доставка SV:";
			this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TypePay
			// 
			this.TypePay.BackColor = System.Drawing.Color.White;
			this.TypePay.ForeColor = System.Drawing.Color.Black;
			this.TypePay.Location = new System.Drawing.Point(440, 290);
			this.TypePay.Name = "TypePay";
			this.TypePay.SelectedValue = -1;
			this.TypePay.Size = new System.Drawing.Size(120, 21);
			this.TypePay.TabIndex = 19;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(296, 292);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(144, 18);
			this.label24.TabIndex = 78;
			this.label24.Text = "Форма оплаты SV:";
			this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// DeliveryPeriod
			// 
			this.DeliveryPeriod.BackColor = System.Drawing.Color.White;
			this.DeliveryPeriod.Location = new System.Drawing.Point(354, 441);
			this.DeliveryPeriod.Name = "DeliveryPeriod";
			this.DeliveryPeriod.SelectedValue = -1;
			this.DeliveryPeriod.Size = new System.Drawing.Size(206, 21);
			this.DeliveryPeriod.TabIndex = 26;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(294, 442);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(58, 21);
			this.label23.TabIndex = 75;
			this.label23.Text = "Период:";
			this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// DeliveryDate
			// 
			this.DeliveryDate.BackColor = System.Drawing.Color.White;
			this.DeliveryDate.BoundValue = new System.DateTime(2012, 10, 17, 17, 54, 47, 531);
			this.DeliveryDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.DeliveryDate.LinkedTo = null;
			this.DeliveryDate.Location = new System.Drawing.Point(152, 441);
			this.DeliveryDate.Name = "DeliveryDate";
			this.DeliveryDate.ReadOnly = false;
			this.DeliveryDate.ShowButtons = true;
			this.DeliveryDate.Size = new System.Drawing.Size(136, 20);
			this.DeliveryDate.TabIndex = 25;
			this.DeliveryDate.Value = new System.DateTime(2012, 10, 17, 17, 54, 47, 531);
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(32, 443);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(120, 19);
			this.label22.TabIndex = 74;
			this.label22.Text = "Дата доставки:";
			this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Comment
			// 
			this.Comment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.Comment.Location = new System.Drawing.Point(152, 475);
			this.Comment.Multiline = true;
			this.Comment.Name = "Comment";
			this.Comment.Size = new System.Drawing.Size(408, 216);
			this.Comment.TabIndex = 27;
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(8, 475);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(144, 19);
			this.label21.TabIndex = 71;
			this.label21.Text = "Комментарий:";
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(8, 107);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(144, 19);
			this.label20.TabIndex = 70;
			this.label20.Text = "Населённый пункт:";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// SecondPhone
			// 
			this.SecondPhone.Location = new System.Drawing.Point(407, 77);
			this.SecondPhone.Name = "SecondPhone";
			this.SecondPhone.Size = new System.Drawing.Size(153, 20);
			this.SecondPhone.TabIndex = 4;
			// 
			// DeliveryPriceText
			// 
			this.DeliveryPriceText.BackColor = System.Drawing.Color.White;
			this.DeliveryPriceText.BoundProp = null;
			this.DeliveryPriceText.ForeColor = System.Drawing.Color.Black;
			this.DeliveryPriceText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.DeliveryPriceText.IsCurrency = true;
			this.DeliveryPriceText.Location = new System.Drawing.Point(152, 352);
			this.DeliveryPriceText.Name = "DeliveryPriceText";
			this.DeliveryPriceText.Size = new System.Drawing.Size(80, 20);
			this.DeliveryPriceText.TabIndex = 21;
			this.DeliveryPriceText.Validated += new System.EventHandler(this.DeliveryPriceText_Validated);
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(8, 354);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(144, 19);
			this.label19.TabIndex = 67;
			this.label19.Text = "Цена доставки";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// DeliveryList
			// 
			this.DeliveryList.BackColor = System.Drawing.Color.White;
			this.DeliveryList.ForeColor = System.Drawing.Color.Black;
			this.DeliveryList.Location = new System.Drawing.Point(152, 321);
			this.DeliveryList.Name = "DeliveryList";
			this.DeliveryList.SelectedValue = -1;
			this.DeliveryList.Size = new System.Drawing.Size(408, 21);
			this.DeliveryList.TabIndex = 20;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(8, 323);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(144, 19);
			this.label15.TabIndex = 65;
			this.label15.Text = "Доставка:";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(3, 261);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(144, 18);
			this.label14.TabIndex = 63;
			this.label14.Text = "Лифт:";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(3, 230);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(144, 18);
			this.label12.TabIndex = 62;
			this.label12.Text = "Метро:";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// LiftList
			// 
			this.LiftList.Location = new System.Drawing.Point(152, 258);
			this.LiftList.Name = "LiftList";
			this.LiftList.SelectedValue = -1;
			this.LiftList.Size = new System.Drawing.Size(120, 21);
			this.LiftList.TabIndex = 16;
			// 
			// MetroList
			// 
			this.MetroList.Location = new System.Drawing.Point(152, 227);
			this.MetroList.Name = "MetroList";
			this.MetroList.SelectedValue = -1;
			this.MetroList.Size = new System.Drawing.Size(200, 21);
			this.MetroList.TabIndex = 14;
			// 
			// PodyezdText
			// 
			this.PodyezdText.Location = new System.Drawing.Point(420, 197);
			this.PodyezdText.Name = "PodyezdText";
			this.PodyezdText.Size = new System.Drawing.Size(62, 20);
			this.PodyezdText.TabIndex = 13;
			// 
			// LevelText
			// 
			this.LevelText.Location = new System.Drawing.Point(272, 197);
			this.LevelText.Name = "LevelText";
			this.LevelText.Size = new System.Drawing.Size(80, 20);
			this.LevelText.TabIndex = 12;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(232, 200);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 18);
			this.label5.TabIndex = 57;
			this.label5.Text = "Этаж:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// CodeText
			// 
			this.CodeText.Location = new System.Drawing.Point(152, 197);
			this.CodeText.Name = "CodeText";
			this.CodeText.Size = new System.Drawing.Size(80, 20);
			this.CodeText.TabIndex = 11;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 200);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(144, 18);
			this.label7.TabIndex = 56;
			this.label7.Text = "Домофон:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// BuildText
			// 
			this.BuildText.Location = new System.Drawing.Point(420, 167);
			this.BuildText.Name = "BuildText";
			this.BuildText.Size = new System.Drawing.Size(62, 20);
			this.BuildText.TabIndex = 9;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(362, 170);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 18);
			this.label1.TabIndex = 53;
			this.label1.Text = "Стр.:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// FlatText
			// 
			this.FlatText.Location = new System.Drawing.Point(518, 167);
			this.FlatText.Name = "FlatText";
			this.FlatText.Size = new System.Drawing.Size(42, 20);
			this.FlatText.TabIndex = 10;
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(488, 170);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(24, 18);
			this.label18.TabIndex = 51;
			this.label18.Text = "Кв.:";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// KorpText
			// 
			this.KorpText.Location = new System.Drawing.Point(272, 167);
			this.KorpText.Name = "KorpText";
			this.KorpText.Size = new System.Drawing.Size(80, 20);
			this.KorpText.TabIndex = 8;
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(232, 170);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(40, 18);
			this.label17.TabIndex = 49;
			this.label17.Text = "Корп.:";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// HouseText
			// 
			this.HouseText.Location = new System.Drawing.Point(152, 167);
			this.HouseText.Name = "HouseText";
			this.HouseText.Size = new System.Drawing.Size(80, 20);
			this.HouseText.TabIndex = 7;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(8, 170);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(144, 18);
			this.label16.TabIndex = 47;
			this.label16.Text = "Дом:";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// StatusList
			// 
			this.StatusList.Location = new System.Drawing.Point(152, 410);
			this.StatusList.Name = "StatusList";
			this.StatusList.SelectedValue = -1;
			this.StatusList.Size = new System.Drawing.Size(408, 21);
			this.StatusList.TabIndex = 24;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(8, 410);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(144, 18);
			this.label13.TabIndex = 44;
			this.label13.Text = "Статус заказа:";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// OperatorText
			// 
			this.OperatorText.BoundProp = null;
			this.OperatorText.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.OperatorText.ClassName = "CPeople";
			this.OperatorText.FieldNames = null;
			this.OperatorText.FormatRows = null;
			this.OperatorText.HeaderNames = "Фамилия,Имя,Отчество";
			this.OperatorText.Location = new System.Drawing.Point(152, 382);
			this.OperatorText.Name = "OperatorText";
			this.OperatorText.RowNames = "lastName,firstName,middleName";
			this.OperatorText.Size = new System.Drawing.Size(408, 28);
			this.OperatorText.TabIndex = 23;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(8, 382);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(144, 18);
			this.label10.TabIndex = 42;
			this.label10.Text = "Оператор:";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// PayType
			// 
			this.PayType.Location = new System.Drawing.Point(152, 290);
			this.PayType.Name = "PayType";
			this.PayType.SelectedValue = -1;
			this.PayType.Size = new System.Drawing.Size(120, 21);
			this.PayType.TabIndex = 18;
			// 
			// PersonText
			// 
			this.PersonText.Location = new System.Drawing.Point(152, 15);
			this.PersonText.Name = "PersonText";
			this.PersonText.Size = new System.Drawing.Size(408, 20);
			this.PersonText.TabIndex = 1;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(8, 17);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(144, 19);
			this.label11.TabIndex = 35;
			this.label11.Text = "Контактное лицо:";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// EMailText
			// 
			this.EMailText.Location = new System.Drawing.Point(152, 45);
			this.EMailText.Name = "EMailText";
			this.EMailText.Size = new System.Drawing.Size(408, 20);
			this.EMailText.TabIndex = 2;
			// 
			// PhoneText
			// 
			this.PhoneText.Location = new System.Drawing.Point(152, 77);
			this.PhoneText.Name = "PhoneText";
			this.PhoneText.Size = new System.Drawing.Size(157, 20);
			this.PhoneText.TabIndex = 3;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 140);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(144, 18);
			this.label8.TabIndex = 26;
			this.label8.Text = "Улица:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// StreetText
			// 
			this.StreetText.Location = new System.Drawing.Point(152, 137);
			this.StreetText.Name = "StreetText";
			this.StreetText.Size = new System.Drawing.Size(408, 20);
			this.StreetText.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 292);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(144, 18);
			this.label6.TabIndex = 22;
			this.label6.Text = "Форма оплаты:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 45);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(144, 18);
			this.label4.TabIndex = 20;
			this.label4.Text = "Контактный e-mail:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 77);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(144, 19);
			this.label2.TabIndex = 18;
			this.label2.Text = "Телефоны:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(351, 200);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 18);
			this.label3.TabIndex = 59;
			this.label3.Text = "Подъезд:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnCancel);
			this.panel2.Controls.Add(this.btnShowVersions);
			this.panel2.Controls.Add(this.ExportToSV);
			this.panel2.Controls.Add(this.btnSave);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 486);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(779, 46);
			this.panel2.TabIndex = 18;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(12, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 28);
			this.btnCancel.TabIndex = 31;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnShowVersions
			// 
			this.btnShowVersions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnShowVersions.BackColor = System.Drawing.SystemColors.Control;
			this.btnShowVersions.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnShowVersions.Location = new System.Drawing.Point(102, 6);
			this.btnShowVersions.Name = "btnShowVersions";
			this.btnShowVersions.Size = new System.Drawing.Size(152, 28);
			this.btnShowVersions.TabIndex = 32;
			this.btnShowVersions.Text = "Просмотреть изменения";
			this.btnShowVersions.UseVisualStyleBackColor = false;
			this.btnShowVersions.Click += new System.EventHandler(this.btnShowVersions_Click);
			// 
			// ExportToSV
			// 
			this.ExportToSV.Location = new System.Drawing.Point(412, 10);
			this.ExportToSV.Name = "ExportToSV";
			this.ExportToSV.Size = new System.Drawing.Size(128, 27);
			this.ExportToSV.TabIndex = 33;
			this.ExportToSV.Text = "Экспорт в SV";
			this.ExportToSV.UseVisualStyleBackColor = true;
			this.ExportToSV.Click += new System.EventHandler(this.ExportToSV_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.BackColor = System.Drawing.SystemColors.Control;
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(697, 10);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 28);
			this.btnSave.TabIndex = 34;
			this.btnSave.Text = "Сохранить";
			this.btnSave.UseVisualStyleBackColor = false;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.btnDel);
			this.panel5.Controls.Add(this.btnAdd);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel5.Location = new System.Drawing.Point(744, 3);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(24, 454);
			this.panel5.TabIndex = 1;
			// 
			// btnDel
			// 
			this.btnDel.Location = new System.Drawing.Point(0, 37);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new System.Drawing.Size(24, 26);
			this.btnDel.TabIndex = 30;
			this.btnDel.Text = "-";
			this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(0, 0);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(24, 27);
			this.btnAdd.TabIndex = 29;
			this.btnAdd.Text = "+";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(779, 486);
			this.tabControl1.TabIndex = 5;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.panel1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(771, 460);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Общие данные";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.GoodsGrid);
			this.tabPage2.Controls.Add(this.panel5);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(771, 460);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Товары";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// EditOrder
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(779, 532);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.panel2);
			this.Name = "EditOrder";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
			this.Text = "Редактирование заказа ";
			this.Load += new System.EventHandler(this.EditOrder_Load);
			this.Click += new System.EventHandler(this.btnShowVersions_Click);
			((System.ComponentModel.ISupportInitialize)(this.gridViewTemplate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetGoods)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableGoods)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private TabPage tabPage2;
		private System.Data.DataColumn dataColumn8;
		private Telerik.WinControls.UI.RadGridView GoodsGrid;
		private Telerik.WinControls.UI.GridViewTemplate gridViewTemplate1;
	}
}
