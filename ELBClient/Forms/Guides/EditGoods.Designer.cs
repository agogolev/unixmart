using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditGoods
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Button btnAttachImages;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage GeneralPage;
		private System.Windows.Forms.TabPage ParamPage;
		private System.Windows.Forms.TextBox Model;
		private ColumnMenuExtender.DataBoundTextBox MinSellPrice;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabPage AccessoryPage;
		private System.Windows.Forms.TabPage ReviewPage;
		private ColumnMenuExtender.DataGridISM dgParams;
		private System.Windows.Forms.Panel panel44;
		private System.Windows.Forms.Label label13;
		private ColumnMenuExtender.UserControls.AddDelDataGridExt dgPressReleases;
		private ColumnMenuExtender.UserControls.AddDelDataGridExt dgAccessories;
		private System.Windows.Forms.TabPage KeywordsPage;
		private System.Windows.Forms.Label PricesLabel;
		private System.Windows.Forms.Label InStockLabel;
		private ColumnMenuExtender.DataGridISM dgPrices;
		private ColumnMenuExtender.DataGridISM dgInStocks;
		private ColumnMenuExtender.DataBoundTextBox PostavshikIDText;
		private System.Windows.Forms.Label PostavshikIDLabel;
		private System.Windows.Forms.TextBox ManufPrice;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TabPage AdditionalPage;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private ColumnMenuExtender.DataGridISM dgIsNew;
		private ColumnMenuExtender.DataGridISM dgIsHit;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox PageDescription;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox Keywords;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox Title;
		private Label label3;
		private ColumnMenuExtender.DataGridISM dgIsSale;
		private Label label6;
		private ColumnMenuExtender.DataGridISM dgIsFavorites;
		private TextBox ProductType;
		private Label label8;
		private Label label11;
		private UserControls.SelectorTextBoxExt Category;
		private UserControls.SelectorTextBox Manufacturer;
		private Label label7;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.FormattableTextBoxColumn colValue;
		private ColumnMenuExtender.FormattableTextBoxColumn colOrdValue;
		private ColumnMenuExtender.FormattableTextBoxColumn colUnit;
		private ColumnMenuExtender.FormattableBooleanColumn colMandatory;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableBooleanColumn colIsFullName;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle4;
		private ColumnMenuExtender.FormattableTextBoxColumn colPrice;
		private MetaData.DataSetISM dsPrices;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn3;
		private MetaData.DataSetISM dsInStock;
		private System.Data.DataTable dataTable2;
		private System.Data.DataColumn dataColumn5;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle5;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private MetaData.DataSetISM dsIsHit;
		private MetaData.DataSetISM dsIsNew;
		private System.Data.DataTable dataTable5;
		private System.Data.DataColumn dataColumn20;
		private System.Data.DataColumn dataColumn22;
		private System.Data.DataTable dataTable6;
		private System.Data.DataColumn dataColumn23;
		private System.Data.DataColumn dataColumn25;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle8;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn3;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle9;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn4;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colcbShopTypeNew;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colcbShopTypeHit;
		private MetaData.DataSetISM dsIsSales;
		private System.Data.DataTable dataTable4;
		private System.Data.DataColumn dataColumn11;
		private System.Data.DataColumn dataColumn12;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn1;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn2;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle6;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn3;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn2;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle7;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn4;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn5;
		private MetaData.DataSetISM dsIsFavorites;
		private System.Data.DataTable dataTable3;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn4;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn6;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;

		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.dsInStock = new MetaData.DataSetISM();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dsPrices = new MetaData.DataSetISM();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dsIsHit = new MetaData.DataSetISM();
            this.dataTable5 = new System.Data.DataTable();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dsIsNew = new MetaData.DataSetISM();
            this.dataTable6 = new System.Data.DataTable();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dsIsSales = new MetaData.DataSetISM();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dsIsFavorites = new MetaData.DataSetISM();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.GeneralPage = new System.Windows.Forms.TabPage();
            this.SalesNotesText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.VariantLabel = new System.Windows.Forms.Label();
            this.VariantTypeSelect = new ColumnMenuExtender.DataBoundComboBox();
            this.YandexUrl = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Manufacturer = new ELBClient.UserControls.SelectorTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Category = new ELBClient.UserControls.SelectorTextBoxExt();
            this.ProductType = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ManufPrice = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.PostavshikIDText = new ColumnMenuExtender.DataBoundTextBox();
            this.PostavshikIDLabel = new System.Windows.Forms.Label();
            this.InStockLabel = new System.Windows.Forms.Label();
            this.dgInStocks = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle5 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn1 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn8 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn14 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn9 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn7 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn15 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.extendedDataGridDTPickerColumn1 = new ColumnMenuExtender.ExtendedDataGridDTPickerColumn();
            this.PricesLabel = new System.Windows.Forms.Label();
            this.dgPrices = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle4 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn2 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.colPrice = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn6 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.MinSellPrice = new ColumnMenuExtender.DataBoundTextBox();
            this.Model = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AdditionalPage = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.dgIsFavorites = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle7 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn4 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableBooleanColumn5 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.dgIsSale = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle6 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn3 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableBooleanColumn2 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.label21 = new System.Windows.Forms.Label();
            this.PageDescription = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.Keywords = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.TextBox();
            this.dgIsHit = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle9 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.colcbShopTypeHit = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableBooleanColumn4 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn9 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.dgIsNew = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle8 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.colcbShopTypeNew = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableBooleanColumn3 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.ParamPage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgParams = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.colOrdValue = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colValue = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colUnit = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colMandatory = new ColumnMenuExtender.FormattableBooleanColumn();
            this.colIsFullName = new ColumnMenuExtender.FormattableBooleanColumn();
            this.panel44 = new System.Windows.Forms.Panel();
            this.DescriptionsGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle10 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn5 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.label13 = new System.Windows.Forms.Label();
            this.AccessoryPage = new System.Windows.Forms.TabPage();
            this.dgAccessories = new ColumnMenuExtender.UserControls.AddDelDataGridExt();
            this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.ReviewPage = new System.Windows.Forms.TabPage();
            this.dgPressReleases = new ColumnMenuExtender.UserControls.AddDelDataGridExt();
            this.extendedDataGridTableStyle3 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.KeywordsPage = new System.Windows.Forms.TabPage();
            this.KeywordsGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle11 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.DelKeyword = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LoadKeywordsButton = new System.Windows.Forms.Button();
            this.AnalogPage = new System.Windows.Forms.TabPage();
            this.AnalogsAddDelDataGrid = new ColumnMenuExtender.UserControls.AddDelDataGridExt();
            this.extendedDataGridTableStyle12 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn10 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn11 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.InstructionPage = new System.Windows.Forms.TabPage();
            this.DocsDataGrid = new ColumnMenuExtender.UserControls.AddDelDataGridExt();
            this.extendedDataGridTableStyle13 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn12 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn13 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAttachImages = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dsInStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPrices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsHit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsSales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsFavorites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.GeneralPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VariantTypeSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgInStocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrices)).BeginInit();
            this.AdditionalPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsFavorites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsSale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsHit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsNew)).BeginInit();
            this.ParamPage.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgParams)).BeginInit();
            this.panel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionsGrid)).BeginInit();
            this.AccessoryPage.SuspendLayout();
            this.ReviewPage.SuspendLayout();
            this.KeywordsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KeywordsGrid)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.AnalogPage.SuspendLayout();
            this.InstructionPage.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // dsInStock
            // 
            this.dsInStock.DataSetName = "NewDataSet";
            this.dsInStock.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsInStock.OnlyRowsWithoutErrors = false;
            this.dsInStock.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn5,
            this.dataColumn7,
            this.dataColumn10,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn19});
            this.dataTable2.TableName = "Table";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "ordValue";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn7
            // 
            this.dataColumn7.AllowDBNull = false;
            this.dataColumn7.ColumnName = "propValue";
            this.dataColumn7.DataType = typeof(bool);
            this.dataColumn7.DefaultValue = true;
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "instantDelivery";
            this.dataColumn10.DataType = typeof(bool);
            // 
            // dataColumn15
            // 
            this.dataColumn15.AllowDBNull = false;
            this.dataColumn15.Caption = "inPath";
            this.dataColumn15.ColumnName = "inPath";
            this.dataColumn15.DataType = typeof(bool);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "pathDelivery";
            this.dataColumn16.DataType = typeof(System.DateTime);
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "stockStatus";
            this.dataColumn17.DataType = typeof(int);
            // 
            // dataColumn18
            // 
            this.dataColumn18.AllowDBNull = false;
            this.dataColumn18.ColumnName = "onDemand";
            this.dataColumn18.DataType = typeof(bool);
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "lastActiveDate";
            this.dataColumn19.DataType = typeof(System.DateTime);
            // 
            // dsPrices
            // 
            this.dsPrices.DataSetName = "NewDataSet";
            this.dsPrices.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsPrices.OnlyRowsWithoutErrors = false;
            this.dsPrices.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn3,
            this.dataColumn6,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn13});
            this.dataTable1.TableName = "Table";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "ordValue";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.AllowDBNull = false;
            this.dataColumn3.ColumnName = "propValue";
            this.dataColumn3.DataType = typeof(decimal);
            this.dataColumn3.DefaultValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // dataColumn6
            // 
            this.dataColumn6.AllowDBNull = false;
            this.dataColumn6.ColumnName = "isManual";
            this.dataColumn6.DataType = typeof(bool);
            this.dataColumn6.DefaultValue = false;
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "bid";
            this.dataColumn8.DataType = typeof(int);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "cbid";
            this.dataColumn9.DataType = typeof(int);
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "deliveryPrice";
            this.dataColumn13.DataType = typeof(decimal);
            // 
            // dsIsHit
            // 
            this.dsIsHit.DataSetName = "NewDataSet";
            this.dsIsHit.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsIsHit.OnlyRowsWithoutErrors = false;
            this.dsIsHit.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable5});
            // 
            // dataTable5
            // 
            this.dataTable5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn20,
            this.dataColumn22,
            this.dataColumn14});
            this.dataTable5.TableName = "Table";
            // 
            // dataColumn20
            // 
            this.dataColumn20.AllowDBNull = false;
            this.dataColumn20.ColumnName = "ordValue";
            this.dataColumn20.DataType = typeof(int);
            // 
            // dataColumn22
            // 
            this.dataColumn22.AllowDBNull = false;
            this.dataColumn22.ColumnName = "propValue";
            this.dataColumn22.DataType = typeof(bool);
            this.dataColumn22.DefaultValue = true;
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "hitWeight";
            this.dataColumn14.DataType = typeof(int);
            // 
            // dsIsNew
            // 
            this.dsIsNew.DataSetName = "NewDataSet";
            this.dsIsNew.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsIsNew.OnlyRowsWithoutErrors = false;
            this.dsIsNew.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable6});
            // 
            // dataTable6
            // 
            this.dataTable6.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn23,
            this.dataColumn25});
            this.dataTable6.TableName = "Table";
            // 
            // dataColumn23
            // 
            this.dataColumn23.AllowDBNull = false;
            this.dataColumn23.ColumnName = "ordValue";
            this.dataColumn23.DataType = typeof(int);
            // 
            // dataColumn25
            // 
            this.dataColumn25.AllowDBNull = false;
            this.dataColumn25.ColumnName = "propValue";
            this.dataColumn25.DataType = typeof(bool);
            this.dataColumn25.DefaultValue = true;
            // 
            // dsIsSales
            // 
            this.dsIsSales.DataSetName = "NewDataSet";
            this.dsIsSales.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsIsSales.OnlyRowsWithoutErrors = false;
            this.dsIsSales.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable4});
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12});
            this.dataTable4.TableName = "Table";
            // 
            // dataColumn11
            // 
            this.dataColumn11.AllowDBNull = false;
            this.dataColumn11.ColumnName = "ordValue";
            this.dataColumn11.DataType = typeof(int);
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "propValue";
            this.dataColumn12.DataType = typeof(bool);
            this.dataColumn12.DefaultValue = true;
            // 
            // dsIsFavorites
            // 
            this.dsIsFavorites.DataSetName = "NewDataSet";
            this.dsIsFavorites.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsIsFavorites.OnlyRowsWithoutErrors = false;
            this.dsIsFavorites.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable3});
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn2,
            this.dataColumn4});
            this.dataTable3.TableName = "Table";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "ordValue";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "propValue";
            this.dataColumn4.DataType = typeof(bool);
            this.dataColumn4.DefaultValue = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.GeneralPage);
            this.tabControl1.Controls.Add(this.AdditionalPage);
            this.tabControl1.Controls.Add(this.ParamPage);
            this.tabControl1.Controls.Add(this.AccessoryPage);
            this.tabControl1.Controls.Add(this.ReviewPage);
            this.tabControl1.Controls.Add(this.KeywordsPage);
            this.tabControl1.Controls.Add(this.AnalogPage);
            this.tabControl1.Controls.Add(this.InstructionPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(962, 447);
            this.tabControl1.TabIndex = 0;
            // 
            // GeneralPage
            // 
            this.GeneralPage.Controls.Add(this.SalesNotesText);
            this.GeneralPage.Controls.Add(this.label14);
            this.GeneralPage.Controls.Add(this.VariantLabel);
            this.GeneralPage.Controls.Add(this.VariantTypeSelect);
            this.GeneralPage.Controls.Add(this.YandexUrl);
            this.GeneralPage.Controls.Add(this.label12);
            this.GeneralPage.Controls.Add(this.Manufacturer);
            this.GeneralPage.Controls.Add(this.label7);
            this.GeneralPage.Controls.Add(this.label11);
            this.GeneralPage.Controls.Add(this.Category);
            this.GeneralPage.Controls.Add(this.ProductType);
            this.GeneralPage.Controls.Add(this.label8);
            this.GeneralPage.Controls.Add(this.ManufPrice);
            this.GeneralPage.Controls.Add(this.label18);
            this.GeneralPage.Controls.Add(this.PostavshikIDText);
            this.GeneralPage.Controls.Add(this.PostavshikIDLabel);
            this.GeneralPage.Controls.Add(this.InStockLabel);
            this.GeneralPage.Controls.Add(this.dgInStocks);
            this.GeneralPage.Controls.Add(this.PricesLabel);
            this.GeneralPage.Controls.Add(this.dgPrices);
            this.GeneralPage.Controls.Add(this.MinSellPrice);
            this.GeneralPage.Controls.Add(this.Model);
            this.GeneralPage.Controls.Add(this.label9);
            this.GeneralPage.Controls.Add(this.label4);
            this.GeneralPage.Location = new System.Drawing.Point(4, 25);
            this.GeneralPage.Name = "GeneralPage";
            this.GeneralPage.Size = new System.Drawing.Size(954, 418);
            this.GeneralPage.TabIndex = 0;
            this.GeneralPage.Text = "Общие";
            // 
            // SalesNotesText
            // 
            this.SalesNotesText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SalesNotesText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SalesNotesText.Location = new System.Drawing.Point(112, 199);
            this.SalesNotesText.Name = "SalesNotesText";
            this.SalesNotesText.Size = new System.Drawing.Size(833, 22);
            this.SalesNotesText.TabIndex = 64;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(8, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 21);
            this.label14.TabIndex = 63;
            this.label14.Text = "Sales Notes:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VariantLabel
            // 
            this.VariantLabel.Location = new System.Drawing.Point(422, 49);
            this.VariantLabel.Name = "VariantLabel";
            this.VariantLabel.Size = new System.Drawing.Size(91, 18);
            this.VariantLabel.TabIndex = 62;
            this.VariantLabel.Text = "Вариант:";
            this.VariantLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VariantTypeSelect
            // 
            this.VariantTypeSelect.AutoCompleteDisplayMember = null;
            this.VariantTypeSelect.AutoCompleteValueMember = null;
            this.VariantTypeSelect.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.VariantTypeSelect.Location = new System.Drawing.Point(519, 48);
            this.VariantTypeSelect.Name = "VariantTypeSelect";
            this.VariantTypeSelect.SelectedValue = -1;
            this.VariantTypeSelect.Size = new System.Drawing.Size(208, 24);
            this.VariantTypeSelect.TabIndex = 61;
            // 
            // YandexUrl
            // 
            this.YandexUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.YandexUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YandexUrl.Location = new System.Drawing.Point(112, 168);
            this.YandexUrl.Name = "YandexUrl";
            this.YandexUrl.Size = new System.Drawing.Size(833, 22);
            this.YandexUrl.TabIndex = 60;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(8, 168);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 21);
            this.label12.TabIndex = 59;
            this.label12.Text = "Ссылка на Яндекс:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Manufacturer
            // 
            this.Manufacturer.BoundProp = null;
            this.Manufacturer.ButtonBackColor = System.Drawing.SystemColors.Control;
            this.Manufacturer.ClassName = "CCompany";
            this.Manufacturer.FieldNames = null;
            this.Manufacturer.FormatRows = null;
            this.Manufacturer.HeaderNames = "Название";
            this.Manufacturer.ListFormName = "Компании";
            this.Manufacturer.Location = new System.Drawing.Point(113, 138);
            this.Manufacturer.Name = "Manufacturer";
            this.Manufacturer.RowNames = "companyName";
            this.Manufacturer.Size = new System.Drawing.Size(303, 24);
            this.Manufacturer.TabIndex = 58;
            this.Manufacturer.TextBoxBorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 57;
            this.label7.Text = "Производитель:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(8, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 18);
            this.label11.TabIndex = 56;
            this.label11.Text = "Категория:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Category
            // 
            this.Category.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Category.BoundProp = null;
            this.Category.ButtonBackColor = System.Drawing.SystemColors.Control;
            this.Category.Location = new System.Drawing.Point(113, 108);
            this.Category.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Category.Name = "Category";
            this.Category.Size = new System.Drawing.Size(832, 24);
            this.Category.TabIndex = 55;
            this.Category.TextBoxBorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Category.ChooseClick += new System.EventHandler(this.Category_ChooseClick);
            this.Category.DelClick += new System.EventHandler(this.Category_DelClick);
            // 
            // ProductType
            // 
            this.ProductType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProductType.Location = new System.Drawing.Point(112, 48);
            this.ProductType.Name = "ProductType";
            this.ProductType.Size = new System.Drawing.Size(304, 22);
            this.ProductType.TabIndex = 54;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 18);
            this.label8.TabIndex = 53;
            this.label8.Text = "Тип товара:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ManufPrice
            // 
            this.ManufPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ManufPrice.Location = new System.Drawing.Point(312, 78);
            this.ManufPrice.Name = "ManufPrice";
            this.ManufPrice.Size = new System.Drawing.Size(104, 22);
            this.ManufPrice.TabIndex = 52;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(192, 79);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(134, 18);
            this.label18.TabIndex = 51;
            this.label18.Text = "Цена производителя:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PostavshikIDText
            // 
            this.PostavshikIDText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PostavshikIDText.BoundProp = null;
            this.PostavshikIDText.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PostavshikIDText.IsCurrency = false;
            this.PostavshikIDText.Location = new System.Drawing.Point(113, 78);
            this.PostavshikIDText.Name = "PostavshikIDText";
            this.PostavshikIDText.Size = new System.Drawing.Size(48, 22);
            this.PostavshikIDText.TabIndex = 40;
            // 
            // PostavshikIDLabel
            // 
            this.PostavshikIDLabel.Location = new System.Drawing.Point(8, 79);
            this.PostavshikIDLabel.Name = "PostavshikIDLabel";
            this.PostavshikIDLabel.Size = new System.Drawing.Size(100, 18);
            this.PostavshikIDLabel.TabIndex = 39;
            this.PostavshikIDLabel.Text = "Код поставщика:";
            this.PostavshikIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InStockLabel
            // 
            this.InStockLabel.Location = new System.Drawing.Point(516, 219);
            this.InStockLabel.Name = "InStockLabel";
            this.InStockLabel.Size = new System.Drawing.Size(96, 18);
            this.InStockLabel.TabIndex = 34;
            this.InStockLabel.Text = "Опубликовано:";
            this.InStockLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgInStocks
            // 
            this.dgInStocks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgInStocks.BackgroundColor = System.Drawing.Color.White;
            this.dgInStocks.DataMember = "";
            this.dgInStocks.FilterString = null;
            this.dgInStocks.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgInStocks.Location = new System.Drawing.Point(519, 240);
            this.dgInStocks.Name = "dgInStocks";
            this.dgInStocks.Order = null;
            this.dgInStocks.Size = new System.Drawing.Size(426, 171);
            this.dgInStocks.TabIndex = 33;
            this.dgInStocks.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle5});
            // 
            // extendedDataGridTableStyle5
            // 
            this.extendedDataGridTableStyle5.DataGrid = this.dgInStocks;
            this.extendedDataGridTableStyle5.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn1,
            this.formattableBooleanColumn1,
            this.formattableBooleanColumn8,
            this.formattableTextBoxColumn14,
            this.formattableBooleanColumn9,
            this.formattableBooleanColumn7,
            this.formattableTextBoxColumn15,
            this.extendedDataGridDTPickerColumn1});
            this.extendedDataGridTableStyle5.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle5.MappingName = "table";
            // 
            // extendedDataGridComboBoxColumn1
            // 
            this.extendedDataGridComboBoxColumn1.FieldName = null;
            this.extendedDataGridComboBoxColumn1.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn1.Format = "";
            this.extendedDataGridComboBoxColumn1.FormatInfo = null;
            this.extendedDataGridComboBoxColumn1.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn1.MappingName = "ordValue";
            this.extendedDataGridComboBoxColumn1.Width = 75;
            // 
            // formattableBooleanColumn1
            // 
            this.formattableBooleanColumn1.AllowNull = false;
            this.formattableBooleanColumn1.FieldName = null;
            this.formattableBooleanColumn1.FilterFieldName = null;
            this.formattableBooleanColumn1.HeaderText = "Опубликовано";
            this.formattableBooleanColumn1.MappingName = "propValue";
            this.formattableBooleanColumn1.Width = 75;
            // 
            // formattableBooleanColumn8
            // 
            this.formattableBooleanColumn8.FieldName = null;
            this.formattableBooleanColumn8.FilterFieldName = null;
            this.formattableBooleanColumn8.HeaderText = "В пути";
            this.formattableBooleanColumn8.MappingName = "inPath";
            this.formattableBooleanColumn8.Width = 75;
            // 
            // formattableTextBoxColumn14
            // 
            this.formattableTextBoxColumn14.FieldName = null;
            this.formattableTextBoxColumn14.FilterFieldName = null;
            this.formattableTextBoxColumn14.Format = "dd.MM.yyyy";
            this.formattableTextBoxColumn14.FormatInfo = null;
            this.formattableTextBoxColumn14.HeaderText = "Ожидается";
            this.formattableTextBoxColumn14.MappingName = "pathDelivery";
            this.formattableTextBoxColumn14.Width = 75;
            // 
            // formattableBooleanColumn9
            // 
            this.formattableBooleanColumn9.FieldName = null;
            this.formattableBooleanColumn9.FilterFieldName = null;
            this.formattableBooleanColumn9.HeaderText = "Под заказ";
            this.formattableBooleanColumn9.MappingName = "onDemand";
            this.formattableBooleanColumn9.Width = 75;
            // 
            // formattableBooleanColumn7
            // 
            this.formattableBooleanColumn7.AllowNull = false;
            this.formattableBooleanColumn7.FieldName = null;
            this.formattableBooleanColumn7.FilterFieldName = null;
            this.formattableBooleanColumn7.HeaderText = "Экспресс доставка";
            this.formattableBooleanColumn7.MappingName = "instantDelivery";
            this.formattableBooleanColumn7.Width = 75;
            // 
            // formattableTextBoxColumn15
            // 
            this.formattableTextBoxColumn15.FieldName = null;
            this.formattableTextBoxColumn15.FilterFieldName = null;
            this.formattableTextBoxColumn15.Format = "";
            this.formattableTextBoxColumn15.FormatInfo = null;
            this.formattableTextBoxColumn15.HeaderText = "Статус склада";
            this.formattableTextBoxColumn15.MappingName = "stockStatus";
            this.formattableTextBoxColumn15.Width = 75;
            // 
            // extendedDataGridDTPickerColumn1
            // 
            this.extendedDataGridDTPickerColumn1.FieldName = null;
            this.extendedDataGridDTPickerColumn1.FilterFieldName = null;
            this.extendedDataGridDTPickerColumn1.Format = null;
            this.extendedDataGridDTPickerColumn1.HeaderText = "Последняя активность";
            this.extendedDataGridDTPickerColumn1.MappingName = "lastActiveDate";
            this.extendedDataGridDTPickerColumn1.Width = 75;
            // 
            // PricesLabel
            // 
            this.PricesLabel.Location = new System.Drawing.Point(12, 219);
            this.PricesLabel.Name = "PricesLabel";
            this.PricesLabel.Size = new System.Drawing.Size(72, 18);
            this.PricesLabel.TabIndex = 32;
            this.PricesLabel.Text = "Цены:";
            this.PricesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgPrices
            // 
            this.dgPrices.BackgroundColor = System.Drawing.Color.White;
            this.dgPrices.DataMember = "";
            this.dgPrices.FilterString = null;
            this.dgPrices.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgPrices.Location = new System.Drawing.Point(15, 240);
            this.dgPrices.Name = "dgPrices";
            this.dgPrices.Order = null;
            this.dgPrices.Size = new System.Drawing.Size(498, 171);
            this.dgPrices.TabIndex = 31;
            this.dgPrices.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle4});
            // 
            // extendedDataGridTableStyle4
            // 
            this.extendedDataGridTableStyle4.DataGrid = this.dgPrices;
            this.extendedDataGridTableStyle4.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn2,
            this.colPrice,
            this.formattableBooleanColumn6,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn8});
            this.extendedDataGridTableStyle4.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle4.MappingName = "table";
            // 
            // extendedDataGridComboBoxColumn2
            // 
            this.extendedDataGridComboBoxColumn2.FieldName = null;
            this.extendedDataGridComboBoxColumn2.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn2.Format = "";
            this.extendedDataGridComboBoxColumn2.FormatInfo = null;
            this.extendedDataGridComboBoxColumn2.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn2.MappingName = "ordValue";
            this.extendedDataGridComboBoxColumn2.Width = 75;
            // 
            // colPrice
            // 
            this.colPrice.FieldName = null;
            this.colPrice.FilterFieldName = null;
            this.colPrice.Format = "#.00";
            this.colPrice.FormatInfo = null;
            this.colPrice.HeaderText = "Цена";
            this.colPrice.MappingName = "propValue";
            this.colPrice.Width = 75;
            // 
            // formattableBooleanColumn6
            // 
            this.formattableBooleanColumn6.AllowNull = false;
            this.formattableBooleanColumn6.FieldName = null;
            this.formattableBooleanColumn6.FilterFieldName = null;
            this.formattableBooleanColumn6.HeaderText = "Ручная цена";
            this.formattableBooleanColumn6.MappingName = "isManual";
            this.formattableBooleanColumn6.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "bid";
            this.formattableTextBoxColumn4.MappingName = "bid";
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "cbid";
            this.formattableTextBoxColumn5.MappingName = "cbid";
            this.formattableTextBoxColumn5.Width = 75;
            // 
            // formattableTextBoxColumn8
            // 
            this.formattableTextBoxColumn8.FieldName = null;
            this.formattableTextBoxColumn8.FilterFieldName = null;
            this.formattableTextBoxColumn8.Format = "#,#0";
            this.formattableTextBoxColumn8.FormatInfo = null;
            this.formattableTextBoxColumn8.HeaderText = "Стоимость доставки";
            this.formattableTextBoxColumn8.MappingName = "deliveryPrice";
            this.formattableTextBoxColumn8.Width = 75;
            // 
            // MinSellPrice
            // 
            this.MinSellPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MinSellPrice.BoundProp = null;
            this.MinSellPrice.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinSellPrice.IsCurrency = false;
            this.MinSellPrice.Location = new System.Drawing.Point(556, 78);
            this.MinSellPrice.Name = "MinSellPrice";
            this.MinSellPrice.Size = new System.Drawing.Size(82, 22);
            this.MinSellPrice.TabIndex = 26;
            // 
            // Model
            // 
            this.Model.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Model.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Model.Location = new System.Drawing.Point(112, 18);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(834, 22);
            this.Model.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(430, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 18);
            this.label9.TabIndex = 15;
            this.label9.Text = "Минимальная цена:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Имя:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AdditionalPage
            // 
            this.AdditionalPage.Controls.Add(this.label6);
            this.AdditionalPage.Controls.Add(this.dgIsFavorites);
            this.AdditionalPage.Controls.Add(this.label3);
            this.AdditionalPage.Controls.Add(this.dgIsSale);
            this.AdditionalPage.Controls.Add(this.label21);
            this.AdditionalPage.Controls.Add(this.PageDescription);
            this.AdditionalPage.Controls.Add(this.label22);
            this.AdditionalPage.Controls.Add(this.Keywords);
            this.AdditionalPage.Controls.Add(this.label23);
            this.AdditionalPage.Controls.Add(this.Title);
            this.AdditionalPage.Controls.Add(this.dgIsHit);
            this.AdditionalPage.Controls.Add(this.dgIsNew);
            this.AdditionalPage.Controls.Add(this.label20);
            this.AdditionalPage.Controls.Add(this.label19);
            this.AdditionalPage.Location = new System.Drawing.Point(4, 25);
            this.AdditionalPage.Name = "AdditionalPage";
            this.AdditionalPage.Size = new System.Drawing.Size(954, 418);
            this.AdditionalPage.TabIndex = 5;
            this.AdditionalPage.Text = "Дополнительно";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(368, 273);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 19);
            this.label6.TabIndex = 55;
            this.label6.Text = "Мы советуем:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgIsFavorites
            // 
            this.dgIsFavorites.BackgroundColor = System.Drawing.Color.White;
            this.dgIsFavorites.DataMember = "";
            this.dgIsFavorites.FilterString = null;
            this.dgIsFavorites.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgIsFavorites.Location = new System.Drawing.Point(368, 295);
            this.dgIsFavorites.Name = "dgIsFavorites";
            this.dgIsFavorites.Order = null;
            this.dgIsFavorites.Size = new System.Drawing.Size(352, 188);
            this.dgIsFavorites.TabIndex = 54;
            this.dgIsFavorites.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle7});
            // 
            // extendedDataGridTableStyle7
            // 
            this.extendedDataGridTableStyle7.DataGrid = this.dgIsFavorites;
            this.extendedDataGridTableStyle7.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn4,
            this.formattableBooleanColumn5});
            this.extendedDataGridTableStyle7.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle7.MappingName = "Table";
            // 
            // extendedDataGridComboBoxColumn4
            // 
            this.extendedDataGridComboBoxColumn4.FieldName = null;
            this.extendedDataGridComboBoxColumn4.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn4.Format = "";
            this.extendedDataGridComboBoxColumn4.FormatInfo = null;
            this.extendedDataGridComboBoxColumn4.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn4.MappingName = "ordValue";
            this.extendedDataGridComboBoxColumn4.Width = 75;
            // 
            // formattableBooleanColumn5
            // 
            this.formattableBooleanColumn5.FieldName = null;
            this.formattableBooleanColumn5.FilterFieldName = null;
            this.formattableBooleanColumn5.HeaderText = "Советуют?";
            this.formattableBooleanColumn5.MappingName = "propValue";
            this.formattableBooleanColumn5.Width = 75;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 273);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 19);
            this.label3.TabIndex = 53;
            this.label3.Text = "Спецпредложения:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgIsSale
            // 
            this.dgIsSale.AllowSorting = false;
            this.dgIsSale.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgIsSale.DataMember = "";
            this.dgIsSale.FilterString = null;
            this.dgIsSale.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgIsSale.Location = new System.Drawing.Point(8, 295);
            this.dgIsSale.Name = "dgIsSale";
            this.dgIsSale.Order = null;
            this.dgIsSale.Size = new System.Drawing.Size(352, 188);
            this.dgIsSale.TabIndex = 52;
            this.dgIsSale.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle6});
            // 
            // extendedDataGridTableStyle6
            // 
            this.extendedDataGridTableStyle6.DataGrid = this.dgIsSale;
            this.extendedDataGridTableStyle6.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn3,
            this.formattableBooleanColumn2});
            this.extendedDataGridTableStyle6.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle6.MappingName = "Table";
            // 
            // extendedDataGridComboBoxColumn3
            // 
            this.extendedDataGridComboBoxColumn3.FieldName = null;
            this.extendedDataGridComboBoxColumn3.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn3.Format = "";
            this.extendedDataGridComboBoxColumn3.FormatInfo = null;
            this.extendedDataGridComboBoxColumn3.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn3.MappingName = "ordValue";
            this.extendedDataGridComboBoxColumn3.Width = 75;
            // 
            // formattableBooleanColumn2
            // 
            this.formattableBooleanColumn2.AllowNull = false;
            this.formattableBooleanColumn2.FieldName = null;
            this.formattableBooleanColumn2.FilterFieldName = null;
            this.formattableBooleanColumn2.HeaderText = "Спецпредложение";
            this.formattableBooleanColumn2.MappingName = "propValue";
            this.formattableBooleanColumn2.Width = 75;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(8, 546);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 18);
            this.label21.TabIndex = 51;
            this.label21.Text = "Description:";
            // 
            // PageDescription
            // 
            this.PageDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PageDescription.Location = new System.Drawing.Point(128, 546);
            this.PageDescription.Name = "PageDescription";
            this.PageDescription.Size = new System.Drawing.Size(810, 22);
            this.PageDescription.TabIndex = 48;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(8, 518);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 19);
            this.label22.TabIndex = 50;
            this.label22.Text = "Keywords:";
            // 
            // Keywords
            // 
            this.Keywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Keywords.Location = new System.Drawing.Point(128, 518);
            this.Keywords.Name = "Keywords";
            this.Keywords.Size = new System.Drawing.Size(810, 22);
            this.Keywords.TabIndex = 47;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(8, 490);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(120, 19);
            this.label23.TabIndex = 49;
            this.label23.Text = "Title:";
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.Location = new System.Drawing.Point(128, 490);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(810, 22);
            this.Title.TabIndex = 46;
            // 
            // dgIsHit
            // 
            this.dgIsHit.BackgroundColor = System.Drawing.Color.White;
            this.dgIsHit.DataMember = "";
            this.dgIsHit.FilterString = null;
            this.dgIsHit.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgIsHit.Location = new System.Drawing.Point(368, 46);
            this.dgIsHit.Name = "dgIsHit";
            this.dgIsHit.Order = null;
            this.dgIsHit.Size = new System.Drawing.Size(352, 217);
            this.dgIsHit.TabIndex = 36;
            this.dgIsHit.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle9});
            // 
            // extendedDataGridTableStyle9
            // 
            this.extendedDataGridTableStyle9.DataGrid = this.dgIsHit;
            this.extendedDataGridTableStyle9.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colcbShopTypeHit,
            this.formattableBooleanColumn4,
            this.formattableTextBoxColumn9});
            this.extendedDataGridTableStyle9.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle9.MappingName = "Table";
            // 
            // colcbShopTypeHit
            // 
            this.colcbShopTypeHit.FieldName = null;
            this.colcbShopTypeHit.FilterFieldName = null;
            this.colcbShopTypeHit.Format = "";
            this.colcbShopTypeHit.FormatInfo = null;
            this.colcbShopTypeHit.HeaderText = "Магазин";
            this.colcbShopTypeHit.MappingName = "ordValue";
            this.colcbShopTypeHit.Width = 75;
            // 
            // formattableBooleanColumn4
            // 
            this.formattableBooleanColumn4.AllowNull = false;
            this.formattableBooleanColumn4.FieldName = null;
            this.formattableBooleanColumn4.FilterFieldName = null;
            this.formattableBooleanColumn4.HeaderText = "Хит";
            this.formattableBooleanColumn4.MappingName = "propValue";
            this.formattableBooleanColumn4.Width = 75;
            // 
            // formattableTextBoxColumn9
            // 
            this.formattableTextBoxColumn9.FieldName = null;
            this.formattableTextBoxColumn9.FilterFieldName = null;
            this.formattableTextBoxColumn9.Format = "";
            this.formattableTextBoxColumn9.FormatInfo = null;
            this.formattableTextBoxColumn9.HeaderText = "Значимость признака";
            this.formattableTextBoxColumn9.MappingName = "hitWeight";
            this.formattableTextBoxColumn9.Width = 75;
            // 
            // dgIsNew
            // 
            this.dgIsNew.BackgroundColor = System.Drawing.Color.White;
            this.dgIsNew.DataMember = "";
            this.dgIsNew.FilterString = null;
            this.dgIsNew.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgIsNew.Location = new System.Drawing.Point(8, 46);
            this.dgIsNew.Name = "dgIsNew";
            this.dgIsNew.Order = null;
            this.dgIsNew.Size = new System.Drawing.Size(352, 217);
            this.dgIsNew.TabIndex = 35;
            this.dgIsNew.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle8});
            // 
            // extendedDataGridTableStyle8
            // 
            this.extendedDataGridTableStyle8.DataGrid = this.dgIsNew;
            this.extendedDataGridTableStyle8.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colcbShopTypeNew,
            this.formattableBooleanColumn3});
            this.extendedDataGridTableStyle8.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle8.MappingName = "Table";
            // 
            // colcbShopTypeNew
            // 
            this.colcbShopTypeNew.FieldName = null;
            this.colcbShopTypeNew.FilterFieldName = null;
            this.colcbShopTypeNew.Format = "";
            this.colcbShopTypeNew.FormatInfo = null;
            this.colcbShopTypeNew.HeaderText = "Магазин";
            this.colcbShopTypeNew.MappingName = "ordValue";
            this.colcbShopTypeNew.Width = 75;
            // 
            // formattableBooleanColumn3
            // 
            this.formattableBooleanColumn3.AllowNull = false;
            this.formattableBooleanColumn3.FieldName = null;
            this.formattableBooleanColumn3.FilterFieldName = null;
            this.formattableBooleanColumn3.HeaderText = "Новинка";
            this.formattableBooleanColumn3.MappingName = "propValue";
            this.formattableBooleanColumn3.Width = 75;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(368, 18);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 19);
            this.label20.TabIndex = 34;
            this.label20.Text = "Хиты:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(16, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 19);
            this.label19.TabIndex = 33;
            this.label19.Text = "Новинки:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ParamPage
            // 
            this.ParamPage.Controls.Add(this.panel1);
            this.ParamPage.Location = new System.Drawing.Point(4, 25);
            this.ParamPage.Name = "ParamPage";
            this.ParamPage.Size = new System.Drawing.Size(954, 418);
            this.ParamPage.TabIndex = 1;
            this.ParamPage.Text = "Характеристики";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgParams);
            this.panel1.Controls.Add(this.panel44);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(954, 418);
            this.panel1.TabIndex = 3;
            // 
            // dgParams
            // 
            this.dgParams.BackgroundColor = System.Drawing.Color.White;
            this.dgParams.DataMember = "";
            this.dgParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgParams.FilterString = null;
            this.dgParams.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgParams.Location = new System.Drawing.Point(0, 223);
            this.dgParams.Name = "dgParams";
            this.dgParams.Order = null;
            this.dgParams.Size = new System.Drawing.Size(954, 195);
            this.dgParams.TabIndex = 0;
            this.dgParams.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.dgParams;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colOrdValue,
            this.colName,
            this.colValue,
            this.colUnit,
            this.colMandatory,
            this.colIsFullName});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            // 
            // colOrdValue
            // 
            this.colOrdValue.FieldName = null;
            this.colOrdValue.FilterFieldName = null;
            this.colOrdValue.Format = "";
            this.colOrdValue.FormatInfo = null;
            this.colOrdValue.HeaderText = "№";
            this.colOrdValue.MappingName = "ordValue";
            this.colOrdValue.ReadOnly = true;
            this.colOrdValue.Width = 75;
            // 
            // colName
            // 
            this.colName.FieldName = null;
            this.colName.FilterFieldName = null;
            this.colName.Format = "";
            this.colName.FormatInfo = null;
            this.colName.HeaderText = "Название";
            this.colName.MappingName = "name";
            this.colName.ReadOnly = true;
            this.colName.Width = 200;
            // 
            // colValue
            // 
            this.colValue.FieldName = null;
            this.colValue.FilterFieldName = null;
            this.colValue.Format = "";
            this.colValue.FormatInfo = null;
            this.colValue.HeaderText = "Значение";
            this.colValue.MappingName = "value";
            this.colValue.NullText = "";
            this.colValue.Width = 200;
            // 
            // colUnit
            // 
            this.colUnit.FieldName = null;
            this.colUnit.FilterFieldName = null;
            this.colUnit.Format = "";
            this.colUnit.FormatInfo = null;
            this.colUnit.HeaderText = "Ед. изм.";
            this.colUnit.MappingName = "unit";
            this.colUnit.NullText = "";
            this.colUnit.Width = 75;
            // 
            // colMandatory
            // 
            this.colMandatory.FieldName = null;
            this.colMandatory.FilterFieldName = null;
            this.colMandatory.HeaderText = "Обязательность";
            this.colMandatory.MappingName = "isMandatory";
            this.colMandatory.ReadOnly = true;
            this.colMandatory.Width = 150;
            // 
            // colIsFullName
            // 
            this.colIsFullName.FieldName = null;
            this.colIsFullName.FilterFieldName = null;
            this.colIsFullName.HeaderText = "Для списка";
            this.colIsFullName.MappingName = "isFullName";
            this.colIsFullName.ReadOnly = true;
            this.colIsFullName.Width = 75;
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.DescriptionsGrid);
            this.panel44.Controls.Add(this.label13);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(0, 0);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(954, 223);
            this.panel44.TabIndex = 0;
            // 
            // DescriptionsGrid
            // 
            this.DescriptionsGrid.BackgroundColor = System.Drawing.Color.White;
            this.DescriptionsGrid.DataMember = "";
            this.DescriptionsGrid.FilterString = null;
            this.DescriptionsGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.DescriptionsGrid.Location = new System.Drawing.Point(120, 9);
            this.DescriptionsGrid.Name = "DescriptionsGrid";
            this.DescriptionsGrid.Order = null;
            this.DescriptionsGrid.Size = new System.Drawing.Size(608, 207);
            this.DescriptionsGrid.TabIndex = 29;
            this.DescriptionsGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle10});
            // 
            // extendedDataGridTableStyle10
            // 
            this.extendedDataGridTableStyle10.DataGrid = this.DescriptionsGrid;
            this.extendedDataGridTableStyle10.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn5,
            this.extendedDataGridSelectorColumn1});
            this.extendedDataGridTableStyle10.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle10.MappingName = "Table";
            // 
            // extendedDataGridComboBoxColumn5
            // 
            this.extendedDataGridComboBoxColumn5.FieldName = null;
            this.extendedDataGridComboBoxColumn5.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn5.Format = "";
            this.extendedDataGridComboBoxColumn5.FormatInfo = null;
            this.extendedDataGridComboBoxColumn5.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn5.MappingName = "ordValue";
            this.extendedDataGridComboBoxColumn5.Width = 75;
            // 
            // extendedDataGridSelectorColumn1
            // 
            this.extendedDataGridSelectorColumn1.FieldName = null;
            this.extendedDataGridSelectorColumn1.FilterFieldName = null;
            this.extendedDataGridSelectorColumn1.Format = "";
            this.extendedDataGridSelectorColumn1.FormatInfo = null;
            this.extendedDataGridSelectorColumn1.HeaderText = "Описание";
            this.extendedDataGridSelectorColumn1.MappingName = "propValue";
            this.extendedDataGridSelectorColumn1.Width = 75;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(8, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 19);
            this.label13.TabIndex = 25;
            this.label13.Text = "Описание:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AccessoryPage
            // 
            this.AccessoryPage.Controls.Add(this.dgAccessories);
            this.AccessoryPage.Location = new System.Drawing.Point(4, 25);
            this.AccessoryPage.Name = "AccessoryPage";
            this.AccessoryPage.Size = new System.Drawing.Size(954, 418);
            this.AccessoryPage.TabIndex = 2;
            this.AccessoryPage.Text = "Аксессуары";
            // 
            // dgAccessories
            // 
            this.dgAccessories.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.dgAccessories.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.dgAccessories.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.dgAccessories.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.dgAccessories.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dgAccessories.ColumnHeadersVisibleOfDataGrid = true;
            this.dgAccessories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAccessories.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.dgAccessories.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.dgAccessories.Location = new System.Drawing.Point(0, 0);
            this.dgAccessories.Name = "dgAccessories";
            this.dgAccessories.Size = new System.Drawing.Size(954, 418);
            this.dgAccessories.TabIndex = 0;
            this.dgAccessories.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
            this.dgAccessories.AddClick += new System.EventHandler(this.addDelDataGridExt1_AddClick);
            this.dgAccessories.DeleteClick += new System.EventHandler(this.addDelDataGridExt1_DeleteClick);
            // 
            // extendedDataGridTableStyle2
            // 
            this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn2});
            this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle2.MappingName = "table";
            this.extendedDataGridTableStyle2.ReadOnly = true;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Название";
            this.formattableTextBoxColumn2.MappingName = "propValue_NK";
            this.formattableTextBoxColumn2.Width = 200;
            // 
            // ReviewPage
            // 
            this.ReviewPage.Controls.Add(this.dgPressReleases);
            this.ReviewPage.Location = new System.Drawing.Point(4, 25);
            this.ReviewPage.Name = "ReviewPage";
            this.ReviewPage.Size = new System.Drawing.Size(954, 418);
            this.ReviewPage.TabIndex = 3;
            this.ReviewPage.Text = "Статьи и обзоры";
            // 
            // dgPressReleases
            // 
            this.dgPressReleases.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.dgPressReleases.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.dgPressReleases.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.dgPressReleases.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.dgPressReleases.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dgPressReleases.ColumnHeadersVisibleOfDataGrid = true;
            this.dgPressReleases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPressReleases.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.dgPressReleases.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.dgPressReleases.Location = new System.Drawing.Point(0, 0);
            this.dgPressReleases.Name = "dgPressReleases";
            this.dgPressReleases.Size = new System.Drawing.Size(954, 418);
            this.dgPressReleases.TabIndex = 1;
            this.dgPressReleases.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle3});
            this.dgPressReleases.AddClick += new System.EventHandler(this.addDelDataGridExt2_AddClick);
            this.dgPressReleases.DeleteClick += new System.EventHandler(this.addDelDataGridExt2_DeleteClick);
            // 
            // extendedDataGridTableStyle3
            // 
            this.extendedDataGridTableStyle3.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn3});
            this.extendedDataGridTableStyle3.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle3.MappingName = "table";
            this.extendedDataGridTableStyle3.ReadOnly = true;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "№";
            this.formattableTextBoxColumn1.MappingName = "ordValue";
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Название";
            this.formattableTextBoxColumn3.MappingName = "propValue_NK";
            this.formattableTextBoxColumn3.Width = 200;
            // 
            // KeywordsPage
            // 
            this.KeywordsPage.Controls.Add(this.KeywordsGrid);
            this.KeywordsPage.Controls.Add(this.panel5);
            this.KeywordsPage.Controls.Add(this.panel4);
            this.KeywordsPage.Location = new System.Drawing.Point(4, 25);
            this.KeywordsPage.Name = "KeywordsPage";
            this.KeywordsPage.Size = new System.Drawing.Size(954, 418);
            this.KeywordsPage.TabIndex = 4;
            this.KeywordsPage.Text = "Ключевые слова";
            // 
            // KeywordsGrid
            // 
            this.KeywordsGrid.BackgroundColor = System.Drawing.Color.White;
            this.KeywordsGrid.DataMember = "";
            this.KeywordsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeywordsGrid.FilterString = null;
            this.KeywordsGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.KeywordsGrid.Location = new System.Drawing.Point(0, 33);
            this.KeywordsGrid.Name = "KeywordsGrid";
            this.KeywordsGrid.Order = null;
            this.KeywordsGrid.Size = new System.Drawing.Size(924, 385);
            this.KeywordsGrid.TabIndex = 6;
            this.KeywordsGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle11});
            // 
            // extendedDataGridTableStyle11
            // 
            this.extendedDataGridTableStyle11.DataGrid = this.KeywordsGrid;
            this.extendedDataGridTableStyle11.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7});
            this.extendedDataGridTableStyle11.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle11.MappingName = "Table";
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = null;
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "№";
            this.formattableTextBoxColumn6.MappingName = "ordValue";
            this.formattableTextBoxColumn6.ReadOnly = true;
            this.formattableTextBoxColumn6.Width = 75;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = null;
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "Ключевое слово";
            this.formattableTextBoxColumn7.MappingName = "propValue";
            this.formattableTextBoxColumn7.Width = 75;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.DelKeyword);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(924, 33);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(30, 385);
            this.panel5.TabIndex = 5;
            // 
            // DelKeyword
            // 
            this.DelKeyword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DelKeyword.Location = new System.Drawing.Point(6, 7);
            this.DelKeyword.Name = "DelKeyword";
            this.DelKeyword.Size = new System.Drawing.Size(21, 26);
            this.DelKeyword.TabIndex = 0;
            this.DelKeyword.Text = "-";
            this.DelKeyword.UseVisualStyleBackColor = true;
            this.DelKeyword.Click += new System.EventHandler(this.DelKeyword_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LoadKeywordsButton);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(954, 33);
            this.panel4.TabIndex = 3;
            // 
            // LoadKeywordsButton
            // 
            this.LoadKeywordsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoadKeywordsButton.Location = new System.Drawing.Point(4, 3);
            this.LoadKeywordsButton.Name = "LoadKeywordsButton";
            this.LoadKeywordsButton.Size = new System.Drawing.Size(128, 27);
            this.LoadKeywordsButton.TabIndex = 1;
            this.LoadKeywordsButton.Text = "Загрузить из буфера";
            this.LoadKeywordsButton.UseVisualStyleBackColor = true;
            this.LoadKeywordsButton.Click += new System.EventHandler(this.LoadKeywordsButton_Click);
            // 
            // AnalogPage
            // 
            this.AnalogPage.Controls.Add(this.AnalogsAddDelDataGrid);
            this.AnalogPage.Location = new System.Drawing.Point(4, 25);
            this.AnalogPage.Name = "AnalogPage";
            this.AnalogPage.Padding = new System.Windows.Forms.Padding(3);
            this.AnalogPage.Size = new System.Drawing.Size(954, 418);
            this.AnalogPage.TabIndex = 6;
            this.AnalogPage.Text = "Аналоги";
            this.AnalogPage.UseVisualStyleBackColor = true;
            // 
            // AnalogsAddDelDataGrid
            // 
            this.AnalogsAddDelDataGrid.BackColor = System.Drawing.SystemColors.Control;
            this.AnalogsAddDelDataGrid.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.AnalogsAddDelDataGrid.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.AnalogsAddDelDataGrid.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.AnalogsAddDelDataGrid.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.AnalogsAddDelDataGrid.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnalogsAddDelDataGrid.ColumnHeadersVisibleOfDataGrid = true;
            this.AnalogsAddDelDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AnalogsAddDelDataGrid.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.AnalogsAddDelDataGrid.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.AnalogsAddDelDataGrid.Location = new System.Drawing.Point(3, 3);
            this.AnalogsAddDelDataGrid.Name = "AnalogsAddDelDataGrid";
            this.AnalogsAddDelDataGrid.Size = new System.Drawing.Size(948, 412);
            this.AnalogsAddDelDataGrid.TabIndex = 0;
            this.AnalogsAddDelDataGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle12});
            this.AnalogsAddDelDataGrid.AddClick += new System.EventHandler(this.AnalogsAddDelDataGrid_AddClick);
            this.AnalogsAddDelDataGrid.DeleteClick += new System.EventHandler(this.AnalogsAddDelDataGrid_DeleteClick);
            // 
            // extendedDataGridTableStyle12
            // 
            this.extendedDataGridTableStyle12.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn10,
            this.formattableTextBoxColumn11});
            this.extendedDataGridTableStyle12.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle12.MappingName = "Table";
            this.extendedDataGridTableStyle12.ReadOnly = true;
            // 
            // formattableTextBoxColumn10
            // 
            this.formattableTextBoxColumn10.FieldName = null;
            this.formattableTextBoxColumn10.FilterFieldName = null;
            this.formattableTextBoxColumn10.Format = "";
            this.formattableTextBoxColumn10.FormatInfo = null;
            this.formattableTextBoxColumn10.HeaderText = "№";
            this.formattableTextBoxColumn10.MappingName = "ordValue";
            this.formattableTextBoxColumn10.Width = 75;
            // 
            // formattableTextBoxColumn11
            // 
            this.formattableTextBoxColumn11.FieldName = null;
            this.formattableTextBoxColumn11.FilterFieldName = null;
            this.formattableTextBoxColumn11.Format = "";
            this.formattableTextBoxColumn11.FormatInfo = null;
            this.formattableTextBoxColumn11.HeaderText = "Название";
            this.formattableTextBoxColumn11.MappingName = "propValue_NK";
            this.formattableTextBoxColumn11.Width = 75;
            // 
            // InstructionPage
            // 
            this.InstructionPage.Controls.Add(this.DocsDataGrid);
            this.InstructionPage.Location = new System.Drawing.Point(4, 25);
            this.InstructionPage.Name = "InstructionPage";
            this.InstructionPage.Padding = new System.Windows.Forms.Padding(3);
            this.InstructionPage.Size = new System.Drawing.Size(954, 418);
            this.InstructionPage.TabIndex = 7;
            this.InstructionPage.Text = "Инструкции";
            this.InstructionPage.UseVisualStyleBackColor = true;
            // 
            // DocsDataGrid
            // 
            this.DocsDataGrid.BackColor = System.Drawing.SystemColors.Control;
            this.DocsDataGrid.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.DocsDataGrid.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.DocsDataGrid.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.DocsDataGrid.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.DocsDataGrid.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DocsDataGrid.ColumnHeadersVisibleOfDataGrid = true;
            this.DocsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DocsDataGrid.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.DocsDataGrid.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.DocsDataGrid.Location = new System.Drawing.Point(3, 3);
            this.DocsDataGrid.Name = "DocsDataGrid";
            this.DocsDataGrid.Size = new System.Drawing.Size(948, 412);
            this.DocsDataGrid.TabIndex = 0;
            this.DocsDataGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle13});
            this.DocsDataGrid.AddClick += new System.EventHandler(this.DocsDataGrid_AddClick);
            this.DocsDataGrid.DeleteClick += new System.EventHandler(this.DocsData_DeleteClick);
            // 
            // extendedDataGridTableStyle13
            // 
            this.extendedDataGridTableStyle13.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn12,
            this.formattableTextBoxColumn13});
            this.extendedDataGridTableStyle13.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle13.MappingName = "Table";
            this.extendedDataGridTableStyle13.ReadOnly = true;
            // 
            // formattableTextBoxColumn12
            // 
            this.formattableTextBoxColumn12.FieldName = null;
            this.formattableTextBoxColumn12.FilterFieldName = null;
            this.formattableTextBoxColumn12.Format = "";
            this.formattableTextBoxColumn12.FormatInfo = null;
            this.formattableTextBoxColumn12.HeaderText = "Тип";
            this.formattableTextBoxColumn12.MappingName = "ordValue";
            this.formattableTextBoxColumn12.Width = 75;
            // 
            // formattableTextBoxColumn13
            // 
            this.formattableTextBoxColumn13.FieldName = null;
            this.formattableTextBoxColumn13.FilterFieldName = null;
            this.formattableTextBoxColumn13.Format = "";
            this.formattableTextBoxColumn13.FormatInfo = null;
            this.formattableTextBoxColumn13.HeaderText = "Данные";
            this.formattableTextBoxColumn13.MappingName = "propValue_NK";
            this.formattableTextBoxColumn13.Width = 75;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnCancel);
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Controls.Add(this.btnAttachImages);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 447);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(962, 46);
            this.panel3.TabIndex = 48;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(8, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(64, 28);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(872, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 28);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAttachImages
            // 
            this.btnAttachImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAttachImages.BackColor = System.Drawing.SystemColors.Control;
            this.btnAttachImages.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAttachImages.Location = new System.Drawing.Point(136, 9);
            this.btnAttachImages.Name = "btnAttachImages";
            this.btnAttachImages.Size = new System.Drawing.Size(120, 28);
            this.btnAttachImages.TabIndex = 9;
            this.btnAttachImages.Text = "Привязка картинок";
            this.btnAttachImages.UseVisualStyleBackColor = false;
            this.btnAttachImages.Click += new System.EventHandler(this.btnAttachImages_Click);
            // 
            // EditGoods
            // 
            this.ClientSize = new System.Drawing.Size(962, 493);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel3);
            this.MinimumSize = new System.Drawing.Size(752, 523);
            this.Name = "EditGoods";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.Text = "Карточка товара";
            this.Load += new System.EventHandler(this.EditGoods_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsInStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPrices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsHit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsSales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsIsFavorites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.GeneralPage.ResumeLayout(false);
            this.GeneralPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VariantTypeSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgInStocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrices)).EndInit();
            this.AdditionalPage.ResumeLayout(false);
            this.AdditionalPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsFavorites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsSale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsHit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgIsNew)).EndInit();
            this.ParamPage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgParams)).EndInit();
            this.panel44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionsGrid)).EndInit();
            this.AccessoryPage.ResumeLayout(false);
            this.ReviewPage.ResumeLayout(false);
            this.KeywordsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KeywordsGrid)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.AnalogPage.ResumeLayout(false);
            this.InstructionPage.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private TextBox YandexUrl;
		private Label label12;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private System.Data.DataColumn dataColumn8;
		private System.Data.DataColumn dataColumn9;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn7;
		private System.Data.DataColumn dataColumn10;
		private ColumnMenuExtender.DataGridISM DescriptionsGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle10;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn5;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
		private Panel panel4;
		private Button LoadKeywordsButton;
		private ColumnMenuExtender.DataGridISM KeywordsGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle11;
		private Panel panel5;
		private Button DelKeyword;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
		private System.Data.DataColumn dataColumn13;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn9;
		private System.Data.DataColumn dataColumn14;
		private TabPage AnalogPage;
		private ColumnMenuExtender.UserControls.AddDelDataGridExt AnalogsAddDelDataGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle12;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn10;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn11;
		private TabPage InstructionPage;
		private ColumnMenuExtender.UserControls.AddDelDataGridExt DocsDataGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle13;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn12;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn13;
		private Label VariantLabel;
		private ColumnMenuExtender.DataBoundComboBox VariantTypeSelect;
		private System.Data.DataColumn dataColumn15;
		private System.Data.DataColumn dataColumn16;
		private System.Data.DataColumn dataColumn17;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn8;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn14;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn15;
		private System.Data.DataColumn dataColumn18;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn9;
		private TextBox SalesNotesText;
		private Label label14;
        private System.Data.DataColumn dataColumn19;
        private ColumnMenuExtender.ExtendedDataGridDTPickerColumn extendedDataGridDTPickerColumn1;
    }
}
