﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using ELBClient.BusinessProvider;
using TreeNode = System.Windows.Forms.TreeNode;
using TreeView = System.Windows.Forms.TreeView;

namespace ELBClient.Forms.Guides
{
    public partial class EditCommodityGroup : EditForm
    {
        public string Name { get; set; }
        public override string ObjectNK => ((CCommodityGroup)Object)?.Name;
        private readonly TreeProvider.TreeProvider _treeProvider = ServiceUtility.TreeProvider;
        private readonly BusinessProvider.BusinessProvider _businessProvider = ServiceUtility.BusinessProvider;
        //private readonly Tree regionsTree = new Tree(0, int.MaxValue);
        private readonly Tree catalogTree = new Tree(0, int.MaxValue);
        private DataSet catalogDataSet;


        public int GroupType { get; set; }
        Binding bindShopType;
        public EditCommodityGroup()
        {
            InitializeComponent();
            Object = new CCommodityGroup();
        }

        private void EditCommodityGroup_Load(object sender, EventArgs e)
        {
            LoadLookup();
            LoadCatalog();
            BuildCatalogTree();
            LoadData();
            BindFields();
        }

        private void LoadCatalog()
        {
            var element = XElement.Parse("<tree />");
            var parentNode = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(1);
            element.Add(new XAttribute("nodeID", parentNode),
                new XAttribute("withParent", "true"));
            catalogDataSet = _treeProvider.GetTreeContent(element.ToString());
        }

        private void BuildCatalogTree()
        {
            DoBuildTree(catalogDataSet, catalogTree);
        }

        private void DoBuildTree(DataSet dataSet, Tree tree)
        {
            var nodes = new Stack();
            nodes.Push(tree);

            foreach (DataRow dr in dataSet.Tables["table"].Rows)
            {
                var nodeID = (int)dr["nodeID"];
                var node = new Node((int)dr["lft"], (int)dr["rgt"])
                {
                    NodeId = nodeID,
                    Name = dr["nodeName"].ToString(),
                    objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"]
                };
                while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
                ((NodeContainer)nodes.Peek()).Nodes.Add(node);
                if ((node.Left + 1) != node.Right)
                {
                    nodes.Push(node);
                }
            }
        }

        private void LoadCatalogTree()
        {
            DoLoadTree(CatalogTreeView, catalogTree,
                ManufThemesDataSet.Table
                .AsEnumerable()
                .Where(row => row.Field<Guid?>("themeOID").HasValue)
                .ToList());
            FillFilters();
        }

        private Node FindCommonParent()
        {
            HashSet<TreeNode> candidates = new HashSet<TreeNode>();
            foreach (TreeNode node in CatalogTreeView.Nodes)
            {
                if (node.Checked) candidates.Add(node);
                FillCandidates(candidates, node);
            }

            if (candidates.Count == 1)
            {
                return (Node)candidates.First().Tag;
            }

            if (candidates.Count > 1)
            {
                var firstNode = candidates.First();
                foreach (var nextNode in candidates.Skip(1))
                {
                    var parentFirst = new List<TreeNode>();
                    parentFirst.Insert(0, firstNode);
                    TreeNode nextParent = firstNode.Parent;
                    while (nextParent != null)
                    {
                        parentFirst.Insert(0, nextParent);
                        nextParent = nextParent.Parent;
                    }

                    var parentNext = new List<TreeNode>();
                    parentNext.Insert(0, nextNode);
                    nextParent = nextNode.Parent;
                    while (nextParent != null)
                    {
                        parentNext.Insert(0, nextParent);
                        nextParent = nextParent.Parent;
                    }
                    for (int i = 0; i < parentFirst.Count && i < parentNext.Count; i++)
                    {
                        if (parentFirst[i] == parentNext[i]) continue;
                        firstNode = parentFirst[i - 1];
                        break;
                    }
                }

                return (Node)firstNode.Tag;
            }
            return null;
        }

        private void FillCandidates(HashSet<TreeNode> candidates, TreeNode parent)
        {
            foreach (TreeNode node in parent.Nodes)
            {
                if (node.Checked) candidates.Add(node);
                FillCandidates(candidates, node);
            }
        }

        private void DoLoadTree(TreeView treeView, Tree tree, IList<DataRow> multiContainer)
        {
            treeView.Nodes.Clear();
            foreach (Node n in tree.Nodes)
            {
                var tn = CreateTreeNode(multiContainer, n);
                treeView.Nodes.Add(tn);
                ExpandParents(tn);
                LoadCatalogNode(tn, n, multiContainer);
            }
        }

        private static TreeNode CreateTreeNode(IList<DataRow> multiContainer, Node node)
        {
            var tn = new TreeNode(node.Name != "" ? node.Name.Replace("\r\n", " ") : "<Unknown>");
            var OID = node.objectOID;
            var nodeChecked = false;
            if (OID != Guid.Empty && multiContainer.Count(row => row.Field<Guid>("themeOID") == OID) != 0) nodeChecked = true;
            else if (OID == Guid.Empty && !multiContainer.Any()) nodeChecked = true;
            tn.Checked = nodeChecked;
            tn.Tag = node;
            return tn;
        }

        private void LoadCatalogNode(TreeNode treeNode, Node node, IList<DataRow> multiContainer)
        {
            foreach (Node n in node.Nodes)
            {
                var tn = CreateTreeNode(multiContainer, n);
                treeNode.Nodes.Add(tn);
                ExpandParents(tn);
                LoadCatalogNode(tn, n, multiContainer);
            }
        }

        private static void ExpandParents(TreeNode node)
        {
            if (node.Checked)
            {
                var parent = node.Parent;
                while (parent != null)
                {
                    parent.Expand();
                    parent = parent.Parent;
                }
            }
        }

        private void BindFields()
        {
            NameText.DataBindings.Add("Text", Object, "Name");
            bindShopType = new Binding("SelectedValue", Object, "ShopType");
            ShopTypeSelect.DataBindings.Add(bindShopType);
            UrlText.DataBindings.Add("Text", Object, "Url");
            DateModifySelect.DataBindings.Add("Text", Object, "DateModify");
            LastGenerateSelect.DataBindings.Add("Text", Object, "LastGenerate");
            PriceAggregatorText.DataBindings.Add("Text", Object, "PriceAggregator");
            GlobalUtmLabelText.DataBindings.Add("Text", Object, "GlobalUtmLabel");
            IncludeBidCBidCheck.DataBindings.Add("Checked", Object, "IncludeBidCBid");
            IsActiveCheck.DataBindings.Add("Checked", Object, "IsActive");
            YandexDtdCheck.DataBindings.Add("Checked", Object, "YandexDtd");
            ParamToDescriptionCheck.DataBindings.Add("Checked", Object, "ParamToDescription");
            TorgMailDtdCheck.DataBindings.Add("Checked", Object, "TorgMailDtd");
            ForContextCheck.DataBindings.Add("Checked", Object, "ForContext");
            ForABCCheck.DataBindings.Add("Checked", Object, "ForABC");
            CategoryUrlCheck.DataBindings.Add("Checked", Object, "CategoryUrl");
            UseLargeImageCheck.DataBindings.Add("Checked", Object, "UseLargeImage");
            PostavshikIdCheck.DataBindings.Add("Checked", Object, "Postavshik_id");
            GoodsGrid.DataSource = new DataView(GoodsDataSet.Table, null, "ordValue", DataViewRowState.CurrentRows);
        }

        private void LoadData()
        {
            if (ObjectOID == Guid.Empty)
                InitEmptyObject();
            else
            {
                var xml = GetResourceString("fmEditCommodityGroup.xml");
                Object.LoadMultiPropFlag = false;
                xml = LoadObject(xml, CCommodityGroup.ClassCID);
                ManufThemesDataSet.LoadRowsFromXML("Table", xml, "manufThemes");
                GoodsDataSet.LoadRowsFromXML("Table", xml, "goods");
                ThemeParamsDataSet.LoadRowsFromXML("Table", xml, "themeParams");
            }
            LoadCatalogTree();
        }

        private void InitEmptyObject()
        {
            ((CCommodityGroup) Object).GroupType = GroupType;
            if (!string.IsNullOrWhiteSpace(Name))
            {
                ((CCommodityGroup) Object).Name = Name;
            }
        }

        private void LoadLookup()
        {
            var xml = lp.GetLookupValues("CCommodityGroup");
            ShopTypeSelect.LoadXml(xml, "shopTypeN");
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsModified)
            {
                SaveObject();
            }
        }

        private void AddGoods_Click(object sender, EventArgs e)
        {
            if (ShopTypeSelect.SelectedIndex == -1)
            {
                ShowError("Выберите магазин");
                return;
            }
            var fm = new ListGoodsDialog { ShopType = ShopTypeSelect.SelectedValue };
            if (fm.ShowDialog(this) != DialogResult.OK) return;
            var i = GoodsDataSet.Table.AsEnumerable().Select(row => row.Field<int>("ordValue")).DefaultIfEmpty(0).Max();
            foreach (var dr in fm.SelectedRows)
            {
                try
                {
                    if (dr == null) continue;
                    var o = (Guid)dr["OID"];
                    var s = dr["NK"].ToString();
                    GoodsDataSet.Table.Rows.Add(new object[] { o, s, "CGoods", ++i });
                }
                catch (ConstraintException)
                {
                    //ShowError("Товар уже добавлен");
                }
            }
        }

        protected override string AllowToSave()
        {
            if ((Object as CCommodityGroup).ShopType == int.MinValue) return "Выберите магазин";
            return base.AllowToSave();
        }

        protected override bool IsModified
        {
            get
            {
                return CatalogTreeView.IsModified || FiltersTreeView.IsModified || GoodsDataSet.IsModified || base.IsModified;
            }
        }

        protected override void SaveObject()
        {
            if (CatalogTreeView.IsModified || FiltersTreeView.IsModified)
            {
                var categories = CatalogTreeView.GetCheckedNodes();
                var filters = FiltersTreeView.GetCheckedNodes();
                foreach (DataRow row in ManufThemesDataSet.Table.Rows)
                {
                    row.Delete();
                }
                foreach (DataRow row in ThemeParamsDataSet.Table.Rows)
                {
                    row.Delete();
                }
                var manufs = filters.Where(n => n.Tag is Manufacturer).Select(n => ((Manufacturer)n.Tag).OID).ToList();
                var i = 1;
                foreach (var category in categories.Where(c => ((Node)c.Tag).objectOID != Guid.Empty))
                {
                    if (manufs.Count == 0)
                    {
                        var row = ManufThemesDataSet.Table.NewRow();
                        row["ordValue"] = i++;
                        row["themeOID"] = ((Node)category.Tag).objectOID;
                        ManufThemesDataSet.Table.Rows.Add(row);
                    }
                    else
                    {
                        foreach (var manuf in manufs)
                        {
                            var row = ManufThemesDataSet.Table.NewRow();
                            row["ordValue"] = i++;
                            row["propValue"] = manuf;
                            row["themeOID"] = ((Node)category.Tag).objectOID;
                            ManufThemesDataSet.Table.Rows.Add(row);
                        }
                    }

                }
                Node catalogNode = FindCommonParent();
                List<int> customFilterValues = filters.Where(n => n.Tag is CategoryFilterValue).Select(n => ((CategoryFilterValue)n.Tag).Id).ToList();
                foreach (var value in customFilterValues)
                {
                    var row = ThemeParamsDataSet.Table.NewRow();
                    row["id"] = value;
                    ThemeParamsDataSet.Table.Rows.Add(row);
                }
            }
            if (ManufThemesDataSet.IsModified)
                Object.ExtMultiProp = ManufThemesDataSet.GetChangesXml("manufThemes", new[] { "ordValue", "propValue", "themeOID" }, new[] { "ordValue", "manufacturerOID", "themeOID" });
            if (ThemeParamsDataSet.IsModified)
                Object.ExtMultiProp += ThemeParamsDataSet.GetChangesXml("themeParams");
            if (GoodsDataSet.IsModified)
                Object.ExtMultiProp += GoodsDataSet.GetChangesXml("goods", new[] { "propValue", "ordValue" }, new[] { "goodsOID", "ordValue" });
            base.SaveObject();
            ManufThemesDataSet.AcceptChanges();
            GoodsDataSet.AcceptChanges();
            CatalogTreeView.AcceptChanges();
            FiltersTreeView.AcceptChanges();
        }

        private void DelGoods_Click(object sender, EventArgs e)
        {
            var dr = GoodsGrid.GetSelectedRow();
            if (dr == null) return;
            dr.Delete();
        }

        private void CatalogTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            FillFilters();
        }

        private void FillFilters()
        {
            try
            {
                var commonParentNode = FindCommonParent();
                var parentNode =
                    (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(((CCommodityGroup)Object).ShopType);
                var filters = _businessProvider.GetCategoryManufacturers(((CCommodityGroup)Object).ShopType,
                    parentNode,
                    commonParentNode.objectOID);
                FiltersTreeView.Nodes.Clear();
                var tn = new TreeNode(commonParentNode.Name != "" ? commonParentNode.Name.Replace("\r\n", " ") : "<Unknown>");
                FiltersTreeView.Nodes.Add(tn);
                var brandsNode = new TreeNode("Бренды");
                tn.Nodes.Add(brandsNode);
                foreach (var manufacturer in filters.Manufacturers)
                {
                    var node = new TreeNode(manufacturer.Name != "" ? manufacturer.Name.Replace("\r\n", " ") : "<Unknown>");
                    node.Tag = manufacturer;
                    if (ManufThemesDataSet.Table.AsEnumerable().Any(r => (r.Field<Guid?>("propValue") ?? Guid.Empty) == manufacturer.OID))
                    {
                        node.Checked = true;
                    }
                    brandsNode.Nodes.Add(node);
                    ExpandParents(node);
                }
                foreach (var filter in filters.Filters)
                {
                    var filterNode = new TreeNode(filter.Name);
                    tn.Nodes.Add(filterNode);
                    foreach (CategoryFilterValue value in filter.Values)
                    {
                        var valueNode = new TreeNode(value.Name);
                        valueNode.Tag = value;
                        if (ThemeParamsDataSet.Table.AsEnumerable().Any(r => r.Field<int>("id") == value.Id))
                        {
                            valueNode.Checked = true;
                        }
                        filterNode.Nodes.Add(valueNode);
                        ExpandParents(valueNode);
                    }
                }

            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }
    }
}

