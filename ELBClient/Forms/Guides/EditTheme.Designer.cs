using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditTheme
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label3;
		private ColumnMenuExtender.DataGridISM dgParams;
		private System.Windows.Forms.Button btnFromTemplate;
		private System.Windows.Forms.Button btnDelParam;
		private System.Windows.Forms.Button btnAddParam;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private ColumnMenuExtender.UserControls.AddDelDataGridExt dgPressReleases;
		private System.Windows.Forms.TabPage tabPage3;
		private ColumnMenuExtender.UserControls.AddDelDataGridExt dgAccessories;
		private System.Windows.Forms.ContextMenu cmAddAccessory;
		private System.Windows.Forms.MenuItem miAddGoods;
		private System.Windows.Forms.MenuItem miAddTheme;
		private ColumnMenuExtender.DataBoundComboBox ddlShopType;
		private System.Windows.Forms.Label label1;
		private Button btnImages;
		private TabPage tabPage4;
		private ColumnMenuExtender.DataGridISM PriceIntervals;
		private Panel panel2;
		private Label label2;
		private ColumnMenuExtender.DataBoundTextBox PriceLimit;
		private CheckBox LoadToYandex;
		private Button button2;
		private System.Windows.Forms.TextBox NameText;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colOrdValue;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colType;
		private ColumnMenuExtender.FormattableTextBoxColumn colUnit;
		private ColumnMenuExtender.FormattableBooleanColumn colIsDomain;
		private MetaData.DataSetISM dsParams;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dcOrdValue;
		private System.Data.DataColumn dcOID;
		private System.Data.DataColumn dcName;
		private System.Data.DataColumn dcType;
		private System.Data.DataColumn dcUnit;
		private System.Data.DataColumn dcIsDomain;
		private System.Data.DataView dataView1;
		private ColumnMenuExtender.FormattableBooleanColumn colIsMandatory;
		private System.Data.DataColumn dcIsMandatory;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn colHeader;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private System.Data.DataColumn dataColumn1;
		private ColumnMenuExtender.FormattableBooleanColumn colIsFullName;
		private System.Data.DataColumn dataColumn2;
		private ColumnMenuExtender.FormattableBooleanColumn colIsFilter;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle5;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;

		private void InitializeComponent()
		{
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.NameText = new System.Windows.Forms.TextBox();
            this.dgParams = new ColumnMenuExtender.DataGridISM();
            this.dataView1 = new System.Data.DataView();
            this.dataTable1 = new System.Data.DataTable();
            this.dcOrdValue = new System.Data.DataColumn();
            this.dcOID = new System.Data.DataColumn();
            this.dcName = new System.Data.DataColumn();
            this.dcType = new System.Data.DataColumn();
            this.dcUnit = new System.Data.DataColumn();
            this.dcIsDomain = new System.Data.DataColumn();
            this.dcIsMandatory = new System.Data.DataColumn();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.colOrdValue = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colType = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.colUnit = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colIsDomain = new ColumnMenuExtender.FormattableBooleanColumn();
            this.colIsMandatory = new ColumnMenuExtender.FormattableBooleanColumn();
            this.colIsFullName = new ColumnMenuExtender.FormattableBooleanColumn();
            this.colIsFilter = new ColumnMenuExtender.FormattableBooleanColumn();
            this.dsParams = new MetaData.DataSetISM();
            this.btnFromTemplate = new System.Windows.Forms.Button();
            this.btnDelParam = new System.Windows.Forms.Button();
            this.btnAddParam = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.H1Text = new System.Windows.Forms.TextBox();
            this.GoodsListCheck = new System.Windows.Forms.CheckBox();
            this.GoogleSelect = new ELBClient.UserControls.SelectorTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPageDescription = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ShortLinkText = new System.Windows.Forms.TextBox();
            this.FromGoods = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlShopType = new ColumnMenuExtender.DataBoundComboBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.PriceIntervals = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle5 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.ShopTypeColumn = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.PriceLimit = new ColumnMenuExtender.DataBoundTextBox();
            this.LoadToYandex = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgAccessories = new ColumnMenuExtender.UserControls.AddDelDataGridExt();
            this.extendedDataGridTableStyle3 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgPressReleases = new ColumnMenuExtender.UserControls.AddDelDataGridExt();
            this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.colHeader = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.SalesNotesGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle6 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.brandSalesNotesColumn = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.formattableTextBoxColumn9 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.SeoTextGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle4 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.AddButton = new System.Windows.Forms.Button();
            this.DelButton = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.TagsDataGrid = new ColumnMenuExtender.UserControls.AddDelPositionDataGrid();
            this.extendedDataGridTableStyle7 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn10 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn11 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.TitlesGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle8 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.brandTitleColumn = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.formattableTextBoxColumn12 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn14 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn13 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnImages = new System.Windows.Forms.Button();
            this.cmAddAccessory = new System.Windows.Forms.ContextMenu();
            this.miAddGoods = new System.Windows.Forms.MenuItem();
            this.miAddTheme = new System.Windows.Forms.MenuItem();
            this.MultiProps = new System.Data.DataSet();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataTable7 = new System.Data.DataTable();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataTable8 = new System.Data.DataTable();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.SeoTextsDataSet = new MetaData.DataSetISM();
            this.dataTable5 = new System.Data.DataTable();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.SalesNotesDataSet = new MetaData.DataSetISM();
            this.dataTable6 = new System.Data.DataTable();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.label10 = new System.Windows.Forms.Label();
            this.AltNameText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsParams)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlShopType)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PriceIntervals)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SalesNotesGrid)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SeoTextGrid)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TitlesGrid)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MultiProps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeoTextsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalesNotesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(8, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(834, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 27);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(9, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Название:";
            // 
            // NameText
            // 
            this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameText.Location = new System.Drawing.Point(120, 16);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(738, 20);
            this.NameText.TabIndex = 1;
            // 
            // dgParams
            // 
            this.dgParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgParams.BackgroundColor = System.Drawing.Color.White;
            this.dgParams.DataMember = "";
            this.dgParams.DataSource = this.dataView1;
            this.dgParams.FilterString = null;
            this.dgParams.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgParams.Location = new System.Drawing.Point(8, 283);
            this.dgParams.Name = "dgParams";
            this.dgParams.Order = null;
            this.dgParams.Size = new System.Drawing.Size(858, 379);
            this.dgParams.TabIndex = 13;
            this.dgParams.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // dataView1
            // 
            this.dataView1.AllowDelete = false;
            this.dataView1.AllowNew = false;
            this.dataView1.Sort = "ordValue";
            this.dataView1.Table = this.dataTable1;
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dcOrdValue,
            this.dcOID,
            this.dcName,
            this.dcType,
            this.dcUnit,
            this.dcIsDomain,
            this.dcIsMandatory,
            this.dataColumn1,
            this.dataColumn2});
            this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "paramTypeOID"}, true)});
            this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dcOID};
            this.dataTable1.TableName = "table";
            // 
            // dcOrdValue
            // 
            this.dcOrdValue.AllowDBNull = false;
            this.dcOrdValue.Caption = "N";
            this.dcOrdValue.ColumnName = "ordValue";
            this.dcOrdValue.DataType = typeof(int);
            this.dcOrdValue.DefaultValue = 0;
            // 
            // dcOID
            // 
            this.dcOID.AllowDBNull = false;
            this.dcOID.ColumnName = "paramTypeOID";
            this.dcOID.DataType = typeof(System.Guid);
            // 
            // dcName
            // 
            this.dcName.Caption = "Название";
            this.dcName.ColumnName = "name";
            // 
            // dcType
            // 
            this.dcType.Caption = "Тип";
            this.dcType.ColumnName = "type";
            // 
            // dcUnit
            // 
            this.dcUnit.Caption = "Ед изм.";
            this.dcUnit.ColumnName = "unit";
            // 
            // dcIsDomain
            // 
            this.dcIsDomain.Caption = "Список";
            this.dcIsDomain.ColumnName = "isDomain";
            this.dcIsDomain.DataType = typeof(bool);
            // 
            // dcIsMandatory
            // 
            this.dcIsMandatory.AllowDBNull = false;
            this.dcIsMandatory.ColumnName = "isMandatory";
            this.dcIsMandatory.DataType = typeof(bool);
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "isFullName";
            this.dataColumn1.DataType = typeof(bool);
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "isFilter";
            this.dataColumn2.DataType = typeof(bool);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.dgParams;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colOrdValue,
            this.colName,
            this.colType,
            this.colUnit,
            this.colIsDomain,
            this.colIsMandatory,
            this.colIsFullName,
            this.colIsFilter});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            // 
            // colOrdValue
            // 
            this.colOrdValue.FieldName = null;
            this.colOrdValue.FilterFieldName = null;
            this.colOrdValue.Format = "";
            this.colOrdValue.FormatInfo = null;
            this.colOrdValue.HeaderText = "№";
            this.colOrdValue.MappingName = "ordValue";
            this.colOrdValue.Width = 75;
            // 
            // colName
            // 
            this.colName.FieldName = null;
            this.colName.FilterFieldName = null;
            this.colName.Format = "";
            this.colName.FormatInfo = null;
            this.colName.HeaderText = "Название";
            this.colName.MappingName = "name";
            this.colName.ReadOnly = true;
            this.colName.Width = 150;
            // 
            // colType
            // 
            this.colType.FieldName = null;
            this.colType.FilterFieldName = null;
            this.colType.Format = "";
            this.colType.FormatInfo = null;
            this.colType.HeaderText = "Тип";
            this.colType.MappingName = "type";
            this.colType.Width = 75;
            // 
            // colUnit
            // 
            this.colUnit.FieldName = null;
            this.colUnit.FilterFieldName = null;
            this.colUnit.Format = "";
            this.colUnit.FormatInfo = null;
            this.colUnit.HeaderText = "Ед. изм.";
            this.colUnit.MappingName = "unit";
            this.colUnit.Width = 75;
            // 
            // colIsDomain
            // 
            this.colIsDomain.FieldName = null;
            this.colIsDomain.FilterFieldName = null;
            this.colIsDomain.HeaderText = "Список";
            this.colIsDomain.MappingName = "isDomain";
            this.colIsDomain.Width = 75;
            // 
            // colIsMandatory
            // 
            this.colIsMandatory.AllowNull = false;
            this.colIsMandatory.FieldName = null;
            this.colIsMandatory.FilterFieldName = null;
            this.colIsMandatory.HeaderText = "Обязательность";
            this.colIsMandatory.MappingName = "isMandatory";
            this.colIsMandatory.Width = 150;
            // 
            // colIsFullName
            // 
            this.colIsFullName.AllowNull = false;
            this.colIsFullName.FieldName = null;
            this.colIsFullName.FilterFieldName = null;
            this.colIsFullName.HeaderText = "Полное название";
            this.colIsFullName.MappingName = "isFullName";
            this.colIsFullName.Width = 75;
            // 
            // colIsFilter
            // 
            this.colIsFilter.AllowNull = false;
            this.colIsFilter.FieldName = null;
            this.colIsFilter.FilterFieldName = null;
            this.colIsFilter.HeaderText = "Фильтрация";
            this.colIsFilter.MappingName = "isFilter";
            this.colIsFilter.Width = 75;
            // 
            // dsParams
            // 
            this.dsParams.DataSetName = "NewDataSet";
            this.dsParams.Locale = new System.Globalization.CultureInfo("ru-RU");
            this.dsParams.OnlyRowsWithoutErrors = false;
            this.dsParams.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // btnFromTemplate
            // 
            this.btnFromTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFromTemplate.Location = new System.Drawing.Point(746, 233);
            this.btnFromTemplate.Name = "btnFromTemplate";
            this.btnFromTemplate.Size = new System.Drawing.Size(120, 45);
            this.btnFromTemplate.TabIndex = 12;
            this.btnFromTemplate.Text = "Создать на основе другой категории";
            this.btnFromTemplate.Click += new System.EventHandler(this.FromTemplate_Click);
            // 
            // btnDelParam
            // 
            this.btnDelParam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelParam.Location = new System.Drawing.Point(872, 320);
            this.btnDelParam.Name = "btnDelParam";
            this.btnDelParam.Size = new System.Drawing.Size(24, 26);
            this.btnDelParam.TabIndex = 15;
            this.btnDelParam.Text = "-";
            this.btnDelParam.Click += new System.EventHandler(this.btnDelParam_Click);
            // 
            // btnAddParam
            // 
            this.btnAddParam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddParam.Location = new System.Drawing.Point(872, 283);
            this.btnAddParam.Name = "btnAddParam";
            this.btnAddParam.Size = new System.Drawing.Size(24, 26);
            this.btnAddParam.TabIndex = 14;
            this.btnAddParam.Text = "+";
            this.btnAddParam.Click += new System.EventHandler(this.btnAddParam_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(914, 696);
            this.tabControl1.TabIndex = 39;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.AltNameText);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.H1Text);
            this.tabPage1.Controls.Add(this.GoodsListCheck);
            this.tabPage1.Controls.Add(this.GoogleSelect);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtPageDescription);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtKeywords);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtTitle);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.ShortLinkText);
            this.tabPage1.Controls.Add(this.FromGoods);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.ddlShopType);
            this.tabPage1.Controls.Add(this.dgParams);
            this.tabPage1.Controls.Add(this.btnFromTemplate);
            this.tabPage1.Controls.Add(this.btnDelParam);
            this.tabPage1.Controls.Add(this.btnAddParam);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.NameText);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(906, 670);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Общие";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(9, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 18);
            this.label9.TabIndex = 65;
            this.label9.Text = "H1:";
            // 
            // H1Text
            // 
            this.H1Text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.H1Text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H1Text.Location = new System.Drawing.Point(120, 158);
            this.H1Text.Name = "H1Text";
            this.H1Text.Size = new System.Drawing.Size(738, 20);
            this.H1Text.TabIndex = 6;
            // 
            // GoodsListCheck
            // 
            this.GoodsListCheck.AutoSize = true;
            this.GoodsListCheck.Location = new System.Drawing.Point(280, 249);
            this.GoodsListCheck.Name = "GoodsListCheck";
            this.GoodsListCheck.Size = new System.Drawing.Size(176, 17);
            this.GoodsListCheck.TabIndex = 10;
            this.GoodsListCheck.Text = "Показывать список товаров";
            this.GoodsListCheck.UseVisualStyleBackColor = true;
            // 
            // GoogleSelect
            // 
            this.GoogleSelect.BoundProp = null;
            this.GoogleSelect.ButtonBackColor = System.Drawing.SystemColors.Control;
            this.GoogleSelect.ClassName = "CGoogleTaxonomy";
            this.GoogleSelect.FieldNames = null;
            this.GoogleSelect.FormatRows = null;
            this.GoogleSelect.HeaderNames = "Название,ID";
            this.GoogleSelect.ListFormName = "Категории Google";
            this.GoogleSelect.Location = new System.Drawing.Point(120, 187);
            this.GoogleSelect.Name = "GoogleSelect";
            this.GoogleSelect.RowNames = "name,id";
            this.GoogleSelect.Size = new System.Drawing.Size(738, 24);
            this.GoogleSelect.TabIndex = 7;
            this.GoogleSelect.TextBoxBorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(9, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 18);
            this.label5.TabIndex = 61;
            this.label5.Text = "Категория google:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(9, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 18);
            this.label6.TabIndex = 51;
            this.label6.Text = "Description:";
            // 
            // txtPageDescription
            // 
            this.txtPageDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPageDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPageDescription.Location = new System.Drawing.Point(120, 132);
            this.txtPageDescription.Name = "txtPageDescription";
            this.txtPageDescription.Size = new System.Drawing.Size(738, 20);
            this.txtPageDescription.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(9, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 18);
            this.label7.TabIndex = 50;
            this.label7.Text = "Keywords:";
            // 
            // txtKeywords
            // 
            this.txtKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeywords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKeywords.Location = new System.Drawing.Point(120, 105);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(738, 20);
            this.txtKeywords.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(9, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 19);
            this.label8.TabIndex = 49;
            this.label8.Text = "Title:";
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTitle.Location = new System.Drawing.Point(120, 77);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(738, 20);
            this.txtTitle.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(9, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 21);
            this.label4.TabIndex = 43;
            this.label4.Text = "Красивая ссылка:";
            // 
            // ShortLinkText
            // 
            this.ShortLinkText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShortLinkText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ShortLinkText.Location = new System.Drawing.Point(120, 217);
            this.ShortLinkText.Name = "ShortLinkText";
            this.ShortLinkText.Size = new System.Drawing.Size(273, 20);
            this.ShortLinkText.TabIndex = 8;
            // 
            // FromGoods
            // 
            this.FromGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FromGoods.Location = new System.Drawing.Point(583, 233);
            this.FromGoods.Name = "FromGoods";
            this.FromGoods.Size = new System.Drawing.Size(157, 45);
            this.FromGoods.TabIndex = 11;
            this.FromGoods.Text = "Сформировать на основе характеристик товаров";
            this.FromGoods.UseVisualStyleBackColor = true;
            this.FromGoods.Click += new System.EventHandler(this.FromGoods_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(9, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 18);
            this.label1.TabIndex = 40;
            this.label1.Text = "Магазин:";
            // 
            // ddlShopType
            // 
            this.ddlShopType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlShopType.Location = new System.Drawing.Point(120, 247);
            this.ddlShopType.Name = "ddlShopType";
            this.ddlShopType.SelectedValue = -1;
            this.ddlShopType.Size = new System.Drawing.Size(121, 20);
            this.ddlShopType.TabIndex = 9;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.PriceIntervals);
            this.tabPage4.Controls.Add(this.panel2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(906, 670);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Диапазоны цен/Яндекс";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // PriceIntervals
            // 
            this.PriceIntervals.BackgroundColor = System.Drawing.Color.White;
            this.PriceIntervals.DataMember = "";
            this.PriceIntervals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PriceIntervals.FilterString = null;
            this.PriceIntervals.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.PriceIntervals.Location = new System.Drawing.Point(0, 72);
            this.PriceIntervals.Name = "PriceIntervals";
            this.PriceIntervals.Order = null;
            this.PriceIntervals.Size = new System.Drawing.Size(906, 598);
            this.PriceIntervals.TabIndex = 3;
            this.PriceIntervals.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle5});
            // 
            // extendedDataGridTableStyle5
            // 
            this.extendedDataGridTableStyle5.DataGrid = this.PriceIntervals;
            this.extendedDataGridTableStyle5.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.ShopTypeColumn,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5});
            this.extendedDataGridTableStyle5.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle5.MappingName = "PriceIntervals";
            // 
            // ShopTypeColumn
            // 
            this.ShopTypeColumn.FieldName = null;
            this.ShopTypeColumn.FilterFieldName = null;
            this.ShopTypeColumn.Format = "";
            this.ShopTypeColumn.FormatInfo = null;
            this.ShopTypeColumn.HeaderText = "Магазин";
            this.ShopTypeColumn.MappingName = "shopType";
            this.ShopTypeColumn.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "0.00";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "От";
            this.formattableTextBoxColumn4.MappingName = "priceBegin";
            this.formattableTextBoxColumn4.Width = 150;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "0.00";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "До";
            this.formattableTextBoxColumn5.MappingName = "priceEnd";
            this.formattableTextBoxColumn5.Width = 150;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.PriceLimit);
            this.panel2.Controls.Add(this.LoadToYandex);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(906, 72);
            this.panel2.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(242, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(182, 42);
            this.button2.TabIndex = 49;
            this.button2.Text = "Перегенерировать файл Яндекс";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Минимальная цена:";
            // 
            // PriceLimit
            // 
            this.PriceLimit.BoundProp = null;
            this.PriceLimit.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PriceLimit.IsCurrency = true;
            this.PriceLimit.Location = new System.Drawing.Point(131, 37);
            this.PriceLimit.Name = "PriceLimit";
            this.PriceLimit.Size = new System.Drawing.Size(100, 20);
            this.PriceLimit.TabIndex = 47;
            // 
            // LoadToYandex
            // 
            this.LoadToYandex.AutoSize = true;
            this.LoadToYandex.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LoadToYandex.Location = new System.Drawing.Point(16, 13);
            this.LoadToYandex.Name = "LoadToYandex";
            this.LoadToYandex.Size = new System.Drawing.Size(129, 17);
            this.LoadToYandex.TabIndex = 46;
            this.LoadToYandex.Text = "Экспорт на Яндекс:";
            this.LoadToYandex.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgAccessories);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(906, 670);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Аксессуары";
            // 
            // dgAccessories
            // 
            this.dgAccessories.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.dgAccessories.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.dgAccessories.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.dgAccessories.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.dgAccessories.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dgAccessories.ColumnHeadersVisibleOfDataGrid = true;
            this.dgAccessories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAccessories.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.dgAccessories.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.dgAccessories.Location = new System.Drawing.Point(0, 0);
            this.dgAccessories.Name = "dgAccessories";
            this.dgAccessories.Size = new System.Drawing.Size(906, 670);
            this.dgAccessories.TabIndex = 1;
            this.dgAccessories.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle3});
            this.dgAccessories.AddClick += new System.EventHandler(this.dgAccessories_AddClick);
            this.dgAccessories.DeleteClick += new System.EventHandler(this.dgAccessories_DeleteClick);
            // 
            // extendedDataGridTableStyle3
            // 
            this.extendedDataGridTableStyle3.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn2});
            this.extendedDataGridTableStyle3.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle3.MappingName = "accessories";
            this.extendedDataGridTableStyle3.ReadOnly = true;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Название";
            this.formattableTextBoxColumn2.MappingName = "propValue_NK";
            this.formattableTextBoxColumn2.Width = 200;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgPressReleases);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(906, 670);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Статьи и обзоры";
            // 
            // dgPressReleases
            // 
            this.dgPressReleases.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.dgPressReleases.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.dgPressReleases.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.dgPressReleases.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.dgPressReleases.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dgPressReleases.ColumnHeadersVisibleOfDataGrid = true;
            this.dgPressReleases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPressReleases.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.dgPressReleases.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.dgPressReleases.Location = new System.Drawing.Point(0, 0);
            this.dgPressReleases.Name = "dgPressReleases";
            this.dgPressReleases.Size = new System.Drawing.Size(906, 670);
            this.dgPressReleases.TabIndex = 2;
            this.dgPressReleases.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
            this.dgPressReleases.AddClick += new System.EventHandler(this.dgPressReleases_AddClick);
            this.dgPressReleases.DeleteClick += new System.EventHandler(this.dgPressReleases_DeleteClick);
            // 
            // extendedDataGridTableStyle2
            // 
            this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.colHeader});
            this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle2.MappingName = "pressReleases";
            this.extendedDataGridTableStyle2.ReadOnly = true;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "№";
            this.formattableTextBoxColumn1.MappingName = "ordValue";
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // colHeader
            // 
            this.colHeader.FieldName = null;
            this.colHeader.FilterFieldName = null;
            this.colHeader.Format = "";
            this.colHeader.FormatInfo = null;
            this.colHeader.HeaderText = "Название";
            this.colHeader.MappingName = "propValue_NK";
            this.colHeader.Width = 200;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.SalesNotesGrid);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(906, 670);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "SalesNotes";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // SalesNotesGrid
            // 
            this.SalesNotesGrid.BackgroundColor = System.Drawing.Color.White;
            this.SalesNotesGrid.DataMember = "";
            this.SalesNotesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SalesNotesGrid.FilterString = null;
            this.SalesNotesGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.SalesNotesGrid.Location = new System.Drawing.Point(0, 0);
            this.SalesNotesGrid.Name = "SalesNotesGrid";
            this.SalesNotesGrid.Order = null;
            this.SalesNotesGrid.Size = new System.Drawing.Size(906, 670);
            this.SalesNotesGrid.TabIndex = 0;
            this.SalesNotesGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle6});
            // 
            // extendedDataGridTableStyle6
            // 
            this.extendedDataGridTableStyle6.DataGrid = this.SalesNotesGrid;
            this.extendedDataGridTableStyle6.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn8,
            this.brandSalesNotesColumn,
            this.formattableTextBoxColumn9});
            this.extendedDataGridTableStyle6.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle6.MappingName = "Table";
            // 
            // formattableTextBoxColumn8
            // 
            this.formattableTextBoxColumn8.FieldName = null;
            this.formattableTextBoxColumn8.FilterFieldName = null;
            this.formattableTextBoxColumn8.Format = "";
            this.formattableTextBoxColumn8.FormatInfo = null;
            this.formattableTextBoxColumn8.HeaderText = "№";
            this.formattableTextBoxColumn8.MappingName = "ordValue";
            this.formattableTextBoxColumn8.ReadOnly = true;
            this.formattableTextBoxColumn8.Width = 75;
            // 
            // brandSalesNotesColumn
            // 
            this.brandSalesNotesColumn.FieldName = null;
            this.brandSalesNotesColumn.FilterFieldName = null;
            this.brandSalesNotesColumn.Format = "";
            this.brandSalesNotesColumn.FormatInfo = null;
            this.brandSalesNotesColumn.HeaderText = "Бренд";
            this.brandSalesNotesColumn.MappingName = "brandName";
            this.brandSalesNotesColumn.Width = 75;
            // 
            // formattableTextBoxColumn9
            // 
            this.formattableTextBoxColumn9.FieldName = null;
            this.formattableTextBoxColumn9.FilterFieldName = null;
            this.formattableTextBoxColumn9.Format = "";
            this.formattableTextBoxColumn9.FormatInfo = null;
            this.formattableTextBoxColumn9.HeaderText = "salesNotes";
            this.formattableTextBoxColumn9.MappingName = "propValue";
            this.formattableTextBoxColumn9.Width = 75;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.SeoTextGrid);
            this.tabPage5.Controls.Add(this.panel5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(906, 670);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "SEO тексты";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // SeoTextGrid
            // 
            this.SeoTextGrid.BackgroundColor = System.Drawing.Color.White;
            this.SeoTextGrid.DataMember = "";
            this.SeoTextGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SeoTextGrid.FilterString = null;
            this.SeoTextGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.SeoTextGrid.Location = new System.Drawing.Point(0, 0);
            this.SeoTextGrid.Name = "SeoTextGrid";
            this.SeoTextGrid.Order = null;
            this.SeoTextGrid.Size = new System.Drawing.Size(876, 670);
            this.SeoTextGrid.TabIndex = 9;
            this.SeoTextGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle4});
            // 
            // extendedDataGridTableStyle4
            // 
            this.extendedDataGridTableStyle4.DataGrid = this.SeoTextGrid;
            this.extendedDataGridTableStyle4.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7});
            this.extendedDataGridTableStyle4.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle4.MappingName = "Table";
            this.extendedDataGridTableStyle4.ReadOnly = true;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "N пп";
            this.formattableTextBoxColumn3.MappingName = "ordValue";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = null;
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "Бренд";
            this.formattableTextBoxColumn6.MappingName = "companyName";
            this.formattableTextBoxColumn6.Width = 75;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = null;
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "Статья";
            this.formattableTextBoxColumn7.MappingName = "propValue_NK";
            this.formattableTextBoxColumn7.Width = 75;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.AddButton);
            this.panel5.Controls.Add(this.DelButton);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(876, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(30, 670);
            this.panel5.TabIndex = 8;
            // 
            // AddButton
            // 
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddButton.Location = new System.Drawing.Point(3, 10);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(24, 26);
            this.AddButton.TabIndex = 1;
            this.AddButton.Text = "+";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DelButton
            // 
            this.DelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DelButton.Location = new System.Drawing.Point(3, 42);
            this.DelButton.Name = "DelButton";
            this.DelButton.Size = new System.Drawing.Size(24, 26);
            this.DelButton.TabIndex = 0;
            this.DelButton.Text = "-";
            this.DelButton.UseVisualStyleBackColor = true;
            this.DelButton.Click += new System.EventHandler(this.DelButton_Click);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.TagsDataGrid);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(906, 670);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Теги";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // TagsDataGrid
            // 
            this.TagsDataGrid.BackColorOfBtnAdd = System.Drawing.SystemColors.Control;
            this.TagsDataGrid.BackColorOfBtnDel = System.Drawing.SystemColors.Control;
            this.TagsDataGrid.BackgroundColorOfDataGrid = System.Drawing.Color.White;
            this.TagsDataGrid.BorderStyleOfDataGrid = System.Windows.Forms.BorderStyle.None;
            this.TagsDataGrid.BorderStyleOfPnlDataGrid = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TagsDataGrid.ColumnHeadersVisibleOfDataGrid = true;
            this.TagsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TagsDataGrid.FlatStyleOfBtnAdd = System.Windows.Forms.FlatStyle.Standard;
            this.TagsDataGrid.FlatStyleOfBtnDel = System.Windows.Forms.FlatStyle.Standard;
            this.TagsDataGrid.Location = new System.Drawing.Point(0, 0);
            this.TagsDataGrid.Name = "TagsDataGrid";
            this.TagsDataGrid.Size = new System.Drawing.Size(906, 670);
            this.TagsDataGrid.TabIndex = 0;
            this.TagsDataGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle7});
            this.TagsDataGrid.AddClick += new System.EventHandler(this.TagsDataGrid_AddClick);
            this.TagsDataGrid.DeleteClick += new System.EventHandler(this.TagsDataGrid_DeleteClick);
            this.TagsDataGrid.UpClick += new System.EventHandler(this.TagsDataGrid_UpClick);
            this.TagsDataGrid.DownClick += new System.EventHandler(this.TagsDataGrid_DownClick);
            // 
            // extendedDataGridTableStyle7
            // 
            this.extendedDataGridTableStyle7.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn10,
            this.formattableTextBoxColumn11});
            this.extendedDataGridTableStyle7.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle7.MappingName = "Tags";
            this.extendedDataGridTableStyle7.ReadOnly = true;
            // 
            // formattableTextBoxColumn10
            // 
            this.formattableTextBoxColumn10.FieldName = null;
            this.formattableTextBoxColumn10.FilterFieldName = null;
            this.formattableTextBoxColumn10.Format = "";
            this.formattableTextBoxColumn10.FormatInfo = null;
            this.formattableTextBoxColumn10.HeaderText = "№";
            this.formattableTextBoxColumn10.MappingName = "ordValue";
            this.formattableTextBoxColumn10.Width = 75;
            // 
            // formattableTextBoxColumn11
            // 
            this.formattableTextBoxColumn11.FieldName = null;
            this.formattableTextBoxColumn11.FilterFieldName = null;
            this.formattableTextBoxColumn11.Format = "";
            this.formattableTextBoxColumn11.FormatInfo = null;
            this.formattableTextBoxColumn11.HeaderText = "Название";
            this.formattableTextBoxColumn11.MappingName = "propValue_NK";
            this.formattableTextBoxColumn11.Width = 300;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.TitlesGrid);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(906, 670);
            this.tabPage9.TabIndex = 7;
            this.tabPage9.Text = "Titles";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // TitlesGrid
            // 
            this.TitlesGrid.BackgroundColor = System.Drawing.Color.White;
            this.TitlesGrid.DataMember = "";
            this.TitlesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TitlesGrid.FilterString = null;
            this.TitlesGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.TitlesGrid.Location = new System.Drawing.Point(0, 0);
            this.TitlesGrid.Name = "TitlesGrid";
            this.TitlesGrid.Order = null;
            this.TitlesGrid.Size = new System.Drawing.Size(906, 670);
            this.TitlesGrid.TabIndex = 0;
            this.TitlesGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle8});
            // 
            // extendedDataGridTableStyle8
            // 
            this.extendedDataGridTableStyle8.DataGrid = this.TitlesGrid;
            this.extendedDataGridTableStyle8.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.brandTitleColumn,
            this.formattableTextBoxColumn12,
            this.formattableTextBoxColumn14,
            this.formattableTextBoxColumn13});
            this.extendedDataGridTableStyle8.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle8.MappingName = "titles";
            // 
            // brandTitleColumn
            // 
            this.brandTitleColumn.FieldName = null;
            this.brandTitleColumn.FilterFieldName = null;
            this.brandTitleColumn.Format = "";
            this.brandTitleColumn.FormatInfo = null;
            this.brandTitleColumn.HeaderText = "Бренд";
            this.brandTitleColumn.MappingName = "brandName";
            this.brandTitleColumn.NullText = "Введите бренд";
            this.brandTitleColumn.Width = 200;
            // 
            // formattableTextBoxColumn12
            // 
            this.formattableTextBoxColumn12.FieldName = null;
            this.formattableTextBoxColumn12.FilterFieldName = null;
            this.formattableTextBoxColumn12.Format = "";
            this.formattableTextBoxColumn12.FormatInfo = null;
            this.formattableTextBoxColumn12.HeaderText = "Title";
            this.formattableTextBoxColumn12.MappingName = "title";
            this.formattableTextBoxColumn12.NullText = "";
            this.formattableTextBoxColumn12.Width = 200;
            // 
            // formattableTextBoxColumn14
            // 
            this.formattableTextBoxColumn14.FieldName = null;
            this.formattableTextBoxColumn14.FilterFieldName = null;
            this.formattableTextBoxColumn14.Format = "";
            this.formattableTextBoxColumn14.FormatInfo = null;
            this.formattableTextBoxColumn14.HeaderText = "Description";
            this.formattableTextBoxColumn14.MappingName = "pageDescription";
            this.formattableTextBoxColumn14.NullText = "";
            this.formattableTextBoxColumn14.Width = 200;
            // 
            // formattableTextBoxColumn13
            // 
            this.formattableTextBoxColumn13.FieldName = null;
            this.formattableTextBoxColumn13.FilterFieldName = null;
            this.formattableTextBoxColumn13.Format = "";
            this.formattableTextBoxColumn13.FormatInfo = null;
            this.formattableTextBoxColumn13.HeaderText = "Keywords";
            this.formattableTextBoxColumn13.MappingName = "keywords";
            this.formattableTextBoxColumn13.NullText = "";
            this.formattableTextBoxColumn13.Width = 75;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnImages);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 696);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(914, 37);
            this.panel1.TabIndex = 40;
            // 
            // btnImages
            // 
            this.btnImages.Location = new System.Drawing.Point(103, 9);
            this.btnImages.Name = "btnImages";
            this.btnImages.Size = new System.Drawing.Size(140, 28);
            this.btnImages.TabIndex = 17;
            this.btnImages.Text = "Картинки категории";
            this.btnImages.Click += new System.EventHandler(this.btnImages_Click);
            // 
            // cmAddAccessory
            // 
            this.cmAddAccessory.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miAddGoods,
            this.miAddTheme});
            // 
            // miAddGoods
            // 
            this.miAddGoods.Index = 0;
            this.miAddGoods.Text = "Добавить товар";
            this.miAddGoods.Click += new System.EventHandler(this.miAddGoods_Click);
            // 
            // miAddTheme
            // 
            this.miAddTheme.Index = 1;
            this.miAddTheme.Text = "Добавить категорию сайта";
            this.miAddTheme.Click += new System.EventHandler(this.miAddTheme_Click);
            // 
            // MultiProps
            // 
            this.MultiProps.DataSetName = "NewDataSet";
            this.MultiProps.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2,
            this.dataTable3,
            this.dataTable4,
            this.dataTable7,
            this.dataTable8});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5});
            this.dataTable2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "shopType",
                        "priceBegin"}, true)});
            this.dataTable2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn3,
        this.dataColumn4};
            this.dataTable2.TableName = "PriceIntervals";
            // 
            // dataColumn3
            // 
            this.dataColumn3.AllowDBNull = false;
            this.dataColumn3.ColumnName = "shopType";
            this.dataColumn3.DataType = typeof(int);
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "priceBegin";
            this.dataColumn4.DataType = typeof(decimal);
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "priceEnd";
            this.dataColumn5.DataType = typeof(decimal);
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn6,
            this.dataColumn7});
            this.dataTable3.TableName = "Accessories";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "propValue";
            this.dataColumn6.DataType = typeof(System.Guid);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "propValue_NK";
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10});
            this.dataTable4.TableName = "PressReleases";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "ordValue";
            this.dataColumn8.DataType = typeof(int);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "propValue";
            this.dataColumn9.DataType = typeof(System.Guid);
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "propValue_NK";
            // 
            // dataTable7
            // 
            this.dataTable7.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn18,
            this.dataColumn21,
            this.dataColumn22});
            this.dataTable7.TableName = "Tags";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "ordValue";
            this.dataColumn18.DataType = typeof(int);
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "propValue";
            this.dataColumn21.DataType = typeof(System.Guid);
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "propValue_NK";
            // 
            // dataTable8
            // 
            this.dataTable8.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25,
            this.dataColumn26,
            this.dataColumn27});
            this.dataTable8.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "manufacturerOID"}, false)});
            this.dataTable8.TableName = "Titles";
            // 
            // dataColumn23
            // 
            this.dataColumn23.AllowDBNull = false;
            this.dataColumn23.ColumnName = "manufacturerOID";
            this.dataColumn23.DataType = typeof(System.Guid);
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "brandName";
            // 
            // dataColumn25
            // 
            this.dataColumn25.AllowDBNull = false;
            this.dataColumn25.ColumnName = "title";
            // 
            // dataColumn26
            // 
            this.dataColumn26.AllowDBNull = false;
            this.dataColumn26.ColumnName = "keywords";
            // 
            // dataColumn27
            // 
            this.dataColumn27.AllowDBNull = false;
            this.dataColumn27.ColumnName = "pageDescription";
            // 
            // SeoTextsDataSet
            // 
            this.SeoTextsDataSet.DataSetName = "NewDataSet";
            this.SeoTextsDataSet.OnlyRowsWithoutErrors = false;
            this.SeoTextsDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable5});
            // 
            // dataTable5
            // 
            this.dataTable5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15});
            this.dataTable5.TableName = "Table";
            // 
            // dataColumn11
            // 
            this.dataColumn11.AllowDBNull = false;
            this.dataColumn11.AutoIncrement = true;
            this.dataColumn11.AutoIncrementSeed = ((long)(1));
            this.dataColumn11.ColumnName = "ordValue";
            this.dataColumn11.DataType = typeof(int);
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "propValue";
            this.dataColumn12.DataType = typeof(System.Guid);
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "propValue_NK";
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "manufacturerOID";
            this.dataColumn14.DataType = typeof(System.Guid);
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "companyName";
            // 
            // SalesNotesDataSet
            // 
            this.SalesNotesDataSet.DataSetName = "NewDataSet";
            this.SalesNotesDataSet.OnlyRowsWithoutErrors = false;
            this.SalesNotesDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable6});
            // 
            // dataTable6
            // 
            this.dataTable6.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn19,
            this.dataColumn20});
            this.dataTable6.TableName = "Table";
            // 
            // dataColumn16
            // 
            this.dataColumn16.AllowDBNull = false;
            this.dataColumn16.AutoIncrement = true;
            this.dataColumn16.AutoIncrementSeed = ((long)(1));
            this.dataColumn16.ColumnName = "ordValue";
            this.dataColumn16.DataType = typeof(int);
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "propValue";
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "manufacturerOID";
            this.dataColumn19.DataType = typeof(System.Guid);
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "brandName";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(9, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 19);
            this.label10.TabIndex = 67;
            this.label10.Text = "Другое название:";
            // 
            // AltNameText
            // 
            this.AltNameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AltNameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AltNameText.Location = new System.Drawing.Point(120, 47);
            this.AltNameText.Name = "AltNameText";
            this.AltNameText.Size = new System.Drawing.Size(738, 20);
            this.AltNameText.TabIndex = 2;
            // 
            // EditTheme
            // 
            this.ClientSize = new System.Drawing.Size(914, 733);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(464, 111);
            this.Name = "EditTheme";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Категория";
            this.Load += new System.EventHandler(this.fmEditTheme_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsParams)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlShopType)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PriceIntervals)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SalesNotesGrid)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SeoTextGrid)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TitlesGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MultiProps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeoTextsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalesNotesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private Button FromGoods;
		private Label label4;
		private TextBox ShortLinkText;
		private System.Data.DataTable dataTable2;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataColumn dataColumn5;
		private System.Data.DataTable dataTable3;
		private System.Data.DataTable dataTable4;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn ShopTypeColumn;
		private System.Data.DataSet MultiProps;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;
		private System.Data.DataColumn dataColumn8;
		private System.Data.DataColumn dataColumn9;
		private System.Data.DataColumn dataColumn10;
		private TabPage tabPage5;
		private Panel panel5;
		private Button AddButton;
		private Button DelButton;
		private ColumnMenuExtender.DataGridISM SeoTextGrid;
		private MetaData.DataSetISM SeoTextsDataSet;
		private System.Data.DataTable dataTable5;
		private System.Data.DataColumn dataColumn11;
		private System.Data.DataColumn dataColumn12;
		private System.Data.DataColumn dataColumn13;
		private System.Data.DataColumn dataColumn14;
		private System.Data.DataColumn dataColumn15;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private TabPage tabPage6;
		private ColumnMenuExtender.DataGridISM SalesNotesGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn brandSalesNotesColumn;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn9;
		private MetaData.DataSetISM SalesNotesDataSet;
		private System.Data.DataTable dataTable6;
		private System.Data.DataColumn dataColumn16;
		private System.Data.DataColumn dataColumn17;
		private System.Data.DataColumn dataColumn19;
		private System.Data.DataColumn dataColumn20;
        private Label label6;
        private TextBox txtPageDescription;
        private Label label7;
        private TextBox txtKeywords;
        private Label label8;
        private TextBox txtTitle;
        private TabPage tabPage7;
        private ColumnMenuExtender.UserControls.AddDelPositionDataGrid TagsDataGrid;
        private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle7;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn10;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn11;
        private System.Data.DataTable dataTable7;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private UserControls.SelectorTextBox GoogleSelect;
        private Label label5;
        private CheckBox GoodsListCheck;
        private Label label9;
        private TextBox H1Text;
        private ColumnMenuExtender.UserControls.AddDelDataGridExt addDelDataGridExt1;
        private System.Data.DataTable dataTable8;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private TabPage tabPage9;
        private ColumnMenuExtender.DataGridISM TitlesGrid;
        private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle8;
        private ColumnMenuExtender.ExtendedDataGridSelectorColumn brandTitleColumn;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn12;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn13;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn14;
        private Label label10;
        private TextBox AltNameText;
    }
}
