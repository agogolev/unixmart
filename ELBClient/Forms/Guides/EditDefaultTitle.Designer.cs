using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditDefaultTitle
    {
		#region Windows Form Designer generated code

		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox NameText;
		private System.Windows.Forms.Panel panel1;
		
		private System.ComponentModel.IContainer components = null;

		private void InitializeComponent()
		{
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.NameText = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DefaultTypeSelect = new ColumnMenuExtender.DataBoundComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PageDescriptionText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.KeywordsText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TitleText = new System.Windows.Forms.TextBox();
            this.IsActiveCheck = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Category = new ELBClient.UserControls.SelectorTextBoxExt();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultTypeSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(8, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(800, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 27);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 14;
            this.label3.Text = "Название:";
            // 
            // NameText
            // 
            this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameText.Location = new System.Drawing.Point(123, 9);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(749, 20);
            this.NameText.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 221);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(882, 46);
            this.panel1.TabIndex = 15;
            // 
            // DefaultTypeSelect
            // 
            this.DefaultTypeSelect.DefaultItemsCountInDropDown = 10;
            this.DefaultTypeSelect.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.DefaultTypeSelect.Location = new System.Drawing.Point(123, 37);
            this.DefaultTypeSelect.Name = "DefaultTypeSelect";
            this.DefaultTypeSelect.SelectedValue = -1;
            this.DefaultTypeSelect.Size = new System.Drawing.Size(205, 20);
            this.DefaultTypeSelect.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 18);
            this.label4.TabIndex = 30;
            this.label4.Text = "Для какого типа:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 51;
            this.label6.Text = "Description:";
            // 
            // PageDescriptionText
            // 
            this.PageDescriptionText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PageDescriptionText.Location = new System.Drawing.Point(123, 153);
            this.PageDescriptionText.Name = "PageDescriptionText";
            this.PageDescriptionText.Size = new System.Drawing.Size(749, 20);
            this.PageDescriptionText.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 50;
            this.label7.Text = "Keywords:";
            // 
            // KeywordsText
            // 
            this.KeywordsText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.KeywordsText.Location = new System.Drawing.Point(123, 125);
            this.KeywordsText.Name = "KeywordsText";
            this.KeywordsText.Size = new System.Drawing.Size(749, 20);
            this.KeywordsText.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(3, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 19);
            this.label8.TabIndex = 49;
            this.label8.Text = "Title:";
            // 
            // TitleText
            // 
            this.TitleText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TitleText.Location = new System.Drawing.Point(123, 97);
            this.TitleText.Name = "TitleText";
            this.TitleText.Size = new System.Drawing.Size(749, 20);
            this.TitleText.TabIndex = 4;
            // 
            // IsActiveCheck
            // 
            this.IsActiveCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IsActiveCheck.Location = new System.Drawing.Point(3, 181);
            this.IsActiveCheck.Name = "IsActiveCheck";
            this.IsActiveCheck.Size = new System.Drawing.Size(132, 18);
            this.IsActiveCheck.TabIndex = 7;
            this.IsActiveCheck.Text = "Активен:";
            this.IsActiveCheck.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(3, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 18);
            this.label11.TabIndex = 58;
            this.label11.Text = "Категория:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Category
            // 
            this.Category.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Category.BoundProp = null;
            this.Category.ButtonBackColor = System.Drawing.SystemColors.Control;
            this.Category.Location = new System.Drawing.Point(123, 65);
            this.Category.Name = "Category";
            this.Category.Size = new System.Drawing.Size(747, 24);
            this.Category.TabIndex = 3;
            this.Category.TextBoxBorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Category.ChooseClick += new System.EventHandler(this.Category_ChooseClick);
            this.Category.DelClick += new System.EventHandler(this.Category_DelClick);
            // 
            // EditDefaultTitle
            // 
            this.ClientSize = new System.Drawing.Size(882, 267);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Category);
            this.Controls.Add(this.IsActiveCheck);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.PageDescriptionText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.KeywordsText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TitleText);
            this.Controls.Add(this.DefaultTypeSelect);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.label3);
            this.MinimumSize = new System.Drawing.Size(464, 111);
            this.Name = "EditDefaultTitle";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Тайтлы по умолчанию";
            this.Load += new System.EventHandler(this.fmEditTheme_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DefaultTypeSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        private ColumnMenuExtender.DataBoundComboBox DefaultTypeSelect;
        private Label label4;
        private Label label6;
        private TextBox PageDescriptionText;
        private Label label7;
        private TextBox KeywordsText;
        private Label label8;
        private TextBox TitleText;
        private CheckBox IsActiveCheck;
        private Label label11;
        private UserControls.SelectorTextBoxExt Category;
    }
}
