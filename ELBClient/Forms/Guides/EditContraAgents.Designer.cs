﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditContraAgents
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.GroupBox gbMetro;
		private System.Windows.Forms.TabPage tabBankReq;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ContextMenu cmExpenses;
		private System.Windows.Forms.MenuItem miAddExp;
		private System.Windows.Forms.MenuItem miDelExp;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox ShortLinkText;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox NameText;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;

		private void InitializeComponent()
		{
            this.tabBankReq = new System.Windows.Forms.TabPage();
            this.gbMetro = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmExpenses = new System.Windows.Forms.ContextMenu();
            this.miAddExp = new System.Windows.Forms.MenuItem();
            this.miDelExp = new System.Windows.Forms.MenuItem();
            this.extendedDataGridComboBoxColumn1 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAttachImages = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.ShortLinkText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.NameText = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CompanyURLText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.AltNameText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPageDescription = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.gbMetro.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tabBankReq
            // 
            this.tabBankReq.Location = new System.Drawing.Point(4, 25);
            this.tabBankReq.Name = "tabBankReq";
            this.tabBankReq.Size = new System.Drawing.Size(608, 251);
            this.tabBankReq.TabIndex = 0;
            // 
            // gbMetro
            // 
            this.gbMetro.Controls.Add(this.label7);
            this.gbMetro.Controls.Add(this.label6);
            this.gbMetro.Controls.Add(this.label5);
            this.gbMetro.Controls.Add(this.textBox1);
            this.gbMetro.Controls.Add(this.label1);
            this.gbMetro.Location = new System.Drawing.Point(8, 7);
            this.gbMetro.Name = "gbMetro";
            this.gbMetro.Size = new System.Drawing.Size(200, 100);
            this.gbMetro.TabIndex = 5;
            this.gbMetro.TabStop = false;
            this.gbMetro.Text = "Название и контактная информация";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(16, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Полное название \\ имя:";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(16, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "Контактное лицо:";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(16, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Телефон:";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(160, 136);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(416, 20);
            this.textBox1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(16, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Краткое наименование:";
            // 
            // cmExpenses
            // 
            this.cmExpenses.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miAddExp,
            this.miDelExp});
            // 
            // miAddExp
            // 
            this.miAddExp.Index = 0;
            this.miAddExp.Text = "Добавить";
            // 
            // miDelExp
            // 
            this.miDelExp.Index = 1;
            this.miDelExp.Text = "Удалить";
            // 
            // extendedDataGridComboBoxColumn1
            // 
            this.extendedDataGridComboBoxColumn1.FieldName = null;
            this.extendedDataGridComboBoxColumn1.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn1.Format = "";
            this.extendedDataGridComboBoxColumn1.FormatInfo = null;
            this.extendedDataGridComboBoxColumn1.HeaderText = "Тип";
            this.extendedDataGridComboBoxColumn1.MappingName = "type";
            this.extendedDataGridComboBoxColumn1.NullText = "";
            this.extendedDataGridComboBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Величина";
            this.formattableTextBoxColumn2.MappingName = "value";
            this.formattableTextBoxColumn2.NullText = "";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Комментарий";
            this.formattableTextBoxColumn3.MappingName = "comment";
            this.formattableTextBoxColumn3.NullText = "";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.MappingName = "ordValue";
            this.formattableTextBoxColumn4.NullText = "";
            this.formattableTextBoxColumn4.ReadOnly = true;
            this.formattableTextBoxColumn4.Width = 0;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Адрес";
            this.formattableTextBoxColumn1.MappingName = "propValue";
            this.formattableTextBoxColumn1.NullText = "";
            this.formattableTextBoxColumn1.Width = 250;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(8, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(449, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 27);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAttachImages);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 275);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 37);
            this.panel1.TabIndex = 13;
            // 
            // btnAttachImages
            // 
            this.btnAttachImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAttachImages.BackColor = System.Drawing.SystemColors.Control;
            this.btnAttachImages.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAttachImages.Location = new System.Drawing.Point(156, 4);
            this.btnAttachImages.Name = "btnAttachImages";
            this.btnAttachImages.Size = new System.Drawing.Size(120, 28);
            this.btnAttachImages.TabIndex = 14;
            this.btnAttachImages.Text = "Привязка картинок";
            this.btnAttachImages.UseVisualStyleBackColor = false;
            this.btnAttachImages.Click += new System.EventHandler(this.btnAttachImages_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(6, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 23);
            this.label4.TabIndex = 23;
            this.label4.Text = "Красивая ссылка:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ShortLinkText
            // 
            this.ShortLinkText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShortLinkText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ShortLinkText.Location = new System.Drawing.Point(125, 46);
            this.ShortLinkText.Name = "ShortLinkText";
            this.ShortLinkText.Size = new System.Drawing.Size(392, 20);
            this.ShortLinkText.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(6, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Название:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NameText
            // 
            this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NameText.Location = new System.Drawing.Point(125, 9);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(392, 20);
            this.NameText.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtPageDescription);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtKeywords);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtTitle);
            this.panel2.Controls.Add(this.CompanyURLText);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.AltNameText);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.ShortLinkText);
            this.panel2.Controls.Add(this.NameText);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(529, 275);
            this.panel2.TabIndex = 24;
            // 
            // CompanyURLText
            // 
            this.CompanyURLText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CompanyURLText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CompanyURLText.Location = new System.Drawing.Point(125, 119);
            this.CompanyURLText.Name = "CompanyURLText";
            this.CompanyURLText.Size = new System.Drawing.Size(392, 20);
            this.CompanyURLText.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(6, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 23);
            this.label8.TabIndex = 27;
            this.label8.Text = "Ссылка на сайт";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AltNameText
            // 
            this.AltNameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AltNameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AltNameText.Location = new System.Drawing.Point(125, 82);
            this.AltNameText.Name = "AltNameText";
            this.AltNameText.Size = new System.Drawing.Size(392, 20);
            this.AltNameText.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(6, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 23);
            this.label2.TabIndex = 25;
            this.label2.Text = "Другое имя:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(5, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 18);
            this.label9.TabIndex = 51;
            this.label9.Text = "Description:";
            // 
            // txtPageDescription
            // 
            this.txtPageDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPageDescription.Location = new System.Drawing.Point(125, 218);
            this.txtPageDescription.Name = "txtPageDescription";
            this.txtPageDescription.Size = new System.Drawing.Size(392, 20);
            this.txtPageDescription.TabIndex = 48;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(5, 187);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 18);
            this.label10.TabIndex = 50;
            this.label10.Text = "Keywords:";
            // 
            // txtKeywords
            // 
            this.txtKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeywords.Location = new System.Drawing.Point(125, 185);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(392, 20);
            this.txtKeywords.TabIndex = 47;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 154);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 19);
            this.label11.TabIndex = 49;
            this.label11.Text = "Title:";
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.Location = new System.Drawing.Point(125, 152);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(392, 20);
            this.txtTitle.TabIndex = 46;
            // 
            // EditContraAgents
            // 
            this.ClientSize = new System.Drawing.Size(529, 312);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(537, 342);
            this.Name = "EditContraAgents";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Компания";
            this.Load += new System.EventHandler(this.fmEditContraAgents_Load);
            this.gbMetro.ResumeLayout(false);
            this.gbMetro.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        private Button btnAttachImages;
        private TextBox AltNameText;
        private Label label2;
        private TextBox CompanyURLText;
        private Label label8;
        private Label label9;
        private TextBox txtPageDescription;
        private Label label10;
        private TextBox txtKeywords;
        private Label label11;
        private TextBox txtTitle;
    }
}
