﻿using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	public partial class EditDisableExpressDelivery : EditForm
	{
		
		public EditDisableExpressDelivery()
		{
			InitializeComponent();
			Object = new CDisableExpressDelivery();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch{}
			//this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void BindFields()
		{
			NameText.DataBindings.Add("Text", Object, "Name");
			DateOccurValue.DataBindings.Add("BoundValue", Object, "DateOccur");
		}

		private void fmEditDisableExpressDelivery_Load(object sender, System.EventArgs e)
		{
			LoadObject("name,dateOccur", null);
			BindFields();
		}
	}
}
