﻿namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListGroup.
	/// </summary>
	public partial class ListAction : ListForm
	{
		

		/// <summary>
		/// Required designer variable.
		/// </summary>


		public ListAction()
		{
			InitializeComponent();
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmListGroup_Load(object sender, System.EventArgs e)
		{
			LoadLookup();
			mainDataGrid1.LoadData();
		}

		private void LoadLookup()
		{
			var xml = lp.GetLookupValues("CAction");
			extendedDataGridComboBoxColumn1.ComboBox.LoadXml(xml, "actionTypeN");
		}
	}
}
