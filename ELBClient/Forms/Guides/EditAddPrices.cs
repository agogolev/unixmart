﻿using System;
using System.Data;
using System.Linq;
using MetaData;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	public partial class EditAddPrices : ListForm
	{
		private DataSetISM Source;
		int shopType1 = 0;
		public EditAddPrices()
		{
			InitializeComponent();
			formattableTextBoxColumn4.CommitCell += new ValidateCellEventHandler(CheckNull_CommitCell);
			formattableTextBoxColumn5.CommitCell += new ValidateCellEventHandler(CheckNull_CommitCell);
		}

		void CheckNull_CommitCell(object sender, DataGridValidateCellEventArgs e)
		{
			DataRowView drv = (DataRowView)e.CurrencyManager.Current;
			if (e.Value == Convert.DBNull)
			{
				e.IsCommit = false;
				ShowError("Данное поле не может быть пустым");
			}
			else e.IsCommit = true;
		}

		private void EditAddPrices_Load(object sender, EventArgs e)
		{
			string sql = @"
SELECT
	*
FROM
	t_TypeShop
";
			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			ShopType1.Items.AddRange(ds.Table.AsEnumerable().Select(row => new ListItem { Value = row.Field<int>("shopType"), Text = row.Field<string>("name") }).ToArray());
		}

		public override void LoadData()
		{
			shopType1 = ((ListItem)ShopType1.SelectedItem).Value;
			string sql = @"
SELECT
	n.lft
	, OID = m.OID
	, fullName = dbo.GetFullTheme(m.OID, 22464)    
	, deliveryFirst1 = mp1.deliveryFirst
	, deliverySecond1 = mp1.deliverySecond
FROM
	t_Nodes n
	inner join t_MasterCatalog m on n.object = m.OID and n.tree = '20e1ac79-5a3c-4b49-94e0-9e640842fe81'
	left join t_MasterPrices mp1 on m.OID = mp1.OID and mp1.shopType = {0} 
WHERE
	{1}
ORDER BY
	n.lft
";
			string path = "1=1";
			if (!string.IsNullOrWhiteSpace(Path.Text))
			{
				path = string.Format("dbo.GetFullTheme(m.OID, 22464) like {0}", ToSqlString("%" + Path.Text.Trim() + "%"));
			}
            sql = string.Format(sql, shopType1, path);
            Source = new DataSetISM(lp.GetDataSetSql(sql));
			AddPrices.DataSource = new DataView(Source.Table, null, "lft", DataViewRowState.CurrentRows);
			AddPrices.DisableNewDel();
			Source.Table.RowChanged += new DataRowChangeEventHandler(Table_RowChanged);
		}

		void Table_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			DataRow dr = e.Row;
			Guid OID = (Guid)dr["OID"];

			string sql = @"
IF EXISTS (SELECT * FROM t_MasterPrices WHERE OID = {0} and shopType = {1}) Begin
	UPDATE t_MasterPrices SET deliveryFirst = {2}, deliverySecond = {3}
	WHERE
		OID = {0} and shopType = {1}
end
else Begin
	INSERT INTO t_MasterPrices(OID, shopType, deliveryFirst, deliverySecond)
	VALUES({0}, {1}, {2}, {3})
end
";
			op.ExecuteCommand(string.Format(sql, ToSqlString(OID), ToSqlString(shopType1),
				ToSqlString(dr["deliveryFirst1"]), ToSqlString(dr["deliverySecond1"])));

		}

		private void ShowButton_Click(object sender, EventArgs e)
		{
			LoadData();
		}
    }
}
