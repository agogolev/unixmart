using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditComment
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private ColumnMenuExtender.DataBoundTextBox txtPeople;
		private System.Windows.Forms.TextBox txtHeader;
		private ColumnMenuExtender.DataBoundTextBox txtReit;
		private System.Windows.Forms.TextBox txtSelfComment;
		private System.Windows.Forms.Label label6;
		private ColumnMenuExtender.DataBoundTextBox txtObject;
		private IContainer components = null;

		private void InitializeComponent()
		{
			this.txtName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtPeople = new ColumnMenuExtender.DataBoundTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtHeader = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtReit = new ColumnMenuExtender.DataBoundTextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtSelfComment = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.txtObject = new ColumnMenuExtender.DataBoundTextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// txtName
			// 
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtName.Location = new System.Drawing.Point(112, 9);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(424, 20);
			this.txtName.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 27);
			this.label1.TabIndex = 1;
			this.label1.Text = "NickName:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 45);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 27);
			this.label2.TabIndex = 3;
			this.label2.Text = "Пользователь:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtPeople
			// 
			this.txtPeople.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtPeople.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPeople.BoundProp = null;
			this.txtPeople.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtPeople.IsCurrency = false;
			this.txtPeople.Location = new System.Drawing.Point(112, 46);
			this.txtPeople.Name = "txtPeople";
			this.txtPeople.ReadOnly = true;
			this.txtPeople.Size = new System.Drawing.Size(424, 20);
			this.txtPeople.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 119);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 26);
			this.label3.TabIndex = 5;
			this.label3.Text = "Заголовок";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtHeader
			// 
			this.txtHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtHeader.Location = new System.Drawing.Point(112, 120);
			this.txtHeader.Name = "txtHeader";
			this.txtHeader.Size = new System.Drawing.Size(424, 20);
			this.txtHeader.TabIndex = 3;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 156);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 26);
			this.label4.TabIndex = 7;
			this.label4.Text = "Оценка:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtReit
			// 
			this.txtReit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtReit.BoundProp = null;
			this.txtReit.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtReit.IsCurrency = false;
			this.txtReit.Location = new System.Drawing.Point(112, 157);
			this.txtReit.Name = "txtReit";
			this.txtReit.Size = new System.Drawing.Size(72, 20);
			this.txtReit.TabIndex = 4;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 194);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 26);
			this.label5.TabIndex = 9;
			this.label5.Text = "Сообщение:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSelfComment
			// 
			this.txtSelfComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtSelfComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSelfComment.Location = new System.Drawing.Point(112, 194);
			this.txtSelfComment.Multiline = true;
			this.txtSelfComment.Name = "txtSelfComment";
			this.txtSelfComment.Size = new System.Drawing.Size(424, 63);
			this.txtSelfComment.TabIndex = 5;
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.button1.Location = new System.Drawing.Point(8, 266);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 27);
			this.button1.TabIndex = 6;
			this.button1.Text = "Закрыть";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(464, 266);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 27);
			this.button2.TabIndex = 7;
			this.button2.Text = "Сохранить";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 82);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 26);
			this.label6.TabIndex = 11;
			this.label6.Text = "Товар:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtObject
			// 
			this.txtObject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtObject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtObject.BoundProp = null;
			this.txtObject.HintFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtObject.IsCurrency = false;
			this.txtObject.Location = new System.Drawing.Point(112, 83);
			this.txtObject.Name = "txtObject";
			this.txtObject.ReadOnly = true;
			this.txtObject.Size = new System.Drawing.Size(424, 20);
			this.txtObject.TabIndex = 2;
			// 
			// EditComment
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(544, 300);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.txtObject);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtSelfComment);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtReit);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtHeader);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtPeople);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtName);
			this.Name = "EditComment";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "6";
			this.Load += new System.EventHandler(this.fmEditComment_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}


	}
}
