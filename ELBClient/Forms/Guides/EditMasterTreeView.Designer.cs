using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditMasterTreeView
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TreeView tv1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem miAdd;
		private System.Windows.Forms.MenuItem miEdit;
		private System.Windows.Forms.MenuItem miDel;
		private System.Windows.Forms.MenuItem miAddSibling;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem miUp;
		private System.Windows.Forms.MenuItem miDown;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.MenuItem miEditObject;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnToExcel;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Data.DataSet dataSet1;
		private System.Data.DataSet dataSet2;
		private System.ComponentModel.IContainer components;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditMasterTreeView));
			this.dataSet1 = new System.Data.DataSet();
			this.tv1 = new System.Windows.Forms.TreeView();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miAdd = new System.Windows.Forms.MenuItem();
			this.miAddSibling = new System.Windows.Forms.MenuItem();
			this.miEdit = new System.Windows.Forms.MenuItem();
			this.miEditObject = new System.Windows.Forms.MenuItem();
			this.miDel = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.miUp = new System.Windows.Forms.MenuItem();
			this.miDown = new System.Windows.Forms.MenuItem();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnToExcel = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel3.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// tv1
			// 
			this.tv1.AllowDrop = true;
			this.tv1.ContextMenu = this.contextMenu1;
			this.tv1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tv1.HideSelection = false;
			this.tv1.Location = new System.Drawing.Point(0, 0);
			this.tv1.Name = "tv1";
			this.tv1.Size = new System.Drawing.Size(608, 438);
			this.tv1.TabIndex = 2;
			this.tv1.DoubleClick += new System.EventHandler(this.tv1_DoubleClick);
			this.tv1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tv1_MouseDown);
			this.tv1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tv1_MouseMove);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miAdd,
            this.miAddSibling,
            this.miEdit,
            this.miEditObject,
            this.miDel,
            this.menuItem1,
            this.miUp,
            this.miDown});
			// 
			// miAdd
			// 
			this.miAdd.Index = 0;
			this.miAdd.Text = "Добавить дочерний";
			this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
			// 
			// miAddSibling
			// 
			this.miAddSibling.Index = 1;
			this.miAddSibling.Text = "Добавить";
			this.miAddSibling.Click += new System.EventHandler(this.miAddSibling_Click);
			// 
			// miEdit
			// 
			this.miEdit.Index = 2;
			this.miEdit.Text = "Редактировать";
			this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
			// 
			// miEditObject
			// 
			this.miEditObject.Index = 3;
			this.miEditObject.Text = "Объект";
			this.miEditObject.Click += new System.EventHandler(this.miEditObject_Click);
			// 
			// miDel
			// 
			this.miDel.Index = 4;
			this.miDel.Text = "Удалить";
			this.miDel.Click += new System.EventHandler(this.miDel_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 5;
			this.menuItem1.Text = "-";
			// 
			// miUp
			// 
			this.miUp.Index = 6;
			this.miUp.Text = "Вверх";
			this.miUp.Click += new System.EventHandler(this.miUp_Click);
			// 
			// miDown
			// 
			this.miDown.Index = 7;
			this.miDown.Text = "Вниз";
			this.miDown.Click += new System.EventHandler(this.miDown_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
			this.imageList1.Images.SetKeyName(0, "");
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.tv1);
			this.panel3.Controls.Add(this.panel1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(608, 485);
			this.panel3.TabIndex = 30;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnToExcel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 438);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(608, 47);
			this.panel1.TabIndex = 3;
			// 
			// btnToExcel
			// 
			this.btnToExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnToExcel.Location = new System.Drawing.Point(432, 9);
			this.btnToExcel.Name = "btnToExcel";
			this.btnToExcel.Size = new System.Drawing.Size(168, 27);
			this.btnToExcel.TabIndex = 0;
			this.btnToExcel.Text = "Выгрузка описаний в Excel";
			this.btnToExcel.Visible = false;
			// 
			// EditMasterTreeView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(608, 485);
			this.Controls.Add(this.panel3);
			this.MinimumSize = new System.Drawing.Size(250, 231);
			this.Name = "EditMasterTreeView";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Load += new System.EventHandler(this.fmEditTree_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
