﻿using System;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListGoodsWithParam.
	/// </summary>
	public partial class ListGoodsWithParam : ListForm
	{
		private DataSetISM dsGoods;
		private string checkValue;
		public string CheckValue {
			get {
				return this.checkValue;
			}
			set {
				this.checkValue = value; 
			}
		}
		private Guid paramType;
		public Guid ParamType {
			get {
				return this.paramType;
			}
			set {
				this.paramType = value; 
			}
		}
		
		
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ListGoodsWithParam()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmListGoodsWithParam_Load(object sender, System.EventArgs e) {
			LoadData();
		}
		public override void LoadData() {
			ListProvider.ListProvider prov = Classes.ServiceUtility.ListProvider;
			string sql = @"
SELECT
	g.OID,
	g.shortName,
	category = dbo.NK(g.category),
	manufacturer = dbo.NK(g.manufacturer)
FROM
	t_Goods g
	inner join t_GoodsParams gp on g.OID = gp.OID And gp.ParamTypeOID = '"+ParamType+"' and value = "+ToSqlString(checkValue)+@"
ORDER BY
	shortName
";
			dsGoods = new DataSetISM(prov.GetDataSetSql(sql));
			dgList.SetDataBinding(dsGoods, "table");
			dgList.DisableNewDel();
		}

		private void dgList_DoubleClick(object sender, System.EventArgs e) {
			DataRow dr = dgList.GetSelectedRow();
			EditGoods fm = new EditGoods();
			fm.ObjectOID = (Guid)dr["OID"];
			fm.MdiParent = this.MdiParent;
			fm.Show();
		}

		private void button1_Click(object sender, System.EventArgs e) {
			Close();
		}
	}
}
