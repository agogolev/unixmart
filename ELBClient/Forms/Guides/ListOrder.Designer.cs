using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	public partial class ListOrder
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox cbShowAll;
		private System.Windows.Forms.Button btnRefresh;
		private DataBoundComboBox cbShopType;
		private System.Windows.Forms.Label lbShopType;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private ColumnMenuExtender.DataGridISM dgOrders;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colOrderNum;
		private ColumnMenuExtender.FormattableTextBoxColumn colPerson;
		private ColumnMenuExtender.FormattableTextBoxColumn colPhone;
		private ColumnMenuExtender.FormattableTextBoxColumn colOperator_NK;
		private ColumnMenuExtender.FormattableTextBoxColumn colDateCreate;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colStatus;
		private ColumnMenuExtender.FormattableTextBoxColumn colDeliveryDate;
		private ColumnMenuExtender.FormattableTextBoxColumn colComment;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colDeliveryPeriod;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colTypePay;
		private ColumnMenuExtender.FormattableBooleanColumn colIsSendToSv;
		private ColumnMenuExtender.FormattableTextBoxColumn colWikiM;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colSourceType;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colShopType;
		private ColumnMenuExtender.FormattableTextBoxColumn colCity;
		private ColumnMenuExtender.FormattableTextBoxColumn colOrderItemCount;
		private ColumnMenuExtender.FormattableTextBoxColumn colOrderTotalPrice;
		private ContextMenuStrip SelectDomainMenuStrip;
		private ToolStripMenuItem wwwelectroburgruToolStripMenuItem;
		private ToolStripMenuItem spbelectroburgruToolStripMenuItem;
		private ToolStripMenuItem nskelectroburgruToolStripMenuItem;
		private IContainer components;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.colOrderNum = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colPerson = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colPhone = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colOperator_NK = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colStatus = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colDateCreate = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.dgOrders = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colComment = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colDeliveryDate = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colDeliveryPeriod = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colTypePay = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colIsSendToSv = new ColumnMenuExtender.FormattableBooleanColumn();
			this.colWikiM = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colSourceType = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colShopType = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colCity = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colOrderItemCount = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colOrderTotalPrice = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colDeliveryPrice = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colEMail = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colDocType = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colSVID = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.lbShopType = new System.Windows.Forms.Label();
			this.cbShopType = new ColumnMenuExtender.DataBoundComboBox();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.cbShowAll = new System.Windows.Forms.CheckBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.SelectDomainMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.wwwelectroburgruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.spbelectroburgruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.nskelectroburgruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.kdrelectroburgruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.SaveListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.SaveList = new System.Windows.Forms.SaveFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.dgOrders)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SelectDomainMenuStrip.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// colOrderNum
			// 
			this.colOrderNum.FieldName = null;
			this.colOrderNum.FilterFieldName = null;
			this.colOrderNum.Format = "";
			this.colOrderNum.FormatInfo = null;
			this.colOrderNum.HeaderText = "№";
			this.colOrderNum.MappingName = "orderNum";
			this.columnMenuExtender1.SetMenu(this.colOrderNum, this.menuFilterSort1);
			this.colOrderNum.ReadOnly = true;
			this.colOrderNum.Width = 75;
			// 
			// colPerson
			// 
			this.colPerson.FieldName = null;
			this.colPerson.FilterFieldName = null;
			this.colPerson.Format = "";
			this.colPerson.FormatInfo = null;
			this.colPerson.HeaderText = "Клиент";
			this.colPerson.MappingName = "person";
			this.columnMenuExtender1.SetMenu(this.colPerson, this.menuFilterSort1);
			this.colPerson.ReadOnly = true;
			this.colPerson.Width = 75;
			// 
			// colPhone
			// 
			this.colPhone.FieldName = null;
			this.colPhone.FilterFieldName = null;
			this.colPhone.Format = "";
			this.colPhone.FormatInfo = null;
			this.colPhone.HeaderText = "Телефон";
			this.colPhone.MappingName = "phone";
			this.columnMenuExtender1.SetMenu(this.colPhone, this.menuFilterSort1);
			this.colPhone.ReadOnly = true;
			this.colPhone.Width = 75;
			// 
			// colOperator_NK
			// 
			this.colOperator_NK.FieldName = "NK(operator)";
			this.colOperator_NK.FilterFieldName = null;
			this.colOperator_NK.Format = "";
			this.colOperator_NK.FormatInfo = null;
			this.colOperator_NK.HeaderText = "Оператор";
			this.colOperator_NK.MappingName = "operator_NK";
			this.columnMenuExtender1.SetMenu(this.colOperator_NK, this.menuFilterSort1);
			this.colOperator_NK.ReadOnly = true;
			this.colOperator_NK.Width = 200;
			// 
			// colStatus
			// 
			this.colStatus.FieldName = null;
			this.colStatus.FilterFieldName = null;
			this.colStatus.Format = "";
			this.colStatus.FormatInfo = null;
			this.colStatus.HeaderText = "Статус";
			this.colStatus.MappingName = "status";
			this.columnMenuExtender1.SetMenu(this.colStatus, this.menuFilterSort1);
			this.colStatus.ReadOnly = true;
			this.colStatus.Width = 75;
			// 
			// colDateCreate
			// 
			this.colDateCreate.FieldName = null;
			this.colDateCreate.FilterFieldName = null;
			this.colDateCreate.Format = "";
			this.colDateCreate.FormatInfo = null;
			this.colDateCreate.HeaderText = "Дата создания";
			this.colDateCreate.MappingName = "dateCreate";
			this.columnMenuExtender1.SetMenu(this.colDateCreate, this.menuFilterSort1);
			this.colDateCreate.ReadOnly = true;
			this.colDateCreate.Width = 200;
			// 
			// dgOrders
			// 
			this.dgOrders.BackgroundColor = System.Drawing.Color.White;
			this.dgOrders.ColumnDragEnabled = true;
			this.dgOrders.DataMember = "";
			this.dgOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgOrders.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgOrders.Location = new System.Drawing.Point(0, 46);
			this.dgOrders.Name = "dgOrders";
			this.dgOrders.Order = null;
			this.dgOrders.Size = new System.Drawing.Size(776, 202);
			this.dgOrders.StockClass = "COrder";
			this.dgOrders.StockNumInBatch = 0;
			this.dgOrders.TabIndex = 2;
			this.dgOrders.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dgOrders, true);
			this.dgOrders.Reload += new System.EventHandler(this.dgOrders_Reload);
			this.dgOrders.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dgOrders_ActionKeyPressed);
			this.dgOrders.DoubleClick += new System.EventHandler(this.dgOrders_DoubleClick);
			this.dgOrders.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgOrders_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgOrders;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colOrderNum,
            this.colPerson,
            this.colPhone,
            this.colOperator_NK,
            this.colStatus,
            this.colDateCreate,
            this.colComment,
            this.colDeliveryDate,
            this.colDeliveryPeriod,
            this.colTypePay,
            this.colIsSendToSv,
            this.colWikiM,
            this.colSourceType,
            this.colShopType,
            this.colCity,
            this.colOrderItemCount,
            this.colOrderTotalPrice,
            this.colDeliveryPrice,
            this.colEMail,
            this.colDocType,
            this.colSVID});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			// 
			// colComment
			// 
			this.colComment.FieldName = null;
			this.colComment.FilterFieldName = null;
			this.colComment.Format = "";
			this.colComment.FormatInfo = null;
			this.colComment.HeaderText = "Комментарий";
			this.colComment.MappingName = "comment";
			this.columnMenuExtender1.SetMenu(this.colComment, this.menuFilterSort1);
			this.colComment.ReadOnly = true;
			this.colComment.Width = 75;
			// 
			// colDeliveryDate
			// 
			this.colDeliveryDate.FieldName = null;
			this.colDeliveryDate.FilterFieldName = null;
			this.colDeliveryDate.Format = "dd.MM.yyyy";
			this.colDeliveryDate.FormatInfo = null;
			this.colDeliveryDate.HeaderText = "Дата доставки";
			this.colDeliveryDate.MappingName = "deliveryDate";
			this.columnMenuExtender1.SetMenu(this.colDeliveryDate, this.menuFilterSort1);
			this.colDeliveryDate.ReadOnly = true;
			this.colDeliveryDate.Width = 75;
			// 
			// colDeliveryPeriod
			// 
			this.colDeliveryPeriod.FieldName = null;
			this.colDeliveryPeriod.FilterFieldName = null;
			this.colDeliveryPeriod.Format = "";
			this.colDeliveryPeriod.FormatInfo = null;
			this.colDeliveryPeriod.HeaderText = "Период доставки";
			this.colDeliveryPeriod.MappingName = "deliveryPeriod";
			this.columnMenuExtender1.SetMenu(this.colDeliveryPeriod, this.menuFilterSort1);
			this.colDeliveryPeriod.ReadOnly = true;
			this.colDeliveryPeriod.Width = 75;
			// 
			// colTypePay
			// 
			this.colTypePay.FieldName = null;
			this.colTypePay.FilterFieldName = null;
			this.colTypePay.Format = "";
			this.colTypePay.FormatInfo = null;
			this.colTypePay.HeaderText = "Форма оплаты";
			this.colTypePay.MappingName = "typePay";
			this.columnMenuExtender1.SetMenu(this.colTypePay, this.menuFilterSort1);
			this.colTypePay.ReadOnly = true;
			this.colTypePay.Width = 75;
			// 
			// colIsSendToSv
			// 
			this.colIsSendToSv.FieldName = null;
			this.colIsSendToSv.FilterFieldName = null;
			this.colIsSendToSv.HeaderText = "Послано в SV";
			this.colIsSendToSv.MappingName = "isSendToSV";
			this.columnMenuExtender1.SetMenu(this.colIsSendToSv, this.menuFilterSort1);
			this.colIsSendToSv.ReadOnly = true;
			this.colIsSendToSv.Width = 75;
			// 
			// colWikiM
			// 
			this.colWikiM.FieldName = null;
			this.colWikiM.FilterFieldName = null;
			this.colWikiM.Format = "";
			this.colWikiM.FormatInfo = null;
			this.colWikiM.HeaderText = "Код WikiM";
			this.colWikiM.MappingName = "wikiMID";
			this.columnMenuExtender1.SetMenu(this.colWikiM, this.menuFilterSort1);
			this.colWikiM.ReadOnly = true;
			this.colWikiM.Width = 75;
			// 
			// colSourceType
			// 
			this.colSourceType.FieldName = null;
			this.colSourceType.FilterFieldName = null;
			this.colSourceType.Format = "";
			this.colSourceType.FormatInfo = null;
			this.colSourceType.HeaderText = "Источник заказа";
			this.colSourceType.MappingName = "sourceType";
			this.columnMenuExtender1.SetMenu(this.colSourceType, this.menuFilterSort1);
			this.colSourceType.ReadOnly = true;
			this.colSourceType.Width = 75;
			// 
			// colShopType
			// 
			this.colShopType.FieldName = null;
			this.colShopType.FilterFieldName = null;
			this.colShopType.Format = "";
			this.colShopType.FormatInfo = null;
			this.colShopType.HeaderText = "Регион";
			this.colShopType.MappingName = "shopType";
			this.columnMenuExtender1.SetMenu(this.colShopType, this.menuFilterSort1);
			this.colShopType.ReadOnly = true;
			this.colShopType.Width = 75;
			// 
			// colCity
			// 
			this.colCity.FieldName = null;
			this.colCity.FilterFieldName = null;
			this.colCity.Format = "";
			this.colCity.FormatInfo = null;
			this.colCity.HeaderText = "Населённый пункт";
			this.colCity.MappingName = "city";
			this.columnMenuExtender1.SetMenu(this.colCity, this.menuFilterSort1);
			this.colCity.ReadOnly = true;
			this.colCity.Width = 75;
			// 
			// colOrderItemCount
			// 
			this.colOrderItemCount.FieldName = "fOrderItemCount(OID)";
			this.colOrderItemCount.FilterFieldName = null;
			this.colOrderItemCount.Format = "";
			this.colOrderItemCount.FormatInfo = null;
			this.colOrderItemCount.HeaderText = "Кол-во товаров";
			this.colOrderItemCount.MappingName = "orderItemCount";
			this.columnMenuExtender1.SetMenu(this.colOrderItemCount, this.menuFilterSort1);
			this.colOrderItemCount.ReadOnly = true;
			this.colOrderItemCount.Width = 75;
			// 
			// colOrderTotalPrice
			// 
			this.colOrderTotalPrice.FieldName = "fOrderTotalPrice(OID)";
			this.colOrderTotalPrice.FilterFieldName = null;
			this.colOrderTotalPrice.Format = "#,#0.##";
			this.colOrderTotalPrice.FormatInfo = null;
			this.colOrderTotalPrice.HeaderText = "Стоимость заказа";
			this.colOrderTotalPrice.MappingName = "orderTotalPrice";
			this.columnMenuExtender1.SetMenu(this.colOrderTotalPrice, this.menuFilterSort1);
			this.colOrderTotalPrice.ReadOnly = true;
			this.colOrderTotalPrice.Width = 75;
			// 
			// colDeliveryPrice
			// 
			this.colDeliveryPrice.FieldName = null;
			this.colDeliveryPrice.FilterFieldName = null;
			this.colDeliveryPrice.Format = "#,#0.##";
			this.colDeliveryPrice.FormatInfo = null;
			this.colDeliveryPrice.HeaderText = "Стоимость доставки";
			this.colDeliveryPrice.MappingName = "deliveryPrice";
			this.columnMenuExtender1.SetMenu(this.colDeliveryPrice, this.menuFilterSort1);
			this.colDeliveryPrice.ReadOnly = true;
			this.colDeliveryPrice.Width = 75;
			// 
			// colEMail
			// 
			this.colEMail.FieldName = null;
			this.colEMail.FilterFieldName = null;
			this.colEMail.Format = "";
			this.colEMail.FormatInfo = null;
			this.colEMail.HeaderText = "e-mail";
			this.colEMail.MappingName = "EMail";
			this.columnMenuExtender1.SetMenu(this.colEMail, this.menuFilterSort1);
			this.colEMail.ReadOnly = true;
			this.colEMail.Width = 75;
			// 
			// colDocType
			// 
			this.colDocType.FieldName = null;
			this.colDocType.FilterFieldName = null;
			this.colDocType.Format = "";
			this.colDocType.FormatInfo = null;
			this.colDocType.HeaderText = "Доставка SV";
			this.colDocType.MappingName = "docType";
			this.columnMenuExtender1.SetMenu(this.colDocType, this.menuFilterSort1);
			this.colDocType.ReadOnly = true;
			this.colDocType.Width = 75;
			// 
			// colSVID
			// 
			this.colSVID.FieldName = null;
			this.colSVID.FilterFieldName = null;
			this.colSVID.Format = "";
			this.colSVID.FormatInfo = null;
			this.colSVID.HeaderText = "Код SV";
			this.colSVID.MappingName = "svId";
			this.columnMenuExtender1.SetMenu(this.colSVID, this.menuFilterSort1);
			this.colSVID.Width = 75;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dtpDateEnd);
			this.panel1.Controls.Add(this.dtpDateBegin);
			this.panel1.Controls.Add(this.lbShopType);
			this.panel1.Controls.Add(this.cbShopType);
			this.panel1.Controls.Add(this.btnRefresh);
			this.panel1.Controls.Add(this.cbShowAll);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(776, 46);
			this.panel1.TabIndex = 1;
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Location = new System.Drawing.Point(152, 12);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.Size = new System.Drawing.Size(136, 20);
			this.dtpDateEnd.TabIndex = 5;
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Location = new System.Drawing.Point(8, 12);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(136, 20);
			this.dtpDateBegin.TabIndex = 4;
			// 
			// lbShopType
			// 
			this.lbShopType.Location = new System.Drawing.Point(512, 9);
			this.lbShopType.Name = "lbShopType";
			this.lbShopType.Size = new System.Drawing.Size(64, 27);
			this.lbShopType.TabIndex = 3;
			this.lbShopType.Text = "Магазин:";
			this.lbShopType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbShopType
			// 
			this.cbShopType.Location = new System.Drawing.Point(584, 10);
			this.cbShopType.Name = "cbShopType";
			this.cbShopType.SelectedValue = -1;
			this.cbShopType.Size = new System.Drawing.Size(184, 21);
			this.cbShopType.TabIndex = 2;
			// 
			// btnRefresh
			// 
			this.btnRefresh.Location = new System.Drawing.Point(432, 9);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(75, 27);
			this.btnRefresh.TabIndex = 1;
			this.btnRefresh.Text = "Обновить";
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// cbShowAll
			// 
			this.cbShowAll.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbShowAll.Location = new System.Drawing.Point(296, 9);
			this.cbShowAll.Name = "cbShowAll";
			this.cbShowAll.Size = new System.Drawing.Size(128, 28);
			this.cbShowAll.TabIndex = 0;
			this.cbShowAll.Text = "Показать все заказы";
			this.cbShowAll.CheckedChanged += new System.EventHandler(this.cbShowAll_CheckedChanged);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnClose);
			this.panel2.Controls.Add(this.btnNew);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 248);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(776, 37);
			this.panel2.TabIndex = 3;
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(8, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 1;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnNew
			// 
			this.btnNew.Location = new System.Drawing.Point(696, 9);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(75, 27);
			this.btnNew.TabIndex = 0;
			this.btnNew.Text = "Новый";
			this.btnNew.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnNew_MouseUp);
			// 
			// SelectDomainMenuStrip
			// 
			this.SelectDomainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wwwelectroburgruToolStripMenuItem,
            this.spbelectroburgruToolStripMenuItem,
            this.nskelectroburgruToolStripMenuItem,
            this.kdrelectroburgruToolStripMenuItem});
			this.SelectDomainMenuStrip.Name = "SelectDomainMenuStrip";
			this.SelectDomainMenuStrip.Size = new System.Drawing.Size(172, 92);
			// 
			// wwwelectroburgruToolStripMenuItem
			// 
			this.wwwelectroburgruToolStripMenuItem.Name = "wwwelectroburgruToolStripMenuItem";
			this.wwwelectroburgruToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.wwwelectroburgruToolStripMenuItem.Text = "www.electroburg.ru";
			this.wwwelectroburgruToolStripMenuItem.Click += new System.EventHandler(this.wwwelectroburgruToolStripMenuItem_Click);
			// 
			// spbelectroburgruToolStripMenuItem
			// 
			this.spbelectroburgruToolStripMenuItem.Name = "spbelectroburgruToolStripMenuItem";
			this.spbelectroburgruToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.spbelectroburgruToolStripMenuItem.Text = "spb.electroburg.ru";
			this.spbelectroburgruToolStripMenuItem.Click += new System.EventHandler(this.spbelectroburgruToolStripMenuItem_Click);
			// 
			// nskelectroburgruToolStripMenuItem
			// 
			this.nskelectroburgruToolStripMenuItem.Name = "nskelectroburgruToolStripMenuItem";
			this.nskelectroburgruToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.nskelectroburgruToolStripMenuItem.Text = "nsk.electroburg.ru";
			this.nskelectroburgruToolStripMenuItem.Click += new System.EventHandler(this.nskelectroburgruToolStripMenuItem_Click);
			// 
			// kdrelectroburgruToolStripMenuItem
			// 
			this.kdrelectroburgruToolStripMenuItem.Name = "kdrelectroburgruToolStripMenuItem";
			this.kdrelectroburgruToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
			this.kdrelectroburgruToolStripMenuItem.Text = "kdr.electroburg.ru";
			this.kdrelectroburgruToolStripMenuItem.Click += new System.EventHandler(this.kdrelectroburgruToolStripMenuItem_Click);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveListToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(167, 26);
			// 
			// SaveListToolStripMenuItem
			// 
			this.SaveListToolStripMenuItem.Name = "SaveListToolStripMenuItem";
			this.SaveListToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
			this.SaveListToolStripMenuItem.Text = "Сохранить список";
			this.SaveListToolStripMenuItem.Click += new System.EventHandler(this.SaveListToolStripMenuItem_Click);
			// 
			// SaveList
			// 
			this.SaveList.DefaultExt = "xls";
			this.SaveList.Filter = "Файлы Excel|*.xls|Все файлы|*.*";
			this.SaveList.Title = "Сохранить список";
			// 
			// ListOrder
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(776, 285);
			this.Controls.Add(this.dgOrders);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "ListOrder";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Заказы";
			this.Load += new System.EventHandler(this.fmListOrder_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgOrders)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.SelectDomainMenuStrip.ResumeLayout(false);
			this.contextMenuStrip1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.FormattableTextBoxColumn colDeliveryPrice;
		private ColumnMenuExtender.FormattableTextBoxColumn colEMail;
		private ToolStripMenuItem kdrelectroburgruToolStripMenuItem;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colDocType;
		private ContextMenuStrip contextMenuStrip1;
		private ToolStripMenuItem SaveListToolStripMenuItem;
		private SaveFileDialog SaveList;
		private ColumnMenuExtender.FormattableTextBoxColumn colSVID;		
	}
}
