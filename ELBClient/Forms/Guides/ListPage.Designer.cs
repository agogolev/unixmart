using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListPage
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// mainDataGrid1
			// 
			this.mainDataGrid1.AdditionalInfo = "";
			this.mainDataGrid1.ClassName = "CPage";
			this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
			this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.mainDataGrid1.Name = "mainDataGrid1";
			this.mainDataGrid1.Size = new System.Drawing.Size(542, 323);
			this.mainDataGrid1.TabIndex = 2;
			this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.Width = 150;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Сайт";
			this.formattableTextBoxColumn2.MappingName = "site";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// ListPage
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 323);
			this.Controls.Add(this.mainDataGrid1);
			this.Name = "ListPage";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Страницы";
			this.Load += new System.EventHandler(this.fmListPage_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
