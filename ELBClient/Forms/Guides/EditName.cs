﻿using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	public partial class EditName : EditForm
	{
		public EditName()
		{
			InitializeComponent();
			Object = new CTheme();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch{}
			//this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void BindFields()
		{
			txtName.DataBindings.Add("Text", Object, "ThemeName");
		}

		private void fmEditTheme_Load(object sender, System.EventArgs e)
		{
			LoadObject("themeName", null);
			BindFields();
		}
	}
}
