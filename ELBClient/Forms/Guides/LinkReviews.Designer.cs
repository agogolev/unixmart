using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class LinkReviews
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
private System.Windows.Forms.Button btnClose;
private System.Windows.Forms.Panel panel2;
private System.Windows.Forms.ContextMenu contextMenu1;
private System.Windows.Forms.MenuItem menuItem1;
private System.Windows.Forms.Panel panel3;
private System.Windows.Forms.TreeView tvFirstTree;
private System.Windows.Forms.TreeView tvSecondTree;
private System.Windows.Forms.Splitter splitter1;
private System.Windows.Forms.ComboBox cbFirst;
private System.Windows.Forms.ComboBox cbSecond;
private System.Windows.Forms.Button button1;
private System.Windows.Forms.Button button2;
private System.Windows.Forms.Label label1;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.btnClose = new System.Windows.Forms.Button();
			this.tvFirstTree = new System.Windows.Forms.TreeView();
			this.tvSecondTree = new System.Windows.Forms.TreeView();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.cbSecond = new System.Windows.Forms.ComboBox();
			this.cbFirst = new System.Windows.Forms.ComboBox();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 455);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(768, 46);
			this.panel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AllowDrop = true;
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Location = new System.Drawing.Point(520, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(240, 28);
			this.label1.TabIndex = 1;
			this.label1.Text = "Удалить привязки";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label1.DragDrop += new System.Windows.Forms.DragEventHandler(this.label1_DragDrop);
			this.label1.DragOver += new System.Windows.Forms.DragEventHandler(this.label1_DragOver);
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(8, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// tvFirstTree
			// 
			this.tvFirstTree.AllowDrop = true;
			this.tvFirstTree.Dock = System.Windows.Forms.DockStyle.Left;
			this.tvFirstTree.Location = new System.Drawing.Point(0, 46);
			this.tvFirstTree.Name = "tvFirstTree";
			this.tvFirstTree.Size = new System.Drawing.Size(392, 409);
			this.tvFirstTree.TabIndex = 1;
			this.tvFirstTree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvFirstTree_ItemDrag);
			this.tvFirstTree.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvFirstTree_DragDrop);
			this.tvFirstTree.DragOver += new System.Windows.Forms.DragEventHandler(this.tvFirstTree_DragOver);
			this.tvFirstTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvFirstTree_MouseDown);
			// 
			// tvSecondTree
			// 
			this.tvSecondTree.AllowDrop = true;
			this.tvSecondTree.ContextMenu = this.contextMenu1;
			this.tvSecondTree.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvSecondTree.Location = new System.Drawing.Point(0, 0);
			this.tvSecondTree.Name = "tvSecondTree";
			this.tvSecondTree.Size = new System.Drawing.Size(376, 409);
			this.tvSecondTree.TabIndex = 0;
			this.tvSecondTree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvSecondTree_ItemDrag);
			this.tvSecondTree.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvSecondTree_DragDrop);
			this.tvSecondTree.DragOver += new System.Windows.Forms.DragEventHandler(this.tvSecondTree_DragOver);
			this.tvSecondTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvSecondTree_MouseDown);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Показать привязанные обзоры";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tvSecondTree);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(392, 46);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(376, 409);
			this.panel2.TabIndex = 2;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.button2);
			this.panel3.Controls.Add(this.button1);
			this.panel3.Controls.Add(this.cbSecond);
			this.panel3.Controls.Add(this.cbFirst);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(768, 46);
			this.panel3.TabIndex = 5;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(544, 10);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 27);
			this.button2.TabIndex = 3;
			this.button2.Text = "Показать";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(160, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 27);
			this.button1.TabIndex = 2;
			this.button1.Text = "Показать";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// cbSecond
			// 
			this.cbSecond.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSecond.Location = new System.Drawing.Point(400, 12);
			this.cbSecond.Name = "cbSecond";
			this.cbSecond.Size = new System.Drawing.Size(144, 21);
			this.cbSecond.TabIndex = 1;
			// 
			// cbFirst
			// 
			this.cbFirst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbFirst.Location = new System.Drawing.Point(8, 9);
			this.cbFirst.Name = "cbFirst";
			this.cbFirst.Size = new System.Drawing.Size(152, 21);
			this.cbFirst.TabIndex = 0;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(392, 46);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 409);
			this.splitter1.TabIndex = 6;
			this.splitter1.TabStop = false;
			// 
			// LinkReviews
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(768, 501);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.tvFirstTree);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel3);
			this.Name = "LinkReviews";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Привязка обзоров";
			this.Load += new System.EventHandler(this.fmLinkMaster_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
