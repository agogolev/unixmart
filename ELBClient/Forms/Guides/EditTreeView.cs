﻿using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Xml.Linq;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using System.Xml;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditTree.
	/// </summary>
	public partial class EditTreeView : EditForm
	{
		private int parentNode;
		public int ParentNode
		{
			get
			{
				return this.parentNode;
			}
			set
			{
				this.parentNode = value;
			}
		}
		private TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
		private Classes.Tree _tree;
		private string _oldFullPath = "";
		private XmlDocument _nodeRules;
		private System.Data.DataSet dataSet1;
		
		private System.ComponentModel.IContainer components;

		public EditTreeView()
		{
			InitializeComponent();
			Object = new CTree();
			_tree = new Tree(0, int.MaxValue);
			_nodeRules = new XmlDocument();
			_nodeRules.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><root></root>");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void fmEditTree_Load(object sender, System.EventArgs e)
		{
			LoadObjects();
			BuildTree();
			LoadTree();
		}

		private void LoadObjects()
		{
			LoadObject("treeName", null);

			//Filling OID
			var element = XElement.Parse("<tree />");
			if (ObjectOID != Guid.Empty) element.Add(new XAttribute("OID", ObjectOID));
			if (ParentNode != 0) element.Add(new XAttribute("nodeID", ParentNode),
				new XAttribute("withParent", "true"));
			dataSet1 = treeProvider.GetTreeContent(element.ToString());
			XmlDocument doc = GetResource("nodeRules.xml");
			XmlNode n = doc.DocumentElement.SelectSingleNode("treeView[@OID='" + ObjectOID.ToString().ToUpper() + "']/@answerOID");
			if (n != null)
			{
				string answerOID = n.Value;
				CAnswer ans = new CAnswer();
				ans.LoadXml(op.GetObject(answerOID, "body", null));
				_nodeRules = new XmlDocument();
				_nodeRules.LoadXml(ans.Body);
			}
		}

		//Filling Tree
		private void BuildTree()
		{
			Stack nodes = new Stack();
			nodes.Push(_tree);

			int prevNode = 0;
			foreach (DataRow dr in dataSet1.Tables["table"].Rows)
			{
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if (nodeID != prevNode)
				{
					node = new Node((int)dr["lft"], (int)dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString().RestoreCr();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"];
					node.objectNK = dr["NK"].ToString().RestoreCr();// == DBNull.Value ? null: (string) dr["NK"];
					node.objectNKRus = dr["label"].ToString().RestoreCr();
					node.className = dr["className"].ToString();
					while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer)nodes.Peek()).Nodes.Add(node);
					if ((node.Left + 1) != node.Right)
					{
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else
				{
					if (node != null)
					{
						node.AddString.Rows.Add(new string[3] { (string)dr["ordValue"], (string)dr["value"], (string)dr["url"] });
						node.AddString.AcceptChanges();
					}
				}
			}
		}

		private void LoadTree()
		{
			tv1.Nodes.Clear();
			foreach (Node n in _tree.Nodes)
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tv1.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node)
		{
			foreach (Node n in node.Nodes)
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		//Add Del Edit nodes
		private void miAdd_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;
			XmlNode nDescr = GetNodeDescription();
			string[] allowedClassName = null;
			if (nDescr != null)
			{
				XmlNodeList nl = nDescr.SelectNodes(".//object/@className");
				allowedClassName = new string[nl.Count];
				for (int i = 0; i < nl.Count; i++)
				{
					allowedClassName[i] = nl[i].Value;
				}
			}
			EditTreeViewNode fm = new EditTreeViewNode();
			fm.AllowedClassName = allowedClassName;
			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				//1

				XmlDocument doc = new XmlDocument();
				XmlNode root = doc.CreateNode(XmlNodeType.Element, "node", "");

				XmlAttribute attr = doc.CreateAttribute("parentNode");
				attr.Value = ((Node)selectedNode.Tag).NodeId.ToString();
				root.Attributes.Append(attr);

				Node n = new Node();
				n.Name = fm.NodeName.Prop;
				n.objectOID = fm.NodeObject.Prop;
				n.objectNK = fm.NodeObjectNK.Prop;
				n.objectNKRus = fm.NodeObjectNKRus;
				n.className = fm.NodeObjectNKRus;

				if (n.Name != "")
				{
					attr = doc.CreateAttribute("nodeName");
					attr.Value = n.Name;
					root.Attributes.Append(attr);
				}
				if (n.objectOID != Guid.Empty)
				{
					attr = doc.CreateAttribute("object");
					attr.Value = n.objectOID.ToString();
					root.Attributes.Append(attr);
				}
				DataTable deleted = n.AddString.GetChanges(DataRowState.Deleted);
				DataTable added = n.AddString.GetChanges(DataRowState.Added);
				if (added != null)
				{
					foreach (DataRow dr in added.Rows)
					{
						XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string)dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string)dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string)dr["url"];
					}
				}

				doc.AppendChild(root);
				n.NodeId = treeProvider.CreateNode(doc.OuterXml);
				n.AddString.AcceptChanges();

				TreeNode tn = new TreeNode(n.Name.Replace("\r\n", " "));
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tn.Tag = n;
				selectedNode.Nodes.Insert(0, tn);
			}//1
		}

		private void miAddSibling_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;
			XmlNode nDescr = GetNodeDescription();
			string[] AllowedClassName = null;
			if (nDescr != null)
			{
				XmlNodeList nl = nDescr.SelectNodes(".//object/@className");
				AllowedClassName = new string[nl.Count];
				for (int i = 0; i < nl.Count; i++)
				{
					AllowedClassName[i] = nl[i].Value;
				}
			}

			EditTreeViewNode fm = new EditTreeViewNode();
			fm.AllowedClassName = AllowedClassName;
			if (fm.ShowDialog(this) == DialogResult.OK)
			{//1
				XmlDocument doc = new XmlDocument();
				XmlNode root = doc.CreateNode(XmlNodeType.Element, "node", "");

				XmlAttribute attr1 = doc.CreateAttribute("leftSibling");
				attr1.Value = ((Node)selectedNode.Tag).NodeId.ToString();
				root.Attributes.Append(attr1);

				Node n = new Node();
				n.Name = fm.NodeName.Prop;
				n.objectOID = fm.NodeObject.Prop;
				n.objectNK = fm.NodeObjectNK.Prop;
				n.objectNKRus = fm.NodeObjectNKRus;
				n.className = fm.NodeObjectNKRus;

				if (n.Name != "")
				{
					XmlAttribute attr = doc.CreateAttribute("nodeName");
					attr.Value = n.Name;
					root.Attributes.Append(attr);
				}
				if (n.objectOID != Guid.Empty)
				{
					XmlAttribute attr = doc.CreateAttribute("object");
					attr.Value = n.objectOID.ToString();
					root.Attributes.Append(attr);
				}
				DataTable deleted = n.AddString.GetChanges(DataRowState.Deleted);
				DataTable added = n.AddString.GetChanges(DataRowState.Added);
				if (added != null)
				{
					foreach (DataRow dr in added.Rows)
					{
						XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string)dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string)dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string)dr["url"];
					}
				}

				doc.AppendChild(root);
				n.NodeId = treeProvider.CreateNode(doc.OuterXml);
				n.AddString.AcceptChanges();

				TreeNode tn = new TreeNode(n.Name.Replace("\r\n", " "));
				if (n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tn.Tag = n;
				TreeNode parent = tv1.SelectedNode.Parent;
				int indx = tv1.SelectedNode.Index;

				if (parent != null)
					parent.Nodes.Insert(indx + 1, tn);
				else tv1.Nodes.Insert(indx + 1, tn);
			}//1
		}

		private void miEdit_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;
			XmlNode nDescr = GetNodeDescription();
			string[] AllowedClassName = null;
			bool allowObject = true;
			if (nDescr != null)
			{
				XmlNodeList nl = nDescr.SelectNodes(".//object/@className");
				AllowedClassName = new string[nl.Count];
				for (int i = 0; i < nl.Count; i++)
				{
					AllowedClassName[i] = nl[i].Value;
				}
				if (nDescr.Attributes["object"] != null && nDescr.Attributes["object"].Value == "0") allowObject = false;
			}

			EditTreeViewNode fm = new EditTreeViewNode();
			fm.AllowObject = allowObject;
			fm.AllowedClassName = AllowedClassName;
			Node n = (Node)selectedNode.Tag;
			fm.NodeID = n.NodeId;
			fm.NodeName = new MetaData.StringProperty(n.Name);
			fm.NodeObject = new MetaData.GuidProperty(n.objectOID);
			fm.NodeObjectNK = new MetaData.StringProperty(n.objectNK);
			fm.NodeObjectNKRus = n.objectNKRus;

			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				//1

				XmlDocument doc = new XmlDocument();
				XmlNode root = doc.CreateElement("node");
				//Node n1 = (Node)selectedNode.Tag;

				XmlAttribute attr1 = doc.CreateAttribute("nodeID");
				attr1.Value = n.NodeId.ToString();
				root.Attributes.Append(attr1);

				if (n.Name != fm.NodeName.Prop)
				{
					n.Name = fm.NodeName.Prop;
					selectedNode.Text = fm.NodeName.Prop.Replace("\r\n", " ");
					XmlAttribute attr = doc.CreateAttribute("nodeName");
					attr.Value = fm.NodeName.Prop;
					root.Attributes.Append(attr);
				}
				Guid temp = fm.NodeObject.Prop;
				n.objectNK = fm.NodeObjectNK.Prop;
				if (n.objectOID != temp)
				{
					n.objectOID = temp;
					n.objectNKRus = fm.NodeObjectNKRus;
					if (temp != Guid.Empty)
						selectedNode.ForeColor = Color.Red;
					else selectedNode.ForeColor = Color.Black;
					XmlAttribute attr = doc.CreateAttribute("object");
					attr.Value = temp.ToString();
					root.Attributes.Append(attr);
				}
				DataTable deleted = n.AddString.GetChanges(DataRowState.Deleted);
				if (deleted != null)
				{
					foreach (DataRow dr in deleted.Rows)
					{
						XmlNode n2 = root.AppendChild(doc.CreateElement("delete"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string)dr["ordValue", DataRowVersion.Original];
					}
				}
				DataTable added = n.AddString.GetChanges(DataRowState.Added);
				if (added != null)
				{
					foreach (DataRow dr in added.Rows)
					{
						XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string)dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string)dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string)dr["url"];
					}
				}
				DataTable updated = n.AddString.GetChanges(DataRowState.Modified);
				if (updated != null)
				{
					foreach (DataRow dr in updated.Rows)
					{
						XmlNode n2 = root.AppendChild(doc.CreateElement("update"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string)dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string)dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string)dr["url"];
					}
				}
				doc.AppendChild(root);
				treeProvider.UpdateNode(doc.OuterXml);
				n.AddString.AcceptChanges();
			}//1
		}

		private void miEditObject_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;
			Node n = (Node)selectedNode.Tag;
			EditForm frm = Classes.FormSelection.GetEditForm(n.className);
			if (frm != null)
			{
				frm.ObjectOID = n.objectOID;
				frm.MdiParent = this.MdiParent;
				frm.Show();
			}
		}
		private void miDel_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				TreeNode selectedNode = tv1.SelectedNode;

				if (selectedNode != null)
				{
					TreeNode parent = selectedNode.Parent;
					TreeNode tmp;
					int indx = selectedNode.Index;

					treeProvider.DeleteNode(((Node)selectedNode.Tag).NodeId);

					if (selectedNode.Nodes.Count != 0)
					{
						tmp = (TreeNode)selectedNode.Clone();

						if (parent != null)
						{
							foreach (TreeNode child in tmp.Nodes)
								parent.Nodes.Insert(indx++, child);
						}
						else
						{	//Root
							foreach (TreeNode child in tmp.Nodes)
								tv1.Nodes.Insert(indx++, child);
						}
					}
					selectedNode.Remove();
				}
			}
		}

		//Working With Tree
		private void tv1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				tv1.SelectedNode = tv1.GetNodeAt(e.X, e.Y);
				if (tv1.SelectedNode == null)
				{
					tv1.ContextMenu = null;
				}
				else
				{
					tv1.ContextMenu = contextMenu1;
					GetMenuState();
				}
			}
		}
		private void GetMenuState()
		{
			miAdd.Visible = true;
			miAddSibling.Visible = true;
			miDel.Visible = true;
			miEdit.Visible = true;
			miEditObject.Visible = true;
			Node node = (Node)tv1.SelectedNode.Tag;
			XmlNode n = GetNodeDescription();

			if (n != null)
			{
				XmlAttribute attr = n.Attributes["delete"];
				if (attr != null && attr.Value == "0") miDel.Visible = false;
				attr = n.Attributes["child"];
				if (attr != null && attr.Value == "0") miAdd.Visible = false;
				attr = n.Attributes["sibling"];
				if (attr != null && attr.Value == "0") miAddSibling.Visible = false;
			}
			else
			{// К динамическим разделам нельзя добавлять child
				//miAdd.Visible = false;
			}
			if (node.objectOID != Guid.Empty) miEditObject.Visible = true;
			else miEditObject.Visible = false;
			if (tv1.SelectedNode.Nodes.Count != 0)
			{
				miDel.Visible = false; // Ноды с потомками удалять нельзя
			}
		}
		private XmlNode GetNodeDescription()
		{
			TreeNode tNode = tv1.SelectedNode;
			Node node = (Node)tNode.Tag;
			XmlNode n = _nodeRules.DocumentElement.SelectSingleNode("node[@nodeID = " + node.NodeId + "]");
			if (n != null) return n;
			else
			{
				TreeNode parent = tNode.Parent;
				if (parent == null) return null;
				else return GetNodeDescription(parent);
			}
		}
		private XmlNode GetNodeDescription(TreeNode tNode)
		{
			Node node = (Node)tNode.Tag;
			XmlNode n = _nodeRules.DocumentElement.SelectSingleNode("node[@nodeID = " + node.NodeId + "]");
			if (n != null && n.SelectSingleNode("childNode") != null) return n.SelectSingleNode("childNode");
			else
			{
				TreeNode parent = tNode.Parent;
				if (parent == null) return null;
				else return GetNodeDescription(parent);
			}
		}

		private void tv1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			TreeNode tn = tv1.GetNodeAt(e.X, e.Y);

			if (tn != null)
			{
				string currentFullPath = tn.FullPath;

				if (currentFullPath != _oldFullPath)
				{
					_oldFullPath = currentFullPath;

					if (((Node)tn.Tag).objectNK != "")
					{
						if (toolTip1.Active)
							toolTip1.Active = false; //turn it off 
						toolTip1.SetToolTip(tv1, string.Format("Объект: {0}\nКласс: {1}", ((Node)tn.Tag).objectNK, ((Node)tn.Tag).objectNKRus));
						toolTip1.Active = true; //make it active so it can show 
					}
					else
					{
						if (toolTip1.Active)
							toolTip1.Active = false; //turn it off 
					}
				}
			}
		}

		private void miUp_Click(object sender, System.EventArgs e)
		{
			//перемещение нода вверх
			TreeNode selectedNode = tv1.SelectedNode;
			TreeNode prevNode = selectedNode.PrevNode;
			if (selectedNode != null && prevNode != null)
			{
				try
				{
					int nodeID = ((Node)selectedNode.Tag).NodeId;
					op.ExecuteCommand("spShiftNode @nodeID='" + nodeID + "', @up=1");
					prevNode.Parent.Nodes.Remove(prevNode);
					selectedNode.Parent.Nodes.Insert(selectedNode.Index + 1, prevNode);
				}
				catch
				{
					this.ShowError("Не удалось передвинуть элемент");
				}
			}
		}

		private void miDown_Click(object sender, System.EventArgs e)
		{
			//перемещение нода вниз
			TreeNode selectedNode = tv1.SelectedNode;
			TreeNode nextNode = selectedNode.NextNode;
			if (selectedNode != null && nextNode != null)
			{
				try
				{
					int nodeID = ((Node)selectedNode.Tag).NodeId;
					op.ExecuteCommand("spShiftNode @nodeID=" + nodeID + ", @up=0");
					selectedNode.Parent.Nodes.Remove(selectedNode);
					nextNode.Parent.Nodes.Insert(nextNode.Index + 1, selectedNode);
				}
				catch
				{
					this.ShowError("Не удалось передвинуть элемент");
				}
			}
		}

		private void tv1_DoubleClick(object sender, System.EventArgs e)
		{
			if (tv1.SelectedNode != null)
			{
				Node node = (Node)tv1.SelectedNode.Tag;
				if (node.objectNK != "") miEditObject_Click(tv1, EventArgs.Empty);
			}
		}
	}
}
