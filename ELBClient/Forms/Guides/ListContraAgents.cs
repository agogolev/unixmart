﻿namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListContraAgents.
	/// </summary>
	public partial class ListContraAgents : ListForm
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		
				
		public ListContraAgents()
		{
			InitializeComponent();
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmListContraAgents_Load(object sender, System.EventArgs e) {
			mainDataGrid1.LoadData();
		}

	}
}
