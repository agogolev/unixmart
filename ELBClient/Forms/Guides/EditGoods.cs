﻿using System;
using System.Linq;
using System.Windows.Forms;
using ELBClient.Classes;
using ColumnMenuExtender;
using ELBClient.Forms.Dialogs;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditGoods.
	/// </summary>
	public partial class EditGoods : EditForm
	{
		public bool IsDiscount { get; set; }
		private DataSetISM dsParams;
		private DataTable DocsData;
		public ObjectItem NewGoodsCategory { get; set; }

		public EditGoods()
		{
			InitializeComponent();
			Object = new CGoods();
			extendedDataGridSelectorColumn1.ButtonClick += extendedDataGridSelectorColumn1_ButtonClick;
		}

		void extendedDataGridSelectorColumn1_ButtonClick(object sender, DataGridCellEventArgs e)
		{
			DataRow dr = DescriptionsGrid.GetSelectedRow();
			if (dr != null)
			{
				var description = dr["propValue"].ToString();

				var frm = new Info { Body = description };
				frm.ShowDialog(MdiParent);
				dr["propValue"] = frm.Body;
			}
		}

		public void btnSave_Click(object sender, EventArgs e)
		{

			SaveObject();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		public void EditGoods_Load(object sender, EventArgs e)
		{
			LoadRights();

			LoadLookup();
			if (ObjectOID != Guid.Empty)
			{
				LoadData();
			}
			if (IsDiscount)
			{
				PricesLabel.Visible = false;
				dgPrices.Visible = false;
				InStockLabel.Visible = false;
				dgInStocks.Visible = false;
			}
			BindFields();
			LoadGrids();
		}

		private void LoadRights()
		{
		    if (MainFrm.IsGroupMember(ELBExpImp.Constants.AdminOID))
			{
			}
            else if (MainFrm.IsGroupMember(ELBExpImp.Constants.PriceManagerOID))
			{
			}
            else if (MainFrm.IsGroupMember(ELBExpImp.Constants.SellerOID))
			{
				tabControl1.TabPages.Remove(GeneralPage);
				tabControl1.TabPages.Remove(AdditionalPage);
				tabControl1.TabPages.Remove(AccessoryPage);
				tabControl1.TabPages.Remove(ReviewPage);
				tabControl1.TabPages.Remove(KeywordsPage);
				tabControl1.TabPages.Remove(AnalogPage);
			}
            else if (MainFrm.IsGroupMember(ELBExpImp.Constants.GoodsDescriptorOID))
			{
				tabControl1.TabPages.Remove(GeneralPage);
				tabControl1.TabPages.Remove(AdditionalPage);
				tabControl1.TabPages.Remove(AccessoryPage);
				tabControl1.TabPages.Remove(ReviewPage);
				tabControl1.TabPages.Remove(KeywordsPage);
				tabControl1.TabPages.Remove(AnalogPage);
			}
            else if (MainFrm.IsGroupMember(ELBExpImp.Constants.ContentManagerOID))
			{
				tabControl1.TabPages.Remove(GeneralPage);
				tabControl1.TabPages.Remove(AdditionalPage);
				tabControl1.TabPages.Remove(ParamPage);
				tabControl1.TabPages.Remove(AccessoryPage);
				tabControl1.TabPages.Remove(ReviewPage);
				tabControl1.TabPages.Remove(KeywordsPage);
				tabControl1.TabPages.Remove(AnalogPage);
			}
		}
		private void LoadLookup()
		{
			//string xml =lp.GetLookupValues("CParamType");
			string xml = lp.GetLookupValues("COrder");
			colcbShopTypeNew.ComboBox.LoadXml(xml, "shopTypeN");
			colcbShopTypeHit.ComboBox.LoadXml(xml, "shopTypeN");
			extendedDataGridComboBoxColumn1.ComboBox.LoadXml(xml, "shopTypeN");
			extendedDataGridComboBoxColumn2.ComboBox.LoadXml(xml, "shopTypeN");
			extendedDataGridComboBoxColumn3.ComboBox.LoadXml(xml, "shopTypeN");
			extendedDataGridComboBoxColumn4.ComboBox.LoadXml(xml, "shopTypeN");
			extendedDataGridComboBoxColumn5.ComboBox.LoadXml(xml, "shopTypeN");
			xml = lp.GetLookupValues("CGoods");
			VariantTypeSelect.LoadXml(xml, "variantTypeN");
			if (VariantTypeSelect.Items.Count != 0) return;
			VariantTypeSelect.Visible = false;
			VariantLabel.Visible = false;
		}
		private void BindFields()
		{
			Model.DataBindings.Add("Text", Object, "Model");
			ProductType.DataBindings.Add("Text", Object, "ProductType");
			Category.DataBindings.Add("BoundProp", Object, "Category");
			Manufacturer.DataBindings.Add("BoundProp", Object, "Manufacturer");
			MinSellPrice.DataBindings.Add("BoundProp", Object, "MinSellPrice");
			PostavshikIDText.DataBindings.Add("BoundProp", Object, "Postavshik_id");
			ManufPrice.DataBindings.Add("Text", Object, "ManufPrice");
			Title.DataBindings.Add("Text", Object, "Title");
			Keywords.DataBindings.Add("Text", Object, "Keywords");
			PageDescription.DataBindings.Add("Text", Object, "PageDescription");
			YandexUrl.DataBindings.Add("Text", Object, "YandexUrl");
			SalesNotesText.DataBindings.Add("Text", Object, "SalesNotes");
			VariantTypeSelect.DataBindings.Add("SelectedValue", Object, "variantType");
		}

		private void LoadData()
		{
			string xml = GetResourceString("fmEditGoods.xml");
			xml = LoadObject(xml, CGoods.ClassCID);
			dsPrices.LoadRowsFromXML("Table", xml, "prices");
			dsInStock.LoadRowsFromXML("Table", xml, "inStocks");
			dsIsNew.LoadRowsFromXML("Table", xml, "isNews");
			dsIsHit.LoadRowsFromXML("Table", xml, "isHits");
			dsIsSales.LoadRowsFromXML("Table", xml, "isSales");
			dsIsFavorites.LoadRowsFromXML("Table", xml, "isFavorites");
			DescriptionsGrid.DataSource = (Object as CGoods).Descriptions;
			var obj = (Object as CObject);
			KeywordsGrid.DataSource = obj.Keywrds;
			obj.Keywrds.Columns["ordValue"].AutoIncrement = true;
			var ordValue = obj.Keywrds.AsEnumerable().Max(row => row.Field<int?>("ordValue"));
			obj.Keywrds.Columns["ordValue"].AutoIncrementSeed = (ordValue ?? 0) + 1;

			DocsData = ((CObject)Object).Docs.Copy();
			DocsDataGrid.extendedDataGrid1.DataSource = DocsData;
			DocsDataGrid.extendedDataGrid1.DisableNewDel();
		}
		private void LoadGrids()
		{
			var sql = @"
Declare 
	@themeOID uniqueidentifier

SELECT TOP 1 
	@themeOID = tm.OID 
FROM t_ThemeMasters tm
	inner join t_Goods g on tm.masterOID = g.category
WHERE 
	g.OID = '" + Object.OID + @"'
	and EXISTS(SELECT * FROM t_TemplateParams WHERE OID = tm.OID)
	
SELECT
	gp.ordValue,
	pt.name,
	paramType = pt.OID,
	gp.value,
	type = pt.paramType,
	tp.isDomain,
	isMandatory = ISNULL(tp.isMandatory, 0),
	isFullName = ISNULL(tp.isFullName, 0),
	gp.unit
FROM
	t_GoodsParams gp
	inner join t_ParamType pt on gp.paramTypeOID = pt.OID And gp.OID = '" + ObjectOID + @"'
	left join t_TemplateParams tp on tp.OID = @themeOID And tp.paramTypeOID = pt.OID
ORDER BY
	gp.ordValue

SELECT
	tp.ordValue,
	pt.name,
	paramType = pt.OID,
	value = null,
	type = pt.paramType,
	tp.isDomain,
	tp.isMandatory,
	tp.isFullName,
	tp.unit
FROM
	t_TemplateParams tp
	inner join t_ParamType pt on tp.paramTypeOID = pt.OID And tp.OID = @themeOID
ORDER BY
	tp.ordValue
";
			dsParams = new DataSetISM(lp.GetDataSetSql(sql));
			var dv = new DataView(dsParams.Table, null, "ordValue", DataViewRowState.CurrentRows);
			dgParams.SetDataBinding(dv, null);
			dgParams.DisableNewDel();
			if (dsParams.Table.Rows.Count != dsParams.Tables[1].Rows.Count)
			{
				foreach (DataRow dr in dsParams.Tables[1].Rows)
				{
					dv = new DataView(dsParams.Table, "paramType = '" + dr["paramType"] + "'", null, DataViewRowState.CurrentRows);
					if (dv.Count == 0)
					{
						dsParams.Table.Rows.Add(new[] { dr["ordValue"], dr["name"], dr["paramType"], DBNull.Value, dr["type"], dr["isDomain"], dr["isMandatory"], dr["isFullName"], dr["unit"] });
					}
				}
			}

			dgPrices.SetDataBinding(dsPrices, "Table");
			dgInStocks.SetDataBinding(dsInStock, "Table");
			dgIsNew.SetDataBinding(dsIsNew, "Table");
			dgIsHit.SetDataBinding(dsIsHit, "Table");
			dgIsSale.SetDataBinding(dsIsSales, "Table");
			dgIsFavorites.SetDataBinding(dsIsFavorites, "Table");

			dgAccessories.extendedDataGrid1.DataSource = ((CObject)Object).Accessories;
			dgAccessories.extendedDataGrid1.DisableNewDel();
			dgPressReleases.extendedDataGrid1.DataSource = ((CObject)Object).PressReleases;
			dgPressReleases.extendedDataGrid1.DisableNewDel();
			AnalogsAddDelDataGrid.extendedDataGrid1.DataSource = ((CGoods)Object).Analogs;
			AnalogsAddDelDataGrid.extendedDataGrid1.DisableNewDel();
		}

		private void btnAttachImages_Click(object sender, EventArgs e)
		{
			if (ObjectOID == Guid.Empty)
				MessageBox.Show(this, @"Сначала сохраните объект!");
			else
			{
				var fm = new AttachImages(Object) { MdiParent = ParentForm };
				fm.Show();
			}
		}

		protected override bool IsModified
		{
			get
			{
				return IsParamsModified() || dsPrices.IsModified || dsInStock.IsModified || dsIsSales.IsModified || dsIsFavorites.IsModified || dsIsHit.IsModified || dsIsNew.IsModified || base.IsModified;
			}
		}

		private bool IsParamsModified()
		{
			if (dsParams.Table.Select(null, null, DataViewRowState.ModifiedCurrent).Length != 0) return true;
			if (dsParams.Table.Select(null, null, DataViewRowState.Deleted).Length != 0) return true;
			return dsParams.Table.Select(null, null, DataViewRowState.Added).Count(row => !string.IsNullOrEmpty(row.Field<string>("value"))) != 0;
		}

		protected override string AllowToSave()
		{
			if (dsParams.Table.Rows.Cast<DataRow>().Where(dr => (bool)dr["isMandatory"]).Any(dr => dr["value"] == DBNull.Value))
			{
				return "Не все обязательные поля заполнены";
			}
			return base.AllowToSave();
		}

		protected override void SaveObject()
		{
			if (dsParams.IsModified)
				Object.ExtMultiProp = dsParams.GetChangesXml("Params");
			if (dsPrices.IsModified)
				Object.ExtMultiProp += dsPrices.GetChangesXml("Prices").Replace("ordValue", "shopType").Replace("propValue", "price");
			if (dsInStock.IsModified)
				Object.ExtMultiProp += dsInStock.GetChangesXml("InStocks").Replace("ordValue", "shopType").Replace("propValue", "inStock");
			if (dsIsSales.IsModified)
				Object.ExtMultiProp += dsIsSales.GetChangesXml("IsSales").Replace("ordValue", "shopType").Replace("propValue", "isSale");
			if (dsIsFavorites.IsModified)
				Object.ExtMultiProp += dsIsFavorites.GetChangesXml("IsFavorites").Replace("ordValue", "shopType").Replace("propValue", "isFavorite");
			if (dsIsNew.IsModified)
				Object.ExtMultiProp += dsIsNew.GetChangesXml("IsNews").Replace("ordValue", "shopType").Replace("propValue", "isNew");
			if (dsIsHit.IsModified)
				Object.ExtMultiProp += dsIsHit.GetChangesXml("IsHits").Replace("ordValue", "shopType").Replace("propValue", "isHit");
			base.SaveObject();
			LoadGrids();
			//dsParams.AcceptChanges();
			dsPrices.AcceptChanges();
			dsInStock.AcceptChanges();
			dsIsSales.AcceptChanges();
			dsIsNew.AcceptChanges();
			dsIsHit.AcceptChanges();
		}

		private void addDelDataGridExt2_AddClick(object sender, EventArgs e)
		{
			var fm = new ListDialog("CArticle", new[] { "header", "pubDate" }, new[] { "Название", "Дата публикации" });
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			var o = fm.SelectedOID;
			var s = fm.SelectedNK;
			var ordValue = GetLastPressValue();
			((CObject)Object).PressReleases.Rows.Add(new object[] { o, s, "CArticle", ordValue });
		}

		private void addDelDataGridExt2_DeleteClick(object sender, EventArgs e)
		{
			DataRow dr = dgPressReleases.extendedDataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
		}

		private int GetLastPressValue()
		{
			int ordValue = 0;
			foreach (DataRow dr in ((CObject)Object).PressReleases.Rows)
			{
				if (dr.RowState != DataRowState.Deleted && dr["ordValue"] != DBNull.Value
					&& (int)dr["ordValue"] > ordValue)
					ordValue = (int)dr["ordValue"];
			}
			return ++ordValue;
		}

		private int GetLastAnalogValue()
		{
			int ordValue = 0;
			foreach (DataRow dr in ((CGoods)Object).Analogs.Rows)
			{
				if (dr.RowState != DataRowState.Deleted && dr["ordValue"] != DBNull.Value
					&& (int)dr["ordValue"] > ordValue)
					ordValue = (int)dr["ordValue"];
			}
			return ++ordValue;
		}
		private void addDelDataGridExt1_AddClick(object sender, EventArgs e)
		{
			var fm = new ListGoodsDialog { ShopType = 1 };
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			foreach (var dr in fm.SelectedRows)
			{
				if (dr == null) return;
				var o = (Guid)dr["OID"];
				var s = dr["shortName"].ToString();
				((CObject)Object).Accessories.Rows.Add(new object[] { o, s, "CGoods" });
			}

		}


		private void addDelDataGridExt1_DeleteClick(object sender, EventArgs e)
		{
			DataRow dr = dgAccessories.extendedDataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
		}

		private void Category_ChooseClick(object sender, EventArgs e)
		{
			var fm = new TreeObjectDialog { Owner = MdiParent, RootNode = 22464 };
			if (fm.ShowDialog(this) != DialogResult.OK || fm.SelectedObject == null) return;
			(Object as CGoods).Category = fm.SelectedObject;
			Category.BoundProp = fm.SelectedObject;
		}

		private void Category_DelClick(object sender, EventArgs e)
		{
			(Object as CGoods).Category = ObjectItem.Empty;
			Category.BoundProp = ObjectItem.Empty;
		}

		private void LoadKeywordsButton_Click(object sender, EventArgs e)
		{
			if (!Clipboard.ContainsText())
			{
				ShowError("В буфере обмена нетекстовые данные");
				return;
			}
			var phrases = Clipboard.GetText();
			foreach (var item in phrases.Split("\n".ToCharArray()).Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.Trim()))
			{
				var dr = (Object as CObject).Keywrds.NewRow();
				dr["propValue"] = item;
				(Object as CObject).Keywrds.Rows.Add(dr);
			}
		}

		private void DelKeyword_Click(object sender, EventArgs e)
		{
			DataRow dr = KeywordsGrid.GetSelectedRow();
			if (dr != null) dr.Delete();
		}

		private void AnalogsAddDelDataGrid_AddClick(object sender, EventArgs e)
		{
			var fm = new ListGoodsDialog { ShopType = 1 };
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			foreach (var dr in fm.SelectedRows)
			{
				if (dr == null) return;
				var o = (Guid)dr["OID"];
				var s = dr["shortName"].ToString();
				var ordValue = GetLastAnalogValue();
				((CGoods)Object).Analogs.Rows.Add(new object[] { o, s, "CGoods", ordValue });
			}

		}

		private void AnalogsAddDelDataGrid_DeleteClick(object sender, EventArgs e)
		{
			DataRow dr = AnalogsAddDelDataGrid.extendedDataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
		}

		private void DocsData_DeleteClick(object sender, EventArgs e)
		{
			DataRow dr = DocsDataGrid.extendedDataGrid1.GetSelectedRow();

			if (dr != null)
			{
				var ordValue = (string)dr["ordValue"];
				dr.Delete();
				DataRow drf = ((CObject)Object).Docs.Rows.Find(ordValue);
				if (drf != null)
					drf.Delete();
			}

		}

		private void DocsDataGrid_AddClick(object sender, EventArgs e)
		{
			var fm = new AttachDocs("CGoods", Object.OID);
		    fm.ShowDialog(this);
			DocsData.Rows.Clear();
			foreach (var row in fm.AttachedDocs.Rows.Cast<DataRow>())
			{
				DocsData.Rows.Add(new object[] { row.Field<Guid>("propValue"), row.Field<string>("propValue_NK"), "CBinaryData", row.Field<string>("ordValue") });
			}

		}

	}
}
