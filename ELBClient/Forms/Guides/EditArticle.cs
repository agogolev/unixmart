﻿using System;
using System.Linq;
using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using System.Data;
using Ecommerce.Extensions;

//using System.Threading;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditArticle.
	/// </summary>
	public partial class EditArticle : EditForm
	{
	    public string Header { get; set; }
        private Binding shortLinkBind;
        private const int size = 51200;//50KB
		/// <summary>
		/// Required designer variable.
		/// </summary>


		public EditArticle()
		{
			InitializeComponent();
			Object = new CArticle();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void fmEditArticle_Load(object sender, System.EventArgs e)
		{
			//var picker = new ExtDateTimePicker();
			//tabPage1.Controls.Add(picker);
			//picker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			//picker.LinkedTo = null;
			//picker.Location = new System.Drawing.Point(426, 35);
			//picker.Name = "dtpDate";
			//picker.ReadOnly = false;
			//picker.ShowButtons = true;
			//picker.Size = new System.Drawing.Size(313, 20);
			//picker.TabIndex = 2;
			if (ObjectOID == Guid.Empty)
				InitEmptyObject();
			else
				LoadObject("header,annotation,pubDate,articleType,title,keywords,pageDescription,shortLink", "tags,themes"); //body просим в edit закладке
			LoadLookup();
			BindFields();
			FillGrids();
		}

		private void InitEmptyObject()
		{
			((CArticle)Object).PubDate = DateTime.Today;
            if (MainFrm.IsGroupMember(ELBExpImp.Constants.ContentManagerOID))
			{
				((CArticle)Object).ArticleType = ELBExpImp.Constants.ReviewTypeArticle;
			}
		    if (!string.IsNullOrWhiteSpace(Header))
		    {
		        ((CArticle) Object).Header = Header;
		    }
		}
		private void FillGrids()
		{
			int currentOrd = 0;
			if ((Object as CArticle).Tags.Rows.Count != 0) currentOrd = (from row in (Object as CArticle).Tags.AsEnumerable()
																																	 select row.Field<int>("ordValue")).Max();
			(Object as CArticle).Tags.Columns["ordValue"].AutoIncrement = true;
			(Object as CArticle).Tags.Columns["ordValue"].AutoIncrementSeed = currentOrd + 1;
			Tags.DataSource = (Object as CArticle).Tags;
			ThemesDataGrid.MultiContainer = ((CObject)Object).Themes;
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (((CArticle)Object).PubDate == DateTime.MinValue || ((CArticle)Object).PubDate == DateTimePicker.MinDateTime)
				dtpDate.BoundValue = DateTime.Today;

			try
			{
				SaveObject();
			}
			catch { }
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			if (Object.OID == Guid.Empty)
			{
				MessageBox.Show(this, @"Сначала сохраните объект!");
			}
			else
			{
				EditHtmlDialog fm = new EditHtmlDialog(Object.OID, int.MaxValue);
				if (((CArticle)Object).Header != "") fm.Header = ((CArticle)Object).Header;
				if (!this.Modal)
				{
					fm.MdiParent = this.MdiParent;
					fm.Show();
				}
				else
				{
					fm.ShowDialog(this);
				}
			}
		}

		private void BindFields()
		{
			tbHeader.DataBindings.Add("Text", Object, "Header");
            shortLinkBind = new Binding("Text", Object, "ShortLink");
            ShortLinkText.DataBindings.Add(shortLinkBind);
            tbAnnotation.DataBindings.Add("Text", Object, "Annotation");
			dtpDate.DataBindings.Add("BoundValue", Object, "pubDate");
			TypeOfArticleCombo.DataBindings.Add("SelectedValue", Object, "articleType");
			txtTitle.DataBindings.Add("Text", Object, "Title");
			txtKeywords.DataBindings.Add("Text", Object, "Keywords");
			txtPageDescription.DataBindings.Add("Text", Object, "PageDescription");

		}

		private void LoadLookup()
		{
			string xml = lp.GetLookupValues("CArticle");
			TypeOfArticleCombo.LoadXml(xml, "articleTypeN");
            if (MainFrm.IsGroupMember(ELBExpImp.Constants.ContentManagerOID))
			{
				for (int i = TypeOfArticleCombo.Items.Count - 1; i >= 0; i--)
				{
					var li = TypeOfArticleCombo.Items[i];
					if ((int)li.Value != ELBExpImp.Constants.ReviewTypeArticle) TypeOfArticleCombo.Items.Remove(li);
				}
			}
		}

		private void btnImages_Click(object sender, System.EventArgs e)
		{
			if (Object.OID == Guid.Empty)
				MessageBox.Show(this, @"Сначала сохраните объект!");
			else
			{
				AttachImages4Article fm = new AttachImages4Article(Object);
				fm.Text += " для статьи: " + ((CArticle)Object).Header;
				fm.ShowDialog(this);
			}
		}

		private void label1_Click(object sender, System.EventArgs e)
		{

		}

		public override string ObjectNK
		{
			get
			{
				return (Object as CArticle).Header;
			}
		}

        private void TranslitButton_Click(object sender, EventArgs e)
        {
            string shortLink = ShortLinkText.Text.TrimInner().Trim().TranslitEncode(true).ToLower();
            ((CObject)Object).ShortLink = shortLink;
            shortLinkBind.ReadValue();
        }
    }
}
