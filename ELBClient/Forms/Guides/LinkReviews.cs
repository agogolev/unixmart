﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;



namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmLinkReviews.
	/// </summary>
	public partial class LinkReviews : EditForm
	{
		private int sourceDrag;
		private int firstNode;
		public int FirstNode {
			get {
				return this.firstNode;
			}
			set {
				this.firstNode = value;
			}
		}
		private int secondNode;
		public int SecondNode {
			get {
				return this.secondNode;
			}
			set {
				this.secondNode = value;
			}
		}
		private TreeProvider.TreeProvider treeProvider = Classes.ServiceUtility.TreeProvider;
		private Classes.Tree treeFirst, treeSecond;
		private DataSetISM dsLinks;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public LinkReviews()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void fmLinkMaster_Load(object sender, System.EventArgs e) {
			LoadLinks();
			LoadLookup();
		}
		private void LoadLookup() {
			DataSetISM ds = new DataSetISM();
			DataTable dt = new DataTable("Table");
			ds.Tables.Add(dt);
			dt.Columns.Add("CatalogName", typeof(string));
			dt.Columns.Add("ParentNode", typeof(int));
			dt.Rows.Add(new object[]{"Москва", (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(1)});
			cbFirst.DisplayMember = "CatalogName";
			cbFirst.ValueMember = "ParentNode";
			cbFirst.DataSource = ds.Table;
			DataSetISM ds1 = (DataSetISM)ds.Copy();
			cbSecond.DisplayMember = "CatalogName";
			cbSecond.ValueMember = "ParentNode";
			cbSecond.DataSource = ds1.Table;
		}

		private void LoadFirstCatalog() {
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\""+FirstNode+"\" />";
			DataSet dataSet1 = treeProvider.GetTreeContent(xml);
			Stack nodes = new Stack();
			this.treeFirst = new Tree(0, int.MaxValue);
			nodes.Push(this.treeFirst);

			int prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows) {
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if(nodeID != prevNode) {
					node = new Node((int) dr["lft"], (int) dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty: (Guid) dr["OID"];
					node.objectNK = dr["NK"].ToString();
					node.objectNKRus = dr["label"].ToString();
					node.className = dr["className"].ToString();
					while(nodes.Peek() != null && ((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else {
					if(node != null) {
						node.AddString.Rows.Add(new string[3] {(string)dr["ordValue"], (string)dr["value"], (string)dr["url"]});
						node.AddString.AcceptChanges();
					}
				}
			}		
		}
		private void LoadSecondCatalog() {
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\""+SecondNode+"\" />";
			DataSet dataSet1 = treeProvider.GetTreeContent(xml);
			Stack nodes = new Stack();
			this.treeSecond = new Tree(0, int.MaxValue);
			nodes.Push(this.treeSecond);

			int prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows) {
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if(nodeID != prevNode) {
					node = new Node((int) dr["lft"], (int) dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty: (Guid) dr["OID"];
					node.objectNK = dr["NK"].ToString();
					node.objectNKRus = dr["label"].ToString();
					node.className = dr["className"].ToString();
					while(nodes.Peek() != null && ((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else {
					if(node != null) {
						node.AddString.Rows.Add(new string[3] {(string)dr["ordValue"], (string)dr["value"], (string)dr["url"]});
						node.AddString.AcceptChanges();
					}
				}
			}		
		}
		private void LoadLinks() {
			string sql = @"
SELECT
	pr.OID,
	countReviews = count(*)
FROM 
	t_PressReleases pr
GROUP BY
	pr.OID
";
			dsLinks = new DataSetISM(lp.GetDataSetSql(sql));
		}
		private void LoadFirstTree() {
			tvFirstTree.SuspendLayout();
			try {
				tvFirstTree.Nodes.Clear();
				foreach(Node n in this.treeFirst.Nodes) {
					TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
					tn.Tag = n;//n.objectOID;
					tvFirstTree.Nodes.Add(tn);
					if(n.objectOID != Guid.Empty) LoadLinksInfo(tn);
					LoadNode(tn, n);
				}
			}
			finally {
				tvFirstTree.ResumeLayout();
			}
		}
		private void LoadSecondTree() {
			tvSecondTree.SuspendLayout();
			try {
				tvSecondTree.Nodes.Clear();
				foreach(Node n in this.treeSecond.Nodes) {
					TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
					tn.Tag = n;//n.objectOID;
					tvSecondTree.Nodes.Add(tn);
					if(n.objectOID != Guid.Empty) LoadLinksInfo(tn);
					LoadNode(tn, n);
				}
			}
			finally {
				tvSecondTree.ResumeLayout();
			}
		}

		private void LoadNode(TreeNode treeNode, Node node) {
			foreach(Node n in node.Nodes) {
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;
				treeNode.Nodes.Add(tn);
				if(n.objectOID != Guid.Empty) LoadLinksInfo(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadLinksInfo(TreeNode tn) {
			DataView dv = new DataView(dsLinks.Table, "OID = '"+(tn.Tag as Node).objectOID+"'", null, DataViewRowState.CurrentRows);
			if(dv.Count != 0) {
				int count = (int)dv[0]["countReviews"];
				(tn.Tag as Node).Count = count;
				tn.Text += " ("+count+")";
				tn.ForeColor = Color.Red;
			}
		}

		private void tvSecondTree_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e) {
			Node n = (Node)(e.Item as TreeNode).Tag;
			if(n.Count != 0)	{
				sourceDrag = 2;
				tvSecondTree.DoDragDrop(e.Item, DragDropEffects.Move); 
			}
		}

		private void tvFirstTree_DragOver(object sender, System.Windows.Forms.DragEventArgs e) {
			if(sourceDrag == 1) {
				e.Effect = DragDropEffects.None;			
			}
			else {
				Point pt = tvFirstTree.PointToClient(new Point(e.X, e.Y));
				TreeNode tNode = tvFirstTree.GetNodeAt(pt);
				if(tNode != null) {
					Node dest = (Node)tNode.Tag;
					if(dest.objectOID != Guid.Empty) {
						e.Effect = DragDropEffects.Move;
						tvFirstTree.SelectedNode = tNode;
					}
					else e.Effect = DragDropEffects.None;			
				}
			}
		}

		private void tvFirstTree_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) {
			if(e.Data.GetDataPresent(typeof(TreeNode))) {
				TreeNode source = (TreeNode)e.Data.GetData(typeof(TreeNode));
				Point pt = tvFirstTree.PointToClient(new Point(e.X, e.Y));
				TreeNode dest = tvFirstTree.GetNodeAt(pt);
				Guid dstOID = (dest.Tag as Node).objectOID;
				Guid srcOID = (source.Tag as Node).objectOID;
				string sql = @"
DELETE FROM t_PressReleases WHERE OID = "+ToSqlString(dstOID)+@"
INSERT INTO t_PressReleases (OID, ordValue, articleOID)
SELECT "+ToSqlString(dstOID)+@", ordValue, articleOID
FROM 
	t_PressReleases
WHERE
	OID = "+ToSqlString(srcOID)+@"
SELECT Count(*) FROM t_PressReleases WHERE OID = "+ToSqlString(dstOID)+@"
";
				DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
				if(ds.Table.Rows.Count != 0) {
					if((int)ds.Table.Rows[0][0] != 0) {
						dest.Text = (source.Tag as Node).Name+" ("+ds.Table.Rows[0][0]+")";
						(dest.Tag as Node).Count = (int)ds.Table.Rows[0][0];
						dest.ForeColor = Color.Red;
					}
					else {
						dest.Text = (dest.Tag as Node).Name;
						(dest.Tag as Node).Count = (int)ds.Table.Rows[0][0];
						dest.ForeColor = Color.Black;
					}
				}
				op.ClearCache();
			}
		}

		private void tvFirstTree_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e) {
			Node n = (Node)(e.Item as TreeNode).Tag;
			if(n.Count != 0)	{
				sourceDrag = 1;
				tvFirstTree.DoDragDrop(e.Item, DragDropEffects.Move); 
			}
		}

		private void tvSecondTree_DragOver(object sender, System.Windows.Forms.DragEventArgs e) {
			if(sourceDrag == 2) {
				e.Effect = DragDropEffects.None;	
			}
			else {
				Point pt = tvSecondTree.PointToClient(new Point(e.X, e.Y));
				TreeNode tNode = tvSecondTree.GetNodeAt(pt);
				if(tNode != null) {
					Node dest = (Node)tNode.Tag;
					if(dest.objectOID != Guid.Empty) {
						e.Effect = DragDropEffects.Move;
						tvSecondTree.SelectedNode = tNode;
					}
					else e.Effect = DragDropEffects.None;			
				}
			}
		}

		private void tvSecondTree_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) {
			if(e.Data.GetDataPresent(typeof(TreeNode))) {
				TreeNode source = (TreeNode)e.Data.GetData(typeof(TreeNode));
				Point pt = tvSecondTree.PointToClient(new Point(e.X, e.Y));
				TreeNode dest = tvSecondTree.GetNodeAt(pt);
				Guid dstOID = (dest.Tag as Node).objectOID;
				Guid srcOID = (source.Tag as Node).objectOID;
				string sql = @"
DELETE FROM t_PressReleases WHERE OID = "+ToSqlString(dstOID)+@"
INSERT INTO t_PressReleases (OID, ordValue, articleOID)
SELECT "+ToSqlString(dstOID)+@", ordValue, articleOID
FROM 
	t_PressReleases
WHERE
	OID = "+ToSqlString(srcOID)+@"
SELECT Count(*) FROM t_PressReleases WHERE OID = "+ToSqlString(dstOID)+@"
";
				DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
				if(ds.Table.Rows.Count != 0) {
					if((int)ds.Table.Rows[0][0] != 0) {
						dest.Text = (source.Tag as Node).Name+" ("+ds.Table.Rows[0][0]+")";
						(dest.Tag as Node).Count = (int)ds.Table.Rows[0][0];
						dest.ForeColor = Color.Red;
					}
					else {
						dest.Text = (dest.Tag as Node).Name;
						(dest.Tag as Node).Count = (int)ds.Table.Rows[0][0];
						dest.ForeColor = Color.Black;
					}
				}
				op.ClearCache();
			}
		}

		private void tvSecondTree_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) {
			TreeNode node = tvSecondTree.GetNodeAt(new Point(e.X, e.Y));
			if (node != null) {
				tvSecondTree.SelectedNode = node;
			}
		}

		private void button1_Click(object sender, System.EventArgs e) {
			FirstNode = (int)cbFirst.SelectedValue;
			LoadFirstCatalog();
			LoadFirstTree();
		
		}

		private void button2_Click(object sender, System.EventArgs e) {
			SecondNode = (int)cbSecond.SelectedValue;
			LoadSecondCatalog();
			LoadSecondTree();
		}

		private void tvFirstTree_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) {
			TreeNode node = tvFirstTree.GetNodeAt(new Point(e.X, e.Y));
			if (node != null) {
				tvFirstTree.SelectedNode = node;
			}
		}

		private void label1_DragOver(object sender, System.Windows.Forms.DragEventArgs e) {
			e.Effect = DragDropEffects.Move;
		}

		private void label1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) {
			if(e.Data.GetDataPresent(typeof(TreeNode))) {
				TreeNode source = (TreeNode)e.Data.GetData(typeof(TreeNode));
				Guid srcOID = (source.Tag as Node).objectOID;
				string sql = @"
DELETE FROM t_PressReleases WHERE OID = "+ToSqlString(srcOID)+@"
SELECT Count(*) FROM t_PressReleases WHERE OID = "+ToSqlString(srcOID)+@"
";
				DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
				if(ds.Table.Rows.Count != 0) {
					source.Text = (source.Tag as Node).Name+" ("+ds.Table.Rows[0][0]+")";
					if((int)ds.Table.Rows[0][0] != 0) {
						source.Text = (source.Tag as Node).Name+" ("+ds.Table.Rows[0][0]+")";
						(source.Tag as Node).Count = (int)ds.Table.Rows[0][0];
						source.ForeColor = Color.Red;
					}
					else {
						source.Text = (source.Tag as Node).Name;
						(source.Tag as Node).Count = (int)ds.Table.Rows[0][0];
						source.ForeColor = Color.Black;
					}
				}
				op.ClearCache();
			}
		}

	}
}
