﻿using System;
using System.Data;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;

namespace ELBClient.Forms.Guides
{
	public partial class EditParamType : EditForm
	{
		private DataSet dsList;
		
		public EditParamType()
		{
			InitializeComponent();
			Object = new CParamType();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch{}
			//this.Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void BindFields()
		{
			txtName.DataBindings.Add("Text", Object, "Name");
			cbParamType.DataBindings.Add("SelectedValue", Object, "ParamType");
			txtUnit.DataBindings.Add("Text", Object, "Unit");
			cbIsDomain.DataBindings.Add("Checked", Object, "IsDomain");
			cbIsMandatory.DataBindings.Add("Checked", Object, "IsMandatory");
			cbIsFullName.DataBindings.Add("Checked", Object, "IsFullName");
		}

		private void FillGrid() {
			ListProvider.ListProvider lprov = Classes.ServiceUtility.ListProvider;
			dgList.CreateAuxFilter("paramType", "paramType_filter", MetaData.FilterVerb.Equal, false, new object[]{ObjectOID});
			dsList = lprov.GetList(dgList.GetDataXml().OuterXml);
			DataView dv = new DataView(dsList.Tables[0], null, "value", DataViewRowState.CurrentRows);
			dgList.SetDataBinding(dv, null);
		}
		private void fmEditTheme_Load(object sender, System.EventArgs e)
		{
			LoadLookup();
			LoadObject("name,paramType", null);
			BindFields();
			FillGrid();
		}
		private void LoadLookup() {
			string xml = lp.GetLookupValues("CParamType");
			cbParamType.LoadXml(xml, "paramTypeN");
		}

		private void cbIsDomain_CheckedChanged(object sender, System.EventArgs e) {
			lbList.Visible = cbIsDomain.Checked;
			dgList.Visible = cbIsDomain.Checked;
			btnAddValue.Visible = cbIsDomain.Checked;
			btnDelValue.Visible = cbIsDomain.Checked;
			btnCheckValues.Visible = cbIsDomain.Checked;
		}

		private void btnAddValue_Click(object sender, System.EventArgs e) {
			EditStringItem fm = new EditStringItem();
			fm.ParamType = new ObjectItem(ObjectOID, txtName.Text, "CParamType");
			fm.ReloadGrid = new ReloadDelegate(FillGrid);
			if(fm.ShowDialog(this) == DialogResult.OK) {
			}
		}

		private void btnDelValue_Click(object sender, System.EventArgs e) {
			DataRow dr = dgList.GetSelectedRow();
			if(dr != null) {
				ObjectProvider.ObjectProvider prov = Classes.ServiceUtility.ObjectProvider;
				ObjectProvider.ExceptionISM exISM = prov.DeleteObject((Guid) dr["OID"]);
				if(exISM != null)
					ShowError("Ошибка удаления: " + exISM.LiteralMessage);
				FillGrid();
			}
		}

		private void btnCheckValues_Click(object sender, System.EventArgs e) {
			DataRow dr = dgList.GetSelectedRow();
			ListGoodsWithParam fm = new ListGoodsWithParam();
			fm.ParamType = ObjectOID;
			fm.CheckValue = (string)dr["value"];
			fm.MdiParent = this.MdiParent;
			fm.Show();
		}
	}
}
