using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListAction
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
			this.extendedDataGridComboBoxColumn1 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "dd.MM.yyyy";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Начало";
			this.formattableTextBoxColumn2.MappingName = "dateBegin";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "dd.MM.yyyy";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Конец";
			this.formattableTextBoxColumn3.MappingName = "dateEnd";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = "description?header";
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "Описание";
			this.formattableTextBoxColumn4.MappingName = "description";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn4, this.menuFilterSort1);
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// formattableTextBoxColumn5
			// 
			this.formattableTextBoxColumn5.FieldName = "commodityGroup?Name";
			this.formattableTextBoxColumn5.FilterFieldName = null;
			this.formattableTextBoxColumn5.Format = "";
			this.formattableTextBoxColumn5.FormatInfo = null;
			this.formattableTextBoxColumn5.HeaderText = "Группа товаров";
			this.formattableTextBoxColumn5.MappingName = "commodityGroup";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn5, this.menuFilterSort1);
			this.formattableTextBoxColumn5.Width = 75;
			// 
			// formattableBooleanColumn1
			// 
			this.formattableBooleanColumn1.FieldName = null;
			this.formattableBooleanColumn1.FilterFieldName = null;
			this.formattableBooleanColumn1.HeaderText = "Активность";
			this.formattableBooleanColumn1.MappingName = "isPublic";
			this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn1, this.menuFilterSort1);
			this.formattableBooleanColumn1.Width = 75;
			// 
			// extendedDataGridComboBoxColumn1
			// 
			this.extendedDataGridComboBoxColumn1.FieldName = null;
			this.extendedDataGridComboBoxColumn1.FilterFieldName = null;
			this.extendedDataGridComboBoxColumn1.Format = "";
			this.extendedDataGridComboBoxColumn1.FormatInfo = null;
			this.extendedDataGridComboBoxColumn1.HeaderText = "Тип акции";
			this.extendedDataGridComboBoxColumn1.MappingName = "actionType";
			this.columnMenuExtender1.SetMenu(this.extendedDataGridComboBoxColumn1, this.menuFilterSort1);
			this.extendedDataGridComboBoxColumn1.Width = 75;
			// 
			// mainDataGrid1
			// 
			this.mainDataGrid1.AdditionalInfo = "";
			this.mainDataGrid1.ClassName = "CAction";
			this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
			this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.mainDataGrid1.Name = "mainDataGrid1";
			this.mainDataGrid1.Size = new System.Drawing.Size(542, 370);
			this.mainDataGrid1.TabIndex = 0;
			this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.formattableBooleanColumn1,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5,
            this.extendedDataGridComboBoxColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// ListAction
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 370);
			this.Controls.Add(this.mainDataGrid1);
			this.MinimumSize = new System.Drawing.Size(550, 404);
			this.Name = "ListAction";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Акции";
			this.Load += new System.EventHandler(this.fmListGroup_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn1;
	}
}
