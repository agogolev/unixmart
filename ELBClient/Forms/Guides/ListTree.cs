﻿namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListTree.
	/// </summary>
	public partial class ListTree : ListForm
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ListTree()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmListTree_Load(object sender, System.EventArgs e) {
			//mainDataGrid1.dataGridISM1.CreateAuxFilter("isGeneric", "f1", FilterVerb.Equal, false, new object[]{1});
			mainDataGrid1.LoadData();
		}
	}
}
