using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditAction
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel3;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.GeoTargetingTreeView = new System.Windows.Forms.TreeView();
			this.panel3 = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.GeoExclusive = new System.Windows.Forms.RadioButton();
			this.GeoInclusive = new System.Windows.Forms.RadioButton();
			this.ActionTypeSelect = new ColumnMenuExtender.DataBoundComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.CommodityGroupSelector = new ELBClient.UserControls.SelectorTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.DescriptionSelector = new ELBClient.UserControls.SelectorTextBox();
			this.IsPublicCheck = new System.Windows.Forms.CheckBox();
			this.DateEndDTP = new ELBClient.UserControls.ExtDateTimePicker();
			this.label10 = new System.Windows.Forms.Label();
			this.NameText = new System.Windows.Forms.TextBox();
			this.DateBeginDTP = new ELBClient.UserControls.ExtDateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.panel2.SuspendLayout();
			this.panel5.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.panel3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel4
			// 
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(200, 100);
			this.panel4.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel5);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(685, 778);
			this.panel2.TabIndex = 3;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.tabControl1);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel5.Location = new System.Drawing.Point(0, 337);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(685, 404);
			this.panel5.TabIndex = 34;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(685, 404);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.GeoTargetingTreeView);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(677, 378);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Геотаргетинг";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// GeoTargetingTreeView
			// 
			this.GeoTargetingTreeView.CheckBoxes = true;
			this.GeoTargetingTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GeoTargetingTreeView.Location = new System.Drawing.Point(0, 0);
			this.GeoTargetingTreeView.Name = "GeoTargetingTreeView";
			this.GeoTargetingTreeView.Size = new System.Drawing.Size(677, 378);
			this.GeoTargetingTreeView.TabIndex = 0;
			this.GeoTargetingTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.GeoTargetingTree_AfterCheck);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.groupBox1);
			this.panel3.Controls.Add(this.ActionTypeSelect);
			this.panel3.Controls.Add(this.label4);
			this.panel3.Controls.Add(this.label2);
			this.panel3.Controls.Add(this.CommodityGroupSelector);
			this.panel3.Controls.Add(this.label1);
			this.panel3.Controls.Add(this.DescriptionSelector);
			this.panel3.Controls.Add(this.IsPublicCheck);
			this.panel3.Controls.Add(this.DateEndDTP);
			this.panel3.Controls.Add(this.label10);
			this.panel3.Controls.Add(this.NameText);
			this.panel3.Controls.Add(this.DateBeginDTP);
			this.panel3.Controls.Add(this.label5);
			this.panel3.Controls.Add(this.label9);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(685, 337);
			this.panel3.TabIndex = 33;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.GeoExclusive);
			this.groupBox1.Controls.Add(this.GeoInclusive);
			this.groupBox1.Location = new System.Drawing.Point(344, 246);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(319, 84);
			this.groupBox1.TabIndex = 60;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Опции геотаргетинга";
			// 
			// GeoExclusive
			// 
			this.GeoExclusive.AutoSize = true;
			this.GeoExclusive.Location = new System.Drawing.Point(6, 58);
			this.GeoExclusive.Name = "GeoExclusive";
			this.GeoExclusive.Size = new System.Drawing.Size(188, 17);
			this.GeoExclusive.TabIndex = 1;
			this.GeoExclusive.TabStop = true;
			this.GeoExclusive.Text = "Все регионы кроме указанных";
			this.GeoExclusive.UseVisualStyleBackColor = true;
			// 
			// GeoInclusive
			// 
			this.GeoInclusive.AutoSize = true;
			this.GeoInclusive.Location = new System.Drawing.Point(6, 30);
			this.GeoInclusive.Name = "GeoInclusive";
			this.GeoInclusive.Size = new System.Drawing.Size(227, 17);
			this.GeoInclusive.TabIndex = 0;
			this.GeoInclusive.TabStop = true;
			this.GeoInclusive.Text = "Включить только указанные регионы";
			this.GeoInclusive.UseVisualStyleBackColor = true;
			this.GeoInclusive.CheckedChanged += new System.EventHandler(this.GeoInclusive_CheckedChanged);
			// 
			// ActionTypeSelect
			// 
			this.ActionTypeSelect.Location = new System.Drawing.Point(130, 246);
			this.ActionTypeSelect.Name = "ActionTypeSelect";
			this.ActionTypeSelect.SelectedValue = -1;
			this.ActionTypeSelect.Size = new System.Drawing.Size(208, 21);
			this.ActionTypeSelect.TabIndex = 58;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(10, 247);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 18);
			this.label4.TabIndex = 59;
			this.label4.Text = "Тип акции:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(10, 207);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 17);
			this.label2.TabIndex = 57;
			this.label2.Text = "Группа товаров:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CommodityGroupSelector
			// 
			this.CommodityGroupSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CommodityGroupSelector.BoundProp = null;
			this.CommodityGroupSelector.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.CommodityGroupSelector.ClassName = "CCommodityGroup";
			this.CommodityGroupSelector.FieldNames = "name";
			this.CommodityGroupSelector.FormatRows = null;
			this.CommodityGroupSelector.HeaderNames = "Название";
			this.CommodityGroupSelector.Location = new System.Drawing.Point(130, 207);
			this.CommodityGroupSelector.Name = "CommodityGroupSelector";
			this.CommodityGroupSelector.RowNames = null;
			this.CommodityGroupSelector.Size = new System.Drawing.Size(533, 23);
			this.CommodityGroupSelector.TabIndex = 56;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(10, 167);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 18);
			this.label1.TabIndex = 55;
			this.label1.Text = "Описание:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DescriptionSelector
			// 
			this.DescriptionSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DescriptionSelector.BoundProp = null;
			this.DescriptionSelector.ButtonBackColor = System.Drawing.SystemColors.Control;
			this.DescriptionSelector.ClassName = "CArticle";
			this.DescriptionSelector.FieldNames = "header,pubDate";
			this.DescriptionSelector.FormatRows = null;
			this.DescriptionSelector.HeaderNames = "Название,Дата публикации";
			this.DescriptionSelector.Location = new System.Drawing.Point(130, 167);
			this.DescriptionSelector.Name = "DescriptionSelector";
			this.DescriptionSelector.RowNames = null;
			this.DescriptionSelector.Size = new System.Drawing.Size(533, 25);
			this.DescriptionSelector.TabIndex = 54;
			// 
			// IsPublicCheck
			// 
			this.IsPublicCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.IsPublicCheck.Location = new System.Drawing.Point(10, 123);
			this.IsPublicCheck.Name = "IsPublicCheck";
			this.IsPublicCheck.Size = new System.Drawing.Size(136, 25);
			this.IsPublicCheck.TabIndex = 53;
			this.IsPublicCheck.Text = "Опубликовано:";
			this.IsPublicCheck.UseVisualStyleBackColor = true;
			// 
			// DateEndDTP
			// 
			this.DateEndDTP.BoundValue = new System.DateTime(2012, 12, 29, 15, 14, 28, 990);
			this.DateEndDTP.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.DateEndDTP.LinkedTo = null;
			this.DateEndDTP.Location = new System.Drawing.Point(130, 85);
			this.DateEndDTP.Name = "DateEndDTP";
			this.DateEndDTP.ReadOnly = false;
			this.DateEndDTP.ShowButtons = true;
			this.DateEndDTP.Size = new System.Drawing.Size(208, 20);
			this.DateEndDTP.TabIndex = 51;
			this.DateEndDTP.Value = new System.DateTime(2012, 12, 29, 15, 14, 28, 990);
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(10, 87);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(112, 18);
			this.label10.TabIndex = 52;
			this.label10.Text = "Конец:";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// NameText
			// 
			this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.NameText.Location = new System.Drawing.Point(130, 12);
			this.NameText.Name = "NameText";
			this.NameText.Size = new System.Drawing.Size(533, 20);
			this.NameText.TabIndex = 47;
			// 
			// DateBeginDTP
			// 
			this.DateBeginDTP.BoundValue = new System.DateTime(2012, 12, 29, 15, 14, 29, 131);
			this.DateBeginDTP.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.DateBeginDTP.LinkedTo = null;
			this.DateBeginDTP.Location = new System.Drawing.Point(130, 48);
			this.DateBeginDTP.Name = "DateBeginDTP";
			this.DateBeginDTP.ReadOnly = false;
			this.DateBeginDTP.ShowButtons = true;
			this.DateBeginDTP.Size = new System.Drawing.Size(208, 20);
			this.DateBeginDTP.TabIndex = 48;
			this.DateBeginDTP.Value = new System.DateTime(2012, 12, 29, 15, 14, 29, 131);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(10, 51);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(112, 18);
			this.label5.TabIndex = 49;
			this.label5.Text = "Начало:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(10, 14);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(72, 19);
			this.label9.TabIndex = 50;
			this.label9.Text = "Заголовок:";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 741);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(685, 37);
			this.panel1.TabIndex = 32;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(4, 5);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.Location = new System.Drawing.Point(607, 5);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 9;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// EditAction
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(685, 778);
			this.Controls.Add(this.panel2);
			this.Name = "EditAction";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование акции";
			this.Load += new System.EventHandler(this.EditAction_Load);
			this.panel2.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		private TextBox NameText;
		private UserControls.ExtDateTimePicker DateBeginDTP;
		private Label label5;
		private Label label9;
		private CheckBox IsPublicCheck;
		private UserControls.ExtDateTimePicker DateEndDTP;
		private Label label10;
		private Label label2;
		private UserControls.SelectorTextBox CommodityGroupSelector;
		private Label label1;
		private UserControls.SelectorTextBox DescriptionSelector;
		private ColumnMenuExtender.DataBoundComboBox ActionTypeSelect;
		private Label label4;
        private Panel panel5;
        private TabControl tabControl1;
        private TabPage tabPage3;
        private TreeView GeoTargetingTreeView;
        private GroupBox groupBox1;
        private RadioButton GeoExclusive;
        private RadioButton GeoInclusive;
	}
}
