﻿using System.Linq;
using System.Collections;
using ELBClient.Classes;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using System;

//using System.Threading;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditArticle.
	/// </summary>
	public partial class EditAction : EditForm
	{
		private const int size = 51200;//50KB
		/// <summary>
		/// Required designer variable.
		/// </summary>

        private readonly Tree regionsTree = new Tree(0, int.MaxValue);
        private readonly TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
        private DataSet regionsDataSet;

		public EditAction()
		{
			InitializeComponent();
			Object = new CAction();
		}

		private void EditAction_Load(object sender, System.EventArgs e)
		{            
			LoadLookup();
            LoadRegions();
            BuildTrees();
            LoadObject("name,dateBegin,dateEnd,isPublic,description,commodityGroup,actionType,geoInclusive", "locations"); //body просим в edit закладке
			BindFields();
            LoadTrees();
		}

		private void LoadLookup()
		{
			var xml = lp.GetLookupValues("CAction");
			ActionTypeSelect.LoadXml(xml, "actionTypeN");
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch { }
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void BindFields()
		{
			NameText.DataBindings.Add("Text", Object, "Name");
			DateBeginDTP.DataBindings.Add("BoundValue", Object, "DateBegin");
			DateEndDTP.DataBindings.Add("BoundValue", Object, "DateEnd");
			IsPublicCheck.DataBindings.Add("Checked", Object, "IsPublic");
			DescriptionSelector.DataBindings.Add("BoundProp", Object, "Description");
			CommodityGroupSelector.DataBindings.Add("BoundProp", Object, "CommodityGroup");
			ActionTypeSelect.DataBindings.Add("SelectedValue", Object, "ActionType");
            if ((Object as CAction).GeoInclusive) GeoInclusive.Checked = true;
            else GeoExclusive.Checked = true;
            GeoInclusive.CheckedChanged += GeoInclusive_CheckedChanged;
		}

		public override string ObjectNK
		{
			get
			{
				return (Object as CAction).Name;
			}
		}

        protected override void SaveObject()
        {
            try
            {
                var item = Object as CAction;
                if (LocationIsModified())
                {
                    for (var i = item.Locations.Rows.Count - 1; i >= 0; i--) item.Locations.Rows[i].Delete();
                    var items = GetNewLocation();
                    if (items.Count == 0 || (items.Count == 1 && items[0].objectOID == Guid.Empty)) { }
                    else
                    {
                        items
                            .ToList()
                            .ForEach(n => item.Locations.Rows.Add(new object[] { n.objectOID }));
                    }
                }
                base.SaveObject();
            }
            catch (Exception e)
            {
                ShowError(e.Message);
            }
        }


        //Geo

        private void BuildTrees()
        {
            DoBuildTree(regionsDataSet, regionsTree);
        }

        private void DoBuildTree(DataSet dataSet, Tree tree)
        {
            var nodes = new Stack();
            nodes.Push(tree);

            foreach (DataRow dr in dataSet.Tables["table"].Rows)
            {
                var nodeID = (int)dr["nodeID"];
                var node = new Node((int)dr["lft"], (int)dr["rgt"])
                {
                    NodeId = nodeID,
                    Name = dr["nodeName"].ToString(),
                    objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"]
                };
                while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
                ((NodeContainer)nodes.Peek()).Nodes.Add(node);
                if ((node.Left + 1) != node.Right)
                {
                    nodes.Push(node);
                }
            }
        }

        private void LoadTrees()
        {
            DoLoadTree(GeoTargetingTreeView, regionsTree, (Object as CAction).Locations.AsEnumerable().ToList());
        }

        private void DoLoadTree(TreeView treeView, Tree tree, IList<DataRow> multiContainer)
        {
            treeView.Nodes.Clear();
            foreach (Node n in tree.Nodes)
            {
                var tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : "<Unknown>");
                var OID = n.objectOID;
                var nodeChecked = false;
                if (OID != Guid.Empty && multiContainer.Count(row => row.Field<Guid>("propValue") == OID) != 0) nodeChecked = true;
                else if (OID == Guid.Empty && !multiContainer.Any()) nodeChecked = true;
                tn.Checked = nodeChecked;
                tn.Tag = n;//n.objectOID;
                treeView.Nodes.Add(tn);
                LoadNode(tn, n, multiContainer);
            }
        }

        private void LoadNode(TreeNode treeNode, Node node, IList<DataRow> locations)
        {
            foreach (Node n in node.Nodes)
            {
                var tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : "<Unknown>");
                var OID = n.objectOID;
                var nodeChecked = false;
                if (locations != null)
                {
                    var count = locations.Count(row => row.Field<Guid>("propValue") == OID);
                    if ((OID != Guid.Empty && count != 0)) nodeChecked = true;
                    else if ((OID == Guid.Empty && !locations.Any())) nodeChecked = true;
                }
                tn.Checked = nodeChecked;
                tn.Tag = n;//n.objectOID;
                treeNode.Nodes.Add(tn);
                LoadNode(tn, n, locations);
            }
        }

        private IList<Node> GetNewLocation()
        {
            var selectedItems = new List<Node>();
            foreach (TreeNode n1 in GeoTargetingTreeView.Nodes)
            {
                if (n1.Checked && ((Node)n1.Tag).objectOID != Guid.Empty) selectedItems.Add((Node)n1.Tag);
                ProcessChild(selectedItems, n1);
            }
            return selectedItems;
        }

        private void ProcessChild(List<Node> selectedItems, TreeNode n)
        {
            foreach (TreeNode n1 in n.Nodes)
            {
                if (n1.Checked) selectedItems.Add((Node)n1.Tag);
                ProcessChild(selectedItems, n1);
            }
        }

        private void GeoInclusive_CheckedChanged(object sender, EventArgs e)
        {
            var item = Object as CAction;
            item.GeoInclusive = GeoInclusive.Checked;
        }

        private void GeoTargetingTree_AfterCheck(object sender, TreeViewEventArgs e)
        {
            var item = e.Node;
            if (item == null) return;
            if (item.Checked)
            {
                var parentNode = item.Parent;
                while (parentNode != null)
                {
                    parentNode.Checked = false;
                    parentNode = parentNode.Parent;
                }
                RemoveCheckedStateChild(item);
            }
        }

        private void RemoveCheckedStateChild(TreeNode item)
        {
            foreach (TreeNode tn in item.Nodes)
            {
                tn.Checked = false;
                RemoveCheckedStateChild(tn);
            }
        }

        private void LoadRegions()
        {
            //Filling OID
            const string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree OID = \"3094c215-57e7-4623-b4b9-6c93914e8c88\" />";

            regionsDataSet = treeProvider.GetTreeContent(xml);
        }

        protected override bool IsModified
        {
            get
            {
                return LocationIsModified() || base.IsModified;
            }
        }
        private bool LocationIsModified()
        {
            var selectedItems = GetNewLocation();
            var obj = Object as CAction;

            if (selectedItems.Count(n => n.objectOID != Guid.Empty) != obj.Locations.Rows.Count) return true;
            var isModified = false;
            selectedItems.ToList().ForEach(n =>
            {
                if (obj.Locations.AsEnumerable().Count(row => row.Field<Guid>("propValue") == n.objectOID) == 0) isModified = true;
            });
            return isModified;
        }
	}
}
