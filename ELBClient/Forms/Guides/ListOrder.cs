﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ColumnMenuExtender;
using Ecommerce.Extensions;
using MetaData;

namespace ELBClient.Forms.Guides
{
    /// <summary>
    ///     Summary description for fmListOrder.
    /// </summary>
    public partial class ListOrder : ListForm
    {
        /// <summary>
        ///     Required designer variable.
        /// </summary>
        public ListOrder()
        {
            InitializeComponent();
            dgOrders.GetForeColorForRow += extendedDataGridComboBoxColumn1_OnGetForeColorForRow;
        }

        public bool HasWholesale { get; set; }
        public bool ShowWholesale { get; set; }
        public bool IsDiscount { get; set; }
        public int ShopType { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsOperator { get; set; }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>

        #endregion
        public override void LoadData()
        {
            LoadFilter();
            DataSet ds = lp.GetList(dgOrders.GetDataXml().OuterXml);
            dgOrders.SetDataBinding(ds, "Table");
            dgOrders.DisableNewDel();
            base.LoadData();
        }

        private void fmListOrder_Load(object sender, EventArgs e)
        {
            dtpDateBegin.Value = DateTime.Today;
            dtpDateEnd.Value = DateTime.Today;
            LoadLookup();
            ModifyColumnDefinition();
            LoadData();
            dtpDateBegin.ValueChanged += dtpDateBegin_ValueChanged;
            dtpDateEnd.ValueChanged += dtpDateEnd_ValueChanged;
            cbShopType.SelectedIndexChanged += cbShopType_SelectedIndexChanged;
        }

        private void ModifyColumnDefinition()
        {
            if (!IsAdmin)
            {
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colSourceType);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colShopType);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colOrderItemCount);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colOrderTotalPrice);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colDeliveryPrice);
            }
            if (!IsOperator)
            {
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colPerson);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colPhone);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colComment);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colWikiM);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colCity);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colIsSendToSv);
                extendedDataGridTableStyle1.GridColumnStyles.Remove(colEMail);
            }
        }

        private void LoadLookup()
        {
            string xml = lp.GetLookupValues("COrder");
            colStatus.ComboBox.LoadXml(xml, "statusType");
            colDeliveryPeriod.ComboBox.LoadXml(xml, "deliveryPeriodN");
            colTypePay.ComboBox.LoadXml(xml, "typePaySV");
            colSourceType.ComboBox.LoadXml(xml, "sourceTypeN");
            colShopType.ComboBox.LoadXml(xml, "shopTypeN");
            colDocType.ComboBox.LoadXml(xml, "docTypeN");
            if (ShopType == 0)
            {
                //xml = GetResourceString("fmListOrderLookup.xml");
                //colShopType.ComboBox.LoadXml(xml, "shopType");
                cbShopType.Items.Clear();
                cbShopType.LoadXml(xml, "shopTypeN");
                cbShopType.InsertItem(0, new ListItem(0, "Все"));
                cbShopType.SelectedIndex = 0;
                lbShopType.Visible = true;
                cbShopType.Visible = true;
                //colShopType.Width = 100;
            }
            else
            {
                lbShopType.Visible = false;
                cbShopType.Visible = false;
                //colShopType.Width = 0;
            }
        }

        public void LoadFilter()
        {
            dgOrders.StorableFilters.Keys
                .OfType<string>()
                .Where(s => extendedDataGridTableStyle1
                    .GridColumnStyles
                    .OfType<DataGridColumnStyle>()
                    .All(cs => string.Compare(cs.MappingName, s, StringComparison.CurrentCultureIgnoreCase) != 0))
                .ToList().ForEach(s => dgOrders.StorableFilters.Remove(s));

            dgOrders.Filters.Clear();
            dgOrders.FilterString = "";
            if (!cbShowAll.Checked)
            {
                dgOrders.CreateAuxFilter("operator", "f1_op", FilterVerb.Equal, false,
                    new object[] {(((MainFrm) MdiParent) ?? ((MainFrm) Owner)).Manager});
                dgOrders.CreateAuxFilter("operator", "f2_op", FilterVerb.IsNull, false, new object[] {});
                dgOrders.FilterString = "(f1_op OR f2_op)";
            }
            if (ShopType != 0)
            {
                dgOrders.CreateAuxFilter("shopType", "fshoptype", FilterVerb.Equal, false, new object[] {ShopType});
                if (dgOrders.FilterString.Length != 0) dgOrders.FilterString += " AND ";
                dgOrders.FilterString += "fshoptype";
            }
            else if (cbShopType.SelectedIndex != 0)
            {
                dgOrders.CreateAuxFilter("shopType", "fshoptype", FilterVerb.Equal, false,
                    new object[] {cbShopType.SelectedValue});
                if (dgOrders.FilterString.Length != 0) dgOrders.FilterString += " AND ";
                dgOrders.FilterString += "fshoptype";
            }
            dgOrders.CreateAuxFilter("dateCreate", "datecreate_filter", FilterVerb.Between, false,
                new object[] {dtpDateBegin.Value, dtpDateEnd.Value.AddDays(1).AddSeconds(-1)});
            if (dgOrders.FilterString != "") dgOrders.FilterString += " AND ";
            dgOrders.FilterString += "datecreate_filter";

            if (HasWholesale)
            {
                if (ShowWholesale)
                {
                    Guid manager = (((MainFrm) MdiParent) ?? ((MainFrm) Owner)).Manager;
                    dgOrders.CreateAuxFilter("peopleOID.manager", "peoplemanager_filter", FilterVerb.Equal, false,
                        new object[] {manager});
                    if (dgOrders.FilterString != "") dgOrders.FilterString += " AND ";
                    dgOrders.FilterString += "peoplemanager_filter";
                }
                else
                {
                    dgOrders.CreateAuxFilter("peopleOID.manager", "peoplemanager_filter", FilterVerb.IsNull, false, null);
                    if (dgOrders.FilterString != "") dgOrders.FilterString += " AND ";
                    dgOrders.FilterString += "peopleManager_filter";
                }
            }
        }

        private void cbShowAll_CheckedChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private Brush extendedDataGridComboBoxColumn1_OnGetForeColorForRow(int rowNum, CurrencyManager source)
        {
            var dv = (DataView) source.List;
            DataRowView dr = dv[rowNum];
            Brush br = null;
            switch ((int) dr["status"])
            {
                case 1:
                    if ((string) dr["operator_NK"] == "") br = new SolidBrush(Color.Green);
                    break;
                case 4:
                    br = new SolidBrush(Color.Red);
                    break;
                case 6:
                    br = new SolidBrush(Color.Gray);
                    break;
                case 7:
                    br = new SolidBrush(Color.DarkGray);
                    break;
            }
            return br;
        }

        private void cbShopType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CreateNewOrder(int shopType)
        {
            var fm = new EditOrder {MdiParent = MdiParent, ShopType = shopType};
            fm.ReloadGrid += LoadData;
            fm.Show();
        }

        private void dtpDateBegin_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dtpDateEnd_ValueChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgOrders_DoubleClick(object sender, EventArgs e)
        {
            if (dgOrders.Hit.Type != DataGrid.HitTestType.Cell && dgOrders.Hit.Type != DataGrid.HitTestType.RowHeader)
                return;
            DataRow dr = dgOrders.GetSelectedRow();
            if (dr == null) return;
            var fm = new EditOrder {ObjectOID = (Guid) dr["OID"], MdiParent = MdiParent, HasWholesale = HasWholesale};
            fm.ReloadGrid += LoadData;
            fm.Show();
        }

        private void dgOrders_ActionKeyPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dgOrders_DoubleClick(sender, EventArgs.Empty);
            }
        }

        private void btnNew_MouseUp(object sender, MouseEventArgs e)
        {
            SelectDomainMenuStrip.Show(btnNew.PointToScreen(e.Location));
        }

        private void wwwelectroburgruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewOrder(1);
        }

        private void spbelectroburgruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewOrder(2);
        }

        private void nskelectroburgruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewOrder(3);
        }

        private void kdrelectroburgruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewOrder(4);
        }

        private void SaveListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SaveList.ShowDialog(this) == DialogResult.OK)
            {
                var re = new Regex(@"\s+", RegexOptions.Singleline);
                var sb = new StringBuilder();
                var items = (DataSet) dgOrders.DataSource;
                using (var wrt = new StreamWriter(SaveList.FileName, false, Encoding.GetEncoding(1251)))
                {
                    wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
                        .Select(column => re.Replace(column.HeaderText, " "))
                        .Intersperse("\t")
                        .Aggregate(sb, (builder, str) =>
                        {
                            builder.Append(str);
                            return builder;
                        }, builder =>
                        {
                            string result = builder.ToString();
                            builder.Length = 0;
                            return result;
                        }));
                    items.Tables[0].AsEnumerable().ToList().ForEach(row => wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
                        .Select(column => re.Replace(row[column.MappingName].ToString(), " "))
                        .Intersperse("\t")
                        .Aggregate(sb, (builder, str) =>
                        {
                            builder.Append(str);
                            return builder;
                        }, builder =>
                        {
                            string result = builder.ToString();
                            builder.Length = 0;
                            return result;
                        })));
                }
            }
        }

        private void dgOrders_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (dgOrders.Hit.Type == DataGrid.HitTestType.Cell)
                {
                    contextMenuStrip1.Show(dgOrders, new Point(e.X, e.Y));
                }
            }
        }

        private void dgOrders_Reload(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}