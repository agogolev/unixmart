using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using MetaData;

namespace ELBClient.Forms.Guides
{
	public partial class EditMasterCatalog
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
		private Panel panel2;
		private ColumnMenuExtender.DataGridISM AddPrices;
		private System.Windows.Forms.TextBox txtName;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;

		private void InitializeComponent()
		{
            this.panel2 = new System.Windows.Forms.Panel();
            this.IDText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.AddPricesDelivery = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn2 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.AddPrices = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.extendedDataGridComboBoxColumn1 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
            this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.AddPricesSource = new MetaData.DataSetISM();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddPricesDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPrices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPricesSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.IDText);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(892, 46);
            this.panel2.TabIndex = 41;
            // 
            // IDText
            // 
            this.IDText.BackColor = System.Drawing.Color.White;
            this.IDText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IDText.Location = new System.Drawing.Point(415, 12);
            this.IDText.Name = "IDText";
            this.IDText.ReadOnly = true;
            this.IDText.Size = new System.Drawing.Size(90, 20);
            this.IDText.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(389, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 19);
            this.label1.TabIndex = 16;
            this.label1.Text = "ID:";
            // 
            // txtName
            // 
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(134, 12);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(224, 20);
            this.txtName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Название:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 352);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 46);
            this.panel1.TabIndex = 40;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(812, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 27);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(8, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AddPricesDelivery
            // 
            this.AddPricesDelivery.BackgroundColor = System.Drawing.Color.White;
            this.AddPricesDelivery.DataMember = "";
            this.AddPricesDelivery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddPricesDelivery.FilterString = null;
            this.AddPricesDelivery.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.AddPricesDelivery.Location = new System.Drawing.Point(3, 3);
            this.AddPricesDelivery.Name = "AddPricesDelivery";
            this.AddPricesDelivery.Order = null;
            this.AddPricesDelivery.Size = new System.Drawing.Size(878, 274);
            this.AddPricesDelivery.TabIndex = 2;
            this.AddPricesDelivery.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
            // 
            // extendedDataGridTableStyle2
            // 
            this.extendedDataGridTableStyle2.DataGrid = this.AddPricesDelivery;
            this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn2,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn4});
            this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle2.MappingName = "Table";
            // 
            // extendedDataGridComboBoxColumn2
            // 
            this.extendedDataGridComboBoxColumn2.FieldName = null;
            this.extendedDataGridComboBoxColumn2.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn2.Format = "";
            this.extendedDataGridComboBoxColumn2.FormatInfo = null;
            this.extendedDataGridComboBoxColumn2.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn2.MappingName = "shopType";
            this.extendedDataGridComboBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "0.##";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Наценка";
            this.formattableTextBoxColumn2.MappingName = "addPricePercent";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "#,0.##";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Доставка первого товара";
            this.formattableTextBoxColumn3.MappingName = "deliveryFirst";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "#,0.##";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Доставка второго товара";
            this.formattableTextBoxColumn4.MappingName = "deliverySecond";
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // AddPrices
            // 
            this.AddPrices.BackgroundColor = System.Drawing.Color.White;
            this.AddPrices.DataMember = "";
            this.AddPrices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddPrices.FilterString = null;
            this.AddPrices.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.AddPrices.Location = new System.Drawing.Point(0, 0);
            this.AddPrices.Name = "AddPrices";
            this.AddPrices.Order = null;
            this.AddPrices.Size = new System.Drawing.Size(884, 247);
            this.AddPrices.TabIndex = 3;
            this.AddPrices.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.AddPrices;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.extendedDataGridComboBoxColumn1,
            this.extendedDataGridSelectorColumn1,
            this.formattableTextBoxColumn1});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "Table1";
            // 
            // extendedDataGridComboBoxColumn1
            // 
            this.extendedDataGridComboBoxColumn1.FieldName = null;
            this.extendedDataGridComboBoxColumn1.FilterFieldName = null;
            this.extendedDataGridComboBoxColumn1.Format = "";
            this.extendedDataGridComboBoxColumn1.FormatInfo = null;
            this.extendedDataGridComboBoxColumn1.HeaderText = "Магазин";
            this.extendedDataGridComboBoxColumn1.MappingName = "shopType";
            this.extendedDataGridComboBoxColumn1.Width = 75;
            // 
            // extendedDataGridSelectorColumn1
            // 
            this.extendedDataGridSelectorColumn1.FieldName = null;
            this.extendedDataGridSelectorColumn1.FilterFieldName = null;
            this.extendedDataGridSelectorColumn1.HeaderText = "Бренд";
            this.extendedDataGridSelectorColumn1.MappingName = "brandName";
            this.extendedDataGridSelectorColumn1.NullText = "";
            this.extendedDataGridSelectorColumn1.Width = 75;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "0.##";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Наценка на бренд";
            this.formattableTextBoxColumn1.MappingName = "addPricePercent";
            this.formattableTextBoxColumn1.NullText = "";
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // AddPricesSource
            // 
            this.AddPricesSource.DataSetName = "NewDataSet";
            this.AddPricesSource.OnlyRowsWithoutErrors = false;
            this.AddPricesSource.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4});
            this.dataTable1.TableName = "Table";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "shopType";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "addPricePercent";
            this.dataColumn2.DataType = typeof(decimal);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "deliveryFirst";
            this.dataColumn3.DataType = typeof(decimal);
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "deliverySecond";
            this.dataColumn4.DataType = typeof(decimal);
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8});
            this.dataTable2.TableName = "Table1";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "shopType";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "brandOID";
            this.dataColumn6.DataType = typeof(System.Guid);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "brandName";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "addPricePercent";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 46);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(892, 306);
            this.tabControl1.TabIndex = 42;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.AddPricesDelivery);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(884, 280);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Доставка";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.AddPrices);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(884, 247);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Доп. цена";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // EditMasterCatalog
            // 
            this.ClientSize = new System.Drawing.Size(892, 398);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(464, 111);
            this.Name = "EditMasterCatalog";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Категория";
            this.Load += new System.EventHandler(this.fmEditTheme_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AddPricesDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPrices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddPricesSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn1;
		private ColumnMenuExtender.DataGridISM AddPricesDelivery;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private MetaData.DataSetISM AddPricesSource;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataTable dataTable2;
		private System.Data.DataColumn dataColumn5;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;
		private System.Data.DataColumn dataColumn8;
        private TextBox IDText;
        private Label label1;
        private TabControl tabControl1;
        private TabPage tabPage2;
        private TabPage tabPage3;
    }
}
