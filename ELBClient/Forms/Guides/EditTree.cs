﻿using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using System.Xml;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditTree.
	/// </summary>
	public partial class EditTree : EditForm
	{
//		private ObjectProvider.ObjectProvider objectProvider = ServiceUtility.ObjectProvider;
		private TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
		private Tree _tree;
//		private int oldNodeIndex = -1; 
		private string oldFullPath = "";
		private System.Data.DataSet dataSet1;
		private System.ComponentModel.IContainer components;

		public EditTree()
		{
			InitializeComponent();
			Object = new CTree();
			_tree = new Tree(0, int.MaxValue);
		}

		private void fmEditTree_Load(object sender, System.EventArgs e)
		{
			LoadObjects();
			DataBinding();
			BuildTree();
			LoadTree();
		}

		private void LoadObjects()
		{
			LoadObject("treeName", null);

			//Filling OID
			XmlDocument doc = GetResource("fmEditTree.xml");
			XmlNode n = doc.SelectSingleNode("tree");
			foreach(XmlAttribute attribute in n.Attributes)
			{
				if(attribute.Name == "OID")
					attribute.Value = Object.OID.ToString();
			}
			string xml = doc.OuterXml;
			dataSet1 = treeProvider.GetTreeContent(xml);
		 }

		private void DataBinding()
		{
			tbName.DataBindings.Add("Text", (CTree)Object, "treeName");
		}

//Filling Tree
		private void BuildTree() 
		{
			Stack nodes = new Stack();
			nodes.Push(_tree);

			int prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows)
			{
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if(nodeID != prevNode) {
					node = new Node((int) dr["lft"], (int) dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString().RestoreCr();
					node.NodeUrl = dr["nodeUrl"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty: (Guid) dr["OID"];
					node.objectNK = dr["NK"].ToString().RestoreCr();// == DBNull.Value ? null: (string) dr["NK"];
					node.objectNKRus = dr["label"].ToString().RestoreCr();
					while(((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
					}
				else {
					if(node != null) {
						node.AddString.Rows.Add(new string[3] {(string)dr["ordValue"], (string)dr["value"], (string)dr["url"]});
						node.AddString.AcceptChanges();
					}
				}
			}
		}

		private void LoadTree()
		{
			tv1.Nodes.Clear();
			foreach(Node n in _tree.Nodes) 
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tv1.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node)
		{
			foreach(Node n in node.Nodes) 
			{
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch{}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

//Add Del Edit nodes
		private void miAdd_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;

			EditTreeNodeDialog fm = new EditTreeNodeDialog();
			if (fm.ShowDialog(this) == DialogResult.OK)
			{//1
			
				XmlDocument doc = new XmlDocument();
				XmlNode root = doc.CreateNode(XmlNodeType.Element, "node", "");
				
				if(selectedNode != null)
				{
					XmlAttribute attr = doc.CreateAttribute("parentNode");
					attr.Value = ((Node)selectedNode.Tag).NodeId.ToString();
					root.Attributes.Append(attr);				
				}
				else // nodeCount == 0 || selectedNode == null
				{
					if(Object.OID == Guid.Empty)
					{
						SaveObject();
						//ObjectOID = Object.OID; // не уверен, но мало ли где используется ObjectOID... 
					}
					XmlAttribute attr = doc.CreateAttribute("treeOID");
					attr.Value = Object.OID.ToString();
					root.Attributes.Append(attr);				
				}

				Node n = new Node();
				n.Name = fm.NodeName;
				n.NodeUrl = fm.NodeUrl;
				n.objectOID = fm.NodeObject;
				n.objectNK = fm.NodeObjectNK;
				n.objectNKRus = fm.NodeObjectNKRus;
				//n.dateBegin
				//n.dateEnd
				if(n.Name != "")
				{
					XmlAttribute attr = doc.CreateAttribute("nodeName");
					attr.Value = n.Name;
					root.Attributes.Append(attr);
				}
				if(n.NodeUrl != "")
				{
					XmlAttribute attr = doc.CreateAttribute("nodeUrl");
					attr.Value = n.NodeUrl;
					root.Attributes.Append(attr);
				}
				if(n.objectOID != Guid.Empty)
				{
					XmlAttribute attr = doc.CreateAttribute("object");
					attr.Value = n.objectOID.ToString();
					root.Attributes.Append(attr);
				}
				DataTable deleted = n.AddString.GetChanges(DataRowState.Deleted);
				DataTable added = n.AddString.GetChanges(DataRowState.Added);
				if(added != null) {
					foreach(DataRow dr in added.Rows) {
						XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
					}
				}

				doc.AppendChild(root);
				n.NodeId = treeProvider.CreateNode(doc.OuterXml);
				n.AddString.AcceptChanges();

				TreeNode tn = new TreeNode(n.Name.Replace("\r\n", " "));
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tn.Tag = n;

				if(selectedNode != null)
					selectedNode.Nodes.Insert(0, tn);
				else
				{
					tv1.Nodes.Insert(0, tn);
					tv1.SelectedNode = tv1.Nodes[0];
				}
			}//1
		}
		
		private void miAddSibling_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;

			if(selectedNode != null)
			{
				EditTreeNodeDialog fm = new EditTreeNodeDialog();
				if (fm.ShowDialog(this) == DialogResult.OK)
				{//1
			
					XmlDocument doc = new XmlDocument();
					XmlNode root = doc.CreateNode(XmlNodeType.Element, "node", "");
				
					XmlAttribute attr1 = doc.CreateAttribute("leftSibling");
					attr1.Value = ((Node)selectedNode.Tag).NodeId.ToString();
					root.Attributes.Append(attr1);				
				
					Node n = new Node();
					n.Name = fm.NodeName;
					n.NodeUrl = fm.NodeUrl;
					n.objectOID = fm.NodeObject;
					n.objectNK = fm.NodeObjectNK;
					n.objectNKRus = fm.NodeObjectNKRus;
					//n.dateBegin
					//n.dateEnd
					if(n.Name != "")
					{
						XmlAttribute attr = doc.CreateAttribute("nodeName");
						attr.Value = n.Name;
						root.Attributes.Append(attr);
					}
					if(n.NodeUrl != "")
					{
						XmlAttribute attr = doc.CreateAttribute("nodeUrl");
						attr.Value = n.NodeUrl;
						root.Attributes.Append(attr);
					}
					if(n.objectOID != Guid.Empty)
					{
						XmlAttribute attr = doc.CreateAttribute("object");
						attr.Value = n.objectOID.ToString();
						root.Attributes.Append(attr);
					}
					DataTable deleted = n.AddString.GetChanges(DataRowState.Deleted);
					DataTable added = n.AddString.GetChanges(DataRowState.Added);
					if(added != null) {
						foreach(DataRow dr in added.Rows) {
							XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
							n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
							n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
							n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
						}
					}

					doc.AppendChild(root);
					n.NodeId = treeProvider.CreateNode(doc.OuterXml);
					n.AddString.AcceptChanges();

					TreeNode tn = new TreeNode(n.Name.Replace("\r\n", " "));
					if(n.objectOID != Guid.Empty)
						tn.ForeColor = Color.Red;
					tn.Tag = n;
					TreeNode parent = tv1.SelectedNode.Parent;
					int indx = tv1.SelectedNode.Index;
					
					if(parent != null)
						parent.Nodes.Insert(indx+1, tn);
					else tv1.Nodes.Insert(indx+1, tn);
				}//1
			}
		}

		private void miEdit_Click(object sender, System.EventArgs e)
		{
			TreeNode selectedNode = tv1.SelectedNode;

			if(selectedNode != null)
			{
				EditTreeNodeDialog fm = new EditTreeNodeDialog();
				Node n = (Node)selectedNode.Tag;
				fm.NodeID = n.NodeId.ToString();
				fm.NodeName = n.Name;
				fm.NodeUrl = n.NodeUrl;
				fm.NodeObject = n.objectOID;
				fm.NodeObjectNK = n.objectNK;
				fm.NodeObjectNKRus = n.objectNKRus;

				if (fm.ShowDialog(this) == DialogResult.OK)
				{//1
				
					XmlDocument doc = new XmlDocument();
					XmlNode root = doc.CreateElement("node");
					Node n1 = (Node)selectedNode.Tag;
				
					XmlAttribute attr1 = doc.CreateAttribute("nodeID");
					attr1.Value = n1.NodeId.ToString();
					root.Attributes.Append(attr1);				
					
					if(n1.Name != fm.NodeName)
					{
						n1.Name = fm.NodeName;
						selectedNode.Text = fm.NodeName.Replace("\r\n", " ");
						XmlAttribute attr = doc.CreateAttribute("nodeName");
						attr.Value = fm.NodeName;
						root.Attributes.Append(attr);
					}
					if(n1.NodeUrl != fm.NodeUrl)
					{
						n1.NodeUrl = fm.NodeUrl;
						XmlAttribute attr = doc.CreateAttribute("nodeUrl");
						attr.Value = fm.NodeUrl;
						root.Attributes.Append(attr);
					}
					Guid temp = fm.NodeObject;
					if(n1.objectOID != temp)
					{
						n1.objectOID = temp;
						n1.objectNK = fm.NodeObjectNK;
						n1.objectNKRus = fm.NodeObjectNKRus;
						if(temp != Guid.Empty)
							selectedNode.ForeColor = Color.Red;
						else selectedNode.ForeColor = Color.Black;
						XmlAttribute attr = doc.CreateAttribute("object");
						attr.Value = temp.ToString(); //пришлось сделать через другую переменную, компилятор ругался на то, что объект наследуемый от marshal by ref object не может быть приведён к string ...
						root.Attributes.Append(attr);
					}
					DataTable deleted = n1.AddString.GetChanges(DataRowState.Deleted);
					if(deleted != null) {
						foreach(DataRow dr in deleted.Rows) {
							XmlNode n2 = root.AppendChild(doc.CreateElement("delete"));
							n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue", DataRowVersion.Original];
						}
					}
					DataTable added = n1.AddString.GetChanges(DataRowState.Added);
					if(added != null) {
						foreach(DataRow dr in added.Rows) {
							XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
							n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
							n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
							n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
						}
					}
					DataTable updated = n1.AddString.GetChanges(DataRowState.Modified);
					if(updated != null) {
						foreach(DataRow dr in updated.Rows) {
							XmlNode n2 = root.AppendChild(doc.CreateElement("update"));
							n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
							n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
							n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
						}
					}
					doc.AppendChild(root);
					treeProvider.UpdateNode(doc.OuterXml);
					n1.AddString.AcceptChanges();
				}//1
			}
		}

		private void miDel_Click(object sender, System.EventArgs e)
		{
			if(MessageBox.Show(this, "Вы уверены, что хотите удалить?","Внимание",MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				TreeNode selectedNode = tv1.SelectedNode;
		
				if(selectedNode != null)
				{
					TreeNode parent = selectedNode.Parent;
					TreeNode tmp;
					int indx = selectedNode.Index;

					treeProvider.DeleteNode(((Node)selectedNode.Tag).NodeId);
				
					if(selectedNode.Nodes.Count != 0)
					{
						tmp  = (TreeNode)selectedNode.Clone();

						if(parent != null)
						{
							foreach(TreeNode child in tmp.Nodes)
								parent.Nodes.Insert(indx++,child);
						}
						else	//Root
						{
							foreach(TreeNode child in tmp.Nodes)
								tv1.Nodes.Insert(indx++,child);
						}
					}
					selectedNode.Remove();
				}
			}
		}

//Working With Tree
		private void tv1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right) 
			{ 
				tv1.SelectedNode = tv1.GetNodeAt(e.X ,e.Y);
				if(tv1.SelectedNode == null)
				{
					contextMenu1.MenuItems[1].Enabled = false;
					contextMenu1.MenuItems[2].Enabled = false;
					contextMenu1.MenuItems[3].Enabled = false;
				}
				else
				{
					contextMenu1.MenuItems[1].Enabled = true;
					contextMenu1.MenuItems[2].Enabled = true;
					contextMenu1.MenuItems[3].Enabled = true;
				}
				//contextMenu1.Shows(tv1, new Point(e.X, e.Y));
			}  
		}

		private void tv1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) 
		{ 
			TreeNode tn = tv1.GetNodeAt(e.X, e.Y); 
 
			if(tn != null) 
			{ 
//				int currentNodeIndex = tn.Index;
				string currentFullPath = tn.FullPath;
 
//				if(currentNodeIndex != oldNodeIndex) 
				if(currentFullPath != oldFullPath) 
				{ 
//					oldNodeIndex = currentNodeIndex; 
					oldFullPath = currentFullPath;
 
					if(toolTip1.Active) 
						toolTip1.Active = false; //turn it off 
					toolTip1.SetToolTip(tv1, string.Format("Объект: {0}\nURL: {1}\nКласс: {2}", ((Node)tn.Tag).objectNK, ((Node)tn.Tag).NodeUrl,((Node)tn.Tag).objectNKRus)); 
					toolTip1.Active = true; //make it active so it can show 
				} 
			} 
		}

		private void miUp_Click(object sender, System.EventArgs e)
		{
			//перемещение нода вверх
			TreeNode sn = tv1.SelectedNode;
			int nodeID = ((Node)sn.Tag).NodeId;
			try
			{
				if(sn != null)
					op.ExecuteCommand("spShiftNode @nodeID='"+nodeID+"', @up=1");
				SaveObject();
				tv1.Nodes.Clear();
				_tree = new Tree(0, int.MaxValue);
				LoadObjects();
				BuildTree();
				LoadTree();
//				selectedNode.Expand();
				this.tv1.ExpandAll();
			}
			catch{this.ShowError("Не удалось передвинуть элемент");}
		}

		private void miDown_Click(object sender, System.EventArgs e)
		{
			//перемещение нода вниз
			TreeNode selectedNode = tv1.SelectedNode;
			int nodeID = ((Node)selectedNode.Tag).NodeId;
			try
			{
				if(selectedNode != null) {
					ObjectProvider.ObjectProvider provider = ServiceUtility.ObjectProvider;
					provider.ExecuteCommand("spShiftNode @nodeID="+nodeID+", @up=0");
				}
				SaveObject();
				tv1.Nodes.Clear();
				_tree = new Tree(0, int.MaxValue);
				LoadObjects();
				BuildTree();
				LoadTree();
//				selectedNode.Expand();
				this.tv1.ExpandAll();
			}
			catch{this.ShowError("Не удалось передвинуть элемент");}
		}

		private void miRight_Click(object sender, System.EventArgs e)
		{
			//перемещение нода вправо
			TreeNode selectedNode = tv1.SelectedNode;
			try
			{
				int nodeID = ((Node)selectedNode.Tag).NodeId;
				if(selectedNode != null)
					op.ExecuteCommand("spChangeParentNode @nodeID="+nodeID+", @inside=1");
				SaveObject();
				tv1.Nodes.Clear();
				_tree = new Tree(0, int.MaxValue);
				LoadObjects();
				BuildTree();
				LoadTree();
//				selectedNode.Expand();
				this.tv1.ExpandAll();
			}
			catch{this.ShowError("Не удалось передвинуть элемент");}
		}

		private void miLeft_Click(object sender, System.EventArgs e)
		{
			//перемещение нода влево
			TreeNode selectedNode = tv1.SelectedNode;
			int nodeID = ((Node)selectedNode.Tag).NodeId;
			try
			{
				if(selectedNode != null)
					op.ExecuteCommand("spChangeParentNode @nodeID="+nodeID+", @inside=0");
				SaveObject();
				tv1.Nodes.Clear();
				_tree = new Tree(0, int.MaxValue);
				LoadObjects();
				BuildTree();
				LoadTree();
//				selectedNode.Expand();
				this.tv1.ExpandAll();
			}
			catch{this.ShowError("Не удалось передвинуть элемент");}
		}

//		private TreeNode FindNode(TreeNodeCollection tnc,Node n)
//		{
//			foreach (TreeNode tn in tnc)
//			{
//				if(((Node)tn.Tag) == n)
//					return tn;
//				else if (tn.Nodes.Count > 0)
//				{
//					TreeNode tnf = FindNode(tn.Nodes,n);
//					if (tnf != null)
//						return tnf;
//				}
//			}
//			return null;
//		}

		private void tv1_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e)
		{
			DoDragDrop(e.Item, DragDropEffects.Move);
		}

		private void tv1_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			e.Effect = DragDropEffects.Move;
		}

		private void tv1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			TreeNode newNode;
			if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode") == true)
			{
				if ((e.KeyState & 4) == 4)//LeftSiblingNode //+Shift
				{
					Point pt;
					TreeNode destinationNode;
					pt = tv1.PointToClient(new Point(e.X, e.Y));
					destinationNode = tv1.GetNodeAt(pt);
					newNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
					if(destinationNode != newNode)
					{
						int newNodeID = ((Node)newNode.Tag).NodeId;
						int siblingNodeID = ((Node)destinationNode.Tag).NodeId;
						int indexofNode = destinationNode.Index;
						TreeNode Parent = destinationNode.Parent;
						if(Parent != null) Parent.Nodes.Insert(indexofNode+1, (TreeNode)newNode.Clone());
						else tv1.Nodes.Insert(indexofNode+1,(TreeNode)newNode.Clone());
						//////////
						op.ExecuteCommand("spMoveNode @nodeID="+newNodeID+", @parent=null, @leftSibling="+siblingNodeID);
						SaveObject();
						///////////
						destinationNode.Expand();
						newNode.Remove();
					}
				}
				else // parent
				{
					Point pt;
					TreeNode destinationNode;
					pt = tv1.PointToClient(new Point(e.X, e.Y));
					destinationNode = tv1.GetNodeAt(pt);
					newNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
					if(destinationNode != newNode)
					{
						int newNodeID = ((Node)newNode.Tag).NodeId;
						int parentNodeID = ((Node)destinationNode.Tag).NodeId;
						destinationNode.Nodes.Insert(0, (TreeNode)newNode.Clone());
						//////////
						op.ExecuteCommand("spMoveNode @nodeID="+newNodeID+", @parent="+parentNodeID);
						SaveObject();
						///////////
						destinationNode.Expand();
						newNode.Remove();
					}
				}
			}
		} 

		#region Tree
		public class NodeContainer 
		{
			public int Left, Right;
			public ArrayList Nodes = new ArrayList();
			public NodeContainer(int left, int right) 
			{
				Left = left;
				Right = right;
			}
		}
		public class Node : NodeContainer 
		{
			public int NodeId;
			public string Name;
			public string NodeUrl;
			public string dateBegin;
			public string dateEnd;
			public Guid objectOID;
			public string objectNK;
			public string objectNKRus;
			public DataTable AddString;
			public Node(int left, int right) : base(left, right){
				init();
			}
			public Node() : base(0, 0){
				init();
			}
			private void init() {
				AddString = new DataTable();
				DataColumn key = AddString.Columns.Add("ordValue", typeof(string));
				AddString.PrimaryKey = new DataColumn[1]{key};
				AddString.Columns.Add("value", typeof(string));
				AddString.Columns.Add("url", typeof(string));
			}
		}
		public class Tree : NodeContainer 
		{
			public String name;
			public Tree(int left, int right) : base(left, right){}
		}
		#endregion
	
	}
}
