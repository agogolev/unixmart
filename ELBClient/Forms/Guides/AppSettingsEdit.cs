﻿using System.Data;
using System.Linq;
using ColumnMenuExtender;
using MetaData;
using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	public partial class AppSettingsEdit : EditForm
	{
		public bool SalesNotesModified { get; set; }
		public AppSettingsEdit()
		{
			InitializeComponent();
		}

		private void LoadLookup()
		{
			const string sql = @"
SELECT
	shopType,
	name
FROM
	t_TypeShop
ORDER BY
	shopType
";
			var ds = new DataSetISM(lp.GetDataSetSql(sql));
			cbShopType.AddItems(ds.Table.AsEnumerable().Select(k => (object) new ListItem { Value = k.Field<int>("shopType"), Text = k.Field<string>("name") }).ToArray());
		}

		private void cbShopType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadSettings();
		}

		private void LoadSettings()
		{
			var bp = ServiceUtility.BusinessProvider;
			Sales_NotesText.Text = bp.GetAppSettings(cbShopType.SelectedValue, "sales_notes");
		}

		private void Sales_NotesText_TextChanged(object sender, System.EventArgs e)
		{
			SalesNotesModified = true;
		}
		protected override bool IsModified
		{
			get
			{
				return SalesNotesModified;
			}
		}

		private void AppSettingsEdit_Load(object sender, System.EventArgs e)
		{
			LoadLookup();
			cbShopType.SelectedIndex = 0;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveObject();
		}

		protected override void SaveObject()
		{
			var bp = ServiceUtility.BusinessProvider;
			if(SalesNotesModified) bp.SetAppSettings(cbShopType.SelectedValue, "sales_notes", Sales_NotesText.Text.Trim());
		}
	}
}
