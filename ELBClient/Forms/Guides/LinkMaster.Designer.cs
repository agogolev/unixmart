using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class LinkMaster
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TreeView tvSiteTree;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.TreeView tvMasterTree;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.tvSiteTree = new System.Windows.Forms.TreeView();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.tvMasterTree = new System.Windows.Forms.TreeView();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 455);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(768, 46);
			this.panel1.TabIndex = 0;
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(8, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// tvSiteTree
			// 
			this.tvSiteTree.AllowDrop = true;
			this.tvSiteTree.Dock = System.Windows.Forms.DockStyle.Left;
			this.tvSiteTree.Location = new System.Drawing.Point(0, 0);
			this.tvSiteTree.Name = "tvSiteTree";
			this.tvSiteTree.Size = new System.Drawing.Size(384, 455);
			this.tvSiteTree.TabIndex = 1;
			this.tvSiteTree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvSiteTree_ItemDrag);
			this.tvSiteTree.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvSiteTree_DragDrop);
			this.tvSiteTree.DragOver += new System.Windows.Forms.DragEventHandler(this.tvSiteTree_DragOver);
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(384, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 455);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// tvMasterTree
			// 
			this.tvMasterTree.AllowDrop = true;
			this.tvMasterTree.ContextMenu = this.contextMenu1;
			this.tvMasterTree.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvMasterTree.Location = new System.Drawing.Point(0, 0);
			this.tvMasterTree.Name = "tvMasterTree";
			this.tvMasterTree.Size = new System.Drawing.Size(381, 455);
			this.tvMasterTree.TabIndex = 0;
			this.tvMasterTree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvMasterTree_ItemDrag);
			this.tvMasterTree.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvMasterTree_DragDrop);
			this.tvMasterTree.DragEnter += new System.Windows.Forms.DragEventHandler(this.tvMasterTree_DragEnter);
			this.tvMasterTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvMasterTree_MouseDown);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Показать привязанные категории сайта";
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tvMasterTree);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(387, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(381, 455);
			this.panel2.TabIndex = 2;
			// 
			// LinkMaster
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(768, 501);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.tvSiteTree);
			this.Controls.Add(this.panel1);
			this.Name = "LinkMaster";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Првязка мастера";
			this.Load += new System.EventHandler(this.fmLinkMaster_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
