﻿using System;
using System.Data;
using System.Windows.Forms;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListPeople.
	/// </summary>
	public partial class ListPeople : ListForm
	{
		public bool HasWholesale { get; set; }

		public ListPeople()
		{
			InitializeComponent();
		}

		public override void LoadData()
		{
			dataSet1 = lp.GetList(dataGridISM1.GetDataXml().OuterXml);

			int cri = dataGridISM1.CurrentRowIndex;
			dataGridISM1.Enabled = false;
			dataGridISM1.SetDataBinding(dataSet1, "table");
			dataGridISM1.Enabled = true;

			DataTable dt = dataSet1.Tables["table"];
			if (cri == -1)
			{
				if (dt.Rows.Count > 0)
					cri = 0;
			}
			else if (cri > dt.Rows.Count - 1)
				cri = dt.Rows.Count - 1;
			if (cri >= 0)
			{
				dataGridISM1.CurrentRowIndex = cri;
				dataGridISM1.Select(cri);
			}

			// корявый способ избавится от дизабленных скролбаров :)
			dataGridISM1.Width++; dataGridISM1.Width--;
		}

		private void fmListPeople_Load(object sender, EventArgs e)
		{
			LoadGroups();
			dataGridPager1.BindToDataGrid(dataGridISM1);
			listContextMenu1.Bind(this);
		}

		private void listContextMenu1_NewClick(object sender, EventArgs e)
		{
			showEditForm(Guid.Empty);
		}

		private void listContextMenu1_EditClick(object sender, EventArgs e)
		{
			showEditForm(listContextMenu1.SelectedOID);
		}

		private void showEditForm(Guid OID)
		{
			var form = new EditPeople { ObjectOID = OID, MdiParent = MdiParent, HasWholesale = HasWholesale };
			form.ReloadGrid += LoadData;
			form.Show();
		}

		private void cbGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbGroup.SelectedIndex == 0)
				dataGridISM1.DeleteFilter("groupFilter");
			else
			{
				Guid oid = (Guid)cbGroup.SelectedItem.Tag;
				dataGridISM1.CreateAuxFilter("groups.groupOID", "groupFilter", MetaData.FilterVerb.Equal, false, new object[] { oid });
			}
			dataGridISM1.OnReload(new EventArgs());
		}

		private void LoadGroups()
		{
			cbGroup.AddItem(new ListItem(0, "Все группы"));
			var xml = DataGridISM.GetDataXmlNow("CGroup", "1", "100", "OID,name", null);
			var ds = lp.GetList(xml.OuterXml);
			var i = 1;
			var selected = 0;
			foreach (DataRow dr in ds.Tables["table"].Rows)
			{

				if (dr["name"].ToString() == "")
					dr["name"] = "Без названия";
				if (dr["oid"].ToString() != "")
					cbGroup.AddItem(new ListItem(i, dr["name"].ToString()) { Tag = new Guid(dr["oid"].ToString()) });
				if (string.Compare(dr["name"].ToString(), "Администраторы", StringComparison.CurrentCultureIgnoreCase) == 0) selected = i;
				i++;
			}
			cbGroup.SelectedIndex = selected;
		}

		#region Working with DataGrid
		private void dataGridISM1_Reload(object sender, EventArgs e)
		{
			LoadData();
		}

		private void dataGridISM1_DoubleClick(object sender, EventArgs e)
		{
			var pt = dataGridISM1.PointToClient(Cursor.Position);

			var hti = dataGridISM1.HitTest(pt);
			if (hti.Type != DataGrid.HitTestType.Cell) return;
			var dr = dataGridISM1.GetSelectedRow();
			showEditForm((Guid)dr["OID"]);
		}

		private void dataGridISM1_ActionKeyPressed(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Insert:
					showEditForm(Guid.Empty);
					break;
				case Keys.Delete:
					{
						var dt = TableInGrid.GetTable(dataGridISM1);
						Form form = null;
						if (Parent is Form)
							form = Parent as Form;
						listContextMenu1.DeleteObject(form, (Guid)dt.Rows[dataGridISM1.CurrentRowIndex]["OID"]);
					}
					break;
				case Keys.Enter:
					{
						DataTable dt = TableInGrid.GetTable(dataGridISM1);
						showEditForm((Guid)dt.Rows[dataGridISM1.CurrentRowIndex]["OID"]);
					}
					break;
			}
		}

		#endregion

	}
}
