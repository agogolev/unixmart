using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditTree
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.TextBox tbName;
private System.Windows.Forms.Label label1;
private System.Windows.Forms.Button btnCancel;
private System.Windows.Forms.TreeView tv1;
private System.Windows.Forms.Panel panel1;
private System.Windows.Forms.Panel panel2;
private System.Windows.Forms.Panel panel3;
private System.Windows.Forms.ContextMenu contextMenu1;
private System.Windows.Forms.MenuItem miAdd;
private System.Windows.Forms.MenuItem miEdit;
private System.Windows.Forms.MenuItem miDel;
private System.Windows.Forms.MenuItem miAddSibling;
private System.Windows.Forms.ToolTip toolTip1;
private System.Windows.Forms.MenuItem menuItem1;
private System.Windows.Forms.MenuItem miUp;
private System.Windows.Forms.MenuItem miDown;
private System.Windows.Forms.MenuItem miRight;
private System.Windows.Forms.MenuItem miLeft;
private System.Windows.Forms.ImageList imageList1;
private System.Windows.Forms.Button btnSave;
private System.Windows.Forms.Button btnSave2;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTree));
			this.tbName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.dataSet1 = new System.Data.DataSet();
			this.tv1 = new System.Windows.Forms.TreeView();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.miAdd = new System.Windows.Forms.MenuItem();
			this.miAddSibling = new System.Windows.Forms.MenuItem();
			this.miEdit = new System.Windows.Forms.MenuItem();
			this.miDel = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.miUp = new System.Windows.Forms.MenuItem();
			this.miDown = new System.Windows.Forms.MenuItem();
			this.miRight = new System.Windows.Forms.MenuItem();
			this.miLeft = new System.Windows.Forms.MenuItem();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSave2 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnSave = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.panel3 = new System.Windows.Forms.Panel();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// tbName
			// 
			this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbName.Location = new System.Drawing.Point(72, 9);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(424, 20);
			this.tbName.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 19);
			this.label1.TabIndex = 23;
			this.label1.Text = "Название:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(8, 9);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// tv1
			// 
			this.tv1.AllowDrop = true;
			this.tv1.ContextMenu = this.contextMenu1;
			this.tv1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tv1.Location = new System.Drawing.Point(0, 0);
			this.tv1.Name = "tv1";
			this.tv1.Size = new System.Drawing.Size(542, 331);
			this.tv1.TabIndex = 2;
			this.tv1.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tv1_ItemDrag);
			this.tv1.DragDrop += new System.Windows.Forms.DragEventHandler(this.tv1_DragDrop);
			this.tv1.DragEnter += new System.Windows.Forms.DragEventHandler(this.tv1_DragEnter);
			this.tv1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tv1_MouseDown);
			this.tv1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tv1_MouseMove);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miAdd,
            this.miAddSibling,
            this.miEdit,
            this.miDel,
            this.menuItem1,
            this.miUp,
            this.miDown,
            this.miRight,
            this.miLeft});
			// 
			// miAdd
			// 
			this.miAdd.Index = 0;
			this.miAdd.Text = "Добавить дочерний";
			this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
			// 
			// miAddSibling
			// 
			this.miAddSibling.Index = 1;
			this.miAddSibling.Text = "Добавить";
			this.miAddSibling.Click += new System.EventHandler(this.miAddSibling_Click);
			// 
			// miEdit
			// 
			this.miEdit.Index = 2;
			this.miEdit.Text = "Редактировать";
			this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
			// 
			// miDel
			// 
			this.miDel.Index = 3;
			this.miDel.Text = "Удалить";
			this.miDel.Click += new System.EventHandler(this.miDel_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 4;
			this.menuItem1.Text = "-";
			// 
			// miUp
			// 
			this.miUp.Index = 5;
			this.miUp.Text = "Вверх";
			this.miUp.Click += new System.EventHandler(this.miUp_Click);
			// 
			// miDown
			// 
			this.miDown.Index = 6;
			this.miDown.Text = "Вниз";
			this.miDown.Click += new System.EventHandler(this.miDown_Click);
			// 
			// miRight
			// 
			this.miRight.Index = 7;
			this.miRight.Text = "Вправо";
			this.miRight.Click += new System.EventHandler(this.miRight_Click);
			// 
			// miLeft
			// 
			this.miLeft.Index = 8;
			this.miLeft.Text = "Влево";
			this.miLeft.Click += new System.EventHandler(this.miLeft_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.btnSave2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 377);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(542, 46);
			this.panel1.TabIndex = 28;
			// 
			// btnSave2
			// 
			this.btnSave2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave2.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave2.Location = new System.Drawing.Point(462, 9);
			this.btnSave2.Name = "btnSave2";
			this.btnSave2.Size = new System.Drawing.Size(72, 27);
			this.btnSave2.TabIndex = 4;
			this.btnSave2.Text = "Сохранить";
			this.btnSave2.Visible = false;
			this.btnSave2.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.btnSave);
			this.panel2.Controls.Add(this.tbName);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(542, 46);
			this.panel2.TabIndex = 29;
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnSave.ImageIndex = 0;
			this.btnSave.ImageList = this.imageList1;
			this.btnSave.Location = new System.Drawing.Point(504, 9);
			this.btnSave.Name = "btnSave";
			this.btnSave.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.btnSave.Size = new System.Drawing.Size(24, 28);
			this.btnSave.TabIndex = 26;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
			this.imageList1.Images.SetKeyName(0, "");
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.tv1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 46);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(542, 331);
			this.panel3.TabIndex = 30;
			// 
			// EditTree
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 423);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(250, 231);
			this.Name = "EditTree";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Структура сайта";
			this.Load += new System.EventHandler(this.fmEditTree_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
