﻿using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmEditComment.
	/// </summary>
	public partial class EditComment : EditForm
	{

		public EditComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			Object = new CComment();
		}

		private void fmEditComment_Load(object sender, System.EventArgs e) {
			LoadData();
			BindFields();
		}
		private void LoadData() {
			LoadObject("name,header,peopleOID,object,reit,selfComment", null);
		}
		private void BindFields() {
			txtName.DataBindings.Add("Text", Object, "Name");
			txtPeople.DataBindings.Add("BoundProp", Object, "PeopleOID");
			txtObject.DataBindings.Add("BoundProp", Object, "Object");
			txtHeader.DataBindings.Add("Text", Object, "Header");
			txtReit.DataBindings.Add("BoundProp", Object, "Reit");
			txtSelfComment.DataBindings.Add("Text", Object, "SelfComment");
		}

		private void button2_Click(object sender, System.EventArgs e) {
			SaveObject();
		}

		private void button1_Click(object sender, System.EventArgs e) {
			Close();
		}
	}
}
