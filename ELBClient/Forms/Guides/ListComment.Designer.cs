using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListComment
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.FormattableTextBoxColumn colHeader;
		private ColumnMenuExtender.FormattableTextBoxColumn colPeople;
		private ColumnMenuExtender.FormattableTextBoxColumn colObject;
		private ColumnMenuExtender.FormattableTextBoxColumn colReit;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.colHeader = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colPeople = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colObject = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colReit = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// mainDataGrid1
			// 
			this.mainDataGrid1.AdditionalInfo = "";
			this.mainDataGrid1.ClassName = "CComment";
			this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
			this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.mainDataGrid1.Name = "mainDataGrid1";
			this.mainDataGrid1.Size = new System.Drawing.Size(542, 323);
			this.mainDataGrid1.TabIndex = 2;
			this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// colName
			// 
			this.colName.FieldName = null;
			this.colName.FilterFieldName = null;
			this.colName.Format = "";
			this.colName.FormatInfo = null;
			this.colName.HeaderText = "NickName";
			this.colName.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.colName, this.menuFilterSort1);
			this.colName.Width = 150;
			// 
			// colHeader
			// 
			this.colHeader.FieldName = null;
			this.colHeader.FilterFieldName = null;
			this.colHeader.Format = "";
			this.colHeader.FormatInfo = null;
			this.colHeader.HeaderText = "Заголовок";
			this.colHeader.MappingName = "header";
			this.columnMenuExtender1.SetMenu(this.colHeader, this.menuFilterSort1);
			this.colHeader.Width = 75;
			// 
			// colPeople
			// 
			this.colPeople.FieldName = "NK(peopleOID)";
			this.colPeople.FilterFieldName = null;
			this.colPeople.Format = "";
			this.colPeople.FormatInfo = null;
			this.colPeople.MappingName = "peopleOID_NK";
			this.columnMenuExtender1.SetMenu(this.colPeople, this.menuFilterSort1);
			this.colPeople.Width = 75;
			// 
			// colObject
			// 
			this.colObject.FieldName = "NK(object)";
			this.colObject.FilterFieldName = null;
			this.colObject.Format = "";
			this.colObject.FormatInfo = null;
			this.colObject.HeaderText = "Товар";
			this.colObject.MappingName = "object_NK";
			this.columnMenuExtender1.SetMenu(this.colObject, this.menuFilterSort1);
			this.colObject.Width = 75;
			// 
			// colReit
			// 
			this.colReit.FieldName = null;
			this.colReit.FilterFieldName = null;
			this.colReit.Format = "";
			this.colReit.FormatInfo = null;
			this.colReit.HeaderText = "Оценка";
			this.colReit.MappingName = "reit";
			this.columnMenuExtender1.SetMenu(this.colReit, this.menuFilterSort1);
			this.colReit.Width = 75;
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colName,
            this.colHeader,
            this.colPeople,
            this.colObject,
            this.colReit});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// ListComment
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 323);
			this.Controls.Add(this.mainDataGrid1);
			this.Name = "ListComment";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Комментарии пользователей";
			this.Load += new System.EventHandler(this.fmListComment_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
