using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListDisableExpressDelivery
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Название";
			this.formattableTextBoxColumn1.MappingName = "name";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "dd.MM.yyyy";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Дата";
			this.formattableTextBoxColumn2.MappingName = "dateOccur";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// mainDataGrid1
			// 
			this.mainDataGrid1.AdditionalInfo = "";
			this.mainDataGrid1.ClassName = "CDisableExpressDelivery";
			this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
			this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.mainDataGrid1.Name = "mainDataGrid1";
			this.mainDataGrid1.Size = new System.Drawing.Size(542, 370);
			this.mainDataGrid1.TabIndex = 1;
			this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// ListDisableExpressDelivery
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 370);
			this.Controls.Add(this.mainDataGrid1);
			this.MinimumSize = new System.Drawing.Size(550, 404);
			this.Name = "ListDisableExpressDelivery";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Отмена экспресс доставки";
			this.Load += new System.EventHandler(this.fmListTree_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
	}
}
