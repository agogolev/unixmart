﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ELBClient.Classes;
using MetaData;
using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Guides
{
    public partial class BrowseDefaultTitle : ListForm
    {
        public IList<ShopMenu> Shops { get; set; }
        private int _shopType;
        private int _catalogRootNode;
        private Guid _selectedCategory;
        private readonly Tree tree;
        private DataSet _nodesDataSet;
        private DataSetISM _titlesDataSet;
        private readonly TreeProvider.TreeProvider _treeProvider = ServiceUtility.TreeProvider;

        public BrowseDefaultTitle()
        {
            InitializeComponent();
            tree = new Tree(0, int.MaxValue);
            TitlesGrid.GetForeColorForRow += TitlesGrid_GetForeColorForRow;
        }

        private Brush TitlesGrid_GetForeColorForRow(int rowNum, CurrencyManager source)
        {
            var dv = (DataView)source.List;
            var dr = dv[rowNum];
            Brush br = null;
            if (!(bool)dr["isActive"])
            {
                br = new SolidBrush(Color.Gray);
            }
            return br;
        }

        private void BrowseDefaultTitle_Load(object sender, EventArgs e)
        {
            LoadLookup();
            LoadShops();
            _catalogRootNode = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(_shopType);
            BuildTree();
            LoadTree();
            LoadGrid();
        }

        private void BuildTree()
        {
            string xml;
            if (_catalogRootNode == 0)
            {
                throw new Exception("Не задан корневой элемент");
            }
            else
            {
                xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\"" + _catalogRootNode + "\" withParent=\"true\"/>";
            }
            _nodesDataSet = _treeProvider.GetTreeContent(xml);

            Stack nodes = new Stack();
            nodes.Push(tree);

            int prevNode = 0;
            foreach (DataRow dr in _nodesDataSet.Tables["table"].Rows)
            {
                int nodeID = (int)dr["nodeID"];
                Node node = null;
                if (nodeID != prevNode)
                {
                    node = new Node((int)dr["lft"], (int)dr["rgt"]);
                    node.NodeId = nodeID;
                    node.Name = dr["nodeName"].ToString().RestoreCr();
                    node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"];
                    node.objectNK = dr["NK"].ToString().RestoreCr();// == DBNull.Value ? null: (string) dr["NK"];
                    node.objectNKRus = dr["label"].ToString().RestoreCr();
                    node.className = dr["className"].ToString();
                    while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
                    ((NodeContainer)nodes.Peek()).Nodes.Add(node);
                    if ((node.Left + 1) != node.Right)
                    {
                        nodes.Push(node);
                    }
                    prevNode = nodeID;
                }
                else
                {
                    if (node != null)
                    {
                        node.AddString.Rows.Add(new string[3] { (string)dr["ordValue"], (string)dr["value"], (string)dr["url"] });
                        node.AddString.AcceptChanges();
                    }
                }
            }
        }

        private void LoadTree()
        {
            CatalogTreeView.Nodes.Clear();
            foreach (Node n in tree.Nodes)
            {
                TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
                tn.Tag = n;//n.objectOID;
                //if (n.objectOID != Guid.Empty)
                //    tn.ForeColor = Color.Red;
                CatalogTreeView.Nodes.Add(tn);
                LoadNode(tn, n);
            }
            CatalogTreeView.Nodes[0].Expand();
        }

        private void LoadNode(TreeNode treeNode, Node node)
        {
            foreach (Node n in node.Nodes)
            {
                TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
                tn.Tag = n;//n.objectOID;
                //if (n.objectOID != Guid.Empty)
                //    tn.ForeColor = Color.Red;
                treeNode.Nodes.Add(tn);
                LoadNode(tn, n);
            }
        }

        private void LoadShops()
        {
            Shops = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).Shops;
            var shop = Shops.Where(s => s.IsActive).OrderBy(s => s.ShopType).First();
            _shopType = shop.ShopType;
        }

        private void LoadLookup()
        {
            string xml = lp.GetLookupValues("CDefaultTitle");
            DefaultTypeSelect.LoadXml(xml, "defaultTypeN");
            if (Additionals.ContainsKey("DefaultTypeSelect"))
            {
                DefaultTypeSelect.SelectedIndex = int.Parse(Additionals["DefaultTypeSelect"]);
            }
            else DefaultTypeSelect.SelectedIndex = 0;
            DefaultTypeSelect.SelectedIndexChanged += DefaultTypeChanged;
        }

        private void DefaultTypeChanged(object sender, PositionChangedEventArgs e)
        {
            LoadGrid();
        }

        private void BrowseDefaultTitle_BeforeClosing(object sender, EventArgs e)
        {
            Additionals["DefaultTypeSelect"] = DefaultTypeSelect.SelectedIndex.ToString();
        }

        private void LoadGrid()
        {
            int defaultType = DefaultTypeSelect.SelectedValue;
            var sql = $@"
SELECT
    tag = 1
    , o.OID
    , o.title
    , o.keywords
    , o.pageDescription
    , dt.name
    , dt.isActive
    , t.themeName
	, n1.lft
    , o.dateCreate
FROM
    t_DefaultTitle dt
    left join t_Theme t on dt.theme = t.OID
    inner join t_Object o on dt.OID = o.OID and dt.defaultType = {defaultType}
    inner join t_Nodes n1 on dt.theme = n1.object 
    inner join t_Nodes n2 on n2.lft BETWEEN n1.lft and n1.rgt and n2.object = '{_selectedCategory}'
    inner join t_Nodes n3 on n1.lft BETWEEN n3.lft and n3.rgt and n3.nodeID = {_catalogRootNode}
UNION ALL
SELECT 
    2
    , o.OID
    , o.title
    , o.keywords
    , o.pageDescription
    , dt.name
    , dt.isActive
    , ''
	, 0
    , o.dateCreate
FROM
    t_DefaultTitle dt
    inner join t_Object o on dt.OID = o.OID and dt.defaultType = {defaultType} and dt.theme is null
ORDER BY
    tag DESC, lft, dateCreate
";
            _titlesDataSet = new DataSetISM(lp.GetDataSetSql(sql));
            TitlesGrid.SetDataBinding(_titlesDataSet, "Table");
            TitlesGrid.DisableNewDel();
        }

        private void CatalogTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var tvSelect = (TreeView)sender;
            if (tvSelect.SelectedNode == null) return;
            tvSelect.SelectedNode.Expand();

            _selectedCategory = ((Node) tvSelect.SelectedNode.Tag).objectOID;
            LoadGrid();
        }

        protected virtual void showEditForm(Guid OID)
        {
            var form = FormSelection.GetEditForm(TitlesGrid.StockClass);
            if (form == null) return;
            form.ObjectOID = OID;
            form.MdiParent = ParentForm;
            form.ReloadGrid += LoadGrid;
            form.Show();
            form.Activate();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            showEditForm(Guid.Empty);
        }

        private void TitlesGrid_DoubleClick(object sender, EventArgs e)
        {
            if (TitlesGrid.Hit.Type == DataGrid.HitTestType.Cell || TitlesGrid.Hit.Type == DataGrid.HitTestType.RowHeader)
            {
                DataRow dr = TitlesGrid.GetSelectedRow();
                showEditForm((Guid)dr["OID"]);
            }

        }
    }
}
