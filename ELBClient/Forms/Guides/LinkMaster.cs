﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using MetaData;



namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmLinkMaster.
	/// </summary>
	public partial class LinkMaster : EditForm
	{
		private int catalogNode;
		public int CatalogNode {
			get {
				return this.catalogNode;
			}
			set {
				this.catalogNode = value;
			}
		}
		private int masterNode;
		public int MasterNode {
			get {
				return this.masterNode;
			}
			set {
				this.masterNode = value;
			}
		}
		private TreeProvider.TreeProvider treeProvider = Classes.ServiceUtility.TreeProvider;
		private Classes.Tree treeSite, treeMaster;
		private DataSetISM dsLinks;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		public LinkMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			treeSite		= new Tree(0, int.MaxValue);
			treeMaster = new Tree(0, int.MaxValue);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void fmLinkMaster_Load(object sender, System.EventArgs e) {
			LoadCatalogs();
		}
		private void LoadCatalogs() {
			LoadSiteCatalog();
			LoadMasterCatalog();
			LoadLinks();
			LoadSiteTree();
			LoadMasterTree();
		}
		private void LoadSiteCatalog() {
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\""+CatalogNode+"\" />";
			DataSet dataSet1 = treeProvider.GetTreeContent(xml);
			Stack nodes = new Stack();
			nodes.Push(this.treeSite);

			int prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows) {
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if(nodeID != prevNode) {
					node = new Node((int) dr["lft"], (int) dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty: (Guid) dr["OID"];
					node.objectNK = dr["NK"].ToString();
					node.objectNKRus = dr["label"].ToString();
					node.className = dr["className"].ToString();
					while(nodes.Peek() != null && ((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else {
					if(node != null) {
						node.AddString.Rows.Add(new string[3] {(string)dr["ordValue"], (string)dr["value"], (string)dr["url"]});
						node.AddString.AcceptChanges();
					}
				}
			}		
		}
		private void LoadMasterCatalog() {
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\""+MasterNode+"\" />";
			DataSet dataSet1 = treeProvider.GetTreeContent(xml);
			Stack nodes = new Stack();
			nodes.Push(this.treeMaster);

			int prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows) {
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if(nodeID != prevNode) {
					node = new Node((int) dr["lft"], (int) dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty: (Guid) dr["OID"];
					node.objectNK = dr["NK"].ToString();
					node.objectNKRus = dr["label"].ToString();
					node.className = dr["className"].ToString();
					while(nodes.Peek() != null && ((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else {
					if(node != null) {
						node.AddString.Rows.Add(new string[3] {(string)dr["ordValue"], (string)dr["value"], (string)dr["url"]});
						node.AddString.AcceptChanges();
					}
				}
			}		
		}
		private void LoadLinks() {
			string sql = @"
SELECT 
	tm.OID, 
	masterOID, 
	t.themeName,
	c.name 
FROM 
	t_ThemeMasters tm
	inner join t_Theme t on tm.OID = t.OID
	inner join t_MasterCatalog c on tm.masterOID = c.OID
";
			dsLinks = new DataSetISM(lp.GetDataSetSql(sql));
		}
		private void LoadSiteTree() {
			tvSiteTree.Nodes.Clear();
			foreach(Node n in this.treeSite.Nodes) {
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tvSiteTree.Nodes.Add(tn);
				if(/*n.Nodes.Count == 0 && */n.objectOID != Guid.Empty) 
						LoadLinksNodes(tn);
				LoadNode(tn, n);
			}
		}
		private void LoadMasterTree() {
			tvMasterTree.Nodes.Clear();
			foreach(Node n in this.treeMaster.Nodes) {
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tvMasterTree.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node) {
			foreach(Node n in node.Nodes) {
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				if(/*n.Nodes.Count == 0 && */n.objectOID != Guid.Empty) 
					LoadLinksNodes(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadLinksNodes(TreeNode tn) {
			DataView dv = new DataView(dsLinks.Table, "OID = '"+(tn.Tag as Node).objectOID+"'", null, DataViewRowState.CurrentRows);
			for(int i = 0; i < dv.Count; i++){
				DataRowView drv = dv[i];
				TreeNode node = new TreeNode(drv["name"].ToString());
				node.ForeColor = Color.Green;
				Node n = new Node(0, 0);
				n.objectOID = (Guid) drv["masterOID"];
				node.Tag = n;
				tn.Nodes.Add(node);
			}
		}
		private void tvMasterTree_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e) {
			Node n = (Node)(e.Item as TreeNode).Tag;
			if(n.Left + 1 == n.Right && n.objectOID != Guid.Empty)	tvMasterTree.DoDragDrop(e.Item, DragDropEffects.Move); 
		}

		private void tvSiteTree_DragOver(object sender, System.Windows.Forms.DragEventArgs e) {
			Point pt = tvSiteTree.PointToClient(new Point(e.X, e.Y));
			TreeNode tNode = tvSiteTree.GetNodeAt(pt);
			if(tNode != null) {
				Node dest = (Node)tNode.Tag;
				if(dest.Left + 1 == dest.Right && dest.objectOID != Guid.Empty) {
					e.Effect = DragDropEffects.Move;
				}
				else e.Effect = DragDropEffects.None;
			}
		}

		private void tvSiteTree_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) {
			if(e.Data.GetDataPresent(typeof(TreeNode))) {
				TreeNode source = (TreeNode)e.Data.GetData(typeof(TreeNode));
				Point pt = tvSiteTree.PointToClient(new Point(e.X, e.Y));
				TreeNode dest = tvSiteTree.GetNodeAt(pt);
				Guid OID = (dest.Tag as Node).objectOID;
				Guid masterOID = (source.Tag as Node).objectOID;
				string sql = @"
IF EXISTS (SELECT * FROM t_ThemeMasters WHERE OID = '"+OID+@"' And masterOID = '"+masterOID+@"') Begin
	SELECT 0
End
else Begin
	INSERT INTO t_ThemeMasters VALUES('"+OID+@"', '"+masterOID+@"')
	SELECT 1
End
";		
				DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
				op.ClearCache();
				if((int)ds.Table.Rows[0][0] == 0) ShowError("Данная привязка уже есть");
				else {
					TreeNode n = new TreeNode(source.Text);
					n.ForeColor = Color.Green;
					Node nd = new Node(0, 0);
					nd.objectOID = masterOID;
					n.Tag = nd;
					dest.Nodes.Add(n);
				}
			}
		}

		private void tvSiteTree_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e) {
			if((e.Item as TreeNode).ForeColor == Color.Green) {
				tvSiteTree.DoDragDrop(e.Item, DragDropEffects.Move); 
			}
		}

		private void tvMasterTree_DragEnter(object sender, System.Windows.Forms.DragEventArgs e) {
			e.Effect = DragDropEffects.Move;
		}

		private void tvMasterTree_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) {
			if(e.Data.GetDataPresent(typeof(TreeNode))) {
				TreeNode source = (TreeNode)e.Data.GetData(typeof(TreeNode));
				Guid masterOID = (source.Tag as Node).objectOID;
				TreeNode parent = source.Parent;
				Guid OID = (parent.Tag as Node).objectOID;
				string sql = @"
	DELETE FROM t_ThemeMasters WHERE OID = '"+OID+@"' And masterOID = '"+masterOID+@"'
";		
				ObjectProvider.ObjectProvider prov = Classes.ServiceUtility.ObjectProvider;
				try {
					prov.ExecuteCommand(sql);
					parent.Nodes.Remove(source);
				}
				catch {
					ShowError("Ошибка удаления");
				}
			}
		}

		private void menuItem1_Click(object sender, System.EventArgs e) {
			TreeNode selected = tvMasterTree.SelectedNode;
			Node n = (Node)selected.Tag;
			Guid masterThemeOID = n.objectOID;
			LinkedThemes fm = new LinkedThemes();
			fm.SelectedMaster = masterThemeOID;
			fm.MdiParent = this.MdiParent;
			fm.Show();
		}

		private void tvMasterTree_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) {
			TreeNode node = tvMasterTree.GetNodeAt(new Point(e.X, e.Y));
			if (node != null) {
				tvMasterTree.SelectedNode = node;
			}
		}

	}
}
