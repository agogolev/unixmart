﻿using System;
using System.Data;
using System.ComponentModel;
using ELBClient.Classes;
using MetaData;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListCommodityGroup.
	/// </summary>
	public partial class ListCommodityGroup : ListForm
	{
		public int GroupType { get; set; }
		public ListCommodityGroup()
		{
			InitializeComponent();
			mainDataGrid1.dataGridISM1.ColumnDragEnabled = false;
			extendedDataGridSelectorColumn1.ButtonClick += extendedDataGridSelectorColumn1_ButtonClick;
			mainDataGrid1.BeforeShowEditForm += ApplyGroupType;
		}

		private void ApplyGroupType(EditForm form)
		{
			var frm = form as EditCommodityGroup;
			frm.GroupType = GroupType;
		}
		void extendedDataGridSelectorColumn1_ButtonClick(object sender, ColumnMenuExtender.DataGridCellEventArgs e)
		{
			BusinessProvider.BusinessProvider provider = ServiceUtility.BusinessProvider;
			BusinessProvider.ExceptionISM exISM = provider.GenerateCommodityGroupXml((Guid)((DataRowView)e.CurrentRow)["OID"]);
			if (exISM != null)
			{
				ShowError(string.Format("{0}/r/n{1}", exISM.LiteralMessage, exISM.LiteralStackTrace));
			}
			mainDataGrid1.LoadData();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void fmListCommodityGroup_Load(object sender, System.EventArgs e)
		{
			if (GroupType != 1)
			{
				extendedDataGridTableStyle1.GridColumnStyles.Remove(formattableTextBoxColumn2);
				extendedDataGridTableStyle1.GridColumnStyles.Remove(extendedDataGridSelectorColumn1);
			}
			mainDataGrid1.dataGridISM1.CreateAuxFilter("groupType", FilterVerb.Equal, false, new object[] { GroupType });
			mainDataGrid1.LoadData();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (GroupType != 1)
			{
				extendedDataGridTableStyle1.GridColumnStyles.Add(formattableTextBoxColumn2);
				extendedDataGridTableStyle1.GridColumnStyles.Add(extendedDataGridSelectorColumn1);
			}
			base.OnClosing(e);
		}

	}
}
