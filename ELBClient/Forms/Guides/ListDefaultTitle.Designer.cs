using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListDefaultTitle
    {
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
        private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;

        private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
            this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
            this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Название";
            this.formattableTextBoxColumn1.MappingName = "name";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
            this.formattableTextBoxColumn1.NullText = "";
            this.formattableTextBoxColumn1.Width = 150;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = "defaultType?name";
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Для чего";
            this.formattableTextBoxColumn3.MappingName = "defaultTypeName";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
            this.formattableTextBoxColumn3.NullText = "";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableBooleanColumn1
            // 
            this.formattableBooleanColumn1.FieldName = null;
            this.formattableBooleanColumn1.FilterFieldName = null;
            this.formattableBooleanColumn1.HeaderText = "Активность";
            this.formattableBooleanColumn1.MappingName = "isActive";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn1, this.menuFilterSort1);
            this.formattableBooleanColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Title";
            this.formattableTextBoxColumn2.MappingName = "title";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Keywords";
            this.formattableTextBoxColumn4.MappingName = "keywords";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn4, this.menuFilterSort1);
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "Description";
            this.formattableTextBoxColumn5.MappingName = "pageDescription";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn5, this.menuFilterSort1);
            this.formattableTextBoxColumn5.Width = 75;
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = "theme?themeName";
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "Категория";
            this.formattableTextBoxColumn6.MappingName = "themeName";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
            this.formattableTextBoxColumn6.Width = 150;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = null;
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "dd.MM.yyyy HH:mm";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "Создан";
            this.formattableTextBoxColumn7.MappingName = "dateCreate";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
            // 
            // mainDataGrid1
            // 
            this.mainDataGrid1.AdditionalInfo = "";
            this.mainDataGrid1.ClassName = "CDefaultTitle";
            this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
            this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.mainDataGrid1.Name = "mainDataGrid1";
            this.mainDataGrid1.Size = new System.Drawing.Size(542, 374);
            this.mainDataGrid1.TabIndex = 2;
            this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.mainDataGrid1.BeforeLoad += new System.EventHandler(this.mainDataGrid1_BeforeLoad);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.AllowSorting = false;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn3,
            this.formattableBooleanColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // ListDefaultTitle
            // 
            this.ClientSize = new System.Drawing.Size(542, 374);
            this.Controls.Add(this.mainDataGrid1);
            this.MinimumSize = new System.Drawing.Size(550, 404);
            this.Name = "ListDefaultTitle";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Тайтлы по умолчанию";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
    }
}
