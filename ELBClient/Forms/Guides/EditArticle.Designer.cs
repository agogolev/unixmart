using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditArticle
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox tbHeader;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private ELBClient.UserControls.ExtDateTimePicker dtpDate;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TextBox tbAnnotation;
		private System.Windows.Forms.Button btnEdit;
		private ColumnMenuExtender.DataBoundComboBox TypeOfArticleCombo;
		private System.Windows.Forms.Button btnImages;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtPageDescription;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtKeywords;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtTitle;
		private TabControl tabControl1;
		private TabPage tabPage1;
		private TabPage tabPage2;
		private ColumnMenuExtender.DataGridISM Tags;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbHeader = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpDate = new ELBClient.UserControls.ExtDateTimePicker();
            this.txtPageDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.tbAnnotation = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.TypeOfArticleCombo = new ColumnMenuExtender.DataBoundComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Tags = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ThemesDataGrid = new ELBClient.UserControls.AddDelDataGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnImages = new System.Windows.Forms.Button();
            this.TranslitButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ShortLinkText = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TypeOfArticleCombo)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tags)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 100);
            this.panel4.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(846, 451);
            this.panel2.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(846, 414);
            this.panel3.TabIndex = 33;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(846, 414);
            this.tabControl1.TabIndex = 46;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.TranslitButton);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.ShortLinkText);
            this.tabPage1.Controls.Add(this.tbHeader);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.dtpDate);
            this.tabPage1.Controls.Add(this.txtPageDescription);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtKeywords);
            this.tabPage1.Controls.Add(this.tbAnnotation);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtTitle);
            this.tabPage1.Controls.Add(this.TypeOfArticleCombo);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(838, 388);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Основные";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tbHeader
            // 
            this.tbHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbHeader.Location = new System.Drawing.Point(126, 7);
            this.tbHeader.Name = "tbHeader";
            this.tbHeader.Size = new System.Drawing.Size(694, 20);
            this.tbHeader.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 18);
            this.label6.TabIndex = 45;
            this.label6.Text = "Description:";
            // 
            // dtpDate
            // 
            this.dtpDate.BoundValue = new System.DateTime(2012, 7, 31, 14, 7, 33, 988);
            this.dtpDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpDate.LinkedTo = null;
            this.dtpDate.Location = new System.Drawing.Point(126, 66);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.ReadOnly = false;
            this.dtpDate.ShowButtons = true;
            this.dtpDate.Size = new System.Drawing.Size(313, 20);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.Value = new System.DateTime(2012, 7, 31, 14, 7, 33, 988);
            // 
            // txtPageDescription
            // 
            this.txtPageDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPageDescription.Location = new System.Drawing.Point(126, 176);
            this.txtPageDescription.Name = "txtPageDescription";
            this.txtPageDescription.Size = new System.Drawing.Size(696, 20);
            this.txtPageDescription.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "Дата публикации:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(6, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 44;
            this.label7.Text = "Keywords:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "Заголовок:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKeywords
            // 
            this.txtKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeywords.Location = new System.Drawing.Point(126, 149);
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(696, 20);
            this.txtKeywords.TabIndex = 41;
            // 
            // tbAnnotation
            // 
            this.tbAnnotation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnnotation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAnnotation.Location = new System.Drawing.Point(126, 204);
            this.tbAnnotation.Multiline = true;
            this.tbAnnotation.Name = "tbAnnotation";
            this.tbAnnotation.Size = new System.Drawing.Size(698, 146);
            this.tbAnnotation.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 19);
            this.label8.TabIndex = 43;
            this.label8.Text = "Title:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 25);
            this.label3.TabIndex = 31;
            this.label3.Text = "Аннотация:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTitle
            // 
            this.txtTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitle.Location = new System.Drawing.Point(126, 121);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(696, 20);
            this.txtTitle.TabIndex = 40;
            // 
            // TypeOfArticleCombo
            // 
            this.TypeOfArticleCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TypeOfArticleCombo.Location = new System.Drawing.Point(126, 93);
            this.TypeOfArticleCombo.Name = "TypeOfArticleCombo";
            this.TypeOfArticleCombo.SelectedValue = -1;
            this.TypeOfArticleCombo.Size = new System.Drawing.Size(208, 20);
            this.TypeOfArticleCombo.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 18);
            this.label4.TabIndex = 28;
            this.label4.Text = "Тип статьи:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Tags);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(838, 237);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Теги";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Tags
            // 
            this.Tags.BackgroundColor = System.Drawing.Color.White;
            this.Tags.DataMember = "";
            this.Tags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tags.FilterString = null;
            this.Tags.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.Tags.Location = new System.Drawing.Point(3, 3);
            this.Tags.Name = "Tags";
            this.Tags.Order = null;
            this.Tags.Size = new System.Drawing.Size(832, 231);
            this.Tags.TabIndex = 0;
            this.Tags.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.Tags;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "Table";
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Номер пп";
            this.formattableTextBoxColumn1.MappingName = "ordValue";
            this.formattableTextBoxColumn1.ReadOnly = true;
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Название";
            this.formattableTextBoxColumn2.MappingName = "propValue";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ThemesDataGrid);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(838, 237);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Категории";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ThemesDataGrid
            // 
            this.ThemesDataGrid.AddDelDataGridColumnNames = null;
            this.ThemesDataGrid.ClassName = "CTheme";
            this.ThemesDataGrid.ColumnHeaderNames = "Название";
            this.ThemesDataGrid.ColumnNames = "themeName";
            this.ThemesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ThemesDataGrid.DT = null;
            this.ThemesDataGrid.Location = new System.Drawing.Point(3, 3);
            this.ThemesDataGrid.MultiContainer = null;
            this.ThemesDataGrid.Name = "ThemesDataGrid";
            this.ThemesDataGrid.Size = new System.Drawing.Size(832, 231);
            this.ThemesDataGrid.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnImages);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 414);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(846, 37);
            this.panel1.TabIndex = 32;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 27);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Закрыть";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(768, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 27);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(92, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(176, 27);
            this.btnEdit.TabIndex = 7;
            this.btnEdit.Text = "Редактировать содержание";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnImages
            // 
            this.btnImages.Location = new System.Drawing.Point(276, 5);
            this.btnImages.Name = "btnImages";
            this.btnImages.Size = new System.Drawing.Size(176, 27);
            this.btnImages.TabIndex = 8;
            this.btnImages.Text = "Картинки статьи";
            this.btnImages.Click += new System.EventHandler(this.btnImages_Click);
            // 
            // TranslitButton
            // 
            this.TranslitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TranslitButton.Location = new System.Drawing.Point(717, 34);
            this.TranslitButton.Name = "TranslitButton";
            this.TranslitButton.Size = new System.Drawing.Size(104, 24);
            this.TranslitButton.TabIndex = 51;
            this.TranslitButton.Text = "Транслитерация";
            this.TranslitButton.Click += new System.EventHandler(this.TranslitButton_Click);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 20);
            this.label5.TabIndex = 50;
            this.label5.Text = "Красивая ссылка:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ShortLinkText
            // 
            this.ShortLinkText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShortLinkText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ShortLinkText.Location = new System.Drawing.Point(126, 36);
            this.ShortLinkText.Name = "ShortLinkText";
            this.ShortLinkText.Size = new System.Drawing.Size(591, 20);
            this.ShortLinkText.TabIndex = 49;
            // 
            // EditArticle
            // 
            this.ClientSize = new System.Drawing.Size(846, 451);
            this.Controls.Add(this.panel2);
            this.Name = "EditArticle";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Редактирование статьи";
            this.Load += new System.EventHandler(this.fmEditArticle_Load);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TypeOfArticleCombo)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Tags)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private TabPage tabPage3;
		private UserControls.AddDelDataGrid ThemesDataGrid;
        private Button TranslitButton;
        private Label label5;
        private TextBox ShortLinkText;
    }
}
