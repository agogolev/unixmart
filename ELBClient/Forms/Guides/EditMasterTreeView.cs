﻿using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using System.Xml;
using System.Data;
using MetaData;


namespace ELBClient.Forms.Guides {
	/// <summary>
	/// Summary description for fmEditTree.
	/// </summary>
	public partial class EditMasterTreeView : EditForm {
		public int ShopType { get; set; }

		public int ParentNode { get; set; }

		public int SecondParentNode { get; set; }

		private readonly TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
		private readonly Tree _tree;
		private string _oldFullPath = "";
		private XmlDocument _nodeRules;
		

		public EditMasterTreeView() {
			InitializeComponent();
			Object = new CTree();
			_tree = new Tree(0, int.MaxValue);
			_nodeRules = new XmlDocument();
			_nodeRules.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><root></root>");
			if(ShopType != 1) {
				btnToExcel.Visible = false;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmEditTree_Load(object sender, EventArgs e) {
			LoadObjects();
			BuildTree();
			LoadTree();
		}

		private void LoadObjects() {
			//LoadObject("treeName", null);

			//Filling OID
			string xml;
			if(ParentNode == 0) {
				xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree OID = \""+ObjectOID+"\" />";
			}
			else {
				xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\""+ ParentNode+"\" withParent=\"true\"/>";
			}
			dataSet1 = treeProvider.GetTreeContent(xml);
			if(SecondParentNode != 0) {
				xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\""+ SecondParentNode+"\" withParent=\"true\"/>";
				dataSet2 = treeProvider.GetTreeContent(xml);
			}
			var doc = GetResource("nodeRules.xml");
			var n = doc.DocumentElement.SelectSingleNode("treeView[@OID='"+ObjectOID.ToString().ToUpper()+"']/@answerOID");
			if (n == null) return;
			var answerOID = n.Value;
			var ans = new CAnswer();
			ans.LoadXml(op.GetObject(answerOID, "body", null));
			_nodeRules = new XmlDocument();
			_nodeRules.LoadXml(ans.Body);
		}

		//Filling Tree
		private void BuildTree() {
			var nodes = new Stack();
			nodes.Push(_tree);

			var prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows) {
				var nodeID = (int)dr["nodeID"];
				if(nodeID != prevNode) {
					var node = new Node((int) dr["lft"], (int) dr["rgt"])
					            	{
					            		NodeId = nodeID,
					            		Name = dr["nodeName"].ToString().RestoreCr(),
					            		objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid) dr["OID"],
					            		objectNK = dr["NK"].ToString().RestoreCr(),
					            		objectNKRus = dr["label"].ToString().RestoreCr(),
					            		className = dr["className"].ToString()
					            	};
					// == DBNull.Value ? null: (string) dr["NK"];
					while(((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
			}
			if (dataSet2 == null) return;
			prevNode = 0;
			foreach(DataRow dr in dataSet2.Tables["table"].Rows) {
				var nodeID = (int)dr["nodeID"];
				if (nodeID == prevNode) continue;
				var node = new Node((int) dr["lft"], (int) dr["rgt"])
				            	{
				            		NodeId = nodeID,
				            		Name = dr["nodeName"].ToString().RestoreCr(),
				            		objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid) dr["OID"],
				            		objectNK = dr["NK"].ToString().RestoreCr(),
				            		objectNKRus = dr["label"].ToString().RestoreCr(),
				            		className = dr["className"].ToString()
				            	};
				// == DBNull.Value ? null: (string) dr["NK"];
				while(((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
				((NodeContainer) nodes.Peek()).Nodes.Add(node);
				if ((node.Left+1) != node.Right) {
					nodes.Push(node);
				}
				prevNode = nodeID;
			}
		}

		private void LoadTree() {
			tv1.Nodes.Clear();
			foreach(Node n in _tree.Nodes) {
				var tn =
					new TreeNode(n.Name != ""
					             	? n.Name.Replace("\r\n", " ")
					             	: (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>"))) {Tag = n};
				//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tv1.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void LoadNode(TreeNode treeNode, Node node) {
			foreach(Node n in node.Nodes) {
				var tn =
					new TreeNode(n.Name != ""
					             	? n.Name.Replace("\r\n", " ")
					             	: (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>"))) {Tag = n};
				//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		//Add Del Edit nodes
		private void miAdd_Click(object sender, EventArgs e) {
			var selectedNode = tv1.SelectedNode;
			var nDescr = GetNodeDescription();
			var nl = nDescr.SelectNodes(".//object/@className");
			var AllowedClassName = new string[nl.Count];
			for(int i = 0; i < nl.Count; i++) {
				AllowedClassName[i] = nl[i].Value;
			}
			var fm = new EditTreeViewNode {AllowedClassName = AllowedClassName};
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			//1
			
			var doc = new XmlDocument();
			var root = doc.CreateNode(XmlNodeType.Element, "node", "");
				
			var attr = doc.CreateAttribute("parentNode");
			attr.Value = ((Node)selectedNode.Tag).NodeId.ToString();
			root.Attributes.Append(attr);

			var n = new Node
			        	{
			        		Name = fm.NodeName.Prop,
			        		objectOID = fm.NodeObject.Prop,
			        		objectNK = fm.NodeObjectNK.Prop,
			        		objectNKRus = fm.NodeObjectNKRus,
			        		className = fm.NodeObjectNKRus
			        	};

			if(n.Name != "") 
			{
				attr = doc.CreateAttribute("nodeName");
				attr.Value = n.Name;
				root.Attributes.Append(attr);
			}
			if(n.objectOID != Guid.Empty) {
				attr = doc.CreateAttribute("object");
				attr.Value = n.objectOID.ToString();
				root.Attributes.Append(attr);
			}
			var added = n.AddString.GetChanges(DataRowState.Added);
			if(added != null) {
				foreach(DataRow dr in added.Rows) {
					XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
					n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
					n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
					n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
				}
			}

			doc.AppendChild(root);
			n.NodeId = treeProvider.CreateNode(doc.OuterXml);
			n.AddString.AcceptChanges();

			var tn = new TreeNode(n.Name.Replace("\r\n", " "));
			if(n.objectOID != Guid.Empty)
				tn.ForeColor = Color.Red;
			tn.Tag = n;
			selectedNode.Nodes.Insert(0, tn);
		}
		
		private void miAddSibling_Click(object sender, EventArgs e) {
			var selectedNode = tv1.SelectedNode;
			var nDescr = GetNodeDescription();
			string [] AllowedClassName = null;
			if(nDescr != null) {
				var nl = nDescr.SelectNodes(".//object/@className");
				AllowedClassName = new string[nl.Count];
				for(int i = 0; i < nl.Count; i++) {
					AllowedClassName[i] = nl[i].Value;
				}
			}

			var fm = new EditTreeViewNode {AllowedClassName = AllowedClassName};
			if (fm.ShowDialog(this) == DialogResult.OK) {//1
				var doc = new XmlDocument();
				var root = doc.CreateNode(XmlNodeType.Element, "node", "");
			
				var attr1 = doc.CreateAttribute("leftSibling");
				attr1.Value = ((Node)selectedNode.Tag).NodeId.ToString();
				root.Attributes.Append(attr1);

				var n = new Node
				        	{
				        		Name = fm.NodeName.Prop,
				        		objectOID = fm.NodeObject.Prop,
				        		objectNK = fm.NodeObjectNK.Prop,
				        		objectNKRus = fm.NodeObjectNKRus,
				        		className = fm.NodeObjectNKRus
				        	};

				if(n.Name != "") {
					XmlAttribute attr = doc.CreateAttribute("nodeName");
					attr.Value = n.Name;
					root.Attributes.Append(attr);
				}
				if(n.objectOID != Guid.Empty) {
					XmlAttribute attr = doc.CreateAttribute("object");
					attr.Value = n.objectOID.ToString();
					root.Attributes.Append(attr);
				}
				var added = n.AddString.GetChanges(DataRowState.Added);
				if(added != null) {
					foreach(DataRow dr in added.Rows) {
						XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
					}
				}

				doc.AppendChild(root);
				n.NodeId = treeProvider.CreateNode(doc.OuterXml);
				n.AddString.AcceptChanges();

				var tn = new TreeNode(n.Name.Replace("\r\n", " "));
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tn.Tag = n;
				var parent = tv1.SelectedNode.Parent;
				var indx = tv1.SelectedNode.Index;
				
				if(parent != null)
					parent.Nodes.Insert(indx+1, tn);
				else tv1.Nodes.Insert(indx+1, tn);
			}//1
		}

		private void miEdit_Click(object sender, EventArgs e) {
			var selectedNode = tv1.SelectedNode;
			var nDescr = GetNodeDescription();
			string [] AllowedClassName = null;
			bool allowObject = true;
			if(nDescr != null) {
				var nl = nDescr.SelectNodes(".//object/@className");
				AllowedClassName = new string[nl.Count];
				for(int i = 0; i < nl.Count; i++) {
					AllowedClassName[i] = nl[i].Value;
				}
				if(nDescr.Attributes["object"] != null && nDescr.Attributes["object"].Value == "0") allowObject = false;
			}

			var fm = new EditTreeViewNode {AllowObject = allowObject, AllowedClassName = AllowedClassName};
			var n = (Node)selectedNode.Tag;
			fm.NodeID = n.NodeId;
			fm.NodeName = new StringProperty(n.Name);
			fm.NodeObject = new GuidProperty(n.objectOID);
			fm.NodeObjectNK = new StringProperty(n.objectNK);
			fm.NodeObjectNKRus = n.objectNKRus;

			if (fm.ShowDialog(this) == DialogResult.OK) {
				//1
			
				var doc = new XmlDocument();
				var root = doc.CreateElement("node");
				//Node n1 = (Node)selectedNode.Tag;
			
				var attr1 = doc.CreateAttribute("nodeID");
				attr1.Value = n.NodeId.ToString();
				root.Attributes.Append(attr1);				
				
				if(n.Name != fm.NodeName.Prop) {
					n.Name = fm.NodeName.Prop;
					selectedNode.Text = fm.NodeName.Prop.Replace("\r\n", " ");
					var attr = doc.CreateAttribute("nodeName");
					attr.Value = fm.NodeName.Prop;
					root.Attributes.Append(attr);
				}
				var temp = fm.NodeObject.Prop;
				n.objectNK = fm.NodeObjectNK.Prop;
				if(n.objectOID != temp) {
					n.objectOID = temp;
					n.objectNKRus = fm.NodeObjectNKRus;
					selectedNode.ForeColor = temp != Guid.Empty ? Color.Red : Color.Black;
					var attr = doc.CreateAttribute("object");
					attr.Value = temp.ToString(); 
					root.Attributes.Append(attr);
				}
				var deleted = n.AddString.GetChanges(DataRowState.Deleted);
				if(deleted != null) {
					foreach(DataRow dr in deleted.Rows) {
						XmlNode n2 = root.AppendChild(doc.CreateElement("delete"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue", DataRowVersion.Original];
					}
				}
				var added = n.AddString.GetChanges(DataRowState.Added);
				if(added != null) {
					foreach(DataRow dr in added.Rows) {
						XmlNode n2 = root.AppendChild(doc.CreateElement("add"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
					}
				}
				var updated = n.AddString.GetChanges(DataRowState.Modified);
				if(updated != null) {
					foreach(DataRow dr in updated.Rows) {
						XmlNode n2 = root.AppendChild(doc.CreateElement("update"));
						n2.Attributes.Append(doc.CreateAttribute("ordValue")).Value = (string) dr["ordValue"];
						n2.Attributes.Append(doc.CreateAttribute("value")).Value = (string) dr["value"];
						n2.Attributes.Append(doc.CreateAttribute("url")).Value = (string) dr["url"];
					}
				}
				doc.AppendChild(root);
				treeProvider.UpdateNode(doc.OuterXml);
				n.AddString.AcceptChanges();
			}//1
		}

		private void miEditObject_Click(object sender, EventArgs e) 
		{
			var selectedNode = tv1.SelectedNode;
			var n = (Node)selectedNode.Tag;
			var frm = FormSelection.GetEditForm(n.className);
			if (frm == null) return;
			frm.ObjectOID = n.objectOID;
			frm.MdiParent = MdiParent;
			frm.Show();
		}
		private void miDel_Click(object sender, EventArgs e) 
		{
			if (
				MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) !=
				DialogResult.Yes) return;
			var selectedNode = tv1.SelectedNode;
		
			if(selectedNode != null) {
				var parent = selectedNode.Parent;
				var indx = selectedNode.Index;

				treeProvider.DeleteNode(((Node)selectedNode.Tag).NodeId);
				
				if(selectedNode.Nodes.Count != 0)
				{
					var tmp = (TreeNode)selectedNode.Clone();

					if(parent != null) {
						foreach(TreeNode child in tmp.Nodes)
							parent.Nodes.Insert(indx++,child);
					}
					else {	//Root
						foreach(TreeNode child in tmp.Nodes)
							tv1.Nodes.Insert(indx++,child);
					}
				}
				selectedNode.Remove();
			}
		}

		//Working With Tree
		private void tv1_MouseDown(object sender, MouseEventArgs e) {
			if(e.Button == MouseButtons.Right) { 
				tv1.SelectedNode = tv1.GetNodeAt(e.X ,e.Y);
				if(tv1.SelectedNode == null) {
					tv1.ContextMenu = null;
				}
				else {
					tv1.ContextMenu = contextMenu1;
					GetMenuState();
				}
			}  
		}
		private void GetMenuState() {
			miAdd.Visible = true;
			miAddSibling.Visible = true;
			miDel.Visible = true;
			miEdit.Visible = true;
			miEditObject.Visible = true;
			var node = (Node)tv1.SelectedNode.Tag;
			var n = GetNodeDescription();
				
			if(n != null) {
				var attr = n.Attributes["delete"];
				if(attr != null && attr.Value == "0") miDel.Visible = false;
				attr = n.Attributes["child"];
				if(attr != null && attr.Value == "0") miAdd.Visible = false;
				attr = n.Attributes["sibling"];
				if(attr != null && attr.Value == "0") miAddSibling.Visible = false;
			}
			miEditObject.Visible = node.objectOID != Guid.Empty;
			if(tv1.SelectedNode.Nodes.Count != 0) {
				miDel.Visible = false; // Ноды с потомками удалять нельзя
			}
		}
		private XmlNode GetNodeDescription() {
			var tNode = tv1.SelectedNode;
			var node = (Node)tNode.Tag;
			var n = _nodeRules.DocumentElement.SelectSingleNode("node[@nodeID = "+node.NodeId+"]");
			if(n != null) return n;
			var parent = tNode.Parent;
			return parent == null ? null : GetNodeDescription(parent);
		}
		private XmlNode GetNodeDescription(TreeNode tNode) {
			var node = (Node)tNode.Tag;
			var n = _nodeRules.DocumentElement.SelectSingleNode("node[@nodeID = "+node.NodeId+"]");
			if(n != null && n.SelectSingleNode("childNode") != null) return n.SelectSingleNode("childNode");
			var parent = tNode.Parent;
			return parent == null ? null : GetNodeDescription(parent);
		}

		private void tv1_MouseMove(object sender, MouseEventArgs e) { 
			var tn = tv1.GetNodeAt(e.X, e.Y);

			if (tn == null) return;
			var currentFullPath = tn.FullPath;
 
			if(currentFullPath != _oldFullPath) { 
				_oldFullPath = currentFullPath;
 
				if(((Node)tn.Tag).objectNK != "") {
					if(toolTip1.Active) 
						toolTip1.Active = false; //turn it off 
					toolTip1.SetToolTip(tv1, string.Format("Объект: {0}\nКласс: {1}", ((Node)tn.Tag).objectNK, ((Node)tn.Tag).objectNKRus)); 
					toolTip1.Active = true; //make it active so it can show 
				}
				else {
					if(toolTip1.Active) 
						toolTip1.Active = false; //turn it off 
				}
			}
		}

		private void miUp_Click(object sender, EventArgs e) {
			//перемещение нода вверх
			var selectedNode = tv1.SelectedNode;
			var prevNode = selectedNode.PrevNode;
			if(prevNode != null) {
				try {
					var nodeID = ((Node)selectedNode.Tag).NodeId;
					op.ExecuteCommand("spShiftNode @nodeID='"+nodeID+"', @up=1");
					prevNode.Parent.Nodes.Remove(prevNode);
					selectedNode.Parent.Nodes.Insert(selectedNode.Index+1, prevNode);
				}
				catch{
					ShowError("Не удалось передвинуть элемент");
				}
			}
		}

		private void miDown_Click(object sender, EventArgs e) {
			//перемещение нода вниз
			var selectedNode = tv1.SelectedNode;
			var nextNode = selectedNode.NextNode;
			if (nextNode == null) return;
			try {
				int nodeID = ((Node)selectedNode.Tag).NodeId;
				op.ExecuteCommand("spShiftNode @nodeID="+nodeID+", @up=0");
				selectedNode.Parent.Nodes.Remove(selectedNode);
				nextNode.Parent.Nodes.Insert(nextNode.Index+1, selectedNode);
			}
			catch{
				ShowError("Не удалось передвинуть элемент");
			}
		}

		private void tv1_DoubleClick(object sender, EventArgs e) {
			if(tv1.SelectedNode != null) {
				var node = (Node)tv1.SelectedNode.Tag;
				if(node.objectNK != "") miEditObject_Click(tv1, EventArgs.Empty);
			}
		}
	}
}
