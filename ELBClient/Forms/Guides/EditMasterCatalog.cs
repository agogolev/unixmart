﻿using System;
using System.Data;
using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	public partial class EditMasterCatalog : EditForm
	{
		public override string ObjectNK
		{
			get
			{
				return ((CMasterCatalog) Object).Name;
			}
		}

		public EditMasterCatalog()
		{
			InitializeComponent();
			Object = new CMasterCatalog();
			extendedDataGridSelectorColumn1.ButtonClick += extendedDataGridSelectorColumn1_ButtonClick;
		}

		void extendedDataGridSelectorColumn1_ButtonClick(object sender, DataGridCellEventArgs e)
		{
			var drv = (DataRowView)e.CurrentRow;
			var itemsDlg = new ListDialog("CCompany", new[] { "companyName" }, new[] { "Название компании" });
			if (itemsDlg.ShowDialog(this) != DialogResult.OK) return;
			drv["brandOID"] = itemsDlg.SelectedOID;
			drv["brandName"] = itemsDlg.SelectedRow["companyName"].ToString();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		#endregion

		private void btnSave_Click(object sender, EventArgs e)
		{
			SaveObject();
		}

		protected override void SaveObject()
		{
			Object.ExtMultiProp = AddPricesSource.GetChangesXml("Table", "addPrices");
			Object.ExtMultiProp += AddPricesSource.GetChangesXml("Table1", "addPricesBrand");
			base.SaveObject();
		}
		protected override bool IsModified
		{
			get
			{
				return AddPricesSource.IsModified || base.IsModified;
			}
		}
		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void BindFields()
		{
            txtName.DataBindings.Add("Text", Object, "Name");
            IDText.DataBindings.Add("Text", Object, "ObjectID");
		}

		private void fmEditTheme_Load(object sender, EventArgs e)
		{
			LoadLookup();
			LoadData();
			BindFields();
			ActiveControl = btnCancel;
		}

		private void LoadLookup()
		{
			string xml = lp.GetLookupValues("COrder");
			extendedDataGridComboBoxColumn1.ComboBox.LoadXml(xml, "shopTypeN");
			extendedDataGridComboBoxColumn2.ComboBox.LoadXml(xml, "shopTypeN");
		}


		private void LoadData()
		{
			string xml = GetResourceString("fmEditMasterCatalog.xml");
			Object.LoadMultiPropFlag = false;
			xml = LoadObject(xml, CMasterCatalog.ClassCID);
			FillGrids(xml);
		}
		private void FillGrids(string xml)
		{
			AddPricesSource.LoadRowsFromXML("Table", xml, "addPrices");
			AddPricesSource.LoadRowsFromXML("Table1", xml, "addPricesBrand");
			AddPricesDelivery.SetDataBinding(AddPricesSource, "Table");
			AddPrices.SetDataBinding(AddPricesSource, "Table1");
		}
	}
}
