﻿using System;
using System.Data;
using ELBClient.Classes;

namespace ELBClient.Forms.Guides
{
    /// <summary>
    /// Summary description for ListLandingPage.
    /// </summary>
    public partial class ListLandingPage : ListForm
	{
		public ListLandingPage()
		{
			InitializeComponent();
            extendedDataGridSelectorColumn1.ButtonClick += ExtendedDataGridSelectorColumn1_ButtonClick;
		}

        private void ExtendedDataGridSelectorColumn1_ButtonClick(object sender, ColumnMenuExtender.DataGridCellEventArgs e)
        {
            BusinessProvider.BusinessProvider provider = ServiceUtility.BusinessProvider;
            if (((DataRowView) e.CurrentRow)["commodityGroup"] == DBNull.Value) return;
            BusinessProvider.ExceptionISM exISM = provider.GenerateCommodityGroupXml((Guid)((DataRowView)e.CurrentRow)["commodityGroup"]);
            if (exISM != null)
            {
                ShowError($"{exISM.LiteralMessage}/r/n{exISM.LiteralStackTrace}");
            }
            mainDataGrid1.LoadData();

        }
    }
}
