using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListCommodityGroup
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private System.Windows.Forms.ImageList imageList1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;

		private System.ComponentModel.IContainer components;

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListCommodityGroup));
            this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Название";
            this.formattableTextBoxColumn1.MappingName = "name";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = "fCommodityGroupUrl(OID)";
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Url";
            this.formattableTextBoxColumn2.MappingName = "url";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Последняя редакция";
            this.formattableTextBoxColumn3.MappingName = "dateModify";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableBooleanColumn1
            // 
            this.formattableBooleanColumn1.FieldName = null;
            this.formattableBooleanColumn1.FilterFieldName = null;
            this.formattableBooleanColumn1.HeaderText = "Активна";
            this.formattableBooleanColumn1.MappingName = "isActive";
            this.columnMenuExtender1.SetMenu(this.formattableBooleanColumn1, this.menuFilterSort1);
            this.formattableBooleanColumn1.Width = 75;
            // 
            // extendedDataGridSelectorColumn1
            // 
            this.extendedDataGridSelectorColumn1.FieldName = null;
            this.extendedDataGridSelectorColumn1.FilterFieldName = null;
            this.extendedDataGridSelectorColumn1.HeaderText = "Последняя генерация";
            this.extendedDataGridSelectorColumn1.MappingName = "lastGenerate";
            this.columnMenuExtender1.SetMenu(this.extendedDataGridSelectorColumn1, this.menuFilterSort1);
            this.extendedDataGridSelectorColumn1.NullText = "не было";
            this.extendedDataGridSelectorColumn1.Width = 75;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // mainDataGrid1
            // 
            this.mainDataGrid1.AdditionalInfo = "";
            this.mainDataGrid1.ClassName = "CCommodityGroup";
            this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
            this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.mainDataGrid1.Name = "mainDataGrid1";
            this.mainDataGrid1.Size = new System.Drawing.Size(542, 374);
            this.mainDataGrid1.TabIndex = 0;
            this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.AllowSorting = false;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.extendedDataGridSelectorColumn1,
            this.formattableBooleanColumn1});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // ListCommodityGroup
            // 
            this.ClientSize = new System.Drawing.Size(542, 374);
            this.Controls.Add(this.mainDataGrid1);
            this.MinimumSize = new System.Drawing.Size(550, 404);
            this.Name = "ListCommodityGroup";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Группы товаров";
            this.Load += new System.EventHandler(this.fmListCommodityGroup_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
    }
}
