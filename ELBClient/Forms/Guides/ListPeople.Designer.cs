using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Guides
{
	public partial class ListPeople
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnNew;
		private ColumnMenuExtender.DataBoundComboBox cbGroup;
		private ColumnMenuExtender.DataGridISM dataGridISM1;
		private ColumnMenuExtender.DataGridPager dataGridPager1;
		private ELBClient.UserControls.ListContextMenu listContextMenu1;
		private System.Data.DataSet dataSet1;


		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;

		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.dataSet1 = new System.Data.DataSet();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dataGridISM1 = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.cbGroup = new ColumnMenuExtender.DataBoundComboBox();
			this.dataGridPager1 = new ColumnMenuExtender.DataGridPager();
			this.btnNew = new System.Windows.Forms.Button();
			this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dataGridISM1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(542, 287);
			this.panel2.TabIndex = 3;
			// 
			// dataGridISM1
			// 
			this.dataGridISM1.AllowSorting = false;
			this.dataGridISM1.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dataGridISM1.DataMember = "";
			this.dataGridISM1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridISM1.FilterString = null;
			this.dataGridISM1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridISM1.Location = new System.Drawing.Point(0, 0);
			this.dataGridISM1.Name = "dataGridISM1";
			this.dataGridISM1.Order = null;
			this.dataGridISM1.ReadOnly = true;
			this.dataGridISM1.Size = new System.Drawing.Size(542, 287);
			this.dataGridISM1.StockClass = "CPeople";
			this.dataGridISM1.TabIndex = 1;
			this.dataGridISM1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dataGridISM1, true);
			this.dataGridISM1.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dataGridISM1.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dataGridISM1_ActionKeyPressed);
			this.dataGridISM1.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dataGridISM1;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn7,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn6});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.PreferredColumnWidth = 150;
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Фамилия";
			this.formattableTextBoxColumn1.MappingName = "lastName";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.NullText = "";
			this.formattableTextBoxColumn1.Width = 150;
			// 
			// formattableTextBoxColumn7
			// 
			this.formattableTextBoxColumn7.FieldName = null;
			this.formattableTextBoxColumn7.FilterFieldName = null;
			this.formattableTextBoxColumn7.Format = "";
			this.formattableTextBoxColumn7.FormatInfo = null;
			this.formattableTextBoxColumn7.HeaderText = "Имя";
			this.formattableTextBoxColumn7.MappingName = "firstName";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
			this.formattableTextBoxColumn7.Width = 150;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "eMail";
			this.formattableTextBoxColumn2.MappingName = "eMail";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
			this.formattableTextBoxColumn2.NullText = "";
			this.formattableTextBoxColumn2.Width = 150;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = "company";
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Компания";
			this.formattableTextBoxColumn3.MappingName = "company";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
			this.formattableTextBoxColumn3.Width = 150;
			// 
			// formattableTextBoxColumn6
			// 
			this.formattableTextBoxColumn6.FieldName = null;
			this.formattableTextBoxColumn6.FilterFieldName = null;
			this.formattableTextBoxColumn6.Format = "";
			this.formattableTextBoxColumn6.FormatInfo = null;
			this.formattableTextBoxColumn6.HeaderText = "ID SV";
			this.formattableTextBoxColumn6.MappingName = "ID";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
			this.formattableTextBoxColumn6.NullText = "";
			this.formattableTextBoxColumn6.Width = 150;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.cbGroup);
			this.panel1.Controls.Add(this.dataGridPager1);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 287);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(542, 83);
			this.panel1.TabIndex = 2;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 19);
			this.label6.TabIndex = 14;
			this.label6.Text = "Группа:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbGroup
			// 
			this.cbGroup.Location = new System.Drawing.Point(72, 9);
			this.cbGroup.Name = "cbGroup";
			this.cbGroup.SelectedValue = -1;
			this.cbGroup.Size = new System.Drawing.Size(224, 21);
			this.cbGroup.TabIndex = 0;
			this.cbGroup.SelectedIndexChanged += new PositionChangedEventHandler(this.cbGroup_SelectedIndexChanged);
			// 
			// dataGridPager1
			// 
			this.dataGridPager1.AllowAll = true;
			this.dataGridPager1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dataGridPager1.Batch = 30;
			this.dataGridPager1.Location = new System.Drawing.Point(8, 46);
			this.dataGridPager1.Name = "dataGridPager1";
			this.dataGridPager1.PageCount = 0;
			this.dataGridPager1.PageNum = 1;
			this.dataGridPager1.Size = new System.Drawing.Size(280, 28);
			this.dataGridPager1.TabIndex = 17;
			// 
			// btnNew
			// 
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNew.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnNew.Location = new System.Drawing.Point(464, 46);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(75, 27);
			this.btnNew.TabIndex = 16;
			this.btnNew.Text = "Новый";
			this.btnNew.Click += new System.EventHandler(this.listContextMenu1_NewClick);
			// 
			// listContextMenu1
			// 
			this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
			this.listContextMenu1.NewClick += new System.EventHandler(this.listContextMenu1_NewClick);
			// 
			// ListPeople
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 370);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.MinimumSize = new System.Drawing.Size(550, 404);
			this.Name = "ListPeople";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Персоны";
			this.Load += new System.EventHandler(this.fmListPeople_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridISM1)).EndInit();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
	}
}
