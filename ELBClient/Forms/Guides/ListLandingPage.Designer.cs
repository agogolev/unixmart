using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListLandingPage
    {
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
            this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn9 = new ColumnMenuExtender.FormattableTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Название";
            this.formattableTextBoxColumn1.MappingName = "name";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
            this.formattableTextBoxColumn1.NullText = "";
            this.formattableTextBoxColumn1.Width = 150;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Ссылка";
            this.formattableTextBoxColumn2.MappingName = "shortLink";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn2, this.menuFilterSort1);
            this.formattableTextBoxColumn2.NullText = "";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = "commodityGroup?name";
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Группа";
            this.formattableTextBoxColumn3.MappingName = "commodityGroupName";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn3, this.menuFilterSort1);
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // extendedDataGridSelectorColumn1
            // 
            this.extendedDataGridSelectorColumn1.FieldName = "commodityGroup?lastGenerate";
            this.extendedDataGridSelectorColumn1.FilterFieldName = null;
            this.extendedDataGridSelectorColumn1.Format = "";
            this.extendedDataGridSelectorColumn1.FormatInfo = null;
            this.extendedDataGridSelectorColumn1.HeaderText = "Последняя генерация";
            this.extendedDataGridSelectorColumn1.MappingName = "commodityGroupLastGenerate";
            this.columnMenuExtender1.SetMenu(this.extendedDataGridSelectorColumn1, this.menuFilterSort1);
            this.extendedDataGridSelectorColumn1.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = "article?header";
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "СЕО текст";
            this.formattableTextBoxColumn4.MappingName = "articleName";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn4, this.menuFilterSort1);
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn6
            // 
            this.formattableTextBoxColumn6.FieldName = null;
            this.formattableTextBoxColumn6.FilterFieldName = null;
            this.formattableTextBoxColumn6.Format = "";
            this.formattableTextBoxColumn6.FormatInfo = null;
            this.formattableTextBoxColumn6.HeaderText = "Кол-во товаров";
            this.formattableTextBoxColumn6.MappingName = "actualGoodsCount";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
            this.formattableTextBoxColumn6.Width = 75;
            // 
            // formattableTextBoxColumn7
            // 
            this.formattableTextBoxColumn7.FieldName = null;
            this.formattableTextBoxColumn7.FilterFieldName = null;
            this.formattableTextBoxColumn7.Format = "";
            this.formattableTextBoxColumn7.FormatInfo = null;
            this.formattableTextBoxColumn7.HeaderText = "H1";
            this.formattableTextBoxColumn7.MappingName = "h1";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn7, this.menuFilterSort1);
            this.formattableTextBoxColumn7.Width = 75;
            // 
            // mainDataGrid1
            // 
            this.mainDataGrid1.AdditionalInfo = "";
            this.mainDataGrid1.ClassName = "CLandingPage";
            this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
            this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.mainDataGrid1.Name = "mainDataGrid1";
            this.mainDataGrid1.Size = new System.Drawing.Size(542, 374);
            this.mainDataGrid1.TabIndex = 2;
            this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.AllowSorting = false;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.extendedDataGridSelectorColumn1,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn7,
            this.formattableTextBoxColumn8,
            this.formattableTextBoxColumn9});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.MappingName = "commodityGroup";
            this.formattableTextBoxColumn5.Width = 0;
            // 
            // formattableTextBoxColumn8
            // 
            this.formattableTextBoxColumn8.FieldName = null;
            this.formattableTextBoxColumn8.FilterFieldName = null;
            this.formattableTextBoxColumn8.Format = "";
            this.formattableTextBoxColumn8.FormatInfo = null;
            this.formattableTextBoxColumn8.HeaderText = "Title";
            this.formattableTextBoxColumn8.MappingName = "title";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn8, this.menuFilterSort1);
            this.formattableTextBoxColumn8.Width = 150;
            // 
            // formattableTextBoxColumn9
            // 
            this.formattableTextBoxColumn9.FieldName = null;
            this.formattableTextBoxColumn9.FilterFieldName = null;
            this.formattableTextBoxColumn9.Format = "";
            this.formattableTextBoxColumn9.FormatInfo = null;
            this.formattableTextBoxColumn9.HeaderText = "Description";
            this.formattableTextBoxColumn9.MappingName = "pageDescription";
            this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn9, this.menuFilterSort1);
            this.formattableTextBoxColumn9.Width = 150;
            // 
            // ListLandingPage
            // 
            this.ClientSize = new System.Drawing.Size(542, 374);
            this.Controls.Add(this.mainDataGrid1);
            this.MinimumSize = new System.Drawing.Size(550, 404);
            this.Name = "ListLandingPage";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Посадочные страницы";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
        private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn9;
    }
}
