﻿using ELBClient.UserControls;
using MetaData;

namespace ELBClient.Forms.Guides
{
	partial class EditCommodityGroup
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.NameText = new System.Windows.Forms.TextBox();
            this.ShopTypeSelect = new ColumnMenuExtender.DataBoundComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.UrlText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.UseLargeImageCheck = new System.Windows.Forms.CheckBox();
            this.CategoryUrlCheck = new System.Windows.Forms.CheckBox();
            this.ForABCCheck = new System.Windows.Forms.CheckBox();
            this.ForContextCheck = new System.Windows.Forms.CheckBox();
            this.TorgMailDtdCheck = new System.Windows.Forms.CheckBox();
            this.ParamToDescriptionCheck = new System.Windows.Forms.CheckBox();
            this.YandexDtdCheck = new System.Windows.Forms.CheckBox();
            this.IsActiveCheck = new System.Windows.Forms.CheckBox();
            this.IncludeBidCBidCheck = new System.Windows.Forms.CheckBox();
            this.GlobalUtmLabelText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.LastGenerateSelect = new System.Windows.Forms.TextBox();
            this.DateModifySelect = new System.Windows.Forms.TextBox();
            this.PriceAggregatorText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GoodsGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.AddGoods = new System.Windows.Forms.Button();
            this.DelGoods = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.CatalogTreeView = new ELBClient.UserControls.DataBoundTreeViewControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.FiltersTreeView = new ELBClient.UserControls.DataBoundTreeViewControl();
            this.ManufThemesDataSet = new MetaData.DataSetISM();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.GoodsDataSet = new MetaData.DataSetISM();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.ThemeParamsDataSet = new MetaData.DataSetISM();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.PostavshikIdCheck = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.ShopTypeSelect)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ManufThemesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThemeParamsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название:";
            // 
            // NameText
            // 
            this.NameText.Location = new System.Drawing.Point(111, 20);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(200, 20);
            this.NameText.TabIndex = 1;
            // 
            // ShopTypeSelect
            // 
            this.ShopTypeSelect.AutoCompleteDisplayMember = null;
            this.ShopTypeSelect.AutoCompleteValueMember = null;
            this.ShopTypeSelect.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ShopTypeSelect.Location = new System.Drawing.Point(111, 47);
            this.ShopTypeSelect.Name = "ShopTypeSelect";
            this.ShopTypeSelect.SelectedValue = -1;
            this.ShopTypeSelect.Size = new System.Drawing.Size(200, 20);
            this.ShopTypeSelect.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Магазин:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UrlText
            // 
            this.UrlText.Location = new System.Drawing.Point(111, 78);
            this.UrlText.Name = "UrlText";
            this.UrlText.Size = new System.Drawing.Size(200, 20);
            this.UrlText.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Название файла:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(320, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Дата редакции:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(320, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Последняя генерация:";
            // 
            // CloseButton
            // 
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(12, 9);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 37;
            this.CloseButton.Text = "Закрыть";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.Close_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(565, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 38;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(652, 340);
            this.tabControl1.TabIndex = 41;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.PostavshikIdCheck);
            this.tabPage1.Controls.Add(this.UseLargeImageCheck);
            this.tabPage1.Controls.Add(this.CategoryUrlCheck);
            this.tabPage1.Controls.Add(this.ForABCCheck);
            this.tabPage1.Controls.Add(this.ForContextCheck);
            this.tabPage1.Controls.Add(this.TorgMailDtdCheck);
            this.tabPage1.Controls.Add(this.ParamToDescriptionCheck);
            this.tabPage1.Controls.Add(this.YandexDtdCheck);
            this.tabPage1.Controls.Add(this.IsActiveCheck);
            this.tabPage1.Controls.Add(this.IncludeBidCBidCheck);
            this.tabPage1.Controls.Add(this.GlobalUtmLabelText);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.LastGenerateSelect);
            this.tabPage1.Controls.Add(this.DateModifySelect);
            this.tabPage1.Controls.Add(this.PriceAggregatorText);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.NameText);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.ShopTypeSelect);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.UrlText);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(644, 314);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Общая информация";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // UseLargeImageCheck
            // 
            this.UseLargeImageCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UseLargeImageCheck.Location = new System.Drawing.Point(227, 234);
            this.UseLargeImageCheck.Name = "UseLargeImageCheck";
            this.UseLargeImageCheck.Size = new System.Drawing.Size(167, 24);
            this.UseLargeImageCheck.TabIndex = 55;
            this.UseLargeImageCheck.Text = "Большие картинки:";
            this.UseLargeImageCheck.UseVisualStyleBackColor = true;
            // 
            // CategoryUrlCheck
            // 
            this.CategoryUrlCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CategoryUrlCheck.Location = new System.Drawing.Point(15, 234);
            this.CategoryUrlCheck.Name = "CategoryUrlCheck";
            this.CategoryUrlCheck.Size = new System.Drawing.Size(167, 24);
            this.CategoryUrlCheck.TabIndex = 54;
            this.CategoryUrlCheck.Text = "Url категории:";
            this.CategoryUrlCheck.UseVisualStyleBackColor = true;
            // 
            // ForABCCheck
            // 
            this.ForABCCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ForABCCheck.Location = new System.Drawing.Point(448, 204);
            this.ForABCCheck.Name = "ForABCCheck";
            this.ForABCCheck.Size = new System.Drawing.Size(167, 24);
            this.ForABCCheck.TabIndex = 53;
            this.ForABCCheck.Text = "Для ABC:";
            this.ForABCCheck.UseVisualStyleBackColor = true;
            // 
            // ForContextCheck
            // 
            this.ForContextCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ForContextCheck.Location = new System.Drawing.Point(227, 204);
            this.ForContextCheck.Name = "ForContextCheck";
            this.ForContextCheck.Size = new System.Drawing.Size(167, 24);
            this.ForContextCheck.TabIndex = 52;
            this.ForContextCheck.Text = "Для контекста:";
            this.ForContextCheck.UseVisualStyleBackColor = true;
            // 
            // TorgMailDtdCheck
            // 
            this.TorgMailDtdCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TorgMailDtdCheck.Location = new System.Drawing.Point(462, 174);
            this.TorgMailDtdCheck.Name = "TorgMailDtdCheck";
            this.TorgMailDtdCheck.Size = new System.Drawing.Size(153, 24);
            this.TorgMailDtdCheck.TabIndex = 51;
            this.TorgMailDtdCheck.Text = "Формат Torg.Mail:";
            this.TorgMailDtdCheck.UseVisualStyleBackColor = true;
            // 
            // ParamToDescriptionCheck
            // 
            this.ParamToDescriptionCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ParamToDescriptionCheck.Location = new System.Drawing.Point(15, 204);
            this.ParamToDescriptionCheck.Name = "ParamToDescriptionCheck";
            this.ParamToDescriptionCheck.Size = new System.Drawing.Size(167, 24);
            this.ParamToDescriptionCheck.TabIndex = 50;
            this.ParamToDescriptionCheck.Text = "Параметры -> описание:";
            this.ParamToDescriptionCheck.UseVisualStyleBackColor = true;
            // 
            // YandexDtdCheck
            // 
            this.YandexDtdCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.YandexDtdCheck.Location = new System.Drawing.Point(222, 174);
            this.YandexDtdCheck.Name = "YandexDtdCheck";
            this.YandexDtdCheck.Size = new System.Drawing.Size(172, 24);
            this.YandexDtdCheck.TabIndex = 49;
            this.YandexDtdCheck.Text = "Строгий формат Yandex:";
            this.YandexDtdCheck.UseVisualStyleBackColor = true;
            // 
            // IsActiveCheck
            // 
            this.IsActiveCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IsActiveCheck.Location = new System.Drawing.Point(323, 76);
            this.IsActiveCheck.Name = "IsActiveCheck";
            this.IsActiveCheck.Size = new System.Drawing.Size(149, 24);
            this.IsActiveCheck.TabIndex = 48;
            this.IsActiveCheck.Text = "Активна:";
            this.IsActiveCheck.UseVisualStyleBackColor = true;
            // 
            // IncludeBidCBidCheck
            // 
            this.IncludeBidCBidCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IncludeBidCBidCheck.Location = new System.Drawing.Point(15, 174);
            this.IncludeBidCBidCheck.Name = "IncludeBidCBidCheck";
            this.IncludeBidCBidCheck.Size = new System.Drawing.Size(167, 24);
            this.IncludeBidCBidCheck.TabIndex = 47;
            this.IncludeBidCBidCheck.Text = "Включить BID/CBID:";
            this.IncludeBidCBidCheck.UseVisualStyleBackColor = true;
            // 
            // GlobalUtmLabelText
            // 
            this.GlobalUtmLabelText.Location = new System.Drawing.Point(151, 144);
            this.GlobalUtmLabelText.Name = "GlobalUtmLabelText";
            this.GlobalUtmLabelText.Size = new System.Drawing.Size(465, 20);
            this.GlobalUtmLabelText.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Глобальная UTM метка:";
            // 
            // LastGenerateSelect
            // 
            this.LastGenerateSelect.Location = new System.Drawing.Point(456, 47);
            this.LastGenerateSelect.Name = "LastGenerateSelect";
            this.LastGenerateSelect.Size = new System.Drawing.Size(160, 20);
            this.LastGenerateSelect.TabIndex = 44;
            // 
            // DateModifySelect
            // 
            this.DateModifySelect.Location = new System.Drawing.Point(456, 21);
            this.DateModifySelect.Name = "DateModifySelect";
            this.DateModifySelect.Size = new System.Drawing.Size(160, 20);
            this.DateModifySelect.TabIndex = 43;
            // 
            // PriceAggregatorText
            // 
            this.PriceAggregatorText.Location = new System.Drawing.Point(151, 115);
            this.PriceAggregatorText.Name = "PriceAggregatorText";
            this.PriceAggregatorText.Size = new System.Drawing.Size(160, 20);
            this.PriceAggregatorText.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Прайс агрегатор:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.GoodsGrid);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(644, 314);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Выбор товаров";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GoodsGrid
            // 
            this.GoodsGrid.BackgroundColor = System.Drawing.Color.White;
            this.GoodsGrid.DataMember = "";
            this.GoodsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GoodsGrid.FilterString = null;
            this.GoodsGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.GoodsGrid.Location = new System.Drawing.Point(3, 3);
            this.GoodsGrid.Name = "GoodsGrid";
            this.GoodsGrid.Order = null;
            this.GoodsGrid.Size = new System.Drawing.Size(607, 308);
            this.GoodsGrid.TabIndex = 1;
            this.GoodsGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
            // 
            // extendedDataGridTableStyle2
            // 
            this.extendedDataGridTableStyle2.DataGrid = this.GoodsGrid;
            this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5});
            this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle2.MappingName = "Table";
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Название";
            this.formattableTextBoxColumn4.MappingName = "propValue_NK";
            this.formattableTextBoxColumn4.ReadOnly = true;
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "Приоритет";
            this.formattableTextBoxColumn5.MappingName = "ordValue";
            this.formattableTextBoxColumn5.Width = 75;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.AddGoods);
            this.panel2.Controls.Add(this.DelGoods);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(610, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(31, 308);
            this.panel2.TabIndex = 0;
            // 
            // AddGoods
            // 
            this.AddGoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddGoods.Location = new System.Drawing.Point(3, 3);
            this.AddGoods.Name = "AddGoods";
            this.AddGoods.Size = new System.Drawing.Size(24, 26);
            this.AddGoods.TabIndex = 3;
            this.AddGoods.Text = "+";
            this.AddGoods.UseVisualStyleBackColor = true;
            this.AddGoods.Click += new System.EventHandler(this.AddGoods_Click);
            // 
            // DelGoods
            // 
            this.DelGoods.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DelGoods.Location = new System.Drawing.Point(3, 35);
            this.DelGoods.Name = "DelGoods";
            this.DelGoods.Size = new System.Drawing.Size(24, 26);
            this.DelGoods.TabIndex = 2;
            this.DelGoods.Text = "-";
            this.DelGoods.UseVisualStyleBackColor = true;
            this.DelGoods.Click += new System.EventHandler(this.DelGoods_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.CatalogTreeView);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(644, 314);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Категории";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // CatalogTreeView
            // 
            this.CatalogTreeView.CheckBoxes = true;
            this.CatalogTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CatalogTreeView.IsModified = false;
            this.CatalogTreeView.Location = new System.Drawing.Point(0, 0);
            this.CatalogTreeView.Name = "CatalogTreeView";
            this.CatalogTreeView.Size = new System.Drawing.Size(644, 314);
            this.CatalogTreeView.TabIndex = 9;
            this.CatalogTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.CatalogTreeView_AfterCheck);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.FiltersTreeView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(644, 314);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Фильтры";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // FiltersTreeView
            // 
            this.FiltersTreeView.CheckBoxes = true;
            this.FiltersTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FiltersTreeView.IsModified = false;
            this.FiltersTreeView.Location = new System.Drawing.Point(0, 0);
            this.FiltersTreeView.Name = "FiltersTreeView";
            this.FiltersTreeView.Size = new System.Drawing.Size(644, 314);
            this.FiltersTreeView.TabIndex = 0;
            // 
            // ManufThemesDataSet
            // 
            this.ManufThemesDataSet.DataSetName = "NewDataSet";
            this.ManufThemesDataSet.OnlyRowsWithoutErrors = false;
            this.ManufThemesDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn4,
            this.dataColumn3,
            this.dataColumn5});
            this.dataTable1.TableName = "Table";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.AutoIncrement = true;
            this.dataColumn1.AutoIncrementSeed = ((long)(1));
            this.dataColumn1.ColumnName = "ordValue";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "propValue";
            this.dataColumn2.DataType = typeof(System.Guid);
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "propValue_NK";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "themeOID";
            this.dataColumn3.DataType = typeof(System.Guid);
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "themeName";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CloseButton);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 340);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 42);
            this.panel1.TabIndex = 42;
            // 
            // GoodsDataSet
            // 
            this.GoodsDataSet.DataSetName = "NewDataSet";
            this.GoodsDataSet.OnlyRowsWithoutErrors = false;
            this.GoodsDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dataTable2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "propValue"}, true)});
            this.dataTable2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn6};
            this.dataTable2.TableName = "Table";
            // 
            // dataColumn6
            // 
            this.dataColumn6.AllowDBNull = false;
            this.dataColumn6.ColumnName = "propValue";
            this.dataColumn6.DataType = typeof(System.Guid);
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "propValue_NK";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "propValue_className";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "ordValue";
            this.dataColumn9.DataType = typeof(int);
            // 
            // ThemeParamsDataSet
            // 
            this.ThemeParamsDataSet.DataSetName = "NewDataSet";
            this.ThemeParamsDataSet.OnlyRowsWithoutErrors = false;
            this.ThemeParamsDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable3});
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10});
            this.dataTable3.TableName = "Table";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "id";
            this.dataColumn10.DataType = typeof(int);
            // 
            // PostavshikIdCheck
            // 
            this.PostavshikIdCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PostavshikIdCheck.Location = new System.Drawing.Point(448, 234);
            this.PostavshikIdCheck.Name = "PostavshikIdCheck";
            this.PostavshikIdCheck.Size = new System.Drawing.Size(167, 24);
            this.PostavshikIdCheck.TabIndex = 56;
            this.PostavshikIdCheck.Text = "Postavshik_id:";
            this.PostavshikIdCheck.UseVisualStyleBackColor = true;
            // 
            // EditCommodityGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 382);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "EditCommodityGroup";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Товарная группа";
            this.Load += new System.EventHandler(this.EditCommodityGroup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ShopTypeSelect)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ManufThemesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GoodsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThemeParamsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox NameText;
		private ColumnMenuExtender.DataBoundComboBox ShopTypeSelect;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox UrlText;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button CloseButton;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabPage tabPage5;
		private MetaData.DataSetISM ManufThemesDataSet;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataColumn dataColumn3;
		private System.Data.DataColumn dataColumn4;
		private System.Data.DataColumn dataColumn5;
		private MetaData.DataSetISM GoodsDataSet;
		private System.Data.DataTable dataTable2;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;
		private System.Data.DataColumn dataColumn8;
		private System.Data.DataColumn dataColumn9;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button AddGoods;
		private System.Windows.Forms.Button DelGoods;
		private ColumnMenuExtender.DataGridISM GoodsGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private System.Windows.Forms.TextBox PriceAggregatorText;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox LastGenerateSelect;
		private System.Windows.Forms.TextBox DateModifySelect;
		private System.Windows.Forms.TextBox GlobalUtmLabelText;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox IncludeBidCBidCheck;
		private System.Windows.Forms.CheckBox IsActiveCheck;
		private System.Windows.Forms.CheckBox TorgMailDtdCheck;
		private System.Windows.Forms.CheckBox ParamToDescriptionCheck;
		private System.Windows.Forms.CheckBox YandexDtdCheck;
		private System.Windows.Forms.CheckBox ForContextCheck;
        private System.Windows.Forms.CheckBox ForABCCheck;
        private System.Windows.Forms.CheckBox CategoryUrlCheck;
        private DataBoundTreeViewControl CatalogTreeView;
        private DataSetISM ThemeParamsDataSet;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn10;
        private System.Windows.Forms.TabPage tabPage3;
        private DataBoundTreeViewControl FiltersTreeView;
        private System.Windows.Forms.CheckBox UseLargeImageCheck;
        private System.Windows.Forms.CheckBox PostavshikIdCheck;
    }
}