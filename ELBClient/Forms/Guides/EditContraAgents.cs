﻿using System;
using System.Windows.Forms;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;

namespace ELBClient.Forms.Guides
{
    /// <summary>
    ///     Summary description for fmContraAgent.
    /// </summary>
    public partial class EditContraAgents : EditForm
    {
        public EditContraAgents()
        {
            InitializeComponent();
            Object = new CCompany();
        }

        protected override bool IsModified
        {
            get { return Object.IsModified; }
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>

        #endregion
        private void fmEditContraAgents_Load(object sender, EventArgs e)
        {
            try
            {
                LoadObject("companyName,shortLink,altName,companyURL,title,keywords,pageDescription", null);
                BindFields();
            }
            catch (Exception ex)
            {
                ShowError("Ошибка загрузки: " + ex.Message);
            }
        }

        private void BindFields()
        {
            NameText.DataBindings.Add("Text", Object, "CompanyName");
            ShortLinkText.DataBindings.Add("Text", Object, "ShortLink");
            AltNameText.DataBindings.Add("Text", Object, "AltName");
            CompanyURLText.DataBindings.Add("Text", Object, "CompanyURL");
            txtTitle.DataBindings.Add("Text", Object, "Title");
            txtKeywords.DataBindings.Add("Text", Object, "Keywords");
            txtPageDescription.DataBindings.Add("Text", Object, "PageDescription");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveObject();
            }
            catch
            {
            }
        }

        protected override void SaveObject()
        {
            base.SaveObject();

            if (ReloadGrid != null)
                ReloadGrid();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAttachImages_Click(object sender, EventArgs e)
        {
            if (ObjectOID == Guid.Empty)
                MessageBox.Show(this, @"Сначала сохраните объект!");
            else
            {
                var fm = new AttachImages(Object) { MdiParent = ParentForm };
                fm.Show();
            }
        }
    }
}