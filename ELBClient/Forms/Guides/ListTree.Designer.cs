using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListTree
	{
		#region Windows Form Designer generated code
		private ELBClient.UserControls.MainDataGrid mainDataGrid1;
		private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.FormattableTextBoxColumn dataGridTextBoxColumn1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.dataGridTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.mainDataGrid1 = new ELBClient.UserControls.MainDataGrid();
			this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridTextBoxColumn1
			// 
			this.dataGridTextBoxColumn1.FieldName = null;
			this.dataGridTextBoxColumn1.FilterFieldName = null;
			this.dataGridTextBoxColumn1.Format = "";
			this.dataGridTextBoxColumn1.FormatInfo = null;
			this.dataGridTextBoxColumn1.HeaderText = "Название";
			this.dataGridTextBoxColumn1.MappingName = "treeName";
			this.columnMenuExtender1.SetMenu(this.dataGridTextBoxColumn1, this.menuFilterSort1);
			this.dataGridTextBoxColumn1.Width = 150;
			// 
			// mainDataGrid1
			// 
			this.mainDataGrid1.AdditionalInfo = "";
			this.mainDataGrid1.ClassName = "CTree";
			this.mainDataGrid1.CMExtender = this.columnMenuExtender1;
			this.mainDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainDataGrid1.Location = new System.Drawing.Point(0, 0);
			this.mainDataGrid1.Name = "mainDataGrid1";
			this.mainDataGrid1.Size = new System.Drawing.Size(542, 370);
			this.mainDataGrid1.TabIndex = 1;
			this.mainDataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.dataGridTableStyle1});
			// 
			// dataGridTableStyle1
			// 
			this.dataGridTableStyle1.AllowSorting = false;
			this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.dataGridTextBoxColumn1});
			this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridTableStyle1.MappingName = "table";
			this.dataGridTableStyle1.PreferredColumnWidth = 150;
			// 
			// ListTree
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(542, 370);
			this.Controls.Add(this.mainDataGrid1);
			this.MinimumSize = new System.Drawing.Size(550, 404);
			this.Name = "ListTree";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Структуры сайта";
			this.Load += new System.EventHandler(this.fmListTree_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
