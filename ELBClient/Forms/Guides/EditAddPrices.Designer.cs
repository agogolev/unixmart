﻿namespace ELBClient.Forms.Guides
{
	partial class EditAddPrices
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Path = new System.Windows.Forms.TextBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.ShopType1 = new System.Windows.Forms.ComboBox();
            this.AddPrices = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddPrices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Path);
            this.panel1.Controls.Add(this.RefreshButton);
            this.panel1.Controls.Add(this.ShopType1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1060, 71);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Магазины:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "В пути:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Path
            // 
            this.Path.Location = new System.Drawing.Point(118, 39);
            this.Path.Margin = new System.Windows.Forms.Padding(4);
            this.Path.Name = "Path";
            this.Path.Size = new System.Drawing.Size(516, 22);
            this.Path.TabIndex = 2;
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(373, 7);
            this.RefreshButton.Margin = new System.Windows.Forms.Padding(4);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(261, 28);
            this.RefreshButton.TabIndex = 1;
            this.RefreshButton.Text = "Показать";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.ShowButton_Click);
            // 
            // ShopType1
            // 
            this.ShopType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ShopType1.FormattingEnabled = true;
            this.ShopType1.Location = new System.Drawing.Point(118, 9);
            this.ShopType1.Margin = new System.Windows.Forms.Padding(4);
            this.ShopType1.Name = "ShopType1";
            this.ShopType1.Size = new System.Drawing.Size(247, 24);
            this.ShopType1.TabIndex = 0;
            // 
            // AddPrices
            // 
            this.AddPrices.AllowSorting = false;
            this.AddPrices.BackgroundColor = System.Drawing.Color.White;
            this.AddPrices.DataMember = "";
            this.AddPrices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddPrices.FilterString = null;
            this.AddPrices.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.AddPrices.Location = new System.Drawing.Point(0, 71);
            this.AddPrices.Margin = new System.Windows.Forms.Padding(4);
            this.AddPrices.Name = "AddPrices";
            this.AddPrices.Order = null;
            this.AddPrices.Size = new System.Drawing.Size(1060, 267);
            this.AddPrices.TabIndex = 2;
            this.AddPrices.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.AllowSorting = false;
            this.extendedDataGridTableStyle1.DataGrid = this.AddPrices;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "Table";
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Путь";
            this.formattableTextBoxColumn1.MappingName = "fullName";
            this.formattableTextBoxColumn1.ReadOnly = true;
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "#,0.##";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Доставка первого 1";
            this.formattableTextBoxColumn4.MappingName = "deliveryFirst1";
            this.formattableTextBoxColumn4.NullText = "";
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn5
            // 
            this.formattableTextBoxColumn5.FieldName = null;
            this.formattableTextBoxColumn5.FilterFieldName = null;
            this.formattableTextBoxColumn5.Format = "#,0.##";
            this.formattableTextBoxColumn5.FormatInfo = null;
            this.formattableTextBoxColumn5.HeaderText = "Доставка втрого 1";
            this.formattableTextBoxColumn5.MappingName = "deliverySecond1";
            this.formattableTextBoxColumn5.NullText = "";
            this.formattableTextBoxColumn5.Width = 75;
            // 
            // EditAddPrices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 338);
            this.Controls.Add(this.AddPrices);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EditAddPrices";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.Text = "Наценки на категории";
            this.Load += new System.EventHandler(this.EditAddPrices_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddPrices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox ShopType1;
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridISM AddPrices;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private System.Windows.Forms.Button RefreshButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox Path;
	}
}