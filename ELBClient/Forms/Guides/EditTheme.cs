﻿using System;
using System.Data;
using System.Collections;
using System.Linq;
using System.Windows.Forms;
using ELBClient.Forms.Dialogs;
using ColumnMenuExtender;
using MetaData;
using System.Configuration;
using ELBClient.Classes;
using HttpReader;

namespace ELBClient.Forms.Guides
{
    public partial class EditTheme : EditForm
	{
		public override string ObjectNK
		{
			get
			{
				return ((CTheme) Object).ThemeName;
			}
		}
		public EditTheme()
		{
			InitializeComponent();
			Object = new CTheme();
			brandSalesNotesColumn.ButtonClick += brandColumn_ButtonClick;
            brandTitleColumn.ButtonClick += brandColumn_ButtonClick;
            dataTable8.RowChanging += DataTable8_RowChanging;
            dataTable8.TableNewRow += DataTable8_TableNewRow;
		}

        private void DataTable8_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            var dr = e.Row;
            RemoveNulls(dr);
        }

        private void RemoveNulls(DataRow dr)
        {
            if (dr.Field<string>("title") == null) dr["title"] = "";
            if (dr.Field<string>("keywords") == null) dr["keywords"] = "";
            if (dr.Field<string>("pageDescription") == null) dr["pageDescription"] = "";
        }

        private void DataTable8_RowChanging(object sender, DataRowChangeEventArgs e)
        {
            var dr = e.Row;
            if (dr.RowState == DataRowState.Deleted) return;
            RemoveNulls(dr);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>


        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        #endregion

        private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				SaveObject();
			}
			catch { }
			//this.Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void BindFields()
		{
            NameText.DataBindings.Clear();
            AltNameText.DataBindings.Clear();
            H1Text.DataBindings.Clear();
            ShortLinkText.DataBindings.Clear();
			ddlShopType.DataBindings.Clear();
			LoadToYandex.DataBindings.Clear();
			PriceLimit.DataBindings.Clear();
            NameText.DataBindings.Add("Text", Object, "themeName");
            AltNameText.DataBindings.Add("Text", Object, "altName");
            H1Text.DataBindings.Add("Text", Object, "H1");
            ShortLinkText.DataBindings.Add("Text", Object, "ShortLink");
			ddlShopType.DataBindings.Add("SelectedValue", Object, "ShopType");
			LoadToYandex.DataBindings.Add("Checked", Object, "LoadToYandex");
			PriceLimit.DataBindings.Add("BoundProp", Object, "PriceLimit");
            txtTitle.DataBindings.Add("Text", Object, "Title");
            txtKeywords.DataBindings.Add("Text", Object, "Keywords");
            txtPageDescription.DataBindings.Add("Text", Object, "PageDescription");
            GoogleSelect.DataBindings.Add("BoundProp", Object, "Google");
            GoodsListCheck.DataBindings.Add("Checked", Object, "goodsList");
        }

        private void LoadLookup()
		{
			string xml = lp.GetLookupValues("COrder");
			ddlShopType.LoadXml(xml, "shopTypeN");
			ShopTypeColumn.ComboBox.LoadXml(xml, "shopTypeN");
			xml = lp.GetLookupValues("CParamType");
			colType.ComboBox.LoadXml(xml, "paramTypeN");
		}
		private void fmEditTheme_Load(object sender, EventArgs e)
		{
			LoadLookup();
			LoadData();
		}
		private void LoadData()
		{
			string xml = GetResourceString("fmEditTheme.xml");
			Object.LoadMultiPropFlag = false;
			xml = LoadObject(xml, CTheme.ClassCID);
			BindFields();
			FillGrid(xml, true);
		}
		private void FillGrid(string xml, bool acceptChange)
		{
			FillParams(xml, acceptChange);
			MultiProps.LoadRowsFromXml("PriceIntervals", xml, "priceIntervals");
			MultiProps.LoadRowsFromXml("Accessories", xml, "accessories");
            MultiProps.LoadRowsFromXml("PressReleases", xml, "pressReleases");
            MultiProps.LoadRowsFromXml("Tags", xml, "tags");
            MultiProps.LoadRowsFromXml("Titles", xml, "titles");

            PriceIntervals.SetDataBinding(MultiProps, "priceIntervals");
			dgPressReleases.extendedDataGrid1.SetDataBinding(MultiProps, "pressReleases");
            dgAccessories.extendedDataGrid1.SetDataBinding(MultiProps, "accessories");
            var dataView = new DataView(MultiProps.Tables["tags"], null, "ordValue", DataViewRowState.CurrentRows);
            TagsDataGrid.extendedDataGrid1.SetDataBinding(dataView, null);
            TitlesGrid.SetDataBinding(MultiProps, "titles");

            dgPressReleases.extendedDataGrid1.DisableNewDel();
			dgAccessories.extendedDataGrid1.DisableNewDel();
            TagsDataGrid.extendedDataGrid1.DisableNewDel();

            SeoTextsDataSet.LoadRowsFromXML("Table", xml, "seoTexts");
			SeoTextGrid.SetDataBinding(SeoTextsDataSet, "Table");
			SeoTextGrid.DisableNewDel();

			SalesNotesDataSet.LoadRowsFromXML("Table", xml, "salesNotes");
			SalesNotesGrid.SetDataBinding(SalesNotesDataSet, "Table");
			//SalesNotesGrid.DisableNewDel();

		}

		private void FillParams(string xml, bool acceptChange)
		{
			for (int i = 0; i < dsParams.Table.Rows.Count; i++)
			{
				DataRow dr = dsParams.Table.Rows[i];
				dr.Delete();
			}
			//dsParams.Table.Rows.Clear();
			dsParams.LoadRowsFromXML("table", xml, "params", acceptChange);
			dgParams.DisableNewDel();
		}
		private void btnAddParam_Click(object sender, EventArgs e)
		{
			var fm = new ListParamType();
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			var nextOrd = NextOrd();
			try
			{
				dsParams.Table.Rows.Add(new[] { nextOrd, fm.SelectedOID, fm.SelectedName, fm.ParamType, "", false, false, false, false });
			}
			catch (ConstraintException)
			{
				ShowError("Данный параметр уже есть в коллекции");
			}
		}
		private int NextOrd()
		{
			if (dsParams.Table.Rows.Count == 0) return 1;
			var dv = new DataView(dsParams.Table, null, "ordValue Desc", DataViewRowState.CurrentRows);
			var i = (int)dv[0]["ordValue"];
			i++;
			return i;
		}
		protected override bool IsModified
		{
			get
			{
				return SalesNotesDataSet.IsModified || SeoTextsDataSet.IsModified || dsParams.IsModified || MultiProps.IsModified() || base.IsModified;
			}
		}
		protected override void SaveObject()
		{
			try
			{
				if (dsParams.IsModified)
					Object.ExtMultiProp = dsParams.GetChangesXml("Params");
				if (MultiProps.IsModified("priceIntervals"))
					Object.ExtMultiProp += MultiProps.GetChangesXml("priceIntervals", "PriceIntervals");
				if (MultiProps.IsModified("pressReleases"))
					Object.ExtMultiProp += MultiProps.GetChangesXml("pressReleases", "PressReleases", new[] { "ordValue", "propValue" }, new[] { "ordValue", "articleOID" });
				if (MultiProps.IsModified("accessories"))
					Object.ExtMultiProp += MultiProps.GetChangesXml("accessories", "Accessories", new[] { "propValue" }, new[] { "accessoryOID" });
				if (SeoTextsDataSet.IsModified)
					Object.ExtMultiProp = SeoTextsDataSet.GetChangesXml("seoTexts", new[] { "ordValue", "propValue", "manufacturerOID" }, new[] { "ordValue", "articleOID", "manufacturerOID" });
				if (SalesNotesDataSet.IsModified)
					Object.ExtMultiProp = SalesNotesDataSet.GetChangesXml("salesNotes", new[] { "ordValue", "propValue", "manufacturerOID" }, new[] { "ordValue", "salesNotes", "manufacturerOID" });
                if (MultiProps.IsModified("tags"))
                    Object.ExtMultiProp += MultiProps.GetChangesXml("tags", "Tags", new[] { "ordValue", "propValue" }, new[] { "ordValue", "landingPageOID" });
                if (MultiProps.IsModified("titles"))
                    Object.ExtMultiProp += MultiProps.GetChangesXml("titles", "Titles", new[] { "manufacturerOID", "title", "keywords", "pageDescription" }, new[] { "manufacturerOID", "title", "keywords", "pageDescription" });
                base.SaveObject();
				dsParams.AcceptChanges();
				MultiProps.AcceptChanges();
				SeoTextsDataSet.AcceptChanges();
				SalesNotesDataSet.AcceptChanges();
			}
			catch (Exception e) {
                ShowError(e.Message);
            }
		}

		private void btnDelParam_Click(object sender, EventArgs e)
		{
			var dr = dgParams.GetSelectedRow();
			if (dr != null) dr.Delete();
		}

		private void FromTemplate_Click(object sender, EventArgs e)
		{
			var rootNode = (((MainFrm)MdiParent) ?? ((MainFrm)Owner)).CatalogRoot(1);
			var fm = new TreeObjectDialog { Owner = MdiParent, RootNode = rootNode };
			if (fm.ShowDialog(this) != DialogResult.OK || fm.SelectedObject == null) return;
			var OID = fm.SelectedObject.OID;
			if (OID == Object.OID)
			{
				ShowError("Это та же категория");
				return;
			}
			var xml = GetResourceString("fmEditTheme.xml");
			var prov = ServiceUtility.ObjectProvider;
			xml = prov.GetObject(OID.ToString(), xml, null);
			FillParams(xml, false);
		}

		//private void dgParams_DoubleClick(object sender, EventArgs e)
		//{
		//  DataRow dr = dgParams.GetSelectedRow();
		//  DataGrid.HitTestInfo hti = dgParams.Hit;
		//  if (hti.Column != 5)
		//  {
		//    if (dr != null)
		//    {
		//      Guid OID = (Guid)dr["OID"];
		//      fmEditParamType fm = new fmEditParamType();
		//      fm.ObjectOID = OID;
		//      fm.MdiParent = this.MdiParent;
		//      fm.ReloadGrid = new ReloadDelegate(LoadData);
		//      fm.Shows();

		//    }
		//  }
		//}

		private void dgPressReleases_AddClick(object sender, EventArgs e)
		{
			var fm = new ListDialog("CArticle", new[] { "header", "pubDate" }, new[] { "Название", "Дата публикации" });
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			var o = fm.SelectedOID;
			var s = fm.SelectedNK;
			var ordValue = GetLastValue("pressReleases");
			MultiProps.Tables["pressReleases"].Rows.Add(new object[] { ordValue, o, s });
			//((CObject)Object).PressReleases.Rows.Add(new object[] { o, s, "CArticle", ordValue });
		}

		private void dgPressReleases_DeleteClick(object sender, EventArgs e)
		{
			var dr = dgPressReleases.extendedDataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
		}
		private int GetLastValue(string collection)
		{
			var ordValue = 0;
			foreach (DataRow dr in MultiProps.Tables[collection].Rows)
			{
				if (dr.RowState != DataRowState.Deleted && dr["ordValue"] != DBNull.Value && (int) dr["ordValue"] > ordValue)
				{
					ordValue = (int) dr["ordValue"];
				}
			}
			return ++ordValue;
		}

		private void dgAccessories_AddClick(object sender, EventArgs e)
		{
			cmAddAccessory.Show(dgAccessories, dgAccessories.PointToClient(Cursor.Position));
		}

		private void dgAccessories_DeleteClick(object sender, EventArgs e)
		{
			var dr = dgAccessories.extendedDataGrid1.GetSelectedRow();
			if (dr != null)
				dr.Delete();
		}

		private void miAddGoods_Click(object sender, EventArgs e)
		{
			var fm = new ListGoodsDialog {ShopType = ((CTheme) Object).ShopType};
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			foreach (var dr in fm.SelectedRows)
			{
				if (dr == null) continue;
				var o = (Guid)dr["OID"];
				var s = dr["shortName"].ToString();
				MultiProps.Tables["accessories"].Rows.Add(new object[] { o, s });
			}
		}

		private void miAddTheme_Click(object sender, EventArgs e)
		{
			var fm = new ListThemesDialog {ShopType = ((CTheme) Object).ShopType};
			if (fm.ShowDialog(this) != DialogResult.OK) return;
			var o = fm.SelectedOID;
			var s = fm.SelectedName;
			MultiProps.Tables["accessories"].Rows.Add(new object[] { o, s });
		}

		private void btnImages_Click(object sender, EventArgs e)
		{
			if (Object.OID == Guid.Empty)
				MessageBox.Show(this, @"Сначала сохраните объект!");
			else
			{
				var fm = new AttachImages(Object);
				fm.Text += @" для категории: " + ((CTheme)Object).ThemeName;
				fm.ShowDialog(this);
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			var url = string.Format("http://{0}/Yandex/RefreshButton/", ConfigurationManager.AppSettings["connection"]);
#if DEBUG
			url = "http://localhost:1438/Yandex/Refresh/";
#endif
			var read = new HTTPReader(url, null, null);
			try
			{
				read.Request();
			}
			catch { }

		}

		private void FromGoods_Click(object sender, EventArgs e)
		{
			SaveObject();
			if (Object.OID == Guid.Empty)
			{
				ShowError("Сначала сохраните объект");
				return;
			}
			const string sql = @"
SELECT DISTINCT 
	gp.ordValue, gp.paramTypeOID, dbo.NK(gp.paramTypeOID) 
FROM 
	t_Theme t 
	inner join t_ThemeMasters tm on t.OID = tm.OID
	inner join t_Goods g on tm.masterOID = g.category
	inner join t_GoodsParams gp on g.OID = gp.OID
WHERE
	t.OID = {0}
ORDER BY
	gp.ordValue, gp.paramTypeOID, dbo.NK(gp.paramTypeOID)
";
			const string sqlDelete = @"
DELETE FROM t_TemplateParams
WHERE
	OID = {0}
";
			const string sqlInsert = @"
INSERT INTO t_TemplateParams (OID, paramTypeOID, ordValue)
VALUES({0}, {1}, {2})
";
			op.ExecuteCommand(string.Format(sqlDelete, ToSqlString(Object.OID)));
			var i = 1;
			var paramTypes = new Hashtable();
			var ds = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(ObjectOID))));
			foreach (var paramType in ds.Table.Rows.Cast<DataRow>().Select(dr => (Guid)dr["paramTypeOID"]).Where(paramType => !paramTypes.ContainsKey(paramType)))
			{
				op.ExecuteCommand(string.Format(sqlInsert, ToSqlString(Object.OID), ToSqlString(paramType), ToSqlString(i++)));
				paramTypes[paramType] = 1;
			}
			LoadData();
			MessageBox.Show(@"Задача завершена");
		}

		private void DelButton_Click(object sender, EventArgs e)
		{
			var dr = SeoTextGrid.GetSelectedRow();
			if (dr == null) return;
			dr.Delete();
		}

		private void AddButton_Click(object sender, EventArgs e)
		{
			var fm = new ListManufArticlesDialog();
			if (fm.ShowDialog(this) == DialogResult.OK)
			{
				var dr = SeoTextsDataSet.Table.NewRow();
				dr["propValue"] = fm.SelectedArticle["OID"];
				dr["propValue_NK"] = fm.SelectedArticle["header"];
				dr["manufacturerOID"] = fm.SelectedCompany != null ? (Guid)fm.SelectedCompany["OID"] : Guid.Empty;
				dr["companyName"] = fm.SelectedCompany != null ? fm.SelectedCompany["companyName"] : null;
				SeoTextsDataSet.Table.Rows.Add(dr);
			}
		}

		private void brandColumn_ButtonClick(object sender, DataGridCellEventArgs e)
		{
			var drv = (DataRowView)e.CurrentRow;
			var itemsDlg = new ListDialog("CCompany", new[] { "companyName" }, new[] { "Название компании" });
			if (itemsDlg.ShowDialog(this) != DialogResult.OK) return;
			drv["manufacturerOID"] = itemsDlg.SelectedOID;
			drv["brandName"] = itemsDlg.SelectedRow["companyName"].ToString();
		}

        private void TagsDataGrid_AddClick(object sender, EventArgs e)
        {
            var fm = new ListDialog("CLandingPage", new[] { "name", "shortLink" }, new[] { "Название", "Ссылка" });
            if (fm.ShowDialog(this) != DialogResult.OK) return;
            var o = fm.SelectedOID;
            var s = fm.SelectedNK;
            var ordValue = GetLastValue("tags");
            MultiProps.Tables["tags"].Rows.Add(new object[] { ordValue, o, s });
        }

        private void TagsDataGrid_DeleteClick(object sender, EventArgs e)
        {
            var dr = TagsDataGrid.extendedDataGrid1.GetSelectedRow();
            dr?.Delete();
        }

        private void TagsDataGrid_UpClick(object sender, EventArgs e)
        {
            var dr = TagsDataGrid.extendedDataGrid1.GetSelectedRow();
            DataRow before = GetBeforeSelectedRow();
            if (before != null)
            {
                int ord = dr.Field<int>("ordValue");
                dr["ordValue"] = before.Field<int>("ordValue");
                before["ordValue"] = ord;
            }

        }

        private void TagsDataGrid_DownClick(object sender, EventArgs e)
        {
            var dr = TagsDataGrid.extendedDataGrid1.GetSelectedRow();
            DataRow after = GetAfterSelectedRow();
            if (after != null)
            {
                int ord = dr.Field<int>("ordValue");
                dr["ordValue"] = after.Field<int>("ordValue");
                after["ordValue"] = ord;
            }
        }

        private DataRow GetAfterSelectedRow()
        {
            TagsDataGrid.extendedDataGrid1.SuspendLayout();
            try
            {
                if (TagsDataGrid.extendedDataGrid1.DataSource == null || TagsDataGrid.extendedDataGrid1.DataMember == null)
                    return null;

                CurrencyManager cm = (CurrencyManager)TagsDataGrid.extendedDataGrid1.BindingContext[TagsDataGrid.extendedDataGrid1.DataSource, TagsDataGrid.extendedDataGrid1.DataMember];

                cm.Position += 1;
                try
                {
                    if (cm != null && cm.Position != -1 && cm.Current is DataRowView)
                    {
                        DataRowView rv = (DataRowView)cm.Current;
                        return rv.Row;
                    }
                    else
                        return null;
                }
                finally
                {
                    cm.Position -= 1;
                }
            }
            finally
            {
                TagsDataGrid.extendedDataGrid1.ResumeLayout();
            }
        }
        private DataRow GetBeforeSelectedRow()
        {
            TagsDataGrid.extendedDataGrid1.SuspendLayout();
            try
            {
                if (TagsDataGrid.extendedDataGrid1.DataSource == null || TagsDataGrid.extendedDataGrid1.DataMember == null)
                    return null;

                CurrencyManager cm = (CurrencyManager)TagsDataGrid.extendedDataGrid1.BindingContext[TagsDataGrid.extendedDataGrid1.DataSource, TagsDataGrid.extendedDataGrid1.DataMember];

                cm.Position -= 1;
                try
                {
                    if (cm != null && cm.Position != -1 && cm.Current is DataRowView)
                    {
                        DataRowView rv = (DataRowView)cm.Current;
                        return rv.Row;
                    }
                    else
                        return null;
                }
                finally
                {
                    cm.Position += 1;
                }
            }
            finally
            {
                TagsDataGrid.extendedDataGrid1.ResumeLayout();
            }
        }
    }
}
