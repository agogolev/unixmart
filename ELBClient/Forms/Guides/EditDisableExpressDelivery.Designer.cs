using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class EditDisableExpressDelivery
	{
		#region Windows Form Designer generated code
		private System.ComponentModel.Container components = null;
private System.Windows.Forms.Button btnCancel;
private System.Windows.Forms.Button btnSave;
private System.Windows.Forms.Label label1;
private System.Windows.Forms.TextBox NameText;

		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.NameText = new System.Windows.Forms.TextBox();
			this.DateOccurValue = new ELBClient.UserControls.ExtDateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 19);
			this.label1.TabIndex = 16;
			this.label1.Text = "Название:";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.Location = new System.Drawing.Point(8, 67);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 27);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Закрыть";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.Location = new System.Drawing.Point(368, 67);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 27);
			this.btnSave.TabIndex = 4;
			this.btnSave.Text = "Сохранить";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// NameText
			// 
			this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.NameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.NameText.Location = new System.Drawing.Point(132, 9);
			this.NameText.Name = "NameText";
			this.NameText.Size = new System.Drawing.Size(308, 20);
			this.NameText.TabIndex = 0;
			// 
			// DateOccurValue
			// 
			this.DateOccurValue.CustomFormat = "yyyy-MM-dd HH:mm:ss";
			this.DateOccurValue.LinkedTo = null;
			this.DateOccurValue.Location = new System.Drawing.Point(132, 39);
			this.DateOccurValue.Name = "DateOccurValue";
			this.DateOccurValue.ReadOnly = false;
			this.DateOccurValue.ShowButtons = true;
			this.DateOccurValue.Size = new System.Drawing.Size(208, 20);
			this.DateOccurValue.TabIndex = 27;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 18);
			this.label2.TabIndex = 28;
			this.label2.Text = "Дата публикации:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// EditDisableExpressDelivery
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(458, 103);
			this.Controls.Add(this.DateOccurValue);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.NameText);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(464, 120);
			this.Name = "EditDisableExpressDelivery";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Редактирование отмены экспресс доставки";
			this.Load += new System.EventHandler(this.fmEditDisableExpressDelivery_Load);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private UserControls.ExtDateTimePicker DateOccurValue;
		private Label label2;
	}
}
