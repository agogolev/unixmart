using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Guides
{
	public partial class ListGoodsWithParam
	{
		#region Windows Form Designer generated code
		private ColumnMenuExtender.DataGridISM dgList;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colName;
		private ColumnMenuExtender.FormattableTextBoxColumn colCategory;
		private ColumnMenuExtender.FormattableTextBoxColumn colManufacturer;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.dgList = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colName = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colCategory = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colManufacturer = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dgList
			// 
			this.dgList.BackgroundColor = System.Drawing.Color.White;
			this.dgList.DataMember = "";
			this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgList.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgList.Location = new System.Drawing.Point(0, 0);
			this.dgList.Name = "dgList";
			this.dgList.Order = null;
			this.dgList.Size = new System.Drawing.Size(424, 343);
			this.dgList.TabIndex = 0;
			this.dgList.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.dgList.DoubleClick += new System.EventHandler(this.dgList_DoubleClick);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgList;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colName,
            this.colCategory,
            this.colManufacturer});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// colName
			// 
			this.colName.FieldName = null;
			this.colName.FilterFieldName = null;
			this.colName.Format = "";
			this.colName.FormatInfo = null;
			this.colName.HeaderText = "Значение";
			this.colName.MappingName = "shortName";
			this.colName.Width = 200;
			// 
			// colCategory
			// 
			this.colCategory.FieldName = null;
			this.colCategory.FilterFieldName = null;
			this.colCategory.Format = "";
			this.colCategory.FormatInfo = null;
			this.colCategory.HeaderText = "Каталог";
			this.colCategory.MappingName = "category";
			this.colCategory.Width = 75;
			// 
			// colManufacturer
			// 
			this.colManufacturer.FieldName = null;
			this.colManufacturer.FilterFieldName = null;
			this.colManufacturer.Format = "";
			this.colManufacturer.FormatInfo = null;
			this.colManufacturer.HeaderText = "Производитель";
			this.colManufacturer.MappingName = "manufacturer";
			this.colManufacturer.Width = 75;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 343);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(424, 46);
			this.panel1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 9);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 27);
			this.button1.TabIndex = 0;
			this.button1.Text = "Закрыть";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// ListGoodsWithParam
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(424, 389);
			this.Controls.Add(this.dgList);
			this.Controls.Add(this.panel1);
			this.Name = "ListGoodsWithParam";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Товары с параметром";
			this.Load += new System.EventHandler(this.fmListGoodsWithParam_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
