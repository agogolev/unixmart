﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ColumnMenuExtender;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListTheme.
	/// </summary>
	public partial class ListTheme : ListForm
	{

		

		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ListTheme()
		{
			InitializeComponent();
		}

		public override void LoadData()
		{
			dataSet1 = lp.GetList(dataGridISM1.GetDataXml().OuterXml);
			foreach (DataRow dr in dataSet1.Tables[0].Rows) dr["FullTheme"] = dr["FullTheme"].ToString().Replace("\r", "").Replace("\n", " ");
			int cri = dataGridISM1.CurrentRowIndex;
			dataGridISM1.Enabled = false;
			dataGridISM1.SetDataBinding(dataSet1, "table");
			dataGridISM1.Enabled = true;

			DataTable dt = dataSet1.Tables["table"];
			if (cri == -1)
			{
				if (dt.Rows.Count > 0)
					cri = 0;
			}
			else if (cri > dt.Rows.Count - 1)
				cri = dt.Rows.Count - 1;
			if (cri >= 0)
			{
				dataGridISM1.CurrentRowIndex = cri;
				dataGridISM1.Select(cri);
			}

			// корявый способ избавится от дизабленных скролбаров :)
			dataGridISM1.Width++; dataGridISM1.Width--;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void dataGridISM1_Reload(object sender, System.EventArgs e)
		{
			LoadData();
		}

		private void fmTheme_Load(object sender, System.EventArgs e)
		{
			dataGridPager1.BindToDataGrid(dataGridISM1);
			LoadData();
			dataGridPager1.UpdatePager(sender, e);
			listContextMenu1.Bind(this);
		}

		private void btnNew_Click(object sender, System.EventArgs e)
		{
			showEditForm(Guid.Empty);
		}

		private void listContextMenu1_EditClick(object sender, System.EventArgs e)
		{
			showEditForm(listContextMenu1.SelectedOID);
		}

		private void showEditForm(Guid OID)
		{
			EditTheme form = new EditTheme();
			form.ObjectOID = OID;
			form.MdiParent = this.MdiParent;
			form.ReloadGrid += new ReloadDelegate(LoadData);
			form.Show();
		}


		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				DataRow dr = null;
				//				if ((dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, e)) != null) 
				if ((dr = dataGridISM1.GetSelectedRow()) != null)
				{
					listContextMenu1.SelectedOID = (Guid)dr["OID"];
					listContextMenu1.Show(dataGridISM1, new Point(e.X, e.Y));
				}
			}
		}

		private void dataGridISM1_DoubleClick(object sender, System.EventArgs e)
		{
			System.Drawing.Point pt = dataGridISM1.PointToClient(Cursor.Position);

			DataGrid.HitTestInfo hti = dataGridISM1.HitTest(pt);
			if (hti.Type == DataGrid.HitTestType.Cell)
			{
				//				DataRow dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, pt.X, pt.Y);
				DataRow dr = dataGridISM1.GetSelectedRow();
				showEditForm((Guid)dr["OID"]);
			}
		}

		private void dataGridISM1_ActionKeyPressed(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Insert)
			{
				showEditForm(Guid.Empty);
			}
			else if (e.KeyCode == Keys.Delete)
			{
				DataTable dt = TableInGrid.GetTable(dataGridISM1);
				Form form = null;
				if (this.Parent is Form)
					form = this.Parent as Form;
				listContextMenu1.DeleteObject(form, (Guid)dt.Rows[dataGridISM1.CurrentRowIndex]["OID"]);
			}
			else if (e.KeyCode == Keys.Enter)
			{
				DataTable dt = TableInGrid.GetTable(dataGridISM1);
				showEditForm((Guid)dt.Rows[dataGridISM1.CurrentRowIndex]["OID"]);
			}
		}


	}
}
