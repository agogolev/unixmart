﻿using System.Data;
using System.Windows.Forms;

namespace ELBClient.Forms.Guides
{
	/// <summary>
	/// Summary description for fmListContraAgents.
	/// </summary>
	public partial class ListBinaryData : ListForm
	{
		
				
		public ListBinaryData()
		{
			InitializeComponent();
			extendedDataGridImageColumn1.SetShowImage += extendedDataGridImageColumn1_SetShowImage;
			extendedDataGridSelectorColumn1.ButtonClick += extendedDataGridSelectorColumn1_ButtonClick;
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void extendedDataGridImageColumn1_SetShowImage(object sender, ColumnMenuExtender.DataGridImageCellEventArgs e) {
			if((int)e.CellValue == 1) e.ImageIndex = 0;
		}

		void extendedDataGridSelectorColumn1_ButtonClick(object sender, ColumnMenuExtender.DataGridCellEventArgs e)
		{
			var dr = (DataRowView) e.CurrentRow;
			Clipboard.SetText(string.Format("/bin.aspx?ID={0}", dr["OID"]));
			MessageBox.Show(this, "Ссылка находится в буфере обмена");
		}

		private void fmListBinaryData_Load(object sender, System.EventArgs e) {
			mainDataGrid1.LoadData();
		}
	}
}
