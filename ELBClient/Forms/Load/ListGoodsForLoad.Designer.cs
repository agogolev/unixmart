using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class ListGoodsForLoad
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridISM dgGoods;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ContextMenu treeMenu;
		private System.Windows.Forms.MenuItem miUpdateTree;
		private System.Windows.Forms.Label lbCount;
		private System.Windows.Forms.TreeView tvMaster;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ELBClient.UserControls.ListContextMenu listContextMenu1;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;

		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListGoodsForLoad));
			this.dgGoods = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnNew = new System.Windows.Forms.Button();
			this.dataSet1 = new System.Data.DataSet();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.listContextMenu1 = new ELBClient.UserControls.ListContextMenu();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tvMaster = new System.Windows.Forms.TreeView();
			this.treeMenu = new System.Windows.Forms.ContextMenu();
			this.miUpdateTree = new System.Windows.Forms.MenuItem();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.lbCount = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// dgGoods
			// 
			this.dgGoods.AllowSorting = false;
			this.dgGoods.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgGoods.ColumnDragEnabled = true;
			this.dgGoods.DataMember = "";
			this.dgGoods.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgGoods.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgGoods.Location = new System.Drawing.Point(0, 0);
			this.dgGoods.Name = "dgGoods";
			this.dgGoods.Order = null;
			this.dgGoods.ReadOnly = true;
			this.dgGoods.Size = new System.Drawing.Size(536, 431);
			this.dgGoods.StockClass = "CGoods";
			this.dgGoods.StockNumInBatch = 0;
			this.dgGoods.TabIndex = 15;
			this.dgGoods.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dgGoods, false);
			this.dgGoods.Reload += new System.EventHandler(this.dataGridISM1_Reload);
			this.dgGoods.ActionKeyPressed += new System.Windows.Forms.KeyEventHandler(this.dataGridISM1_ActionKeyPressed);
			this.dgGoods.DoubleClick += new System.EventHandler(this.dataGridISM1_DoubleClick);
			this.dgGoods.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridISM1_MouseUp);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dgGoods;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn6});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			this.extendedDataGridTableStyle1.PreferredColumnWidth = 100;
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Наименование";
			this.formattableTextBoxColumn1.MappingName = "shortName";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn1, this.menuFilterSort1);
			this.formattableTextBoxColumn1.NullText = "";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = "fCategoryName(OID, 1)";
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "Категория";
			this.formattableTextBoxColumn4.MappingName = "categoryName";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn4, this.menuFilterSort1);
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// formattableTextBoxColumn6
			// 
			this.formattableTextBoxColumn6.FieldName = "NK(manufacturer)";
			this.formattableTextBoxColumn6.FilterFieldName = null;
			this.formattableTextBoxColumn6.Format = "";
			this.formattableTextBoxColumn6.FormatInfo = null;
			this.formattableTextBoxColumn6.HeaderText = "Производитель";
			this.formattableTextBoxColumn6.MappingName = "manufacturer";
			this.columnMenuExtender1.SetMenu(this.formattableTextBoxColumn6, this.menuFilterSort1);
			this.formattableTextBoxColumn6.Width = 75;
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "");
			this.imageList1.Images.SetKeyName(1, "");
			this.imageList1.Images.SetKeyName(2, "");
			// 
			// btnNew
			// 
			this.btnNew.BackColor = System.Drawing.SystemColors.Control;
			this.btnNew.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnNew.Location = new System.Drawing.Point(8, 9);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(192, 28);
			this.btnNew.TabIndex = 17;
			this.btnNew.Text = "Заполнить полное наименование";
			this.btnNew.UseVisualStyleBackColor = false;
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// listContextMenu1
			// 
			this.listContextMenu1.EditClick += new System.EventHandler(this.listContextMenu1_EditClick);
			this.listContextMenu1.NewClick += new System.EventHandler(this.btnNew_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tvMaster);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(256, 477);
			this.panel2.TabIndex = 19;
			// 
			// tvMaster
			// 
			this.tvMaster.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvMaster.Location = new System.Drawing.Point(0, 0);
			this.tvMaster.Name = "tvMaster";
			this.tvMaster.Size = new System.Drawing.Size(256, 477);
			this.tvMaster.TabIndex = 0;
			this.tvMaster.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMaster_AfterSelect);
			// 
			// treeMenu
			// 
			this.treeMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miUpdateTree});
			// 
			// miUpdateTree
			// 
			this.miUpdateTree.Index = 0;
			this.miUpdateTree.Text = "Обновить";
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.dgGoods);
			this.panel3.Controls.Add(this.panel1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(256, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(536, 477);
			this.panel3.TabIndex = 20;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.lbCount);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 431);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(536, 46);
			this.panel1.TabIndex = 20;
			// 
			// lbCount
			// 
			this.lbCount.Location = new System.Drawing.Point(208, 9);
			this.lbCount.Name = "lbCount";
			this.lbCount.Size = new System.Drawing.Size(304, 27);
			this.lbCount.TabIndex = 18;
			this.lbCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// splitter1
			// 
			this.splitter1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.splitter1.Location = new System.Drawing.Point(256, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 477);
			this.splitter1.TabIndex = 39;
			this.splitter1.TabStop = false;
			// 
			// ListGoodsForLoad
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(792, 477);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.MinimumSize = new System.Drawing.Size(560, 115);
			this.Name = "ListGoodsForLoad";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Список товаров";
			this.Load += new System.EventHandler(this.fmListGoods_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
