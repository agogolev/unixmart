using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class ConvertAddress
	{
		#region Windows Form Designer generated code
		private ColumnMenuExtender.DataGridISM dgPeoples;
		private ColumnMenuExtender.DataGridISM dgContacts;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label lbCount;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.dgPeoples = new ColumnMenuExtender.DataGridISM();
			this.dgContacts = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.lbCount = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dgPeoples)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgContacts)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgPeoples
			// 
			this.dgPeoples.BackgroundColor = System.Drawing.Color.White;
			this.dgPeoples.DataMember = "";
			this.dgPeoples.Dock = System.Windows.Forms.DockStyle.Top;
			this.dgPeoples.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgPeoples.Location = new System.Drawing.Point(0, 0);
			this.dgPeoples.Name = "dgPeoples";
			this.dgPeoples.Order = null;
			this.dgPeoples.Size = new System.Drawing.Size(608, 320);
			this.dgPeoples.TabIndex = 0;
			this.dgPeoples.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																													this.extendedDataGridTableStyle1});
			// 
			// dgContacts
			// 
			this.dgContacts.BackgroundColor = System.Drawing.Color.White;
			this.dgContacts.DataMember = "";
			this.dgContacts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgContacts.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgContacts.Location = new System.Drawing.Point(0, 320);
			this.dgContacts.Name = "dgContacts";
			this.dgContacts.Order = null;
			this.dgContacts.Size = new System.Drawing.Size(608, 181);
			this.dgContacts.TabIndex = 1;
			this.dgContacts.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																													 this.extendedDataGridTableStyle2});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgPeoples;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																																																									this.formattableTextBoxColumn1,
																																																									this.formattableTextBoxColumn2});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "имя";
			this.formattableTextBoxColumn1.MappingName = "lastName";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "email";
			this.formattableTextBoxColumn2.MappingName = "email";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// extendedDataGridTableStyle2
			// 
			this.extendedDataGridTableStyle2.DataGrid = this.dgContacts;
			this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																																																									this.formattableTextBoxColumn3,
																																																									this.formattableTextBoxColumn4});
			this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle2.MappingName = "table.contacts";
			this.extendedDataGridTableStyle2.ReadOnly = true;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Тип";
			this.formattableTextBoxColumn3.MappingName = "ordValue";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = null;
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "Значение";
			this.formattableTextBoxColumn4.MappingName = "contact";
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.lbCount);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 469);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(608, 32);
			this.panel1.TabIndex = 2;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(528, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "Закрыть";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(8, 8);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(96, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Конвертация";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// lbCount
			// 
			this.lbCount.Location = new System.Drawing.Point(112, 8);
			this.lbCount.Name = "lbCount";
			this.lbCount.TabIndex = 2;
			this.lbCount.Text = "Счётчик: 0";
			// 
			// fmConvertAddress
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(608, 501);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.dgContacts);
			this.Controls.Add(this.dgPeoples);
			this.Name = "fmConvertAddress";
			this.Text = "fmConvertAddress";
			this.Load += new System.EventHandler(this.fmConvertAddress_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgPeoples)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgContacts)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
