using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class LoadParams
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
private System.Windows.Forms.Button btnClose;
private System.Windows.Forms.Panel panel2;
private System.Windows.Forms.ListBox lbItems;
private System.Windows.Forms.Button LoadList;
private System.Windows.Forms.OpenFileDialog odFiles;
private System.Windows.Forms.TextBox txtLog;
private System.Windows.Forms.Panel panel3;
private System.Windows.Forms.Splitter splitter1;
private System.Windows.Forms.Panel panel4;
private System.Windows.Forms.Button LoadParamsButton;
private System.Windows.Forms.Label lbCounter;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.lbCounter = new System.Windows.Forms.Label();
			this.LoadParamsButton = new System.Windows.Forms.Button();
			this.LoadList = new System.Windows.Forms.Button();
			this.lbItems = new System.Windows.Forms.ListBox();
			this.odFiles = new System.Windows.Forms.OpenFileDialog();
			this.txtLog = new System.Windows.Forms.TextBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 455);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(648, 46);
			this.panel1.TabIndex = 0;
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(568, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lbCounter);
			this.panel2.Controls.Add(this.LoadParamsButton);
			this.panel2.Controls.Add(this.LoadList);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(648, 74);
			this.panel2.TabIndex = 1;
			// 
			// lbCounter
			// 
			this.lbCounter.Location = new System.Drawing.Point(273, 38);
			this.lbCounter.Name = "lbCounter";
			this.lbCounter.Size = new System.Drawing.Size(96, 19);
			this.lbCounter.TabIndex = 3;
			this.lbCounter.Text = "Счётчик:";
			// 
			// LoadParamsButton
			// 
			this.LoadParamsButton.Location = new System.Drawing.Point(126, 9);
			this.LoadParamsButton.Name = "LoadParamsButton";
			this.LoadParamsButton.Size = new System.Drawing.Size(128, 48);
			this.LoadParamsButton.TabIndex = 2;
			this.LoadParamsButton.Text = "Загрузить";
			this.LoadParamsButton.Click += new System.EventHandler(this.LoadParams_Click);
			// 
			// LoadList
			// 
			this.LoadList.Location = new System.Drawing.Point(8, 9);
			this.LoadList.Name = "LoadList";
			this.LoadList.Size = new System.Drawing.Size(112, 48);
			this.LoadList.TabIndex = 0;
			this.LoadList.Text = "Загрузить с диска";
			this.LoadList.Click += new System.EventHandler(this.LoadList_Click);
			// 
			// lbItems
			// 
			this.lbItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lbItems.Location = new System.Drawing.Point(0, 0);
			this.lbItems.Name = "lbItems";
			this.lbItems.Size = new System.Drawing.Size(248, 368);
			this.lbItems.TabIndex = 2;
			// 
			// txtLog
			// 
			this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtLog.Location = new System.Drawing.Point(0, 0);
			this.txtLog.Multiline = true;
			this.txtLog.Name = "txtLog";
			this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtLog.Size = new System.Drawing.Size(397, 381);
			this.txtLog.TabIndex = 4;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.lbItems);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel3.Location = new System.Drawing.Point(0, 74);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(248, 381);
			this.panel3.TabIndex = 5;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(248, 74);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 381);
			this.splitter1.TabIndex = 6;
			this.splitter1.TabStop = false;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.txtLog);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(251, 74);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(397, 381);
			this.panel4.TabIndex = 7;
			// 
			// LoadParams
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(648, 501);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "LoadParams";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Загрузка описаний товаров";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
