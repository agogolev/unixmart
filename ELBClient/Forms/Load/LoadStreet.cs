﻿using System;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using System.Windows.Forms;
using System.IO;
using MetaData;

namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmLoadStreet.
	/// </summary>
	public partial class LoadStreet : ListForm
	{
		private DataSetISM ds;
		private DataSetISM dsCity;
		private const string connStr = @"DSN=Файлы dBASE;DBQ=%DirName%;DefaultDir=%DirName%;DriverId=533;";
		private readonly Hashtable cities = new Hashtable();
		private readonly Hashtable shopTypes = new Hashtable();
		private int processedItems;
	

		public LoadStreet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e) {
			Close();
		}
		private bool LoadCities() {
			processedItems = 0;
			shopTypes["77"] = 1;//Москва и моск обл
			shopTypes["50"] = 1;
			shopTypes["78"] = 2;// питер и обл
			shopTypes["47"] = 2;
			shopTypes["38"] = 3;// иркутск и обл
			//shopTypes["??"] = 4;// краснодар и обл
			shopTypes["52"] = 5;// Нижний Новгород и обл
			string connS = connStr.Replace("%DirName%", Path.GetDirectoryName(ofdDBF.FileName));
			var cmd = new OdbcCommand("", new OdbcConnection(connS)) {CommandType = CommandType.Text};
			var sql = new StringBuilder();
			sql.Append(@"
SELECT  * 
FROM
	KLADR
WHERE
	CODE LIKE '77%' OR
	CODE LIKE '50%' OR
	CODE LIKE '78%' OR
	CODE LIKE '47%' OR
	CODE LIKE '38%' OR
	CODE LIKE '52%' 
");
			/*
SELECT  * 
FROM
	KLADR
WHERE
	CODE LIKE '77%' OR
	CODE LIKE '50%' OR
	CODE LIKE '78%' OR
	CODE LIKE '47%' OR
	CODE LIKE '38%' OR
	CODE LIKE '52%' 
			  */
			cmd.CommandText = sql.ToString();
			var da = new OdbcDataAdapter(cmd);
			var dsCities = new DataSetISM();
			da.Fill(dsCities);
			foreach(DataRow dr in dsCities.Table.Rows) {
				var ID = (string)dr["code"];
				if(ID.Substring(ID.Length-2, 2) != "00") continue; // Устаревшее название города
				var region = ID.Substring(0, 2);
				var shortID = ID.Substring(0, ID.Length-2);
				var name = (string)dr["name"];
				var type = (string)dr["socr"];
				var sql1 = @"
Declare
	@cityOID uniqueidentifier

SELECT @cityOID = OID FROM t_City WHERE ID = '"+ID+@"'
if @cityOID is null Begin
	exec spInnerCreateObject 'CCity', @cityOID OUTPUT
End

UPDATE t_City SET
	name = "+ToSqlString(name)+@",
	type = "+ToSqlString(type)+@",
	shopType = "+ToSqlString(shopTypes[region])+@",
	ID = "+ToSqlString(ID)+@",
	shortID = "+ToSqlString(shortID)+@"
WHERE
	OID = @cityOID
";
				try {
					op.ExecuteCommand(sql1);
				}
				catch(Exception ex) {
					ShowError(ID+" "+name+": "+ex.Message);
					return false;
				}
				processedItems++;
				lbCount.Text = @"Обработано городов и районов: "+processedItems;
				lbCount.Refresh();
			}
			return true;
		}
		private void PrepareCity() {
			dsCity = new DataSetISM(lp.GetDataSetSql("SELECT * FROM t_City"));
			foreach(DataRow dr in dsCity.Table.Rows) {
				cities[dr["shortID"]] = dr["OID"];
			}
		}
		private Guid FindCity(string code) {
			var cityOID = cities[code];
			return cityOID == null ? Guid.Empty : (Guid)cityOID;
		}
		private void btnLoad_Click(object sender, EventArgs e)
		{
			if (ofdDBF.ShowDialog(this) != DialogResult.OK) return;
			try {
					
				var fileName = Path.GetFileNameWithoutExtension(ofdDBF.FileName);
				var connS = connStr.Replace("%DirName%", Path.GetDirectoryName(ofdDBF.FileName));
				var cmd = new OdbcCommand("", new OdbcConnection(connS)) {CommandType = CommandType.Text};
				var sql = new StringBuilder();
				sql.Append(@"
SELECT top 1000 * 
FROM
	"+fileName+@"
");
				cmd.CommandText = sql.ToString();
				var da = new OdbcDataAdapter(cmd);
				ds = new DataSetISM();
				da.Fill(ds);
				dgStreets.DataSource = ds;
				dgStreets.DataMember = "table";
				processedItems = 0;
			}
			catch (Exception ex) {
				MessageBox.Show(ex.Message);
				return;
			}
		}

		private void btnExport_Click(object sender, EventArgs e) {
			if (cbUpdateCities.Checked) {
				if(!LoadCities()) return;
			}
			PrepareCity();
			var connS = connStr.Replace("%DirName%", Path.GetDirectoryName(ofdDBF.FileName));
			var conn = new OdbcConnection(connS);
			var cmd = new OdbcCommand("", conn) {CommandType = CommandType.Text};
			processedItems = 0;
			const string sql1 = @"
SELECT * FROM STREET
WHERE
	CODE LIKE '77%' OR
	CODE LIKE '50%' OR
	CODE LIKE '78%' OR
	CODE LIKE '47%' OR
	CODE LIKE '38%' OR
	CODE LIKE '52%' 
";
			/*
SELECT * FROM STREET
WHERE
	CODE LIKE '77%' OR
	CODE LIKE '50%' OR
	CODE LIKE '78%' OR
	CODE LIKE '47%' OR
	CODE LIKE '38%' OR
	CODE LIKE '52%' 
			 * */
			cmd.CommandText = sql1;
			conn.Open();
			OdbcDataReader dr = null;
			try {
				dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
				while(dr.Read()) {
					lbCount.Text = @"Обработано улиц: "+processedItems;
					lbCount.Refresh();
					processedItems++;
					var ID = (string)dr["code"];
					if(ID.Substring(ID.Length-2, 2) != "00") continue; // Устаревшая улица
					var cityID = ID.Substring(0, ID.Length - 6);
					var cityOID = FindCity(cityID);
					if(cityOID == Guid.Empty) continue;
					
					var name = (string)dr["name"];
					var type = (string)dr["socr"];
					var sql = @"
Declare
	@streetOID uniqueidentifier

SELECT @streetOID = OID FROM t_Street WHERE ID = '"+ID+@"'
if @streetOID is null Begin
	exec spInnerCreateObject 'CStreet', @streetOID OUTPUT
	UPDATE t_Street SET
		name = "+ToSqlString(name)+@",
		type = "+ToSqlString(type)+@",
		city = "+ToSqlString(cityOID)+@",
		ID = "+ToSqlString(ID)+@"
	WHERE
		OID = @streetOID
End
";
					op.ExecuteCommand(sql);
				}
			}
			finally {
				if (dr != null) dr.Close();
			}
			MessageBox.Show(this, @"Импорт завершён");
			lbCount.Text = "";
		}
	}
}
