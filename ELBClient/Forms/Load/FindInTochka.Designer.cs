﻿namespace ELBClient.Forms.Load
{
	partial class FindInTochka
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.OurGoods = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle2 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableBooleanColumn2 = new ColumnMenuExtender.FormattableBooleanColumn();
			this.panel4 = new System.Windows.Forms.Panel();
			this.button3 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.NotLinkedOur = new System.Windows.Forms.CheckBox();
			this.ShowOur = new System.Windows.Forms.Button();
			this.NameOur = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.BrandListOur = new System.Windows.Forms.ComboBox();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel1 = new System.Windows.Forms.Panel();
			this.TochkaProducts = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
			this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label5 = new System.Windows.Forms.Label();
			this.NotLinkedTochka = new System.Windows.Forms.CheckBox();
			this.NameTochka = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.ShowTochka = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.BrandListTochka = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.OurGoods)).BeginInit();
			this.panel4.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.TochkaProducts)).BeginInit();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// OurGoods
			// 
			this.OurGoods.BackgroundColor = System.Drawing.Color.White;
			this.OurGoods.DataMember = "";
			this.OurGoods.Dock = System.Windows.Forms.DockStyle.Fill;
			this.OurGoods.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.OurGoods.Location = new System.Drawing.Point(634, 61);
			this.OurGoods.Name = "OurGoods";
			this.OurGoods.Order = null;
			this.OurGoods.Size = new System.Drawing.Size(568, 521);
			this.OurGoods.TabIndex = 4;
			this.OurGoods.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle2});
			// 
			// extendedDataGridTableStyle2
			// 
			this.extendedDataGridTableStyle2.DataGrid = this.OurGoods;
			this.extendedDataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn4,
            this.formattableBooleanColumn2});
			this.extendedDataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle2.MappingName = "Table";
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Модель";
			this.formattableTextBoxColumn3.MappingName = "Model";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = null;
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "Бренд";
			this.formattableTextBoxColumn4.MappingName = "ManufacturerName";
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// formattableBooleanColumn2
			// 
			this.formattableBooleanColumn2.FieldName = null;
			this.formattableBooleanColumn2.FilterFieldName = null;
			this.formattableBooleanColumn2.HeaderText = "Связать";
			this.formattableBooleanColumn2.MappingName = "IsLinked";
			this.formattableBooleanColumn2.Width = 75;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.button3);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel4.Location = new System.Drawing.Point(606, 61);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(28, 521);
			this.panel4.TabIndex = 3;
			// 
			// button3
			// 
			this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button3.Location = new System.Drawing.Point(0, 0);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(28, 521);
			this.button3.TabIndex = 0;
			this.button3.Text = "<>";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label6);
			this.panel2.Controls.Add(this.NotLinkedOur);
			this.panel2.Controls.Add(this.ShowOur);
			this.panel2.Controls.Add(this.NameOur);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.BrandListOur);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(606, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(596, 61);
			this.panel2.TabIndex = 2;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(24, 14);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(123, 20);
			this.label6.TabIndex = 10;
			this.label6.Text = "Наши товары";
			// 
			// NotLinkedOur
			// 
			this.NotLinkedOur.AutoSize = true;
			this.NotLinkedOur.Checked = true;
			this.NotLinkedOur.CheckState = System.Windows.Forms.CheckState.Checked;
			this.NotLinkedOur.Location = new System.Drawing.Point(431, 34);
			this.NotLinkedOur.Name = "NotLinkedOur";
			this.NotLinkedOur.Size = new System.Drawing.Size(138, 17);
			this.NotLinkedOur.TabIndex = 7;
			this.NotLinkedOur.Text = "Только без привязки";
			this.NotLinkedOur.UseVisualStyleBackColor = true;
			// 
			// ShowOur
			// 
			this.ShowOur.Location = new System.Drawing.Point(476, 7);
			this.ShowOur.Name = "ShowOur";
			this.ShowOur.Size = new System.Drawing.Size(75, 23);
			this.ShowOur.TabIndex = 5;
			this.ShowOur.Text = "Показать";
			this.ShowOur.UseVisualStyleBackColor = true;
			this.ShowOur.Click += new System.EventHandler(this.ShowOur_Click);
			// 
			// NameOur
			// 
			this.NameOur.Location = new System.Drawing.Point(272, 32);
			this.NameOur.Name = "NameOur";
			this.NameOur.Size = new System.Drawing.Size(153, 20);
			this.NameOur.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(214, 35);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(61, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Название:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(214, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(42, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Бренд:";
			// 
			// BrandListOur
			// 
			this.BrandListOur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.BrandListOur.FormattingEnabled = true;
			this.BrandListOur.Location = new System.Drawing.Point(272, 8);
			this.BrandListOur.Name = "BrandListOur";
			this.BrandListOur.Size = new System.Drawing.Size(201, 21);
			this.BrandListOur.TabIndex = 2;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(603, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 582);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.TochkaProducts);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(603, 582);
			this.panel1.TabIndex = 0;
			// 
			// TochkaProducts
			// 
			this.TochkaProducts.BackgroundColor = System.Drawing.Color.White;
			this.TochkaProducts.DataMember = "";
			this.TochkaProducts.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TochkaProducts.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.TochkaProducts.Location = new System.Drawing.Point(0, 61);
			this.TochkaProducts.Name = "TochkaProducts";
			this.TochkaProducts.Order = null;
			this.TochkaProducts.Size = new System.Drawing.Size(603, 521);
			this.TochkaProducts.TabIndex = 4;
			this.TochkaProducts.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.TochkaProducts;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableBooleanColumn1,
            this.extendedDataGridSelectorColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Полное название";
			this.formattableTextBoxColumn1.MappingName = "ProductName";
			this.formattableTextBoxColumn1.ReadOnly = true;
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Короткое название";
			this.formattableTextBoxColumn2.MappingName = "ShortName";
			this.formattableTextBoxColumn2.ReadOnly = true;
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableBooleanColumn1
			// 
			this.formattableBooleanColumn1.FieldName = null;
			this.formattableBooleanColumn1.FilterFieldName = null;
			this.formattableBooleanColumn1.HeaderText = "Связать";
			this.formattableBooleanColumn1.MappingName = "IsLinked";
			this.formattableBooleanColumn1.Width = 75;
			// 
			// extendedDataGridSelectorColumn1
			// 
			this.extendedDataGridSelectorColumn1.FieldName = null;
			this.extendedDataGridSelectorColumn1.FilterFieldName = null;
			this.extendedDataGridSelectorColumn1.HeaderText = "Наш привязанный товар";
			this.extendedDataGridSelectorColumn1.MappingName = "ourGoodsName";
			this.extendedDataGridSelectorColumn1.Width = 75;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label5);
			this.panel3.Controls.Add(this.NotLinkedTochka);
			this.panel3.Controls.Add(this.NameTochka);
			this.panel3.Controls.Add(this.label3);
			this.panel3.Controls.Add(this.ShowTochka);
			this.panel3.Controls.Add(this.label1);
			this.panel3.Controls.Add(this.BrandListTochka);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(603, 61);
			this.panel3.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(12, 14);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(125, 20);
			this.label5.TabIndex = 9;
			this.label5.Text = "Товары точки";
			// 
			// NotLinkedTochka
			// 
			this.NotLinkedTochka.AutoSize = true;
			this.NotLinkedTochka.Checked = true;
			this.NotLinkedTochka.CheckState = System.Windows.Forms.CheckState.Checked;
			this.NotLinkedTochka.Location = new System.Drawing.Point(391, 36);
			this.NotLinkedTochka.Name = "NotLinkedTochka";
			this.NotLinkedTochka.Size = new System.Drawing.Size(138, 17);
			this.NotLinkedTochka.TabIndex = 8;
			this.NotLinkedTochka.Text = "Только без привязки";
			this.NotLinkedTochka.UseVisualStyleBackColor = true;
			// 
			// NameTochka
			// 
			this.NameTochka.Location = new System.Drawing.Point(232, 34);
			this.NameTochka.Name = "NameTochka";
			this.NameTochka.Size = new System.Drawing.Size(153, 20);
			this.NameTochka.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(174, 37);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(61, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Название:";
			// 
			// ShowTochka
			// 
			this.ShowTochka.Location = new System.Drawing.Point(439, 8);
			this.ShowTochka.Name = "ShowTochka";
			this.ShowTochka.Size = new System.Drawing.Size(75, 23);
			this.ShowTochka.TabIndex = 2;
			this.ShowTochka.Text = "Показать";
			this.ShowTochka.UseVisualStyleBackColor = true;
			this.ShowTochka.Click += new System.EventHandler(this.ShowTochka_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(174, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(42, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Бренд:";
			// 
			// BrandListTochka
			// 
			this.BrandListTochka.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.BrandListTochka.FormattingEnabled = true;
			this.BrandListTochka.Location = new System.Drawing.Point(232, 9);
			this.BrandListTochka.Name = "BrandListTochka";
			this.BrandListTochka.Size = new System.Drawing.Size(201, 21);
			this.BrandListTochka.TabIndex = 0;
			// 
			// FindInTochka
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1202, 582);
			this.Controls.Add(this.OurGoods);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel1);
			this.Name = "FindInTochka";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Поиск соответствий";
			this.Load += new System.EventHandler(this.FindInTochka_Load);
			((System.ComponentModel.ISupportInitialize)(this.OurGoods)).EndInit();
			this.panel4.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.TochkaProducts)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox BrandListTochka;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox NameTochka;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button ShowTochka;
		private System.Windows.Forms.Button ShowOur;
		private System.Windows.Forms.TextBox NameOur;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox BrandListOur;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button button3;
		private ColumnMenuExtender.DataGridISM TochkaProducts;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private ColumnMenuExtender.DataGridISM OurGoods;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn2;
		private System.Windows.Forms.CheckBox NotLinkedOur;
		private System.Windows.Forms.CheckBox NotLinkedTochka;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
	}
}