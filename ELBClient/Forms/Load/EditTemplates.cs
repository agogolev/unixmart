﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ColumnMenuExtender;
using Ecommerce.Extensions;
using ELBClient.Classes;
using ELBClient.Forms.Dialogs;
using MetaData;
using OfficeOpenXml;

namespace ELBClient.Forms.Load
{
    public partial class EditTemplates : ListForm
    {
        private readonly TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
        public Dictionary<int, int> ShopTypes;
        private DataSet dataSource;
        //private int parentNode = ELBExpImp.Constants.MASTER_NODE_ROOT;
        private Tree tree;

        public EditTemplates()
        {
            InitializeComponent();
            extendedDataGridSelectorColumn1.ButtonClick += extendedDataGridSelectorColumn1_ButtonClick;
        }

        public DataSetISM SourceData { get; set; }
        public Guid SelectedTheme { get; set; }

        private void extendedDataGridSelectorColumn1_ButtonClick(object sender, DataGridCellEventArgs e)
        {
            var drv = (DataRowView)e.CurrentRow;
            if ((bool)drv["isFilter"])
            {
                var fm = new EditFilter { ObjectOID = (Guid)drv["paramTypeOID"], ThemeOID = (Guid)drv["OID"] };
                fm.ShowDialog(this);
            }
        }

        private void LoadObjects()
        {
            //LoadObject("treeName", null);

            //Filling OID
            string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree nodeID=\"" +
                         ShopTypes[cbShopType.SelectedValue] + "\" withParent=\"true\"/>";
            dataSource = treeProvider.GetTreeContent(xml);
        }

        //Filling Tree
        private void BuildTree()
        {
            var nodes = new Stack();
            tree = new Tree(0, int.MaxValue);
            nodes.Push(tree);

            int prevNode = 0;
            foreach (DataRow dr in dataSource.Tables["table"].Rows)
            {
                var nodeID = (int)dr["nodeID"];
                if (nodeID == prevNode) continue;
                var node = new Node((int)dr["lft"], (int)dr["rgt"])
                {
                    NodeId = nodeID,
                    Name = dr["nodeName"].ToString(),
                    objectOID = dr["OID"] == DBNull.Value ? Guid.Empty : (Guid)dr["OID"],
                    objectNK = dr["NK"].ToString(),
                    objectNKRus = dr["label"].ToString(),
                    className = dr["className"].ToString()
                };
                while (((NodeContainer)nodes.Peek()).Right < node.Left) nodes.Pop();
                ((NodeContainer)nodes.Peek()).Nodes.Add(node);
                if ((node.Left + 1) != node.Right)
                {
                    nodes.Push(node);
                }
                prevNode = nodeID;
            }
        }

        private void LoadTree()
        {
            MasterCatalogTreeView.Nodes.Clear();
            foreach (Node n in tree.Nodes)
            {
                var tn =
                    new TreeNode(n.Name != ""
                        ? n.Name.Replace("\r\n", " ")
                        : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")))
                    {
                        Tag = n
                    };
                if (n.objectOID != Guid.Empty)
                    tn.ForeColor = Color.Red;
                MasterCatalogTreeView.Nodes.Add(tn);
                LoadNode(tn, n);
            }
        }

        private void LoadNode(TreeNode treeNode, Node node)
        {
            foreach (Node n in node.Nodes)
            {
                var tn =
                    new TreeNode(n.Name != ""
                        ? n.Name.Replace("\r\n", " ")
                        : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")))
                    {
                        Tag = n
                    };
                if (n.objectOID != Guid.Empty)
                    tn.ForeColor = Color.Red;
                treeNode.Nodes.Add(tn);
                LoadNode(tn, n);
            }
        }

        private void EditTemplates_Load(object sender, EventArgs e)
        {
            LoadLookup();
        }

        private void LoadLookup()
        {
            const string sql = @"
SELECT
	shopType,
	name,
	themeRootNode
FROM
	t_TypeShop
ORDER BY
	shopType
";
            var ds = new DataSetISM(lp.GetDataSetSql(sql));
            // ReSharper disable CoVariantArrayConversion
            cbShopType.AddItems(
                ds.Table.AsEnumerable()
                    .Select(k => new ListItem { Value = k.Field<int>("shopType"), Text = k.Field<string>("name") })
                    .ToArray());
            ShopTypes = ds.Table.AsEnumerable().ToDictionary(row => row.Field<int>("shopType"),
                row => row.Field<int>("themeRootNode"));
            // ReSharper restore CoVariantArrayConversion
        }

        private void MasterCatalogTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var n = (Node)e.Node.Tag;
            SelectedTheme = n.objectOID;
            LoadGrid();
        }

        private void LoadGrid()
        {
            Guid selectedOID = SelectedTheme;
            const string sql = @"
SELECT
	t.OID,
	tp.ordValue,
	tp.paramTypeOID,
	paramName = pt.name,
    altName = pt.altName,
	tp.unit,
	tp.isFilter,
	tp.isFullName,
    tp.isVisible
FROM
	t_Theme t
	left outer join t_TemplateParams tp on t.OID = tp.OID
    left join t_ParamType pt on tp.paramTypeOID = pt.OID
WHERE
	t.OID = {0}
";
            SourceData = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(selectedOID))));
            SourceData.Table.RowChanged += Table_RowChanged;
            SourceData.Table.Columns.Add("selectFilter", typeof(string), "");
            TemplateGrid.SetDataBinding(SourceData, "Table");
            TemplateGrid.DisableNewDel();
        }

        private void Table_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            DataRow dr = e.Row;
            const string sql = @"
UPDATE t_TemplateParams SET unit = {0}, isfilter = {1}, isFullName = {2}, isVisible = {3}
WHERE
	OID = {4} and ordValue = {5}

UPDATE t_GoodsParams SET unit = {0}
FROM t_GoodsParams gp
	inner join t_Goods g on gp.OID = g.OID and gp.paramTypeOID = {6}
	inner join t_ThemeMasters tm on g.category = tm.masterOID And tm.OID = {4}

UPDATE t_ParamType SET altName = {7} WHERE OID = {6}
	
";
            string sqlUpdate = string.Format(sql,
                ToSqlString(dr["unit"]),
                ToSqlString(dr["isFilter"]),
                ToSqlString(dr["isFullName"]),
                ToSqlString(dr["isVisible"]),
                ToSqlString(dr["OID"]),
                ToSqlString(dr["ordValue"]),
                ToSqlString(dr["paramTypeOID"]),
                string.IsNullOrWhiteSpace(dr["altName"].ToString()) ? "null" : ToSqlString(dr["altName"]));
            op.ExecuteCommand(sqlUpdate);
        }

        private void cbShopType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadObjects();
            BuildTree();
            LoadTree();
        }

        private void ClearTemplateButton_Click(object sender, EventArgs e)
        {
            if (MasterCatalogTreeView.SelectedNode == null)
            {
                ShowError("Выберите категорию");
                return;
            }
            Guid selectedOID = ((Node)MasterCatalogTreeView.SelectedNode.Tag).objectOID;
            const string sql = @"
DELETE FROM t_TemplateParams 
WHERE
	OID = {0}
";
            op.ExecuteCommand(string.Format(sql, ToSqlString(selectedOID)));
            LoadGrid();
            MessageBox.Show(@"Задача завершена");
        }

        private void BuildTemplateButton_Click(object sender, EventArgs e)
        {
            if (MasterCatalogTreeView.SelectedNode == null)
            {
                ShowError(@"Выберите категорию");
                return;
            }
            Guid selectedOID = ((Node)MasterCatalogTreeView.SelectedNode.Tag).objectOID;
            const string sql = @"
SELECT DISTINCT gp.ordValue, gp.paramTypeOID, dbo.NK(gp.paramTypeOID), gp.unit
FROM
    t_Nodes n 
	inner join t_Theme t on n.object = t.OID
    inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.tree = '0e5f5d35-3f4f-4418-a571-c8e9974550b2'
	inner join t_ThemeMasters tm on n1.object = tm.OID
	inner join t_Goods g on tm.masterOID = g.category
	inner join t_GoodsParams gp on g.OID = gp.OID
WHERE
	t.OID = {0}
ORDER BY gp.ordValue, dbo.NK(gp.paramTypeOID)
";
            const string sqlDelete = @"
DELETE FROM t_TemplateParams
WHERE
	OID = {0}
";
            const string sqlInsert = @"
INSERT INTO t_TemplateParams (OID, paramTypeOID, ordValue, unit)
VALUES({0}, {1}, {2}, {3})
";
            op.ExecuteCommand(string.Format(sqlDelete, ToSqlString(selectedOID)));
            int i = 1;
            var ds = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(selectedOID))));
            var paramTypes = new Hashtable();
            foreach (DataRow dr in ds.Table.Rows)
            {
                var paramType = (Guid)dr["paramTypeOID"];
                var unit = (string)dr["unit"];
                if (!paramTypes.ContainsKey(paramType))
                {
                    op.ExecuteCommand(string.Format(sqlInsert, ToSqlString(selectedOID), ToSqlString(paramType),
                        ToSqlString(i++), ToSqlString(unit)));
                    paramTypes[paramType] = 1;
                }
            }
            LoadGrid();
            MessageBox.Show(@"Задача завершена");
        }

        private void ClearParams_Click(object sender, EventArgs e)
        {
            if (MasterCatalogTreeView.SelectedNode == null)
            {
                ShowError("Выберите категорию");
                return;
            }
            Guid selectedOID = ((Node)MasterCatalogTreeView.SelectedNode.Tag).objectOID;
            if (
                MessageBox.Show(@"Вы уверены?", @"Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                const string sql = @"
DELETE FROM t_GoodsParams 
FROM 
	t_GoodsParams gp
	inner join t_Goods g on gp.OID = g.OID
	inner join t_ThemeMasters tm on tm.masterOID = g.category
WHERE
	tm.OID = {0}
";
                op.ExecuteCommand(string.Format(sql, ToSqlString(selectedOID)));
                MessageBox.Show(@"Задача завершена");
            }
        }

        private void TemplateGrid_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int rowNum = TemplateGrid.Hit.Row;
                TemplateGrid.CurrentRowIndex = rowNum;
                TemplateMenu.Show(TemplateGrid, e.X, e.Y);
            }
        }

        private void AddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateGrid.EndEdit();
            DataRow currentRow = TemplateGrid.GetSelectedRow();
            if (currentRow == null) return;
            var OID = (Guid)currentRow["OID"];
            int ordValue = currentRow["ordValue"] == DBNull.Value ? 1 : (int)currentRow["ordValue"] + 1;
            var fm = new EditStringValue();
            if (fm.ShowDialog(this) == DialogResult.OK)
            {
                string name = fm.ParamName;
                const string sql = @"
Declare
	@OID uniqueidentifier

	SELECT @OID = OID FROM t_ParamType WHERE name = {0}
	if @OID is null Begin
		exec spInnerCreateObject 'CParamType', @OID OUTPUT

		UPDATE t_ParamType SET
			name = {0},
			paramType = 1
		WHERE
			OID = @OID
	end

	UPDATE t_TemplateParams SET ordValue = ordValue + 1 WHERE OID = {2} and ordValue >= {1}
	INSERT INTO t_TemplateParams(OID, ordValue, paramTypeOID) VALUES({2}, {1}, @OID)	

	UPDATE t_GoodsParams SET ordValue = tp.ordValue
	FROM
		t_GoodsParams gp
		inner join t_Goods g on gp.OID = g.OID 
		inner join t_ThemeMasters tm on g.category = tm.masterOID  
		inner join t_TemplateParams tp on tm.OID = tp.OID and gp.paramTypeOID = tp.paramTypeOID and tp.OID = {2}
";
                op.ExecuteCommand(string.Format(sql, ToSqlString(name), ordValue, ToSqlString(OID)));
                LoadGrid();
            }
        }

        private void DeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateGrid.EndEdit();
            DataRow currentRow = TemplateGrid.GetSelectedRow();
            if (currentRow == null || currentRow["ordValue"] == DBNull.Value) return;
            var OID = (Guid)currentRow["OID"];
            var ordValue = (int)currentRow["ordValue"];
            const string sql = @"
DEclare
	@paramTypeOID uniqueidentifier
SELECT @paramTypeOID = paramTypeOID FROM t_TemplateParams WHERE OID = {1} And ordValue = {0}

Delete FROM t_GoodsParams 
	FROM t_GoodsParams gp
	inner join t_Goods g on gp.OID = g.OID 
	inner join t_ThemeMasters tm on g.category = tm.masterOID  
	inner join t_TemplateParams tp on tm.OID = tp.OID and gp.paramTypeOID = tp.paramTypeOID and tp.OID = {1} and tp.paramTypeOID = @paramTypeOID

DELETE FROM t_TemplateParams WHERE OID = {1} And ordValue = {0}
UPDATE t_TemplateParams SET ordValue = ordValue - 1 WHERE OID = {1} and ordValue > {0}

UPDATE t_GoodsParams SET ordValue = tp.ordValue
FROM
	t_GoodsParams gp
	inner join t_Goods g on gp.OID = g.OID 
	inner join t_ThemeMasters tm on g.category = tm.masterOID  
	inner join t_TemplateParams tp on tm.OID = tp.OID and gp.paramTypeOID = tp.paramTypeOID and tp.OID = {1}
";
            op.ExecuteCommand(string.Format(sql, ordValue, ToSqlString(OID)));
            LoadGrid();
        }

        private void EditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateGrid.EndEdit();
            DataRow currentRow = TemplateGrid.GetSelectedRow();
            if (currentRow == null || currentRow["ordValue"] == DBNull.Value) return;
            var OID = (Guid)currentRow["OID"];
            var ordValue = (int)currentRow["ordValue"];
            var fm = new EditStringValue { ParamName = (string)currentRow["paramName"] };
            if (fm.ShowDialog(this) != DialogResult.OK) return;
            string name = fm.ParamName;
            const string sql = @"
Declare
	@OID uniqueidentifier,
	@paramTypeOID uniqueidentifier

	SELECT @OID = paramTypeOID FROM t_TemplateParams WHERE OID = {1} And ordValue = {0}
	SELECT @paramTypeOID = OID FROM t_ParamType WHERE name = {2}

	UPDATE t_GoodsParams SET ordValue = tp.ordValue
	FROM
		t_GoodsParams gp
		inner join t_Goods g on gp.OID = g.OID 
		inner join t_ThemeMasters tm on g.category = tm.masterOID  
		inner join t_TemplateParams tp on tm.OID = tp.OID and gp.paramTypeOID = tp.paramTypeOID and tp.OID = {1}

	if @paramTypeOID is NULL Begin
		UPDATE t_ParamType SET name = {2} WHERE OID = @OID
	end
	else begin
		UPDATE t_TemplateParams Set paramTypeOID = @paramTypeOID WHERE OID = {1} And ordValue = {0}
		
		UPDATE t_GoodsParams SET paramTypeOID =  @paramTypeOID 
		FROM
			t_GoodsParams gp
			inner join t_Goods g on gp.OID = g.OID 
			inner join t_ThemeMasters tm on g.category = tm.masterOID  
			inner join t_TemplateParams tp on tm.OID = tp.OID and gp.ordValue = tp.ordValue and tp.OID = {1} And tp.ordValue = {0}
			
	end
";
            op.ExecuteCommand(string.Format(sql, ordValue, ToSqlString(OID), ToSqlString(name)));
            LoadGrid();
        }

        private void UpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateGrid.EndEdit();
            DataRow currentRow = TemplateGrid.GetSelectedRow();
            if (currentRow == null || currentRow["ordValue"] == DBNull.Value) return;
            var OID = (Guid)currentRow["OID"];
            var ordValue = (int)currentRow["ordValue"];
            if (ordValue == 1) return; // уже вверху списка
            const string sql = @"
UPDATE t_TemplateParams 
SET
	ordValue = CASE WHEN ordValue = {0} THEN {0} - 1 WHEN ordValue = {0} - 1 THEN {0} END
WHERE
	OID = {1} and ordValue in ({0}, {0} - 1)

UPDATE t_GoodsParams SET ordValue = tp.ordValue
FROM
	t_GoodsParams gp
	inner join t_Goods g on gp.OID = g.OID 
	inner join t_ThemeMasters tm on g.category = tm.masterOID  
	inner join t_TemplateParams tp on tm.OID = tp.OID and gp.paramTypeOID = tp.paramTypeOID and tp.OID = {1}
";
            op.ExecuteCommand(string.Format(sql, ordValue, ToSqlString(OID)));
            LoadGrid();
        }

        private void DownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TemplateGrid.EndEdit();
            DataRow currentRow = TemplateGrid.GetSelectedRow();
            if (currentRow == null || currentRow["ordValue"] == DBNull.Value) return;
            var OID = (Guid)currentRow["OID"];
            var ordValue = (int)currentRow["ordValue"];
            if (!SourceData.Table.AsEnumerable().Any(row => row.Field<int>("ordValue") > ordValue))
                return; // уже внизу списка
            const string sql = @"
UPDATE t_TemplateParams 
SET
	ordValue = CASE WHEN ordValue = {0} THEN {0} + 1 WHEN ordValue = {0} + 1 THEN {0} END
WHERE
	OID = {1} and ordValue in ({0}, {0} + 1)

UPDATE t_GoodsParams SET ordValue = tp.ordValue
FROM
	t_GoodsParams gp
	inner join t_Goods g on gp.OID = g.OID 
	inner join t_ThemeMasters tm on g.category = tm.masterOID  
	inner join t_TemplateParams tp on tm.OID = tp.OID and gp.paramTypeOID = tp.paramTypeOID and tp.OID = {1}
";
            op.ExecuteCommand(string.Format(sql, ordValue, ToSqlString(OID)));
            LoadGrid();
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            if (MasterCatalogTreeView.SelectedNode == null)
            {
                ShowError("Выберите категорию");
                return;
            }
            Guid selectedOID = ((Node)MasterCatalogTreeView.SelectedNode.Tag).objectOID;
            int shopType = cbShopType.SelectedValue;
            const string sql = @"
SELECT
	g.OID,
	g.postavshik_id, 
	g.productType, 
	g.model,
	inStock = ISNULL(gs.inStock, 0)
FROM
	t_Goods g
	left join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = {1}
	inner join t_ThemeMasters tm on g.category = tm.masterOID and tm.OID = {0}
WHERE
    g.postavshik_id is not null
ORDER BY
	g.postavshik_id

SELECT
	tp.ordValue,
	pt.name,
	tp.unit
FROM
	t_TemplateParams tp
	inner join t_ParamType pt on tp.paramTypeOID = pt.OID and tp.OID = {0}
ORDER BY
	tp.ordValue

SELECT
	g.OID,
	pt.name,
	gp.value
FROM
	t_Goods g
	inner join t_GoodsParams gp on g.OID = gp.OID
	inner join t_ParamType pt on gp.paramTypeOID = pt.OID
	inner join t_ThemeMasters tm on g.category = tm.masterOID and tm.OID = {0}	
WHERE
    g.postavshik_id is not null
";
            var ds = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(selectedOID), shopType)));
            var table = new DataTable("Table");
            table.Columns.Add("postavshik_id");
            table.Columns.Add("inStock");
            table.Columns.Add("productType");
            table.Columns.Add("model");
            ds.Tables[1].AsEnumerable()
                .Select(row => new ParamType { ParamName = row.Field<string>("name"), Unit = row.Field<string>("unit") })
                .ToList()
                .ForEach(item => table.Columns.Add(item.ParamName.Trim().ToLower()));
            DataRow newRow = table.NewRow();
            newRow["postavshik_id"] = "PostavshikID";
            newRow["inStock"] = "Наличие на складе";
            newRow["productType"] = "productType";
            newRow["model"] = "model";
            ds.Tables[1].AsEnumerable()
                .Select(row => new ParamType { ParamName = row.Field<string>("name"), Unit = row.Field<string>("unit") })
                .ToList()
                .ForEach(item => newRow[item.ParamName.Trim().ToLower()] = item.NameUnit);
            table.Rows.Add(newRow);
            ds.Table.AsEnumerable()
                .Select(row => new
                {
                    OID = row.Field<Guid>("OID"),
                    PostavshikID = row.Field<int>("postavshik_id"),
                    InStock = row.Field<bool>("inStock") ? 1 : 0,
                    ProductType = row.Field<string>("productType"),
                    Model = row.Field<string>("model"),
                })
                .ToList()
                .ForEach(r =>
                {
                    newRow = table.NewRow();
                    newRow["postavshik_id"] = r.PostavshikID;
                    newRow["inStock"] = r.InStock;
                    newRow["productType"] = r.ProductType;
                    newRow["model"] = r.Model;
                    ds.Tables[2]
                        .AsEnumerable()
                        .Where(
                            row =>
                                row.Field<Guid>("OID") == r.OID &&
                                table.Columns.Contains(row.Field<string>("name").Trim().ToLower()))
                        .ToList()
                        .ForEach(
                            row =>
                                newRow[row.Field<string>("name").Trim().ToLower()] =
                                    row.Field<string>("value").Trim().Replace("\r", "").Replace("\n", " | "));
                    table.Rows.Add(newRow);
                });

            if (SaveExcel.ShowDialog(this) == DialogResult.OK)
            {
                FileInfo fi = new FileInfo(SaveExcel.FileName);
                var re = new Regex(@"\s+", RegexOptions.Singleline);
                using (ExcelPackage pkg = new ExcelPackage(fi))
                {
                    var sheet = pkg.Workbook.Worksheets["Sheet goods"];
                    if (sheet == null) sheet = pkg.Workbook.Worksheets.Add("Sheet goods");
                    int i = 1;
                    table.AsEnumerable().ToList().ForEach(row =>
                    {
                        for (var j = 1; j <= row.Table.Columns.Count; j++)
                        {
                            sheet.Cells[i, j].Value = re.Replace(row[j-1].ToString(), " ");
                        }
                        i++;
                    });
                    pkg.Save();
                }
                MessageBox.Show(@"Экспорт завершён");
            }
        }

        private void ImportButton_Click(object sender, EventArgs e)
        {
            if (SelectedTheme == Guid.Empty)
            {
                ShowError("Выберите категорию");
                return;
            }
            var fm = new ImportTemplates
            {
                MdiParent = MdiParent,
                ParamTypes = SourceData
                    .Table
                    .AsEnumerable()
                    .Where(row => row.Field<Guid?>("paramTypeOID").HasValue)
                    .Select(row => new ParamType
                    {
                        OID = row.Field<Guid>("paramTypeOID"),
                        ParamName = row.Field<string>("paramName"),
                        Unit = row.Field<string>("unit")
                    }).ToList(),
                ThemeOID = SelectedTheme
            };
            fm.ReloadGrid += LoadGrid;
            fm.Show();
        }
    }
}