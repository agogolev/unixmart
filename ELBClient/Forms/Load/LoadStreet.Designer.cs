using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class LoadStreet
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.OpenFileDialog ofdDBF;
		private ColumnMenuExtender.DataGridISM dgStreets;
		private System.Windows.Forms.Label lbCount;
		private System.Windows.Forms.CheckBox cbUpdateCities;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.lbCount = new System.Windows.Forms.Label();
			this.btnExport = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.dgStreets = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.ofdDBF = new System.Windows.Forms.OpenFileDialog();
			this.cbUpdateCities = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgStreets)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.lbCount);
			this.panel1.Controls.Add(this.cbUpdateCities);
			this.panel1.Controls.Add(this.btnExport);
			this.panel1.Controls.Add(this.btnLoad);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 389);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(616, 40);
			this.panel1.TabIndex = 0;
			// 
			// lbCount
			// 
			this.lbCount.Location = new System.Drawing.Point(328, 8);
			this.lbCount.Name = "lbCount";
			this.lbCount.Size = new System.Drawing.Size(200, 23);
			this.lbCount.TabIndex = 3;
			this.lbCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnExport
			// 
			this.btnExport.Location = new System.Drawing.Point(128, 8);
			this.btnExport.Name = "btnExport";
			this.btnExport.TabIndex = 2;
			this.btnExport.Text = "Импорт";
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(8, 8);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(112, 23);
			this.btnLoad.TabIndex = 1;
			this.btnLoad.Text = "Загрузить данные";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(536, 8);
			this.btnClose.Name = "btnClose";
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// dgStreets
			// 
			this.dgStreets.BackgroundColor = System.Drawing.Color.White;
			this.dgStreets.DataMember = "";
			this.dgStreets.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgStreets.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgStreets.Location = new System.Drawing.Point(0, 0);
			this.dgStreets.Name = "dgStreets";
			this.dgStreets.Order = null;
			this.dgStreets.Size = new System.Drawing.Size(616, 389);
			this.dgStreets.TabIndex = 1;
			this.dgStreets.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																													this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgStreets;
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// ofdDBF
			// 
			this.ofdDBF.Filter = "Файлы DBF|*.dbf|Все файлы|*.*";
			// 
			// cbUpdateCities
			// 
			this.cbUpdateCities.Location = new System.Drawing.Point(208, 8);
			this.cbUpdateCities.Name = "cbUpdateCities";
			this.cbUpdateCities.Size = new System.Drawing.Size(120, 24);
			this.cbUpdateCities.TabIndex = 4;
			this.cbUpdateCities.Text = "Обновить города";
			// 
			// fmLoadStreet
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 429);
			this.Controls.Add(this.dgStreets);
			this.Controls.Add(this.panel1);
			this.Name = "fmLoadStreet";
			this.Text = "Загрузка улиц";
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgStreets)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
