using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using ColumnMenuExtender;

namespace ELBClient.Forms.Load
{
	public partial class DeleteDescription
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.OpenFileDialog ofdlgExcel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private DataBoundComboBox cbSheet;
		private System.Windows.Forms.DataGrid dgVoucher;
		private System.Windows.Forms.NumericUpDown udNumber;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.ComboBox cbCodes;
		private System.Data.DataSet dsCodes;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.button2 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.udNumber = new System.Windows.Forms.NumericUpDown();
			this.cbSheet = new DataBoundComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cbCodes = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.ofdlgExcel = new System.Windows.Forms.OpenFileDialog();
			this.dgVoucher = new System.Windows.Forms.DataGrid();
			this.dsCodes = new System.Data.DataSet();
			this.dataTable1 = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.udNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgVoucher)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dsCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.udNumber);
			this.panel1.Controls.Add(this.cbSheet);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.cbCodes);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(992, 40);
			this.panel1.TabIndex = 0;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(904, 8);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(80, 23);
			this.button2.TabIndex = 26;
			this.button2.Text = "Обработка";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(600, 7);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 23);
			this.label4.TabIndex = 15;
			this.label4.Text = "Колонка номера:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// udNumber
			// 
			this.udNumber.Location = new System.Drawing.Point(704, 8);
			this.udNumber.Maximum = new System.Decimal(new int[] {
																														 10,
																														 0,
																														 0,
																														 0});
			this.udNumber.Name = "udNumber";
			this.udNumber.Size = new System.Drawing.Size(40, 20);
			this.udNumber.TabIndex = 14;
			// 
			// cbSheet
			// 
			this.cbSheet.Location = new System.Drawing.Point(200, 8);
			this.cbSheet.Name = "cbSheet";
			this.cbSheet.SelectedValue = -1;
			this.cbSheet.Size = new System.Drawing.Size(136, 21);
			this.cbSheet.TabIndex = 13;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(152, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(40, 23);
			this.label1.TabIndex = 12;
			this.label1.Text = "Лист:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbCodes
			// 
			this.cbCodes.DataSource = this.dataTable1;
			this.cbCodes.DisplayMember = "codeName";
			this.cbCodes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbCodes.Location = new System.Drawing.Point(448, 8);
			this.cbCodes.Name = "cbCodes";
			this.cbCodes.Size = new System.Drawing.Size(136, 21);
			this.cbCodes.TabIndex = 11;
			this.cbCodes.ValueMember = "codeID";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(352, 7);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 23);
			this.label3.TabIndex = 10;
			this.label3.Text = "Чей код:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(128, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Загрузить данные";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// ofdlgExcel
			// 
			this.ofdlgExcel.Filter = "Excel Files|*.xls|All Files|*.*";
			// 
			// dgVoucher
			// 
			this.dgVoucher.DataMember = "";
			this.dgVoucher.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgVoucher.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgVoucher.Location = new System.Drawing.Point(0, 40);
			this.dgVoucher.Name = "dgVoucher";
			this.dgVoucher.Size = new System.Drawing.Size(992, 348);
			this.dgVoucher.TabIndex = 1;
			// 
			// dsCodes
			// 
			this.dsCodes.DataSetName = "NewDataSet";
			this.dsCodes.Locale = new System.Globalization.CultureInfo("ru-RU");
			this.dsCodes.Tables.AddRange(new System.Data.DataTable[] {
																																 this.dataTable1});
			// 
			// dataTable1
			// 
			this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
																																			this.dataColumn1,
																																			this.dataColumn2});
			this.dataTable1.TableName = "Table";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "codeID";
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "codeName";
			// 
			// fmDeleteDescription
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(992, 388);
			this.Controls.Add(this.dgVoucher);
			this.Controls.Add(this.panel1);
			this.Name = "fmDeleteDescription";
			this.Text = "Загрузка ваучеров - ";
			this.Load += new System.EventHandler(this.fmLoadVoucher_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.udNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgVoucher)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dsCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
