﻿using System.IO;
using System.Windows.Forms;

namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmLoadFile2Server.
	/// </summary>
	public partial class LoadFile2Server : GenericForm
	{
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public LoadFile2Server()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void button1_Click(object sender, System.EventArgs e) {
			if(ofdArchive.ShowDialog(this) != DialogResult.OK) return;
			string fileName = Path.GetFileName(ofdArchive.FileName);
			byte[] content;
			using (FileStream fs = new FileStream(ofdArchive.FileName, FileMode.Open, FileAccess.Read, FileShare.Read)) {
				content = new byte[fs.Length];
				fs.Read(content, 0, content.Length);
			}
		
			FileProvider.ExceptionISM exISM = null;
			FileProvider.FileProvider fp = Classes.ServiceUtility.FileProvider;
			fp.UploadFile(fileName, FileProvider.FileMode.Create, content, out exISM);
			if(exISM != null) {
				ShowError(exISM.LiteralMessage);
			}
			else MessageBox.Show(this, @"Загрузка завершена");
		}
	}
}
