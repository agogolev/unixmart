﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Data;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Excel;
using MetaData;
//using Excel = Microsoft.Office.Interop.Excel;

namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmLoadParams.
	/// </summary>
	public partial class LoadParams : GenericForm
	{
		private Hashtable denyGoods = new Hashtable();
		private Hashtable acceptGoods = new Hashtable();
		private IDictionary<string, Guid> paramTypes;


		public LoadParams()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void LoadList_Click(object sender, System.EventArgs e)
		{
			if (odFiles.ShowDialog(this) == DialogResult.OK)
			{
				DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(odFiles.FileName));
				if (!di.Exists)
				{
					ShowError("Каталог не существует");
					return;
				}
				ClearObjects();
				FileInfo[] files = di.GetFiles("*.xls");
				foreach (FileInfo fi in files)
				{
					lbItems.Items.Add(Path.GetFileNameWithoutExtension(fi.Name));
				}
			}
		}
		private void ClearObjects()
		{
			lbItems.Items.Clear();
			acceptGoods.Clear();
			denyGoods.Clear();
			txtLog.Text = "";
		}

		private string ModifyName(string goodsName)
		{
			return goodsName.Trim().Replace("=", "/");
		}

		private void CheckForID()
		{
			Regex re = new Regex(@"^(\d*)$", RegexOptions.IgnoreCase);
			ListProvider.ListProvider prov = Classes.ServiceUtility.ListProvider;
			StringBuilder sb = new StringBuilder();
			foreach (string goods in lbItems.Items)
			{
				int ID = 0;
				Match m = re.Match(goods);
				if (m.Success)
				{
					ID = int.Parse(m.Groups[1].Value);
				}
				else
				{
					sb.Append("Нечисловой код в названии файла" + goods + "\r\n");
					denyGoods[goods] = 1;
					continue;
				}

				string sql = string.Format(@"
Declare 
	@OID uniqueidentifier

SELECT Count(*) 
FROM t_Goods g
WHERE g.postavshik_id = {0}

SELECT g.OID
FROM t_Goods g
WHERE g.postavshik_id = {0} 
", ID);
				DataSetISM ds = new DataSetISM(prov.GetDataSetSql(sql));
				int countGoods = (int)ds.Table.Rows[0][0];
				if (countGoods == 0)
				{
					sb.Append("Товар " + goods + " не найден в базе\r\n");
					denyGoods[goods] = 1;
				}
				else if (countGoods != 1)
				{
					sb.Append("В базе данных присутсвует несколько товаров с одинаковым кодом " + goods + "\r\n");
					denyGoods[goods] = 1;
				}
				else
				{
					acceptGoods[ID] = new GoodsDescription { OID = (Guid)ds.Tables[1].Rows[0][0], PostavshikID = ID };
				}
			}
			sb.Append("Количество товаров прошедших проверку: " + acceptGoods.Count + "\r\n");
			sb.Append("Количество товаров не прошедших проверку: " + denyGoods.Count + "\r\n");
			txtLog.Text += sb.ToString();
		}

		private void LoadParams_Click(object sender, System.EventArgs e)
		{
			txtLog.Text = "";
			denyGoods.Clear();
			acceptGoods.Clear();
			CheckForID();
			ProcessList();
		}
		private void ProcessList()
		{
			string sql = @"
SELECT
	OID,
	name
FROM
	t_ParamType";
			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			paramTypes = (from row in ds.Table.AsEnumerable()
										select new
										{
											Name = row.Field<string>("name").ToLower().Trim(),
											OID = row.Field<Guid>("OID")
										}).ToDictionary(k => k.Name, k => k.OID);

			string sqlParams = @"
Declare 
	@ordValue int

SELECT @ordValue = MAX(ordValue) FROM t_GoodsParams WHERE OID = {0}

if @ordValue is null Set @ordValue = 0
	
SET @ordValue = @ordValue + 1

if NOT EXISTS (SELECT * FROM t_GoodsParams WHERE OID = {0} And paramTypeOID = {1}) 
	INSERT INTO t_GoodsParams (OID, paramTypeOID, ordValue, value, unit) Values ({0}, {1}, @ordValue, {2}, {3})
else UPDATE t_GoodsParams SET value = {2}, unit = {3} WHERE OID = {0} And paramTypeOID = {1}
";
			string sqlCreateParam = @"
Declare
	@OID uniqueidentifier

	SELECT @OID = OID FROM t_ParamType WHERE name = {0}
	if @OID is null Begin
		exec spInnerCreateObject 'CParamType', @OID OUTPUT
		UPDATE t_ParamType SET
			name = {0}
		WHERE
			OID = @OID
	End
	SELECT @OID
";
			string sqlDeleteEmptyParams = @"
DELETE FROM t_GoodsParams
WHERE OID = {0} and value = ''
";
			try
			{
				//System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
				int count = 0;
				foreach (DictionaryEntry de in acceptGoods)
				{
					using (FileStream stream = File.Open(Path.GetDirectoryName(odFiles.FileName) + "\\" + de.Key + ".xls", FileMode.Open, FileAccess.Read))
					{
						using (IExcelDataReader read = ExcelReaderFactory.CreateBinaryReader(stream))
						{
							GoodsDescription goods = (GoodsDescription)de.Value;
							op.ExecuteCommand(string.Format("DELETE FROM t_GoodsParams WHERE OID = {0}", ToSqlString(goods.OID)));
							while (read.Read()) {
								string paramName = read.GetString(0);
								string paramValue = (read.GetString(1) ?? "").Trim();
								string unit = read.GetString(2);
								if (string.IsNullOrEmpty(paramName)) continue;
								//if (string.IsNullOrEmpty(paramValue)) continue;
								//if (string.Compare(paramName.Trim(), "тип", true) == 0)
								//{
								//  UpdateProductType(goods.OID, paramValue.Trim());
								//}
								if (!paramTypes.ContainsKey(paramName.ToLower().Trim()))
								{
									ds = new DataSetISM(lp.GetDataSetSql(string.Format(sqlCreateParam, ToSqlString(paramName))));
									paramTypes[paramName.ToLower().Trim()] = (Guid)ds.Table.Rows[0][0];
								}
								Guid paramTypeOID = paramTypes[paramName.ToLower().Trim()];

								try
								{
									op.ExecuteCommand(string.Format(sqlParams,
										ToSqlString(goods.OID),
										ToSqlString(paramTypeOID),
										ToSqlString(paramValue.Trim()),
										ToSqlString(unit ?? "")));
								}
								catch (Exception ex)
								{
									txtLog.Text += @"Ошибка сохранения параметра в товаре " + goods.PostavshikID + ", параметр " + paramName + " значение " + paramValue + " Ошибка: " + ex.Message + "\r\n";
								}
							}
							op.ExecuteCommand(string.Format(sqlDeleteEmptyParams,
								ToSqlString(goods.OID)));
						}
					}
					
					count++;
					lbCounter.Text = "Счётчик: " + count;
					lbCounter.Refresh();
				}
				op.ClearCache();
				MessageBox.Show(this, @"Загрузка описаний товаров закончена");
			}
			catch (Exception ex)
			{
				op.ClearCache();
				ShowError("Загрузка описаний товаров прервана - " + ex.Message);
			}
			finally
			{
				//if (xWkb != null)
				//{
				//  xWkb.Close(true, m_Opt, m_Opt);
				//  xWSh = null;
				//  xWkb = null;
				//  xApp = null;

				//}
				//System.Threading.Thread.CurrentThread.CurrentCulture = info;
			}
		}

		private void UpdateProductType(Guid OID, string productType)
		{
			string sql = @"
UPDATE t_Goods SET 
	productType = {1}
WHERE OID = {0}
";
			try
			{
				op.ExecuteCommand(string.Format(sql, ToSqlString(OID), ToSqlString(productType)));
			}
			catch (Exception ex)
			{
				txtLog.Text += "Ошибка сохранения типа товара " + productType + ", Ошибка: " + ex.Message + "\r\n";
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			Close();
		}

	}
	public class GoodsDescription
	{
		public Guid OID { get; set; }
		public int PostavshikID { get; set; }
		//public string Name { get; set; }
		//public Dictionary<Guid, DataRowView> Params { get; set; }
		//public override string ToString()
		//{
		//  return Name;
		//}

	}
}
