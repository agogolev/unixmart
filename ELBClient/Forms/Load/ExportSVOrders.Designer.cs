﻿namespace ELBClient.Forms.Load
{
	partial class ExportSVOrders
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.ExportToFile = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.LoadDataButton = new System.Windows.Forms.Button();
			this.dgOrders = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn5 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn6 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn7 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn8 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn9 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn10 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn11 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn12 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn13 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn14 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn15 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn16 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn17 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn18 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn19 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn20 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn21 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn22 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn23 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn24 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn25 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn26 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn27 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn28 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn29 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn30 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn31 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn32 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn33 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn34 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.SaveList = new System.Windows.Forms.SaveFileDialog();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgOrders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.ExportToFile);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.dtpDateEnd);
			this.panel1.Controls.Add(this.dtpDateBegin);
			this.panel1.Controls.Add(this.LoadDataButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(892, 41);
			this.panel1.TabIndex = 0;
			// 
			// ExportToFile
			// 
			this.ExportToFile.Location = new System.Drawing.Point(526, 6);
			this.ExportToFile.Name = "ExportToFile";
			this.ExportToFile.Size = new System.Drawing.Size(105, 28);
			this.ExportToFile.TabIndex = 10;
			this.ExportToFile.Text = "Экспорт в файл";
			this.ExportToFile.UseVisualStyleBackColor = true;
			this.ExportToFile.Click += new System.EventHandler(this.ExportToFile_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(208, 14);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 13);
			this.label2.TabIndex = 9;
			this.label2.Text = "по:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(4, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 13);
			this.label1.TabIndex = 8;
			this.label1.Text = "Создан с:";
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Location = new System.Drawing.Point(234, 10);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.Size = new System.Drawing.Size(136, 20);
			this.dtpDateEnd.TabIndex = 7;
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Location = new System.Drawing.Point(66, 10);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(136, 20);
			this.dtpDateBegin.TabIndex = 6;
			// 
			// LoadDataButton
			// 
			this.LoadDataButton.Location = new System.Drawing.Point(378, 6);
			this.LoadDataButton.Name = "LoadDataButton";
			this.LoadDataButton.Size = new System.Drawing.Size(114, 28);
			this.LoadDataButton.TabIndex = 0;
			this.LoadDataButton.Text = "Просмотр";
			this.LoadDataButton.UseVisualStyleBackColor = true;
			this.LoadDataButton.Click += new System.EventHandler(this.LoadFile_Click);
			// 
			// dgOrders
			// 
			this.dgOrders.AllowSorting = false;
			this.dgOrders.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgOrders.ColumnDragEnabled = true;
			this.dgOrders.DataMember = "";
			this.dgOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgOrders.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgOrders.Location = new System.Drawing.Point(0, 41);
			this.dgOrders.Name = "dgOrders";
			this.dgOrders.Order = null;
			this.dgOrders.ReadOnly = true;
			this.dgOrders.Size = new System.Drawing.Size(892, 416);
			this.dgOrders.StockClass = "CGoods";
			this.dgOrders.StockNumInBatch = 0;
			this.dgOrders.TabIndex = 16;
			this.dgOrders.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgOrders;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn5,
            this.formattableTextBoxColumn6,
            this.formattableTextBoxColumn7,
            this.formattableTextBoxColumn8,
            this.formattableTextBoxColumn9,
            this.formattableTextBoxColumn10,
            this.formattableTextBoxColumn11,
            this.formattableTextBoxColumn12,
            this.formattableTextBoxColumn13,
            this.formattableTextBoxColumn14,
            this.formattableTextBoxColumn15,
            this.formattableTextBoxColumn16,
            this.formattableTextBoxColumn17,
            this.formattableTextBoxColumn18,
            this.formattableTextBoxColumn19,
            this.formattableTextBoxColumn20,
            this.formattableTextBoxColumn21,
            this.formattableTextBoxColumn22,
            this.formattableTextBoxColumn23,
            this.formattableTextBoxColumn24,
            this.formattableTextBoxColumn25,
            this.formattableTextBoxColumn26,
            this.formattableTextBoxColumn27,
            this.formattableTextBoxColumn28,
            this.formattableTextBoxColumn29,
            this.formattableTextBoxColumn30,
            this.formattableTextBoxColumn31,
            this.formattableTextBoxColumn32,
            this.formattableTextBoxColumn33,
            this.formattableTextBoxColumn34});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "id";
			this.formattableTextBoxColumn1.MappingName = "id";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "svId";
			this.formattableTextBoxColumn2.MappingName = "svId";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "orderNum";
			this.formattableTextBoxColumn3.MappingName = "orderNum";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// formattableTextBoxColumn4
			// 
			this.formattableTextBoxColumn4.FieldName = null;
			this.formattableTextBoxColumn4.FilterFieldName = null;
			this.formattableTextBoxColumn4.Format = "";
			this.formattableTextBoxColumn4.FormatInfo = null;
			this.formattableTextBoxColumn4.HeaderText = "orderDate";
			this.formattableTextBoxColumn4.MappingName = "orderDate";
			this.formattableTextBoxColumn4.Width = 75;
			// 
			// formattableTextBoxColumn5
			// 
			this.formattableTextBoxColumn5.FieldName = null;
			this.formattableTextBoxColumn5.FilterFieldName = null;
			this.formattableTextBoxColumn5.Format = "";
			this.formattableTextBoxColumn5.FormatInfo = null;
			this.formattableTextBoxColumn5.HeaderText = "deliveryDate";
			this.formattableTextBoxColumn5.MappingName = "deliveryDate";
			this.formattableTextBoxColumn5.Width = 75;
			// 
			// formattableTextBoxColumn6
			// 
			this.formattableTextBoxColumn6.FieldName = null;
			this.formattableTextBoxColumn6.FilterFieldName = null;
			this.formattableTextBoxColumn6.Format = "";
			this.formattableTextBoxColumn6.FormatInfo = null;
			this.formattableTextBoxColumn6.HeaderText = "type_Doc";
			this.formattableTextBoxColumn6.MappingName = "type_Doc";
			this.formattableTextBoxColumn6.Width = 75;
			// 
			// formattableTextBoxColumn7
			// 
			this.formattableTextBoxColumn7.FieldName = null;
			this.formattableTextBoxColumn7.FilterFieldName = null;
			this.formattableTextBoxColumn7.Format = "";
			this.formattableTextBoxColumn7.FormatInfo = null;
			this.formattableTextBoxColumn7.HeaderText = "typePay";
			this.formattableTextBoxColumn7.MappingName = "typePay";
			this.formattableTextBoxColumn7.Width = 75;
			// 
			// formattableTextBoxColumn8
			// 
			this.formattableTextBoxColumn8.FieldName = null;
			this.formattableTextBoxColumn8.FilterFieldName = null;
			this.formattableTextBoxColumn8.Format = "";
			this.formattableTextBoxColumn8.FormatInfo = null;
			this.formattableTextBoxColumn8.HeaderText = "predopl";
			this.formattableTextBoxColumn8.MappingName = "predopl";
			this.formattableTextBoxColumn8.Width = 75;
			// 
			// formattableTextBoxColumn9
			// 
			this.formattableTextBoxColumn9.FieldName = null;
			this.formattableTextBoxColumn9.FilterFieldName = null;
			this.formattableTextBoxColumn9.Format = "";
			this.formattableTextBoxColumn9.FormatInfo = null;
			this.formattableTextBoxColumn9.HeaderText = "deliveryPrice";
			this.formattableTextBoxColumn9.MappingName = "deliveryPrice";
			this.formattableTextBoxColumn9.Width = 75;
			// 
			// formattableTextBoxColumn10
			// 
			this.formattableTextBoxColumn10.FieldName = null;
			this.formattableTextBoxColumn10.FilterFieldName = null;
			this.formattableTextBoxColumn10.Format = "";
			this.formattableTextBoxColumn10.FormatInfo = null;
			this.formattableTextBoxColumn10.HeaderText = "managerId";
			this.formattableTextBoxColumn10.MappingName = "managerId";
			this.formattableTextBoxColumn10.Width = 75;
			// 
			// formattableTextBoxColumn11
			// 
			this.formattableTextBoxColumn11.FieldName = null;
			this.formattableTextBoxColumn11.FilterFieldName = null;
			this.formattableTextBoxColumn11.Format = "";
			this.formattableTextBoxColumn11.FormatInfo = null;
			this.formattableTextBoxColumn11.HeaderText = "deliveryFrom";
			this.formattableTextBoxColumn11.MappingName = "deliveryFrom";
			this.formattableTextBoxColumn11.Width = 75;
			// 
			// formattableTextBoxColumn12
			// 
			this.formattableTextBoxColumn12.FieldName = null;
			this.formattableTextBoxColumn12.FilterFieldName = null;
			this.formattableTextBoxColumn12.Format = "";
			this.formattableTextBoxColumn12.FormatInfo = null;
			this.formattableTextBoxColumn12.HeaderText = "deliveryTo";
			this.formattableTextBoxColumn12.MappingName = "deliveryTo";
			this.formattableTextBoxColumn12.Width = 75;
			// 
			// formattableTextBoxColumn13
			// 
			this.formattableTextBoxColumn13.FieldName = null;
			this.formattableTextBoxColumn13.FilterFieldName = null;
			this.formattableTextBoxColumn13.Format = "";
			this.formattableTextBoxColumn13.FormatInfo = null;
			this.formattableTextBoxColumn13.HeaderText = "familia";
			this.formattableTextBoxColumn13.MappingName = "familia";
			this.formattableTextBoxColumn13.Width = 75;
			// 
			// formattableTextBoxColumn14
			// 
			this.formattableTextBoxColumn14.FieldName = null;
			this.formattableTextBoxColumn14.FilterFieldName = null;
			this.formattableTextBoxColumn14.Format = "";
			this.formattableTextBoxColumn14.FormatInfo = null;
			this.formattableTextBoxColumn14.HeaderText = "imya";
			this.formattableTextBoxColumn14.MappingName = "imya";
			this.formattableTextBoxColumn14.Width = 75;
			// 
			// formattableTextBoxColumn15
			// 
			this.formattableTextBoxColumn15.FieldName = null;
			this.formattableTextBoxColumn15.FilterFieldName = null;
			this.formattableTextBoxColumn15.Format = "";
			this.formattableTextBoxColumn15.FormatInfo = null;
			this.formattableTextBoxColumn15.HeaderText = "otchestvo";
			this.formattableTextBoxColumn15.MappingName = "otchestvo";
			this.formattableTextBoxColumn15.Width = 75;
			// 
			// formattableTextBoxColumn16
			// 
			this.formattableTextBoxColumn16.FieldName = null;
			this.formattableTextBoxColumn16.FilterFieldName = null;
			this.formattableTextBoxColumn16.Format = "";
			this.formattableTextBoxColumn16.FormatInfo = null;
			this.formattableTextBoxColumn16.HeaderText = "email";
			this.formattableTextBoxColumn16.MappingName = "email";
			this.formattableTextBoxColumn16.Width = 75;
			// 
			// formattableTextBoxColumn17
			// 
			this.formattableTextBoxColumn17.FieldName = null;
			this.formattableTextBoxColumn17.FilterFieldName = null;
			this.formattableTextBoxColumn17.Format = "";
			this.formattableTextBoxColumn17.FormatInfo = null;
			this.formattableTextBoxColumn17.HeaderText = "homePhone";
			this.formattableTextBoxColumn17.MappingName = "homePhone";
			this.formattableTextBoxColumn17.Width = 75;
			// 
			// formattableTextBoxColumn18
			// 
			this.formattableTextBoxColumn18.FieldName = null;
			this.formattableTextBoxColumn18.FilterFieldName = null;
			this.formattableTextBoxColumn18.Format = "";
			this.formattableTextBoxColumn18.FormatInfo = null;
			this.formattableTextBoxColumn18.HeaderText = "cellPhone";
			this.formattableTextBoxColumn18.MappingName = "cellPhone";
			this.formattableTextBoxColumn18.Width = 75;
			// 
			// formattableTextBoxColumn19
			// 
			this.formattableTextBoxColumn19.FieldName = null;
			this.formattableTextBoxColumn19.FilterFieldName = null;
			this.formattableTextBoxColumn19.Format = "";
			this.formattableTextBoxColumn19.FormatInfo = null;
			this.formattableTextBoxColumn19.HeaderText = "dopPhone";
			this.formattableTextBoxColumn19.MappingName = "dopPhone";
			this.formattableTextBoxColumn19.Width = 75;
			// 
			// formattableTextBoxColumn20
			// 
			this.formattableTextBoxColumn20.FieldName = null;
			this.formattableTextBoxColumn20.FilterFieldName = null;
			this.formattableTextBoxColumn20.Format = "";
			this.formattableTextBoxColumn20.FormatInfo = null;
			this.formattableTextBoxColumn20.HeaderText = "city";
			this.formattableTextBoxColumn20.MappingName = "city";
			this.formattableTextBoxColumn20.Width = 75;
			// 
			// formattableTextBoxColumn21
			// 
			this.formattableTextBoxColumn21.FieldName = null;
			this.formattableTextBoxColumn21.FilterFieldName = null;
			this.formattableTextBoxColumn21.Format = "";
			this.formattableTextBoxColumn21.FormatInfo = null;
			this.formattableTextBoxColumn21.HeaderText = "metro";
			this.formattableTextBoxColumn21.MappingName = "metro";
			this.formattableTextBoxColumn21.Width = 75;
			// 
			// formattableTextBoxColumn22
			// 
			this.formattableTextBoxColumn22.FieldName = null;
			this.formattableTextBoxColumn22.FilterFieldName = null;
			this.formattableTextBoxColumn22.Format = "";
			this.formattableTextBoxColumn22.FormatInfo = null;
			this.formattableTextBoxColumn22.HeaderText = "street";
			this.formattableTextBoxColumn22.MappingName = "street";
			this.formattableTextBoxColumn22.Width = 75;
			// 
			// formattableTextBoxColumn23
			// 
			this.formattableTextBoxColumn23.FieldName = null;
			this.formattableTextBoxColumn23.FilterFieldName = null;
			this.formattableTextBoxColumn23.Format = "";
			this.formattableTextBoxColumn23.FormatInfo = null;
			this.formattableTextBoxColumn23.HeaderText = "dom";
			this.formattableTextBoxColumn23.MappingName = "dom";
			this.formattableTextBoxColumn23.Width = 75;
			// 
			// formattableTextBoxColumn24
			// 
			this.formattableTextBoxColumn24.FieldName = null;
			this.formattableTextBoxColumn24.FilterFieldName = null;
			this.formattableTextBoxColumn24.Format = "";
			this.formattableTextBoxColumn24.FormatInfo = null;
			this.formattableTextBoxColumn24.HeaderText = "korpus";
			this.formattableTextBoxColumn24.MappingName = "korpus";
			this.formattableTextBoxColumn24.Width = 75;
			// 
			// formattableTextBoxColumn25
			// 
			this.formattableTextBoxColumn25.FieldName = null;
			this.formattableTextBoxColumn25.FilterFieldName = null;
			this.formattableTextBoxColumn25.Format = "";
			this.formattableTextBoxColumn25.FormatInfo = null;
			this.formattableTextBoxColumn25.HeaderText = "stroenie";
			this.formattableTextBoxColumn25.MappingName = "stroenie";
			this.formattableTextBoxColumn25.Width = 75;
			// 
			// formattableTextBoxColumn26
			// 
			this.formattableTextBoxColumn26.FieldName = null;
			this.formattableTextBoxColumn26.FilterFieldName = null;
			this.formattableTextBoxColumn26.Format = "";
			this.formattableTextBoxColumn26.FormatInfo = null;
			this.formattableTextBoxColumn26.HeaderText = "vladenie";
			this.formattableTextBoxColumn26.MappingName = "vladenie";
			this.formattableTextBoxColumn26.Width = 75;
			// 
			// formattableTextBoxColumn27
			// 
			this.formattableTextBoxColumn27.FieldName = null;
			this.formattableTextBoxColumn27.FilterFieldName = null;
			this.formattableTextBoxColumn27.Format = "";
			this.formattableTextBoxColumn27.FormatInfo = null;
			this.formattableTextBoxColumn27.HeaderText = "kvartira";
			this.formattableTextBoxColumn27.MappingName = "kvartira";
			this.formattableTextBoxColumn27.Width = 75;
			// 
			// formattableTextBoxColumn28
			// 
			this.formattableTextBoxColumn28.FieldName = null;
			this.formattableTextBoxColumn28.FilterFieldName = null;
			this.formattableTextBoxColumn28.Format = "";
			this.formattableTextBoxColumn28.FormatInfo = null;
			this.formattableTextBoxColumn28.HeaderText = "podezd";
			this.formattableTextBoxColumn28.MappingName = "podezd";
			this.formattableTextBoxColumn28.Width = 75;
			// 
			// formattableTextBoxColumn29
			// 
			this.formattableTextBoxColumn29.FieldName = null;
			this.formattableTextBoxColumn29.FilterFieldName = null;
			this.formattableTextBoxColumn29.Format = "";
			this.formattableTextBoxColumn29.FormatInfo = null;
			this.formattableTextBoxColumn29.HeaderText = "etaz";
			this.formattableTextBoxColumn29.MappingName = "etaz";
			this.formattableTextBoxColumn29.Width = 75;
			// 
			// formattableTextBoxColumn30
			// 
			this.formattableTextBoxColumn30.FieldName = null;
			this.formattableTextBoxColumn30.FilterFieldName = null;
			this.formattableTextBoxColumn30.Format = "";
			this.formattableTextBoxColumn30.FormatInfo = null;
			this.formattableTextBoxColumn30.HeaderText = "domofon";
			this.formattableTextBoxColumn30.MappingName = "domofon";
			this.formattableTextBoxColumn30.Width = 75;
			// 
			// formattableTextBoxColumn31
			// 
			this.formattableTextBoxColumn31.FieldName = null;
			this.formattableTextBoxColumn31.FilterFieldName = null;
			this.formattableTextBoxColumn31.Format = "";
			this.formattableTextBoxColumn31.FormatInfo = null;
			this.formattableTextBoxColumn31.HeaderText = "lift";
			this.formattableTextBoxColumn31.MappingName = "lift";
			this.formattableTextBoxColumn31.Width = 75;
			// 
			// formattableTextBoxColumn32
			// 
			this.formattableTextBoxColumn32.FieldName = null;
			this.formattableTextBoxColumn32.FilterFieldName = null;
			this.formattableTextBoxColumn32.Format = "";
			this.formattableTextBoxColumn32.FormatInfo = null;
			this.formattableTextBoxColumn32.HeaderText = "status";
			this.formattableTextBoxColumn32.MappingName = "status";
			this.formattableTextBoxColumn32.Width = 75;
			// 
			// formattableTextBoxColumn33
			// 
			this.formattableTextBoxColumn33.FieldName = null;
			this.formattableTextBoxColumn33.FilterFieldName = null;
			this.formattableTextBoxColumn33.Format = "";
			this.formattableTextBoxColumn33.FormatInfo = null;
			this.formattableTextBoxColumn33.HeaderText = "direction";
			this.formattableTextBoxColumn33.MappingName = "direction";
			this.formattableTextBoxColumn33.Width = 75;
			// 
			// formattableTextBoxColumn34
			// 
			this.formattableTextBoxColumn34.FieldName = null;
			this.formattableTextBoxColumn34.FilterFieldName = null;
			this.formattableTextBoxColumn34.Format = "";
			this.formattableTextBoxColumn34.FormatInfo = null;
			this.formattableTextBoxColumn34.HeaderText = "comments";
			this.formattableTextBoxColumn34.MappingName = "comments";
			this.formattableTextBoxColumn34.Width = 75;
			// 
			// SaveList
			// 
			this.SaveList.DefaultExt = "xls";
			this.SaveList.Filter = "Файлы Excel|*.xls|Все файлы|*.*";
			this.SaveList.Title = "Сохранить список";
			// 
			// ExportSVOrders
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(892, 457);
			this.Controls.Add(this.dgOrders);
			this.Controls.Add(this.panel1);
			this.Name = "ExportSVOrders";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "LoadGoods";
			this.Load += new System.EventHandler(this.LoadGoods_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgOrders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridISM dgOrders;
		private System.Windows.Forms.Button LoadDataButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.SaveFileDialog SaveList;
		private System.Windows.Forms.Button ExportToFile;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn5;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn6;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn7;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn8;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn9;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn10;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn11;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn12;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn13;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn14;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn15;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn16;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn17;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn18;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn19;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn20;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn21;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn22;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn23;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn24;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn25;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn26;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn27;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn28;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn29;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn30;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn31;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn32;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn33;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn34;
	}
}