﻿using System;
using System.Data;
using MetaData;

namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmConvertAddress.
	/// </summary>
	public partial class ConvertAddress : ListForm
	{
		private DataSetISM ds;
		
		
		
		
		
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public ConvertAddress()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void fmConvertAddress_Load(object sender, System.EventArgs e) {
			LoadData();
		}
		public override void LoadData() {
			string sql = @"
SELECT 
	p.OID,
	lastName,
	email
FROM
	t_people p
	inner join t_Object o on p.OID = o.OID
order by o.dateCreate desc

SELECT
	OID,
	ordValue,
	contact
FROM
	t_peopleContacts
";
			ListProvider.ListProvider lp = Classes.ServiceUtility.ListProvider;
			ds = new DataSetISM(lp.GetDataSetSql(sql));
			DataRelation dr = new DataRelation("contacts", ds.Table.Columns["OID"],
				ds.Tables[1].Columns["OID"]);
			ds.Relations.Add(dr);
			dgPeoples.SetDataBinding(ds, "table");
			dgContacts.SetDataBinding(ds, "table.contacts");
		}

		private void button1_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void button2_Click(object sender, System.EventArgs e) {
			int processed = 0;
			ObjectProvider.ObjectProvider op = Classes.ServiceUtility.ObjectProvider;
			foreach(DataRow dr in ds.Table.Rows) {
				Guid peopleOID = (Guid)dr["OID"];
				DataView dvCount = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'кол-во адресов'", null, DataViewRowState.CurrentRows);
				if(dvCount.Count == 0) continue;
				string sql = "DELETE FROM t_PeopleAddresses WHERE OID = '"+peopleOID+"'";
				op.ExecuteCommand(sql);
				int countAddr = int.Parse(dvCount[0]["contact"].ToString());
				DataView dvIsMain = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'выбр. адрес'", null, DataViewRowState.CurrentRows);
				int mainAddress = 1;
				if(dvIsMain.Count != 0) mainAddress = int.Parse(dvIsMain[0]["contact"].ToString());
				for(int i = 1; i <= countAddr; i++) {
					int isMain = (i == mainAddress ? 1 : 0);
					DataView dvTown = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'город_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvStreet = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'улица_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvHouse = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'дом_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvPodezd = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'подъезд_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvFlat = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'квартира_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvEtag = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'этаж_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvCorpus = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'корпус_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvCode = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'код_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvComment = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'коментарии_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvPhone = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'телефон_"+i+"'", null, DataViewRowState.CurrentRows);
					DataView dvPoluch = new DataView(ds.Tables[1], "OID = '"+peopleOID+"' And ordValue = 'получатель_"+i+"'", null, DataViewRowState.CurrentRows);
					string town = (dvTown.Count == 0 ? "" : dvTown[0]["contact"].ToString());
					string street = (dvStreet.Count == 0 ? "" : dvStreet[0]["contact"].ToString());
					string house = (dvHouse.Count == 0 ? "" : dvHouse[0]["contact"].ToString());
					string podezd = (dvPodezd.Count == 0 ? "" : dvPodezd[0]["contact"].ToString());
					string flat = (dvFlat.Count == 0 ? "" : dvFlat[0]["contact"].ToString());
					string etag = (dvEtag.Count == 0 ? "" : dvEtag[0]["contact"].ToString());
					string corpus = (dvCorpus.Count == 0 ? "" : dvCorpus[0]["contact"].ToString());
					string code = (dvCode.Count == 0 ? "" : dvCode[0]["contact"].ToString());
					string comment = (dvComment.Count == 0 ? "" : dvComment[0]["contact"].ToString());
					string phone = (dvPhone.Count == 0 ? "" : dvPhone[0]["contact"].ToString());
					string poluch = (dvPoluch.Count == 0 ? "" : dvPoluch[0]["contact"].ToString());
					sql = @"INSERT INTO t_PeopleAddresses (OID, ordValue, isMain, town, street, house, podezd,
flat, etag, corpus, code, comment, phone, poluch)
VALUES ("+ToSqlString(peopleOID.ToString())+", "+i+", "+isMain+", "+ToSqlString(town)+", "+ToSqlString(street)+", "
						+ToSqlString(house)+", "+ToSqlString(podezd)+", "+ToSqlString(flat)+", "+ToSqlString(etag)+", "
						+ToSqlString(corpus)+", "+ToSqlString(code)+", "+ToSqlString(comment)+", "+ToSqlString(phone)+", "
						+ToSqlString(poluch)+")";
					op.ExecuteCommand(sql);
				}
				processed++;
				lbCount.Text = "Счётчик: "+processed;
				lbCount.Refresh();
			}
		}
	}
}
