﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using Ecommerce.Extensions;
using MetaData;
using ELBClient.ObjectProvider;
using System.Globalization;
using Excel;

namespace ELBClient.Forms.Load
{
    public partial class LoadGoodsFromExcel : ListForm
    {
        public int StartFrom { get; set; }
        public int ShopType { get; set; }
        public string ShopName { get; set; }
        private DataSet _ds;
        private int _processedItems = 0;
        public LoadGoodsFromExcel()
        {
            InitializeComponent();
            StartFrom = 1;
        }

        private void LoadFile_Click(object sender, EventArgs e)
        {
            if (LoadExcel.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    using (FileStream stream = File.Open(LoadExcel.FileName, FileMode.Open, FileAccess.Read))
                    {
                        if (Path.GetExtension(LoadExcel.FileName).ToLower() == ".xls")
                        {
                            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
                            {
                                excelReader.Read();
                                _ds = new DataSetISM();
                                DataTable dt = new DataTable("Table");
                                _ds.Tables.Add(dt);
                                int i = 0;
                                string name = null;
                                do
                                {
                                    name = excelReader.GetString(i);
                                    dt.Columns.Add(name);
                                    i++;
                                }
                                while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
                                while (excelReader.Read())
                                {
                                    DataRow dr = dt.NewRow();
                                    for (int j = 0; j < dt.Columns.Count; j++)
                                    {
                                        dr[j] = excelReader.GetString(j);
                                    }
                                    dt.Rows.Add(dr);
                                }
                                _ds.AcceptChanges();
                            }

                        }
                        else if (Path.GetExtension(LoadExcel.FileName).ToLower() == ".xlsx")
                        {
                            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                _ds = excelReader.AsDataSet();
                                excelReader.Read();
                            }
                        }

                    }

                    dgGoods.DataSource = _ds;
                    dgGoods.DataMember = "Table";
                    _processedItems = 0;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ImportInStock_Click(object sender, EventArgs e)
		{
			if (_ds == null)
			{
				ShowError("Загрузите файл");
				return;
			}
			
            op.ExecuteCommand("UPDATE t_GoodsInStocks SET inLoad = 0");

            const string sql = @"

if EXISTS (SELECT * FROM t_GoodsInStocks gp inner join t_Goods g on gp.OID = g.OID and g.postavshik_id = @id)
	UPDATE t_GoodsInStocks SET inStock = @inStock, inLoad = 1 
	FROM t_GoodsInStocks gp 
		inner join t_Goods g on gp.OID = g.OID and g.postavshik_id = @id
else
	INSERT INTO t_GoodsInStocks (OID, shopType, inStock, inLoad) 
	SELECT g.OID, s.shopType, @inStock, 1 FROM t_Goods g, t_TypeShop s WHERE postavshik_id = @id and s.active = 1

";
			_processedItems = StartFrom - 1;
			for (int i = StartFrom - 1; i < _ds.Tables[0].Rows.Count; i++)
			{
				DataRow dr = _ds.Tables[0].Rows[i];
                int id; 
                if(!int.TryParse(dr[0].ToString().Trim(), out id)) {
                    continue;
                }
				var inStock = false;
			    if (dr[2] != null && dr[2] != DBNull.Value && !string.IsNullOrWhiteSpace((string) dr[2]))
			    {
			        decimal amount;
                    if (decimal.TryParse((string)dr[2], out amount) && amount != 0) inStock = true;
			    }
                    

				op.ExecuteCommandParams(sql, new NameValuePair[] {
					new NameValuePair { Name = "id", Value = id.ToString(CultureInfo.InvariantCulture)},
					new NameValuePair { Name = "inStock", Value = inStock.ToSqlString() }
				}, false);
				Counter.Text = @"Обработано: " + ++_processedItems;
				Counter.Update();
			}

            op.ExecuteCommand(@"
UPDATE t_GoodsInStocks SET inStock = 0 WHERE inLoad = 0

UPDATE t_GoodsInStocks SET inLoad = 0
");
			MessageBox.Show(this, @"Импорт каталога завершён");
		}

        private void LoadGoods_Load(object sender, EventArgs e)
        {
            Text = string.Format("Загрузка остатков - {0}", ShopName);
            StartFromText.DataBindings.Add("Text", this, "StartFrom");
        }

    }
}
