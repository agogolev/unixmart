using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class SetIdentity
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.OpenFileDialog odFiles;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label lbCounter;
		private ColumnMenuExtender.DataGridISM dgItems;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cbSource;
		private System.Windows.Forms.ComboBox cbDest;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel2 = new System.Windows.Forms.Panel();
			this.cbSource = new System.Windows.Forms.ComboBox();
			this.dataTable1 = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.cbDest = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btnClose = new System.Windows.Forms.Button();
			this.lbCounter = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.odFiles = new System.Windows.Forms.OpenFileDialog();
			this.dgItems = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.dataSet1 = new System.Data.DataSet();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.cbSource);
			this.panel2.Controls.Add(this.cbDest);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.btnClose);
			this.panel2.Controls.Add(this.lbCounter);
			this.panel2.Controls.Add(this.button3);
			this.panel2.Controls.Add(this.button1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(648, 80);
			this.panel2.TabIndex = 1;
			// 
			// cbSource
			// 
			this.cbSource.DataSource = this.dataTable1;
			this.cbSource.DisplayMember = "CodeName";
			this.cbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSource.Location = new System.Drawing.Point(128, 40);
			this.cbSource.Name = "cbSource";
			this.cbSource.Size = new System.Drawing.Size(121, 21);
			this.cbSource.TabIndex = 9;
			this.cbSource.ValueMember = "FieldName";
			// 
			// dataTable1
			// 
			this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
																																			this.dataColumn1,
																																			this.dataColumn2});
			this.dataTable1.TableName = "Table1";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "FieldName";
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "CodeName";
			// 
			// cbDest
			// 
			this.cbDest.DataSource = this.dataTable1;
			this.cbDest.DisplayMember = "CodeName";
			this.cbDest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDest.Location = new System.Drawing.Point(368, 40);
			this.cbDest.Name = "cbDest";
			this.cbDest.Size = new System.Drawing.Size(121, 21);
			this.cbDest.TabIndex = 8;
			this.cbDest.ValueMember = "FieldName";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(248, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(120, 30);
			this.label2.TabIndex = 7;
			this.label2.Text = "Код второй колонки (приёмник):";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 42);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 30);
			this.label1.TabIndex = 6;
			this.label1.Text = "Код первой колонки (источник):";
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(568, 8);
			this.btnClose.Name = "btnClose";
			this.btnClose.TabIndex = 5;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// lbCounter
			// 
			this.lbCounter.Location = new System.Drawing.Point(464, 8);
			this.lbCounter.Name = "lbCounter";
			this.lbCounter.Size = new System.Drawing.Size(96, 16);
			this.lbCounter.TabIndex = 3;
			this.lbCounter.Text = "Счётчик:";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(128, 8);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(112, 24);
			this.button3.TabIndex = 2;
			this.button3.Text = "Импорт";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(112, 24);
			this.button1.TabIndex = 0;
			this.button1.Text = "Загрузить данные";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// dgItems
			// 
			this.dgItems.BackgroundColor = System.Drawing.Color.White;
			this.dgItems.DataMember = "";
			this.dgItems.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgItems.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgItems.Location = new System.Drawing.Point(0, 80);
			this.dgItems.Name = "dgItems";
			this.dgItems.Order = null;
			this.dgItems.Size = new System.Drawing.Size(648, 421);
			this.dgItems.TabIndex = 2;
			this.dgItems.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																																												this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.dgItems;
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			// 
			// dataSource
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("ru-RU");
			this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
																																	this.dataTable1});
			// 
			// fmSetIdentity
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(648, 501);
			this.Controls.Add(this.dgItems);
			this.Controls.Add(this.panel2);
			this.Name = "fmSetIdentity";
			this.Text = "fmLoadParams";
			this.Load += new System.EventHandler(this.fmSetIdentity_Load);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
