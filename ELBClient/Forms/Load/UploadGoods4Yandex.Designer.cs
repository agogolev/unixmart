﻿using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Load
{
	partial class UploadGoods4Yandex
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.ExportButton = new System.Windows.Forms.Button();
			this.lbShopType = new System.Windows.Forms.Label();
			this.cbShopType = new ColumnMenuExtender.DataBoundComboBox();
			this.GoodsGrid = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.SaveCsv = new System.Windows.Forms.SaveFileDialog();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.ExportButton);
			this.panel1.Controls.Add(this.lbShopType);
			this.panel1.Controls.Add(this.cbShopType);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(626, 32);
			this.panel1.TabIndex = 6;
			// 
			// ExportButton
			// 
			this.ExportButton.Location = new System.Drawing.Point(286, 5);
			this.ExportButton.Name = "ExportButton";
			this.ExportButton.Size = new System.Drawing.Size(169, 23);
			this.ExportButton.TabIndex = 6;
			this.ExportButton.Text = "Экспорт в Excel";
			this.ExportButton.UseVisualStyleBackColor = true;
			this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
			// 
			// lbShopType
			// 
			this.lbShopType.Location = new System.Drawing.Point(4, 4);
			this.lbShopType.Name = "lbShopType";
			this.lbShopType.Size = new System.Drawing.Size(64, 23);
			this.lbShopType.TabIndex = 5;
			this.lbShopType.Text = "Магазин:";
			this.lbShopType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbShopType
			// 
			this.cbShopType.Location = new System.Drawing.Point(76, 5);
			this.cbShopType.Name = "cbShopType";
			this.cbShopType.SelectedValue = -1;
			this.cbShopType.Size = new System.Drawing.Size(184, 21);
			this.cbShopType.TabIndex = 4;
            this.cbShopType.SelectedIndexChanged += new PositionChangedEventHandler(this.cbShopType_SelectedIndexChanged);
			// 
			// GoodsGrid
			// 
			this.GoodsGrid.BackgroundColor = System.Drawing.Color.White;
			this.GoodsGrid.DataMember = "";
			this.GoodsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GoodsGrid.FilterString = null;
			this.GoodsGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.GoodsGrid.Location = new System.Drawing.Point(0, 32);
			this.GoodsGrid.Name = "GoodsGrid";
			this.GoodsGrid.Order = null;
			this.GoodsGrid.Size = new System.Drawing.Size(626, 234);
			this.GoodsGrid.TabIndex = 7;
			this.GoodsGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.GoodsGrid;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "ID";
			this.formattableTextBoxColumn1.MappingName = "modelID";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Вендор";
			this.formattableTextBoxColumn2.MappingName = "vendor";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// formattableTextBoxColumn3
			// 
			this.formattableTextBoxColumn3.FieldName = null;
			this.formattableTextBoxColumn3.FilterFieldName = null;
			this.formattableTextBoxColumn3.Format = "";
			this.formattableTextBoxColumn3.FormatInfo = null;
			this.formattableTextBoxColumn3.HeaderText = "Модель";
			this.formattableTextBoxColumn3.MappingName = "model";
			this.formattableTextBoxColumn3.Width = 75;
			// 
			// SaveCsv
			// 
			this.SaveCsv.DefaultExt = "xls";
			this.SaveCsv.Filter = "Файлы Excel|*.xls|Все файлы|*.*";
			this.SaveCsv.Title = "Сохранить список";
			// 
			// UploadGoods4Yandex
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(626, 266);
			this.Controls.Add(this.GoodsGrid);
			this.Controls.Add(this.panel1);
			this.Name = "UploadGoods4Yandex";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Проверка файла Яндекс";
			this.Load += new System.EventHandler(this.UploadGoods4Yandex_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lbShopType;
		private ColumnMenuExtender.DataBoundComboBox cbShopType;
		private ColumnMenuExtender.DataGridISM GoodsGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private System.Windows.Forms.Button ExportButton;
		private System.Windows.Forms.SaveFileDialog SaveCsv;
	}
}