using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class LoadFile2Server
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Button button1;
private System.Windows.Forms.OpenFileDialog ofdArchive;
private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.ofdArchive = new System.Windows.Forms.OpenFileDialog();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(64, 40);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(72, 40);
			this.button1.TabIndex = 0;
			this.button1.Text = "Загрузить архив";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// ofdArchive
			// 
			this.ofdArchive.Filter = "Zip архив|*.zip|Все файлы|*.*";
			// 
			// fmLoadFile2Server
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(200, 140);
			this.Controls.Add(this.button1);
			this.Name = "fmLoadFile2Server";
			this.Text = "Загрузить";
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
