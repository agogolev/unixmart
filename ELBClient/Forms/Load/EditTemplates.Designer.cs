﻿using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Load
{
	partial class EditTemplates
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.MasterCatalogTreeView = new System.Windows.Forms.TreeView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbShopType = new System.Windows.Forms.Label();
            this.cbShopType = new ColumnMenuExtender.DataBoundComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ImportButton = new System.Windows.Forms.Button();
            this.ExportButton = new System.Windows.Forms.Button();
            this.ClearParams = new System.Windows.Forms.Button();
            this.BuildTemplateButton = new System.Windows.Forms.Button();
            this.ClearTemplateButton = new System.Windows.Forms.Button();
            this.TemplateGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn4 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableBooleanColumn2 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn1 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.formattableBooleanColumn3 = new ColumnMenuExtender.FormattableBooleanColumn();
            this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.TemplateMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LoadExcel = new System.Windows.Forms.OpenFileDialog();
            this.SaveExcel = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbShopType)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TemplateGrid)).BeginInit();
            this.TemplateMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MasterCatalogTreeView
            // 
            this.MasterCatalogTreeView.AllowDrop = true;
            this.MasterCatalogTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.MasterCatalogTreeView.HideSelection = false;
            this.MasterCatalogTreeView.Location = new System.Drawing.Point(0, 32);
            this.MasterCatalogTreeView.Name = "MasterCatalogTreeView";
            this.MasterCatalogTreeView.Size = new System.Drawing.Size(260, 496);
            this.MasterCatalogTreeView.TabIndex = 3;
            this.MasterCatalogTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.MasterCatalogTreeView_AfterSelect);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(260, 32);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 496);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbShopType);
            this.panel1.Controls.Add(this.cbShopType);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 32);
            this.panel1.TabIndex = 5;
            // 
            // lbShopType
            // 
            this.lbShopType.Location = new System.Drawing.Point(4, 4);
            this.lbShopType.Name = "lbShopType";
            this.lbShopType.Size = new System.Drawing.Size(64, 23);
            this.lbShopType.TabIndex = 5;
            this.lbShopType.Text = "Магазин:";
            this.lbShopType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbShopType
            // 
            this.cbShopType.AutoCompleteDisplayMember = null;
            this.cbShopType.AutoCompleteValueMember = null;
            this.cbShopType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cbShopType.Location = new System.Drawing.Point(76, 5);
            this.cbShopType.Name = "cbShopType";
            this.cbShopType.SelectedValue = -1;
            this.cbShopType.Size = new System.Drawing.Size(184, 20);
            this.cbShopType.TabIndex = 4;
            this.cbShopType.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cbShopType_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ImportButton);
            this.panel2.Controls.Add(this.ExportButton);
            this.panel2.Controls.Add(this.ClearParams);
            this.panel2.Controls.Add(this.BuildTemplateButton);
            this.panel2.Controls.Add(this.ClearTemplateButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(263, 476);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(745, 52);
            this.panel2.TabIndex = 6;
            // 
            // ImportButton
            // 
            this.ImportButton.Location = new System.Drawing.Point(514, 6);
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.Size = new System.Drawing.Size(122, 35);
            this.ImportButton.TabIndex = 4;
            this.ImportButton.Text = "Загрузка описаний товаров";
            this.ImportButton.UseVisualStyleBackColor = true;
            this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // ExportButton
            // 
            this.ExportButton.Location = new System.Drawing.Point(376, 6);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(125, 35);
            this.ExportButton.TabIndex = 3;
            this.ExportButton.Text = "Экспорт описаний товаров";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // ClearParams
            // 
            this.ClearParams.Location = new System.Drawing.Point(241, 6);
            this.ClearParams.Name = "ClearParams";
            this.ClearParams.Size = new System.Drawing.Size(129, 35);
            this.ClearParams.TabIndex = 2;
            this.ClearParams.Text = "Очистить описания товаров в категории";
            this.ClearParams.UseVisualStyleBackColor = true;
            this.ClearParams.Visible = false;
            this.ClearParams.Click += new System.EventHandler(this.ClearParams_Click);
            // 
            // BuildTemplateButton
            // 
            this.BuildTemplateButton.Location = new System.Drawing.Point(120, 6);
            this.BuildTemplateButton.Name = "BuildTemplateButton";
            this.BuildTemplateButton.Size = new System.Drawing.Size(115, 35);
            this.BuildTemplateButton.TabIndex = 1;
            this.BuildTemplateButton.Text = "На основе описаний товаров ";
            this.BuildTemplateButton.UseVisualStyleBackColor = true;
            this.BuildTemplateButton.Click += new System.EventHandler(this.BuildTemplateButton_Click);
            // 
            // ClearTemplateButton
            // 
            this.ClearTemplateButton.Location = new System.Drawing.Point(3, 6);
            this.ClearTemplateButton.Name = "ClearTemplateButton";
            this.ClearTemplateButton.Size = new System.Drawing.Size(111, 34);
            this.ClearTemplateButton.TabIndex = 0;
            this.ClearTemplateButton.Text = "Очистить шаблон категории";
            this.ClearTemplateButton.UseVisualStyleBackColor = true;
            this.ClearTemplateButton.Click += new System.EventHandler(this.ClearTemplateButton_Click);
            // 
            // TemplateGrid
            // 
            this.TemplateGrid.BackgroundColor = System.Drawing.Color.White;
            this.TemplateGrid.DataMember = "";
            this.TemplateGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TemplateGrid.FilterString = null;
            this.TemplateGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.TemplateGrid.Location = new System.Drawing.Point(263, 32);
            this.TemplateGrid.Name = "TemplateGrid";
            this.TemplateGrid.Order = null;
            this.TemplateGrid.Size = new System.Drawing.Size(745, 444);
            this.TemplateGrid.TabIndex = 7;
            this.TemplateGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.TemplateGrid.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TemplateGrid_MouseDown);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.TemplateGrid;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn4,
            this.formattableTextBoxColumn3,
            this.formattableBooleanColumn2,
            this.formattableBooleanColumn1,
            this.formattableBooleanColumn3,
            this.extendedDataGridSelectorColumn1});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "Table";
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "№ пп.";
            this.formattableTextBoxColumn1.MappingName = "ordValue";
            this.formattableTextBoxColumn1.ReadOnly = true;
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Название";
            this.formattableTextBoxColumn2.MappingName = "paramName";
            this.formattableTextBoxColumn2.ReadOnly = true;
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn4
            // 
            this.formattableTextBoxColumn4.FieldName = null;
            this.formattableTextBoxColumn4.FilterFieldName = null;
            this.formattableTextBoxColumn4.Format = "";
            this.formattableTextBoxColumn4.FormatInfo = null;
            this.formattableTextBoxColumn4.HeaderText = "Имя для фильтра";
            this.formattableTextBoxColumn4.MappingName = "altName";
            this.formattableTextBoxColumn4.NullText = "";
            this.formattableTextBoxColumn4.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Ед. изм.";
            this.formattableTextBoxColumn3.MappingName = "unit";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // formattableBooleanColumn2
            // 
            this.formattableBooleanColumn2.AllowNull = false;
            this.formattableBooleanColumn2.FieldName = null;
            this.formattableBooleanColumn2.FilterFieldName = null;
            this.formattableBooleanColumn2.HeaderText = "Для фильтра";
            this.formattableBooleanColumn2.MappingName = "isFilter";
            this.formattableBooleanColumn2.Width = 75;
            // 
            // formattableBooleanColumn1
            // 
            this.formattableBooleanColumn1.AllowNull = false;
            this.formattableBooleanColumn1.FieldName = null;
            this.formattableBooleanColumn1.FilterFieldName = null;
            this.formattableBooleanColumn1.HeaderText = "Для списка";
            this.formattableBooleanColumn1.MappingName = "isFullName";
            this.formattableBooleanColumn1.Width = 75;
            // 
            // formattableBooleanColumn3
            // 
            this.formattableBooleanColumn3.AllowNull = false;
            this.formattableBooleanColumn3.FieldName = null;
            this.formattableBooleanColumn3.FilterFieldName = null;
            this.formattableBooleanColumn3.HeaderText = "Видимый в карточке";
            this.formattableBooleanColumn3.MappingName = "isVisible";
            this.formattableBooleanColumn3.Width = 75;
            // 
            // extendedDataGridSelectorColumn1
            // 
            this.extendedDataGridSelectorColumn1.FieldName = null;
            this.extendedDataGridSelectorColumn1.FilterFieldName = null;
            this.extendedDataGridSelectorColumn1.Format = "";
            this.extendedDataGridSelectorColumn1.FormatInfo = null;
            this.extendedDataGridSelectorColumn1.HeaderText = "...";
            this.extendedDataGridSelectorColumn1.MappingName = "selectFilter";
            this.extendedDataGridSelectorColumn1.NullText = "";
            this.extendedDataGridSelectorColumn1.Width = 20;
            // 
            // TemplateMenu
            // 
            this.TemplateMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddToolStripMenuItem,
            this.DeleteToolStripMenuItem,
            this.EditToolStripMenuItem,
            this.UpToolStripMenuItem,
            this.DownToolStripMenuItem});
            this.TemplateMenu.Name = "contextMenuStrip1";
            this.TemplateMenu.Size = new System.Drawing.Size(155, 114);
            // 
            // AddToolStripMenuItem
            // 
            this.AddToolStripMenuItem.Name = "AddToolStripMenuItem";
            this.AddToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.AddToolStripMenuItem.Text = "Добавить";
            this.AddToolStripMenuItem.Click += new System.EventHandler(this.AddToolStripMenuItem_Click);
            // 
            // DeleteToolStripMenuItem
            // 
            this.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem";
            this.DeleteToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.DeleteToolStripMenuItem.Text = "Удалить";
            this.DeleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItem_Click);
            // 
            // EditToolStripMenuItem
            // 
            this.EditToolStripMenuItem.Name = "EditToolStripMenuItem";
            this.EditToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.EditToolStripMenuItem.Text = "Редактировать";
            this.EditToolStripMenuItem.Click += new System.EventHandler(this.EditToolStripMenuItem_Click);
            // 
            // UpToolStripMenuItem
            // 
            this.UpToolStripMenuItem.Name = "UpToolStripMenuItem";
            this.UpToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.UpToolStripMenuItem.Text = "Вверх";
            this.UpToolStripMenuItem.Click += new System.EventHandler(this.UpToolStripMenuItem_Click);
            // 
            // DownToolStripMenuItem
            // 
            this.DownToolStripMenuItem.Name = "DownToolStripMenuItem";
            this.DownToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.DownToolStripMenuItem.Text = "Вниз";
            this.DownToolStripMenuItem.Click += new System.EventHandler(this.DownToolStripMenuItem_Click);
            // 
            // LoadExcel
            // 
            this.LoadExcel.FileName = "openFileDialog1";
            // 
            // SaveExcel
            // 
            this.SaveExcel.DefaultExt = "xls";
            this.SaveExcel.Filter = "Файлы Excel 2010|*.xlsx|Все файлы|*.*";
            this.SaveExcel.Title = "Сохранить список";
            // 
            // EditTemplates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 528);
            this.Controls.Add(this.TemplateGrid);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.MasterCatalogTreeView);
            this.Controls.Add(this.panel1);
            this.Name = "EditTemplates";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Редактирование шаблонов";
            this.Load += new System.EventHandler(this.EditTemplates_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbShopType)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TemplateGrid)).EndInit();
            this.TemplateMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TreeView MasterCatalogTreeView;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private ColumnMenuExtender.DataGridISM TemplateGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn2;
		private System.Windows.Forms.Label lbShopType;
		private ColumnMenuExtender.DataBoundComboBox cbShopType;
		private System.Windows.Forms.Button BuildTemplateButton;
		private System.Windows.Forms.Button ClearTemplateButton;
		private System.Windows.Forms.Button ClearParams;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
		private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn1;
		private System.Windows.Forms.ContextMenuStrip TemplateMenu;
		private System.Windows.Forms.ToolStripMenuItem AddToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem DeleteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem EditToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem UpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem DownToolStripMenuItem;
		private System.Windows.Forms.Button ExportButton;
		private System.Windows.Forms.Button ImportButton;
		private System.Windows.Forms.OpenFileDialog LoadExcel;
		private System.Windows.Forms.SaveFileDialog SaveExcel;
        private ColumnMenuExtender.FormattableBooleanColumn formattableBooleanColumn3;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn4;
    }
}