﻿namespace ELBClient.Forms.Load
{
	partial class ShortGoodsList
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.GoodsGrid = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GoodsGrid
            // 
            this.GoodsGrid.BackgroundColor = System.Drawing.Color.White;
            this.GoodsGrid.DataMember = "";
            this.GoodsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GoodsGrid.FilterString = null;
            this.GoodsGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.GoodsGrid.Location = new System.Drawing.Point(0, 0);
            this.GoodsGrid.Name = "GoodsGrid";
            this.GoodsGrid.Order = null;
            this.GoodsGrid.Size = new System.Drawing.Size(517, 264);
            this.GoodsGrid.TabIndex = 0;
            this.GoodsGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            this.GoodsGrid.DoubleClick += new System.EventHandler(this.GoodsGrid_DoubleClick);
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.GoodsGrid;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.formattableTextBoxColumn2,
            this.formattableTextBoxColumn3});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "Table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // formattableTextBoxColumn1
            // 
            this.formattableTextBoxColumn1.FieldName = null;
            this.formattableTextBoxColumn1.FilterFieldName = null;
            this.formattableTextBoxColumn1.Format = "";
            this.formattableTextBoxColumn1.FormatInfo = null;
            this.formattableTextBoxColumn1.HeaderText = "Код сайта";
            this.formattableTextBoxColumn1.MappingName = "objectId";
            this.formattableTextBoxColumn1.Width = 75;
            // 
            // formattableTextBoxColumn2
            // 
            this.formattableTextBoxColumn2.FieldName = null;
            this.formattableTextBoxColumn2.FilterFieldName = null;
            this.formattableTextBoxColumn2.Format = "";
            this.formattableTextBoxColumn2.FormatInfo = null;
            this.formattableTextBoxColumn2.HeaderText = "Код поставщика";
            this.formattableTextBoxColumn2.MappingName = "postavshik_id";
            this.formattableTextBoxColumn2.Width = 75;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Модель";
            this.formattableTextBoxColumn3.MappingName = "model";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // ShortGoodsList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 264);
            this.Controls.Add(this.GoodsGrid);
            this.Name = "ShortGoodsList";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Товары для параметра";
            this.Load += new System.EventHandler(this.ShortGoodsList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GoodsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private ColumnMenuExtender.DataGridISM GoodsGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
	}
}