﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using MetaData;
using ELBClient.ObjectProvider;
using Excel;

namespace ELBClient.Forms.Load
{
	public partial class LoadAnalogs : ListForm
	{
		public int StartFrom { get; set; }
		private DataSet _ds;
		private int _processedItems = 0;
		public LoadAnalogs()
		{
			InitializeComponent();
			StartFrom = 1;
		}

		private void LoadFile_Click(object sender, EventArgs e)
		{
			if (LoadExcel.ShowDialog(this) == DialogResult.OK)
			{
				try
				{
					using (FileStream stream = File.Open(LoadExcel.FileName, FileMode.Open, FileAccess.Read))
					{
						using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
						{
							excelReader.Read();
							_ds = new DataSetISM();
							var dt = new DataTable("Table");
							_ds.Tables.Add(dt);
							int i = 0;
							string name = null;
							do
							{
								name = excelReader.GetString(i);
								dt.Columns.Add(name);
								i++;
							}
							while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
							while (excelReader.Read())
							{
								DataRow dr = dt.NewRow();
								for (int j = 0; j < dt.Columns.Count; j++)
								{
									dr[j] = excelReader.GetString(j);
								}
								dt.Rows.Add(dr);
							}
							_ds.AcceptChanges();
						}
					}

					dgGoods.DataSource = _ds;
					dgGoods.DataMember = "Table";
					_processedItems = 0;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					return;
				}
			}
		}

		private void ImportAnalogs_Click(object sender, EventArgs e)
		{
			if (_ds == null)
			{
				ShowError("Загрузите файл");
				return;
			}

			const string sqlDel = @"
DELETE FROM t_GoodsAnalogs
FROM t_Goods g inner join t_GoodsAnalogs oa on g.OID = oa.OID and g.postavshik_id = @id
";

			const string sql = @"

Declare
	@analogOID uniqueidentifier,
	@ordValue int

SELECT @analogOID = OID FROM t_Goods WHERE postavshik_id = @analogID
SELECT @ordValue = ISNULL(MAX(ordValue), 0) + 1 FROM t_GoodsAnalogs ga inner join t_Goods g on ga.OID = g.OID and g.postavshik_id = @id

if @analogOID is not null Begin
	if NOT EXISTS (SELECT * FROM t_GoodsAnalogs ga inner join t_Goods g on ga.OID = g.OID and g.postavshik_id = @id and ga.analogOID = @analogOID) Begin
		INSERT INTO t_GoodsAnalogs (OID, ordValue, analogOID) 
		SELECT OID, @ordValue, @analogOID FROM t_Goods WHERE postavshik_id = @id
	end
End
";
			_processedItems = StartFrom - 1;
			for (int i = StartFrom - 1; i < _ds.Tables[0].Rows.Count; i++)
			{
				var dr = _ds.Tables[0].Rows[i];
				int id;
				if(int.TryParse(dr[0].ToString(), out id))
				{
					op.ExecuteCommandParams(sqlDel, new[] {
							new NameValuePair { Name = "id", Value = id.ToString()}}, false);
					if (dr[1] != null && dr[1] != DBNull.Value && !string.IsNullOrWhiteSpace((string)dr[1]))
					{
						var analogs = dr[1].ToString().Trim();
						analogs.Split(",".ToCharArray()).ToList().ForEach(item =>
						{
							int analogID;
							if (int.TryParse(item, out analogID))
							{
								op.ExecuteCommandParams(sql, new[] {
								new NameValuePair { Name = "id", Value = id.ToString()},
								new NameValuePair { Name = "analogID", Value = analogID.ToString()}}, false);
							} 
						
						});
					}
				}

				Counter.Text = "Обработано: " + ++_processedItems;
				Counter.Update();
			}
			MessageBox.Show(this, @"Импорт сопутствующих товаров завершён");
		}

		private void LoadGoods_Load(object sender, EventArgs e)
		{
			StartFromText.DataBindings.Add("Text", this, "StartFrom");
		}

	}
}
