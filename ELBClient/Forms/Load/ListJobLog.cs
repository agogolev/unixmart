﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ColumnMenuExtender;
using ELBClient.Forms.Dialogs;

namespace ELBClient.Forms.Load
{
	public partial class ListJobLog : ListForm
	{
		private readonly BusinessProvider.BusinessProvider bp = Classes.ServiceUtility.BusinessProvider;
		public ListJobLog()
		{
			InitializeComponent();
			LogGrid.GetForeColorForRow += LogGrid_GetForeColorForRow;
			extendedDataGridSelectorColumn1.ButtonClick += extendedDataGridSelectorColumn1_ButtonClick;
		}

		void extendedDataGridSelectorColumn1_ButtonClick(object sender, DataGridCellEventArgs e)
		{
			var drv = (DataRowView)e.CurrentRow;
			var fm = new Info { Body = (string)drv["stackTrace"], ReadOnly = true };
			fm.ShowDialog(this);
		}

		Brush LogGrid_GetForeColorForRow(int rowNum, CurrencyManager source)
		{
			var dv = (DataView)source.List;
			var dr = dv[rowNum];
			Brush br = null;
			if ((int)dr["level"] > 4)
			{
				br = new SolidBrush(Color.Red);
			}
			return br;
		}

		private void ListJobLog_Load(object sender, EventArgs e)
		{
			dtpDateBegin.Value = DateTime.Today;
			dtpDateEnd.Value = DateTime.Today;
			LoadLookup();
			LoadData();
		}

		private void LoadLookup()
		{
			var ds = bp.GetJobsLookupContent();
			if (ds != null)
			{
				var i = 0;
				JobSelect.AddItems(ds.Tables[1].AsEnumerable().Select(row => new ListItem(i++, row.Field<string>("name")) { Tag = row.Field<Guid>("OID") }).ToArray());
				LevelSelect.AddItems(ds.Tables[0].AsEnumerable().Select(row => new ListItem(row.Field<int>("level"), row.Field<string>("name"))).ToArray());
			}
			LevelSelect.InsertItem(0, new ListItem(0, "Все"));
			extendedDataGridComboBoxColumn1.ComboBox.AddItems(ds.Tables[0].AsEnumerable().Select(row => new ListItem(row.Field<int>("level"), row.Field<string>("name"))).ToArray());
			JobSelect.SelectedIndex = 0;
			LevelSelect.SelectedIndex = 0;
		}

		public override void LoadData()
		{
            var data = bp.GetJobLog((Guid)JobSelect.SelectedItem.Tag, (int)LevelSelect.SelectedItem.Value, dtpDateBegin.Value,
			             dtpDateEnd.Value.AddDays(1));
			data.Tables[0].Columns.Add("stackTraceButton");
			LogGrid.SetDataBinding(data, "Table");
			LogGrid.DisableNewDel();
		}
		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			LoadData();
		}
	}
}
