﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using MetaData;
using ELBClient.ObjectProvider;
using System.Text.RegularExpressions;
using Excel;

namespace ELBClient.Forms.Load
{
	public partial class LoadKeywords : ListForm
	{
		public int StartFrom { get; set; }
		private DataSet _ds;
		private int _processedItems = 0;
		public LoadKeywords()
		{
			InitializeComponent();
			StartFrom = 1;
		}

		private void LoadFile_Click(object sender, EventArgs e)
		{
			if (LoadExcel.ShowDialog(this) == DialogResult.OK)
			{
				try
				{
					using (FileStream stream = File.Open(LoadExcel.FileName, FileMode.Open, FileAccess.Read))
					{
						using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
						{
							excelReader.Read();
							_ds = new DataSetISM();
							DataTable dt = new DataTable("Table");
							_ds.Tables.Add(dt);
							int i = 0;
							string name = null;
							do
							{
								name = excelReader.GetString(i);
								dt.Columns.Add(name);
								i++;
							}
							while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
							while (excelReader.Read())
							{
								DataRow dr = dt.NewRow();
								for (int j = 0; j < dt.Columns.Count; j++)
								{
									dr[j] = excelReader.GetString(j);
								}
								dt.Rows.Add(dr);
							}
							_ds.AcceptChanges();
						}
					}

					dgGoods.DataSource = _ds;
					dgGoods.DataMember = "Table";
					_processedItems = 0;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					return;
				}
			}
		}

		private void ImportDescriptions_Click(object sender, EventArgs e)
		{
			if (_ds == null)
			{
				ShowError("Загрузите файл");
				return;
			}

			string sql = @"

if EXISTS (SELECT * FROM t_ObjectKeywords ok inner join t_Goods g on ok.OID = g.OID and g.postavshik_id = @id and ok.ordValue = @ordValue)
	UPDATE t_ObjectKeywords SET keywords = @keywords
	FROM t_ObjectKeywords ok 
		inner join t_Goods g on ok.OID = g.OID and g.postavshik_id = @id and ok.ordValue = @ordValue
else
	INSERT INTO t_ObjectKeywords (OID, ordValue, keywords) 
	SELECT OID, @ordValue, @keywords FROM t_Goods WHERE postavshik_id = @id
";
			_processedItems = StartFrom - 1;
			Regex SpacesReg = new Regex(@"\s+", RegexOptions.Singleline);
			Regex MinusReg = new Regex(@"\s*,\s*", RegexOptions.Singleline);
			for (int i = StartFrom - 1; i < _ds.Tables[0].Rows.Count; i++)
			{
				DataRow dr = _ds.Tables[0].Rows[i];
				int id = int.Parse(dr[0].ToString().Trim());
				string keywords;
				if (dr[3] != null && dr[3] != DBNull.Value && !string.IsNullOrWhiteSpace((string)dr[3]))
				{
					keywords = dr[3].ToString().Trim();
					int ordValue = 1;
					keywords.Split("|".ToCharArray()).ToList().ForEach(item =>
					{
						var normKey = SpacesReg.Replace(MinusReg.Replace(item, " -"), " "); 
						op.ExecuteCommandParams(sql, new NameValuePair[] {
							new NameValuePair { Name = "id", Value = id.ToString()},
							new NameValuePair { Name = "ordValue", Value = ordValue++.ToString()},
							new NameValuePair { Name = "keywords", Value = normKey },
						}, false);
					});

				}

				Counter.Text = "Обработано: " + ++_processedItems;
				Counter.Update();
			}
			MessageBox.Show(this, @"Импорт ключевых слов завершён");
		}

		private void LoadGoods_Load(object sender, EventArgs e)
		{
			StartFromText.DataBindings.Add("Text", this, "StartFrom");
		}

	}
}
