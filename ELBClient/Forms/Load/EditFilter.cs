﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Ecommerce.Extensions;
using MetaData;
using ELBClient.Classes;

namespace ELBClient.Forms.Load
{
	public partial class EditFilter : EditForm
	{
		private DataSetISM ParamDataSource;
		public Guid ThemeOID { get; set; }
		public EditFilter()
		{
			InitializeComponent();
		}

		private void EditFilter_Load(object sender, EventArgs e)
		{
			LoadData();
		}

		private void LoadData()
		{
			LoadTemplateParams();
			LoadGoodsParams();
		}

		private void LoadTemplateParams()
		{
			string sql = @"
SELECT
	*
FROM
	t_ParamType
WHERE
	OID = {1}

SELECT
	*
FROM
	t_TemplateParamValues 
WHERE
	OID = {0}
	and paramTypeOID = {1}
	
SELECT
	*
FROM
	t_TemplateParamAssociation
WHERE
	OID = {0}
	and paramTypeOID = {1}
";
			DataSetISM TemplateDataSource = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(ThemeOID), ToSqlString(ObjectOID))));

			if (TemplateDataSource.Table.Rows.Count != 0)
			{
				TreeNode tn = new TreeNode(
					(string)TemplateDataSource.Table.Rows[0]["name"],
					TemplateDataSource.Tables[1].AsEnumerable()
						.OrderBy(k => k.Field<int>("ordValue"))
						.Select(k => new TreeNode(k.Field<string>("name"),
							TemplateDataSource.Tables[2].AsEnumerable()
								.Where(k1 => string.Compare(k1.Field<string>("name"), k.Field<string>("name"), true) == 0)
								.OrderBy(k1 => k1.Field<string>("name"))
								.Select(k1 => new TreeNode(string.Format("{0} {1}", k1.Field<string>("value"), k1.Field<string>("unit")))
								{
									Tag = new TemplateParamAssociation
									{
										OID = k1.Field<Guid>("OID"),
										ParamType = k1.Field<Guid>("ParamTypeOID"),
										Name = k1.Field<string>("name").ToLower(),
										Value = k1.Field<string>("value"),
										Unit = k1.Field<string>("unit")
									}
								}).ToArray())
							{
								Tag = new TemplateParamValue
								{
									OID = k.Field<Guid>("OID"),
									ParamType = k.Field<Guid>("ParamTypeOID"),
									Name = k.Field<string>("name").ToLower(),
									OrdValue = k.Field<int>("ordValue")
								}
							}).ToArray())
							{
								Tag = new CParamType
								{
									OID = (Guid)TemplateDataSource.Table.Rows[0]["OID"]
								}
							};
				TemplateParamValuesTreeView.Nodes.Clear();
				TemplateParamValuesTreeView.Nodes.Add(tn);
				TemplateParamValuesTreeView.ExpandAll();
			}
		}

		private void LoadGoodsParams()
		{
			string sql = @"
SELECT DISTINCT
	fullValue = gp.value + ' ' + gp.unit,
	gp.value,
	gp.unit,
	countAssociations = (SELECT Count(*) FROM t_TemplateParamAssociation WHERE OID = {0} And paramTypeOID = {1} and value = gp.value and unit = gp.unit) 
FROM
	t_Nodes n 
	inner join t_Theme t on n.object = t.OID
    inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.tree = '0e5f5d35-3f4f-4418-a571-c8e9974550b2'
	inner join t_ThemeMasters tm on n1.object = tm.OID
	inner join t_Goods g on g.category = tm.masterOID
	inner join t_GoodsParams gp on g.OID = gp.OID
WHERE
	t.OID = {0} and gp.paramTypeOID = {1}

/*SELECT
	g.OID,
	g.Model,
	manufacturerName = c.companyName,
	g.ProductType,
	gp.value,
	gp.unit
FROM
	t_Theme t
	inner join t_ThemeMasters tm on t.OID = tm.OID
	inner join t_Goods g on g.category = tm.masterOID
	left join t_Company c on g.manufacturer = c.OID
	inner join t_GoodsParams gp on g.OID = gp.OID
WHERE
	t.OID = {0} and gp.paramTypeOID = {1}*/
";
			sql = string.Format(sql, ToSqlString(ThemeOID), ToSqlString(ObjectOID));
			ParamDataSource = new DataSetISM(lp.GetDataSetSql(sql));
			ParamValuesTreeView.Nodes.Clear();
			ParamValuesTreeView.Nodes.AddRange(ParamDataSource.Table.AsEnumerable()
				.OrderBy(k => k.Field<string>("fullValue"))
				.Select(k => new TreeNode(k.Field<string>("fullValue"))
				{
					Tag = new GoodsParam
						{
							Value = k.Field<string>("value"),
							Unit = k.Field<string>("unit")
						},
					ForeColor = k.Field<int>("countAssociations") != 0 ? Color.Green : Color.Red
				}).ToArray());
		}

		private void AddFilterToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode selectedNode = TemplateParamValuesTreeView.SelectedNode;
			int level = selectedNode.Level;

			if (level > 1) return;
			else if (level == 0)
			{
				TreeNode newNode = selectedNode.Nodes.Insert(0, "Новый фильтр");
				newNode.Tag = new TemplateParamValue
				{
					OID = ThemeOID,
					ParamType = ObjectOID,
					OrdValue = 0,
					IsNew = true
				};
				TemplateParamValuesTreeView.SelectedNode = newNode;
				newNode.BeginEdit();
			}
			else
			{
				TreeNode newNode = selectedNode.Parent.Nodes.Insert(selectedNode.Index + 1, "Новый фильтр");
				newNode.Tag = new TemplateParamValue
				{
					OID = ThemeOID,
					ParamType = ObjectOID,
					OrdValue = selectedNode.Index + 1,
					IsNew = true
				};
				TemplateParamValuesTreeView.SelectedNode = newNode;
				newNode.BeginEdit();
			}
		}

		private void DeleteFilterToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode selectedNode = TemplateParamValuesTreeView.SelectedNode;
			int level = selectedNode.Level;

			if (level != 1) return;
			TemplateParamValue itemDeleted = selectedNode.Tag as TemplateParamValue;
			DeleteFilter(itemDeleted, selectedNode);
		}

		private void TemplateMenu_Opening(object sender, CancelEventArgs e)
		{
			int level = TemplateParamValuesTreeView.SelectedNode.Level;
			if (level > 1) e.Cancel = true;
			else if (level == 0) DeleteFilterToolStripMenuItem.Enabled = false;
			else DeleteFilterToolStripMenuItem.Enabled = true;
		}

		private void TemplateParamValuesTreeView_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
		{
			if (e.Node.Level != 1) e.CancelEdit = true;
		}

		private void TemplateParamValuesTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
		{
			if (e.CancelEdit) return;
			TemplateParamValue itemEdit = e.Node.Tag as TemplateParamValue;
			string newName = e.Label;
			if (itemEdit == null || string.IsNullOrWhiteSpace(newName))
			{
				if (itemEdit != null && itemEdit.IsNew) e.Node.Parent.Nodes.RemoveAt(e.Node.Index);
				e.CancelEdit = true;
				return;
			}
			if (itemEdit.IsNew)
			{
				InsertFilter(itemEdit, newName);
			}
			else
			{
				UpdateFilter(itemEdit, newName);
			}
		}

		private void UpdateFilter(TemplateParamValue itemEdit, string newName)
		{
			const string sql = "UPDATE t_TemplateParamValues SET name = {3} WHERE OID = {0} And paramTypeOID = {1} and ordValue = {2}";
			op.ExecuteCommand(string.Format(sql, ThemeOID.ToSqlString(), ObjectOID.ToSqlString(), itemEdit.OrdValue, newName.ToSqlString()));
			itemEdit.Name = newName;
		}

		private void InsertFilter(TemplateParamValue itemEdit, string name)
		{
			const string sql = @"
UPDATE t_TemplateParamValues SET ordValue = ordValue + 1 WHERE OID = {0} And paramTypeOID = {1} and ordValue >= {2}
INSERT INTO t_TemplateParamValues VALUES({0}, {1}, {3}, {2})";
			op.ExecuteCommand(string.Format(sql, ThemeOID.ToSqlString(), ObjectOID.ToSqlString(), itemEdit.OrdValue, name.ToSqlString()));
			itemEdit.Name = name;
			itemEdit.IsNew = false;
		}

		private void DeleteFilter(TemplateParamValue itemDeleted, TreeNode selectedNode)
		{
			string sql = @"
DELETE FROM t_TemplateParamValues WHERE OID = {0} And paramTypeOID = {1} and ordValue = {2}
UPDATE t_TemplateParamValues SET ordValue = ordValue - 1 WHERE OID = {0} And paramTypeOID = {1} and ordValue > {2}
";
			op.ExecuteCommand(string.Format(sql, ThemeOID.ToSqlString(), ObjectOID.ToSqlString(), itemDeleted.OrdValue));

			selectedNode.Parent.Nodes.RemoveAt(selectedNode.Index);
		}

		private void TemplateParamValuesTreeView_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				TreeNode tn = TemplateParamValuesTreeView.HitTest(e.Location).Node;
				if (tn != null)
					TemplateParamValuesTreeView.SelectedNode = tn;
			}
		}

		private void ParamValuesTreeView_ItemDrag(object sender, ItemDragEventArgs e)
		{
			GoodsParam gp = (GoodsParam)(e.Item as TreeNode).Tag;
			ParamValuesTreeView.DoDragDrop(gp, DragDropEffects.Move);
		}

		private void TemplateParamValuesTreeView_ItemDrag(object sender, ItemDragEventArgs e)
		{
			TreeNode item = e.Item as TreeNode;
			if (item.Level != 2) return;
			TemplateParamAssociation tpa = (TemplateParamAssociation)(e.Item as TreeNode).Tag;
			ParamValuesTreeView.DoDragDrop(tpa, DragDropEffects.Move);
		}
		private void TemplateParamValuesTreeView_DragOver(object sender, DragEventArgs e)
		{
			Point pt = TemplateParamValuesTreeView.PointToClient(new Point(e.X, e.Y));
			TreeNode tNode = TemplateParamValuesTreeView.GetNodeAt(pt);
			if (tNode == null || tNode.Level == 0) e.Effect = DragDropEffects.None;
			else e.Effect = DragDropEffects.Move;

		}

		private void ParamValuesTreeView_DragOver(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Move;
		}
		private void TemplateParamValuesTreeView_DragDrop(object sender, DragEventArgs e)
		{
			Point pt = TemplateParamValuesTreeView.PointToClient(new Point(e.X, e.Y));
			TreeNode dest = TemplateParamValuesTreeView.GetNodeAt(pt);
			string name = null, value = null, unit = null;
			if (e.Data.GetDataPresent(typeof(GoodsParam)))
			{
				GoodsParam source = (GoodsParam)e.Data.GetData(typeof(GoodsParam));
				if (dest.Level == 1)
				{
					name = (dest.Tag as TemplateParamValue).Name;
				}
				else if (dest.Level == 2)
				{
					name = (dest.Tag as TemplateParamAssociation).Name;
				}
				else return;
				value = source.Value;
				unit = source.Unit;
			}
			else if (e.Data.GetDataPresent(typeof(TemplateParamAssociation)))
			{
				TemplateParamAssociation source = (TemplateParamAssociation)e.Data.GetData(typeof(TemplateParamAssociation));
				if (dest.Level == 1)
				{
					name = (dest.Tag as TemplateParamValue).Name;
				}
				else if (dest.Level == 2)
				{
					name = (dest.Tag as TemplateParamAssociation).Name;
				}
				else return;
				if (string.Compare(source.Name, name, true) == 0) return;
				value = source.Value;
				unit = source.Unit;
			}
			string sql = @"
IF EXISTS (SELECT * FROM t_TemplateParamAssociation WHERE OID = {0} And paramTypeOID = {1} and value = {3} and unit = {4}) Begin
	UPDATE t_TemplateParamAssociation Set name = {2} WHERE OID = {0} And paramTypeOID = {1} and value = {3} and unit = {4}
End
else Begin
	INSERT INTO t_TemplateParamAssociation VALUES({0}, {1}, {2}, {3}, {4})
End
";
			op.ExecuteCommand(string.Format(sql, ThemeOID.ToSqlString(), ObjectOID.ToSqlString(), name.ToSqlString(), value.ToSqlString(), unit.ToSqlString()));
			op.ClearCache();
			LoadData();
		}

		private void ParamValuesTreeView_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(TemplateParamAssociation)))
			{
				TemplateParamAssociation source = (TemplateParamAssociation)e.Data.GetData(typeof(TemplateParamAssociation));
				string sql = @"
DELETE FROM t_TemplateParamAssociation WHERE OID = {0} And paramTypeOID = {1} and name = {2} and value = {3} and unit = {4}
";
				op.ExecuteCommand(string.Format(sql, ThemeOID.ToSqlString(), ObjectOID.ToSqlString(), source.Name.ToSqlString(), source.Value.ToSqlString(), source.Unit.ToSqlString()));
				op.ClearCache();
				LoadData();
			}
		}

		private void GoodsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			TreeNode selectedNode = ParamValuesTreeView.SelectedNode;
			if (selectedNode != null)
			{
				GoodsParam gp = (GoodsParam)(selectedNode).Tag;
				ShortGoodsList fm = new ShortGoodsList { ThemeOID = this.ThemeOID, ParamTypeOID = this.ObjectOID, Value = gp.Value, Unit = gp.Unit };
				fm.ShowDialog(this);
				LoadData();
			}
		}

		private void GoodsWithoutParam_Click(object sender, EventArgs e)
		{
			ShortGoodsList fm = new ShortGoodsList { ThemeOID = this.ThemeOID, ParamTypeOID = this.ObjectOID };
			fm.ShowDialog(this);
			LoadData();
		}

	}

	public class TemplateParamValue
	{
		public Guid OID { get; set; }
		public Guid ParamType { get; set; }
		public string Name { get; set; }
		public int OrdValue { get; set; }
		public bool IsNew { get; set; }
	}
	public class TemplateParamAssociation
	{
		public Guid OID { get; set; }
		public Guid ParamType { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
		public string Unit { get; set; }
	}
	public class GoodsParam
	{
		public string Value { get; set; }
		public string Unit { get; set; }
	}
}
