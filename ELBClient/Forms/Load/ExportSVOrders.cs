﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using Ecommerce.Extensions;
using MetaData;

namespace ELBClient.Forms.Load
{
	public partial class ExportSVOrders : ListForm
	{
		public int StartFrom { get; set; }
		public int ShopType { get; set; }
		public string ShopName { get; set; }
		private DataSetISM _ds;
		private BusinessProvider.BusinessProvider bp = Classes.ServiceUtility.BusinessProvider;
		private BusinessProvider.ExceptionISM exISM = null;
		public ExportSVOrders()
		{
			InitializeComponent();
		}

		private void LoadFile_Click(object sender, EventArgs e)
		{
			string sql = @"
SELECT * FROM Orders
WHERE 
	orderDate BETWEEN {0} And {1}

SELECT oi.* FROM Orders_Items oi
	inner join Orders o on oi.OrderId = o.id and o.orderDate BETWEEN {0} And {1} 
";
			_ds = new DataSetISM(bp.GetDataSetISM(ShopType, string.Format(sql, ToSqlString(dtpDateBegin.Value.Date), ToSqlString(dtpDateEnd.Value.Date.AddDays(1))), out exISM));
			if (exISM != null) {
				ShowError(exISM.LiteralMessage);
				return;
			}
			dgOrders.SetDataBinding(_ds, "Table");
			dgOrders.DisableNewDel();
		}

		private void LoadGoods_Load(object sender, EventArgs e)
		{
			Text = string.Format("Выгрузка заказов - {0}", ShopName);
			dtpDateBegin.Value = DateTime.Today.AddDays(1 - DateTime.Today.Day);
			dtpDateEnd.Value = DateTime.Today;
		}

		private void ExportToFile_Click(object sender, EventArgs e)
		{
			if (SaveList.ShowDialog(this) == DialogResult.OK)
			{
				Regex re = new Regex(@"\s+", RegexOptions.Singleline);
				Regex reFileName = new Regex(@"(.*)\.([^\.]*)", RegexOptions.Singleline);
				StringBuilder sb = new StringBuilder();
				var items = (DataSet)dgOrders.DataSource;
				string fileNameOrders = SaveList.FileName;
				string fileNameItems = reFileName.Replace(SaveList.FileName, "$1_Items.$2");
				using (StreamWriter wrt = new StreamWriter(fileNameOrders, false, Encoding.GetEncoding(1251)))
				{
					wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
							.Select(column => re.Replace(column.HeaderText, " "))
							.Intersperse("\t")
							.Aggregate(sb, (builder, str) => { builder.Append(str); return builder; }, builder => { string result = builder.ToString(); builder.Length = 0; return result; }));
					items.Tables[0].AsEnumerable().ToList().ForEach(row =>
					{
						wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
							.Select(column => re.Replace(row[column.MappingName].ToString(), " "))
							.Intersperse("\t")
							.Aggregate(sb, (builder, str) => { builder.Append(str); return builder; }, builder => { string result = builder.ToString(); builder.Length = 0; return result; }));
					});
				}
				using (StreamWriter wrt = new StreamWriter(fileNameItems, false, Encoding.GetEncoding(1251)))
				{
					wrt.WriteLine(items.Tables[1].Columns.Cast<DataColumn>()
							.Where(column => string.Compare("TStamp", column.Caption, true) != 0)
							.Select(column => re.Replace(column.Caption, " "))
							.Intersperse("\t")
							.Aggregate(sb, (builder, str) => { builder.Append(str); return builder; }, builder => { string result = builder.ToString(); builder.Length = 0; return result; }));
					items.Tables[1].AsEnumerable().ToList().ForEach(row =>
					{
						wrt.WriteLine(items.Tables[1].Columns.Cast<DataColumn>()
							.Where(column => string.Compare("TStamp", column.Caption, true) != 0)
							.Select(column => re.Replace(row[column.Caption].ToString(), " "))
							.Intersperse("\t")
							.Aggregate(sb, (builder, str) => { builder.Append(str); return builder; }, builder => { string result = builder.ToString(); builder.Length = 0; return result; }));
					});
				}
			}
		}

	}
}
