﻿namespace ELBClient.Forms.Load
{
	partial class EditFilter
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.TemplateParamValuesTreeView = new System.Windows.Forms.TreeView();
			this.TemplateMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.AddFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DeleteFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.ParamValuesTreeView = new System.Windows.Forms.TreeView();
			this.ParamMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.GoodsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panel2 = new System.Windows.Forms.Panel();
			this.GoodsWithoutParam = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.TemplateMenu.SuspendLayout();
			this.panel1.SuspendLayout();
			this.ParamMenu.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.TemplateParamValuesTreeView);
			this.splitContainer1.Panel1.Controls.Add(this.panel1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.ParamValuesTreeView);
			this.splitContainer1.Panel2.Controls.Add(this.panel2);
			this.splitContainer1.Size = new System.Drawing.Size(762, 454);
			this.splitContainer1.SplitterDistance = 381;
			this.splitContainer1.TabIndex = 1;
			// 
			// TemplateParamValuesTreeView
			// 
			this.TemplateParamValuesTreeView.AllowDrop = true;
			this.TemplateParamValuesTreeView.ContextMenuStrip = this.TemplateMenu;
			this.TemplateParamValuesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TemplateParamValuesTreeView.LabelEdit = true;
			this.TemplateParamValuesTreeView.Location = new System.Drawing.Point(0, 33);
			this.TemplateParamValuesTreeView.Name = "TemplateParamValuesTreeView";
			this.TemplateParamValuesTreeView.Size = new System.Drawing.Size(381, 421);
			this.TemplateParamValuesTreeView.TabIndex = 0;
			this.TemplateParamValuesTreeView.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.TemplateParamValuesTreeView_BeforeLabelEdit);
			this.TemplateParamValuesTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.TemplateParamValuesTreeView_AfterLabelEdit);
			this.TemplateParamValuesTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.TemplateParamValuesTreeView_ItemDrag);
			this.TemplateParamValuesTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.TemplateParamValuesTreeView_DragDrop);
			this.TemplateParamValuesTreeView.DragOver += new System.Windows.Forms.DragEventHandler(this.TemplateParamValuesTreeView_DragOver);
			this.TemplateParamValuesTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TemplateParamValuesTreeView_MouseDown);
			// 
			// TemplateMenu
			// 
			this.TemplateMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddFilterToolStripMenuItem,
            this.DeleteFilterToolStripMenuItem});
			this.TemplateMenu.Name = "TemplateMenu";
			this.TemplateMenu.Size = new System.Drawing.Size(171, 48);
			this.TemplateMenu.Opening += new System.ComponentModel.CancelEventHandler(this.TemplateMenu_Opening);
			// 
			// AddFilterToolStripMenuItem
			// 
			this.AddFilterToolStripMenuItem.Name = "AddFilterToolStripMenuItem";
			this.AddFilterToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.AddFilterToolStripMenuItem.Text = "Добавить фильтр";
			this.AddFilterToolStripMenuItem.Click += new System.EventHandler(this.AddFilterToolStripMenuItem_Click);
			// 
			// DeleteFilterToolStripMenuItem
			// 
			this.DeleteFilterToolStripMenuItem.Name = "DeleteFilterToolStripMenuItem";
			this.DeleteFilterToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.DeleteFilterToolStripMenuItem.Text = "Удалить фильтр";
			this.DeleteFilterToolStripMenuItem.Click += new System.EventHandler(this.DeleteFilterToolStripMenuItem_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(381, 33);
			this.panel1.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Параметры фильтра";
			// 
			// ParamValuesTreeView
			// 
			this.ParamValuesTreeView.AllowDrop = true;
			this.ParamValuesTreeView.ContextMenuStrip = this.ParamMenu;
			this.ParamValuesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ParamValuesTreeView.Location = new System.Drawing.Point(0, 33);
			this.ParamValuesTreeView.Name = "ParamValuesTreeView";
			this.ParamValuesTreeView.Size = new System.Drawing.Size(377, 421);
			this.ParamValuesTreeView.TabIndex = 0;
			this.ParamValuesTreeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ParamValuesTreeView_ItemDrag);
			this.ParamValuesTreeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.ParamValuesTreeView_DragDrop);
			this.ParamValuesTreeView.DragOver += new System.Windows.Forms.DragEventHandler(this.ParamValuesTreeView_DragOver);
			// 
			// ParamMenu
			// 
			this.ParamMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GoodsToolStripMenuItem});
			this.ParamMenu.Name = "ParamMenu";
			this.ParamMenu.Size = new System.Drawing.Size(163, 26);
			// 
			// GoodsToolStripMenuItem
			// 
			this.GoodsToolStripMenuItem.Name = "GoodsToolStripMenuItem";
			this.GoodsToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.GoodsToolStripMenuItem.Text = "Список товаров";
			this.GoodsToolStripMenuItem.Click += new System.EventHandler(this.GoodsToolStripMenuItem_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.GoodsWithoutParam);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(377, 33);
			this.panel2.TabIndex = 1;
			// 
			// GoodsWithoutParam
			// 
			this.GoodsWithoutParam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.GoodsWithoutParam.Location = new System.Drawing.Point(219, 3);
			this.GoodsWithoutParam.Name = "GoodsWithoutParam";
			this.GoodsWithoutParam.Size = new System.Drawing.Size(146, 23);
			this.GoodsWithoutParam.TabIndex = 2;
			this.GoodsWithoutParam.Text = "Товары без параметра";
			this.GoodsWithoutParam.UseVisualStyleBackColor = true;
			this.GoodsWithoutParam.Click += new System.EventHandler(this.GoodsWithoutParam_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(119, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Значения параметров";
			// 
			// EditFilter
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(762, 454);
			this.Controls.Add(this.splitContainer1);
			this.Name = "EditFilter";
			this.Text = "Редактирование значений фильтра";
			this.Load += new System.EventHandler(this.EditFilter_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.TemplateMenu.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ParamMenu.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TreeView TemplateParamValuesTreeView;
		private System.Windows.Forms.TreeView ParamValuesTreeView;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ContextMenuStrip TemplateMenu;
		private System.Windows.Forms.ToolStripMenuItem AddFilterToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem DeleteFilterToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip ParamMenu;
		private System.Windows.Forms.ToolStripMenuItem GoodsToolStripMenuItem;
		private System.Windows.Forms.Button GoodsWithoutParam;
	}
}