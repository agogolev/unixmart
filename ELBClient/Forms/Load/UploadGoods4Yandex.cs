﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using ColumnMenuExtender;
using Ecommerce.Extensions;
using HttpReader;
using MetaData;

namespace ELBClient.Forms.Load
{
    public partial class UploadGoods4Yandex : ListForm
    {
        public Dictionary<int, string> ShopTypes;

        private string currentUrl;

        public UploadGoods4Yandex()
        {
            InitializeComponent();
        }

        public DataSetISM DataSource { get; set; }

        private void UploadGoods4Yandex_Load(object sender, EventArgs e)
        {
            LoadLookup();
        }

        private void LoadLookup()
        {
            const string sql = @"
SELECT
	shopType,
	name,
	address
FROM
	t_TypeShop
ORDER BY
	shopType
";
            var ds = new DataSetISM(lp.GetDataSetSql(sql));
            cbShopType.AddItems(
                ds.Table.AsEnumerable()
                    .Select(k => new ListItem {Value = k.Field<int>("shopType"), Text = k.Field<string>("name")})
                    .ToArray());
            ShopTypes = ds.Table.AsEnumerable().ToDictionary(row => row.Field<int>("shopType"),
                row => string.Format("{0}/yml/goodsListAdmin.xml", row.Field<string>("address")));
        }

        private void cbShopType_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentUrl = ShopTypes[cbShopType.SelectedValue];
            var read = new HTTPReader(currentUrl, "", "");
            read.Request();
            XDocument doc = XDocument.Parse(read.Response);
            DataSource = new DataSetISM();
            var tbl = new DataTable("Table");
            DataSource.Tables.Add(tbl);
            tbl.Columns.Add("modelId", typeof (int));
            tbl.Columns.Add("vendor");
            tbl.Columns.Add("model");
            if (doc.Root != null)
            {
                XElement xElement = doc.Root.Element("shop");
                if (xElement != null)
                {
                    XElement element = xElement.Element("offers");
                    if (element != null)
                        foreach (XElement e1 in element.Elements("offer"))
                        {
                            tbl.Rows.Add(new object[]
                            {
                                int.Parse(e1.Attributes("modelId").Select(a => a.Value).Single()),
                                e1.Elements("vendor").Select(e2 => e2.Value).Single(),
                                e1.Elements("model").Select(e2 => e2.Value).Single()
                            });
                        }
                }
            }
            GoodsGrid.SetDataBinding(DataSource, "Table");
            GoodsGrid.DisableNewDel();
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            if (SaveCsv.ShowDialog(this) == DialogResult.OK)
            {
                var re = new Regex(@"\s+", RegexOptions.Singleline);
                using (var wrt = new StreamWriter(SaveCsv.FileName, false, Encoding.GetEncoding(1251)))
                {
                    wrt.WriteLine(extendedDataGridTableStyle1.GridColumnStyles.Cast<DataGridColumnStyle>()
                        .Select(column => re.Replace(column.MappingName, " "))
                        .Intersperse("\t")
                        .Aggregate(new StringBuilder(), (builder, str) =>
                        {
                            builder.Append(str);
                            return builder;
                        }, builder =>
                        {
                            string result = builder.ToString();
                            builder.Length = 0;
                            return result;
                        }));
                    DataSource.Table.AsEnumerable().ToList().ForEach(row => wrt.WriteLine(
                        row.ItemArray
                            .Select(item => string.Format("\"{0}\"", re.Replace(item.ToString(), " ")))
                            .Intersperse("\t")
                            .Aggregate(new StringBuilder(), (builder, str) =>
                            {
                                builder.Append(str);
                                return builder;
                            }, builder =>
                            {
                                string result = builder.ToString();
                                builder.Length = 0;
                                return result;
                            })));
                }
                MessageBox.Show(@"Экспорт завершён");
            }
        }
    }
}