﻿using System;
using System.Text.RegularExpressions;
using System.Data;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using MetaData;
using ELBClient.Classes;

namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmLoadParams.
	/// </summary>
	public partial class LoadImages : GenericForm
	{
		private Hashtable denyGoods = new Hashtable();
		private Hashtable acceptGoods = new Hashtable();
		private Hashtable paramTypes = new Hashtable();

		/// <summary>
		/// Required designer variable.
		/// </summary>


		public LoadImages()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private string ModifyName(string goodsName)
		{
			return goodsName.Trim().Replace("=", "/");
		}
		private void btnLoadImages_Click(object sender, System.EventArgs e)
		{
			txtLog.Text = "";
			if (odFiles.ShowDialog(this) == DialogResult.OK)
			{
				DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(odFiles.FileName));
				if (!di.Exists)
				{
					ShowError("Каталог не существует");
					return;
				}
				FileInfo[] files = di.GetFiles("*.*");
				int i = 0;
				foreach (FileInfo fi in files)
				{
					if (ProcessFile(fi.DirectoryName + '\\' + fi.Name))
					{
						i++;
						lbCounter.Text = i.ToString();
						lbCounter.Refresh();
					}
				}
				LogInfo("Загружено " + i + " файлов из " + files.Length);
				op.ClearCache();
			}
		}
		private bool ProcessFile(string fileName)
		{
			string shortName = Path.GetFileName(fileName);
			string ext = Path.GetExtension(fileName).ToUpper();
			if (ext != ".GIF" && ext != ".JPG")
			{
				LogInfo("Не обрабатываемый тип картинки " + shortName + " (должны быть gif или jpg)");
				return false;
			}
			string name = Path.GetFileNameWithoutExtension(fileName);
			Match m;
			Regex re = null;
			int postavshik_id = 0;
			string ordNum = "", imageType = "";
			re = new Regex(@"^(\d+)(_\d*)?(s|big|m)$", RegexOptions.IgnoreCase);
			m = re.Match(name);
			if (m.Success)
			{
			    postavshik_id = int.Parse(m.Groups[1].Value);
				imageType = m.Groups[3].Value.ToLower();
				ordNum = m.Groups[2].Value;
			}
			else
			{
				LogInfo("Неизвестная сигнатура файла " + shortName);
				return false;
			}

			string ordValue = "";
			if (imageType == "s") ordValue = "Для табличного вывода";
			else if (imageType == "m") ordValue = "Для карточки товара";
			else if (imageType == "big") ordValue = "Для popup";
			else
			{
				LogInfo("Неизвестный тип картинки " + imageType + ordNum + ", файл: " + shortName);
				return false;
			}
			if (ordNum != "") ordValue += " " + ordNum.Substring(1).Trim();
			string sql = @"
SELECT
	OID
FROM
	t_Goods
WHERE
	postavshik_id = " + ToSqlString(postavshik_id);
			string errorFind = "Товар " + postavshik_id + " не найден в базе";

			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			if (ds.Table.Rows.Count == 0)
			{
				LogInfo(errorFind);
				return false;
			}
			try
			{
				bool success = SaveImage((Guid)ds.Table.Rows[0]["OID"], fileName, ordValue);
				if (!success) return false;
			}
			catch (Exception e)
			{
				LogInfo(e.Message);
				return false;
			}
			return true;
		}
		private bool SaveImage(Guid goodsOID, string fileName, string ordValue)
		{
			string sql = @"
SELECT oi.binData FROM 
	t_ObjectImage oi
WHERE oi.OID = '" + goodsOID + @"' And ordValue = " + ToSqlString(ordValue) + @"
";
			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			Guid imageOID = Guid.Empty;
			bool isLinked = false;
			if (ds.Table.Rows.Count != 0)
			{
				imageOID = (Guid)ds.Table.Rows[0]["binData"];
				isLinked = true;
			}
			byte[] content;
			string name = Path.GetFileNameWithoutExtension(fileName);
			string szFileName = Path.GetFileName(fileName);
			string mimeType = MetaData.MimeTypeUtil.CheckType(fileName);
			if (mimeType != "")
			{
				Size size = MetaData.Sizer.GetSize(fileName, mimeType);
				CBinaryData img = new CBinaryData();
				if (imageOID != Guid.Empty) img.OID = imageOID;
				img.Name = name;
				img.MimeType = mimeType;
				img.Width = size.Width;
				img.Height = size.Height;
				img.FileName = szFileName;

				FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
				content = new byte[fs.Length];
				fs.Read(content, 0, (int)fs.Length);
				fs.Close();
				img.Data = content;

				ELBClient.ObjectProvider.ExceptionISM exISM = null;
				string xmlObj = op.SaveObject(img.SaveXml(), out exISM);
				if (exISM != null)
				{
					LogInfo(exISM.LiteralMessage);
				}
				else
				{
					if (!isLinked)
					{
						img.LoadXml(xmlObj);
						imageOID = img.OID;
						sql = @"INSERT INTO t_ObjectImage VALUES(" + ToSqlString(goodsOID) + ", " + ToSqlString(ordValue) + ", " + ToSqlString(imageOID) + ")";
						try
						{
							op.ExecuteCommand(sql);
						}
						catch (Exception e)
						{
							LogInfo(e.Message);
							return false;
						}
					}
				}
			}
			else
			{
				LogInfo("Пустой mimeType");
				return false;
			}
			return true;
		}

		private bool SaveImage(DataSetISM dsGoods, string fileName, string ordValue)
		{
			foreach (DataRow dr in dsGoods.Table.Rows)
			{
				Guid goodsOID = (Guid)dr["OID"];
				string sql = @"
SELECT oi.binData FROM 
	t_ObjectImage oi
WHERE oi.OID = '" + goodsOID + @"' And ordValue = " + ToSqlString(ordValue) + @"
";
				DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
				Guid imageOID = Guid.Empty;
				bool isLinked = false;
				if (ds.Table.Rows.Count != 0 && ds.Table.Rows[0]["binData"] != DBNull.Value)
				{
					imageOID = (Guid)ds.Table.Rows[0]["binData"];
					isLinked = true;
				}
				byte[] content;
				string name = Path.GetFileNameWithoutExtension(fileName);
				string szFileName = Path.GetFileName(fileName);
				string mimeType = MetaData.MimeTypeUtil.CheckType(fileName);
				if (mimeType != "")
				{
					Size size = MetaData.Sizer.GetSize(fileName, mimeType);
					CBinaryData img = new CBinaryData();
					if (imageOID != Guid.Empty) img.OID = imageOID;
					img.Name = name;
					img.MimeType = mimeType;
					img.Width = size.Width;
					img.Height = size.Height;
					img.FileName = szFileName;

					FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
					content = new byte[fs.Length];
					fs.Read(content, 0, (int)fs.Length);
					fs.Close();
					img.Data = content;

					ELBClient.ObjectProvider.ExceptionISM exISM = null;
					string xmlObj = op.SaveObject(img.SaveXml(), out exISM);
					if (exISM != null)
					{
						LogInfo(exISM.LiteralMessage);
					}
					else
					{
						if (!isLinked)
						{
							img.LoadXml(xmlObj);
							imageOID = img.OID;
							sql = @"INSERT INTO t_ObjectImage VALUES(" + ToSqlString(goodsOID) + ", " + ToSqlString(ordValue) + ", " + ToSqlString(imageOID) + ")";
							try
							{
								op.ExecuteCommand(sql);
							}
							catch (Exception e)
							{
								LogInfo(e.Message);
								return false;
							}
						}
					}
				}
				else
				{
					LogInfo("Пустой mimeType");
					return false;
				}
			}
			return true;
		}

		private void LogInfo(string message)
		{
			txtLog.Text += message + "\r\n";
			txtLog.Refresh();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
