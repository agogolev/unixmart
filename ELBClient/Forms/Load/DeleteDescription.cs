﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using ColumnMenuExtender;
using MetaData;

namespace ELBClient.Forms.Load
{
    /// <summary>
    ///     Summary description for fmLoadVoucher.
    /// </summary>
    public partial class DeleteDescription : ListForm
    {
        private DataSetISM _ds;
        private int _processedItems;
        private string caption = "Удаление описаний - ";
        private string connStr;


        /// <summary>
        ///     Required designer variable.
        /// </summary>
        public DeleteDescription()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>

        #endregion
        private void LoadLookup()
        {
            dsCodes.Tables[0].Rows.Add(new object[] {"", "Выберите чей код"});
            dsCodes.Tables[0].Rows.Add(new object[] {"expertID", "Код эксперта"});
            dsCodes.Tables[0].Rows.Add(new object[] {"oldiID", "Код ОЛДИ"});
            dsCodes.Tables[0].Rows.Add(new object[] {"svegaID", "Код свега"});
            dsCodes.Tables[0].Rows.Add(new object[] {"stavID", "Код ставрополь"});
            dsCodes.Tables[0].Rows.Add(new object[] {"grafitekID", "Код графитек"});
            dsCodes.Tables[0].Rows.Add(new object[] {"pilotageID", "Код пилотаж"});
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ofdlgExcel.ShowDialog(this) == DialogResult.OK)
            {
                OleDbConnection conn = null;
                DataTable dt = null;
                try
                {
                    connStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%FileName%;Extended Properties=Excel 8.0;";
                    connStr = connStr.Replace("%FileName%", ofdlgExcel.FileName);
                    conn = new OleDbConnection(connStr);
                    conn.Open();
                    dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    Text = caption + Path.GetFileName(ofdlgExcel.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
                finally
                {
                    conn.Close();
                }
                cbSheet.Items.Clear();
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    var sheetName = (string) dr["TABLE_NAME"];
                    var li = new ListItem(i, sheetName);
                    cbSheet.AddItem(li);
                    i++;
                }
                cbSheet.SelectedIndexChanged += cbSheet_SelectedIndexChanged;
                cbSheet.SelectedIndex = 0;
            }
        }

        private void LoadSheet(string name)
        {
            try
            {
                var conn = new OleDbConnection(connStr);
                var cmd = new OleDbCommand("", conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + name + "]";
                var da = new OleDbDataAdapter(cmd);
                _ds = new DataSetISM();
                da.Fill(_ds);
                dgVoucher.DataSource = _ds;
                dgVoucher.DataMember = "table";
                _processedItems = 0;
                ClearControls();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearControls()
        {
            cbCodes.SelectedIndex = 0;
            //udNumber.Value = -1;
        }

        private void fmLoadVoucher_Load(object sender, EventArgs e)
        {
            LoadLookup();
        }

        private void cbSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSheet(cbSheet.SelectedItem.Text);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            string str = CheckConsistency();
            if (str != "")
            {
                ShowError(str);
                return;
            }
            var codeType = (string) cbCodes.SelectedValue;
            _processedItems = 1;
            try
            {
                var colNumber = (int) udNumber.Value;
                foreach (DataRow dr in _ds.Table.Rows)
                {
                    if (dr[colNumber] == DBNull.Value) continue;
                    string number = dr[colNumber].ToString();

                    string sql = @"
	DELETE FROM t_GoodsParams
	FROM t_GoodsParams gp 
		inner join t_Goods g on gp.OID = g.OID
	WHERE
		g." + codeType + @" = " + ToSqlString(number) + @"
";
                    op.ExecuteCommand(sql);
                    _processedItems++;
                }
                MessageBox.Show(this, "Импорт завершён, обработано " + (_processedItems - 1) + " записей.");
            }
            catch (Exception ex)
            {
                ShowError("Строка: " + _processedItems + "\r\n" + ex.Message);
            }
        }

        private string CheckConsistency()
        {
            if (cbCodes.SelectedIndex == 0) return "Выберите чей код";
            if (udNumber.Value == -1) return "Укажите колонку номера";
            return "";
        }
    }
}