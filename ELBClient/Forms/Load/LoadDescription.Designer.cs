﻿namespace ELBClient.Forms.Load
{
	partial class LoadDescription
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.ImportDescriptions = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.StartFromText = new System.Windows.Forms.TextBox();
			this.Counter = new System.Windows.Forms.Label();
			this.LoadFile = new System.Windows.Forms.Button();
			this.dgGoods = new ColumnMenuExtender.DataGridISM();
			this.LoadExcel = new System.Windows.Forms.OpenFileDialog();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.ImportDescriptions);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.StartFromText);
			this.panel1.Controls.Add(this.Counter);
			this.panel1.Controls.Add(this.LoadFile);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(892, 59);
			this.panel1.TabIndex = 0;
			// 
			// ImportDescriptions
			// 
			this.ImportDescriptions.Location = new System.Drawing.Point(417, 11);
			this.ImportDescriptions.Name = "ImportDescriptions";
			this.ImportDescriptions.Size = new System.Drawing.Size(125, 40);
			this.ImportDescriptions.TabIndex = 12;
			this.ImportDescriptions.Text = "Импорт описаний";
			this.ImportDescriptions.UseVisualStyleBackColor = true;
			this.ImportDescriptions.Click += new System.EventHandler(this.ImportDescriptions_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(246, 15);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 13);
			this.label2.TabIndex = 11;
			this.label2.Text = "Начиная с:";
			// 
			// StartFromText
			// 
			this.StartFromText.Location = new System.Drawing.Point(311, 11);
			this.StartFromText.Name = "StartFromText";
			this.StartFromText.Size = new System.Drawing.Size(100, 20);
			this.StartFromText.TabIndex = 10;
			// 
			// Counter
			// 
			this.Counter.Location = new System.Drawing.Point(133, 10);
			this.Counter.Name = "Counter";
			this.Counter.Size = new System.Drawing.Size(110, 23);
			this.Counter.TabIndex = 4;
			this.Counter.Text = "Обработано: 0";
			this.Counter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LoadFile
			// 
			this.LoadFile.Location = new System.Drawing.Point(12, 12);
			this.LoadFile.Name = "LoadFile";
			this.LoadFile.Size = new System.Drawing.Size(114, 39);
			this.LoadFile.TabIndex = 0;
			this.LoadFile.Text = "Загрузить файл импорта";
			this.LoadFile.UseVisualStyleBackColor = true;
			this.LoadFile.Click += new System.EventHandler(this.LoadFile_Click);
			// 
			// dgGoods
			// 
			this.dgGoods.AllowSorting = false;
			this.dgGoods.BackgroundColor = System.Drawing.SystemColors.Window;
			this.dgGoods.ColumnDragEnabled = true;
			this.dgGoods.DataMember = "";
			this.dgGoods.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgGoods.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgGoods.Location = new System.Drawing.Point(0, 59);
			this.dgGoods.Name = "dgGoods";
			this.dgGoods.Order = null;
			this.dgGoods.ReadOnly = true;
			this.dgGoods.Size = new System.Drawing.Size(892, 398);
			this.dgGoods.StockClass = "CGoods";
			this.dgGoods.StockNumInBatch = 0;
			this.dgGoods.TabIndex = 16;
			// 
			// LoadExcel
			// 
			this.LoadExcel.FileName = "openFileDialog1";
			// 
			// LoadDescription
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(892, 457);
			this.Controls.Add(this.dgGoods);
			this.Controls.Add(this.panel1);
			this.Name = "LoadDescription";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Загрузка описаний";
			this.Load += new System.EventHandler(this.LoadGoods_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgGoods)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private ColumnMenuExtender.DataGridISM dgGoods;
		private System.Windows.Forms.Button LoadFile;
		private System.Windows.Forms.OpenFileDialog LoadExcel;
		private System.Windows.Forms.Label Counter;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox StartFromText;
		private System.Windows.Forms.Button ImportDescriptions;
	}
}