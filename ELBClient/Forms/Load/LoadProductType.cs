﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using MetaData;
using ELBClient.ObjectProvider;
using Excel;

namespace ELBClient.Forms.Load
{
	public partial class LoadProductType : ListForm
	{
		public int StartFrom { get; set; }
		private DataSet _ds;
		private int _processedItems = 0;
		public LoadProductType()
		{
			InitializeComponent();
			StartFrom = 1;
		}

		private void LoadFile_Click(object sender, EventArgs e)
		{
			if (LoadExcel.ShowDialog(this) == DialogResult.OK)
			{
				try
				{
					using (FileStream stream = File.Open(LoadExcel.FileName, FileMode.Open, FileAccess.Read))
					{
						using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
						{
							excelReader.Read();
							_ds = new DataSetISM();
							DataTable dt = new DataTable("Table");							
							_ds.Tables.Add(dt);
							int i = 0;
							string name = null;
							do
							{
								name = excelReader.GetString(i);
								dt.Columns.Add(name);
								i++;
							}
							while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
							while (excelReader.Read())
							{
								DataRow dr = dt.NewRow();
								for (int j = 0; j < dt.Columns.Count; j++)
								{
									dr[j] = excelReader.GetString(j);
								}
								dt.Rows.Add(dr);
							}
							_ds.AcceptChanges();
						}
					}

					dgGoods.DataSource = _ds;
					dgGoods.DataMember = "Table";
					_processedItems = 0;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					return;
				}
			}
		}

		private void ImportProductType_Click(object sender, EventArgs e)
		{
			if (_ds == null)
			{
				ShowError("Загрузите файл");
				return;
			}
			
			string sql = @"

UPDATE t_Goods SET productType = @productType
WHERE postavshik_id = @id
";
			_processedItems = StartFrom - 1;
			for (int i = StartFrom - 1; i < _ds.Tables[0].Rows.Count; i++)
			{
				DataRow dr = _ds.Tables[0].Rows[i];
				int id = int.Parse(dr[0].ToString().Trim());
				string productType = "null";
				if (dr[1] != null && dr[1] != DBNull.Value && !string.IsNullOrWhiteSpace((string)dr[1]))
					productType = dr[1].ToString().Trim();
				

				op.ExecuteCommandParams(sql, new [] {
					new NameValuePair { Name = "id", Value = id.ToString()},
					new NameValuePair { Name = "productType", Value = productType}
				}, false);
				Counter.Text = "Обработано: " + ++_processedItems;
				Counter.Update();
			}
			MessageBox.Show(this, @"Импорт типов товаров завершён");
		}

		private void LoadGoods_Load(object sender, EventArgs e)
		{
			Text = "Загрузка типов товаров";
			StartFromText.DataBindings.Add("Text", this, "StartFrom");
		}

	}
}
