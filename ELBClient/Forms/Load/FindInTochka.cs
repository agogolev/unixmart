﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MetaData;
using ELBClient.Classes;

namespace ELBClient.Forms.Load
{
	public partial class FindInTochka : ListForm
	{
		public DataSetISM OurData { get; set; }
		public DataSetISM TochkaData { get; set; }
		public FindInTochka()
		{
			InitializeComponent();
		}


		private void FindInTochka_Load(object sender, EventArgs e)
		{
			LoadLookup();
			extendedDataGridSelectorColumn1.ButtonClick += new ColumnMenuExtender.CellEventHandler(extendedDataGridSelectorColumn1_ButtonClick);
		}

		

		private void LoadLookup()
		{
			string sql = @"SELECT OID, companyName FROM t_Company ORDER BY companyName";
			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			List<CCompany> items = (from row in ds.Table.AsEnumerable()
															orderby row.Field<string>("companyName")
															select new CCompany { OID = row.Field<Guid>("OID"), CompanyName = row.Field<string>("companyName") }).ToList();
			items.Insert(0, new CCompany { CompanyName = "---" });
			BrandListTochka.DataSource = items;
			BrandListTochka.DisplayMember = "CompanyName";
			BrandListTochka.ValueMember = "OID";
			items = (from row in ds.Table.AsEnumerable()
							 orderby row.Field<string>("companyName")
							 select new CCompany { OID = row.Field<Guid>("OID"), CompanyName = row.Field<string>("companyName") }).ToList();
			items.Insert(0, new CCompany { CompanyName = "---" });
			BrandListOur.DataSource = items;
			BrandListOur.DisplayMember = "CompanyName";
			BrandListOur.ValueMember = "OID";
		}

		private void ShowTochka_Click(object sender, EventArgs e)
		{
			LoadTochka();
		}

		private void LoadTochka()
		{
			string sql = @"
SELECT
	itemId,
	productName,
	shortName,
	isLinked = convert(bit, 0),
	OID,
	ourGoodsName = dbo.NK(OID)
FROM
	ProductItems
WHERE
	1=1
";
			if (NotLinkedTochka.Checked)
			{
				sql += @"
	and OID is null";
			}
			if ((Guid)BrandListTochka.SelectedValue != Guid.Empty)
			{
				sql += @"
	and manufOID = " + ToSqlString(BrandListTochka.SelectedValue);
			}
			if (NameTochka.Text != "")
			{
				sql += @"
	and productName like " + ToSqlString("%" + NameTochka.Text.Trim() + "%");
			}
			sql += @"
ORDER BY
	productName";
			TochkaData = new DataSetISM(lp.GetDataSetSql(sql));
			if (TochkaData.Table.Rows.Count == 0)
			{
				ShowError("Ничего не найдено");
			}
			TochkaProducts.SetDataBinding(TochkaData, "Table");
			TochkaProducts.DisableNewDel();
		}

		private void ShowOur_Click(object sender, EventArgs e)
		{
			LoadOur();
		}

		private void LoadOur()
		{
			string sql = @"
SELECT
	OID,
	model,
	manufacturerName = dbo.NK(manufacturer),
	isLinked = convert(bit, 0)
FROM
	t_Goods
WHERE
	1=1
";
			if (NotLinkedOur.Checked)
			{
				sql += @"
	and NOT OID in (SELECT OID FROM ProductItems WHERE NOT OID is null)";
			}
			if ((Guid)BrandListOur.SelectedValue != Guid.Empty)
			{
				sql += @"
	and manufacturer = " + ToSqlString(BrandListOur.SelectedValue);
			}
			if (NameOur.Text != "")
			{
				sql += @"
	and Model like " + ToSqlString("%" + NameOur.Text.Trim() + "%");
			}
			sql += @"
ORDER BY
	Model";

			OurData = new DataSetISM(lp.GetDataSetSql(sql));
			if (OurData.Table.Rows.Count == 0) {
				ShowError("Ничего не найдено");
			}
			OurGoods.SetDataBinding(OurData, "Table");
			OurGoods.DisableNewDel();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			string sql = @"
UPDATE
	ProductItems
	Set OID = {0}
WHERE
	itemId = {1}
";
			DataRow[] ourGoods = OurData.Table.Select("IsLinked=1");
			if (ourGoods.Length == 0) {
				ShowError("Выберите наш товар");
				return;
			}
			Guid OID = (Guid)ourGoods[0]["OID"];
			DataRow[] tochkaGoods = TochkaData.Table.Select("IsLinked=1");
			if (tochkaGoods.Length == 0)
			{
				ShowError("Выберите товар из БД Точка");
				return;
			}
			foreach (DataRow dr in tochkaGoods)
			{
				int itemId = (int)dr["itemId"];
				op.ExecuteCommand(string.Format(sql, ToSqlString(OID), ToSqlString(itemId)));
			}
			LoadOur();
			LoadTochka();
		}
		void extendedDataGridSelectorColumn1_ButtonClick(object sender, ColumnMenuExtender.DataGridCellEventArgs e)
		{
			DataRowView drv = (DataRowView)e.CurrentRow;
			string sql = "UPDATE ProductItems SET OID = null WHERE itemId = " + drv["itemId"];
			op.ExecuteCommand(sql);
			LoadOur();
			LoadTochka();
		}
	}
}
