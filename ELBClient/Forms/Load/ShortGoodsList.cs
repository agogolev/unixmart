﻿using System;
using System.Data;
using System.Windows.Forms;
using MetaData;
using ELBClient.Forms.Guides;

namespace ELBClient.Forms.Load
{
	public partial class ShortGoodsList : ListForm
	{
		public Guid ThemeOID { get; set; }
		public Guid ParamTypeOID { get; set; }
		public string Value { get; set; }
		public string Unit { get; set; }
		public ShortGoodsList()
		{
			InitializeComponent();
		}

		private void ShortGoodsList_Load(object sender, EventArgs e)
		{
			LoadData();
		}

		public override void LoadData()
		{
			string sql;
			DataSetISM ds;
			if (this.Value != null)
			{
				sql = @"
SELECT
	g.OID,
	o.objectId,
	g.postavshik_id,
	g.model
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID
	inner join t_ThemeMasters tm on g.category = tm.masterOID 
    inner join t_Nodes n on tm.OID = n.object and n.tree = '0e5f5d35-3f4f-4418-a571-c8e9974550b2'
    inner join t_Nodes n1 on n1.tree = n.tree and n.lft BETWEEN n1.lft and n1.rgt And n1.object = @themeOID
	inner join t_GoodsParams gp on g.OID = gp.OID And gp.paramTypeOID = @paramTypeOID and gp.value = @value and gp.unit = @unit
";
				ds = new DataSetISM(lp.GetDataSetSqlParams(sql, new []{ 
					new ListProvider.NameValuePair { Name = "themeOID", Value = this.ThemeOID.ToString()},
					new ListProvider.NameValuePair { Name = "paramTypeOID", Value = this.ParamTypeOID.ToString()},
					new ListProvider.NameValuePair { Name = "value", Value = this.Value},
					new ListProvider.NameValuePair { Name = "unit", Value = this.Unit}
				}));
			}
			else
			{
				sql = @"
SELECT
	g.OID,
	o.objectId,
	g.postavshik_id,
	g.model
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID
	inner join t_ThemeMasters tm on g.category = tm.masterOID 
    inner join t_Nodes n on tm.OID = n.object and n.tree = '0e5f5d35-3f4f-4418-a571-c8e9974550b2'
    inner join t_Nodes n1 on n1.tree = n.tree and n.lft BETWEEN n1.lft and n1.rgt And n1.object = @themeOID
WHERE
	NOT EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID And paramTypeOID = @paramTypeOID)
";
				ds = new DataSetISM(lp.GetDataSetSqlParams(sql, new ListProvider.NameValuePair[]{ 
					new ListProvider.NameValuePair { Name = "themeOID", Value = this.ThemeOID.ToString()},
					new ListProvider.NameValuePair { Name = "paramTypeOID", Value = this.ParamTypeOID.ToString()}
				}));
			}
			GoodsGrid.SetDataBinding(ds, "Table");
			GoodsGrid.DisableNewDel();
		}
		private void GoodsGrid_DoubleClick(object sender, EventArgs e)
		{
			if (GoodsGrid.Hit.Type == DataGrid.HitTestType.Cell || GoodsGrid.Hit.Type == DataGrid.HitTestType.RowHeader)
			{
				DataRow dr = GoodsGrid.GetSelectedRow();
				if (dr != null)
				{
					EditGoods fm = new EditGoods { ObjectOID = (Guid)dr["OID"] };
					fm.ReloadGrid += new ReloadDelegate(LoadData);
					fm.ShowDialog(this);
				}
			}

		}
	}
}
