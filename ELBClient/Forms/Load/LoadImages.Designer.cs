using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;

namespace ELBClient.Forms.Load
{
	public partial class LoadImages
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.OpenFileDialog odFiles;
		private System.Windows.Forms.TextBox txtLog;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label lbCounter;
		private System.Windows.Forms.Button btnLoadImages;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnClose = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.lbCounter = new System.Windows.Forms.Label();
			this.btnLoadImages = new System.Windows.Forms.Button();
			this.odFiles = new System.Windows.Forms.OpenFileDialog();
			this.txtLog = new System.Windows.Forms.TextBox();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 455);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(880, 46);
			this.panel1.TabIndex = 0;
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(800, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.lbCounter);
			this.panel2.Controls.Add(this.btnLoadImages);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(880, 65);
			this.panel2.TabIndex = 1;
			// 
			// lbCounter
			// 
			this.lbCounter.Location = new System.Drawing.Point(142, 21);
			this.lbCounter.Name = "lbCounter";
			this.lbCounter.Size = new System.Drawing.Size(96, 18);
			this.lbCounter.TabIndex = 3;
			this.lbCounter.Text = "Счётчик:";
			// 
			// btnLoadImages
			// 
			this.btnLoadImages.Location = new System.Drawing.Point(8, 9);
			this.btnLoadImages.Name = "btnLoadImages";
			this.btnLoadImages.Size = new System.Drawing.Size(128, 37);
			this.btnLoadImages.TabIndex = 2;
			this.btnLoadImages.Text = "Загрузить картинки";
			this.btnLoadImages.Click += new System.EventHandler(this.btnLoadImages_Click);
			// 
			// txtLog
			// 
			this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtLog.Location = new System.Drawing.Point(0, 0);
			this.txtLog.Multiline = true;
			this.txtLog.Name = "txtLog";
			this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtLog.Size = new System.Drawing.Size(880, 390);
			this.txtLog.TabIndex = 4;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.txtLog);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(0, 65);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(880, 390);
			this.panel4.TabIndex = 7;
			// 
			// LoadImages
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(880, 501);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "LoadImages";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Загрузка картинок";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
