﻿using System;
using System.Data;
//using System.Data.Odbc;
using System.Data.OleDb;
using System.Windows.Forms;
using MetaData;

namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmSetIdentity.
	/// </summary>
	public partial class SetIdentity : GenericForm
	{
		private DataSetISM _ds;
		private int _processedItems = 0;
		
		
		
		
		
		
		
		
		
		
		private System.Data.DataSet dataSet1;
		private System.Data.DataTable dataTable1;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public SetIdentity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		private void button1_Click(object sender, System.EventArgs e) {
			if(odFiles.ShowDialog(this) == DialogResult.OK) {
				try {
					string connStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%FileName%;Extended properties=Excel 8.0;";
					connStr = connStr.Replace("%FileName%", odFiles.FileName);
					OleDbConnection conn = new OleDbConnection(connStr);
					OleDbCommand cmd = new OleDbCommand("", conn);
//					string connStr = @"DefaultDir=%DirName%;DBQ=%FileName%;DriverId=790;Driver={Microsoft Excel Driver (*.xls)};ReadOnly=True";
//					connStr = connStr.Replace("%FileName%", Path.GetDirectoryName(odFiles.FileName)+"\\"+Path.GetFileNameWithoutExtension(odFiles.FileName));
//					connStr = connStr.Replace("%DirName%", Path.GetDirectoryName(odFiles.FileName));
//					OdbcCommand cmd = new OdbcCommand("", new OdbcConnection(connStr));
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = "SELECT * FROM [Лист1$]";
					OleDbDataAdapter da = new OleDbDataAdapter(cmd);
//					OdbcDataAdapter da = new OdbcDataAdapter(cmd);
					_ds = new DataSetISM();
					da.Fill(_ds);
					dgItems.DataSource = _ds;
					dgItems.DataMember = "table";
					_processedItems = 0;
				}
				catch (Exception ex) {
					MessageBox.Show(ex.Message);
					return;
				}
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e) {
			Close();
		}

		private void button3_Click(object sender, System.EventArgs e) {
			string fieldDest = (string)cbDest.SelectedValue;
			string fieldSource = (string)cbSource.SelectedValue;

			foreach(DataRow dr in _ds.Table.Rows) {
				string sql = @"
Declare
	@sourceOID uniqueidentifier,
	@destOID uniqueidentifier
SELECT @sourceOID = OID FROM t_Goods WHERE "+fieldSource+" = "+ToSqlString(dr[0])+@"
SELECT @destOID = OID FROM t_Goods WHERE "+fieldDest+" = "+ToSqlString(dr[1])+@"


if @sourceOID <> @destOID Begin
	if EXISTS(SELECT * FROM t_ObjectImage WHERE	OID = @sourceOID) Begin
		DELETE FROM t_ObjectImage WHERE OID = @destOID
		INSERT INTO t_ObjectImage (OID, ordValue, binData)
		SELECT @destOID, ordValue, binData
		FROM t_ObjectImage
		WHERE
			OID = @sourceOID
	end

	if EXISTS(SELECT * FROM t_GoodsParams WHERE	OID = @sourceOID) Begin
		DELETE FROM t_GoodsParams WHERE OID = @destOID
		INSERT INTO t_GoodsParams (OID, paramTypeOID, ordValue, value)
		SELECT @destOID, paramTypeOID, ordValue, value
		FROM t_GoodsParams
		WHERE
			OID = @sourceOID
	end

	if EXISTS(SELECT * FROM t_PressReleases WHERE	OID = @sourceOID) Begin
		DELETE FROM t_PressReleases WHERE OID = @destOID
		INSERT INTO t_PressReleases (OID, ordValue, articleOID)
		SELECT @destOID, ordValue, articleOID
		FROM t_PressReleases
		WHERE
			OID = @sourceOID
	end

End
";
				op.ExecuteCommand(sql);
				_processedItems++;
				lbCounter.Text = "Обработано "+_processedItems;
				lbCounter.Refresh();
			}
			MessageBox.Show(this, @"Загрузка соответствий закончена");
		}

		private void fmSetIdentity_Load(object sender, System.EventArgs e) {
			dataTable1.Rows.Add(new object[]{"expertID", "Экспрерт"});
			dataTable1.Rows.Add(new object[]{"oldiID", "Олди"});
			dataTable1.Rows.Add(new object[]{"svegaID", "Свега"});
			dataTable1.Rows.Add(new object[]{"stavID", "Ставрополь"});
			dataTable1.Rows.Add(new object[]{"grafitekID", "Графитек"});
			dataTable1.Rows.Add(new object[]{"pilotageID", "Пилотаж"});
			DataTable dataTable2 = dataTable1.Copy();
			dataTable2.TableName = "Table2";
			dataSet1.Tables.Add(dataTable2);
			cbSource.DataSource = dataTable2;

		}
	}
}
