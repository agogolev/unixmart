﻿namespace ELBClient.Forms.Load
{
	partial class ListJobLog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.lbShopType = new System.Windows.Forms.Label();
			this.LevelSelect = new ColumnMenuExtender.DataBoundComboBox();
			this.JobSelect = new ColumnMenuExtender.DataBoundComboBox();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.LogGrid = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.formattableTextBoxColumn1 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.extendedDataGridComboBoxColumn1 = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.formattableTextBoxColumn2 = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.extendedDataGridSelectorColumn1 = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.LogGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.lbShopType);
			this.panel1.Controls.Add(this.LevelSelect);
			this.panel1.Controls.Add(this.JobSelect);
			this.panel1.Controls.Add(this.dtpDateEnd);
			this.panel1.Controls.Add(this.dtpDateBegin);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(944, 46);
			this.panel1.TabIndex = 0;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(762, 9);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 27);
			this.button1.TabIndex = 10;
			this.button1.Text = "Показать";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(857, 9);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 1;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// lbShopType
			// 
			this.lbShopType.Location = new System.Drawing.Point(297, 9);
			this.lbShopType.Name = "lbShopType";
			this.lbShopType.Size = new System.Drawing.Size(64, 27);
			this.lbShopType.TabIndex = 9;
			this.lbShopType.Text = "Магазин:";
			this.lbShopType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LevelSelect
			// 
			this.LevelSelect.Location = new System.Drawing.Point(596, 12);
			this.LevelSelect.Name = "LevelSelect";
			this.LevelSelect.SelectedValue = -1;
			this.LevelSelect.Size = new System.Drawing.Size(139, 21);
			this.LevelSelect.TabIndex = 4;
			// 
			// JobSelect
			// 
			this.JobSelect.Location = new System.Drawing.Point(369, 12);
			this.JobSelect.Name = "JobSelect";
			this.JobSelect.SelectedValue = -1;
			this.JobSelect.Size = new System.Drawing.Size(184, 21);
			this.JobSelect.TabIndex = 8;
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Location = new System.Drawing.Point(156, 12);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.Size = new System.Drawing.Size(136, 20);
			this.dtpDateEnd.TabIndex = 7;
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Location = new System.Drawing.Point(12, 12);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(136, 20);
			this.dtpDateBegin.TabIndex = 6;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(566, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 27);
			this.label1.TabIndex = 5;
			this.label1.Text = "Тип:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LogGrid
			// 
			this.LogGrid.BackgroundColor = System.Drawing.Color.White;
			this.LogGrid.ColumnDragEnabled = true;
			this.LogGrid.DataMember = "";
			this.LogGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LogGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.LogGrid.Location = new System.Drawing.Point(0, 46);
			this.LogGrid.Name = "LogGrid";
			this.LogGrid.Order = null;
			this.LogGrid.Size = new System.Drawing.Size(944, 290);
			this.LogGrid.StockNumInBatch = 0;
			this.LogGrid.TabIndex = 3;
			this.LogGrid.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.DataGrid = this.LogGrid;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn1,
            this.extendedDataGridComboBoxColumn1,
            this.formattableTextBoxColumn2,
            this.extendedDataGridSelectorColumn1});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "Table";
			this.extendedDataGridTableStyle1.ReadOnly = true;
			// 
			// formattableTextBoxColumn1
			// 
			this.formattableTextBoxColumn1.FieldName = null;
			this.formattableTextBoxColumn1.FilterFieldName = null;
			this.formattableTextBoxColumn1.Format = "dd.MM.yyyy HH:mm";
			this.formattableTextBoxColumn1.FormatInfo = null;
			this.formattableTextBoxColumn1.HeaderText = "Дата/время";
			this.formattableTextBoxColumn1.MappingName = "dateOccur";
			this.formattableTextBoxColumn1.Width = 75;
			// 
			// extendedDataGridComboBoxColumn1
			// 
			this.extendedDataGridComboBoxColumn1.FieldName = null;
			this.extendedDataGridComboBoxColumn1.FilterFieldName = null;
			this.extendedDataGridComboBoxColumn1.Format = "";
			this.extendedDataGridComboBoxColumn1.FormatInfo = null;
			this.extendedDataGridComboBoxColumn1.HeaderText = "Уровень";
			this.extendedDataGridComboBoxColumn1.MappingName = "level";
			this.extendedDataGridComboBoxColumn1.Width = 75;
			// 
			// formattableTextBoxColumn2
			// 
			this.formattableTextBoxColumn2.FieldName = null;
			this.formattableTextBoxColumn2.FilterFieldName = null;
			this.formattableTextBoxColumn2.Format = "";
			this.formattableTextBoxColumn2.FormatInfo = null;
			this.formattableTextBoxColumn2.HeaderText = "Сообщение";
			this.formattableTextBoxColumn2.MappingName = "message";
			this.formattableTextBoxColumn2.Width = 75;
			// 
			// extendedDataGridSelectorColumn1
			// 
			this.extendedDataGridSelectorColumn1.FieldName = null;
			this.extendedDataGridSelectorColumn1.FilterFieldName = null;
			this.extendedDataGridSelectorColumn1.HeaderText = "Стек вызовов";
			this.extendedDataGridSelectorColumn1.MappingName = "stackTraceButton";
			this.extendedDataGridSelectorColumn1.NullText = "";
			this.extendedDataGridSelectorColumn1.Width = 75;
			// 
			// ListJobLog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(944, 336);
			this.Controls.Add(this.LogGrid);
			this.Controls.Add(this.panel1);
			this.Name = "ListJobLog";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Просмотр лога загрузки товаров";
			this.Load += new System.EventHandler(this.ListJobLog_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.LogGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbShopType;
		private ColumnMenuExtender.DataBoundComboBox LevelSelect;
		private ColumnMenuExtender.DataBoundComboBox JobSelect;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.DataGridISM LogGrid;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn1;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn extendedDataGridComboBoxColumn1;
		private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn2;
		private ColumnMenuExtender.ExtendedDataGridSelectorColumn extendedDataGridSelectorColumn1;
	}
}