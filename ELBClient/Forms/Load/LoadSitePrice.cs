﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using MetaData;
using ELBClient.ObjectProvider;
using System.Globalization;
using Excel;

namespace ELBClient.Forms.Load
{
	public partial class LoadSitePrice : ListForm
	{
		public int StartFrom { get; set; }
		public int ShopType { get; set; }
		public string ShopName { get; set; }
		private DataSet _ds;
		private int _processedItems = 0;
		public LoadSitePrice()
		{
			InitializeComponent();
			StartFrom = 1;
		}

		private void LoadFile_Click(object sender, EventArgs e)
		{
			if (LoadExcel.ShowDialog(this) == DialogResult.OK)
			{
				try
				{
					using (FileStream stream = File.Open(LoadExcel.FileName, FileMode.Open, FileAccess.Read))
					{
						using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
						{
							excelReader.Read();
							_ds = new DataSetISM();
							DataTable dt = new DataTable("Table");							
							_ds.Tables.Add(dt);
							int i = 0;
							string name = null;
							do
							{
								name = excelReader.GetString(i);
								dt.Columns.Add(name);
								i++;
							}
							while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
							while (excelReader.Read())
							{
								DataRow dr = dt.NewRow();
								for (int j = 0; j < dt.Columns.Count; j++)
								{
									dr[j] = excelReader.GetString(j);
								}
								dt.Rows.Add(dr);
							}
							_ds.AcceptChanges();
						}
					}

					dgGoods.DataSource = _ds;
					dgGoods.DataMember = "Table";
					_processedItems = 0;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					return;
				}
			}
		}

		private void ImportPrices_Click(object sender, EventArgs e)
		{
			if (_ds == null)
			{
				ShowError("Загрузите файл");
				return;
			}
			
			string sql = @"

if EXISTS (SELECT * FROM t_GoodsPrices gp inner join t_Goods g on gp.OID = g.OID and g.postavshik_id = @id And gp.shopType = @shopType)
	UPDATE t_GoodsPrices SET price = @price 
	FROM t_GoodsPrices gp 
		inner join t_Goods g on gp.OID = g.OID and g.postavshik_id = @id And gp.shopType = @shopType
else
	INSERT INTO t_GoodsPrices (OID, shopType, price) 
	SELECT OID, @shopType, @price FROM t_Goods WHERE postavshik_id = @id

";
			_processedItems = StartFrom - 1;
			for (int i = StartFrom - 1; i < _ds.Tables[0].Rows.Count; i++)
			{
				DataRow dr = _ds.Tables[0].Rows[i];
				int id = int.Parse(dr[0].ToString().Trim());
				decimal price = decimal.Parse(dr[1].ToString().Trim());

				op.ExecuteCommandParams(sql, new NameValuePair[] {
					new NameValuePair { Name = "id", Value = id.ToString()},
					new NameValuePair { Name = "shopType", Value = ShopType.ToString()},
					new NameValuePair { Name = "price", Value = price.ToString("0.##", CultureInfo.InvariantCulture.NumberFormat)}
				}, false);
				Counter.Text = "Обработано: " + ++_processedItems;
				Counter.Update();
			}
			MessageBox.Show(this, @"Импорт каталога завершён");
		}

		private void LoadGoods_Load(object sender, EventArgs e)
		{
			Text = string.Format("Загрузка цен - {0}", ShopName);
			StartFromText.DataBindings.Add("Text", this, "StartFrom");
		}

	}
}
