﻿using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using ELBClient.Classes;
using MetaData;
using ELBClient.Forms.Dialogs;
using ColumnMenuExtender;
using ELBClient.Forms.Guides;


namespace ELBClient.Forms.Load
{
	/// <summary>
	/// Summary description for fmListGoodsForLoad.
	/// </summary>
	public partial class ListGoodsForLoad : ListForm {
		private ObjectProvider.ObjectProvider prov = Classes.ServiceUtility.ObjectProvider;
		private int MasterNode = 4445;
		private ArrayList filterCategories = new ArrayList();
		private Tree treeMaster = new Tree(0, int.MaxValue);
		private Tree treeSite = new Tree(0, int.MaxValue);
		private TreeProvider.TreeProvider treeProvider = ServiceUtility.TreeProvider;
		private System.ComponentModel.IContainer components;
		
		private System.Data.DataSet dataSet1;
		
		
		
		
		
		
		
		

		public ListGoodsForLoad() {
			InitializeComponent();
			listContextMenu1.miDelete.Visible = false;
			MenuItem mnuDelete = new MenuItem("Удалить");
			mnuDelete.Click += new EventHandler(mnuDelete_Click);
			listContextMenu1.insertItem(4, mnuDelete);
			MenuItem mnuAttachImages = new MenuItem("Привязка картинок");
			mnuAttachImages.Click += new EventHandler(mnuAttachImages_Click);
			listContextMenu1.insertItem(2, mnuAttachImages);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

		public override void LoadData() {
			dataSet1 = lp.GetList(dgGoods.GetDataXml().OuterXml);
			int cri = dgGoods.CurrentRowIndex;
			dgGoods.Enabled = false;
			dgGoods.SetDataBinding(dataSet1, "table");
			dgGoods.Enabled = true;
			//корявый способ избавится от дизабленных скролбаров :)
			dgGoods.Width++;dgGoods.Width--;
			DataTable dt = dataSet1.Tables["table"];
			if (cri == -1) {
				if (dt.Rows.Count > 0) 
					cri = 0;
			}
			else if (cri > dt.Rows.Count-1) 
				cri = dt.Rows.Count-1;

			if (cri >= 0) {
				dgGoods.CurrentRowIndex = cri;
				dgGoods.Select(cri);
			}
			//			dataGridPager1.BindToDataGrid(dgGoods);
		}

		private void fmListGoods_Load(object sender, System.EventArgs e) {
			BuildMasterTree();
			LoadMasterTree();
			
			listContextMenu1.Bind(this);
		}

		private void dataGridISM1_Reload(object sender, System.EventArgs e) {
			LoadData();
		}

		private void btnNew_Click(object sender, System.EventArgs e) {
			DataSet ds = (DataSet)dgGoods.DataSource;
			int i = 0;
			foreach(DataRow dr in ds.Tables[0].Rows) {
				Guid OID = (Guid)dr["OID"];
				ProcessGoods(OID);
				i++;
				lbCount.Text = "Обработано "+i+" из "+ds.Tables[0].Rows.Count;
				lbCount.Update();
			}
			prov.ClearCache();
			MessageBox.Show(this, @"Загрузка полных наименований для данного списка товаров закончена");
		}
		private void ProcessGoods(Guid OID) {
			string sql = @"exec upUpdateFullName '"+OID+"'";
			prov.ExecuteCommand(sql);
		}

		private void listContextMenu1_EditClick(object sender, System.EventArgs e) {
			showEditForm(listContextMenu1.SelectedOID);
		}
		
		private void showEditForm(Guid OID) {
			EditGoods form = new EditGoods();
			form.ObjectOID = OID;
			form.MdiParent = this.MdiParent;
			form.ReloadGrid += new ReloadDelegate(LoadData);
			form.Show();
		}

		private void dataGridISM1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e) {
			if (e.Button == MouseButtons.Right) {
				DataGrid.HitTestInfo hti = dgGoods.HitTest(e.X, e.Y);
				//if (hti.Type == DataGrid.HitTestType.ColumnHeader && columnMenuExtender1 != null) {
				//  int n = hti.Column;
				
				//  DataTable dt = TableInGrid.GetTable(dgGoods);
				//  DataGridColumnStyle s = dgGoods.TableStyles[dt.TableName].GridColumnStyles[n];
				//  ContextMenu cm = columnMenuExtender1.GetMenu(s);
				//  if (cm != null) {
				//    if (cm is MenuFilterSort) {
				//      ((MenuFilterSort)cm).Shows(dgGoods, new Point(e.X, e.Y), dt.TableName, dt.Columns[s.MappingName]);
				//    }
				//    else
				//      cm.Shows(dgGoods, new Point(e.X, e.Y));
				//  }
				//}
				//else 
				if (hti.Type == DataGrid.HitTestType.Cell) {
					int rowNum = hti.Row;
					dgGoods.CurrentRowIndex = rowNum;
					//					int colNum = hti.Column;
					DataTable dt = TableInGrid.GetTable(dgGoods);
					listContextMenu1.SelectedOID = (Guid)dt.Rows[rowNum]["OID"];
					listContextMenu1.Show(dgGoods, new Point(e.X, e.Y));
				}				
			}
		}

		private void dataGridISM1_DoubleClick(object sender, System.EventArgs e) {
			System.Drawing.Point pt = dgGoods.PointToClient(Cursor.Position);
			DataGrid.HitTestInfo hti = dgGoods.HitTest(pt); 
			if(hti.Type == DataGrid.HitTestType.Cell) {
				//				DataRow dr = ColumnMenuExtender.TableInGrid.GetSelectedRow(dataGridISM1, pt.X, pt.Y);
				DataRow dr = dgGoods.GetSelectedRow();
				showEditForm((Guid)dr["OID"]);
			}
		}

		private void dataGridISM1_ActionKeyPressed(object sender, System.Windows.Forms.KeyEventArgs e) {
			if (e.KeyCode == Keys.Insert) {
				showEditForm(Guid.Empty);
			}
			else if (e.KeyCode == Keys.Delete) {
				if(dgGoods.CurrentRowIndex >= 0) {
					DataTable dt = ColumnMenuExtender.TableInGrid.GetTable(dgGoods);
					listContextMenu1.DeleteObject(ParentForm, (Guid)dt.Rows[dgGoods.CurrentRowIndex]["OID"]);
				}
			}
			else if (e.KeyCode == Keys.Enter) {
				if(dgGoods.CurrentRowIndex >= 0) {
					DataTable dt = ColumnMenuExtender.TableInGrid.GetTable(dgGoods);
					showEditForm((Guid)dt.Rows[dgGoods.CurrentRowIndex]["OID"]);
				}
			}
		}

		private void mnuAttachImages_Click(object sender, EventArgs e) {
			ArrayList rows = dgGoods.GetMultiSelectedRows();
			if(rows.Count == 1) {
				DataRow dr = (DataRow) rows[0];
				Guid OID = (Guid)dr["OID"];
				string name = dr["name"].ToString();
				CGoods goods = new CGoods();
				goods.OID = OID;
				AttachImages frm = new AttachImages(goods);
				frm.Text += " для товара: "+name;
				frm.ReloadGrid += new ReloadDelegate(LoadData);
				frm.ShowDialog(this);
			}
			else {
				CGoods goods = new CGoods();
				MassAttachImages frm = new MassAttachImages(goods);
				if(frm.ShowDialog(this) == DialogResult.OK) {
					try {
						Guid imageOID = frm.SelectedImage.OID;
						string ordValue = frm.OrdValue;
						string sql = @"
if EXISTS (SELECT * FROM t_ObjectImage WHERE OID = '%OID%' And ordValue = '%ordValue%') 
	UPDATE t_ObjectImage SET binData = '%imageOID%' WHERE OID = '%OID%' And ordValue = '%ordValue%'
else 
	INSERT INTO t_ObjectImage VALUES('%OID%', '%ordValue%', '%imageOID%')
";
						if(imageOID != Guid.Empty) {
							foreach(DataRow dr in rows) {
								Guid OID = (Guid)dr["OID"];
								ObjectProvider.ObjectProvider prov = ServiceUtility.ObjectProvider;
								string curSql = sql.Replace("%OID%", OID.ToString()).Replace("%ordValue%", ordValue.Replace("'", "''")).Replace("%imageOID%", imageOID.ToString());
								prov.ExecuteCommand(curSql);
							}
						}
					}
					catch(Exception ex) {
						ShowError("Ошибка привязки картинки: "+ex.Message);
					}
					LoadData();
				}
			}
		}
		private void mnuDelete_Click(object sender, EventArgs e) {
			ArrayList rows = dgGoods.GetMultiSelectedRows();
			if (MessageBox.Show(this, "Вы уверены, что хотите удалить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) {
				foreach(DataRow dr in rows) {
					Guid OID = (Guid)dr["OID"];
					ObjectProvider.ObjectProvider prov = ServiceUtility.ObjectProvider;
					ObjectProvider.ExceptionISM ex;
					ex = prov.DeleteObject(OID);
					if (ex != null) {	
						if(ex.LiteralExceptionType == "System.Data.SqlClient.SqlException")
							ShowError("Невозможно удалить объект!\nВозможно он связан с другим объектом.");
						else if(ex.LiteralExceptionType == "MetaData.SystemOIDException")
							ShowError("Невозможно удалить объект!\nВозможно он связан с другим объектом.");
						else ShowError(ex.LiteralMessage);
					}
				}
				LoadData();
			}
		}

		private void BuildMasterTree() {
			TreeProvider.TreeProvider treeProv = Classes.ServiceUtility.TreeProvider;
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><tree OID = \"20E1AC79-5A3C-4B49-94E0-9E640842FE81\" nodeID=\""+MasterNode+"\" />";
			DataSet dataSet1 = treeProv.GetTreeContent(xml);
			Stack nodes = new Stack();
			nodes.Push(this.treeMaster);

			int prevNode = 0;
			foreach(DataRow dr in dataSet1.Tables["table"].Rows) {
				int nodeID = (int)dr["nodeID"];
				Node node = null;
				if(nodeID != prevNode) {
					node = new Node((int) dr["lft"], (int) dr["rgt"]);
					node.NodeId = nodeID;
					node.Name = dr["nodeName"].ToString();
					node.objectOID = dr["OID"] == DBNull.Value ? Guid.Empty: (Guid) dr["OID"];
					node.objectNK = dr["NK"].ToString();// == DBNull.Value ? null: (string) dr["NK"];
					node.objectNKRus = dr["label"].ToString();
					node.className = dr["className"].ToString();
					while(nodes.Peek() != null && ((NodeContainer) nodes.Peek()).Right < node.Left) nodes.Pop();
					((NodeContainer) nodes.Peek()).Nodes.Add(node);
					if ((node.Left+1) != node.Right) {
						nodes.Push(node);
					}
					prevNode = nodeID;
				}
				else {
					if(node != null) {
						node.AddString.Rows.Add(new string[3] {(string)dr["ordValue"], (string)dr["value"], (string)dr["url"]});
						node.AddString.AcceptChanges();
					}
				}
			}
		}
		private void LoadMasterTree() {
			tvMaster.Nodes.Clear();
			TreeNode root = new TreeNode("Все товары");
			tvMaster.Nodes.Add(root);
			foreach(Node n in this.treeMaster.Nodes) {
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;//n.objectOID;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				tvMaster.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}
		private void LoadNode(TreeNode treeNode, Node node) {
			foreach(Node n in node.Nodes) {
				TreeNode tn = new TreeNode(n.Name != "" ? n.Name.Replace("\r\n", " ") : (n.objectNK != "" ? n.objectNK : (n.objectNKRus != "" ? n.objectNKRus : "<Unknown>")));
				tn.Tag = n;
				if(n.objectOID != Guid.Empty)
					tn.ForeColor = Color.Red;
				treeNode.Nodes.Add(tn);
				LoadNode(tn, n);
			}
		}

		private void tvMaster_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e) {
			tvMaster.SelectedNode.Expand();

			dgGoods.Filters.Remove("category");
		
			if(tvMaster.SelectedNode != tvMaster.Nodes[0] && (tvMaster.SelectedNode.Tag as Node).objectOID != Guid.Empty) {
				filterCategories.Clear();
				filterCategories.Add((tvMaster.SelectedNode.Tag as Node).objectOID);
				LoadChildrenCategories(tvMaster.SelectedNode);
				BuildFilter();
			}
						
			dgGoods.OnReload(new EventArgs());
		}
		private void LoadChildrenCategories(TreeNode node) {
			foreach(TreeNode nd in node.Nodes) {
				Guid catOID = (nd.Tag as Node).objectOID;
				if(catOID != Guid.Empty) filterCategories.Add(catOID);
				LoadChildrenCategories(nd);
			}
		}
		private void BuildFilter() {
			dgGoods.CreateAuxFilter("fCategory(OID, 1)", "category", FilterVerb.In, false, filterCategories.ToArray());		
		}
	}
}
