﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using Excel;
using MetaData;

namespace ELBClient.Forms.Load
{
	public partial class ImportTemplates : EditForm
	{
		public Guid ThemeOID { get; set; }
		public IList<ParamType> ParamTypes { get; set; }
		public IDictionary<int, Guid> ParamTypeColumns { get; set; }
		public int CriticalErrors { get; set; }
		public int StartFrom { get; set; }
		private DataSet _ds;
		private int _processedItems;
		public ImportTemplates()
		{
			InitializeComponent();
			StartFrom = 1;
		}

		private void LoadFile_Click(object sender, EventArgs e)
		{
			if (LoadExcel.ShowDialog(this) == DialogResult.OK)
			{
				try
				{
					using (var stream = File.Open(LoadExcel.FileName, FileMode.Open, FileAccess.Read))
					{
						if (Path.GetExtension(LoadExcel.FileName).ToLower() == ".xls")
						{
							using (var excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
							{
								excelReader.Read();
								_ds = new DataSetISM();
								var dt = new DataTable("Table");
								_ds.Tables.Add(dt);
								var i = 0;
								string name;
								do
								{
									name = excelReader.GetString(i);
									dt.Columns.Add(name);
									i++;
								}
								while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
								while (excelReader.Read())
								{
									DataRow dr = dt.NewRow();
									for (int j = 0; j < dt.Columns.Count; j++)
									{
										dr[j] = excelReader.GetString(j);

									}
									if (dr[0] == DBNull.Value) break;
									dt.Rows.Add(dr);
								}
								_ds.AcceptChanges();
							}
						}
						else if (Path.GetExtension(LoadExcel.FileName).ToLower() == ".xlsx")
						{
							using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
							{
								excelReader.IsFirstRowAsColumnNames = true;
								_ds = excelReader.AsDataSet();
								_ds.Tables[0].TableName = "Table";
								//excelReader.Read();
								//_ds = new DataSetISM();
								//var dt = new DataTable("Table");
								//_ds.Tables.Add(dt);
								//var i = 0;
								//string name;
								//do
								//{
								//    name = excelReader.GetString(i);
								//    dt.Columns.Add(name);
								//    i++;
								//}
								//while (!string.IsNullOrEmpty(name) && i < excelReader.FieldCount);
								//while (excelReader.Read())
								//{
								//    DataRow dr = dt.NewRow();
								//    for (int j = 0; j < dt.Columns.Count; j++)
								//    {
								//        dr[j] = excelReader.GetString(j);

								//    }
								//    if (dr[0] == DBNull.Value) break;
								//    dt.Rows.Add(dr);
								//}
								//_ds.AcceptChanges();
							}
						}

					}

					CheckColumns();
					dgGoods.DataSource = _ds;
					dgGoods.DataMember = "Table";
					_processedItems = 0;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
				}
			}
		}

		private void CheckColumns()
		{
			CriticalErrors = 0;
			var errors = new StringBuilder();
			if (string.Compare(_ds.Tables[0].Columns[0].ColumnName.Trim(), "PostavshikID", true) != 0)
			{
				errors.Append("Неправильно названа первая колонка должна быть PostavshikID\r\n");
				CriticalErrors += 1;
			}
		    if (string.Compare(_ds.Tables[0].Columns[1].ColumnName.Trim(), "productType", true) != 0)
		    {
		        errors.Append("Неправильно названа вторая колонка должна быть productType\r\n");
		        CriticalErrors += 1;
		    }
			if (string.Compare(_ds.Tables[0].Columns[2].ColumnName.Trim(), "model", true) != 0)
			{
				errors.Append("Неправильно названа третья колонка должна быть model\r\n");
				CriticalErrors += 1;
			}

			CheckUniqness().ForEach(item =>
										{
											errors.AppendFormat("Не уникальная колонка с названием: {0}\r\n", item);
											CriticalErrors += 1;
										});
			if (errors.Length != 0) ShowError(errors.ToString());
		}

		private void ImportDescriptions_Click(object sender, EventArgs e)
		{
			var makePar = new Regex(@"\s*\|\s*");
			if (_ds == null)
			{
				ShowError("Загрузите файл");
				return;
			}

			if (CriticalErrors != 0)
			{
				ShowError("В файле есть критичные ошибки, не позволяющие корректно загрузить файл. Сначала исправьте их.");
				return;
			}

			CreateTemplate();

			const string sql = @"
UPDATE t_Goods SET 
	productType = {1},
	model = {2}
WHERE
	postavshik_id = {0}

DELETE FROM t_GoodsParams
FROM t_GoodsParams gp 
	inner join t_Goods g on gp.OID = g.OID
	inner join t_ThemeMasters tm on g.category = tm.masterOID and tm.OID = {3}
WHERE
	g.postavshik_id = {0}
";
			const string sqlParams = @"
INSERT INTO t_GoodsParams (OID, paramTypeOID, ordValue, value, unit)
SELECT g.OID, {1}, tp.ordValue, {2}, tp.unit
FROM
	t_TemplateParams tp
	inner join t_ThemeMasters tm on tp.OID = tm.OID
	inner join t_Goods g on tm.masterOID = g.category
WHERE
	tp.OID = {3} and g.postavshik_id = {0} and tp.paramTypeOID = {1}
";
			_processedItems = StartFrom - 1;
			for (var i = StartFrom - 1; i < _ds.Tables[0].Rows.Count; i++)
			{
				var dr = _ds.Tables[0].Rows[i];
				if (dr[0].ToString() == string.Empty) continue;
				var builder = new StringBuilder();
				var id = int.Parse(dr[0].ToString().Trim());
				var productType = dr[1].ToString().Trim();
				var model = dr[2].ToString().Trim();
				builder.AppendFormat(sql, id, ToSqlString(productType), ToSqlString(model), ToSqlString(ThemeOID));
				for (var j = 3; j < _ds.Tables[0].Columns.Count; j++)
				{
					if (!ParamTypeColumns.ContainsKey(j)) continue;
					var paramTypeOID = ParamTypeColumns[j];
					var value = makePar.Replace(dr[j].ToString().Trim(), "\r\n");
					if (string.IsNullOrEmpty(value)) continue;
					builder.AppendFormat(sqlParams, id, ToSqlString(paramTypeOID), ToSqlString(value), ToSqlString(ThemeOID));
				}
				op.ExecuteCommand(builder.ToString());
				Counter.Text = "Обработано: " + ++_processedItems;
				Counter.Update();
			}
			op.ClearCache();
			ReloadGrid();
			MessageBox.Show(this, @"Импорт описаний завершён");
		}

		private List<string> CheckUniqness()
		{
			var re = new Regex(@"(.+)\(([^)]+)\)\s*");
			var names = new List<string>();
			for (var i = 3; i < _ds.Tables[0].Columns.Count; i++)
			{
				var col = _ds.Tables[0].Columns[i];
				var name = col.ColumnName;
				//var unit = "";
				var nameShort = "";
				var m = re.Match(name);
				if (m.Success)
				{
					nameShort = m.Groups[1].Value.Trim();
					//unit = m.Groups[2].Value.Trim();
				}
				else nameShort = name;
				names.Add(nameShort);
			}
			return names.GroupBy(s => s).Where(g => g.Count() > 1).Select(g => g.Key).ToList();
		}
		private void CreateTemplate()
		{
			var re = new Regex(@"(.+)\(([^)]+)\)\s*");
			ParamTypeColumns = new Dictionary<int, Guid>();
			for (var i = 3; i < _ds.Tables[0].Columns.Count; i++)
			{
				var col = _ds.Tables[0].Columns[i];
				var name = col.ColumnName;
				var unit = "";
				var m = re.Match(name);
				if (m.Success)
				{
					name = m.Groups[1].Value.Trim();
					unit = m.Groups[2].Value.Trim();
				}
				string sql = @"
Declare 
	@OID uniqueidentifier,
	@ordValue int = 0
SELECT @OID = OID FROM t_ParamType WHERE name = {0}

if @OID is NULL Begin
	exec spInnerCreateObject 'CParamType', @OID OUTPUT

	UPDATE t_ParamType SET
		name = {0}
	WHERE
		OID = @OID
End

if NOT EXISTS(SELECT * FROM t_TemplateParams WHERE OID = {1} and paramTypeOID = @OID) Begin
	SELECT @ordValue = ISNULL(MAX(ordValue), 0) + 1 FROM t_TemplateParams WHERE OID = {1}

	INSERT INTO t_TemplateParams (OID, ordValue, paramTypeOID, unit) VALUES ({1}, @ordValue, @OID, {2})
end
else Begin
	UPDATE t_TemplateParams SET unit = {2} WHERE OID = {1} and paramTypeOID = @OID
end

SELECT OID = @OID, ordValue = @ordValue

";
				if (ParamTypes.All(p => string.Compare(p.ParamName, name, true) != 0 || string.Compare(p.Unit, unit, true) != 0))
				{
					var data = new DataSetISM(lp.GetDataSetSql(string.Format(sql, ToSqlString(name), ToSqlString(ThemeOID), ToSqlString(unit))));
					var item =
						data.Table.AsEnumerable().Select(row => new { OID = row.Field<Guid>("OID"), OrdValue = row.Field<int>("ordValue") })
							.Single();
					var paramType = new ParamType { OID = item.OID, ParamName = name, Unit = unit };
					ParamTypes.Add(paramType);
					ParamTypeColumns[item.OrdValue + 2] = paramType.OID;
				}
				else
				{
					ParamTypeColumns[i] = ParamTypes.First(p => string.Compare(p.ParamName, name, true) == 0).OID;
				}
			}

		}

		private void LoadGoods_Load(object sender, EventArgs e)
		{
			StartFromText.DataBindings.Add("Text", this, "StartFrom");
		}

	}

	public class ParamType
	{
		public Guid OID { get; set; }
		public string ParamName { get; set; }
		public string Unit { get; set; }
		public string NameUnit { get { return ParamName.Trim() + (string.IsNullOrWhiteSpace(Unit) ? "" : " (" + Unit + ")"); } }
	}
}
