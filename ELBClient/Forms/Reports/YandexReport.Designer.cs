﻿namespace ELBClient.Forms.Reports
{
    partial class YandexReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnShow = new System.Windows.Forms.Button();
            this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dgUrls = new ColumnMenuExtender.DataGridISM();
            this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
            this.formattableTextBoxColumn3 = new ColumnMenuExtender.FormattableTextBoxColumn();
            this.yandexUrlColumn = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.fullUrlColumn = new ColumnMenuExtender.ExtendedDataGridSelectorColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUrls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnShow);
            this.panel1.Controls.Add(this.dtpDateEnd);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dtpDateBegin);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(937, 46);
            this.panel1.TabIndex = 0;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(536, 8);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 27);
            this.btnShow.TabIndex = 8;
            this.btnShow.Text = "Показать";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // dtpDateEnd
            // 
            this.dtpDateEnd.Location = new System.Drawing.Point(330, 11);
            this.dtpDateEnd.Name = "dtpDateEnd";
            this.dtpDateEnd.Size = new System.Drawing.Size(200, 20);
            this.dtpDateEnd.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(274, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 27);
            this.label2.TabIndex = 7;
            this.label2.Text = "Конец:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpDateBegin
            // 
            this.dtpDateBegin.Location = new System.Drawing.Point(74, 11);
            this.dtpDateBegin.Name = "dtpDateBegin";
            this.dtpDateBegin.Size = new System.Drawing.Size(200, 20);
            this.dtpDateBegin.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 27);
            this.label1.TabIndex = 5;
            this.label1.Text = "Начало:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgUrls
            // 
            this.dgUrls.AllowSorting = false;
            this.dgUrls.BackgroundColor = System.Drawing.Color.White;
            this.dgUrls.ColumnDragEnabled = true;
            this.dgUrls.DataMember = "";
            this.dgUrls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgUrls.FilterString = null;
            this.dgUrls.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgUrls.Location = new System.Drawing.Point(0, 46);
            this.dgUrls.Name = "dgUrls";
            this.dgUrls.Order = null;
            this.dgUrls.Size = new System.Drawing.Size(937, 357);
            this.dgUrls.TabIndex = 2;
            this.dgUrls.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
            // 
            // extendedDataGridTableStyle1
            // 
            this.extendedDataGridTableStyle1.DataGrid = this.dgUrls;
            this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.formattableTextBoxColumn3,
            this.yandexUrlColumn,
            this.fullUrlColumn});
            this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.extendedDataGridTableStyle1.MappingName = "Table";
            this.extendedDataGridTableStyle1.ReadOnly = true;
            // 
            // formattableTextBoxColumn3
            // 
            this.formattableTextBoxColumn3.FieldName = null;
            this.formattableTextBoxColumn3.FilterFieldName = null;
            this.formattableTextBoxColumn3.Format = "dd.MM.yyyy HH:mm:ss";
            this.formattableTextBoxColumn3.FormatInfo = null;
            this.formattableTextBoxColumn3.HeaderText = "Время";
            this.formattableTextBoxColumn3.MappingName = "dateOccur";
            this.formattableTextBoxColumn3.Width = 75;
            // 
            // yandexUrlColumn
            // 
            this.yandexUrlColumn.FieldName = null;
            this.yandexUrlColumn.FilterFieldName = null;
            this.yandexUrlColumn.Format = "";
            this.yandexUrlColumn.FormatInfo = null;
            this.yandexUrlColumn.HeaderText = "Яндекс";
            this.yandexUrlColumn.MappingName = "yandexUrl";
            this.yandexUrlColumn.Width = 75;
            // 
            // fullUrlColumn
            // 
            this.fullUrlColumn.FieldName = null;
            this.fullUrlColumn.FilterFieldName = null;
            this.fullUrlColumn.Format = "";
            this.fullUrlColumn.FormatInfo = null;
            this.fullUrlColumn.HeaderText = "Куда";
            this.fullUrlColumn.MappingName = "fullUrl";
            this.fullUrlColumn.Width = 75;
            // 
            // YandexReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 403);
            this.Controls.Add(this.dgUrls);
            this.Controls.Add(this.panel1);
            this.Name = "YandexReport";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Отчёт по страницам Яндекса";
            this.Load += new System.EventHandler(this.YandexReport_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUrls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpDateEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDateBegin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnShow;
        private ColumnMenuExtender.DataGridISM dgUrls;
        private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
        private ColumnMenuExtender.FormattableTextBoxColumn formattableTextBoxColumn3;
        private ColumnMenuExtender.ExtendedDataGridSelectorColumn yandexUrlColumn;
        private ColumnMenuExtender.ExtendedDataGridSelectorColumn fullUrlColumn;
    }
}