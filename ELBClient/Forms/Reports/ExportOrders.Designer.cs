using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using Telerik.WinControls.UI.Data;

namespace ELBClient.Forms.Reports
{
	public partial class ExportOrders
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DateTimePicker dtpDateBegin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtpDateEnd;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnShow;
		private System.Windows.Forms.Button btnClose;
		private ColumnMenuExtender.DataGridISM dgOrders;
		private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.SaveFileDialog sfdExportFile;
		private System.Windows.Forms.CheckBox cbAll;
		private System.Windows.Forms.Label lbShopType;
		private ColumnMenuExtender.DataBoundComboBox cbShopType;
		private ColumnMenuExtender.ExtendedDataGridTableStyle extendedDataGridTableStyle1;
		private ColumnMenuExtender.FormattableTextBoxColumn colOrderNum;
		private ColumnMenuExtender.FormattableTextBoxColumn colBuyer;
		private ColumnMenuExtender.FormattableTextBoxColumn colPhone;
		private ColumnMenuExtender.FormattableTextBoxColumn colEMail;
		private ColumnMenuExtender.FormattableTextBoxColumn colOperator;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colStatus;
		private ColumnMenuExtender.FormattableTextBoxColumn colDateCreate;
		private ColumnMenuExtender.FormattableBooleanColumn colExport;
		private ColumnMenuExtender.ColumnMenuExtender columnMenuExtender1;
		private ColumnMenuExtender.MenuFilterSort menuFilterSort1;
		private ColumnMenuExtender.ExtendedDataGridComboBoxColumn colInterval;
		private System.ComponentModel.Container components = null;

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.lbShopType = new System.Windows.Forms.Label();
			this.cbAll = new System.Windows.Forms.CheckBox();
			this.btnExport = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.btnShow = new System.Windows.Forms.Button();
			this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtpDateBegin = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.cbShopType = new ColumnMenuExtender.DataBoundComboBox();
			this.dgOrders = new ColumnMenuExtender.DataGridISM();
			this.extendedDataGridTableStyle1 = new ColumnMenuExtender.ExtendedDataGridTableStyle();
			this.colOrderNum = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colBuyer = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colPhone = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colEMail = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colOperator = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colStatus = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.colDateCreate = new ColumnMenuExtender.FormattableTextBoxColumn();
			this.colExport = new ColumnMenuExtender.FormattableBooleanColumn();
			this.colInterval = new ColumnMenuExtender.ExtendedDataGridComboBoxColumn();
			this.sfdExportFile = new System.Windows.Forms.SaveFileDialog();
			this.columnMenuExtender1 = new ColumnMenuExtender.ColumnMenuExtender();
			this.menuFilterSort1 = new ColumnMenuExtender.MenuFilterSort();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgOrders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.lbShopType);
			this.panel1.Controls.Add(this.cbAll);
			this.panel1.Controls.Add(this.btnExport);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Controls.Add(this.btnShow);
			this.panel1.Controls.Add(this.dtpDateEnd);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.dtpDateBegin);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.cbShopType);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(776, 65);
			this.panel1.TabIndex = 0;
			// 
			// lbShopType
			// 
			this.lbShopType.Location = new System.Drawing.Point(72, 36);
			this.lbShopType.Name = "lbShopType";
			this.lbShopType.Size = new System.Drawing.Size(64, 26);
			this.lbShopType.TabIndex = 5;
			this.lbShopType.Text = "Магазин:";
			this.lbShopType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbAll
			// 
			this.cbAll.Checked = true;
			this.cbAll.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbAll.Location = new System.Drawing.Point(8, 37);
			this.cbAll.Name = "cbAll";
			this.cbAll.Size = new System.Drawing.Size(104, 18);
			this.cbAll.TabIndex = 7;
			this.cbAll.Text = "Все";
			this.cbAll.CheckedChanged += new System.EventHandler(this.cbAll_CheckedChanged);
			// 
			// btnExport
			// 
			this.btnExport.Location = new System.Drawing.Point(616, 8);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(75, 27);
			this.btnExport.TabIndex = 6;
			this.btnExport.Text = "Экспорт";
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(696, 8);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 27);
			this.btnClose.TabIndex = 5;
			this.btnClose.Text = "Закрыть";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnShow
			// 
			this.btnShow.Location = new System.Drawing.Point(536, 8);
			this.btnShow.Name = "btnShow";
			this.btnShow.Size = new System.Drawing.Size(75, 27);
			this.btnShow.TabIndex = 4;
			this.btnShow.Text = "Показать";
			this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
			// 
			// dtpDateEnd
			// 
			this.dtpDateEnd.Location = new System.Drawing.Point(328, 9);
			this.dtpDateEnd.Name = "dtpDateEnd";
			this.dtpDateEnd.Size = new System.Drawing.Size(200, 20);
			this.dtpDateEnd.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(272, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(48, 27);
			this.label2.TabIndex = 3;
			this.label2.Text = "Конец:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dtpDateBegin
			// 
			this.dtpDateBegin.Location = new System.Drawing.Point(72, 9);
			this.dtpDateBegin.Name = "dtpDateBegin";
			this.dtpDateBegin.Size = new System.Drawing.Size(200, 20);
			this.dtpDateBegin.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 27);
			this.label1.TabIndex = 1;
			this.label1.Text = "Начало:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbShopType
			// 
			this.cbShopType.Location = new System.Drawing.Point(144, 37);
			this.cbShopType.Name = "cbShopType";
			this.cbShopType.SelectedValue = -1;
			this.cbShopType.Size = new System.Drawing.Size(184, 21);
			this.cbShopType.TabIndex = 4;
			this.cbShopType.SelectedIndexChanged += new PositionChangedEventHandler(this.cbShopType_SelectedIndexChanged);
			// 
			// dgOrders
			// 
			this.dgOrders.AllowSorting = false;
			this.dgOrders.BackgroundColor = System.Drawing.Color.White;
			this.dgOrders.ColumnDragEnabled = true;
			this.dgOrders.DataMember = "";
			this.dgOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgOrders.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgOrders.Location = new System.Drawing.Point(0, 65);
			this.dgOrders.Name = "dgOrders";
			this.dgOrders.Order = null;
			this.dgOrders.Size = new System.Drawing.Size(776, 372);
			this.dgOrders.TabIndex = 1;
			this.dgOrders.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.extendedDataGridTableStyle1});
			this.columnMenuExtender1.SetUseGridMenu(this.dgOrders, false);
			this.dgOrders.Reload += new System.EventHandler(this.dgOrders_Reload);
			// 
			// extendedDataGridTableStyle1
			// 
			this.extendedDataGridTableStyle1.AllowSorting = false;
			this.extendedDataGridTableStyle1.DataGrid = this.dgOrders;
			this.extendedDataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.colOrderNum,
            this.colBuyer,
            this.colPhone,
            this.colEMail,
            this.colOperator,
            this.colStatus,
            this.colDateCreate,
            this.colExport,
            this.colInterval});
			this.extendedDataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.extendedDataGridTableStyle1.MappingName = "table";
			// 
			// colOrderNum
			// 
			this.colOrderNum.FieldName = "o.orderNum";
			this.colOrderNum.FilterFieldName = null;
			this.colOrderNum.Format = "";
			this.colOrderNum.FormatInfo = null;
			this.colOrderNum.HeaderText = "№";
			this.colOrderNum.MappingName = "orderNum";
			this.columnMenuExtender1.SetMenu(this.colOrderNum, this.menuFilterSort1);
			this.colOrderNum.ReadOnly = true;
			this.colOrderNum.Width = 75;
			// 
			// colBuyer
			// 
			this.colBuyer.FieldName = "dbo.fOrderBuyer(o.OID)";
			this.colBuyer.FilterFieldName = null;
			this.colBuyer.Format = "";
			this.colBuyer.FormatInfo = null;
			this.colBuyer.HeaderText = "Покупатель";
			this.colBuyer.MappingName = "buyer";
			this.columnMenuExtender1.SetMenu(this.colBuyer, this.menuFilterSort1);
			this.colBuyer.ReadOnly = true;
			this.colBuyer.Width = 200;
			// 
			// colPhone
			// 
			this.colPhone.FieldName = "o.contactPhone";
			this.colPhone.FilterFieldName = null;
			this.colPhone.Format = "";
			this.colPhone.FormatInfo = null;
			this.colPhone.HeaderText = "Телефон";
			this.colPhone.MappingName = "contactPhone";
			this.columnMenuExtender1.SetMenu(this.colPhone, this.menuFilterSort1);
			this.colPhone.ReadOnly = true;
			this.colPhone.Width = 150;
			// 
			// colEMail
			// 
			this.colEMail.FieldName = "o.email";
			this.colEMail.FilterFieldName = null;
			this.colEMail.Format = "";
			this.colEMail.FormatInfo = null;
			this.colEMail.HeaderText = "e-Mail";
			this.colEMail.MappingName = "email";
			this.columnMenuExtender1.SetMenu(this.colEMail, this.menuFilterSort1);
			this.colEMail.ReadOnly = true;
			this.colEMail.Width = 150;
			// 
			// colOperator
			// 
			this.colOperator.FieldName = "dbo.NK(o.operator)";
			this.colOperator.FilterFieldName = null;
			this.colOperator.Format = "";
			this.colOperator.FormatInfo = null;
			this.colOperator.HeaderText = "Оператор";
			this.colOperator.MappingName = "operator_NK";
			this.columnMenuExtender1.SetMenu(this.colOperator, this.menuFilterSort1);
			this.colOperator.ReadOnly = true;
			this.colOperator.Width = 200;
			// 
			// colStatus
			// 
			this.colStatus.FieldName = "o.status";
			this.colStatus.FilterFieldName = null;
			this.colStatus.Format = "";
			this.colStatus.FormatInfo = null;
			this.colStatus.HeaderText = "Статус";
			this.colStatus.MappingName = "status";
			this.columnMenuExtender1.SetMenu(this.colStatus, this.menuFilterSort1);
			this.colStatus.ReadOnly = true;
			this.colStatus.Width = 150;
			// 
			// colDateCreate
			// 
			this.colDateCreate.FieldName = "o1.dateCreate";
			this.colDateCreate.FilterFieldName = null;
			this.colDateCreate.Format = "";
			this.colDateCreate.FormatInfo = null;
			this.colDateCreate.HeaderText = "Создан";
			this.colDateCreate.MappingName = "dateCreate";
			this.columnMenuExtender1.SetMenu(this.colDateCreate, this.menuFilterSort1);
			this.colDateCreate.ReadOnly = true;
			this.colDateCreate.Width = 75;
			// 
			// colExport
			// 
			this.colExport.AllowNull = false;
			this.colExport.FieldName = null;
			this.colExport.FilterFieldName = null;
			this.colExport.MappingName = "export";
			this.colExport.NullValue = "";
			this.colExport.Width = 75;
			// 
			// colInterval
			// 
			this.colInterval.FieldName = null;
			this.colInterval.FilterFieldName = null;
			this.colInterval.Format = "";
			this.colInterval.FormatInfo = null;
			this.colInterval.HeaderText = "Интервал доставки";
			this.colInterval.MappingName = "interval";
			this.columnMenuExtender1.SetMenu(this.colInterval, this.menuFilterSort1);
			this.colInterval.ReadOnly = true;
			this.colInterval.Width = 75;
			// 
			// sfdExportFile
			// 
			this.sfdExportFile.DefaultExt = "xml";
			this.sfdExportFile.Filter = "XML файлы|*.xml|Все файлы|*.*";
			// 
			// ExportOrders
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
			this.ClientSize = new System.Drawing.Size(776, 437);
			this.Controls.Add(this.dgOrders);
			this.Controls.Add(this.panel1);
			this.Name = "ExportOrders";
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Экспорт заказов в 1С";
			this.Load += new System.EventHandler(this.fmExportOrders_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgOrders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
	}
}
