﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using MetaData;
using Telerik.WinControls.UI;

namespace ELBClient.Forms.Reports
{
    /// <summary>
    ///     Summary description for fmExportOrders.
    /// </summary>
    public partial class ExportOrders : ListForm
    {
        private readonly Hashtable intervalNames = new Hashtable();
        private readonly Guid serviceOID = new Guid("5761D71F-7AE3-4216-8196-FD3D8E757210");

        /// <summary>
        ///     Required designer variable.
        /// </summary>
        public ExportOrders()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            dgOrders.GetForeColorForRow += colStatus_OnGetForeColorForRow;
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>

        #endregion
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void fmExportOrders_Load(object sender, EventArgs e)
        {
            dtpDateBegin.Value = DateTime.Today;
            dtpDateEnd.Value = DateTime.Today;
            LoadLookup();
        }

        private void LoadLookup()
        {
            string xml = lp.GetLookupValues("COrder");
            colStatus.ComboBox.LoadXml(xml, "statusType");
            colInterval.ComboBox.LoadXml(xml, "intervalN");
            foreach (RadListDataItem li in colInterval.ComboBox.Items)
            {
                intervalNames[li.Value] = li.Text;
            }
            //xml = GetResourceString("fmListOrderLookup.xml");
            cbShopType.Items.Clear();
            cbShopType.LoadXml(xml, "shopTypeN");
            cbShopType.SelectedIndex = 0;
        }

        public override void LoadData()
        {
//      string whereClause = dgOrders.GetWhereClause();
//      string orderClause = dgOrders.GetOrderClause();
//      if(orderClause == "") orderClause = "o1.dateCreate desc";
//      string sql = @"
//SELECT
//	o.OID,
//	o.orderNum,
//	buyer = dbo.fOrderBuyer(o.OID),
//	o.contactPhone,
//	o.contactPhone2,
//	o.contactPhone3,
//	o.email,
//	operator_NK = dbo.NK(o.operator),
//	o.status,
//	payment = d.name,
//	status = s.name,
//	o.selfComment,
//	o1.dateCreate,
//	o.adres,
//	o.cityName,
//	o.streetName,
//	o.house,
//	o.korp,
//	o.flat,
//	o.interval,
//	export = convert(bit, 1)
//FROM
//	t_Order o 
//	inner join t_Object o1 on o.OID = o1.OID
//	inner join t_TypeDelivery d on o.delivery = d.ID
//	inner join t_TypeStatus s on o.status = s.status
//WHERE
//	o.shopType = "+cbShopType.SelectedValue+@" and
//	o1.dateCreate BETWEEN '"+dtpDateBegin.Value.Date.ToString("yyyy-MM-dd")+"' and '"+dtpDateEnd.Value.Date.AddDays(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss")+@"'";
//      if(whereClause != "") {
//        sql += @"
//	and "+whereClause;
//      }
//      sql += @"
//ORDER BY
//	"+orderClause;
//      DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
//      dgOrders.SetDataBinding(ds, "Table");
//      dgOrders.DisableNewDel();
//      cbAll.Checked = true;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private Brush colStatus_OnGetForeColorForRow(int rowNum, CurrencyManager source)
        {
            var dv = (DataView) source.List;
            DataRowView dr = dv[rowNum];
            Brush br = null;
            switch ((int) dr["status"])
            {
                case 1:
                    if ((string) dr["operator_NK"] == "") br = new SolidBrush(Color.Green);
                    break;
                case 4:
                    br = new SolidBrush(Color.Red);
                    break;
                case 6:
                    br = new SolidBrush(Color.Gray);
                    break;
                case 7:
                    br = new SolidBrush(Color.DarkGray);
                    break;
            }
            return br;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (sfdExportFile.ShowDialog(this) != DialogResult.OK) return;
            var doc = new XmlDocument();
            string sql = @"
SELECT 
	g.OID,
	ID = ISNULL(gc.id1c, gi.ID), 
	oc.price,
	oc.amount
FROM
	t_OrderContents oc
	inner join t_Goods g on oc.goodsOID = g.OID And oc.OID = @OID
	inner join t_GoodsIDs gi on g.OID = gi.OID and gi.shopType = " + cbShopType.SelectedValue + @"
	left join t_GoodsID21C gc on gi.ID = gc.ID and gc.shopType = " + cbShopType.SelectedValue + @"
";

            doc.LoadXml("<?xml version=\"1.0\" encoding=\"Windows-1251\" ?><orders></orders>");
            foreach (DataRow dr in ((DataSetISM) dgOrders.DataSource).Table.Rows)
            {
                if (!(bool) dr["export"]) continue;
                XmlNode order = doc.DocumentElement.AppendChild(doc.CreateElement("order"));
                order.Attributes.Append(doc.CreateAttribute("orderNum")).InnerText = dr["orderNum"].ToString();
                order.Attributes.Append(doc.CreateAttribute("dateCreate")).InnerText =
                    ((DateTime) dr["dateCreate"]).ToString("dd.MM.yyyy");
                order.Attributes.Append(doc.CreateAttribute("clientID")).InnerText = dr["email"].ToString();
                order.Attributes.Append(doc.CreateAttribute("clientName")).InnerText = dr["buyer"].ToString();
                order.Attributes.Append(doc.CreateAttribute("contactPhone")).InnerText = dr["contactPhone"].ToString();
                order.Attributes.Append(doc.CreateAttribute("contactPhone2")).InnerText = dr["contactPhone2"].ToString();
                order.Attributes.Append(doc.CreateAttribute("contactPhone3")).InnerText = dr["contactPhone3"].ToString();
                order.Attributes.Append(doc.CreateAttribute("operator")).InnerText = dr["operator_NK"].ToString();
                order.Attributes.Append(doc.CreateAttribute("comment")).InnerText = dr["selfComment"].ToString();
                order.Attributes.Append(doc.CreateAttribute("interval")).InnerText =
                    (string) intervalNames[(int) dr["interval"]];
                order.Attributes.Append(doc.CreateAttribute("address")).InnerText = dr["adres"].ToString();
                order.Attributes.Append(doc.CreateAttribute("cityName")).InnerText = dr["cityName"].ToString();
                order.Attributes.Append(doc.CreateAttribute("streetName")).InnerText = dr["streetName"].ToString();
                order.Attributes.Append(doc.CreateAttribute("house")).InnerText = dr["house"].ToString();
                order.Attributes.Append(doc.CreateAttribute("korp")).InnerText = dr["korp"].ToString();
                order.Attributes.Append(doc.CreateAttribute("flat")).InnerText = dr["flat"].ToString();
                order.Attributes.Append(doc.CreateAttribute("payment")).InnerText = dr["payment"].ToString();
                order.Attributes.Append(doc.CreateAttribute("status")).InnerText = dr["status"].ToString();
                string sql1 = sql.Replace("@OID", "'" + dr["OID"] + "'");
                var items = new DataSetISM(lp.GetDataSetSql(sql1));
                foreach (DataRow drItem in items.Table.Rows)
                {
                    XmlNode item = order.AppendChild(doc.CreateElement("item"));
                    item.Attributes.Append(doc.CreateAttribute("ID")).InnerText =
                        ((Int64) drItem["ID"]).ToString("00000");
                    if (drItem["price"] != DBNull.Value)
                        item.Attributes.Append(doc.CreateAttribute("price")).InnerText =
                            ((decimal) drItem["price"]).ToString("#.00");
                    item.Attributes.Append(doc.CreateAttribute("amount")).InnerText = drItem["amount"].ToString();
                    if ((Guid) drItem["OID"] != serviceOID)
                        item.Attributes.Append(doc.CreateAttribute("type")).InnerText = "товар";
                    else item.Attributes.Append(doc.CreateAttribute("type")).InnerText = "услуга";
                }
            }
            var wrt = new XmlTextWriter(sfdExportFile.FileName, Encoding.GetEncoding(1251));
            wrt.Formatting = Formatting.Indented;
            doc.Save(wrt);
            wrt.Close();
            MessageBox.Show(this, @"Экспорт заказов закончен");
        }

        private void cbAll_CheckedChanged(object sender, EventArgs e)
        {
            bool check = false;
            if (cbAll.Checked) check = true;
            foreach (DataRow dr in ((DataSetISM) dgOrders.DataSource).Table.Rows)
            {
                dr["export"] = check;
            }
            ((DataSetISM) dgOrders.DataSource).AcceptChanges();
        }

        private void cbShopType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgOrders_Reload(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}