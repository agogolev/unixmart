﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ecommerce.Extensions;
using MetaData;

namespace ELBClient.Forms.Reports
{
    public partial class YandexReport : ListForm
    {
        public YandexReport()
        {
            InitializeComponent();
            yandexUrlColumn.ButtonClick += UrlColumn_ButtonClick;
            fullUrlColumn.ButtonClick += UrlColumn_ButtonClick;
        }

        private void UrlColumn_ButtonClick(object sender, ColumnMenuExtender.DataGridCellEventArgs e)
        {
            object o = e.CellValue;
            if (o != null && o != DBNull.Value)
            {
                Process.Start(o.ToString());
            }
        }

        private void YandexReport_Load(object sender, EventArgs e)
        {
            dtpDateBegin.Value = DateTime.Today.AddDays(-14);
            dtpDateEnd.Value = DateTime.Today;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            var sql = "SELECT dateOccur, yandexUrl, fullUrl FROM rpt_YandexUrl WHERE dateOccur BETWEEN {0} and {1} ORDER BY id";
            var dataSet = new DataSetISM(lp.GetDataSetSql(string.Format(sql, dtpDateBegin.Value.ToSqlString(), dtpDateEnd.Value.AddDays(1).ToSqlString())));
            dgUrls.SetDataBinding(dataSet, "Table");
            dgUrls.DisableNewDel();
        }
    }
}
