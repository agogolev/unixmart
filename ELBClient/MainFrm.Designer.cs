﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Collections;
using Telerik.WinControls;

namespace ELBClient
{
	public partial class MainFrm
	{
		#region Windows Form Designer generated code
		private System.Windows.Forms.StatusBarPanel pnlUser;
		private System.Windows.Forms.StatusBarPanel pnlDept;
		private System.Windows.Forms.StatusBarPanel pnlDate;
		private System.Windows.Forms.StatusBarPanel pnlTime;
		private System.Windows.Forms.Timer timer;
		private System.Windows.Forms.StatusBar sbStatus;
		private System.Windows.Forms.StatusBarPanel pnlOther;
		private System.Windows.Forms.Timer pingTimer;

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.sbStatus = new System.Windows.Forms.StatusBar();
            this.pnlUser = new System.Windows.Forms.StatusBarPanel();
            this.pnlDept = new System.Windows.Forms.StatusBarPanel();
            this.pnlDate = new System.Windows.Forms.StatusBarPanel();
            this.pnlTime = new System.Windows.Forms.StatusBarPanel();
            this.pnlOther = new System.Windows.Forms.StatusBarPanel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pingTimer = new System.Windows.Forms.Timer(this.components);
            this.FileMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoginMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LogoutMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.MenuSep2Rad = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.CloseMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.SiteMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.CatalogTreeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ObjectTreeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LinkMasterMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LinkReviewMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.CommodityGroupMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.SpravMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.OrderMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.WholesaleOrderMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.BinaryDataMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ThemeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.CommentMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.CompanyMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.MasterCatalogMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.DisableExpressDeliveryMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.PeopleMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.PromoMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ArticleMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.GoodsMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.GoodsItemMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.TreeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.AddPriceMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ActionMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.WebConfigMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.BannerRootMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.AdvertCompanyMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.AdvertItemMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.AdvertImageMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ReportAdvertShowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.AdvertPlaceMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LandingPageMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.YandexReportMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.DefaultTypeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.TepmlatesWorkMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadParamsMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ModifyTemplateMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadKeywordsMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadAccessoriesMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadAnalogsMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadProductTypeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ProcessTochkaMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadImagesMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadMoscowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadSitePricesMoscowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadBidsMoscowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ExportOrdersFromSVMoscowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadDescriptionMoscowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadSpbMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadBidsSpbMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ExportOrdersFromSVSpbMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadDescriptionSpbMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ListJobLogMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.UploadGoods4YandexMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.LoadInStockMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.ExportMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ExportOrderMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.WindowMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ItemTHMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ItemTVMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.ItemCascadeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.VisualThemeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.AquaMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.BreezeMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.DesertMenu = new Telerik.WinControls.UI.RadMenuItem();
            this.Office2010Menu = new Telerik.WinControls.UI.RadMenuItem();
            this.Windows7Menu = new Telerik.WinControls.UI.RadMenuItem();
            this.aquaTheme1 = new Telerik.WinControls.Themes.AquaTheme();
            this.breezeTheme1 = new Telerik.WinControls.Themes.BreezeTheme();
            this.desertTheme1 = new Telerik.WinControls.Themes.DesertTheme();
            this.office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.MainMenu = new Telerik.WinControls.UI.RadMenu();
            this.BrowseDefaultTitleMenu = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pnlUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // sbStatus
            // 
            this.sbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sbStatus.Location = new System.Drawing.Point(0, 847);
            this.sbStatus.Name = "sbStatus";
            this.sbStatus.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.pnlUser,
            this.pnlDept,
            this.pnlDate,
            this.pnlTime,
            this.pnlOther});
            this.sbStatus.ShowPanels = true;
            this.sbStatus.Size = new System.Drawing.Size(912, 20);
            this.sbStatus.TabIndex = 1;
            // 
            // pnlUser
            // 
            this.pnlUser.Name = "pnlUser";
            this.pnlUser.Width = 150;
            // 
            // pnlDept
            // 
            this.pnlDept.Name = "pnlDept";
            this.pnlDept.Width = 150;
            // 
            // pnlDate
            // 
            this.pnlDate.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.pnlDate.Name = "pnlDate";
            // 
            // pnlTime
            // 
            this.pnlTime.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.pnlTime.Name = "pnlTime";
            // 
            // pnlOther
            // 
            this.pnlOther.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.pnlOther.Name = "pnlOther";
            this.pnlOther.Width = 395;
            // 
            // timer
            // 
            this.timer.Interval = 30000;
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // pingTimer
            // 
            this.pingTimer.Interval = 10000;
            this.pingTimer.Tick += new System.EventHandler(this.PingTimerTick);
            // 
            // FileMenu
            // 
            this.FileMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LoginMenu,
            this.LogoutMenu,
            this.MenuSep2Rad,
            this.CloseMenu});
            this.FileMenu.Name = "FileMenu";
            this.FileMenu.Text = "Программа ";
            // 
            // LoginMenu
            // 
            this.LoginMenu.Name = "LoginMenu";
            this.LoginMenu.Text = "Подключиться";
            this.LoginMenu.Click += new System.EventHandler(this.LoginMenu_Click);
            // 
            // LogoutMenu
            // 
            this.LogoutMenu.Enabled = false;
            this.LogoutMenu.Name = "LogoutMenu";
            this.LogoutMenu.Text = "Отключиться";
            this.LogoutMenu.Click += new System.EventHandler(this.LogoutMenu_Click);
            // 
            // MenuSep2Rad
            // 
            this.MenuSep2Rad.Name = "MenuSep2Rad";
            this.MenuSep2Rad.Text = "radMenuSeparatorItem1";
            this.MenuSep2Rad.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CloseMenu
            // 
            this.CloseMenu.Name = "CloseMenu";
            this.CloseMenu.Text = "Выход";
            this.CloseMenu.Click += new System.EventHandler(this.CloseMenu_Click);
            // 
            // SiteMenu
            // 
            this.SiteMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.CatalogTreeMenu,
            this.ObjectTreeMenu,
            this.LinkMasterMenu,
            this.LinkReviewMenu});
            this.SiteMenu.Name = "SiteMenu";
            this.SiteMenu.Text = "Сайт";
            this.SiteMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // CatalogTreeMenu
            // 
            this.CatalogTreeMenu.Name = "CatalogTreeMenu";
            this.CatalogTreeMenu.Text = "Каталог сайта";
            this.CatalogTreeMenu.Click += new System.EventHandler(this.CatalogTreeMenu_Click);
            // 
            // ObjectTreeMenu
            // 
            this.ObjectTreeMenu.Name = "ObjectTreeMenu";
            this.ObjectTreeMenu.Text = "Мастер каталог";
            this.ObjectTreeMenu.Click += new System.EventHandler(this.ObjectTreeMenu_Click);
            // 
            // LinkMasterMenu
            // 
            this.LinkMasterMenu.Name = "LinkMasterMenu";
            this.LinkMasterMenu.Text = "Привязка мастера";
            this.LinkMasterMenu.Click += new System.EventHandler(this.LinkMasterMenu_Click);
            // 
            // LinkReviewMenu
            // 
            this.LinkReviewMenu.Name = "LinkReviewMenu";
            this.LinkReviewMenu.Text = "Привязка обзоров";
            this.LinkReviewMenu.Click += new System.EventHandler(this.LinkReviewMenu_Click);
            // 
            // CommodityGroupMenu
            // 
            this.CommodityGroupMenu.AccessibleDescription = "Генерация XML";
            this.CommodityGroupMenu.AccessibleName = "Генерация XML";
            this.CommodityGroupMenu.Name = "CommodityGroupMenu";
            this.CommodityGroupMenu.Text = "Товарные группы";
            this.CommodityGroupMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.CommodityGroupMenu.Click += new System.EventHandler(this.XMLGenerationMenu_Click);
            // 
            // SpravMenu
            // 
            this.SpravMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.OrderMenu,
            this.WholesaleOrderMenu,
            this.BinaryDataMenu,
            this.ThemeMenu,
            this.CommentMenu,
            this.CompanyMenu,
            this.MasterCatalogMenu,
            this.DisableExpressDeliveryMenu,
            this.PeopleMenu,
            this.PromoMenu,
            this.ArticleMenu,
            this.GoodsMenu,
            this.GoodsItemMenu,
            this.TreeMenu,
            this.AddPriceMenu,
            this.CommodityGroupMenu,
            this.ActionMenu,
            this.WebConfigMenu,
            this.BannerRootMenu,
            this.LandingPageMenu,
            this.YandexReportMenu,
            this.radMenuItem3,
            this.DefaultTypeMenu,
            this.BrowseDefaultTitleMenu});
            this.SpravMenu.Name = "SpravMenu";
            this.SpravMenu.Text = "Справочники";
            this.SpravMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // OrderMenu
            // 
            this.OrderMenu.Name = "OrderMenu";
            this.OrderMenu.Text = "Заказы";
            this.OrderMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.OrderMenu.Click += new System.EventHandler(this.OrderMenu_Click);
            // 
            // WholesaleOrderMenu
            // 
            this.WholesaleOrderMenu.Name = "WholesaleOrderMenu";
            this.WholesaleOrderMenu.Text = "Оптовые заказы";
            this.WholesaleOrderMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.WholesaleOrderMenu.Click += new System.EventHandler(this.WholesaleOrderMenu_Click);
            // 
            // BinaryDataMenu
            // 
            this.BinaryDataMenu.Name = "BinaryDataMenu";
            this.BinaryDataMenu.Text = "Картинки, файлы";
            this.BinaryDataMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.BinaryDataMenu.Click += new System.EventHandler(this.BinaryDataMenu_Click);
            // 
            // ThemeMenu
            // 
            this.ThemeMenu.Name = "ThemeMenu";
            this.ThemeMenu.Text = "Категории";
            this.ThemeMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ThemeMenu.Click += new System.EventHandler(this.ThemeMenu_Click);
            // 
            // CommentMenu
            // 
            this.CommentMenu.Name = "CommentMenu";
            this.CommentMenu.Text = "Комментарии";
            this.CommentMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.CommentMenu.Click += new System.EventHandler(this.CommentMenu_Click);
            // 
            // CompanyMenu
            // 
            this.CompanyMenu.Name = "CompanyMenu";
            this.CompanyMenu.Text = "Компании";
            this.CompanyMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.CompanyMenu.Click += new System.EventHandler(this.CompanyMenu_Click);
            // 
            // MasterCatalogMenu
            // 
            this.MasterCatalogMenu.Name = "MasterCatalogMenu";
            this.MasterCatalogMenu.Text = "Мастер категории";
            this.MasterCatalogMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.MasterCatalogMenu.Click += new System.EventHandler(this.MasterCatalogMenu_Click);
            // 
            // DisableExpressDeliveryMenu
            // 
            this.DisableExpressDeliveryMenu.Name = "DisableExpressDeliveryMenu";
            this.DisableExpressDeliveryMenu.Text = "Отмена экспресс доставки";
            this.DisableExpressDeliveryMenu.Click += new System.EventHandler(this.DisableExpressDeliveryMenu_Click);
            // 
            // PeopleMenu
            // 
            this.PeopleMenu.AccessibleDescription = "radMenuItem30";
            this.PeopleMenu.AccessibleName = "radMenuItem30";
            this.PeopleMenu.Name = "PeopleMenu";
            this.PeopleMenu.Text = "Персоны";
            this.PeopleMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.PeopleMenu.Click += new System.EventHandler(this.PeopleMenu_Click);
            // 
            // PromoMenu
            // 
            this.PromoMenu.AccessibleDescription = "radMenuItem31";
            this.PromoMenu.AccessibleName = "radMenuItem31";
            this.PromoMenu.Name = "PromoMenu";
            this.PromoMenu.Text = "Промо-блоки";
            this.PromoMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.PromoMenu.Click += new System.EventHandler(this.PromoMenu_Click);
            // 
            // ArticleMenu
            // 
            this.ArticleMenu.AccessibleDescription = "radMenuItem32";
            this.ArticleMenu.AccessibleName = "radMenuItem32";
            this.ArticleMenu.Name = "ArticleMenu";
            this.ArticleMenu.Text = "Статьи";
            this.ArticleMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ArticleMenu.Click += new System.EventHandler(this.ArticleMenu_Click);
            // 
            // GoodsMenu
            // 
            this.GoodsMenu.AccessibleDescription = "radMenuItem33";
            this.GoodsMenu.AccessibleName = "radMenuItem33";
            this.GoodsMenu.Name = "GoodsMenu";
            this.GoodsMenu.Text = "Товары";
            this.GoodsMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.GoodsMenu.Click += new System.EventHandler(this.GoodsMenu_Click);
            // 
            // GoodsItemMenu
            // 
            this.GoodsItemMenu.Name = "GoodsItemMenu";
            this.GoodsItemMenu.Text = "Уценённые товары";
            this.GoodsItemMenu.Click += new System.EventHandler(this.GoodsItemMenu_Click);
            // 
            // TreeMenu
            // 
            this.TreeMenu.AccessibleDescription = "radMenuItem34";
            this.TreeMenu.AccessibleName = "radMenuItem34";
            this.TreeMenu.Name = "TreeMenu";
            this.TreeMenu.Text = "Деревья";
            this.TreeMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.TreeMenu.Click += new System.EventHandler(this.TreeMenu_Click);
            // 
            // AddPriceMenu
            // 
            this.AddPriceMenu.AccessibleDescription = "radMenuItem35";
            this.AddPriceMenu.AccessibleName = "radMenuItem35";
            this.AddPriceMenu.Name = "AddPriceMenu";
            this.AddPriceMenu.Text = "Наценки на категории";
            this.AddPriceMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.AddPriceMenu.Click += new System.EventHandler(this.AddPriceMenu_Click);
            // 
            // ActionMenu
            // 
            this.ActionMenu.Name = "ActionMenu";
            this.ActionMenu.Text = "Акции";
            this.ActionMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ActionMenu.Click += new System.EventHandler(this.ActionMenu_Click);
            // 
            // WebConfigMenu
            // 
            this.WebConfigMenu.Name = "WebConfigMenu";
            this.WebConfigMenu.Text = "Настройки магазина";
            this.WebConfigMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.WebConfigMenu.Click += new System.EventHandler(this.WebConfigMenu_Click);
            // 
            // BannerRootMenu
            // 
            this.BannerRootMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.AdvertCompanyMenu,
            this.AdvertItemMenu,
            this.AdvertImageMenu,
            this.ReportAdvertShowMenu,
            this.radMenuSeparatorItem2,
            this.AdvertPlaceMenu});
            this.BannerRootMenu.Name = "BannerRootMenu";
            this.BannerRootMenu.Text = "Банеры";
            this.BannerRootMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // AdvertCompanyMenu
            // 
            this.AdvertCompanyMenu.Name = "AdvertCompanyMenu";
            this.AdvertCompanyMenu.Text = "Рекламные компании";
            this.AdvertCompanyMenu.Click += new System.EventHandler(this.AdvertCompanyMenu_Click);
            // 
            // AdvertItemMenu
            // 
            this.AdvertItemMenu.Name = "AdvertItemMenu";
            this.AdvertItemMenu.Text = "Банеры";
            this.AdvertItemMenu.Click += new System.EventHandler(this.AdvertItemMenu_Click);
            // 
            // AdvertImageMenu
            // 
            this.AdvertImageMenu.Name = "AdvertImageMenu";
            this.AdvertImageMenu.Text = "Картинки";
            this.AdvertImageMenu.Click += new System.EventHandler(this.AdvertImageMenu_Click);
            // 
            // ReportAdvertShowMenu
            // 
            this.ReportAdvertShowMenu.Name = "ReportAdvertShowMenu";
            this.ReportAdvertShowMenu.Text = "Отчёт по показам";
            this.ReportAdvertShowMenu.Click += new System.EventHandler(this.ReportAdvertShowMenu_Click);
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AdvertPlaceMenu
            // 
            this.AdvertPlaceMenu.AccessibleDescription = "radMenuItem1";
            this.AdvertPlaceMenu.AccessibleName = "radMenuItem1";
            this.AdvertPlaceMenu.Name = "AdvertPlaceMenu";
            this.AdvertPlaceMenu.Text = "Рекламные места";
            this.AdvertPlaceMenu.Click += new System.EventHandler(this.radMenuItem1_Click);
            // 
            // LandingPageMenu
            // 
            this.LandingPageMenu.Name = "LandingPageMenu";
            this.LandingPageMenu.Text = "Посадочные страницы";
            this.LandingPageMenu.Click += new System.EventHandler(this.LandingPageMenu_Click);
            // 
            // YandexReportMenu
            // 
            this.YandexReportMenu.Name = "YandexReportMenu";
            this.YandexReportMenu.Text = "Страницы Яндекса";
            this.YandexReportMenu.Click += new System.EventHandler(this.YandexReportMenu_Click);
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "Промокоды";
            // 
            // DefaultTypeMenu
            // 
            this.DefaultTypeMenu.Name = "DefaultTypeMenu";
            this.DefaultTypeMenu.Text = "Тайтлы по умолчанию";
            this.DefaultTypeMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.DefaultTypeMenu.Click += new System.EventHandler(this.DefaultTypeMenu_Click);
            // 
            // TepmlatesWorkMenu
            // 
            this.TepmlatesWorkMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LoadParamsMenu,
            this.ModifyTemplateMenu});
            this.TepmlatesWorkMenu.Name = "TepmlatesWorkMenu";
            this.TepmlatesWorkMenu.Text = "Работа с описаниями";
            this.TepmlatesWorkMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // LoadParamsMenu
            // 
            this.LoadParamsMenu.Name = "LoadParamsMenu";
            this.LoadParamsMenu.Text = "Загрузка описаний из Excel";
            this.LoadParamsMenu.Click += new System.EventHandler(this.LoadParamsMenu_Click);
            // 
            // ModifyTemplateMenu
            // 
            this.ModifyTemplateMenu.Name = "ModifyTemplateMenu";
            this.ModifyTemplateMenu.Text = "Работа с шаблонами";
            this.ModifyTemplateMenu.Click += new System.EventHandler(this.ModifyTemplateMenu_Click);
            // 
            // LoadMenu
            // 
            this.LoadMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LoadKeywordsMenu,
            this.LoadAccessoriesMenu,
            this.LoadAnalogsMenu,
            this.LoadProductTypeMenu,
            this.ProcessTochkaMenu,
            this.LoadImagesMenu,
            this.LoadMoscowMenu,
            this.LoadSpbMenu,
            this.ListJobLogMenu,
            this.UploadGoods4YandexMenu,
            this.LoadInStockMenu,
            this.radMenuItem2});
            this.LoadMenu.Name = "LoadMenu";
            this.LoadMenu.Text = "Загрузка";
            this.LoadMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // LoadKeywordsMenu
            // 
            this.LoadKeywordsMenu.Name = "LoadKeywordsMenu";
            this.LoadKeywordsMenu.Text = "Ключевых слов для товаров";
            this.LoadKeywordsMenu.Click += new System.EventHandler(this.LoadKeywordsMenu_Click);
            // 
            // LoadAccessoriesMenu
            // 
            this.LoadAccessoriesMenu.Name = "LoadAccessoriesMenu";
            this.LoadAccessoriesMenu.Text = "Сопутствующих товаров";
            this.LoadAccessoriesMenu.Click += new System.EventHandler(this.LoadAccessoriesMenu_Click);
            // 
            // LoadAnalogsMenu
            // 
            this.LoadAnalogsMenu.Name = "LoadAnalogsMenu";
            this.LoadAnalogsMenu.Text = "Аналогов";
            this.LoadAnalogsMenu.Click += new System.EventHandler(this.LoadAnalogsMenu_Click);
            // 
            // LoadProductTypeMenu
            // 
            this.LoadProductTypeMenu.Name = "LoadProductTypeMenu";
            this.LoadProductTypeMenu.Text = "Типов товаров";
            this.LoadProductTypeMenu.Click += new System.EventHandler(this.LoadProductTypeMenu_Click);
            // 
            // ProcessTochkaMenu
            // 
            this.ProcessTochkaMenu.Name = "ProcessTochkaMenu";
            this.ProcessTochkaMenu.Text = "Поиск соответствий в БД Точка";
            this.ProcessTochkaMenu.Click += new System.EventHandler(this.ProcessTochkaMenu_Click);
            // 
            // LoadImagesMenu
            // 
            this.LoadImagesMenu.Name = "LoadImagesMenu";
            this.LoadImagesMenu.Text = "Загрузка картинок товаров";
            this.LoadImagesMenu.Click += new System.EventHandler(this.LoadImagesMenu_Click);
            // 
            // LoadMoscowMenu
            // 
            this.LoadMoscowMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LoadSitePricesMoscowMenu,
            this.LoadBidsMoscowMenu,
            this.ExportOrdersFromSVMoscowMenu,
            this.LoadDescriptionMoscowMenu});
            this.LoadMoscowMenu.Name = "LoadMoscowMenu";
            this.LoadMoscowMenu.Text = "Москва";
            // 
            // LoadSitePricesMoscowMenu
            // 
            this.LoadSitePricesMoscowMenu.Name = "LoadSitePricesMoscowMenu";
            this.LoadSitePricesMoscowMenu.Text = "Загрузка цен";
            this.LoadSitePricesMoscowMenu.Click += new System.EventHandler(this.LoadSitePricesMoscowMenu_Click);
            // 
            // LoadBidsMoscowMenu
            // 
            this.LoadBidsMoscowMenu.Name = "LoadBidsMoscowMenu";
            this.LoadBidsMoscowMenu.Text = "Загрузка ставок";
            this.LoadBidsMoscowMenu.Click += new System.EventHandler(this.LoadBidsMoscowMenu_Click);
            // 
            // ExportOrdersFromSVMoscowMenu
            // 
            this.ExportOrdersFromSVMoscowMenu.Name = "ExportOrdersFromSVMoscowMenu";
            this.ExportOrdersFromSVMoscowMenu.Text = "Выгрузка заказов из SV";
            // 
            // LoadDescriptionMoscowMenu
            // 
            this.LoadDescriptionMoscowMenu.Name = "LoadDescriptionMoscowMenu";
            this.LoadDescriptionMoscowMenu.Text = "Загрузка описаний";
            this.LoadDescriptionMoscowMenu.Click += new System.EventHandler(this.LoadDescriptionMoscowMenu_Click);
            // 
            // LoadSpbMenu
            // 
            this.LoadSpbMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LoadBidsSpbMenu,
            this.ExportOrdersFromSVSpbMenu,
            this.LoadDescriptionSpbMenu});
            this.LoadSpbMenu.Name = "LoadSpbMenu";
            this.LoadSpbMenu.Text = "Санкт-Петербург";
            // 
            // LoadBidsSpbMenu
            // 
            this.LoadBidsSpbMenu.Name = "LoadBidsSpbMenu";
            this.LoadBidsSpbMenu.Text = "Загрузка ставок";
            this.LoadBidsSpbMenu.Click += new System.EventHandler(this.LoadBidsSpbMenu_Click);
            // 
            // ExportOrdersFromSVSpbMenu
            // 
            this.ExportOrdersFromSVSpbMenu.Name = "ExportOrdersFromSVSpbMenu";
            this.ExportOrdersFromSVSpbMenu.Text = "Выгрузка заказов из SV";
            // 
            // LoadDescriptionSpbMenu
            // 
            this.LoadDescriptionSpbMenu.Name = "LoadDescriptionSpbMenu";
            this.LoadDescriptionSpbMenu.Text = "Загрузка описаний";
            this.LoadDescriptionSpbMenu.Click += new System.EventHandler(this.LoadDescriptionSpbMenu_Click);
            // 
            // ListJobLogMenu
            // 
            this.ListJobLogMenu.Name = "ListJobLogMenu";
            this.ListJobLogMenu.Text = "Просмотр лога загрузки товаров";
            this.ListJobLogMenu.Click += new System.EventHandler(this.ListJobLogMenu_Click);
            // 
            // UploadGoods4YandexMenu
            // 
            this.UploadGoods4YandexMenu.Name = "UploadGoods4YandexMenu";
            this.UploadGoods4YandexMenu.Text = "Проверка загрузки Яндекс";
            // 
            // LoadInStockMenu
            // 
            this.LoadInStockMenu.Name = "LoadInStockMenu";
            this.LoadInStockMenu.Text = "Загрузка остатков";
            this.LoadInStockMenu.Click += new System.EventHandler(this.LoadInStockMenu_Click);
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Test";
            this.radMenuItem2.Click += new System.EventHandler(this.radMenuItem2_Click);
            // 
            // ExportMenu
            // 
            this.ExportMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportOrderMenu,
            this.radMenuItem1});
            this.ExportMenu.Name = "ExportMenu";
            this.ExportMenu.Text = "Экспорт";
            this.ExportMenu.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ExportOrderMenu
            // 
            this.ExportOrderMenu.Name = "ExportOrderMenu";
            this.ExportOrderMenu.Text = "Заказы";
            this.ExportOrderMenu.Click += new System.EventHandler(this.ExportOrderMenu_Click);
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "test";
            this.radMenuItem1.Click += new System.EventHandler(this.radMenuItem1_Click_1);
            // 
            // WindowMenu
            // 
            this.WindowMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ItemTHMenu,
            this.ItemTVMenu,
            this.ItemCascadeMenu,
            this.radMenuSeparatorItem1});
            this.WindowMenu.MdiList = true;
            this.WindowMenu.Name = "WindowMenu";
            this.WindowMenu.Text = "Окно";
            // 
            // ItemTHMenu
            // 
            this.ItemTHMenu.Name = "ItemTHMenu";
            this.ItemTHMenu.Text = "Выстроить горизонтально";
            this.ItemTHMenu.Click += new System.EventHandler(this.ItemTHMenu_Click);
            // 
            // ItemTVMenu
            // 
            this.ItemTVMenu.Name = "ItemTVMenu";
            this.ItemTVMenu.Text = "Выстроить вертикально";
            this.ItemTVMenu.Click += new System.EventHandler(this.ItemTVMenu_Click);
            // 
            // ItemCascadeMenu
            // 
            this.ItemCascadeMenu.Name = "ItemCascadeMenu";
            this.ItemCascadeMenu.Text = "Выстроить каскадом";
            this.ItemCascadeMenu.Click += new System.EventHandler(this.ItemCascadeMenu_Click);
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VisualThemeMenu
            // 
            this.VisualThemeMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.AquaMenu,
            this.BreezeMenu,
            this.DesertMenu,
            this.Office2010Menu,
            this.Windows7Menu});
            this.VisualThemeMenu.Name = "VisualThemeMenu";
            this.VisualThemeMenu.Text = "Тема";
            // 
            // AquaMenu
            // 
            this.AquaMenu.Name = "AquaMenu";
            this.AquaMenu.Text = "Аква";
            this.AquaMenu.Click += new System.EventHandler(this.radMenuItem10_Click);
            // 
            // BreezeMenu
            // 
            this.BreezeMenu.Name = "BreezeMenu";
            this.BreezeMenu.Text = "Бриз";
            this.BreezeMenu.Click += new System.EventHandler(this.radMenuItem11_Click);
            // 
            // DesertMenu
            // 
            this.DesertMenu.Name = "DesertMenu";
            this.DesertMenu.Text = "Пустыня";
            this.DesertMenu.Click += new System.EventHandler(this.radMenuItem12_Click);
            // 
            // Office2010Menu
            // 
            this.Office2010Menu.AccessibleDescription = "radMenuItem13";
            this.Office2010Menu.AccessibleName = "radMenuItem13";
            this.Office2010Menu.Name = "Office2010Menu";
            this.Office2010Menu.Text = "Офис 2010 Black";
            this.Office2010Menu.Click += new System.EventHandler(this.radMenuItem13_Click);
            // 
            // Windows7Menu
            // 
            this.Windows7Menu.AccessibleDescription = "radMenuItem14";
            this.Windows7Menu.AccessibleName = "radMenuItem14";
            this.Windows7Menu.Name = "Windows7Menu";
            this.Windows7Menu.Text = "Windows 7";
            this.Windows7Menu.Click += new System.EventHandler(this.radMenuItem14_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MainMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.FileMenu,
            this.SiteMenu,
            this.SpravMenu,
            this.TepmlatesWorkMenu,
            this.LoadMenu,
            this.ExportMenu,
            this.WindowMenu,
            this.VisualThemeMenu});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            // 
            // 
            // 
            this.MainMenu.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 24);
            this.MainMenu.Size = new System.Drawing.Size(912, 25);
            this.MainMenu.TabIndex = 3;
            this.MainMenu.Text = "radMenu1";
            this.MainMenu.ThemeName = "Aqua";
            // 
            // BrowseDefaultTitleMenu
            // 
            this.BrowseDefaultTitleMenu.Name = "BrowseDefaultTitleMenu";
            this.BrowseDefaultTitleMenu.Text = "Иерархия тайтлов";
            this.BrowseDefaultTitleMenu.Click += new System.EventHandler(this.BrowseDefaultTitleMenu_Click);
            // 
            // MainFrm
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(912, 867);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.sbStatus);
            this.IsMdiContainer = true;
            this.Name = "MainFrm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Система управления контентом сайта";
            this.ThemeName = "Aqua";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.fmMain_Closing);
            this.Load += new System.EventHandler(this.MainFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		
		#endregion

		protected override void Dispose(bool disposing)
		{
			if (Program.mutexStock != null)
				Program.mutexStock.Close();
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

	    private Telerik.WinControls.UI.RadMenuItem FileMenu;
		private Telerik.WinControls.UI.RadMenuItem SiteMenu;
		private Telerik.WinControls.UI.RadMenuItem SpravMenu;
		private Telerik.WinControls.UI.RadMenuItem TepmlatesWorkMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadMenu;
		private Telerik.WinControls.UI.RadMenuItem ExportMenu;
		private Telerik.WinControls.UI.RadMenuItem WindowMenu;
		private Telerik.WinControls.UI.RadMenuItem VisualThemeMenu;
		private Telerik.WinControls.UI.RadMenuItem AquaMenu;
		private Telerik.WinControls.UI.RadMenuItem BreezeMenu;
		private Telerik.WinControls.UI.RadMenuItem DesertMenu;
		private Telerik.WinControls.UI.RadMenuItem Office2010Menu;
		private Telerik.WinControls.UI.RadMenuItem Windows7Menu;
		private Telerik.WinControls.UI.RadMenuItem CatalogTreeMenu;
		private Telerik.WinControls.UI.RadMenuItem ObjectTreeMenu;
		private Telerik.WinControls.UI.RadMenuItem LinkMasterMenu;
		private Telerik.WinControls.Themes.AquaTheme aquaTheme1;
		private Telerik.WinControls.Themes.BreezeTheme breezeTheme1;
		private Telerik.WinControls.Themes.DesertTheme desertTheme1;
		private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme1;
		private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
		private Telerik.WinControls.UI.RadMenuItem LinkReviewMenu;
		private Telerik.WinControls.UI.RadMenuItem CommodityGroupMenu;
		private Telerik.WinControls.UI.RadMenuItem LoginMenu;
		private Telerik.WinControls.UI.RadMenuItem LogoutMenu;
		private Telerik.WinControls.UI.RadMenuSeparatorItem MenuSep2Rad;
		private Telerik.WinControls.UI.RadMenuItem CloseMenu;
		private Telerik.WinControls.UI.RadMenuItem OrderMenu;
		private Telerik.WinControls.UI.RadMenuItem BinaryDataMenu;
		private Telerik.WinControls.UI.RadMenuItem ThemeMenu;
		private Telerik.WinControls.UI.RadMenuItem CommentMenu;
		private Telerik.WinControls.UI.RadMenuItem CompanyMenu;
		private Telerik.WinControls.UI.RadMenuItem MasterCatalogMenu;
		private Telerik.WinControls.UI.RadMenuItem DisableExpressDeliveryMenu;
		private Telerik.WinControls.UI.RadMenuItem PeopleMenu;
		private Telerik.WinControls.UI.RadMenuItem PromoMenu;
		private Telerik.WinControls.UI.RadMenuItem ArticleMenu;
		private Telerik.WinControls.UI.RadMenuItem GoodsMenu;
		private Telerik.WinControls.UI.RadMenuItem TreeMenu;
		private Telerik.WinControls.UI.RadMenuItem AddPriceMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadParamsMenu;
		private Telerik.WinControls.UI.RadMenuItem ModifyTemplateMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadKeywordsMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadProductTypeMenu;
		private Telerik.WinControls.UI.RadMenuItem ProcessTochkaMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadImagesMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadMoscowMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadSpbMenu;
		private Telerik.WinControls.UI.RadMenuItem ExportOrderMenu;
		private Telerik.WinControls.UI.RadMenuItem ItemTHMenu;
		private Telerik.WinControls.UI.RadMenuItem ItemTVMenu;
		private Telerik.WinControls.UI.RadMenuItem ItemCascadeMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadSitePricesMoscowMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadBidsMoscowMenu;
		private Telerik.WinControls.UI.RadMenuItem ExportOrdersFromSVMoscowMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadDescriptionMoscowMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadBidsSpbMenu;
		private Telerik.WinControls.UI.RadMenuItem ExportOrdersFromSVSpbMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadDescriptionSpbMenu;
		private Telerik.WinControls.UI.RadMenuItem LoadAccessoriesMenu;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
		private Telerik.WinControls.UI.RadMenuItem LoadAnalogsMenu;
		private Telerik.WinControls.UI.RadMenuItem ActionMenu;
		private Telerik.WinControls.UI.RadMenuItem BannerRootMenu;
		private Telerik.WinControls.UI.RadMenuItem AdvertCompanyMenu;
		private Telerik.WinControls.UI.RadMenuItem AdvertItemMenu;
		private Telerik.WinControls.UI.RadMenuItem AdvertImageMenu;
		private Telerik.WinControls.UI.RadMenuItem ReportAdvertShowMenu;
		private Telerik.WinControls.UI.RadMenuItem AdvertPlaceMenu;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
		private Telerik.WinControls.UI.RadMenuItem ListJobLogMenu;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
		private Telerik.WinControls.UI.RadMenuItem WholesaleOrderMenu;
		private Telerik.WinControls.UI.RadMenuItem WebConfigMenu;
		private Telerik.WinControls.UI.RadMenuItem UploadGoods4YandexMenu;
		private Telerik.WinControls.UI.RadMenuItem GoodsItemMenu;
        private Telerik.WinControls.UI.RadMenuItem LoadInStockMenu;
        private Telerik.WinControls.UI.RadMenuItem LandingPageMenu;
        private Telerik.WinControls.UI.RadMenu MainMenu;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem YandexReportMenu;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem DefaultTypeMenu;
        private Telerik.WinControls.UI.RadMenuItem BrowseDefaultTitleMenu;
    }
}
