using ELBClient.Forms.Guides;

namespace ELBClient.Classes
{
	/// <summary>
	/// Summary description for FormSelection.
	/// </summary>
	public class FormSelection
	{
		public static EditForm GetEditForm(string className) {
			switch (className)
			{
				case "CBinaryData":
					return new EditBinaryData();
				case "CTree":
					return new EditTree();
				case "CAction":
					return new EditAction();
				case "CArticle":
					return new EditArticle();
				case "CCompany":
					return new EditContraAgents();
                case "CLandingPage":
                    return new EditLandingPage();
                case "CPage":
                    return new EditPage();
                case "CPromo":
					return new EditPromo();
				case "CTheme":
					return new EditTheme();
				case "CMasterCatalog":
					return new EditMasterCatalog();
				case "CGoods":
					return new EditGoods();
				case "COrder":
					return new EditOrder();
				case "CComment":
					return new EditComment();
				case "CDisableExpressDelivery":
					return new EditDisableExpressDelivery();
                case "CCommodityGroup":
                    return new EditCommodityGroup();
                case "CDefaultTitle":
                    return new EditDefaultTitle();

                case "CAdvertItem":
					return new Forms.Banners.EditAdvertItem();
				case "CAdvertCompany":
					return new Forms.Banners.EditAdvertCompany();
				case "CAdvertPlace":
					return new Forms.Banners.EditAdvertPlace();
				default: 
					return null;
					//throw new Exception("FormSelection: Invalid class name");
			}
		}

		public static EditForm GetEditBannersForm(string className, bool showBannerClass = false)
		{
			switch (className)
			{
				case "CBinaryData":
					return new Forms.Banners.EditBinaryDataBanner();
				case "CAdvertItem":
					return new Forms.Banners.EditAdvertItem();
				case "CAdvertCompany":
					return new Forms.Banners.EditAdvertCompany();
				default:
					return null;
				//throw new Exception("FormSelection: Invalid class name");
			}
		}
	}
}
