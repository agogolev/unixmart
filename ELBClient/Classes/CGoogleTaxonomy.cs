//------------------------------------------------------------------------------
// <auto-generated>
//     ���� ��� ������ ����������.
//     ����������� ������:2.0.50727.5477
//
//     ��������� � ���� ����� ����� �������� � ������������ ������ � ����� �������� � ������
//     ��������� ��������� ����.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ELBClient.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using MetaData;
	
	
	public class CGoogleTaxonomy : CObject
	{
		
		private string _name;
		
		private bool _name_isModified;
		
		public CGoogleTaxonomy()
		{
			_name = string.Empty;
			_name_isModified = false;
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("DA9ADBC6-16D9-46D0-A315-F45BD8A3E555");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("DA9ADBC6-16D9-46D0-A315-F45BD8A3E555");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CGoogleTaxonomy";
			}
		}
		
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				if ((_name != value))
				{
					_name = value;
					_name_isModified = true;
				}
			}
		}
		
		public override bool IsModified
		{
			get
			{
				if (_name_isModified)
				{
					return true;
				}
				return base.IsModified;
			}
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("name") != null) 
						&& (root.SelectSingleNode("name").InnerText != null)))
			{
				_name = root.SelectSingleNode("name").InnerText;
				_name_isModified = false;
			}
		}
		
		protected override void LoadMultiProp()
		{
			base.LoadMultiProp();
		}
		
		protected override void SaveSingleProp()
		{
			base.SaveSingleProp();
			if ((_name_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("name"));
				prop.InnerText = _name;
			}
		}
		
		protected override void SaveMultiProp()
		{
			base.SaveMultiProp();
		}
		
		public override void Refresh()
		{
			_name_isModified = false;
			base.Refresh();
		}
	}
}
