//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.2470
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace ELBClient.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using MetaData;
	
	
	public class CSite : CObject
	{
		
		private string _name;
		
		private bool _name_isModified;
		
		private MultiContainer _places;
		
		public CSite()
		{
			_name = string.Empty;
			_name_isModified = false;
			System.Data.DataColumn[] keys;
			System.Data.DataColumn dt;
			_places = new MultiContainer("places");
			_places.TableName = "table";
			keys = new System.Data.DataColumn[1];
			dt = _places.Columns.Add("propValue", typeof(System.Guid));
			keys[0] = dt;
			_places.Columns.Add("propValue_NK", typeof(string));
			_places.Columns.Add("propValue_ClassName", typeof(string));
			_places.PrimaryKey = keys;
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("CCA76253-5393-410D-A96B-5A11B0CB2D45");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("CCA76253-5393-410D-A96B-5A11B0CB2D45");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CSite";
			}
		}
		
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				if ((_name != value))
				{
					_name = value;
					_name_isModified = true;
				}
			}
		}
		
		public MultiContainer Places
		{
			get
			{
				return _places;
			}
		}
		
		public override bool IsModified
		{
			get
			{
				if (_name_isModified)
				{
					return true;
				}
				if (_places.IsModified)
				{
					return true;
				}
				return base.IsModified;
			}
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("name") != null) 
						&& (root.SelectSingleNode("name").InnerText != null)))
			{
				_name = root.SelectSingleNode("name").InnerText;
				_name_isModified = false;
			}
		}
		
		protected override void LoadMultiProp()
		{
			XmlNodeList nl = null;
			nl = root.SelectNodes("places");
			if ((nl.Count != 0))
			{
				_places.Clear();
				for (int i = 0; (i < nl.Count); i = (i + 1))
				{
					XmlNode n = nl[i];
					object propValue = new Guid(n.SelectSingleNode("propValue").InnerText);
					_places.Rows.Add(new object[] {
								propValue,
								n.SelectSingleNode("propValue_NK").InnerText,
								n.SelectSingleNode("propValue_className").InnerText});
				}
				_places.AcceptChanges();
			}
			base.LoadMultiProp();
		}
		
		protected override void SaveSingleProp()
		{
			base.SaveSingleProp();
			if ((_name_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("name"));
				prop.InnerText = _name;
			}
		}
		
		protected override void SaveMultiProp()
		{
			base.SaveMultiProp();
			_places.DiffGram(root);
		}
		
		public override void Refresh()
		{
			_name_isModified = false;
			_places.AcceptChanges();
			base.Refresh();
		}
	}
}
