using System;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;
using ColumnMenuExtender.Forms;
using ELBClient.AuthorizeProvider;

namespace ELBClient.Classes
{
    /// <summary>
    ///     Summary description for ServiceUtility.
    /// </summary>
    public class ServiceUtility
    {
        public static AuthorizeProvider.AuthorizeProvider AuthorizeProvider
        {
            get
            {
                var provider = new AuthorizeProvider.AuthorizeProvider();
                provider.Url = GetConnection(provider.Url);
                provider.SecurityHeaderValue = new SecurityHeader();
                provider.Proxy = ConfigProxy();
                return provider;
            }
        }

        /// <summary>
        ///     SecurityHeader ��������� ������
        /// </summary>
        public static ListProvider.ListProvider ListProvider
        {
            get
            {
                var provider = new ListProvider.ListProvider();
                provider.Url = GetConnection(provider.Url);
                provider.Proxy = ConfigProxy();
                provider.Timeout = GetTimeOut();
                var header = new ListProvider.SecurityHeader();
                header.Ticket = BaseGenericForm.MainForm.CurrentTicket;
                provider.SecurityHeaderValue = header;
                return provider;
            }
        }

        /// <summary>
        ///     SecurityHeader ��������� ������
        /// </summary>
        public static ObjectProvider.ObjectProvider ObjectProvider
        {
            get
            {
                var provider = new ObjectProvider.ObjectProvider();
                provider.Url = GetConnection(provider.Url);
                provider.Proxy = ConfigProxy();
                provider.Timeout = GetTimeOut();
                var header = new ObjectProvider.SecurityHeader();
                header.Ticket = BaseGenericForm.MainForm.CurrentTicket;
                provider.SecurityHeaderValue = header;
                return provider;
            }
        }

        public static TreeProvider.TreeProvider TreeProvider
        {
            get
            {
                var provider = new TreeProvider.TreeProvider();
                provider.Url = GetConnection(provider.Url);
                provider.Proxy = ConfigProxy();
                provider.Timeout = GetTimeOut();
                return provider;
            }
        }

        public static FileProvider.FileProvider FileProvider
        {
            get
            {
                var provider = new FileProvider.FileProvider();
                provider.Url = GetConnection(provider.Url);
                return provider;
            }
        }

        public static BusinessProvider.BusinessProvider BusinessProvider
        {
            get
            {
                var provider = new BusinessProvider.BusinessProvider();
                provider.Url = GetConnection(provider.Url);
                provider.Proxy = ConfigProxy();
                provider.Timeout = GetTimeOut();
                var header = new BusinessProvider.SecurityHeader();
                header.Ticket = BaseGenericForm.MainForm.CurrentTicket;
                provider.SecurityHeaderValue = header;
                return provider;
            }
        }

        private static string GetConnection(string url)
        {
            var serviceLocation = ConfigurationManager.AppSettings["connection"];
            var re = new Regex("https?://([^/]*)/", RegexOptions.IgnoreCase);
            return re.Replace(url, serviceLocation + "/");
        }

        public static string GetBrowseDir()
        {
            var serviceLocation = ConfigurationManager.AppSettings["connection"];
            var browseDir = ConfigurationManager.AppSettings["browseDir"];
            return serviceLocation + "/" + browseDir + "/";
        }

        public static string GetBannersBrowseDir()
        {
            //string serviceLocation = ConfigurationManager.AppSettings["connection"];
            //string browseDir = ConfigurationManager.AppSettings["browseDir"];
            return "http://banners.gogolink.ru/BackendService/Browse/";
        }

        private static int GetTimeOut()
        {
            var s = ConfigurationManager.AppSettings["timeout"];
            var res = 100*1000; //�� ��������� 100 ������

            if (s != null && s != "")
            {
                try
                {
                    res = Convert.ToInt32(s);
                    res *= 1000; //� ����� �������� �������� � ��������
                }
                catch
                {
                }
            }

            return res;
        }

        private static IWebProxy ConfigProxy()
        {
            if (UseProxy())
            {
                var proxyAddr = ConfigurationManager.AppSettings["proxyAddr"];
                IWebProxy wProxy;
                if (proxyAddr == "default") wProxy = WebRequest.DefaultWebProxy;
                else wProxy = new WebProxy(proxyAddr, true);
                var proxyAuth = ConfigurationManager.AppSettings["proxyAuth"];
                if (proxyAuth == "integrated") wProxy.Credentials = CredentialCache.DefaultCredentials;
                else
                {
                    if (ConfigurationManager.AppSettings["domain"] != null)
                    {
                        var login = ConfigurationManager.AppSettings["login"];
                        var domain = ConfigurationManager.AppSettings["domain"];
                        var password = ConfigurationManager.AppSettings["password"];
                        wProxy.Credentials = new NetworkCredential(login, password, domain);
                    }
                    else
                    {
                        var login = ConfigurationManager.AppSettings["login"];
                        var password = ConfigurationManager.AppSettings["password"];
                        wProxy.Credentials = new NetworkCredential(login, password);
                    }
                }
                return wProxy;
            }
            return null;
        }

        public static bool UseProxy()
        {
            foreach (string key in ConfigurationManager.AppSettings.Keys)
            {
                if (key == "proxyAddr") return true;
            }
            return false;
        }
    }
}