using System;
using System.Collections;
using System.Data;

namespace ELBClient.Classes
{
	/// <summary>
	/// Summary description for NodeContainer.
	/// </summary>
	public class NodeContainer {
		public int Left, Right;
		public ArrayList Nodes = new ArrayList();
		public NodeContainer(int left, int right) {
			Left = left;
			Right = right;
		}
	}
	public class Node : NodeContainer {
		public int NodeId;
		public string Name;
		public string dateBegin;
		public string dateEnd;
		public Guid objectOID;
		public string objectNK;
		public string className;
		public string objectNKRus;
		private DataTable addString;
		public DataTable AddString {
			get { 
				return addString; 
			}
		}

		public int Count;
		public Node(int left, int right) : base(left, right){
			init();
		}
		public Node() : base(0, 0){
			init();
		}
		private void init() {
			addString = new DataTable();
			DataColumn key = addString.Columns.Add("ordValue", typeof(string));
			addString.PrimaryKey = new DataColumn[1]{key};
			addString.Columns.Add("value", typeof(string));
			addString.Columns.Add("url", typeof(string));
		}
	}
	public class Tree : NodeContainer {
		public String name;
		public Tree(int left, int right) : base(left, right){}
	}
}
