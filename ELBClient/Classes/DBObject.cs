using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Data;
using System.Text;
using System.Globalization;
using MetaData;

namespace ELBClient.Classes
{
	public abstract class DBObject 
	{
		public List<ProcedureInfo> BeforeSaveProcs { get; set; }
		public List<ProcedureInfo> AfterSaveProcs { get; set; }
		public NumberFormatInfo NumberFormat 
		{
			get 
			{
				NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
				nfi.NumberDecimalSeparator = ".";
				return nfi;
			}
		}
		public DateTimeFormatInfo DateTimeFormat 
		{
			get 
			{
				DateTimeFormatInfo dtfi = new CultureInfo("ru-RU").DateTimeFormat;
				dtfi.FullDateTimePattern += ".fff";
				dtfi.ShortDatePattern = "dd/MM/yyyy";
				return dtfi;
			}
		}

		/// <summary>
		/// ���� ���� ��������� ��� Single ���� ���������� �� �������� isModified, �� ���� ��������� � True
		/// </summary>
		public bool SaveAllFlag = false;
		/// <summary>
		/// ���� ���������� �� ��������� ������ ���� (��������, � XML ���������� �� ������ ���������� ������
		/// ��� ������� LoadMultiProp � ����������� ������������ DataSet), �� ���� ��������� � False;
		/// </summary>
		public bool LoadMultiPropFlag = true;

		public abstract Guid CID 
		{
			get;
		}

		public static Guid ClassCID {
			get {
				return Guid.Empty;
			}
		}

		public abstract string ClassName
		{
			get;
		}

		private Guid _OID = Guid.Empty;

		public Guid OID 
		{
			get
			{
				return _OID;
			}
			set
			{
				_OID = value;
			}
		}
		protected XmlElement root;
		protected XmlDocument doc;

		public XmlDocument MainXmlDoc
		{
			get {return doc;}
		}

		public void LoadXml(string objectXml) 
		{
			if (objectXml == null) return;
			objectXml = objectXml.RestoreCr();
			doc = new XmlDocument();
			doc.LoadXml(objectXml);
			root = doc.DocumentElement;
			Refresh();
			LoadSingleProp();
			if (LoadMultiPropFlag)
				LoadMultiProp();
		}		
		public string SaveXml() 
		{
			doc = new XmlDocument();
			doc.LoadXml("<?xml version=\"1.0\" encoding=\"Windows-1251\" ?><object><singleProp></singleProp><multiProp></multiProp><extMultiProp></extMultiProp><procs></procs></object>");
			root = doc.DocumentElement;
			SaveSingleProp();
			SaveMultiProp();
			SaveExtMultiProp();
			//SaveProcs();
			return doc.OuterXml;
		}

		
		
		private string extMultiProp = "";
		public string ExtMultiProp 
		{
			get
			{
				return extMultiProp;
			}
			set
			{			
				extMultiProp = value;
			}
		}

		private void SaveExtMultiProp()
		{
			XmlElement extMultiPropElement = (XmlElement)doc.DocumentElement.SelectSingleNode("extMultiProp");
			extMultiPropElement.InnerXml = extMultiProp;			
		}
		//private void SaveProcs()
		//{
		//  if (BeforeSaveProcs != null) {
		//    XmlNode n = doc.DocumentElement.SelectSingleNode("procs");
		//    BeforeSaveProcs.ForEach(proc => { 
		//      XmlNode procNode =  n.AppendChild(new XmlElement(""))
		//    });

		//  }
		//}	
		public void RefreshOID(string resultXml) 
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(resultXml);
			_OID = new Guid(doc.DocumentElement.Attributes["OID"].Value);
		}
		public virtual bool IsModified 
		{
			get 
			{
				return false;
			}
			set {}
		}
		protected virtual void LoadSingleProp() 
		{
			Guid cid = new Guid(root.SelectSingleNode("CID").InnerText);
			
			if(cid != CID) throw new Exception("Invalid object class");
			_OID = new Guid(root.Attributes["OID"].Value);
		}
		protected virtual void SaveSingleProp() 
		{
			XmlAttribute attr = root.Attributes.Append(doc.CreateAttribute("OID"));
			attr.Value = _OID.ToString().ToUpper();
			root = (XmlElement)doc.DocumentElement.SelectSingleNode("singleProp");
		}
		protected virtual void SaveMultiProp() 
		{
			root = (XmlElement)doc.DocumentElement.SelectSingleNode("multiProp");
		}
		protected abstract void LoadMultiProp() ;
		public abstract void Refresh();
	}
}