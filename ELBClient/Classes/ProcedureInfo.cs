﻿using System.Collections.Generic;

namespace ELBClient.Classes
{
	public class ProcedureInfo
	{
		public string ProcedureName { get; set; }
		public IDictionary<string, string> Params { get; set; }
	}
}
