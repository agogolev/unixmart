﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ecommerce.Extensions;

namespace ELBClient.Classes
{
	public static class NodeExtender
	{
		public static string FullName(this TreeNode item)
		{
			var items = new List<Node>();
			do
			{
				items.Insert(0, (Node)item.Tag);
				item = item.Parent;
			}
			while (item != null);
			return items.Select(node => node.Name.Replace("\n", " ")).Intersperse(" / ").Aggregate(new StringBuilder(),
				(b, s) => { b.Append(s); return b; },
				b => b.ToString());
		}

        public static IEnumerable<TreeNode> AllNodes(this TreeView item)
        {
            var items = new List<TreeNode>();
            item.Nodes.Cast<TreeNode>().ToList().ForEach(n =>
            {
                items.Add(n);
                AddChild(items, n);
            });
            return items;
        }

        public static IEnumerable<TreeNode> AllNodes(this TreeNode item)
		{
			var items = new List<TreeNode> { item };
			AddChild(items, item);
			return items;
		}

		private static void AddChild(List<TreeNode> items, TreeNode item)
		{
			item.Nodes.Cast<TreeNode>().ToList().ForEach(n =>
			{
				items.Add(n);
				AddChild(items, n);
			});
		}
	}
} 
