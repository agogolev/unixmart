using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Xsl;
using MetaData;
using System.Data;

namespace ELBClient.Classes
{
	/// <summary>
	/// Summary description for MailConstructor.
	/// </summary>
	public class MailConstructor
	{
		public static string BuildMailTemplate(Guid OID, int shopType) {
			ListProvider.ListProvider lp = Classes.ServiceUtility.ListProvider;
			string sql = @"
SELECT
	Tag = 1,
	parent = null,
	[body!1!siteUrl] = ts.address,
	[promo!2!ordValue!element] = null,
	[promo!2!name!element] = null,
	[promo!2!url] = null,
	[promo!2!description!element] = null,
	[promo!2!price] = null,
	[promo!2!image] = null,
	[promo!2!imageOnly] = null,
	[goods!3!OID] =	null,							
	[goods!3!ordValue] = null,					
	[goods!3!shortName!element] = null,
	[goods!3!model!element] =	null,		
	[goods!3!price] =	null,						
	[goods!3!smallImage] = null,
	[review!4!OID] = null,
	[review!4!ordValue] = null,
	[review!4!header!element] = null,
	[review!4!body!element] = null,	
	[review!4!pubDate] = null			
FROM
	t_TypeShop ts
WHERE
	ts.shopType = "+GenericForm.ToSqlString(shopType)+@"
UNION ALL
SELECT
	2,
	1,
	null,
	mp.ordValue,
	p.name,
	p.url,
	p.description,
	p.price,
	p.image,
	P.imageOnly,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null
FROM
	t_MailConstructor m
	inner join t_MailPromos mp on m.OID = mp.OID And m.OID = "+GenericForm.ToSqlString(OID)+@"
	inner join t_Promo p on mp.promoOID = p.OID	
UNION ALL
SELECT
	3,
	1,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	g.OID,
	mg.ordValue,
	g.shortName,
	dbo.fID(g.OID, "+GenericForm.ToSqlString(shopType)+@"),
	dbo.fPrice(g.OID, "+GenericForm.ToSqlString(shopType)+@"),
	dbo.fListImage(g.OID),
	null,
	null,
	null,
	null,
	null
FROM
	t_MailConstructor m
	inner join t_MailGoods mg on m.OID = mg.OID And m.OID = "+GenericForm.ToSqlString(OID)+@"
	inner join t_Goods g on mg.goodsOID = g.OID	
UNION ALL
SELECT
	4,
	1,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	a.OID,
	mr.ordValue,
	a.header,	
	a.body,
	dbo.fGetDatePart(a.pubDate, 0, 0)	
FROM
	t_MailConstructor m
	inner join t_MailReviews mr on m.OID = mr.OID And m.OID = "+GenericForm.ToSqlString(OID)+@"
	inner join t_Article a on mr.articleOID = a.OID	
ORDER BY
	Tag, [promo!2!ordValue!element], [goods!3!ordValue], [review!4!ordValue]
FOR XML EXPLICIT  
	";
			DataSetISM ds = new DataSetISM(lp.GetDataSetSql(sql));
			StringBuilder sb = new StringBuilder();   
			//sb.Append("<?xml version=\"1.0\" encoding=\"windows-1251\" ?>");
			foreach(DataRow dr in ds.Table.Rows) {       
				sb.Append((string)dr[0]);
			}
			XmlDocument doc = new XmlDocument();
			string result = sb.ToString();
			Regex re = new Regex("&lt;img(((?!/&gt;).)*)/&gt;");
			result = re.Replace(result, new MatchEvaluator(replacer));
			doc.LoadXml(result);       
			sql = @"
SELECT
	address
FROM
	t_TypeShop
WHERE
	shopType = "+GenericForm.ToSqlString(shopType);
			DataSetISM dsShop = new DataSetISM(lp.GetDataSetSql(sql));
			string address = (string)dsShop.Table.Rows[0][0];
			string must = "/must_1";
			XslCompiledTransform tr = new XslCompiledTransform();
			string xslt = GenericForm.GetResourceString("mailTemplate.xslt");   
			StringReader sr = new StringReader(xslt);

			XmlTextReader read = new XmlTextReader(sr);
			tr.Load(read, null, null);  
			StringWriter sw = new StringWriter();
			XmlTextWriter writer = new XmlTextWriter(sw);
			XsltArgumentList arr = new XsltArgumentList();
			arr.AddParam("address", "", address);
			arr.AddParam("must", "", must);
			tr.Transform(doc, arr, writer, null); 
			string body = sw.ToString();
			re = new Regex("src=\"(?!http://)", RegexOptions.IgnoreCase);
			body = re.Replace(body, "src=\""+address);
			re = new Regex("href=\"(?!http://)([^#][^\"]*)", RegexOptions.IgnoreCase);
			body = re.Replace(body, "href=\""+address+"$1"+must);
			return body;
		}
		private static string replacer(Match m) {
			string inside = m.Groups[1].Value.ToLower();
			if(inside.IndexOf("float") == -1) {
				return "&lt;img"+m.Groups[1].Value + " style=&quot;float:left;&quot; /&gt;";
			}
			else return m.Value;

		}
	}
}
