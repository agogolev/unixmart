//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.573
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace ELBClient.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using MetaData;
	
	
	public class CAddress : CObject
	{
		
		private string _country;
		
		private bool _country_isModified;
		
		private string _zipCode;
		
		private bool _zipCode_isModified;
		
		private int _regionID;
		
		private bool _regionID_isModified;
		
		private string _city;
		
		private bool _city_isModified;
		
		private string _address;
		
		private bool _address_isModified;
		
		private string _house;
		
		private bool _house_isModified;
		
		private string _build;
		
		private bool _build_isModified;
		
		private string _flat;
		
		private bool _flat_isModified;
		
		public CAddress()
		{
			_country = string.Empty;
			_country_isModified = false;
			_zipCode = string.Empty;
			_zipCode_isModified = false;
			_regionID = int.MinValue;
			_regionID_isModified = false;
			_city = string.Empty;
			_city_isModified = false;
			_address = string.Empty;
			_address_isModified = false;
			_house = string.Empty;
			_house_isModified = false;
			_build = string.Empty;
			_build_isModified = false;
			_flat = string.Empty;
			_flat_isModified = false;
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("88D95671-B992-4E9B-A57A-D797A9FD5B97");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("88D95671-B992-4E9B-A57A-D797A9FD5B97");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CAddress";
			}
		}
		
		public string Country
		{
			get
			{
				return _country;
			}
			set
			{
				if ((_country != value))
				{
					_country = value;
					_country_isModified = true;
				}
			}
		}
		
		public string ZipCode
		{
			get
			{
				return _zipCode;
			}
			set
			{
				if ((_zipCode != value))
				{
					_zipCode = value;
					_zipCode_isModified = true;
				}
			}
		}
		
		public int RegionID
		{
			get
			{
				return _regionID;
			}
			set
			{
				if ((_regionID != value))
				{
					_regionID = value;
					_regionID_isModified = true;
				}
			}
		}
		
		public string City
		{
			get
			{
				return _city;
			}
			set
			{
				if ((_city != value))
				{
					_city = value;
					_city_isModified = true;
				}
			}
		}
		
		public string Address
		{
			get
			{
				return _address;
			}
			set
			{
				if ((_address != value))
				{
					_address = value;
					_address_isModified = true;
				}
			}
		}
		
		public string House
		{
			get
			{
				return _house;
			}
			set
			{
				if ((_house != value))
				{
					_house = value;
					_house_isModified = true;
				}
			}
		}
		
		public string Build
		{
			get
			{
				return _build;
			}
			set
			{
				if ((_build != value))
				{
					_build = value;
					_build_isModified = true;
				}
			}
		}
		
		public string Flat
		{
			get
			{
				return _flat;
			}
			set
			{
				if ((_flat != value))
				{
					_flat = value;
					_flat_isModified = true;
				}
			}
		}
		
		public override bool IsModified
		{
			get
			{
				if (_country_isModified)
				{
					return true;
				}
				if (_zipCode_isModified)
				{
					return true;
				}
				if (_regionID_isModified)
				{
					return true;
				}
				if (_city_isModified)
				{
					return true;
				}
				if (_address_isModified)
				{
					return true;
				}
				if (_house_isModified)
				{
					return true;
				}
				if (_build_isModified)
				{
					return true;
				}
				if (_flat_isModified)
				{
					return true;
				}
				return base.IsModified;
			}
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("country") != null) 
						&& (root.SelectSingleNode("country").InnerText != null)))
			{
				_country = root.SelectSingleNode("country").InnerText;
				_country_isModified = false;
			}
			if (((root.SelectSingleNode("zipCode") != null) 
						&& (root.SelectSingleNode("zipCode").InnerText != null)))
			{
				_zipCode = root.SelectSingleNode("zipCode").InnerText;
				_zipCode_isModified = false;
			}
			if (((root.SelectSingleNode("regionID") != null) 
						&& (root.SelectSingleNode("regionID").InnerText != null)))
			{
				_regionID = Convert.ToInt32(root.SelectSingleNode("regionID").InnerText);
				_regionID_isModified = false;
			}
			if (((root.SelectSingleNode("city") != null) 
						&& (root.SelectSingleNode("city").InnerText != null)))
			{
				_city = root.SelectSingleNode("city").InnerText;
				_city_isModified = false;
			}
			if (((root.SelectSingleNode("address") != null) 
						&& (root.SelectSingleNode("address").InnerText != null)))
			{
				_address = root.SelectSingleNode("address").InnerText;
				_address_isModified = false;
			}
			if (((root.SelectSingleNode("house") != null) 
						&& (root.SelectSingleNode("house").InnerText != null)))
			{
				_house = root.SelectSingleNode("house").InnerText;
				_house_isModified = false;
			}
			if (((root.SelectSingleNode("build") != null) 
						&& (root.SelectSingleNode("build").InnerText != null)))
			{
				_build = root.SelectSingleNode("build").InnerText;
				_build_isModified = false;
			}
			if (((root.SelectSingleNode("flat") != null) 
						&& (root.SelectSingleNode("flat").InnerText != null)))
			{
				_flat = root.SelectSingleNode("flat").InnerText;
				_flat_isModified = false;
			}
		}
		
		protected override void LoadMultiProp()
		{
			base.LoadMultiProp();
		}
		
		protected override void SaveSingleProp()
		{
			base.SaveSingleProp();
			if (_country_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("country"));
				prop.InnerText = _country;
			}
			if (_zipCode_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("zipCode"));
				prop.InnerText = _zipCode;
			}
			if (_regionID_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("regionID"));
				prop.InnerText = _regionID.ToString();
			}
			if (_city_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("city"));
				prop.InnerText = _city;
			}
			if (_address_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("address"));
				prop.InnerText = _address;
			}
			if (_house_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("house"));
				prop.InnerText = _house;
			}
			if (_build_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("build"));
				prop.InnerText = _build;
			}
			if (_flat_isModified)
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("flat"));
				prop.InnerText = _flat;
			}
		}
		
		protected override void SaveMultiProp()
		{
			base.SaveMultiProp();
		}
		
		public override void Refresh()
		{
			_country_isModified = false;
			_zipCode_isModified = false;
			_regionID_isModified = false;
			_city_isModified = false;
			_address_isModified = false;
			_house_isModified = false;
			_build_isModified = false;
			_flat_isModified = false;
			base.Refresh();
		}
	}
}
