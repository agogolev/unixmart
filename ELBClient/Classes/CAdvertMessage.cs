//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.573
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace ELBClient.Classes
{
	using System;
	using System.Text;
	using System.Xml;
	using System.Data;
	using MetaData;
	
	
	public class CAdvertMessage : CObject
	{
		
		private int _campaignID;
		
		private bool _campaignID_isModified;
		
		private int _bannerID;
		
		private bool _bannerID_isModified;
		
		private int _shopType;
		
		private bool _shopType_isModified;
		
		private int _refererType;
		
		private bool _refererType_isModified;
		
		private string _header;
		
		private bool _header_isModified;
		
		private string _text;
		
		private bool _text_isModified;
		
		private string _href;
		
		private bool _href_isModified;
		
		private string _geo;
		
		private bool _geo_isModified;
		
		private string _geoIDs;
		
		private bool _geoIDs_isModified;
		
		private ObjectItem _object;
		
		private bool _object_isModified;
		
		private bool _isDirty;
		
		private bool _isDirty_isModified;
		
		private string _headerTemplate;
		
		private bool _headerTemplate_isModified;
		
		private string _bodyTemplate;
		
		private bool _bodyTemplate_isModified;
		
		private MultiContainer _errors;
		
		private MultiContainer _phrases;
		
		public CAdvertMessage()
		{
			_campaignID = int.MinValue;
			_campaignID_isModified = false;
			_bannerID = int.MinValue;
			_bannerID_isModified = false;
			_shopType = int.MinValue;
			_shopType_isModified = false;
			_refererType = int.MinValue;
			_refererType_isModified = false;
			_header = string.Empty;
			_header_isModified = false;
			_text = string.Empty;
			_text_isModified = false;
			_href = string.Empty;
			_href_isModified = false;
			_geo = string.Empty;
			_geo_isModified = false;
			_geoIDs = string.Empty;
			_geoIDs_isModified = false;
			_object = ObjectItem.Empty;
			_object_isModified = false;
			_isDirty = false;
			_isDirty_isModified = false;
			_headerTemplate = string.Empty;
			_headerTemplate_isModified = false;
			_bodyTemplate = string.Empty;
			_bodyTemplate_isModified = false;
			System.Data.DataColumn[] keys;
			System.Data.DataColumn dt;
			_errors = new MultiContainer("errors");
			_errors.TableName = "table";
			keys = new System.Data.DataColumn[1];
			_errors.Columns.Add("propValue", typeof(string));
			dt = _errors.Columns.Add("ordValue", typeof(System.DateTime));
			keys[0] = dt;
			_errors.PrimaryKey = keys;
			_phrases = new MultiContainer("phrases");
			_phrases.TableName = "table";
			keys = new System.Data.DataColumn[1];
			_phrases.Columns.Add("propValue", typeof(string));
			dt = _phrases.Columns.Add("ordValue", typeof(int));
			keys[0] = dt;
			_phrases.PrimaryKey = keys;
		}
		
		public override Guid CID
		{
			get
			{
				return new Guid("DB599149-2CF5-43A5-9354-AAD5BFD5BB4B");
			}
		}
		
		public new static Guid ClassCID
		{
			get
			{
				return new Guid("DB599149-2CF5-43A5-9354-AAD5BFD5BB4B");
			}
		}
		
		public override string ClassName
		{
			get
			{
				return "CAdvertMessage";
			}
		}
		
		public int CampaignID
		{
			get
			{
				return _campaignID;
			}
			set
			{
				if ((_campaignID != value))
				{
					_campaignID = value;
					_campaignID_isModified = true;
				}
			}
		}
		
		public int BannerID
		{
			get
			{
				return _bannerID;
			}
			set
			{
				if ((_bannerID != value))
				{
					_bannerID = value;
					_bannerID_isModified = true;
				}
			}
		}
		
		public int ShopType
		{
			get
			{
				return _shopType;
			}
			set
			{
				if ((_shopType != value))
				{
					_shopType = value;
					_shopType_isModified = true;
				}
			}
		}
		
		public int RefererType
		{
			get
			{
				return _refererType;
			}
			set
			{
				if ((_refererType != value))
				{
					_refererType = value;
					_refererType_isModified = true;
				}
			}
		}
		
		public string Header
		{
			get
			{
				return _header;
			}
			set
			{
				if ((_header != value))
				{
					_header = value;
					_header_isModified = true;
				}
			}
		}
		
		public string Text
		{
			get
			{
				return _text;
			}
			set
			{
				if ((_text != value))
				{
					_text = value;
					_text_isModified = true;
				}
			}
		}
		
		public string Href
		{
			get
			{
				return _href;
			}
			set
			{
				if ((_href != value))
				{
					_href = value;
					_href_isModified = true;
				}
			}
		}
		
		public string Geo
		{
			get
			{
				return _geo;
			}
			set
			{
				if ((_geo != value))
				{
					_geo = value;
					_geo_isModified = true;
				}
			}
		}
		
		public string GeoIDs
		{
			get
			{
				return _geoIDs;
			}
			set
			{
				if ((_geoIDs != value))
				{
					_geoIDs = value;
					_geoIDs_isModified = true;
				}
			}
		}
		
		public ObjectItem Object
		{
			get
			{
				return _object;
			}
			set
			{
				if ((_object != value))
				{
					_object = value;
					_object_isModified = true;
				}
			}
		}
		
		public bool IsDirty
		{
			get
			{
				return _isDirty;
			}
			set
			{
				if ((_isDirty != value))
				{
					_isDirty = value;
					_isDirty_isModified = true;
				}
			}
		}
		
		public string HeaderTemplate
		{
			get
			{
				return _headerTemplate;
			}
			set
			{
				if ((_headerTemplate != value))
				{
					_headerTemplate = value;
					_headerTemplate_isModified = true;
				}
			}
		}
		
		public string BodyTemplate
		{
			get
			{
				return _bodyTemplate;
			}
			set
			{
				if ((_bodyTemplate != value))
				{
					_bodyTemplate = value;
					_bodyTemplate_isModified = true;
				}
			}
		}
		
		public MultiContainer Errors
		{
			get
			{
				return _errors;
			}
		}
		
		public MultiContainer Phrases
		{
			get
			{
				return _phrases;
			}
		}
		
		public override bool IsModified
		{
			get
			{
				if (_campaignID_isModified)
				{
					return true;
				}
				if (_bannerID_isModified)
				{
					return true;
				}
				if (_shopType_isModified)
				{
					return true;
				}
				if (_refererType_isModified)
				{
					return true;
				}
				if (_header_isModified)
				{
					return true;
				}
				if (_text_isModified)
				{
					return true;
				}
				if (_href_isModified)
				{
					return true;
				}
				if (_geo_isModified)
				{
					return true;
				}
				if (_geoIDs_isModified)
				{
					return true;
				}
				if (_object_isModified)
				{
					return true;
				}
				if (_isDirty_isModified)
				{
					return true;
				}
				if (_headerTemplate_isModified)
				{
					return true;
				}
				if (_bodyTemplate_isModified)
				{
					return true;
				}
				if (_errors.IsModified)
				{
					return true;
				}
				if (_phrases.IsModified)
				{
					return true;
				}
				return base.IsModified;
			}
		}
		
		protected override void LoadSingleProp()
		{
			base.LoadSingleProp();
			if (((root.SelectSingleNode("campaignID") != null) 
						&& (root.SelectSingleNode("campaignID").InnerText != null)))
			{
				_campaignID = Convert.ToInt32(root.SelectSingleNode("campaignID").InnerText);
				_campaignID_isModified = false;
			}
			if (((root.SelectSingleNode("bannerID") != null) 
						&& (root.SelectSingleNode("bannerID").InnerText != null)))
			{
				_bannerID = Convert.ToInt32(root.SelectSingleNode("bannerID").InnerText);
				_bannerID_isModified = false;
			}
			if (((root.SelectSingleNode("shopType") != null) 
						&& (root.SelectSingleNode("shopType").InnerText != null)))
			{
				_shopType = Convert.ToInt32(root.SelectSingleNode("shopType").InnerText);
				_shopType_isModified = false;
			}
			if (((root.SelectSingleNode("refererType") != null) 
						&& (root.SelectSingleNode("refererType").InnerText != null)))
			{
				_refererType = Convert.ToInt32(root.SelectSingleNode("refererType").InnerText);
				_refererType_isModified = false;
			}
			if (((root.SelectSingleNode("header") != null) 
						&& (root.SelectSingleNode("header").InnerText != null)))
			{
				_header = root.SelectSingleNode("header").InnerText;
				_header_isModified = false;
			}
			if (((root.SelectSingleNode("text") != null) 
						&& (root.SelectSingleNode("text").InnerText != null)))
			{
				_text = root.SelectSingleNode("text").InnerText;
				_text_isModified = false;
			}
			if (((root.SelectSingleNode("href") != null) 
						&& (root.SelectSingleNode("href").InnerText != null)))
			{
				_href = root.SelectSingleNode("href").InnerText;
				_href_isModified = false;
			}
			if (((root.SelectSingleNode("geo") != null) 
						&& (root.SelectSingleNode("geo").InnerText != null)))
			{
				_geo = root.SelectSingleNode("geo").InnerText;
				_geo_isModified = false;
			}
			if (((root.SelectSingleNode("geoIDs") != null) 
						&& (root.SelectSingleNode("geoIDs").InnerText != null)))
			{
				_geoIDs = root.SelectSingleNode("geoIDs").InnerText;
				_geoIDs_isModified = false;
			}
			if (((root.SelectSingleNode("object") != null) 
						&& (root.SelectSingleNode("object").InnerText != null)))
			{
				ObjectItem objectItem = new ObjectItem(new Guid(root.SelectSingleNode("object").InnerText), root.SelectSingleNode("object_NK").InnerText, root.SelectSingleNode("object_className").InnerText);
				_object = objectItem;
				_object_isModified = false;
			}
			if (((root.SelectSingleNode("isDirty") != null) 
						&& (root.SelectSingleNode("isDirty").InnerText != null)))
			{
				_isDirty = root.SelectSingleNode("isDirty").InnerText == "1" ? true : false;
				_isDirty_isModified = false;
			}
			if (((root.SelectSingleNode("headerTemplate") != null) 
						&& (root.SelectSingleNode("headerTemplate").InnerText != null)))
			{
				_headerTemplate = root.SelectSingleNode("headerTemplate").InnerText;
				_headerTemplate_isModified = false;
			}
			if (((root.SelectSingleNode("bodyTemplate") != null) 
						&& (root.SelectSingleNode("bodyTemplate").InnerText != null)))
			{
				_bodyTemplate = root.SelectSingleNode("bodyTemplate").InnerText;
				_bodyTemplate_isModified = false;
			}
		}
		
		protected override void LoadMultiProp()
		{
			XmlNodeList nl = null;
			nl = root.SelectNodes("errors");
			if ((nl.Count != 0))
			{
				_errors.Clear();
				for (int i = 0; (i < nl.Count); i = (i + 1))
				{
					XmlNode n = nl[i];
					object propValue = null;
					if ((n.SelectSingleNode("propValue") != null))
					{
						propValue = n.SelectSingleNode("propValue").InnerText;
					}
					else
					{
						propValue = System.DBNull.Value;
					}
					_errors.Rows.Add(new object[] {
								propValue,
								Convert.ToDateTime(n.SelectSingleNode("ordValue").InnerText, this.DateTimeFormat)});
				}
				_errors.AcceptChanges();
			}
			nl = root.SelectNodes("phrases");
			if ((nl.Count != 0))
			{
				_phrases.Clear();
				for (int i = 0; (i < nl.Count); i = (i + 1))
				{
					XmlNode n = nl[i];
					object propValue = null;
					if ((n.SelectSingleNode("propValue") != null))
					{
						propValue = n.SelectSingleNode("propValue").InnerText;
					}
					else
					{
						propValue = System.DBNull.Value;
					}
					_phrases.Rows.Add(new object[] {
								propValue,
								Convert.ToInt32(n.SelectSingleNode("ordValue").InnerText)});
				}
				_phrases.AcceptChanges();
			}
			base.LoadMultiProp();
		}
		
		protected override void SaveSingleProp()
		{
			base.SaveSingleProp();
			if ((_campaignID_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("campaignID"));
				prop.InnerText = _campaignID.ToString();
			}
			if ((_bannerID_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("bannerID"));
				prop.InnerText = _bannerID.ToString();
			}
			if ((_shopType_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("shopType"));
				prop.InnerText = _shopType.ToString();
			}
			if ((_refererType_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("refererType"));
				prop.InnerText = _refererType.ToString();
			}
			if ((_header_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("header"));
				prop.InnerText = _header;
			}
			if ((_text_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("text"));
				prop.InnerText = _text;
			}
			if ((_href_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("href"));
				prop.InnerText = _href;
			}
			if ((_geo_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("geo"));
				prop.InnerText = _geo;
			}
			if ((_geoIDs_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("geoIDs"));
				prop.InnerText = _geoIDs;
			}
			if ((_object_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("object"));
				prop.InnerText = _object.OID.ToString();
			}
			if ((_isDirty_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("isDirty"));
				prop.InnerText = _isDirty.ToString();
			}
			if ((_headerTemplate_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("headerTemplate"));
				prop.InnerText = _headerTemplate;
			}
			if ((_bodyTemplate_isModified || SaveAllFlag))
			{
				XmlNode prop = root.AppendChild(root.OwnerDocument.CreateElement("bodyTemplate"));
				prop.InnerText = _bodyTemplate;
			}
		}
		
		protected override void SaveMultiProp()
		{
			base.SaveMultiProp();
			_errors.DiffGram(root);
			_phrases.DiffGram(root);
		}
		
		public override void Refresh()
		{
			_campaignID_isModified = false;
			_bannerID_isModified = false;
			_shopType_isModified = false;
			_refererType_isModified = false;
			_header_isModified = false;
			_text_isModified = false;
			_href_isModified = false;
			_geo_isModified = false;
			_geoIDs_isModified = false;
			_object_isModified = false;
			_isDirty_isModified = false;
			_headerTemplate_isModified = false;
			_bodyTemplate_isModified = false;
			_errors.AcceptChanges();
			_phrases.AcceptChanges();
			base.Refresh();
		}
	}
}
