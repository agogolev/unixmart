﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using System.Xml;
using ColumnMenuExtender.Forms;
using ELBClient.Classes;

namespace ELBClient
{
    /// <summary>
    ///     Summary description for GenericForm.
    /// </summary>
    public partial class GenericForm : BaseGenericForm
    {
        protected ListProvider.ListProvider lp;
        protected ObjectProvider.ObjectProvider op;

        protected override string FileName
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                       @"\Invento\ELBClient\forms.xml";
            }
        }

        protected override string HintTextBoxFileName
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                       @"\Invento\ELBClient\HintTextBox.xml";
            }
        }

        public void ShowSoapException(SoapException err)
        {
            string message = "Type: " + err.GetType() + "\r\n\r\n";
            message += "*** Message: ***\r\n" + err.Message + "\r\n\r\n";
            message += "*** Actor: ***\r\n" + err.Actor + "\r\n\r\n";
            message += "*** Code: ***\r\n" + err.Code + "\r\n\r\n";
            message += "*** Detail: ***\r\n" + err.Detail.OuterXml;
            MessageBox.Show(this, message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!DesignMode)
            {
                lp = ServiceUtility.ListProvider;
                op = ServiceUtility.ObjectProvider;
            }
            base.OnLoad(e);
        }

        public static string GetResourceString(string name)
        {
            //Loading Embedded Resource
            var sr = new StreamReader(
                Assembly.GetExecutingAssembly().GetManifestResourceStream("ELBClient.Resources." + name),
                Encoding.UTF8);
            return sr.ReadToEnd();
        }

        public static string GetResourceString(string name, int encoding)
        {
            //Loading Embedded Resource
            var sr = new StreamReader(
                Assembly.GetExecutingAssembly().GetManifestResourceStream("ELBClient.Resources." + name),
                Encoding.GetEncoding(encoding));
            return sr.ReadToEnd();
        }

        public static XmlDocument GetResource(string name)
        {
            string xml = GetResourceString(name);
            var doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        public static string ToSqlString(object o)
        {
            if (o == null || o == DBNull.Value) return "null";
            if ((o is string)) return "'" + ((string) o).Replace("'", "''") + "'";
            if ((o is Guid))
                if (((Guid) o) != Guid.Empty) return ("'" + o + "'");
                else return "null";
            if ((o is decimal)) return o.ToString().Replace(",", ".");
            if ((o is DateTime)) return "'" + ((DateTime) o).ToString("yyyyMMdd HH:mm:ss.fff") + "'";
            if ((o is bool)) return (bool) o ? "1" : "0";
            return o.ToString();
        }
    }
}