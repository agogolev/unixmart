﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using ELBClient.Classes;
using ELBClient.ObjectProvider;

namespace ELBClient
{
    public partial class EditForm : GenericForm
    {
        public bool NeedCheckOnClosing = true; //возможность не делать проверку isModified при закрытии формы
        public ReloadDelegate ReloadGrid;
        private Guid _objectOID = Guid.Empty;

        public EditForm()
        {
            Closing += EditForm_Closing;
        }

        #region Save Methods

        protected virtual void SaveObject(CancelEventArgs e, bool isClosingAfter)
        {
            if (!e.Cancel)
                SaveObject(isClosingAfter);
        }

        protected virtual void SaveObject(CancelEventArgs e)
        {
            if (!e.Cancel)
                SaveObject();
        }

        protected virtual void SaveObject()
        {
            SaveObject(MainForm.isCloseAfterSave);
        }

        protected virtual void SaveObject(bool isClosingAfter)
        {
            SaveObject(GetXml(), isClosingAfter);
        }

        public string GetXml()
        {
            BindingContext[Object].EndCurrentEdit();

            return Object.SaveXml();
        }

        protected void SaveObject(string xml, bool isClosingAfter)
        {
            string error = AllowToSave();
            if (!string.IsNullOrEmpty(error))
            {
                ShowError(error);
                return;
            }
            string xmlObj;
            ExceptionISM exISM;

            try
            {
                OnBeforeSave();
                xmlObj = op.SaveObject(xml, out exISM);
            }
            catch (Exception exc)
            {
                //может возникнуть из-за разрыва соединения
                ShowError("Ошибка сохранения: " + exc.Message);
                Object.ExtMultiProp = "";
                throw;
            }

            if (exISM != null)
            {
                if (exISM.LiteralExceptionType == "BackendService.Classes.DBException")
                {
                    ShowError(this, "Ошибка сохранения: ошибка целостности");
                    Object.ExtMultiProp = "";
                    throw new Exception(exISM.LiteralMessage);
                }
                error = "Ошибка сохранения\r\n";
#if DEBUG
                error += exISM.LiteralMessage;
#endif
                ShowError(this, error);
                Object.ExtMultiProp = "";
                throw new Exception(exISM.LiteralMessage);
            }
            if (ReloadGrid != null)
                ReloadGrid();
            if (!isClosingAfter)
            {
                Object.LoadXml(xmlObj);
                ObjectOID = Object.OID;
                Object.ExtMultiProp = "";
                if (ObjectOID != Guid.Empty)
                    MainFrm.OpenedForms[GetType() + "/" + ObjectOID] = this;
            }
            else Close();
        }

        protected void SaveObject(string xml)
        {
            SaveObject(xml, MainForm.isCloseAfterSave);
        }

        #endregion

        public Guid ObjectOID
        {
            get { return _objectOID; }
            set { _objectOID = value; }
        }

        public virtual string ObjectNK
        {
            get { return ""; }
        }

        public string ObjectClassName
        {
            get { return Object != null ? Object.ClassName : string.Empty; }
        }

        public DBObject Object { get; protected set; }

        protected virtual bool IsModified
        {
            get { return Object != null && Object.IsModified; }
        }

        public event EventHandler BeforeSave;

        protected void OnBeforeSave()
        {
            if (BeforeSave != null) BeforeSave(this, EventArgs.Empty);
        }

        /// <summary>
        ///     Грузит все сингл и мульти поля
        /// </summary>
        protected void LoadObject()
        {
            LoadObject("", "");
        }

        /// <summary>
        ///     Грузит только те сингл и мульти поля, которые заказали
        /// </summary>
        /// <param name="fieldNames">Список сингл полей (значение null - если ничего не надо)</param>
        /// <param name="multiProps">Список мульти полей (значение null - если ничего не надо)</param>
        protected void LoadObject(string fieldNames, string multiProps)
        {
            if (ObjectOID != Guid.Empty)
            {
                string xml = op.GetObject(ObjectOID.ToString(), fieldNames, multiProps);
                Object.LoadXml(xml);
            }
            else
            {
                Object.Refresh();
            }
        }

        /// <summary>
        ///     Грузит только те поля, которые указаны в XML файле
        /// </summary>
        /// <param name="xml">XML файл</param>
        /// <param name="CID">CID объекта (в принципе можно было запрашивать, но для экономии трафика сделано так)</param>
        /// <returns>Возвращает вернувшийся XML файл</returns>
        protected string LoadObject(string xml, Guid CID)
        {
            try
            {
                if (ObjectOID != Guid.Empty)
                {
                    xml = op.GetObject(ObjectOID.ToString(), xml, "");
                    var doc = new XmlDocument();
                    doc.LoadXml(xml);
                    XmlNode child = doc.CreateNode(XmlNodeType.Element, "CID", "");
                    child.InnerText = CID.ToString();
                    doc.DocumentElement.AppendChild(child);
                    Object.LoadXml(doc.InnerXml);
                    return doc.InnerXml;
                }
                Object.Refresh();
                return "";
            }
            catch (Exception e)
            {
                ShowError(e.Message);
                return null;
            }
        }

        protected virtual string AllowToSave()
        {
            return "";
        }

        private void EditForm_Closing(object sender, CancelEventArgs e)
        {
            if (MainForm.isCloseWarningShow && IsModified && DialogResult != DialogResult.OK && NeedCheckOnClosing)
            {
                DialogResult dr = MessageBox.Show(this, "Элемент был изменен, хотите сохранить?", "Внимание!",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    string str = AllowToSave();
                    if (str != null)
                    {
                        if (str == "")
                        {
                            SaveObject(e);
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            if (
                                MessageBox.Show(this, str + "\nПродолжить редактирование?", "Внимание",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                                e.Cancel = true;
                        }
                    }
                    else e.Cancel = true;
                }
                else if (dr == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        public void SetCustomTheme(Guid themeOID, string themeNK)
        {
            //useCustomTheme = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            //сохраняем форму в коллекции открытых форм, чтобы позже при вызове данной формы
            //не открывать новую, а активировать уже имеющуюся
            if (!DesignMode && ObjectOID != Guid.Empty)
                MainFrm.OpenedForms[GetType() + "/" + ObjectOID] = this;
            base.OnLoad(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (!DesignMode && ObjectOID != Guid.Empty)
                MainFrm.OpenedForms[GetType() + "/" + ObjectOID] = null;
            base.OnClosed(e);
        }


        public override void Show()
        {
            if (ObjectOID != Guid.Empty)
            {
                var form = (EditForm) MainFrm.OpenedForms[GetType() + "/" + ObjectOID];
                if (form != null)
                {
                    if (form.WindowState == FormWindowState.Minimized)
                        form.WindowState = FormWindowState.Normal;
                    form.Activate();
                }
                else
                    base.Show();
            }
            else
                base.Show();
        }
    }
}