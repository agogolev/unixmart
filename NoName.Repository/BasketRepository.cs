using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DBReader;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using MetaData;
using NoName.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class BasketRepository : IBasketRepository
    {
        private readonly ICouponRepository _couponRepository;

        private readonly IDeliveryRepository _deliveryRepository;

        public BasketRepository(IDeliveryRepository deliveryRepository, ICouponRepository couponRepository)
        {
            _deliveryRepository = deliveryRepository;
            _couponRepository = couponRepository;
        }

        public void ModifyGoodsInBasket(Basket basket, Guid goodsOID, int count, bool relative, bool isSale)
        {
            const string sql = "upModifyBasket";
            var prms = new Hashtable();
            prms["sessionOID"] = basket.Session;
            prms["goodsOID"] = goodsOID;
            prms["amount"] = count;
            prms["relative"] = relative;
            prms["sale"] = isSale;
            //prms["price"] = _couponRepository.GoodsPrice(goods, basket.Coupon);
            DBHelper.ExecuteCommand(sql, prms, true);
        }

        public Basket GetBasketBySession(int shopType, int rootNode, Guid sessionOID)
        {
            const string sql = @"

SELECT 
	b.OID
	, b.session
	, b.deliveryType
	, b.payType
	, b.name
	, b.phone
	, b.email
	, b.city
	, b.streetName
	, b.metroName
	, b.house
	, b.korp
	, b.build
	, b.flat
	, needCityInOrder = td.needCityInfo
    , b.calcDistance
    , promoID = p.OID
    , p.promoCode
    , p.discount
FROM 
	t_Basket b
	left join t_TypeDelivery td on b.deliveryType = td.deliveryType
    left join t_PromoCode p on b.promoCode = p.OID
WHERE session = @session

SELECT
	g.OID
	, o.objectId
	, g.pseudoID
	, g.Model
	, g.productType
	, c.companyName
	, gp.price
	, gp.salePrice
	, gp.deliveryPrice
	, gp.deliveryPrice2
	, gp.deliveryPriceSelf
    , gp.onlinePay
	, bo.ordValue
	, bo.amount
    , bo.sale
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, ISNULL(sc.nodeId, 0) categoryID
FROM
	t_BasketObjects bo
	inner join t_Basket b on bo.OID = b.OID and b.session = @session
	inner join t_Goods g on bo.goodsOID = g.OID
	inner join t_Object o on g.OID = o.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100)
	left join t_Company c on g.manufacturer = c.OID 
	outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для табличного вывода%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID) 
	outer apply (select TOP 1 n.nodeId from t_Goods g
				inner join t_Object on g.OID = t_Object.OID and o.objectID = t_Object.objectID
				inner join t_ThemeMasters tm on tm.masterOID = g.category
				inner join t_Nodes n on n.object = tm.OID
				inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode) AS sc
ORDER BY
	bo.ordValue

SELECT
    td.fixPrice
    , b.deliveryType
    , td.price
FROM
    t_Basket b
    left join t_TypeDelivery td on td.deliveryType = b.deliveryType
WHERE session = @session


SELECT
    pg.*
FROM
    t_PromoCodeGoods pg
    inner join t_Basket b on b.promoCode = pg.OID
WHERE session = @session
";
            var prms = new Hashtable
            {
                ["session"] = sessionOID,
                ["shopType"] = shopType,
                ["rootNode"] = rootNode
            };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            Basket item = ds.Table.AsEnumerable()
                .Select(row => new Basket
                {
                    OID = row.Field<Guid>("OID"),
                    Session = row.Field<Guid>("session"),
                    DeliveryType = row.Field<int?>("deliveryType"),
                    PayType = row.Field<int?>("payType"),
                    NeedCityInOrder = row.Field<bool?>("NeedCityInOrder") ?? false,
                    Name = row.Field<string>("name"),
                    Phone = row.Field<string>("phone"),
                    Email = row.Field<string>("email"),
                    City = row.Field<string>("city"),
                    StreetName = row.Field<string>("streetName"),
                    MetroName = row.Field<string>("metroName"),
                    House = row.Field<string>("house"),
                    Korp = row.Field<string>("korp"),
                    Build = row.Field<string>("build"),
                    Flat = row.Field<string>("flat"),
                    DeliveryPrice = 0,
                    CalcDistance = row.Field<int>("calcDistance"),
                    Coupon = row.Field<Guid?>("promoID").HasValue ?
                        new Coupon
                        {
                            OID = row.Field<Guid>("promoID"),
                            PromoCode = row.Field<string>("promoCode"),
                            Discount = row.Field<decimal>("discount"),
                            AffectedGoods = ds.Tables[3]
                                .AsEnumerable()
                                .Select(r1 => new GoodsItem { OID = r1.Field<Guid>("goodsOID") })
                                .ToList()
                        } : null
                }
                ).SingleOrDefault();

            if (item != null)
            {
                item.Items = ds.Tables[1].AsEnumerable()
                    .Select(row => new BasketContent
                    {
                        OID = row.Field<Guid>("OID"),
                        OrdValue = row.Field<int>("ordValue"),
                        ObjectId = row.Field<int>("objectID"),
                        PseudoID = row.Field<string>("pseudoID"),
                        ProductType = row.Field<string>("productType"),
                        ManufacturerName = row.Field<string>("companyName"),
                        Model = row.Field<string>("Model"),
                        OriginalPrice = row.Field<decimal>("price"),
                        IsSale = row.Field<bool>("sale"),
                        Price = _couponRepository.GoodsPrice(new GoodsItem
                        {
                            OID = row.Field<Guid>("OID"),
                            Price = row.Field<bool>("sale") && row.Field<decimal?>("salePrice").HasValue ? row.Field<decimal?>("salePrice").Value : row.Field<decimal>("price")
                        },
                            item.Coupon),
                        OnlinePay = row.Field<bool>("onlinePay"),
                        Amount = row.Field<int>("amount"),
                        DeliveryFirst = row.Field<decimal?>("deliveryPrice") ?? 0,
                        DeliverySecond = row.Field<decimal?>("deliveryPrice2") ?? 0,
                        DeliverySelf = row.Field<decimal?>("deliveryPriceSelf") ?? 0,
                        Image = new Image
                        {
                            Id = row.Field<int?>("imageID") ?? GoodsRepository.BlankImageSmallId,
                            OID = row.Field<Guid?>("image") ?? GoodsRepository.BlankImageSmall,
                            Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                            Width = row.Field<int?>("imageWidth") ?? GoodsRepository.BlankImageSmallWidth,
                            Height = row.Field<int?>("imageHeight") ?? GoodsRepository.BlankImageSmallHeight,
                            MimeType = row.Field<string>("mimeType") ?? GoodsRepository.BlankImageSmallMimeType
                        },
                    }).ToList();
                var fixDeliveryPrice = ds.Tables[2].AsEnumerable().Select(row => row.Field<decimal?>("fixPrice")).Single();
                var deliveryPrice = ds.Tables[2].AsEnumerable().Select(row => row.Field<decimal?>("price")).Single() ?? 0;
                var deliveryType = ds.Tables[2].AsEnumerable().Select(row => row.Field<int?>("deliveryType")).Single();
                if (deliveryType.HasValue && DeliveryConstants.SelfDeliveryIds.Any(id => deliveryType.Value == id))
                {
                    item.VariableDeliveryPart = _deliveryRepository.CalcDeliveryPrice(item.Items);
                    item.DeliveryPrice = _deliveryRepository.SelfDeliveryPrice(item.Items, deliveryPrice);
                }
                else
                {
                    if (item.Items.Count > 0)
                    {
                        item.VariableDeliveryPart = _deliveryRepository.CalcDeliveryPrice(item.Items);
                        item.DeliveryPriceWithoutDistance = fixDeliveryPrice ??
                                                            item.VariableDeliveryPart + _deliveryRepository.GetAddPrice(item.DeliveryType);
                        item.DeliveryPrice = fixDeliveryPrice ??
                                             item.VariableDeliveryPart + _deliveryRepository.GetAddPrice(item.DeliveryType) + _deliveryRepository.GetDistancePrice(item.DeliveryType, item.CalcDistance);
                    }
                }
            }

            return item ?? new Basket { Session = sessionOID };
        }

        public IList<GoodsItem> GetBasketAccessory(int shopType, int rootNode, Guid sessionOID)
        {
            const string sql = @"
SELECT DISTINCT
	g.OID
	, o.objectID
	, g.Model
	, g.productType
	, c.companyName
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, gp.price
    , gs.inStock
    , gs.zombi
FROM
    t_BasketObjects bo
    inner join t_Basket b on bo.OID = b.OID and b.session = @session
	inner join t_Goods bg on bo.goodsOID = bg.OID
	join t_GoodsAccessories ga on bg.OID = ga.OID	
	join t_Goods g on ga.accessoryOID = g.OID
	inner join t_Object o on g.OID = o.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
	inner join t_Company c on g.manufacturer = c.OID
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для табличного вывода%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_BasketObjects bo
    inner join t_Basket b on bo.OID = b.OID and b.session = @session
	inner join t_Goods bg on bo.goodsOID = bg.OID
	join t_ObjectAccessories ga on bg.OID = ga.OID	
	join t_Goods g on ga.accessoryOID = g.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
    inner join t_ThemeMasters tm on g.category = tm.masterOID
	inner join t_TemplateParams tp on tp.OID = tm.OID And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID";

            var ds = sql.ExecMSql(new { session = sessionOID, shopType, rootNode });
            return ds[0].AsEnumerable()
                .Select(row => new GoodsItem
                {
                    OID = row.Field<Guid>("OID"),
                    ObjectID = row.Field<int>("objectID"),
                    ProductType = row.Field<string>("productType"),
                    Model = row.Field<string>("model"),
                    ManufacturerName = row.Field<string>("companyName"),
                    Image = new Image
                    {
                        Id = row.Field<int?>("imageID") ?? GoodsRepository.BlankImageSmallId,
                        OID = row.Field<Guid?>("image") ?? GoodsRepository.BlankImageSmall,
                        Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                        Width = row.Field<int?>("imageWidth") ?? GoodsRepository.BlankImageSmallWidth,
                        Height = row.Field<int?>("imageHeight") ?? GoodsRepository.BlankImageSmallHeight,
                        MimeType = row.Field<string>("mimeType") ?? GoodsRepository.BlankImageSmallMimeType
                    },
                    Price = row.Field<decimal>("price"),
                    InStock = row.Field<bool>("inStock") && !row.Field<bool>("zombi"),
                    Zombi = row.Field<bool>("zombi"),
                    GoodsParams = ds[1].AsEnumerable()
                        .Where(k => k.Field<Guid>("OID") == row.Field<Guid>("OID") && k.Field<Guid>("paramTypeOID") != Settings.Default.ShortDescriptionParam)
                        .OrderBy(k => k.Field<int>("ordValue"))
                        .Select(k => new ParamItem
                        {
                            ParamType = k.Field<Guid>("paramTypeOID"),
                            ParamName = k.Field<string>("name"),
                            ParamValue = k.Field<string>("value"),
                            IsFilter = k.Field<bool>("isFilter"),
                            IsFullName = k.Field<bool>("isFullName"),
                            Unit = k.Field<string>("unit")
                        }).ToList()
                }).ToList();
        }

        public void SaveDeliveryType(Guid basket, int deliveryType, int calcDistance)
        {
            const string sql = @"
UPDATE t_Basket SET 
    deliveryType = @deliveryType,
    calcDistance = @calcDistance
WHERE OID = @basket";
            var prms = new Hashtable();
            prms["basket"] = basket;
            prms["deliveryType"] = deliveryType;
            prms["calcDistance"] = calcDistance;
            DBHelper.ExecuteScalar(sql, prms, false);
        }

        public void SavePayType(Guid basket, int payType)
        {
            const string sql = "UPDATE t_Basket SET payType = @payType WHERE OID = @basket";
            var prms = new Hashtable();
            prms["basket"] = basket;
            prms["payType"] = payType;
            DBHelper.ExecuteScalar(sql, prms, false);
        }

        public void SavePromoCode(Guid basket, string promoCode)
        {
            const string sql = @"
UPDATE t_Basket 
    SET promoCode = p.OID 
FROM 
    t_Basket b
    left join t_PromoCode p on b.OID = @basket and p.promoCode = @promoCode";
            var prms = new Hashtable();
            prms["basket"] = basket;
            prms["promoCode"] = promoCode;
            DBHelper.ExecuteScalar(sql, prms, false);
        }

        //public void SaveContacts(
        //    Guid basket,
        //    string name,
        //    string phone,
        //    string email)
        //{
        //    const string sql = "UPDATE t_Basket SET name = @name, phone = @phone, email = @email WHERE OID = @basket";
        //    var prms = new Hashtable();
        //    prms["basket"] = basket;
        //    prms["name"] = name;
        //    prms["phone"] = phone;
        //    prms["email"] = email;
        //    DBHelper.ExecuteScalar(sql, prms, false);
        //}

        //public void SaveAddress(
        //    Guid basket,
        //    string city,
        //    string streetName,
        //    string house,
        //    string korp,
        //    string build,
        //    string flat,
        //    string metroName)
        //{
        //    const string sql = "UPDATE t_Basket SET city = @city, streetName = @streetName, house = @house, korp = @korp, build = @build, flat = @flat, metroName = @metroName WHERE OID = @basket";
        //    var prms = new Hashtable();
        //    prms["basket"] = basket;
        //    prms["city"] = city;
        //    prms["streetName"] = streetName;
        //    prms["house"] = house;
        //    prms["korp"] = korp;
        //    prms["build"] = build;
        //    prms["flat"] = flat;
        //    prms["metroName"] = metroName;
        //    DBHelper.ExecuteScalar(sql, prms, false);
        //}
    }
}