using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using DBReader;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using Ecommerce.Repository.CommodityGroup;
using NLog;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class YandexMarketRepository : IYandexMarketRepository
    {
        private static readonly Regex normGoogle = new Regex(@"[^a-z0-9\s_]", RegexOptions.IgnoreCase);
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        private static int cutLengthForYandex = 3000; //500;

        private readonly ICommodityGroupRepository _commodityGroupRepository;

        private readonly IMenuRepository _menuRepository;

        public YandexMarketRepository(ICommodityGroupRepository commodityGroupRepository, IMenuRepository menuRepository)
        {
            _commodityGroupRepository = commodityGroupRepository;
            _menuRepository = menuRepository;
        }

        public void GetMarketData(HttpResponseBase response, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode, bool hasPickup, bool hasStore, bool second)
        {
            var xmlFolder = new Mutex(false, "xmlFolderUnixmartYandex" + shopType);
            xmlFolder.WaitOne();
            try
            {
                var fileName = !second
                    ? HttpContext.Current.Server.MapPath($"/xml/yandexFile_{shopType}.xml")
                    : HttpContext.Current.Server.MapPath($"/xml/yandexFileSecond_{shopType}.xml");
                if (!second)
                {
                    var fi = new FileInfo(fileName);
                    if (!fi.Exists)
                    {
                        CreateFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup, hasStore, false);
                    }
                    response.WriteFile(fi.FullName);
                }
                else
                {
                    var fi = new FileInfo(fileName);
                    if (!fi.Exists)
                    {
                        CreateSecondFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup, hasStore, false);
                    }
                    response.WriteFile(fi.FullName);
                }
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void GetMarketDataRise(HttpResponseBase response, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode, bool hasPickup, bool hasStore)
        {
            var xmlFolder = new Mutex(false, "xmlFolderUnixmartYandex" + shopType);
            xmlFolder.WaitOne();
            try
            {
                var fileName = HttpContext.Current.Server.MapPath($"/xml/yandexFileRise_{shopType}.xml");
                var fi = new FileInfo(fileName);
                if (!fi.Exists)
                {
                    CreateFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup, hasStore, true);
                }
                response.WriteFile(fi.FullName);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshMarketData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup, bool hasStore)
        {
            var xmlFolder = new Mutex(false, $"xmlFolderUnixmartYandex{shopType}");
            xmlFolder.WaitOne();
            try
            {
                var fileName = HttpContext.Current.Server.MapPath($"/xml/yandexFile_{shopType}.xml");
                var fi = new FileInfo(fileName);
                CreateFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup, hasStore, false);

                fileName = HttpContext.Current.Server.MapPath($"/xml/yandexFileRise_{shopType}.xml");
                fi = new FileInfo(fileName);
                CreateFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup, hasStore, true);

                fileName = HttpContext.Current.Server.MapPath($"/xml/yandexFileSecond_{shopType}.xml");
                fi = new FileInfo(fileName);
                CreateSecondFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup, hasStore, false);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshAdmitadData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup)
        {
        }

        public void RefreshGoogleData(int shopType, string siteName, string siteUrl, int rootNode)
        {
            var googleGroup =
                "SELECT OID FROM t_CommodityGroup WHERE url = 'googleMerchant.xml' and shopType = @shopType"
                    .ExecSql(new { shopType })
                    .Select(row => row.Field<Guid>("OID")).FirstOrDefault();
            var xmlFolder = new Mutex(false, string.Format("xmlFolderUnixmartGoogle{0}", shopType));
            xmlFolder.WaitOne();
            try
            {
                var fileName = HttpContext.Current.Server.MapPath("/xml/googleMerchant.xml");
                var fi = new FileInfo(fileName);
                CreateMerchantFile(googleGroup, fi.FullName, shopType, siteName, siteUrl, rootNode);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshWikimartData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup)
        {
            var xmlFolder = new Mutex(false, string.Format("xmlFolderUnixmartWikimart{0}", shopType));
            xmlFolder.WaitOne();
            try
            {
                var fileName =
                    HttpContext.Current.Server.MapPath(string.Format("/xml/wikimartFile_{0}.wml", shopType));
                var fi = new FileInfo(fileName);
                CreateWikimartFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshWikiData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup)
        {
            var xmlFolder = new Mutex(false, string.Format("xmlFolderUnixmartWiki{0}", shopType));
            xmlFolder.WaitOne();
            try
            {
                var fileName = HttpContext.Current.Server.MapPath(string.Format("/xml/wikiFile_{0}.wml", shopType));
                var fi = new FileInfo(fileName);
                const int kortingCompany = 23791;
                const decimal minusPercent = 0.17M;
                CreateWikimartAltFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup,
                    kortingCompany, minusPercent);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshNadaviData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup)
        {
            var xmlFolder = new Mutex(false, string.Format("xmlFolderUnixmartNadavi{0}", shopType));
            xmlFolder.WaitOne();
            try
            {
                var fileName = HttpContext.Current.Server.MapPath(string.Format("/xml/nadaviFile_{0}.xml", shopType));
                var fi = new FileInfo(fileName);
                CreateNadaviFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshUtinetData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup)
        {
            var xmlFolder = new Mutex(false, string.Format("xmlFolderUnixmartUtinet{0}", shopType));
            xmlFolder.WaitOne();
            try
            {
                var fileName = HttpContext.Current.Server.MapPath("/xml/utinetFile.xml");
                var fi = new FileInfo(fileName);
                CreateUtinetFile(fi.FullName, shopType, siteName, siteTitle, siteUrl, rootNode, hasPickup);
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void RefreshCommodityGroupData(Guid OID, string siteName, string siteTitle, string siteUrl, int rootNode,
            bool hasPickup)
        {
            var option =
                "SELECT url, priceAggregator, globalUtmLabel, includeBidCBid, yandexDtd, paramToDescription, torgMailDtd, forContext, forABC, useLargeImage, postavshik_id, ean FROM t_CommodityGroup WHERE OID = @OID"
                    .ExecSql(new { OID })
                    .Select(row => new CommodityGroupOption
                    {
                        Url = row.Field<string>("url"),
                        PriceAggregator = row.Field<string>("priceAggregator"),
                        GlobalUtmLabel = row.Field<string>("globalUtmLabel"),
                        IncludeBidCBid = row.Field<bool>("includeBidCBid"),
                        YandexDtd = row.Field<bool>("yandexDtd"),
                        ParamToDescription = row.Field<bool>("paramToDescription"),
                        TorgMailDtd = row.Field<bool>("torgMailDtd"),
                        ForContext = row.Field<bool>("forContext"),
                        ForABC = row.Field<bool>("forABC"),
                        UseLargeImage = row.Field<bool>("useLargeImage"),
                        PostavshikId = row.Field<bool>("postavshik_id"),
                        EAN = row.Field<bool>("ean")
                    }).SingleOrDefault();
            var fullName = HttpContext.Current.Server.MapPath($"/xml/{option.Url}");
            var xmlFolder = new Mutex(false, fullName.Replace('\\', '_'));
            xmlFolder.WaitOne();
            try
            {
                var fi = new FileInfo(fullName);
                if (!option.TorgMailDtd)
                {
                    CreateCommodityFile(fi.FullName, OID, siteName, siteTitle, siteUrl, rootNode, hasPickup, option);
                }
                else
                {
                    CreateTorgMailFile(fi.FullName, OID, siteName, siteUrl, Settings.Default.LogoUrl, option);
                }
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        public void CreateCommodityFile(string fileName, Guid OID, string siteName, string siteTitle, string siteUrl,
            int rootNode, bool hasPickup, CommodityGroupOption option)
        {
            var catalogOID = Guid.Empty;
            var shopType = _commodityGroupRepository.GetCommodityGroupShop(OID);
            _commodityGroupRepository.GenerateCommodityGroupContent(OID, true, true, true);
            if (string.IsNullOrWhiteSpace(Path.GetFileName(fileName)))
            {
                DBHelper.ExecuteCommand("UPDATE t_CommodityGroup SET lastGenerate = GetDate() WHERE OID = @OID", new Hashtable { { "OID", OID } }, false);
                return;
            }
            var doc = new XDocument(XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
		{4}
		<offers />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl, option.YandexDtd ? "" : "<vendors />")));
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree, o.shortLink
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID 
    inner join t_Object o on t.OID = o.OID
WHERE
    o.shortLink is not null and o.shortLink <> ''
ORDER BY n.lft";
            var categories = doc.Descendants("categories").Single();
            var categoryDictionary = new Dictionary<int, string>();
            var first = true;

            sql.ExecSql(new { rootNode }).AsEnumerable()
                .ToList()
                .ForEach(row =>
                {
                    var nodeID = row.Field<int>("nodeID");
                    var parentNode = row.Field<int?>("parentNode") ?? 0;
                    var category = row.Field<string>("nodeName");
                    var shortLink = row.Field<string>("shortLink");
                    var cat = new XElement("category", new XAttribute("id", nodeID),
                        !option.YandexDtd
                            ? new XAttribute("url", _menuRepository.GetCategoryUrl(siteUrl, shortLink, rootNode))
                            : null, new XText(category));
                    if (parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
                    categories.Add(cat);
                    categoryDictionary[nodeID] = category.TranslitEncode(true).ToLower();
                    if (!first) return;
                    catalogOID = row.Field<Guid>("tree");
                    first = false;
                });
            if (!option.YandexDtd)
            {
                var vendors = doc.Descendants("vendors").Single();

                sql = @"
SELECT vendorID = o.objectID, vendor = ISNULL(c.companyName, '') 
FROM 
	t_Company c 
	inner join t_Object o on c.OID = o.OID
WHERE
	c.OID in (SELECT manufacturer FROM t_Goods)
";

                sql.ExecSql(null).ToList()
                    .ForEach(
                        row =>
                            vendors.Add(new XElement("vendor", new XAttribute("vendorID", row.Field<int>("vendorID")),
                                new XText(row.Field<string>("vendor")))));
            }


            var offers = doc.Descendants("offers").Single();

            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!bid] = gpr.bid,
				[offer!1!cbid] = gpr.cbid,
				[offer!1!promo] = gpr.promo,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendorID] = o1.objectID,
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[offer!1!onDemand] = gs.onDemand,
				[offer!1!deliveryPrice!element] = gpr.deliveryPrice,
                [offer!1!postavshik_id] = g.postavshik_id,
                [offer!1!ean] = g.ean,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!deliveryPrice] = null
			FROM
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_Object o on g.OID = o.OID
				inner join t_GoodsPrices gpr on g.OID = gpr.OID and gpr.shopType = @shopType
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_Company c on c.OID = g.manufacturer	
				inner join t_Object o1 on c.OID = o1.OID
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				cross apply (SELECT TOP 1 binData, ordValue FROM t_ObjectImage 
					WHERE OID = g.OID And ordValue like '%IMAGETYPE%' ORDER BY ordValue) oi
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				gp.unit,
				null,
				null
			FROM	
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
				ISNULL(n2.deliveryFirst, 0)
			FROM	
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join (
					SELECT tm0.masterOID, mp0.deliveryFirst, n0.*
					FROM t_Nodes n0
						inner join t_Nodes n10 on n0.tree = n10.tree And n0.lft > n10.lft And n0.lft < n10.rgt And n10.nodeID = @rootNode
						inner join t_ThemeMasters tm0 on n0.object = tm0.OID
						inner join t_MasterPrices mp0 on tm0.masterOID = mp0.OID and mp0.shopType = @shopType
						inner join t_Theme t0 on t0.OID = tm0.OID
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category
			ORDER BY
				[offer!1!id], Tag, [character!3!ordValue!hide]
			FOR XML EXPLICIT";

            string imageType = option.UseLargeImage ? "для popup%" : "для карточки%";
            sql = sql.Replace("%IMAGETYPE%", imageType);

            var prms = new Hashtable();
            prms["@OID"] = OID;
            prms["@shopType"] = shopType;
            prms["@rootNode"] = rootNode;
            prms["@catalogOID"] = catalogOID;

            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            //?utm_source=avatori.ru&utm_medium=PriceAggregator&utm_term=product-name&utm_campaign=product-category
            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                var attrTypePrefix = item.Attribute("typePrefix");
                var attrVendor = item.Attribute("vendor");
                var attrVendorID = item.Attribute("vendorID");
                var attrModel = item.Attribute("model");
                var categoryID = int.Parse(item.Elements("categoryId").First().Value);
                var available = item.Attribute("onDemand") == null
                    ? "true"
                    : (item.Attribute("onDemand").Value == "1" ? "order" : "true");
                var promo = int.Parse(item.Attribute("promo").Value);

                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                {
                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XAttribute("type", "vendor.model"),
                        option.IncludeBidCBid ? item.Attributes("bid").SingleOrDefault() : null,
                        option.IncludeBidCBid ? item.Attributes("cbid").SingleOrDefault() : null,
                        new XAttribute("available", available),
                        new XElement("url",
                            string.Format("{0}{1}",
                                GoodsHelper.GoodsUrl(siteUrl, attrTypePrefix.Value, attrVendor.Value, attrModel.Value,
                                    int.Parse(attrObjectID.Value)),
                                string.IsNullOrWhiteSpace(option.GlobalUtmLabel)
                                    ? (string.IsNullOrWhiteSpace(option.PriceAggregator)
                                        ? ""
                                        : string.Format(
                                            "?utm_source={0}&utm_medium=PriceAggregator&utm_term={1}&utm_campaign={2}",
                                            option.PriceAggregator,
                                            string.Format("{0}_{1}",
                                                attrVendor.Value.Trim().TranslitEncode(true).ToLower(),
                                                attrModel.Value.Trim().TranslitEncode(true).ToLower()),
                                            categoryDictionary[categoryID]
                                            ))
                                    : option.GlobalUtmLabel
                                )),
                        new XElement("price", attrPrice.Value),
                        new XElement("currencyId", "RUR"),
                        new XElement("categoryId", categoryID),
                        option.PostavshikId && item.Attribute("postavshik_id") != null ? new XElement("postavshik_id", item.Attribute("postavshik_id").Value) : null,
                        option.EAN && item.Attribute("ean") != null && !string.IsNullOrEmpty(item.Attribute("ean").Value) ? new XElement("barcode", item.Attribute("ean").Value) : null,
                        (from item1 in item.Elements("picture")
                         select
                             new XElement("picture",
                                 string.Format("{0}/bin.aspx?ID={1}", siteUrl, item1.Attributes("OID").Single().Value)))
                            .FirstOrDefault(),
                        new XElement("store", "true"),
                        new XElement("pickup", hasPickup.ToString(CultureInfo.InvariantCulture).ToLower()),
                        new XElement("delivery", "true"),
                        item.Elements("deliveryPrice").Select(e => new XElement("local_delivery_cost", e.Value))
                            .Union(
                                item.Elements("categoryId")
                                    .Attributes("deliveryPrice")
                                    .Select(a => new XElement("local_delivery_cost", a.Value)))
                            .First(),
                        new XElement("typePrefix", attrTypePrefix.Value),
                        new XElement("vendor",
                            !option.YandexDtd ? new XAttribute("vendorID", attrVendorID.Value) : null,
                            attrVendor.Value),
                        !string.IsNullOrEmpty(item.Attribute("ean")?.Value) ? new XElement("barcode", item.Attribute("ean").Value) : null,
                        new XElement("model", attrModel.Value),
                        option.ForABC && promo != 0 ? new XElement("bonus", "hit null") : null,
                        option.ForContext ? new XElement("priority", promo) : null,
                        option.ParamToDescription
                            ? (!item.Elements("character").Any()
                                ? null
                                : new XElement("description",
                                    new XText(item.Elements("character").Aggregate(new StringBuilder(),
                                        (sb, e) => sb.AppendFormat("{0}: {1}{2},\r\n",
                                            e.Attributes("name").Single().Value.Replace(":", "").Trim(),
                                            e.Attributes("value").Single().Value.Trim(),
                                            e.Attribute("unit") != null
                                                ? string.Format(" {0}", e.Attributes("unit").Single().Value.Trim())
                                                : ""), sb =>
                                                {
                                                    if (sb.Length > 0) sb.Length -= 3;
                                                    return sb.ToString();
                                                })
                                        ))
                                )
                            : null,
                        !option.ForContext && !option.ParamToDescription
                            ? (from item1 in item.Elements("character")
                               select new XElement("param",
                                   new XAttribute("name",
                                       item1.Attributes("name").Single().Value.Replace(":", "").Trim()),
                                   (from item2 in item1.Attributes("unit")
                                    where !string.IsNullOrWhiteSpace(item2.Value)
                                    select new XAttribute("unit", item2.Value.Trim())).SingleOrDefault(),
                                   item1.Attributes("value").Single().Value))
                            : null
                        ));
                }
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
                DBHelper.ExecuteCommand("UPDATE t_CommodityGroup SET lastGenerate = GetDate() WHERE OID = @OID",
                    new Hashtable { { "OID", OID } }, false);
            }
        }

        public void RegisterMarket(string marketUrl, string fullUrl)
        {
            DBHelper.ExecuteCommand("INSERT INTO rpt_YandexUrl (yandexUrl, fullUrl) Values (@yandexUrl, @fullUrl)", new Hashtable()
            {
                { "yandexUrl", marketUrl },
                { "fullUrl", fullUrl }
            }, false);
        }

        private static void CreateFile(string fileName, int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup, bool hasStore, bool useRisePrice)
        {
            var nfi = CultureInfo.InvariantCulture.NumberFormat;
            var catalogOID = Guid.Empty;
            var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
                XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
        <delivery-options>
            <option cost=""0.0000"" days=""1"" />
        </delivery-options>
        <offers />
        <promos />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";
            var prms = new Hashtable();
            prms["@rootNode"] = rootNode;
            var dr = DBHelper.GetData(sql, prms, false);
            try
            {
                var categories = doc.Descendants("categories").Single();
                var first = true;
                while (dr.Read())
                {
                    var nodeID = (int)dr["nodeID"];
                    var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
                    var category = (string)dr["nodeName"];
                    var cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
                    if (parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
                    categories.Add(cat);
                    if (first)
                    {
                        catalogOID = (Guid)dr["tree"];
                        first = false;
                    }
                }
            }
            finally
            {
                dr.Close();
            }

            var offers = doc.Descendants("offers").Single();
            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr1.price,
				[offer!1!bid] = gpr.bid,
				[offer!1!cbid] = gpr.cbid,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!vendorCode] = g.pseudoID,
				[offer!1!model] = ISNULL(g.model, ''),
                [offer!1!cpa] = gpr.cpa,
				[offer!1!ean] = g.ean,
				[offer!1!deliveryPrice!element] = gpr.deliveryPrice,
                [offer!1!deliveryRise!element] = gpr.deliveryRise,
				[offer!1!deliveryDays] = gs.deliveryDays,
                [offer!1!deliveryValidTo] = gs.deliveryValidTo,
				[offer!1!description!element] = gd.description,
				[offer!1!goodsSalesNotes!element] = g.salesNotes,
				[offer!1!weight!element] = g.weight,
				[offer!1!dimensions!element] = g.dimensions,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!deliveryPrice] = null,
				[salesNotes!5!brand] = null,
				[salesNotes!5!lft] = null,
				[salesNotes!5!lftInner] = null,
				[salesNotes!5!] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
				inner join t_GoodsPrices gpr1 on g.OID = gpr1.OID And gpr1.shopType = (100 + @shopType)	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
                inner join t_GoodsPrices gpr1 on g.OID = gpr1.OID And gpr1.shopType = (100 + @shopType)
				inner join t_ObjectImage oi on g.OID = oi.OID and oi.ordValue like 'для popup%'
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				ISNULL(gp.unit, ''),
				null,
				null,
				null,
				null,
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
                inner join t_GoodsPrices gpr1 on g.OID = gpr1.OID And gpr1.shopType = (100 + @shopType)
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
				n2.deliveryFirst,
				null,
				null,
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
                inner join t_GoodsPrices gpr1 on g.OID = gpr1.OID And gpr1.shopType = (100 + @shopType)
				inner join (
					SELECT tm.masterOID, t.priceLimit, mp.deliveryFirst, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_MasterPrices mp on tm.masterOID = mp.OID and mp.shopType = @shopType
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				5,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.manufacturerOID,
				n2.lft,
				n2.lftInner,
				n2.salesNotes
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
                inner join t_GoodsPrices gpr1 on g.OID = gpr1.OID And gpr1.shopType = (100 + @shopType)
				cross apply (
					SELECT TOP 100 tsn.manufacturerOID, tsn.salesNotes, nn1.lft, lftInner = n.lft
					FROM t_Nodes n
						inner join t_Nodes nn1 on n.tree = nn1.tree and nn1.lft BETWEEN n.lft and n.rgt and n.tree = @catalogOID
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on nn1.object = tm.OID and tm.masterOID = g.category
						inner join t_Theme t on t.OID = n.object And t.loadToYandex = 1
						inner join t_ThemeSalesNotes tsn on t.OID = tsn.OID and (tsn.manufacturerOID is null or tsn.manufacturerOID = g.manufacturer)
				) n2  
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			ORDER BY
				[offer!1!id], Tag, [picture!2!ordValue], [character!3!ordValue!hide], [salesNotes!5!brand] desc, [salesNotes!5!lft], [salesNotes!5!lftInner] desc
			FOR XML EXPLICIT";
            prms = new Hashtable
            {
                ["shopType"] = shopType,
                ["rootNode"] = rootNode,
                ["catalogOID"] = catalogOID
            };
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            var ids = new Dictionary<int, bool>();

            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");
                var productType = item.Attribute("typePrefix").Value;
                var manufacturer = item.Attribute("vendor").Value;
                var name = $"{productType} {manufacturer} {attrModel.Value}";
                var deliveryPrice = decimal.Parse(item.Elements("deliveryPrice").Single().Value, nfi);
                var deliveryRise = decimal.Parse(item.Elements("deliveryRise").Single().Value, nfi);
                if (useRisePrice) deliveryPrice += deliveryRise;
                var deliveryValidTo = DateTime.Parse(item.Attribute("deliveryValidTo").Value);
                var hour = deliveryValidTo.Hour == 0 ? 24 : deliveryValidTo.Hour;
                var paramCount = item.Elements("character")
                    .Count(item1 => item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
                                    item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
                                    "дополнительная информация");

                var description = paramCount == 0 ? null : item.Elements("character")
                    .Where(item1 => item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
                           item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "дополнительная информация")
                    .Select(item1 => $"{item1.Attribute("name").Value.Replace(":", "").Trim()}: {item1.Attribute("value").Value} {item1.Attribute("unit").Value}, ".Replace(" ,", ","))
                    .Aggregate((s1, s2) => $"{s1} {s2}");
                description = Cut(description, cutLengthForYandex);
                var cpa = item.Attribute("cpa").Value;

                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                {
                    var objectID = int.Parse(attrObjectID.Value);
                    ids[objectID] = true;


                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XAttribute("type", "vendor.model"),
                        item.Attributes("bid").SingleOrDefault(),
                        item.Attributes("cbid").SingleOrDefault(),
                        new XAttribute("available", "true"),
                        new XElement("name", name),
                        new XElement("url", string.Format("{0}{1}",
                                GoodsHelper.GoodsUrl(siteUrl, productType, manufacturer, attrModel.Value,
                                objectID),
                            Settings.Default.yandexUtm.Replace("%OFFER_ID%", attrObjectID.Value))),
                        new XElement("price", attrPrice.Value),
                        new XElement("currencyId", "RUR"),
                        (from item1 in item.Elements("categoryId")
                         select new XElement("categoryId", item1.Value)).First(),
                        (from item1 in item.Elements("picture")
                         select
                             new XElement("picture", $"{siteUrl}/bin.aspx?ID={item1.Attributes("OID").Single().Value}"))
                            .Take(10),
                        new XElement("store", hasStore.ToString(CultureInfo.InvariantCulture).ToLower()),
                        new XElement("pickup", hasPickup.ToString(CultureInfo.InvariantCulture).ToLower()),
                        new XElement("delivery", "true"),
                        new XElement("typePrefix", productType),
                        new XElement("vendor", manufacturer),
                        new XElement("vendorCode", item.Attribute("vendorCode").Value),
                        new XElement("model", attrModel.Value),
                        description != null ? new XElement("description", description) : null,
                        item.Elements("goodsSalesNotes")
                            .Where(e => !string.IsNullOrWhiteSpace(e.Value))
                            .Select(e => new XElement("sales_notes", e.Value))
                            .Union(
                                item.Elements("salesNotes")
                                    .Where(e => !string.IsNullOrWhiteSpace(e.Value))
                                    .Select(e => new XElement("sales_notes", e.Value)))
                            .DefaultIfEmpty(string.IsNullOrWhiteSpace(Settings.Default.sales_notes)
                                ? null
                                : new XElement("sales_notes", Settings.Default.sales_notes)).FirstOrDefault(),
                        new XElement("delivery-options",
                                new XElement("option",
                                    new XAttribute("cost", deliveryPrice.ToString(nfi)),
                                    new XAttribute("days", item.Attribute("deliveryDays").Value),
                                    new XAttribute("order-before", hour))),
                        Settings.Default.manufacturer_warranty ? new XElement("manufacturer_warranty", "true") : null,
                        !string.IsNullOrEmpty(item.Attribute("ean")?.Value) ? new XElement("barcode", item.Attribute("ean").Value) : null,
                        from item1 in item.Elements("character")
                        where item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
                              item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
                              "дополнительная информация"
                        select new XElement("param",
                            new XAttribute("name", item1.Attribute("name").Value.Replace(":", "").Trim()),
                            (from item2 in item1.Attributes("unit")
                             where !string.IsNullOrWhiteSpace(item2.Value)
                             select new XAttribute("unit", item2.Value.Trim())).SingleOrDefault(),
                            item1.Attribute("value").Value),
                        new XElement("cpa", cpa),
						item.Element("weight") != null ? new XElement("weight", item.Element("weight").Value) : null,
                        item.Element("dimensions") != null ? new XElement("dimensions", item.Element("dimensions").Value) : null

					));
                }
            }

            var sqlPromo = @"
SELECT 
    p.*, o.objectID 
FROM 
    t_PromoCode p 
    inner join t_Object o on p.OID = o.OID 
WHERE p.isActive = 1

SELECT 
    pg.OID, o.objectID 
FROM 
    t_PromoCodeGoods pg 
    inner join t_PromoCode p on pg.OID = p.OID and p.isActive = 1 and pg.yaLoad = 1
    inner join t_Object o on pg.goodsOID = o.OID
    inner join t_Goods g on o.OID = g.OID
    inner join t_Company c on g.manufacturer = c.OID 
	inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
	inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
	left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
WHERE
	ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
	and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
	and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
";
            var dataSet = DBHelper.GetDataSetMTSql(sqlPromo, new Hashtable { { "shopType", shopType }, { "rootNode", rootNode } }, false, null);
            var promos = doc.Descendants("promos").Single();
            foreach (var dataRow in dataSet.Table.AsEnumerable().Where(r => dataSet.Tables[1].AsEnumerable().Any(row => row.Field<Guid>("OID") == r.Field<Guid>("OID"))).ToList())
            {
                promos.Add(new XElement("promo",
                        new XAttribute("id", dataRow.Field<int>("objectID")),
                        new XAttribute("type", "promo code"),
                        !string.IsNullOrEmpty(dataRow.Field<string>("description")) ? new XElement("description", dataRow.Field<string>("description")) : null,
                        !string.IsNullOrEmpty(dataRow.Field<string>("url")) ? new XElement("url", dataRow.Field<string>("url")) : null,
                        new XElement("promo-code", dataRow.Field<string>("promoCode")),
                        new XElement("discount",
                            new XAttribute("unit", "currency"),
                            new XAttribute("currency", "RUR"),
                            new XText(dataRow.Field<decimal>("discount").ToString("0"))),
                        new XElement("purchase",
                            dataSet.Tables[1].AsEnumerable().Where(r => r.Field<Guid>("OID") == dataRow.Field<Guid>("OID") && ids.ContainsKey(r.Field<int>("objectID")))
                                .Select(r1 => new XElement("product",
                                    new XAttribute("offer-id", r1.Field<int>("objectID"))))
                        )
                    )
                );
            }

            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

		private static void CreateSecondFile(string fileName, int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup, bool hasStore, bool useRisePrice)
		{
			var nfi = CultureInfo.InvariantCulture.NumberFormat;
			var catalogOID = Guid.Empty;
			var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
				XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
        <delivery-options>
            <option cost=""0.0000"" days=""1"" />
        </delivery-options>
        <offers />
        <promos />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
			var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";
			var prms = new Hashtable();
			prms["@rootNode"] = rootNode;
			var dr = DBHelper.GetData(sql, prms, false);
			try
			{
				var categories = doc.Descendants("categories").Single();
				var first = true;
				while (dr.Read())
				{
					var nodeID = (int)dr["nodeID"];
					var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
					var category = (string)dr["nodeName"];
					var cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
					if (parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
					categories.Add(cat);
					if (first)
					{
						catalogOID = (Guid)dr["tree"];
						first = false;
					}
				}
			}
			finally
			{
				dr.Close();
			}

			var offers = doc.Descendants("offers").Single();
			sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!bid] = gpr.bid,
				[offer!1!cbid] = gpr.cbid,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!vendorCode] = g.pseudoID,
				[offer!1!model] = ISNULL(g.model, ''),
                [offer!1!cpa] = gpr.cpa,
				[offer!1!ean] = g.ean,
				[offer!1!deliveryPrice!element] = gpr.deliveryPrice,
                [offer!1!deliveryRise!element] = gpr.deliveryRise,
				[offer!1!deliveryDays] = gs.deliveryDays,
                [offer!1!deliveryValidTo] = gs.deliveryValidTo,
				[offer!1!description!element] = gd.description,
				[offer!1!goodsSalesNotes!element] = g.salesNotes,
				[offer!1!weight!element] = g.weight,
				[offer!1!dimensions!element] = g.dimensions,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!deliveryPrice] = null,
				[salesNotes!5!brand] = null,
				[salesNotes!5!lft] = null,
				[salesNotes!5!lftInner] = null,
				[salesNotes!5!] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStockAdd = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStockAdd = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_ObjectImage oi on g.OID = oi.OID and oi.ordValue like 'для popup%'
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				ISNULL(gp.unit, ''),
				null,
				null,
				null,
				null,
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStockAdd = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
				n2.deliveryFirst,
				null,
				null,
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStockAdd = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join (
					SELECT tm.masterOID, t.priceLimit, mp.deliveryFirst, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_MasterPrices mp on tm.masterOID = mp.OID and mp.shopType = @shopType
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				5,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.manufacturerOID,
				n2.lft,
				n2.lftInner,
				n2.salesNotes
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStockAdd = 1 and gs.Zombi = 0 and gs.inMarket = 1
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				cross apply (
					SELECT TOP 100 tsn.manufacturerOID, tsn.salesNotes, nn1.lft, lftInner = n.lft
					FROM t_Nodes n
						inner join t_Nodes nn1 on n.tree = nn1.tree and nn1.lft BETWEEN n.lft and n.rgt and n.tree = @catalogOID
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on nn1.object = tm.OID and tm.masterOID = g.category
						inner join t_Theme t on t.OID = n.object And t.loadToYandex = 1
						inner join t_ThemeSalesNotes tsn on t.OID = tsn.OID and (tsn.manufacturerOID is null or tsn.manufacturerOID = g.manufacturer)
				) n2  
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			ORDER BY
				[offer!1!id], Tag, [picture!2!ordValue], [character!3!ordValue!hide], [salesNotes!5!brand] desc, [salesNotes!5!lft], [salesNotes!5!lftInner] desc
			FOR XML EXPLICIT";
			prms = new Hashtable();
			prms["shopType"] = shopType;
			prms["rootNode"] = rootNode;
			prms["catalogOID"] = catalogOID;
			var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
			var ids = new Dictionary<int, bool>();

			foreach (var item in listOffers.Descendants("offer"))
			{
				if (!item.Elements("categoryId").Any()) continue;
				var attrObjectID = item.Attribute("objectID");
				var attrPrice = item.Attribute("price");
				var attrVendor = item.Attribute("vendor");
				var attrModel = item.Attribute("model");
				var productType = item.Attribute("typePrefix").Value;
				var manufacturer = item.Attribute("vendor").Value;
				var deliveryPrice = decimal.Parse(item.Elements("deliveryPrice").Single().Value, nfi);
				var deliveryRise = decimal.Parse(item.Elements("deliveryRise").Single().Value, nfi);
				if (useRisePrice) deliveryPrice += deliveryRise;
				var deliveryValidTo = DateTime.Parse(item.Attribute("deliveryValidTo").Value);
				var hour = deliveryValidTo.Hour == 0 ? 24 : deliveryValidTo.Hour;
				var paramCount = item.Elements("character")
					.Count(item1 => item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
									item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
									"дополнительная информация");

				var description = paramCount == 0 ? null : item.Elements("character")
					.Where(item1 => item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
						   item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "дополнительная информация")
					.Select(item1 => $"{item1.Attribute("name").Value.Replace(":", "").Trim()}: {item1.Attribute("value").Value} {item1.Attribute("unit").Value}, ".Replace(" ,", ","))
					.Aggregate((s1, s2) => $"{s1} {s2}");
				description = Cut(description, cutLengthForYandex);
				var cpa = item.Attribute("cpa").Value;

				if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
				{
					var objectID = int.Parse(attrObjectID.Value);
					ids[objectID] = true;


					offers.Add(new XElement("offer",
						new XAttribute("id", attrObjectID.Value),
						new XAttribute("type", "vendor.model"),
						item.Attributes("bid").SingleOrDefault(),
						item.Attributes("cbid").SingleOrDefault(),
						new XAttribute("available", "true"),
						new XElement("url", string.Format("{0}{1}",
								GoodsHelper.GoodsUrl(siteUrl, productType, manufacturer, attrModel.Value,
								objectID),
							Settings.Default.yandexUtm.Replace("%OFFER_ID%", attrObjectID.Value))),
						new XElement("price", attrPrice.Value),
						new XElement("currencyId", "RUR"),
						(from item1 in item.Elements("categoryId")
						 select new XElement("categoryId", item1.Value)).First(),
						(from item1 in item.Elements("picture")
						 select
							 new XElement("picture", $"{siteUrl}/bin.aspx?ID={item1.Attributes("OID").Single().Value}"))
							.Take(10),
						new XElement("store", hasStore.ToString(CultureInfo.InvariantCulture).ToLower()),
						new XElement("pickup", hasPickup.ToString(CultureInfo.InvariantCulture).ToLower()),
						new XElement("delivery", "true"),
						new XElement("typePrefix", productType),
						new XElement("vendor", manufacturer),
						new XElement("vendorCode", item.Attribute("vendorCode").Value),
						new XElement("model", attrModel.Value),
						description != null ? new XElement("description", description) : null,
						item.Elements("goodsSalesNotes")
							.Where(e => !string.IsNullOrWhiteSpace(e.Value))
							.Select(e => new XElement("sales_notes", e.Value))
							.Union(
								item.Elements("salesNotes")
									.Where(e => !string.IsNullOrWhiteSpace(e.Value))
									.Select(e => new XElement("sales_notes", e.Value)))
							.DefaultIfEmpty(string.IsNullOrWhiteSpace(Settings.Default.sales_notes)
								? null
								: new XElement("sales_notes", Settings.Default.sales_notes)).FirstOrDefault(),
						new XElement("delivery-options",
								new XElement("option",
									new XAttribute("cost", deliveryPrice.ToString(nfi)),
									new XAttribute("days", item.Attribute("deliveryDays").Value),
									new XAttribute("order-before", hour))),
						Settings.Default.manufacturer_warranty ? new XElement("manufacturer_warranty", "true") : null,
						!string.IsNullOrEmpty(item.Attribute("ean")?.Value) ? new XElement("barcode", item.Attribute("ean").Value) : null,
						from item1 in item.Elements("character")
						where item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
							  item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
							  "дополнительная информация"
						select new XElement("param",
							new XAttribute("name", item1.Attribute("name").Value.Replace(":", "").Trim()),
							(from item2 in item1.Attributes("unit")
							 where !string.IsNullOrWhiteSpace(item2.Value)
							 select new XAttribute("unit", item2.Value.Trim())).SingleOrDefault(),
							item1.Attribute("value").Value),
						new XElement("cpa", cpa),
						item.Element("weight") != null ? new XElement("weight", item.Element("weight").Value) : null,
						item.Element("dimensions") != null ? new XElement("dimensions", item.Element("dimensions").Value) : null

					));
				}
			}

			var sqlPromo = @"
SELECT 
    p.*, o.objectID 
FROM 
    t_PromoCode p 
    inner join t_Object o on p.OID = o.OID 
WHERE p.isActive = 1

SELECT 
    pg.OID, o.objectID 
FROM 
    t_PromoCodeGoods pg 
    inner join t_PromoCode p on pg.OID = p.OID and p.isActive = 1 and pg.yaLoad = 1
    inner join t_Object o on pg.goodsOID = o.OID
    inner join t_Goods g on o.OID = g.OID
    inner join t_Company c on g.manufacturer = c.OID 
	inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0 and gs.inMarket = 1
	inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
	left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
WHERE
	ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
	and EXISTS (SELECT * FROM t_ObjectImage WHERE OID = g.OID)
	and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
";
			var dataSet = DBHelper.GetDataSetMTSql(sqlPromo, new Hashtable { { "shopType", shopType }, { "rootNode", rootNode } }, false, null);
			var promos = doc.Descendants("promos").Single();
			foreach (var dataRow in dataSet.Table.AsEnumerable().Where(r => dataSet.Tables[1].AsEnumerable().Any(row => row.Field<Guid>("OID") == r.Field<Guid>("OID"))).ToList())
			{
				promos.Add(new XElement("promo",
						new XAttribute("id", dataRow.Field<int>("objectID")),
						new XAttribute("type", "promo code"),
						!string.IsNullOrEmpty(dataRow.Field<string>("description")) ? new XElement("description", dataRow.Field<string>("description")) : null,
						!string.IsNullOrEmpty(dataRow.Field<string>("url")) ? new XElement("url", dataRow.Field<string>("url")) : null,
						new XElement("promo-code", dataRow.Field<string>("promoCode")),
						new XElement("discount",
							new XAttribute("unit", "currency"),
							new XAttribute("currency", "RUR"),
							new XText(dataRow.Field<decimal>("discount").ToString("0"))),
						new XElement("purchase",
							dataSet.Tables[1].AsEnumerable().Where(r => r.Field<Guid>("OID") == dataRow.Field<Guid>("OID") && ids.ContainsKey(r.Field<int>("objectID")))
								.Select(r1 => new XElement("product",
									new XAttribute("offer-id", r1.Field<int>("objectID"))))
						)
					)
				);
			}

			using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
			{
				wrt.Formatting = Formatting.Indented;
				wrt.IndentChar = '\t';
				wrt.Indentation = 2;
				doc.Save(wrt);
			}
		}
		public static string Cut(string item, int maxLength, string suffix = " ...")
        {
            var pattern = ",.: \t\r\n";
            if (string.IsNullOrWhiteSpace(item)) return "";
            var i = 0;
            var j = 0;
            for (var k = 0; k < item.Length && i < maxLength; k++)
            {
                if (pattern.IndexOf(item[j]) == -1)
                {
                    i++;
                }
                j++;
            }

            var result = item.Substring(0, j);
            if (j == item.Length)
            {
                return item;
            }

            if (pattern.IndexOf(item[j]) != -1)
            {
                return result + suffix;
            }

            while (pattern.IndexOf(item[j]) == -1 && j >= 0)
            {
                j--;
            }

            while (pattern.IndexOf(item[j]) != -1 && j >= 0)
            {
                j--;
            }

            return item.Substring(0, j + 1) + suffix;
        }

        private static void CreateAdminFile(string fileName, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode)
        {
            var catalogOID = Guid.Empty;
            var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
                XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
		<offers />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";
            var prms = new Hashtable();
            prms["@rootNode"] = rootNode;
            var dr = DBHelper.GetData(sql, prms, false);
            try
            {
                var categories = doc.Descendants("categories").Single();
                var first = true;
                while (dr.Read())
                {
                    var nodeID = (int)dr["nodeID"];
                    var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
                    var category = (string)dr["nodeName"];
                    var cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
                    if (parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
                    categories.Add(cat);
                    if (first)
                    {
                        catalogOID = (Guid)dr["tree"];
                        first = false;
                    }
                }
            }
            finally
            {
                dr.Close();
            }
            var offers = doc.Descendants("offers").Single();


            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.postavshik_id, 
				[offer!1!objectID] = o.objectID,
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[categoryId!2!] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				2,
				1,
				g.postavshik_id,
				null,
				null,
				null,
				n2.nodeID
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join (
					SELECT tm.masterOID, t.priceLimit, mp.deliveryFirst, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_MasterPrices mp on tm.masterOID = mp.OID and mp.shopType = @shopType
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			ORDER BY
				[offer!1!id], Tag
			FOR XML EXPLICIT";
            prms = new Hashtable();
            prms["@shopType"] = shopType;
            prms["@rootNode"] = rootNode;
            prms["@catalogOID"] = catalogOID;
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var attrObjectID = item.Attribute("objectID");
                var attrID = item.Attribute("id");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");

                if (attrObjectID != null && attrVendor != null && attrModel != null)
                {
                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XAttribute("modelId", attrID.Value),
                        new XElement("vendor", attrVendor.Value),
                        new XElement("model", item.Attribute("model").Value)
                        ));
                }
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

        private static void CreateWikimartFile(string fileName, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode, bool hasPickup)
        {
            var catalogOID = Guid.Empty;
            var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
                XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
		<offers />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";
            var prms = new Hashtable();
            prms["@rootNode"] = rootNode;
            var dr = DBHelper.GetData(sql, prms, false);
            try
            {
                var categories = doc.Descendants("categories").Single();
                var first = true;
                while (dr.Read())
                {
                    var nodeID = (int)dr["nodeID"];
                    var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
                    var category = (string)dr["nodeName"];
                    var cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
                    if (parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
                    categories.Add(cat);
                    if (first)
                    {
                        catalogOID = (Guid)dr["tree"];
                        first = false;
                    }
                }
            }
            finally
            {
                dr.Close();
            }
            var offers = doc.Descendants("offers").Single();


            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!oldprice] = ROUND(gpr.price * 1.09, 0),
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[offer!1!deliveryPrice!element] = 0,
				[offer!1!description!element] = gd.description,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!name] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_ObjectImage oi on g.OID = oi.OID
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				ISNULL(gp.unit, ''),
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
				n2.nodeName
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join (
					SELECT tm.masterOID, t.priceLimit, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			ORDER BY
				[offer!1!id], Tag, [character!3!ordValue!hide]
			FOR XML EXPLICIT";
            prms = new Hashtable();
            prms["@shopType"] = shopType;
            prms["@rootNode"] = rootNode;
            prms["@catalogOID"] = catalogOID;
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                //XAttribute attrOldPrice = item.Attribute("oldprice");
                var attrTypePrefix = item.Attribute("typePrefix");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");
                var description = item.Element("description");

                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XAttribute("type", "vendor.model"),
                        new XAttribute("available", "true"),
                        new XElement("url", GoodsHelper.GoodsUrl(
                            siteUrl,
                            attrTypePrefix.Value,
                            attrVendor.Value,
                            attrModel.Value,
                            int.Parse(attrObjectID.Value))),
                        new XElement("price",
                            decimal.Parse(attrPrice.Value, CultureInfo.InvariantCulture.NumberFormat).ToString("0.00")),
                        //new XElement("oldprice",
                        //    decimal.Parse(attrOldPrice.Value, CultureInfo.InvariantCulture.NumberFormat)
                        //        .ToString("0.00")),
                        new XElement("currencyId", "RUR"),
                        (from item1 in item.Elements("categoryId")
                         select new XElement("categoryId", item1.Value)).First(),
                        (from item1 in item.Elements("picture")
                         select
                             new XElement("picture",
                                 string.Format("{0}/bin.aspx?ID={1}", siteUrl, item1.Attributes("OID").Single().Value)))
                            .FirstOrDefault(),
                        new XElement("store", "false"),
                        new XElement("pickup", hasPickup.ToString(CultureInfo.InvariantCulture).ToLower()),
                        new XElement("delivery", "true"),
                        item.Elements("deliveryPrice").Select(e => new XElement("local_delivery_cost", e.Value)).First(),
                        (from item1 in item.Attributes("typePrefix")
                         select new XElement("typePrefix", item1.Value)).SingleOrDefault(),
                        new XElement("vendor", attrVendor.Value),
                        new XElement("model", attrModel.Value),
                        description != null && !string.IsNullOrWhiteSpace(description.Value)
                            ? new XElement("description", description.Value)
                            : null,
                        string.IsNullOrWhiteSpace(Settings.Default.sales_notes)
                            ? null
                            : new XElement("sales_notes", Settings.Default.sales_notes),
                        Settings.Default.manufacturer_warranty ? new XElement("manufacturer_warranty", "true") : null,
                        from item1 in item.Elements("character")
                        where item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
                              item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
                              "дополнительная информация"
                        select new XElement("param",
                            new XAttribute("name", item1.Attribute("name").Value.Replace(":", "").Trim()),
                            (from item2 in item1.Attributes("unit")
                             where !string.IsNullOrWhiteSpace(item2.Value)
                             select new XAttribute("unit", item2.Value.Trim())).SingleOrDefault(),
                            item1.Attribute("value").Value)
                        ));
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

        private static void CreateWikimartAltFile(string fileName, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode, bool hasPickup, int companyId, decimal minusPercent)
        {
            var catalogOID = Guid.Empty;
            var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
                XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
		<offers />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";
            var prms = new Hashtable();
            prms["@rootNode"] = rootNode;
            var dr = DBHelper.GetData(sql, prms, false);
            try
            {
                var categories = doc.Descendants("categories").Single();
                var first = true;
                while (dr.Read())
                {
                    var nodeID = (int)dr["nodeID"];
                    var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
                    var category = (string)dr["nodeName"];
                    var cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
                    if (parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
                    categories.Add(cat);
                    if (first)
                    {
                        catalogOID = (Guid)dr["tree"];
                        first = false;
                    }
                }
            }
            finally
            {
                dr.Close();
            }
            var offers = doc.Descendants("offers").Single();


            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = case
					when c.ID = @companyId THEN ROUND((gpr.price * (1 - @minusPercent)), 0)
					else gpr.price
				end,
				[offer!1!oldprice] = case
					when c.ID = @companyId THEN ROUND((gpr.price * (1 - @minusPercent)) * (1 + 0.1 + @minusPercent), 0)
					else ROUND(gpr.price * 1.09, 0)
				end,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[offer!1!deliveryPrice!element] = 0,
				[offer!1!description!element] = gd.description,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!name] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_ObjectImage oi on g.OID = oi.OID
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				ISNULL(gp.unit, ''),
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
				n2.nodeName
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join (
					SELECT tm.masterOID, t.priceLimit, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			ORDER BY
				[offer!1!id], Tag, [character!3!ordValue!hide]
			FOR XML EXPLICIT";
            prms = new Hashtable();
            prms["shopType"] = shopType;
            prms["rootNode"] = rootNode;
            prms["catalogOID"] = catalogOID;
            prms["companyId"] = companyId;
            prms["minusPercent"] = minusPercent;
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                //XAttribute attrOldPrice = item.Attribute("oldprice");
                var attrTypePrefix = item.Attribute("typePrefix");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");
                var description = item.Element("description");

                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XAttribute("type", "vendor.model"),
                        new XAttribute("available", "true"),
                        new XElement("url", GoodsHelper.GoodsUrl(
                            siteUrl,
                            attrTypePrefix.Value,
                            attrVendor.Value,
                            attrModel.Value,
                            int.Parse(attrObjectID.Value))),
                        new XElement("price",
                            decimal.Parse(attrPrice.Value, CultureInfo.InvariantCulture.NumberFormat).ToString("0.00")),
                        //new XElement("oldprice",
                        //    decimal.Parse(attrOldPrice.Value, CultureInfo.InvariantCulture.NumberFormat)
                        //        .ToString("0.00")),
                        new XElement("currencyId", "RUR"),
                        (from item1 in item.Elements("categoryId")
                         select new XElement("categoryId", item1.Value)).First(),
                        (from item1 in item.Elements("picture")
                         select
                             new XElement("picture",
                                 string.Format("{0}/bin.aspx?ID={1}", siteUrl, item1.Attributes("OID").Single().Value)))
                            .FirstOrDefault(),
                        new XElement("store", "false"),
                        new XElement("pickup", hasPickup.ToString(CultureInfo.InvariantCulture).ToLower()),
                        new XElement("delivery", "true"),
                        item.Elements("deliveryPrice").Select(e => new XElement("local_delivery_cost", e.Value)).First(),
                        (from item1 in item.Attributes("typePrefix")
                         select new XElement("typePrefix", item1.Value)).SingleOrDefault(),
                        new XElement("vendor", attrVendor.Value),
                        new XElement("model", attrModel.Value),
                        description != null && !string.IsNullOrWhiteSpace(description.Value)
                            ? new XElement("description", description.Value)
                            : null,
                        string.IsNullOrWhiteSpace(Settings.Default.sales_notes)
                            ? null
                            : new XElement("sales_notes", Settings.Default.sales_notes),
                        Settings.Default.manufacturer_warranty ? new XElement("manufacturer_warranty", "true") : null,
                        from item1 in item.Elements("character")
                        where item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
                              item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
                              "дополнительная информация"
                        select new XElement("param",
                            new XAttribute("name", item1.Attribute("name").Value.Replace(":", "").Trim()),
                            (from item2 in item1.Attributes("unit")
                             where !string.IsNullOrWhiteSpace(item2.Value)
                             select new XAttribute("unit", item2.Value.Trim())).SingleOrDefault(),
                            item1.Attribute("value").Value)
                        ));
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

        private static void CreateNadaviFile(string fileName, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode)
        {
            var catalogOID = Guid.Empty;
            var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
                XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
			<currency id=""USD"" rate=""32""/>
		</currencies>
		<categories />
		<items />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
            var sql = @"
SELECT *
FROM t_Nodes
WHERE nodeID in (
	SELECT n.nodeID
	FROM t_Nodes n
		inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	WHERE n.rgt = n.lft + 1
	UNION ALL
	SELECT n.parentNode
	FROM t_Nodes n
		inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	WHERE n.rgt = n.lft + 1
)
ORDER BY lft
";
            var prms = new Hashtable();
            prms["rootNode"] = rootNode;
            var first = true;
            var dr = DBHelper.GetData(sql, prms, false);
            try
            {
                var categories = doc.Descendants("categories").Single();
                while (dr.Read())
                {
                    var nodeID = (int)dr["nodeID"];
                    if (nodeID == rootNode) continue;
                    var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
                    var lft = (int)dr["lft"];
                    var rgt = (int)dr["rgt"];
                    var category = (string)dr["nodeName"];
                    var cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
                    if (rgt == lft + 1 && parentNode != rootNode) cat.Add(new XAttribute("parentId", parentNode));
                    categories.Add(cat);
                    if (!first) continue;
                    catalogOID = (Guid)dr["tree"];
                    first = false;
                }
            }
            finally
            {
                dr.Close();
            }
            var offers = doc.Descendants("items").Single();

            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[categoryId!3!] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null
			FROM
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_ObjectImage oi on g.OID = oi.OID
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join (
					SELECT tm.masterOID, t.priceLimit, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			ORDER BY
				[offer!1!id], Tag
			FOR XML EXPLICIT";
            prms = new Hashtable();
            prms["@shopType"] = shopType;
            prms["@rootNode"] = rootNode;
            prms["@catalogOID"] = catalogOID;
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            foreach (var item in listOffers.Descendants("offer"))
            {
                try
                {
                    if (!item.Elements("categoryId").Any()) continue;
                    var attrObjectID = item.Attribute("objectID");
                    var attrPrice = item.Attribute("price");
                    var attrTypePrefix = item.Attribute("typePrefix");
                    var attrVendor = item.Attribute("vendor");
                    var attrModel = item.Attribute("model");

                    if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                        offers.Add(new XElement("item",
                            new XAttribute("id", attrObjectID.Value),
                            new XElement("name",
                                string.Format("{0} {1} {2}", attrTypePrefix.Value, attrVendor.Value, attrModel.Value)),
                            new XElement("url", GoodsHelper.GoodsUrl(
                                siteUrl,
                                attrTypePrefix.Value,
                                attrVendor.Value,
                                attrModel.Value,
                                int.Parse(attrObjectID.Value))),
                            new XElement("price", attrPrice.Value),
                            (from item1 in item.Elements("categoryId")
                             select new XElement("categoryId", item1.Value)).First(),
                            new XElement("vendor", attrVendor.Value),
                            (from item1 in item.Elements("picture")
                             select
                                 new XElement("image",
                                     string.Format("{0}/bin.aspx?ID={1}", siteUrl,
                                         item1.Attributes("OID").Single().Value))).FirstOrDefault()
                            ));
                }
                catch (Exception)
                {
                    log.Error("Error create file on item:\r\n{0}", item.InnerXml());
                    throw;
                }
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

        private static void CreateUtinetFile(string fileName, int shopType, string siteName, string siteTitle,
            string siteUrl, int rootNode, bool hasPickup)
        {
            var catalogOID = Guid.Empty;
            var doc = new XDocument(new XDocumentType("yml_catalog", null, "shops.dtd", null),
                XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<name>{1}</name>
		<company>{2}</company>
		<url>{3}</url>
		<currencies>
			<currency id=""RUR"" rate=""1""/>
		</currencies>
		<categories />
		<offers />
	</shop>
</yml_catalog>", DateTime.Now, siteName, siteTitle, siteUrl)));
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";
            var prms = new Hashtable();
            prms["@rootNode"] = rootNode;
            var dr = DBHelper.GetData(sql, prms, false);
            try
            {
                var categories = doc.Descendants("categories").Single();
                var first = true;
                var cat = new XElement("category", new XAttribute("id", 1), new XText("Корневой каталог"));
                categories.Add(cat);
                while (dr.Read())
                {
                    var nodeID = (int)dr["nodeID"];
                    var parentNode = dr["parentNode"] != DBNull.Value ? (int)dr["parentNode"] : 0;
                    var category = (string)dr["nodeName"];
                    cat = new XElement("category", new XAttribute("id", nodeID), new XText(category));
                    cat.Add(parentNode != rootNode
                        ? new XAttribute("parentId", parentNode)
                        : new XAttribute("parentId", 1));
                    categories.Add(cat);
                    if (first)
                    {
                        catalogOID = (Guid)dr["tree"];
                        first = false;
                    }
                }
            }
            finally
            {
                dr.Close();
            }
            var offers = doc.Descendants("offers").Single();


            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!bid] = gpr.bid,
				[offer!1!cbid] = gpr.cbid,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[offer!1!deliveryPrice!element] = gpr.deliveryPrice,
				[offer!1!description!element] = gd.description,
				[offer!1!goodsSalesNotes!element] = g.salesNotes,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!deliveryPrice] = null,
				[salesNotes!5!brand] = null,
				[salesNotes!5!lft] = null,
				[salesNotes!5!lftInner] = null,
				[salesNotes!5!] = null
			FROM
				t_Goods g
				inner join t_Object o on g.OID = o.OID
				inner join t_Company c on g.manufacturer = c.OID 
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType --and gpr.isMoic = 0	
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType --and gpr.isMoic = 0
				inner join t_ObjectImage oi on g.OID = oi.OID
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				ISNULL(gp.unit, ''),
				null,
				null,
				null,
				null,
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType --and gpr.isMoic = 0
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
				n2.deliveryFirst,
				null,
				null,
				null,
				null
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType --and gpr.isMoic = 0
				inner join (
					SELECT tm.masterOID, t.priceLimit, mp.deliveryFirst, n.*
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_MasterPrices mp on tm.masterOID = mp.OID and mp.shopType = @shopType
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category and n2.priceLimit <= gpr.price
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
			UNION ALL
			SELECT
				5,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.manufacturerOID,
				n2.lft,
				n2.lftInner,
				n2.salesNotes
			FROM	
				t_Goods g
				inner join t_company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType --and gpr.isMoic = 0
				cross apply (
					SELECT TOP 100 tsn.manufacturerOID, tsn.salesNotes, nn1.lft, lftInner = n.lft
					FROM t_Nodes n
						inner join t_Nodes nn1 on n.tree = nn1.tree and nn1.lft BETWEEN n.lft and n.rgt and n.tree = @catalogOID
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on nn1.object = tm.OID and tm.masterOID = g.category
						inner join t_Theme t on t.OID = n.object And t.loadToYandex = 1
						inner join t_ThemeSalesNotes tsn on t.OID = tsn.OID and (tsn.manufacturerOID is null or tsn.manufacturerOID = g.manufacturer)
					--ORDER BY
						--tsn.manufacturerOID, nn1.lft, n.lft desc
				) n2  
			WHERE
				ISNULL(c.companyName, '') <> '' and ISNULL(g.model, '') <> '' And gpr.price > 0
				and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
				and g.category in (
					SELECT tm.masterOID
					FROM t_Nodes n
						inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
						inner join t_ThemeMasters tm on n.object = tm.OID
						inner join t_Theme t on t.OID = tm.OID And t.loadToYandex = 1 and t.priceLimit <= gpr.price 
				) 
			ORDER BY
				[offer!1!id], Tag, [character!3!ordValue!hide], [salesNotes!5!brand] desc, [salesNotes!5!lft], [salesNotes!5!lftInner] desc
			FOR XML EXPLICIT";
            prms = new Hashtable();
            prms["@shopType"] = shopType;
            prms["@rootNode"] = rootNode;
            prms["@catalogOID"] = catalogOID;
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                var attrTypePrefix = item.Attribute("typePrefix");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");
                //var description = item.Element("description");

                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XAttribute("type", "vendor.model"),
                        item.Attributes("bid").SingleOrDefault(),
                        item.Attributes("cbid").SingleOrDefault(),
                        new XAttribute("available", "true"),
                        new XElement("url", GoodsHelper.GoodsUrl(
                            siteUrl,
                            attrTypePrefix.Value,
                            attrVendor.Value,
                            attrModel.Value,
                            int.Parse(attrObjectID.Value))),
                        new XElement("price", attrPrice.Value),
                        new XElement("currencyId", "RUR"),
                        (from item1 in item.Elements("categoryId")
                         select new XElement("categoryId", item1.Value)).First(),
                        (from item1 in item.Elements("picture")
                         select
                             new XElement("picture",
                                 string.Format("{0}/bin.aspx?ID={1}", siteUrl, item1.Attributes("OID").Single().Value)))
                            .FirstOrDefault(),
                        new XElement("store", "false"),
                        new XElement("pickup", hasPickup.ToString(CultureInfo.InvariantCulture).ToLower()),
                        new XElement("delivery", "true"),
                        item.Elements("deliveryPrice").Select(e => new XElement("local_delivery_cost", e.Value)).First(),
                        (from item1 in item.Attributes("typePrefix")
                         select new XElement("typePrefix", item1.Value)).SingleOrDefault(),
                        new XElement("vendor", attrVendor.Value),
                        new XElement("model", attrModel.Value),
                        //(description != null && !string.IsNullOrWhiteSpace(description.Value)
                        //    ?
                        //        new XElement("description", description.Value)
                        //    :
                        //        null),
                        item.Elements("goodsSalesNotes")
                            .Where(e => !string.IsNullOrWhiteSpace(e.Value))
                            .Select(e => new XElement("sales_notes", e.Value))
                            .Union(
                                item.Elements("salesNotes")
                                    .Where(e => !string.IsNullOrWhiteSpace(e.Value))
                                    .Select(e => new XElement("sales_notes", e.Value)))
                            .DefaultIfEmpty(string.IsNullOrWhiteSpace(Settings.Default.sales_notes)
                                ? null
                                : new XElement("sales_notes", Settings.Default.sales_notes)).FirstOrDefault(),
                        Settings.Default.manufacturer_warranty ? new XElement("manufacturer_warranty", "true") : null,
                        from item1 in item.Elements("character")
                        where item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() != "описание" &&
                              item1.Attribute("name").Value.Replace(":", "").Trim().ToLower() !=
                              "дополнительная информация"
                        select new XElement("param",
                            new XAttribute("name", item1.Attribute("name").Value.Replace(":", "").Trim()),
                            (from item2 in item1.Attributes("unit")
                             where !string.IsNullOrWhiteSpace(item2.Value)
                             select new XAttribute("unit", item2.Value.Trim())).SingleOrDefault(),
                            item1.Attribute("value").Value)
                        ));
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

        private void CreateTorgMailFile(string fileName, Guid OID, string siteName, string siteUrl, string logoUrl,
            CommodityGroupOption option)
        {
            var shopType = _commodityGroupRepository.GetCommodityGroupShop(OID);
            _commodityGroupRepository.GenerateCommodityGroupContent(OID, true, true, true);
            if (string.IsNullOrWhiteSpace(Path.GetFileName(fileName)))
            {
                DBHelper.ExecuteCommand("UPDATE t_CommodityGroup SET lastGenerate = GetDate() WHERE OID = @OID", new Hashtable { { "OID", OID } }, false);
                return;
            }
            var doc = new XDocument(XElement.Parse(string.Format(@"
<yml_catalog date=""{0:yyyy-MM-dd HH:mm}"">
	<shop>
		<shopname>{1}</shopname>
		<logo>{2}{3}</logo>
		<offers />
	</shop>
</yml_catalog>
", DateTime.Now, siteName, siteUrl, logoUrl)));
            var offers = doc.Descendants("offers").Single();


            string sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID, 
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[picture!2!OID] = null,
				[picture!2!ordValue] = null
			FROM
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_Object o on g.OID = o.OID
				inner join t_GoodsPrices gpr on g.OID = gpr.OID and gpr.shopType = @shopType
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				inner join t_Company c on c.OID = g.manufacturer
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue
			FROM
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1 and gs.Zombi = 0
				cross apply (SELECT TOP 1 binData, ordValue FROM t_ObjectImage 
					WHERE OID = g.OID And ordValue like '%IMAGETYPE%' ORDER BY ordValue) oi
			ORDER BY
				[offer!1!id], Tag
			FOR XML EXPLICIT";

            string imageType = option.UseLargeImage ? "для popup%" : "для карточки%";
            sql = sql.Replace("%IMAGETYPE%", imageType);

            var prms = new Hashtable();
            prms["@OID"] = OID;
            prms["@shopType"] = shopType;

            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            //?utm_source=avatori.ru&utm_medium=PriceAggregator&utm_term=product-name&utm_campaign=product-category
            foreach (var item in listOffers.Descendants("offer"))
            {
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                var attrTypePrefix = item.Attribute("typePrefix");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");

                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                {
                    var price = decimal.Parse(attrPrice.Value, CultureInfo.InvariantCulture.NumberFormat);
                    var oldPrice = price * 1.10M;
                    offers.Add(new XElement("offer",
                        new XAttribute("id", attrObjectID.Value),
                        new XElement("name",
                            string.Format("{0} {1} {2}", attrTypePrefix.Value, attrVendor.Value, attrModel.Value)),
                        new XElement("url", string.Format("{0}{1}",
                            GoodsHelper.GoodsUrl(
                                siteUrl,
                                attrTypePrefix.Value,
                                attrVendor.Value,
                                attrModel.Value,
                                int.Parse(attrObjectID.Value)),
                            string.IsNullOrWhiteSpace(option.GlobalUtmLabel) ? "" : option.GlobalUtmLabel
                            )),
                        new XElement("price", price.ToString("0")),
                        new XElement("oldprice", oldPrice.ToString("0")),
                        (from item1 in item.Elements("picture")
                         select
                             new XElement("picture",
                                 string.Format("{0}/bin.aspx?ID={1}", siteUrl, item1.Attributes("OID").Single().Value)))
                            .FirstOrDefault()
                        ));
                }
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.GetEncoding(1251)))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
                DBHelper.ExecuteCommand("UPDATE t_CommodityGroup SET lastGenerate = GetDate() WHERE OID = @OID",
                    new Hashtable { { "OID", OID } }, false);
            }
        }

        private void CreateMerchantFile(Guid OID, string fileName, int shopType, string siteName, string siteUrl,
            int rootNode)
        {
            var catalogOID = Guid.Empty;
            _commodityGroupRepository.GenerateCommodityGroupContent(OID, true, true, true);
            var doc =
                XDocument.Parse(
                    $@"<rss version=""2.0"" xmlns:g=""http://base.google.com/ns/1.0"">
	<channel>
		<title>{siteName}</title>
		<link>{siteUrl}</link>
	</channel>
</rss>");
            var sql = @"
SELECT n.nodeName, n.nodeID, n.parentNode, n.tree
FROM t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft And n.lft < n1.rgt And n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and (n.rgt - n.lft > 1 or t.loadToYandex = 1)
ORDER BY n.lft";

            var first = true;
            var categoryDictionary = new Dictionary<int, string>();
            sql.ExecSql(new { rootNode }).AsEnumerable()
                .ToList()
                .ForEach(row =>
                {
                    var nodeID = row.Field<int>("nodeID");
                    var parentNode = row.Field<int>("parentNode");
                    var prevCategoryName = categoryDictionary.ContainsKey(parentNode)
                        ? $"{categoryDictionary[parentNode]} > "
                        : "";
                    var category = $"{prevCategoryName}{row.Field<string>("nodeName").Replace('>', ' ')}";
                    categoryDictionary[nodeID] = category;
                    if (first)
                    {
                        catalogOID = row.Field<Guid>("tree");
                        first = false;
                    }
                });

            var channel = doc.Descendants("channel").Single();


            sql = @"
			SELECT
				Tag = 1,
				parent = null,
				[offer!1!id] = g.OID,
				[offer!1!objectID] = o.objectID,
				[offer!1!price] = gpr.price,
				[offer!1!typePrefix] = ISNULL(g.productType, ''),
				[offer!1!vendor] = ISNULL(c.companyName, ''),
				[offer!1!model] = ISNULL(g.model, ''),
				[offer!1!available] = gs.inStock,
				[offer!1!deliveryPrice!element] = gpr.deliveryPrice,
				[offer!1!description!element] = gd.description,
				[picture!2!OID] = null,
				[picture!2!ordValue] = null,
				[character!3!name] = null,
				[character!3!ordValue!hide] = null,
				[character!3!value] = null,
				[character!3!unit] = null,
				[categoryId!4!] = null,
				[categoryId!4!productType] = null,
                [categoryId!4!productTypeId] = null
			FROM
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_Object o on g.OID = o.OID
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1
				inner join t_Company c on c.OID = g.manufacturer
				inner join t_Object o1 on c.OID = o1.OID
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			UNION ALL
			SELECT
				2,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				oi.binData,
				oi.ordValue,
				null,
				null,
				null,
				null,
				null,
				null,
				null
			FROM
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				cross apply (SELECT TOP 1 binData, ordValue FROM t_ObjectImage 
					WHERE OID = g.OID And ordValue like 'для карточки%' ORDER BY ordValue) oi
				inner join t_Object o on g.OID = o.OID
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1
				inner join t_Company c on c.OID = g.manufacturer
				inner join t_Object o1 on c.OID = o1.OID
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			UNION ALL
			SELECT
				3,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ISNULL(pt.altName, pt.name),
				gp.ordValue,
				gp.value,
				ISNULL(gp.unit, ''),
				null,
				null,
				null
			FROM	
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join t_GoodsParams gp on g.OID = gp.OID And NOT gp.value is null And RTRIM(LTRIM(gp.value)) <> ''
				inner join t_ParamType pt on gp.paramTypeOID = pt.OID
                outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		            inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		            inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		            ORDER BY n.lft
	            ) category(OID)
                inner join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID and tp.isVisible = 1
				inner join t_Object o on g.OID = o.OID
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1
				inner join t_Company c on c.OID = g.manufacturer
				inner join t_Object o1 on c.OID = o1.OID
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			UNION ALL
			SELECT
				4,
				1,
				g.OID,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				n2.nodeID,
                n2.googleType,
                n2.googleId
			FROM	
				t_CommodityActualGoods cag
				inner join t_Goods g on cag.goodsOID = g.OID and cag.OID = @OID
				inner join (
					SELECT tm0.masterOID, n0.*, googleId = google.id, googleType = google.name
					FROM t_Nodes n0
						inner join t_Nodes n10 on n0.tree = n10.tree And n0.lft > n10.lft And n0.lft < n10.rgt And n10.nodeID = @rootNode
						inner join t_ThemeMasters tm0 on n0.object = tm0.OID
						inner join t_MasterPrices mp0 on tm0.masterOID = mp0.OID and mp0.shopType = @shopType
						inner join t_Theme t0 on t0.OID = tm0.OID
                        left join t_GoogleTaxonomy google on t0.google = google.OID
				) n2 on n2.tree = @catalogOID and n2.masterOID = g.category
				inner join t_Object o on g.OID = o.OID
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType and gs.inStock = 1
				inner join t_Company c on c.OID = g.manufacturer
				inner join t_Object o1 on c.OID = o1.OID
				left join t_GoodsDescriptions gd on g.OID = gd.OID And gd.shopType = @shopType	
			ORDER BY
				[offer!1!id], Tag, [character!3!ordValue!hide]
			FOR XML EXPLICIT";
            var prms = new Hashtable();
            prms["@OID"] = OID;
            prms["@shopType"] = shopType;
            prms["@rootNode"] = rootNode;
            prms["@catalogOID"] = catalogOID;
            var listOffers = DBHelper.GetXmlData(sql, prms, "root", false);
            var utmFormat = "?utm_source=merchantcenter&utm_medium=cpc&utm_campaign=23531_adhands&utm_content={0}";
            foreach (var item in listOffers.Descendants("offer"))
            {
                if (!item.Elements("categoryId").Any()) continue;
                var googleId = int.Parse(item.Elements("categoryId").First().Attribute("productTypeId")?.Value ?? "0");
                var googleTypeName = item.Elements("categoryId").First().Attribute("productType")?.Value;
                var attrObjectID = item.Attribute("objectID");
                var attrPrice = item.Attribute("price");
                var attrTypePrefix = item.Attribute("typePrefix");
                var attrVendor = item.Attribute("vendor");
                var attrModel = item.Attribute("model");
                var description = item.Element("description");
                var utm = string.Format(utmFormat, GoogleNormalize(attrModel.Value));

                XNamespace g = "http://base.google.com/ns/1.0";
                if (attrObjectID != null && attrPrice != null && attrVendor != null && attrModel != null)
                {
                    var price = decimal.Parse(attrPrice.Value, CultureInfo.InvariantCulture.NumberFormat);
                    try
                    {
                        var paramsDescr = item.Elements("character")
                            .Aggregate(new StringBuilder(),
                                (sb, i) =>
                                    sb.AppendFormat("{0}: {1} {2}\r\n",
                                        i.Attribute("name").Value,
                                        i.Attribute("value").Value,
                                        i.Attribute("unit").Value),
                                sb => sb.ToString());
                        channel
                            .Add(new XElement("item",
                                new XElement("title", $"{attrTypePrefix.Value} {attrVendor.Value} {attrModel.Value}"),
                                new XElement("link", GoodsHelper.GoodsUrl(
                                    siteUrl,
                                    attrTypePrefix.Value,
                                    attrVendor.Value,
                                    attrModel.Value,
                                    int.Parse(attrObjectID.Value)) + utm),
                                !string.IsNullOrWhiteSpace(description?.Value)
                                    ? new XElement("description", $"{description.Value}\r\n{paramsDescr}")
                                    : new XElement("description", paramsDescr),
                                googleId != 0 ? new XElement(g + "google_product_category", googleId) : null,
                                !string.IsNullOrEmpty(googleTypeName) ? new XElement(g + "product_type", googleTypeName) : null,
                                new XElement(g + "availability", "in stock"), new XElement(g + "id", attrObjectID.Value),
                                new XElement(g + "price",
                                    price.ToString("0.00 RUB", CultureInfo.InvariantCulture.NumberFormat)),
                                new XElement(g + "condition", "new"), new XElement(g + "brand", attrVendor.Value),
                                (from item1 in item.Elements("picture")
                                 select
                                     new XElement(g + "image_link",
                                         $"{siteUrl}/bin.aspx?ID={item1.Attributes("OID").Single().Value}"))
                                    .FirstOrDefault()));
                    }
                    catch (Exception e)
                    {
                        log.Error("Error: {0}\r\nItem: {1}\r\nStackTrace: {2}", e.Message, item, e.StackTrace);
                        throw;
                    }
                }
            }
            using (var wrt = new XmlTextWriter(fileName, Encoding.UTF8))
            {
                wrt.Formatting = Formatting.Indented;
                wrt.IndentChar = '\t';
                wrt.Indentation = 2;
                doc.Save(wrt);
            }
        }

        private static string GoogleNormalize(string attrModel)
        {
            return normGoogle.Replace(attrModel.TrimInner().Trim().Replace(' ', '_').TranslitEncode(true), "");
        }

        private static string FormatParam(string name, string unit, string value)
        {
            var sb = new StringBuilder();
            sb.Append(name.Replace(":", "").Trim());
            if (!string.IsNullOrWhiteSpace(unit)) sb.AppendFormat("({0})", unit.Trim());
            sb.AppendFormat(": {0}; ", value.Trim());
            return sb.ToString();
        }
    }
}