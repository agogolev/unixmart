using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core.Aspects;
using DBReader;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using MetaData;
using NoName.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class GoodsRepository : IGoodsRepository
    {
        //public ICommodityGroupRepository CommodityGroupRepository { get; set; }
        public static readonly Guid BlankImageLarge = new Guid("424177C1-378B-4526-B0F2-BCC4822ADB73");
        public static readonly Guid BlankImageMedium = new Guid("17666079-0237-4A85-B8C4-C84301E48BE5");
        public static readonly Guid BlankImageSmall = new Guid("F1CE8164-029A-4AD9-BE62-827FBA888A69");
        public const int BlankImageLargeId = 613217;
        public const int BlankImageMediumId = 613218;
        public const int BlankImageSmallId = 613219;
        public const int BlankImageLargeWidth = 500;
        public const int BlankImageMediumWidth = 240;
        public const int BlankImageSmallWidth = 115;
        public const int BlankImageLargeHeight = 500;
        public const int BlankImageMediumHeight = 240;
        public const int BlankImageSmallHeight = 115;
        public const string BlankImageLargeMimeType = "image/jpeg";
        public const string BlankImageMediumMimeType = "image/jpeg";
        public const string BlankImageSmallMimeType = "image/jpeg";

        [Cache]
        public IList<GoodsItem> GetAllGoodsByCategory(int shopType, int selectedNode)
        {
            const string sql = @"
SELECT DISTINCT
	g.OID
	, o.objectID
	, g.model
	, g.pseudoID
	, g.productType
    , g.manufacturer
    , c.companyName
	, image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, gp.price
    , gp.oldPrice
	, gp.deliveryPrice
    , gs.inStock
    , gs.stockAmount
    , gs.zombi
	, isHit = ISNULL(gh.isHit, 0)
	, hitWeight = ISNULL(gh.popularity, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, o.dateCreate
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.nodeId = @selectedNode
	inner join t_ThemeMasters tm on n1.object = tm.OID 
	inner join t_Goods g on tm.masterOID = g.category
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType --and (gs.inStock = 1 or gs.lastActiveDate > DateAdd(Month, -1, GetDate())) 
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	inner join t_Object o on g.OID = o.OID
	inner join t_Company c on g.manufacturer = c.OID
	outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
WHERE
	EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.nodeId = @selectedNode
	inner join t_ThemeMasters tm on n1.object = tm.OID 
	inner join t_Goods g on tm.masterOID = g.category
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType --and (gs.inStock = 1 or gs.lastActiveDate > DateAdd(Month, -1, GetDate()))
	inner join t_TemplateParams tp on tp.OID = n1.object And (tp.IsFilter = 1 or tp.isFullName = 1)
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID   
";
            var prms = new Hashtable { { "selectedNode", selectedNode }, { "shopType", shopType } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            var list = (from row in ds.Table.AsEnumerable()
                        select new GoodsItem
                        {
                            OID = row.Field<Guid>("OID"),
                            ObjectID = row.Field<int>("objectID"),
                            ProductType = row.Field<string>("productType"),
                            Model = row.Field<string>("model"),
                            PseudoID = row.Field<string>("pseudoID"),
                            Manufacturer = row.Field<Guid?>("manufacturer"),
                            ManufacturerName = row.Field<string>("companyName"),
                            ShortDescription = ds.Tables[1].AsEnumerable()
                                .Where(k => k.Field<Guid>("OID") == row.Field<Guid>("OID") && k.Field<Guid>("paramTypeOID") == Settings.Default.ShortDescriptionParam)
                                .Select(k => k.Field<string>("value"))
                                .DefaultIfEmpty("")
                                .Single().ToHtml(),
                            Image = new Image
                            {
                                Id = row.Field<int?>("imageID") ?? BlankImageMediumId,
                                OID = row.Field<Guid?>("image") ?? BlankImageMedium,
                                Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                                Width = row.Field<int?>("imageWidth") ?? BlankImageMediumWidth,
                                Height = row.Field<int?>("imageHeight") ?? BlankImageMediumHeight,
                                MimeType = row.Field<string>("mimeType") ?? BlankImageMediumMimeType
                            },
                            Price = row.Field<decimal>("price"),
                            OldPrice = row.Field<decimal>("oldPrice"),
                            InStock = row.Field<bool>("inStock"), //&& !row.Field<bool>("zombi"),
                            StockAmount = row.Field<int>("stockAmount"),
                            //Zombi = row.Field<bool>("zombi"),
                            IsHit = row.Field<bool>("isHit"),
                            HitWeight = row.Field<int>("hitWeight"),
                            IsNew = row.Field<bool>("isNew"),
                            DateCreate = row.Field<DateTime>("dateCreate"),
                            Delivery = new DeliveryInfo { Price = row.Field<decimal?>("deliveryPrice") ?? 0 },
                            GoodsParams = ds.Tables[1].AsEnumerable()
                                .Where(k => k.Field<Guid>("OID") == row.Field<Guid>("OID") && k.Field<Guid>("paramTypeOID") != Settings.Default.ShortDescriptionParam)
                                .OrderBy(k => k.Field<int>("ordValue"))
                                .Select(k => new ParamItem
                                {
                                    ParamType = k.Field<Guid>("paramTypeOID"),
                                    ParamName = k.Field<string>("name"),
                                    ParamValue = k.Field<string>("value"),
                                    IsFilter = k.Field<bool>("isFilter"),
                                    IsFullName = k.Field<bool>("isFullName"),
                                    Unit = k.Field<string>("unit")
                                }).ToList()
                        }).ToList();
            return list; //showAbsent ? list : list.Where(i => !i.Zombi).ToList();
        }

        [Cache]
        public IList<Tag> GetCategoryTags(int selectedNode)
        {
            const string sql = @"
SELECT
	tt.ordValue
	, l.name
	, o.shortLink  
FROM
    t_LandingPage l
    inner join t_Object o  on l.OID = o.OID
    inner join t_ThemeTags tt on tt.landingPageOID = l.OID
    inner join t_Nodes n on n.object = tt.OID and n.nodeID = @selectedNode
WHERE
    l.commodityGroup is null and l.actualGoodsCount > 0  
UNION  
SELECT DISTINCT      
	tt.ordValue      
	, l.name      
	, o.shortLink  
FROM      
	t_LandingPage l      
	inner join t_CommodityActualGoods cag on l.commodityGroup = cag.OID      
	inner join t_Object o  on l.OID = o.OID      
	inner join t_ThemeTags tt on tt.landingPageOID = l.OID      
	inner join t_Nodes n on n.object = tt.OID and n.nodeID = @selectedNode  
WHERE       
	l.actualGoodsCount > 0  
ORDER BY      
	tt.ordValue 
";
            return sql.ExecSql(new { selectedNode }).AsEnumerable()
                .Select(r => new Tag
                {
                    Name = r.Field<string>("name"),
                    ShortLink = r.Field<string>("shortLink")
                }).ToList();
        }

        [Cache]
        public IList<Tag> GetLandingPageTags(int objectID)
        {
            const string sql = @"
SELECT      
	tt.ordValue      
	, l.name      
	, o.shortLink 
FROM      
	t_LandingPage l      
	inner join t_Object o  on l.OID = o.OID      
	inner join t_ThemeTags tt on tt.landingPageOID = l.OID      
	inner join t_Theme t on t.OID = tt.OID      
	inner join t_ThemeTags tt1 on tt1.OID = t.OID      
	inner join t_Object o1 on tt1.landingPageOID = o1.OID and o1.objectID = @objectID  
WHERE      
	l.commodityGroup is null and l.actualGoodsCount > 0 and o.objectID <> @objectID  
UNION  
SELECT DISTINCT      
	tt.ordValue      
	, l.name      
	, o.shortLink  
FROM      
	t_LandingPage l      
	inner join t_CommodityActualGoods cag on l.commodityGroup = cag.OID      
	inner join t_Object o  on l.OID = o.OID      
	inner join t_ThemeTags tt on tt.landingPageOID = l.OID      
	inner join t_Theme t on t.OID = tt.OID      
	inner join t_ThemeTags tt1 on tt1.OID = t.OID       
	inner join t_Object o1 on tt1.landingPageOID = o1.OID and o1.objectID = @objectID  
WHERE       
	l.actualGoodsCount > 0 and o.objectID <> @objectID  
ORDER BY      
	tt.ordValue
";
            return sql.ExecSql(new { objectID }).AsEnumerable()
                .Select(r => new Tag
                {
                    OrdValue = r.Field<int>("ordValue"),
                    Name = r.Field<string>("name"),
                    ShortLink = r.Field<string>("shortLink")
                })
                .Distinct(new LambdaComparer<Tag>((t1, t2) => string.Compare(t1.ShortLink, t2.ShortLink, true) == 0))
                .OrderBy(t => t.OrdValue)
                .ToList();
        }


        private IList<GoodsItem> GetAllFilteredGoodsByCategory(
            int shopType,
            int selectedNode,
            IList<Guid> manufs,
            IList<CustomFilter> filters)
        {
            IList<GoodsItem> goods = GetAllGoodsByCategory(shopType, selectedNode);
            IEnumerable<GoodsItem> result = goods;

            if (manufs != null && manufs.Count > 0)
                result = result.Where(k => k.Manufacturer.HasValue && manufs.Contains(k.Manufacturer.Value)).ToList();

            if (filters != null && filters.Count != 0)
                result = result.Where(k => filters.Count(k1 => k1.HasGoods(k)) == filters.Count);

            return result.ToList();
        }

        public IList<GoodsItem> GetGoodsByCategory(
            int shopType,
            int selectedNode,
            int skip,
            int take,
            string sort,
            IList<Guid> manufs,
            IList<CustomFilter> filters,
            out int total)
        {
            var goods = GetAllFilteredGoodsByCategory(shopType, selectedNode, manufs, filters);
            total = goods.Count;

            return TrimResult(goods, skip, take, sort);
        }

        private static IList<GoodsItem> TrimResult(IEnumerable<GoodsItem> items, int skip, int take, string sort)
        {
            var result = items;
            switch (sort)
            {
                case null:
                case "pop": result = result.OrderByDescending(v => v.InStock).ThenByDescending(v => v.HitWeight).ThenByDescending(v => v.StockAmount).ThenBy(v => v.Price); break;
                case "up":
                    result = result.OrderByDescending(v => v.InStock).ThenBy(v => v.Price).ThenByDescending(v => v.StockAmount);
                    break;
                case "down": result = result.OrderByDescending(v => v.InStock).ThenByDescending(v => v.Price).ThenByDescending(v => v.StockAmount); break;
            }

            return take > 0 ? result.Skip(skip).Take(take).ToList() : result.ToList();
        }

        [Cache]
        public IList<GoodsItem> GetHits(int shopType, int rootNode)
        {
            const string sql = @"
Declare
	@tempTable table (OID uniqueidentifier) 
	insert into @tempTable
	SELECT 
        gh.OID
    FROM
        t_GoodsIsHits gh 
        inner join t_Goods g on gh.OID = g.OID
        inner join t_ThemeMasters tm on tm.masterOID = g.category
        inner join t_Nodes n on tm.OID = n.object and n.tree = @catalogTree
        inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
        inner join t_GoodsInStocks gs on gh.OID = gs.OID and gs.shopType = @shopType and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
        and gh.isHit = 1 and gh.shopType = @shopType

SELECT DISTINCT 
	g.OID
	, o.objectID
	, g.Model
	, g.productType
    , c.companyName
	, isHits = ISNULL(gh.isHit, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, isSale = ISNULL(gis.isSale, 0)
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
    , hitWeight = ISNULL(gh.popularity, 0)
    , gs.stockAmount
	, gp.price
    , gp.oldPrice
FROM
	t_Goods g
	inner join @tempTable hns on hns.OID = g.OID
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_Object o on g.OID = o.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
    inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	left join t_GoodsIsSales gis on g.OID = gis.OID and gis.shopType = @shopType
WHERE
	EXISTS(SELECT * FROM t_Nodes n inner join t_ThemeMasters tm on n.object = tm.OID inner join t_Goods g1 on tm.masterOID = g1.category and g1.OID = g.OID)

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.nodeId = @rootNode
	inner join t_ThemeMasters tm on n1.object = tm.OID 
	inner join t_Goods g on tm.masterOID = g.category
    inner join @tempTable hns on hns.OID = g.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_TemplateParams tp on tp.OID = n1.object And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID";
            var prms = new Hashtable { { "shopType", shopType }, { "rootNode", rootNode }, { "catalogTree", Settings.Default.CatalogTree } };
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            return (from row in ds.Table.AsEnumerable()
                    select new GoodsItem
                    {
                        OID = row.Field<Guid>("OID"),
                        ObjectID = row.Field<int>("objectID"),
                        ProductType = row.Field<string>("productType"),
                        Model = row.Field<string>("model"),
                        ManufacturerName = row.Field<string>("companyName"),
                        HitWeight = row.Field<int>("hitWeight") + (row.Field<bool>("isHits") ? 10 : 0),
                        IsHit = row.Field<bool>("isHits"),
                        IsNew = row.Field<bool>("isNew"),
                        IsSale = row.Field<bool>("isSale"),
                        Price = row.Field<decimal>("price"),
                        OldPrice = row.Field<decimal>("oldPrice"),
                        Image = new Image
                        {
                            Id = row.Field<int?>("imageID") ?? BlankImageSmallId,
                            OID = row.Field<Guid?>("image") ?? BlankImageSmall,
                            Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                            Width = row.Field<int?>("imageWidth") ?? BlankImageSmallWidth,
                            Height = row.Field<int?>("imageHeight") ?? BlankImageSmallHeight,
                            MimeType = row.Field<string>("mimeType") ?? BlankImageSmallMimeType
                        },
                        GoodsParams = ds.Tables[1].AsEnumerable()
                            .Where(r => r.Field<Guid>("OID") == row.Field<Guid>("OID"))
                            .OrderBy(r => r.Field<int>("ordValue"))
                            .Select(k => new ParamItem
                            {
                                ParamType = k.Field<Guid>("paramTypeOID"),
                                ParamName = k.Field<string>("name"),
                                ParamValue = k.Field<string>("value"),
                                IsFilter = k.Field<bool>("isFilter"),
                                IsFullName = k.Field<bool>("isFullName"),
                                Unit = k.Field<string>("unit")
                            }).ToList()
                    }).OrderByDescending(i => i.HitWeight).ThenByDescending(i => i.StockAmount).ThenByDescending(i => i.Price).ToList();
        }

        [Cache]
        public IList<GoodsItem> GetNew(int shopType, int rootNode)
        {
            const string sql = @"
Declare
	@tempTable table (OID uniqueidentifier) 
	insert into @tempTable
	SELECT
        gn.OID
    FROM
        t_GoodsIsNewes gn
        inner join t_Goods g on gn.OID = g.OID
        inner join t_ThemeMasters tm on tm.masterOID = g.category
        inner join t_Nodes n on tm.OID = n.object and n.tree = @catalogTree
        inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
        inner join t_GoodsInStocks gs on gn.OID = gs.OID and gs.shopType = @shopType and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
        and gn.isNew = 1 and gn.shopType = @shopType

SELECT DISTINCT 
	g.OID
	, o.objectID
	, g.Model
	, g.productType
    , c.companyName
	, isHits = ISNULL(gh.isHit, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, isSale = ISNULL(gis.isSale, 0)
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
    , hitWeight = ISNULL(gh.popularity, 0)
    , gs.stockAmount
	, gp.price
    , gp.oldPrice
FROM
	t_Goods g
	inner join @tempTable hns on hns.OID = g.OID
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_Object o on g.OID = o.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
    inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	left join t_GoodsIsSales gis on g.OID = gis.OID and gis.shopType = @shopType
WHERE
	EXISTS(SELECT * FROM t_Nodes n inner join t_ThemeMasters tm on n.object = tm.OID inner join t_Goods g1 on tm.masterOID = g1.category and g1.OID = g.OID)

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.nodeId = @rootNode
	inner join t_ThemeMasters tm on n1.object = tm.OID 
	inner join t_Goods g on tm.masterOID = g.category
    inner join @tempTable hns on hns.OID = g.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_TemplateParams tp on tp.OID = n1.object And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID";
            var prms = new Hashtable { { "shopType", shopType }, { "rootNode", rootNode }, { "catalogTree", Settings.Default.CatalogTree } };
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            return (from row in ds.Table.AsEnumerable()
                    select new GoodsItem
                    {
                        OID = row.Field<Guid>("OID"),
                        ObjectID = row.Field<int>("objectID"),
                        ProductType = row.Field<string>("productType"),
                        Model = row.Field<string>("model"),
                        ManufacturerName = row.Field<string>("companyName"),
                        HitWeight = row.Field<int>("hitWeight") + (row.Field<bool>("isHits") ? 10 : 0),
                        IsHit = row.Field<bool>("isHits"),
                        IsNew = row.Field<bool>("isNew"),
                        IsSale = row.Field<bool>("isSale"),
                        Price = row.Field<decimal>("price"),
                        OldPrice = row.Field<decimal>("oldPrice"),
                        Image = new Image
                        {
                            Id = row.Field<int?>("imageID") ?? BlankImageSmallId,
                            OID = row.Field<Guid?>("image") ?? BlankImageSmall,
                            Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                            Width = row.Field<int?>("imageWidth") ?? BlankImageSmallWidth,
                            Height = row.Field<int?>("imageHeight") ?? BlankImageSmallHeight,
                            MimeType = row.Field<string>("mimeType") ?? BlankImageSmallMimeType
                        },
                        GoodsParams = ds.Tables[1].AsEnumerable()
                            .Where(r => r.Field<Guid>("OID") == row.Field<Guid>("OID"))
                            .OrderBy(r => r.Field<int>("ordValue"))
                            .Select(k => new ParamItem
                            {
                                ParamType = k.Field<Guid>("paramTypeOID"),
                                ParamName = k.Field<string>("name"),
                                ParamValue = k.Field<string>("value"),
                                IsFilter = k.Field<bool>("isFilter"),
                                IsFullName = k.Field<bool>("isFullName"),
                                Unit = k.Field<string>("unit")
                            }).ToList()
                    }).OrderByDescending(i => i.HitWeight).ThenByDescending(i => i.StockAmount).ThenByDescending(i => i.Price).ToList();
        }

        [Cache]
        public IList<GoodsItem> GetSpecial(int shopType, int rootNode)
        {
            const string sql = @"
Declare
	@tempTable table (OID uniqueidentifier) 
	insert into @tempTable
	SELECT
        gis.OID
    FROM
        t_GoodsIsSales gis
        inner join t_Goods g on gis.OID = g.OID
        inner join t_ThemeMasters tm on tm.masterOID = g.category
        inner join t_Nodes n on tm.OID = n.object and n.tree = @catalogTree
        inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
        inner join t_GoodsInStocks gs on gis.OID = gs.OID and gs.shopType = @shopType and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
        and gis.isSale = 1 and gis.shopType = @shopType 

SELECT DISTINCT 
	g.OID
	, o.objectID
	, g.Model
	, g.productType
    , c.companyName
	, isHits = ISNULL(gh.isHit, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, isSale = ISNULL(gis.isSale, 0)
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
    , hitWeight = ISNULL(gh.popularity, 0)
    , gs.stockAmount
	, gp.price
    , gp.oldPrice
FROM
	t_Goods g
	inner join @tempTable hns on hns.OID = g.OID
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_Object o on g.OID = o.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
    inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	left join t_GoodsIsSales gis on g.OID = gis.OID and gis.shopType = @shopType
WHERE
	EXISTS(SELECT * FROM t_Nodes n inner join t_ThemeMasters tm on n.object = tm.OID inner join t_Goods g1 on tm.masterOID = g1.category and g1.OID = g.OID)

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt and n.nodeId = @rootNode
	inner join t_ThemeMasters tm on n1.object = tm.OID 
	inner join t_Goods g on tm.masterOID = g.category
    inner join @tempTable hns on hns.OID = g.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_TemplateParams tp on tp.OID = n1.object And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID";
            var prms = new Hashtable { { "shopType", shopType }, { "rootNode", rootNode }, { "catalogTree", Settings.Default.CatalogTree } };
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            return (from row in ds.Table.AsEnumerable()
                    select new GoodsItem
                    {
                        OID = row.Field<Guid>("OID"),
                        ObjectID = row.Field<int>("objectID"),
                        ProductType = row.Field<string>("productType"),
                        Model = row.Field<string>("model"),
                        ManufacturerName = row.Field<string>("companyName"),
                        HitWeight = row.Field<int>("hitWeight") + (row.Field<bool>("isHits") ? 10 : 0),
                        IsHit = row.Field<bool>("isHits"),
                        IsNew = row.Field<bool>("isNew"),
                        IsSale = row.Field<bool>("isSale"),
                        Price = row.Field<decimal>("price"),
                        OldPrice = row.Field<decimal>("oldPrice"),
                        Image = new Image
                        {
                            Id = row.Field<int?>("imageID") ?? BlankImageSmallId,
                            OID = row.Field<Guid?>("image") ?? BlankImageSmall,
                            Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                            Width = row.Field<int?>("imageWidth") ?? BlankImageSmallWidth,
                            Height = row.Field<int?>("imageHeight") ?? BlankImageSmallHeight,
                            MimeType = row.Field<string>("mimeType") ?? BlankImageSmallMimeType
                        },
                        GoodsParams = ds.Tables[1].AsEnumerable()
                            .Where(r => r.Field<Guid>("OID") == row.Field<Guid>("OID"))
                            .OrderBy(r => r.Field<int>("ordValue"))
                            .Select(k => new ParamItem
                            {
                                ParamType = k.Field<Guid>("paramTypeOID"),
                                ParamName = k.Field<string>("name"),
                                ParamValue = k.Field<string>("value"),
                                IsFilter = k.Field<bool>("isFilter"),
                                IsFullName = k.Field<bool>("isFullName"),
                                Unit = k.Field<string>("unit")
                            }).ToList()
                    }).OrderByDescending(i => i.HitWeight).ThenByDescending(i => i.StockAmount).ThenByDescending(i => i.Price).ToList();
        }

        public IList<GoodsItem> GetSputnikGoods(int shopType, int rootNode, int selectedNode, int objectID, decimal price, int maxValue)
        {
            IList<GoodsItem> goods = GetAllGoodsByCategory(shopType, selectedNode).Where(item => item.InStock).ToList();
            if (goods.Count == 0) return new List<GoodsItem>();
            if (objectID == 0)
            {
                var arbitaryCategoryGoods = new Dictionary<int, GoodsItem>();
                var rnd = new Random();
                var topLimit = goods.Count;
                do
                {
                    var goodsItem = goods[rnd.Next(topLimit)];
                    if (!arbitaryCategoryGoods.ContainsKey(goodsItem.ObjectID))
                    {
                        arbitaryCategoryGoods[goodsItem.ObjectID] = goodsItem;
                    }
                } while (arbitaryCategoryGoods.Count < Math.Min(maxValue, topLimit));
                return arbitaryCategoryGoods.Values.ToList();

            }
            GoodsItem currentGoods = (from item in goods where item.ObjectID == objectID select item).FirstOrDefault();
            IList<GoodsItem> topItems, bottomItems;
            if (currentGoods == null)
            {
                topItems = goods.Where(item => item.Price >= price).OrderBy(item => item.Price).Take(maxValue).ToList();
                bottomItems = goods.Where(item => item.Price < price).OrderByDescending(item => item.Price).Take(maxValue).ToList();
            }
            else
            {
                topItems = goods.Where(item => item.Price >= currentGoods.Price && item != currentGoods).OrderBy(item => item.Price).Take(maxValue).ToList();
                bottomItems = goods.Where(item => item.Price < currentGoods.Price).OrderByDescending(item => item.Price).Take(maxValue).ToList();
            }
            IEnumerable<GoodsItem> items = bottomItems.Union(topItems).OrderBy(item => item.Price);
            if (topItems.Count < 3) return items.Skip(items.Count() - maxValue).Take(maxValue).ToList();
            if (items.Count() > maxValue) return items.Skip(items.Count() - maxValue).Take(maxValue).ToList();
            return items.ToList();
        }

        public IList<GoodsItem> GetSimilarGoods(int shopType, int rootNode, int selectedNode, int objectID, decimal price, int minValue, int maxValue)
        {
            IList<GoodsItem> goods = GetHits(shopType, selectedNode);
            if (goods.Count < minValue) return new List<GoodsItem>();
            if (objectID == 0)
            {
                var arbitaryCategoryGoods = new Dictionary<int, GoodsItem>();
                var rnd = new Random();
                var topLimit = goods.Count;
                do
                {
                    var goodsItem = goods[rnd.Next(topLimit)];
                    if (!arbitaryCategoryGoods.ContainsKey(goodsItem.ObjectID))
                    {
                        arbitaryCategoryGoods[goodsItem.ObjectID] = goodsItem;
                    }
                } while (arbitaryCategoryGoods.Count < Math.Min(maxValue, topLimit));
                return arbitaryCategoryGoods.Values.ToList();

            }
            GoodsItem currentGoods = (from item in goods where item.ObjectID == objectID select item).FirstOrDefault();
            IList<GoodsItem> topItems, bottomItems;
            if (currentGoods == null)
            {
                topItems = goods.Where(item => item.InStock && item.Price >= price).OrderBy(item => item.Price).Take(maxValue).ToList();
                bottomItems = goods.Where(item => item.InStock && item.Price < price).OrderByDescending(item => item.Price).Take(maxValue).ToList();
            }
            else
            {
                topItems = goods.Where(item => item.InStock && item.Price >= currentGoods.Price && item != currentGoods).OrderBy(item => item.Price).Take(maxValue).ToList();
                bottomItems = goods.Where(item => item.InStock && item.Price < currentGoods.Price).OrderByDescending(item => item.Price).Take(maxValue).ToList();
            }
            IEnumerable<GoodsItem> items = bottomItems.Union(topItems).OrderBy(item => item.Price);
            if (topItems.Count < 3) return items.Skip(items.Count() - maxValue).Take(maxValue).ToList();
            if (items.Count() > maxValue) return items.Skip(items.Count() - maxValue).Take(maxValue).ToList();
            return items.ToList();
        }

        [Cache]
        public IList<GoodsItem> GetAccessoryGoods(int objectId, int shopType)
        {
            const string sql = @"
SELECT 
	g.OID
	, o.objectID
	, g.Model
	, g.productType
	, c.companyName
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, gp.price
	, gp.oldPrice
    , gs.inStock
    , gs.stockAmount
    , hitWeight = ISNULL(gh.popularity, 0)
    , gs.zombi
	, isNew = dbo.fIsNew(g.OID, @shopType)
	, isHit = dbo.fIsHit(g.OID, @shopType)
	, isSale = dbo.fIsSale(g.OID, @shopType)
	, n.nodeID
FROM
	t_Goods ag
	join t_object ao on ag.oid = ao.oid
	join t_GoodsAccessories ga on ao.oid = ga.oid	
	join t_Goods g on ga.accessoryOID = g.OID
	inner join t_Object o on g.OID = o.OID
    left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
	inner join t_Company c on g.manufacturer = c.OID
	outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	cross apply (
		select top 1 n.nodeID
		from t_ThemeMasters tm join t_Nodes n on n.object = tm.OID 
		where tm.masterOID = g.category
	) n(nodeID)
WHERE
	ao.objectid = @oid and
	EXISTS(SELECT * FROM t_Nodes n inner join t_ThemeMasters tm on n.object = tm.OID inner join t_Goods g1 on tm.masterOID = g1.category and g1.OID = g.OID)
ORDER BY
    gp.price, ISNULL(gh.popularity, 0) DESC, gs.stockAmount DESC    
";

            var prms = new Hashtable { { "shopType", shopType }, { "oid", objectId } };
            var ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            return (from row in ds.Table.AsEnumerable()
                    select new GoodsItem
                    {
                        OID = row.Field<Guid>("OID"),
                        CategoryId = row.Field<int>("nodeID"),
                        ObjectID = row.Field<int>("objectID"),
                        Model = row.Field<string>("model"),
                        ProductType = row.Field<string>("productType"),
                        ManufacturerName = row.Field<string>("companyName"),
                        Image = new Image
                        {
                            Id = row.Field<int?>("imageID") ?? BlankImageSmallId,
                            OID = row.Field<Guid?>("image") ?? BlankImageSmall,
                            Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                            Width = row.Field<int?>("imageWidth") ?? BlankImageSmallWidth,
                            Height = row.Field<int?>("imageHeight") ?? BlankImageSmallHeight,
                            MimeType = row.Field<string>("mimeType") ?? BlankImageSmallMimeType
                        },
                        Price = row.Field<decimal>("price"),
                        OldPrice = row.Field<decimal>("oldPrice"),
                        InStock = row.Field<bool>("inStock") && !row.Field<bool>("zombi"),
                        StockAmount = row.Field<int>("stockAmount"),
                        HitWeight = row.Field<int>("hitWeight"),
                        Zombi = row.Field<bool>("zombi"),
                        IsNew = row.Field<bool>("isNew"),
                        IsHit = row.Field<bool>("isHit"),
                        IsSale = row.Field<bool>("isSale")
                    }).ToList();
        }

        [Cache]
        public IList<Company> GetManufacturers(int shopType, int selectedNode)
        {
            const string sql = @"
SELECT 
	c.OID,
	o.objectID,
	c.companyName,
    o.shortLink,
	goodsCount = Count(DISTINCT convert(varchar(100), g.OID))     
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft And n.rgt
	inner join t_ThemeMasters tm on n1.object = tm.OID
	inner join t_Goods g on tm.masterOID = g.category 
	inner join t_GoodsPrices gp on g.OID = gp.OID And gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_Object o on c.OID = o.OID    
WHERE
	n.nodeId = @selectedNode
	and EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)
group by	
	c.OID, o.objectID, o.shortLink, c.companyName
";
            var prms = new Hashtable();
            prms["selectedNode"] = selectedNode;
            prms["shopType"] = shopType;
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            return (from row in ds.Table.AsEnumerable()
                    orderby row.Field<string>("companyName")
                    select new Company
                    {
                        OID = row.Field<Guid>("OID"),
                        Id = row.Field<int>("objectID"),
                        Name = row.Field<string>("companyName"),
                        ShortLink = row.Field<string>("shortLink")?.ToLower(),
                        GoodsCount = row.Field<int>("goodsCount")
                    }
            ).ToList();
        }

        [Cache]
        public IList<PriceIntervalMenu> GetGoodsPriceIntervals(int shopType, int selectedNode)
        {
            const string sql = @"
SELECT DISTINCT
	pi.priceBegin,
	pi.priceEnd,
	goodsCount = Count(DISTINCT goods.OID) 
FROM
	t_Nodes n
	inner join t_ThemePriceIntervals pi on n.object = pi.OID and pi.shopType = @shopType
	cross apply (SELECT g1.OID 
		FROM t_Goods g1 
			inner join t_ThemeMasters tm1 on g1.category = tm1.masterOID
			inner join t_Nodes n1 on n1.tree = n.tree And n1.lft BETWEEN n.lft And n.rgt and tm1.OID = n1.object
			inner join t_GoodsInStocks gs1 on g1.OID = gs1.OID and gs1.shopType = @shopType and gs1.inStock = 1
			inner join t_GoodsPrices gp1 on g1.OID = gp1.OID and gp1.shopType = (@shopType + 100)
				and gp1.price BETWEEN pi.priceBegin And pi.priceEnd
			inner join t_GoodsParams gp on g1.OID = gp.OID
	) goods
WHERE
	n.nodeID = @selectedNode
GROUP BY 
	pi.priceBegin,
	pi.priceEnd
";
            return sql.ExecSql(new { selectedNode, shopType }).Select(row =>
                new PriceIntervalMenu
                {
                    PriceBegin = row.Field<decimal>("PriceBegin"),
                    PriceEnd = row.Field<decimal>("PriceEnd"),
                    GoodsCount = row.Field<int>("goodsCount")
                }).OrderBy(p => p.PriceBegin).ToList();
        }

        [Cache]
        public IList<CustomFilter> GetFilters(int shopType, int id)
        {
            const string sql = @"
SELECT
	pt.OID
	, name = ISNULL(pt.altName, pt.name)
	, tp.ordValue
FROM
	t_Nodes n
	inner join t_TemplateParams tp on n.object = tp.OID And tp.isFilter = 1
	inner join t_ParamType pt on tp.paramTypeOID = pt.OID
WHERE
	n.nodeId = @selectedNode

SELECT
	pt.OID,
	tpv.ordValue,
	tpv.name,
    tpv.id,
	goodsCount = (
		SELECT Count(DISTINCT CONVERT(varchar(100), g1.OID)) 
		FROM
            t_Nodes n1 
            inner join t_Nodes n2 on n1.tree = n2.tree and n2.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @selectedNode 
			inner join t_ThemeMasters tm1 on n2.object = tm1.OID
            inner join t_Goods g1 on tm1.masterOID = g1.category 
			inner join t_GoodsInStocks gs1 on g1.OID = gs1.OID and gs1.shopType = @shopType and gs1.inStock = 1 
			inner join t_GoodsPrices gpr1 on g1.OID = gpr1.OID And gpr1.shopType = @shopType and gpr1.price > 0
			inner join t_GoodsParams gp1 on g1.OID = gp1.OID and gp1.paramTypeOID = pt.OID
			inner join t_TemplateParamAssociation tpa1 on tpa1.OID = tpv.OID And tpa1.paramTypeOID = tpv.paramTypeOID and tpa1.name = tpv.name
				and tpa1.value = gp1.value and tpa1.unit = gp1.unit
	)
FROM
	t_Nodes n
	inner join t_TemplateParams tp on n.object = tp.OID And tp.isFilter = 1
	inner join t_ParamType pt on tp.paramTypeOID = pt.OID
	inner join t_TemplateParamValues tpv on tp.OID = tpv.OID And tp.paramTypeOID = tpv.paramTypeOID
WHERE
	n.nodeId = @selectedNode

SELECT DISTINCT
	pt.OID,
	tpv.ordValue,
	tpv.name,
	tpa.value,
	tpa.unit
FROM
	t_Nodes n
	inner join t_TemplateParams tp on n.object = tp.OID And tp.isFilter = 1
	inner join t_ParamType pt on tp.paramTypeOID = pt.OID
	inner join t_TemplateParamValues tpv on tp.OID = tpv.OID And tp.paramTypeOID = tpv.paramTypeOID
	inner join t_TemplateParamAssociation tpa on tpv.OID = tpa.OID and tpv.paramTypeOID = tpa.paramTypeOID and tpv.name = tpa.name
WHERE
	n.nodeId = @selectedNode
";
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, new Hashtable { { "selectedNode", id }, { "shopType", shopType } }, false, null);
            return ds.Table.AsEnumerable().OrderBy(k => k.Field<int>("ordValue")).Select(k => new CustomFilter
            {
                ParamType = k.Field<Guid>("OID"),
                Name = k.Field<string>("name"),
                Values = ds.Tables[1].AsEnumerable()
                    .Where(k1 => k1.Field<Guid>("OID") == k.Field<Guid>("OID"))
                    .OrderBy(k1 => k1.Field<int>("ordValue"))
                    .Select(k1 => new CustomFilterValue
                    {
                        ParamType = k1.Field<Guid>("OID"),
                        Name = k1.Field<string>("name"),
                        OrdValue = k1.Field<int>("ordValue"),
                        ItemsCount = k1.Field<int>("goodsCount"),
                        Id = k1.Field<int>("id"),
                        Associations = ds.Tables[2].AsEnumerable()
                            .Where(k2 => k2.Field<Guid>("OID") == k1.Field<Guid>("OID") && k2.Field<string>("name") == k1.Field<string>("name"))
                            .Select(k2 => new CustomFilterValueAssociation
                            {
                                Value = k2.Field<string>("value"),
                                Unit = k2.Field<string>("unit")
                            }).ToList()
                    }).ToList()
            }).Distinct().ToList();
        }

        public IList<CustomFilter> GetCurrentFilter(int shopType, int id, NameValueCollection prms)
        {
            var allFilters = GetFilters(shopType, id);
            var result = allFilters.SelectMany(k => k.Values)
                .Where(k => prms.AllKeys.Contains(string.Format("flt{0}_{1}", k.ParamType, k.OrdValue))
                    && prms[string.Format("flt{0}_{1}", k.ParamType, k.OrdValue)].IndexOf(",") != -1).ToList();
            return result.Select(k => new CustomFilter
            {
                ParamType = k.ParamType,
                Values = result.Where(k1 => k1.ParamType == k.ParamType).ToList()
            }).Distinct().ToList();
        }

        private static readonly Regex OrdValueRegex = new Regex(@"(\d*)$", RegexOptions.Compiled);

        private static int GetOrdValue(string item)
        {
            Match m = OrdValueRegex.Match(item);
            return m.Groups[1].Value != "" ? int.Parse(m.Groups[1].Value) : 0;
        }

        [Cache]
        public GoodsItem GetGoodsItem(int objectID, int shopType, int rootNode)
        {
            const string sql = @"   
SELECT 
	g.OID
	, o.objectID
	, g.model
	, g.pseudoID
	, g.productType
	, c.companyName
	, c.altName
    , c.companyURL
	, description = gd.description
	, gp.price
    , gp.salePrice
	, gp.deliveryPrice
    , gp.deliveryPriceSelf
    , gp.onlinePay
	, isHit = ISNULL(gh.isHit, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, isSale = ISNULL(gis.isSale, 0)
	, gs.inStock
	, gs.zombi
	, category.categoryId
	, category.categoryName
    , category.categoryOID
    , o.title
    , o.keywords
    , pageDescription	
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID
	inner join t_Company c on g.manufacturer = c.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100)
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	left join t_GoodsIsSales gis on g.OID = gis.OID and gis.shopType = @shopType
	left join t_GoodsDescriptions gd on g.OID = gd.OID and gd.shopType = @shopType
	outer apply (SELECT TOP 1 n.nodeID, n.nodeName, n.object FROM t_Nodes n 
		inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		ORDER BY n.lft
	) category(categoryId, categoryName, categoryOID)
WHERE
	o.objectID = @objectID

SELECT
	paramOID = pt.OID
	, paramName = ISNULL(pt.altName, pt.name)
	, gp.ordValue
	, gp.value
	, gp.unit
	, tp.isVisible
FROM
	t_GoodsParams gp 
	inner join t_Object o on gp.OID = o.OID
	inner join t_Goods g on o.OID = g.OID
	inner join t_ParamType pt on gp.paramTypeOID = pt.OID
	outer apply (SELECT TOP 1 n.object FROM t_Nodes n 
		inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
		inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category
		ORDER BY n.lft
	) category(OID)
	left join t_TemplateParams tp on category.OID = tp.OID and tp.paramTypeOID = gp.paramTypeOID
WHERE
	o.objectID = @objectID
ORDER BY
	gp.ordValue

SELECT
	oi.ordValue
	, oi.binData
	, b.width
	, b.height
    , b.mimeType
    , obj.objectID	
FROM
	t_ObjectImage oi
	inner join t_Object o on oi.OID = o.OID
	inner join t_BinaryData b on oi.binData = b.OID
    inner join t_Object obj on b.OID = obj.OID
WHERE
	o.objectID = @objectID

SELECT
	b.name
    , b.mimeType
	, od.binDataOID
    , obj.objectID	
FROM
	t_ObjectDocs od
    inner join t_binaryData b on od.binDataOID = b.OID
    inner join t_Object obj on b.OID = obj.OID
	inner join t_Object o on od.OID = o.OID
WHERE
	o.objectID = @objectID
";
            var prms = new Hashtable { { "objectID", objectID }, { "shopType", shopType }, { "rootNode", rootNode } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);

            var item = (from row in ds.Table.AsEnumerable()
                        select new GoodsItem
                        {
                            OID = row.Field<Guid>("OID"),
                            ObjectID = row.Field<int>("objectID"),
                            ProductType = row.Field<string>("productType"),
                            Model = row.Field<string>("model"),
                            PseudoID = row.Field<string>("pseudoID"),
                            ManufacturerName = row.Field<string>("companyName"),
                            ManufacturerAltName = string.IsNullOrEmpty(row.Field<string>("altName")) ? row.Field<string>("companyName") : row.Field<string>("altName"),
                            ManufacturerURL = row.Field<string>("companyURL"),
                            Description = (row.Field<string>("description") ?? "").ToHtml(),
                            ShortDescription = (ds.Tables[1]
                            .AsEnumerable()
                            .Where(dr => dr.Field<Guid>("paramOID") == Settings.Default.ShortDescriptionParam)
                            .Select(dr => dr.Field<string>("value"))
                            .SingleOrDefault() ?? "").CrLf2Br().ToHtml(),
                            CategoryId = row.Field<int?>("categoryId") ?? 0,
                            CategoryOID = row.Field<Guid?>("categoryOID"),
                            CategoryName = (row.Field<string>("categoryName") ?? "").ToLower(),
                            CategoryNameEncoded = (row.Field<string>("categoryName") ?? "").ToLower().TranslitEncode(true),
                            Title = row.Field<string>("title"),
                            Keywords = row.Field<string>("keywords"),
                            PageDescription = row.Field<string>("pageDescription"),
                            Price = row.Field<decimal>("price"),
                            SalePrice = row.Field<decimal?>("salePrice"),
                            IsHit = row.Field<bool>("isHit"),
                            IsNew = row.Field<bool>("isNew"),
                            IsSale = row.Field<bool>("isSale"),
                            InStock = row.Field<bool>("InStock") && !row.Field<bool>("zombi"),
                            Zombi = row.Field<bool>("zombi"),
                            OnlinePay = row.Field<bool>("OnlinePay"),
                            Delivery = new DeliveryInfo
                            {
                                Price = row.Field<decimal?>("deliveryPrice") ?? 0,
                                PriceSelf = row.Field<decimal?>("deliveryPriceSelf") ?? 0,
                                GoodsName = GoodsHelper.FullName(row.Field<string>("productType"), null, row.Field<string>("model"))
                            },
                        }).DefaultIfEmpty(new GoodsItem()).First();

            item.GoodsParams = (from row in ds.Tables[1].AsEnumerable()
                                    //.Where(dr => dr.Field<Guid>("paramOID") != Settings.Default.ShortDescriptionParam)
                                orderby row.Field<int>("ordValue")
                                select new ParamItem
                                {
                                    ParamName = row.Field<string>("paramName"),
                                    ParamValue = row.Field<string>("value"),
                                    Unit = row.Field<string>("unit"),
                                    IsVisible = row.Field<bool?>("isVisible") ?? true
                                }).ToList();
            var imageLink = "";
            if (ds.Tables[2].AsEnumerable().Any(r => r.Field<string>("ordValue").ToLower().StartsWith("для карточки")))
            {
                imageLink = "для карточки";
            }
            else if (ds.Tables[2].AsEnumerable().Any(r => r.Field<string>("ordValue").ToLower().StartsWith("для popup")))
            {
                imageLink = "для popup";
            }

            if (imageLink != "")
            {
                item.GoodsImages = (
                    from row in ds.Tables[2].AsEnumerable()
                    where row.Field<string>("ordValue").ToLower().StartsWith(imageLink)
                    select new ImageTrio
                    {
                        OrdValue = GetOrdValue(row.Field<string>("ordValue")),
                        Image = new Image
                        {
                            Id = row.Field<int>("objectID"),
                            OID = row.Field<Guid>("binData"),
                            Name = item.FullName.TranslitEncode(true).ToLower(),
                            Width = row.Field<int>("width"),
                            Height = row.Field<int>("height"),
                            MimeType = row.Field<string>("mimeType")
                        },
                        SmallImage = (from row1 in ds.Tables[2].AsEnumerable()
                                      where row1.Field<string>("ordValue").ToLower().StartsWith("для табличного")
                                      && GetOrdValue(row1.Field<string>("ordValue")) == GetOrdValue(row.Field<string>("ordValue"))
                                      select new Image
                                      {
                                          Id = row1.Field<int>("objectID"),
                                          OID = row1.Field<Guid>("binData"),
                                          Name = item.FullName.TranslitEncode(true).ToLower(),
                                          Width = row1.Field<int>("width"),
                                          Height = row1.Field<int>("height"),
                                          MimeType = row1.Field<string>("mimeType")
                                      }).SingleOrDefault(),
                        LargeImage = (from row1 in ds.Tables[2].AsEnumerable()
                                      where row1.Field<string>("ordValue").ToLower().StartsWith("для popup")
                                      && GetOrdValue(row1.Field<string>("ordValue")) == GetOrdValue(row.Field<string>("ordValue"))
                                      select new Image
                                      {
                                          Id = row1.Field<int>("objectID"),
                                          OID = row1.Field<Guid>("binData"),
                                          Name = item.FullName.TranslitEncode(true).ToLower(),
                                          Width = row1.Field<int>("width"),
                                          Height = row1.Field<int>("height"),
                                          MimeType = row1.Field<string>("mimeType")
                                      }).SingleOrDefault()
                    }
                    ).ToList();
            }
            else
            {
                item.GoodsImages = new ImageTrio[] {
                    new ImageTrio {
                        LargeImage = new Image
                        {
                            Id = BlankImageLargeId,
                            OID = BlankImageLarge,
                            Name = "no-photo",
                            Width = BlankImageLargeWidth,
                            Height = BlankImageLargeHeight,
                            MimeType = BlankImageLargeMimeType
                        },
                        SmallImage = new Image
                        {
                            Id = BlankImageSmallId,
                            OID = BlankImageSmall,
                            Name = "no-photo",
                            Width = BlankImageSmallWidth,
                            Height = BlankImageSmallHeight,
                            MimeType = BlankImageSmallMimeType
                        },
                        Image = new Image
                        {
                            Id = BlankImageMediumId,
                            OID = BlankImageMedium,
                            Name = "no-photo",
                            Width = BlankImageMediumWidth,
                            Height = BlankImageMediumHeight,
                            MimeType = BlankImageMediumMimeType
                        }
                    }
                };
            }


            if (item.GoodsImages != null)
            {
                foreach (var imgT in item.GoodsImages.Where(imgT => imgT.SmallImage == null))
                {
                    imgT.SmallImage = imgT.Image;
                }
            }

            if (item.GoodsImages?.Count == 0)
            {
                item.GoodsImages = (from row in ds.Tables[2].AsEnumerable()
                                    where row.Field<string>("ordValue").ToLower().StartsWith("для табличного")
                                    select new ImageTrio
                                    {
                                        OrdValue = GetOrdValue(row.Field<string>("ordValue")),
                                        Image = new Image
                                        {
                                            OID = row.Field<Guid>("binData"),
                                            Width = row.Field<int>("width"),
                                            Height = row.Field<int>("height")
                                        },
                                        SmallImage = new Image
                                        {
                                            OID = row.Field<Guid>("binData"),
                                            Width = row.Field<int>("width"),
                                            Height = row.Field<int>("height")
                                        },
                                        LargeImage = new Image
                                        {
                                            OID = row.Field<Guid>("binData"),
                                            Width = row.Field<int>("width"),
                                            Height = row.Field<int>("height")
                                        }
                                    }).ToList();
            }

            if (item.GoodsImages == null) item.GoodsImages = new List<ImageTrio>();

            item.Docs = ds.Tables[3]
                .AsEnumerable()
                .OrderBy(r => r.Field<string>("name"))
                .Select(r => new Image
                {
                    Id = r.Field<int>("objectID"),
                    OID = r.Field<Guid>("binDataOID"),
                    Name = r.Field<string>("name"),
                    MimeType = r.Field<string>("mimeType")
                })
                .ToList();

            return item;
        }

        [Cache]
        public int? GetFirstGoodsCategory(int oid, int catalogRoot)
        {
            const string sql = @"
SELECT TOP 1 n.nodeId 
FROM 
	t_Goods g
	inner join t_Object o on g.OID = o.OID and o.objectID = @id
	inner join t_ThemeMasters tm on tm.masterOID = g.category
	inner join t_Nodes n on n.object = tm.OID 
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
";
            var prms = new Hashtable { { "id", oid }, { "rootNode", catalogRoot } };
            return (int?)DBHelper.ExecuteScalar(sql, prms, false);
        }

        public IDictionary<int, string> GetGoodsUrl(int shopType, int[] objectIDs)
        {
            const string sql = @"
SELECT
	o.objectID,
	g.productType,
	c.companyName,
	g.model
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID
	inner join t_Company c on g.manufacturer = c.OID
WHERE
	o.objectID in ({0})

SELECT
	address
FROM
	t_TypeShop
WHERE 
	shopType = @shopType
	";
            var data = string.Format(sql, objectIDs.AsEnumerable()
                .Aggregate(new StringBuilder(), (sb, i) => sb.AppendFormat("{0}, ", i), sb =>
                {
                    sb.Length -= 2;
                    return sb.ToString();
                })).ExecMSql(new { shopType });
            var siteUrl = data[1].AsEnumerable().Select(row => row.Field<string>("address")).SingleOrDefault();
            return data[0]
                .AsEnumerable()
                .Select(r => new
                {
                    key = r.Field<int>("objectID"),
                    value = GoodsHelper.GoodsUrl(siteUrl, r.Field<string>("productType"), r.Field<string>("companyName"), r.Field<string>("model"), r.Field<int>("objectID"))
                }).ToDictionary(t => t.key, t => t.value);
        }

        [Cache]
        public int GetGoodsCategoryId(int objectId, int catalogRoot)
        {
            const string sql = @"
SELECT
	nn.nodeID
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID
	cross apply (
		select top 1 n.nodeID, n.nodeName
		from t_ThemeMasters tm
			inner join t_Nodes n on n.object = tm.OID
			inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @catalogRoot
		where tm.masterOID = g.category
		order by n.lft
	) nn
WHERE
	o.objectId = @objectId

	";
            return sql.ExecSql(new { objectId, catalogRoot }).Select(row => row.Field<int>("nodeID")).FirstOrDefault();
        }

        [Cache]
        public string GetGoodsPath(int objectID)
        {
            const string sql = @"
SELECT
	o.objectID,
	g.productType,
	companyName = ISNULL(c.companyName, ''),
	model = ISNULL(g.model, '')
FROM
	t_Goods g
	inner join t_Object o on g.OID = o.OID
	inner join t_Company c on g.manufacturer = c.OID
WHERE
	o.objectID  = @objectID
	";
            return sql.ExecSql(new { objectID })
                .Select(r => GoodsHelper.GoodsPath(
                    r.Field<string>("productType"),
                    r.Field<string>("companyName"),
                    r.Field<string>("model"),
                    r.Field<int>("objectID"))
                ).DefaultIfEmpty("/").Single();
        }
    }
}
