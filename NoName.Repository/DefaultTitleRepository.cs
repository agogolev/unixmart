using System;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class DefaultTitleRepository : IDefaultTitleRepository
    {
        private const string title = "Интернет магазин Unixmart.ru";
        private const string keywords = "";
        private const string page_description = "Купить в Unixmart.ru";
        private readonly IBaseRepository _baseRepository;

        public DefaultTitleRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        [Cache]
        public TitleData LoadTitle(int defaultType, Guid categoryOID, int rootNode)
        {
            var sql = @"
SELECT
    tag = 1
    , title
    , keywords
    , pageDescription
	, n1.lft
    , dateCreate
FROM
    t_DefaultTitle dt
    inner join t_Object o on dt.OID = o.OID and dt.isActive = 1 and dt.defaultType = @defaultType
    inner join t_Nodes n1 on dt.theme = n1.object 
    inner join t_Nodes n2 on n2.lft BETWEEN n1.lft and n1.rgt and n2.object = @categoryOID
    inner join t_Nodes n3 on n1.lft BETWEEN n3.lft and n3.rgt and n3.nodeID = @rootNode
UNION ALL
SELECT 
    2
    , title
    , keywords
    , pageDescription
	, 0
    , dateCreate
FROM
    t_DefaultTitle dt
    inner join t_Object o on dt.OID = o.OID and dt.isActive = 1 and dt.defaultType = @defaultType and dt.theme is null
UNION ALL
SELECT
    3
    , @title
    , @keywords
    , @page_description
	, 0
    , null
ORDER BY
    tag, lft desc, dateCreate desc
";
            var data = sql.ExecSql(new { defaultType, categoryOID, rootNode, title, keywords, page_description });
            return new TitleData
            {
                Title = data.First().Field<string>("title"),
                Keywords = data.First().Field<string>("keywords"),
                PageDescription = data.First().Field<string>("pageDescription")
            };
        }

        public TitleData LoadTitle(int defaultType, string shortLink, int rootNode)
        {
            Guid? obj = _baseRepository.GetObject(shortLink);
            return LoadTitle(defaultType, obj ?? Guid.Empty, rootNode);
        }

        //        [Cache]
        //        public TitleData LoadTitle(int defaultType, Guid categoryOID, int rootNode)
        //        {
        //            var sql = @"
        //SELECT TOP 1
        //    title
        //    , keywords
        //    , pageDescription
        //FROM
        //    t_DefaultTitle dt
        //    inner join t_Object o on dt.OID = o.OID and dt.isActive = 1 and dt.defaultType = @defaultType
        //ORDER BY
        //    o.dateCreate desc
        //";
        //            var data = sql.ExecSql(new { defaultType });
        //            return new TitleData
        //            {
        //                Title = data.FirstOrDefault()?.Field<string>("title") ?? title,
        //                Keywords = data.FirstOrDefault()?.Field<string>("keywords") ?? keywords,
        //                PageDescription = data.FirstOrDefault()?.Field<string>("pageDescription") ?? page_description
        //            };
        //        }
    }
}