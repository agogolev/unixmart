using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Core.Aspects;
using DBReader;
using Ecommerce.Extensions;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class PromoBlockRepository : IPromoBlockRepository
    {
        [Cache]
        public IList<PromoBlock> GetPromoList(int parentNode, Action<PromoBlock> process)
        {
            var sql = @"
SELECT
	p.name
	, p.url
	, p.description
	, imageOID = b.oid
    , imageId = o.objectID
	, b.width
	, b.height
    , b.mimeType
FROM 
	t_Nodes n
	inner join t_Promo p on n.object = p.OID
	left join t_binarydata b on p.image = b.oid
    left join t_Object o on b.OID = o.OID
WHERE n.parentNode = @parentNode
ORDER BY n.lft 
";
            var result = sql.ExecSql(new { parentNode }).Select(row => new PromoBlock
            {
                Name = row.Field<string>("name"),
                Url = !string.IsNullOrWhiteSpace(row.Field<string>("url")) ? row.Field<string>("url") : "#",
                Description = row.Field<string>("description"),
                Image = new Image
                {
                    Id = row.Field<int?>("imageId") ?? 0,
                    OID = row.Field<Guid?>("imageOID") ?? Guid.Empty,
                    Name = row.Field<string>("name").TranslitEncode(true).ToLower(),
                    Width = row.Field<int?>("width") ?? 0,
                    Height = row.Field<int?>("height") ?? 0,
                    MimeType = row.Field<string>("mimeType")
                }
            }).ToList();
            if (process != null)
            {
                result.ToList().ForEach(process);
            }
            return result;
        }

        [Cache]
        public IList<IList<PromoBlock>> GetPromoMultiList(int mainPromoBlock)
        {
            string sql = @"
SELECT
	p.name
	, p.url
	, p.description
	, n.nodeId
	, n.parentNode
	, imageOID = b.oid
    , imageId = o.objectID
	, b.width
	, b.height
    , b.mimeType
FROM 
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft and n.lft < n1.rgt and n.nodeId = @parentNode
	left join t_Promo p on n.object = p.OID
	left join t_binarydata b on p.image = b.oid
    left join t_Object o on b.OID = o.OID
ORDER BY n.lft
";
            IEnumerable<DataRow> items = sql.ExecSql(new {parentNode = mainPromoBlock});
            var ret = new List<IList<PromoBlock>>();
            List<int> ids =
                items.Where(row => !row.Field<Guid?>("imageOID").HasValue)
                    .Select(row => row.Field<int>("nodeID"))
                    .ToList();
            ids.ForEach(item =>
            {
                ret.Add(items.Where(row => row.Field<int>("parentNode") == item).Select(row => new PromoBlock
                {
                    Name = row.Field<string>("name"),
                    Url = row.Field<string>("url"),
                    Description = row.Field<string>("description"),
                    Image = new Image
                    {
                        Id = row.Field<int>("imageId"),
                        OID = row.Field<Guid>("imageOID"),
                        Name = row.Field<string>("name").TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("width"),
                        Height = row.Field<int>("height"),
                        MimeType = row.Field<string>("mimeType")
                    }
                }).ToList());
            });
            return ret;
        }

        [Cache]
        public PromoBlock GetPromo(int id)
        {
            string sql = @"
SELECT
	p.name
	, p.url
	, p.description
	, image = ISNULL(b.oid, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb')
    , imageId = ISNULL(o.objectID, 37634)
	, width = ISNULL(b.width, 0)
	, height = ISNULL(b.height, 0)
    , b.mimeType
FROM 
	t_Promo p
	inner join t_Nodes n on p.OID = n.object
	left join t_binarydata b on p.image = b.oid
    left join t_Object o on b.OID = o.OID
where
	n.nodeID = @id
";
            return sql.ExecSql(new {id}).Select(row => new PromoBlock
            {
                Name = row.Field<string>("name"),
                Url = row.Field<string>("url"),
                Description = row.Field<string>("description"),
                Image = new Image
                {
                    Id = row.Field<int>("imageId"),
                    OID = row.Field<Guid>("imageOID"),
                    Name = row.Field<string>("name").TranslitEncode(true).ToLower(),
                    Width = row.Field<int>("width"),
                    Height = row.Field<int>("height"),
                    MimeType = row.Field<string>("mimeType")
                }
            }).DefaultIfEmpty(new PromoBlock
            {
                Image = new Image
                {
                    OID = new Guid("3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb"),
                    Id = 37634,
                    Name = "empty",
                    Width = 0,
                    Height = 0
                }
            }).Single();
        }

        [Cache]
        public HtmlString GetSliderContent(int nodeID)
        {
            const string sql = @"
SELECT
	p.description
FROM 
	t_Nodes n
	inner join t_Promo p on n.object = p.OID
WHERE
	n.nodeID = @nodeID
";
            return
                sql.ExecSql(new {nodeID})
                    .Select(row => new HtmlString(row.Field<string>("description")))
                    .DefaultIfEmpty(new HtmlString(""))
                    .Single();
        }
    }
}