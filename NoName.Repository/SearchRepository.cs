using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using DBReader;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using NoName.Domain;
using NoName.Domain.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;
using MetaData;
using NoName.Extensions;

namespace NoName.Repository
{
    public class SearchRepository : ISearchRepository
    {
        public IList<SearchedGoods> SearchResult(int shopType, int rootNode, string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return new List<SearchedGoods>();

            string sql =
                @"
SELECT
	g.OID
	, o.objectID
	, g.pseudoID
	, g.Model
	, g.productType
    , c.companyName
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, mp.deliveryFirst
	, gp.deliveryPrice
	, gp.price
	, gp.oldPrice
    , gs.inStock
    , gs.stockAmount
    , gs.zombi
	, isHit = ISNULL(gh.isHit, 0)
	, hitWeight = ISNULL(gh.popularity, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, catalogID = n.nodeID
	, catalogName = n.nodeName
FROM t_Goods g
	inner join t_Object o on g.OID = o.OID
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_GoodsInStocks gs ON g.OID = gs.OID AND gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	inner join t_GoodsPrices gp ON g.OID = gp.OID AND gp.shopType = (@shopType + 100) and gp.price > 0
	LEFT JOIN t_GoodsDescriptions gd ON g.OID = gd.OID and gd.shopType = @shopType
	outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	inner join t_ThemeMasters tm ON g.category = tm.masterOID
	inner join t_MasterPrices mp on g.category = mp.OID and mp.shopType = @shopType 
	INNER JOIN t_Nodes n ON tm.OID = n.object
	INNER JOIN t_Nodes n1 ON n.tree = n1.tree AND n.lft BETWEEN n1.lft AND n1.rgt AND n1.nodeId=@rootNode
WHERE
	{0}  {1} 

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft between n1.lft and n1.rgt and n1.nodeId = @rootNode
	inner join t_ThemeMasters tm on n.object = tm.OID
	inner join t_Goods g on tm.masterOID = g.category
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
	LEFT JOIN t_GoodsDescriptions gd ON g.OID = gd.OID and gd.shopType = @shopType
	inner join t_Company c ON g.manufacturer = c.OID
	inner join t_TemplateParams tp on tp.OID = n.object And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID
WHERE
	{0} {1}
ORDER BY
	g.OID, 
	gps.ordValue";

            var prms = new Hashtable();
            prms["shopType"] = shopType;
            prms["rootNode"] = rootNode;

            string searchId = "";
            if (Regex.IsMatch(query.Trim(), @"^\d+$"))
            {
                searchId = string.Format(@" 
or g.pseudoID = {0}
", int.Parse(query));
            }

            sql = string.Format(sql,
                "(ISNULL(g.model, '')+' '+ISNULL(g.productType, '')+' '+ISNULL(c.companyName, '')+' '+ISNULL(substring(gd.description, 1, 5000), ''))"
                    .SplitToSearch(query), searchId);

            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);

            prms.Clear();
            prms["searchPhrase"] = query.TrimInner().Trim();
            DBHelper.ExecuteCommand("upIncrementSearch", prms, true);

            return ds.Table.AsEnumerable()
                .Select(row => new SearchedGoods
                {
                    OID = row.Field<Guid>("OID"),
                    CatalogId = row.Field<int>("catalogID"),
                    CatalogName = row.Field<string>("catalogName"),
                    ObjectID = row.Field<int>("ObjectID"),
                    ProductType = row.Field<string>("productType"),
                    Model = row.Field<string>("model"),
                    ManufacturerName = row.Field<string>("companyName"),
                    ShortDescription = ds.Tables[1].AsEnumerable()
                        .Where(
                            r1 =>
                                r1.Field<Guid>("OID") == row.Field<Guid>("OID") &&
                                r1.Field<Guid>("paramTypeOID") == Settings.Default.ShortDescriptionParam)
                        .Select(k => k.Field<string>("value"))
                        .DefaultIfEmpty("")
                        .Single().ToHtml(),
                    Price = row.Field<decimal>("price"),
                    OldPrice = row.Field<decimal>("oldPrice"),
                    InStock = row.Field<bool>("inStock"),
                    Zombi = row.Field<bool>("zombi") && !row.Field<bool>("zombi"),
                    IsHit = row.Field<bool>("isHit"),
                    HitWeight = row.Field<int>("hitWeight") + (row.Field<bool>("isHit") ? 10 : 0),
                    IsNew = row.Field<bool>("isNew"),
                    Delivery =
                        new DeliveryInfo
                        {
                            Price = row.Field<decimal?>("deliveryPrice") ?? row.Field<decimal>("deliveryFirst")
                            /*, InstantDelivery = r.Field<bool>("instantDelivery")*/
                        },
                    Image = new Image
                    {
                        Id = row.Field<int?>("imageID") ?? GoodsRepository.BlankImageMediumId,
                        OID = row.Field<Guid?>("image") ?? GoodsRepository.BlankImageMedium,
                        Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                        Width = row.Field<int?>("imageWidth") ?? GoodsRepository.BlankImageMediumWidth,
                        Height = row.Field<int?>("imageHeight") ?? GoodsRepository.BlankImageMediumHeight,
                        MimeType = row.Field<string>("mimeType") ?? GoodsRepository.BlankImageMediumMimeType
                    },
                    GoodsParams = ds.Tables[1].AsEnumerable()
                        .Where(
                            r1 =>
                                r1.Field<Guid>("OID") == row.Field<Guid>("OID") &&
                                r1.Field<Guid>("paramTypeOID") != Settings.Default.ShortDescriptionParam)
                        .Select(r1 => new ParamItem
                        {
                            ParamType = r1.Field<Guid>("paramTypeOID"),
                            ParamName = r1.Field<string>("name"),
                            ParamValue = r1.Field<string>("value"),
                            Unit = r1.Field<string>("unit")
                        }).ToList()
                }).ToList();
        }

        public IList<GoodsItem> SearchFilteredResult(IList<SearchedGoods> items, int skip, int take, string sort, int catalogID, out int total)
        {
            total = 0;
            if (!items.Any())
                return new List<GoodsItem>();

            IEnumerable<GoodsItem> source = catalogID != 0
                ? items.Where(s => s.CatalogId == catalogID).DistinctGoods().ToList()
                : items.DistinctGoods().ToList();

            total = source.Count();

            source = GoodsSorter(sort, source);
            if (take == -1)
                return source.ToList();
            return source.Skip(skip).Take(take).ToList();
        }

        public IList<CatalogMenu> SearchResultCatalogs(IList<SearchedGoods> items)
        {
            if (items == null || items.Count == 0)
                return new List<CatalogMenu>();


            return items.Select(g => new CatalogMenu
            {
                NodeId = g.CatalogId,
                Name = g.CatalogName,
                GoodsCount =
                    items.Where(g1 => g1.CatalogId == g.CatalogId).Distinct(
                        new LambdaComparer<SearchedGoods>((x, y) => x.ObjectID == y.ObjectID)).Count()
            }).Distinct(new LambdaComparer<CatalogMenu>((x, y) => x.NodeId == y.NodeId)).ToList();
        }

        private static IList<GoodsItem> GoodsSorter(string sort, IEnumerable<GoodsItem> goods)
        {
            switch (sort)
            {
                case null:
                    goods = goods.OrderByDescending(v => v.HitWeight).ThenBy(v => v.StockAmount).ThenByDescending(v => v.Price).ToList();
                    break;
                case "up":
                    goods = goods.OrderBy(v => v.Price).ThenBy(v => v.StockAmount).ToList();
                    break;
                case "down":
                    goods = goods.OrderByDescending(v => v.Price).ThenBy(v => v.StockAmount).ToList();
                    break;
            }
            return goods.ToList();
        }
    }
}