using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using Ecommerce.Extensions;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class NewsRepository : INewsRepository
    {
        [Cache]
        public IList<Article> GetAllNewsArticle()
        {
            string sql = @"
SELECT TOP 50
	a.OID
	, o.objectID
	, a.header
	, a.annotation
	, a.pubDate
	, image = ISNULL(image.OID, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb')
	, imageID = ISNULL(image.objectID, 37634)
	, imageWidth = ISNULL(image.width, 200)
	, imageHeight = ISNULL(image.height, 200)
    , mimeType = ISNULL(image.mimeType, 'image/png')
FROM 
	t_Article a
	inner join t_object o on a.OID = o.OID
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue  = 'headerImage' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
WHERE
	a.articleType = 2
ORDER BY
	a.pubDate Desc
";
            return sql.ExecSql(null).Select(row => new Article
            {
                OID = row.Field<Guid>("OID"),
                Id = row.Field<int>("objectID"),
                Header = row.Field<string>("header").ToHtml(),
                Annotation = row.Field<string>("annotation").Cut(200, string.Empty, string.Empty).CrLf2Br().ToHtml(),
                PubDate = row.Field<DateTime>("pubDate"),
                Image =
                    new Image
                    {
                        Id = row.Field<int>("imageID"),
                        OID = row.Field<Guid>("image"),
                        Name = row.Field<string>("header").TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("imageWidth"),
                        Height = row.Field<int>("imageHeight"),
                        MimeType = row.Field<string>("mimeType")
                    }
            }).ToList();
        }

        public IList<Article> GetNewsArticleList(int skip, int take, out int total)
        {
            IList<Article> allArticle = GetAllNewsArticle();
            total = allArticle.Count;
            return allArticle.Skip(skip).Take(take).ToList();
        }
    }
}