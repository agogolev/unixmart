using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using Ecommerce.Extensions;
using MetaData;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class CompanyMenuRepository : ICompanyMenuRepository
    {
        [Cache]
        public IList<Domain.DirectoryItem> GetCompanyMenuByABC(int shopType, int rootNode)
        {
            const string sql =
                @"
SELECT
	c.OID,
	o.objectId,
    o.shortLink,
	c.companyName,
	count.goodsCount,
    o.dateCreate
FROM
	t_Company c
	inner join t_Object o on c.OID = o.OID
	cross apply (
        SELECT SUM(goodsCount) 
        FROM t_ThemeManufGoodsCount tmgc inner join t_Nodes n on tmgc.themeOID = n.object and n.parentNode = @rootNode 
        WHERE tmgc.manufacturerOID = c.OID) count(goodsCount)
WHERE 
	count.goodsCount is not null
";

            var prms = new Hashtable();
            prms["shopType"] = shopType;
            prms["rootNode"] = rootNode;
            var ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            IList<Domain.DirectoryItem> items = (from r in ds.Table.AsEnumerable()
                                                 orderby r.Field<string>("companyName")
                                                 select new Domain.DirectoryItem
                                                 {
                                                     Header = r.Field<string>("companyName").FirstLetter(),
                                                     Items = (from r1 in ds.Table.AsEnumerable()
                                                              where r1.Field<string>("companyName").FirstLetter() == r.Field<string>("companyName").FirstLetter()
                                                                && r.Field<int>("goodsCount") > 0
                                                              orderby r1.Field<string>("companyName")
                                                              select new Domain.CompanyMenu
                                                              {
                                                                  CompanyName = r1.Field<string>("companyName"),
                                                                  ItemId = r1.Field<int>("objectId"),
                                                                  ManufRepresentation = r1.Field<string>("shortLink")?.ToLower(),
                                                                  GoodsCount = r1.Field<int>("goodsCount"),
                                                                  OID = r1.Field<Guid>("OID"),
                                                                  DateCreate = r1.Field<DateTime>("dateCreate")
                                                              }).ToList()
                                                 }
                                         ).Distinct(new LambdaComparer<Domain.DirectoryItem>((x, y) => x.Header == y.Header)).
                ToList();
            return items;
        }
        [Cache]
        public Company GetCompany(string shortLink)
        {
            const string sql = @"
SELECT
    c.OID
    , companyName
    , altName
    , o.shortLink
    , o.objectID
    , o.title
    , o.keywords
    , o.pageDescription
FROM
    t_Company c
    inner join t_Object o on c.OID = o.OID and o.shortLink = @shortLink
";
            return sql.ExecSql(new {shortLink}).Select(row => new Company
            {
                OID = row.Field<Guid>("OID"),
                Id = row.Field<int>("objectID"),
                Name = row.Field<string>("companyName"),
                AltName = row.Field<string>("altName"),
                ShortLink = row.Field<string>("shortLink"),
                Title = row.Field<string>("title"),
                Keywords = row.Field<string>("keywords"),
                PageDescription = row.Field<string>("pageDescription")
            })
                .DefaultIfEmpty(new Company())
                .Single();
        }

        [Cache]
        public IList<CatalogMenu> GetCompanyMenuFlat(int rootNode, int shopType)
        {
            const string sql = @"
SELECT
	n.nodeID
	, manufId = obj.objectID
	, manufRepresentation = obj.shortLink
	, n.nodeName
	, n.parentNode
    , n.lft
    , o.shortLink
    , o.dateCreate
	, countChild = (n.rgt - n.lft - 1)/2
	, t.goodsList
	, t.goodsCount
	, l.level
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID and t.goodsCount > 0 
	inner join t_ThemeManufGoodsCount tmgc on t.OID = tmgc.themeOID
	inner join t_Object obj on tmgc.manufacturerOID = obj.OID and tmgc.goodsCount > 0
    inner join t_Object o on t.OId = o.OID
    cross apply (SELECT Count(*) - 1 
		FROM t_Nodes nn inner join t_Nodes nn1 on nn1.tree = nn.tree And nn.lft BETWEEN nn1.lft 
			And nn1.rgt and nn1.nodeID = @rootNode
		WHERE n.tree = nn.tree And n.lft BETWEEN nn.lft And nn.rgt
	) l(level)
WHERE
    obj.shortLink <> '' and o.shortLink <> ''
ORDER BY
	obj.objectID, n.lft
";
            var prms = new Hashtable();
            prms["rootNode"] = rootNode;
            prms["shopType"] = shopType;
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            List<DataRow> rows = ds.Table.AsEnumerable().ToList();
            List<CatalogMenu> items = rows
                .Select(row => new CatalogMenu
                {
                    NodeId = row.Field<int>("nodeID"),
                    SelectedBrand = new Company { Id = row.Field<int>("manufId"), ShortLink = row.Field<string>("manufRepresentation") },
                    ParentNodeId = row.Field<int>("parentNode"),
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("nodeName"),
                    HasChild = row.Field<int>("countChild") != 0,
                    GoodsList = row.Field<bool>("goodsList"),
                    Level = row.Field<int>("level"),
                    GoodsCount = row.Field<int>("goodsCount"),
                    DateCreate = row.Field<DateTime>("dateCreate")
                }).ToList();
            return items;
        }

        [Cache]
        public void GetCompanyCategoryTitle(int nodeID, Guid companyOID, out string title, out string keywords, out string pageDescription)
        {
            var sql = @"
SELECT
    title
    , keywords 
    , pageDescription
FROM
    t_ThemeBrandTitles tbt
    inner join t_Nodes n on n.object = tbt.OID and n.nodeID = @nodeID and tbt.manufacturerOID = @companyOID
";
            var data = sql.ExecSql(new { nodeID = nodeID, companyOID = companyOID });
            var dr = data.FirstOrDefault();
            title = dr?.Field<string>("title");
            keywords = dr?.Field<string>("keywords");
            pageDescription = dr?.Field<string>("pageDescription");
        }
    }
}
