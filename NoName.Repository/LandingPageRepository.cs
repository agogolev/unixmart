using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using MetaData;
using NoName.Domain;
using NoName.Extensions;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class LandingPageRepository : ILandingPageRepository
    {
        private IArticleRepository _articleRepository;

        public LandingPageRepository(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        [Cache]
        public CatalogMenu GetItem(int id)
        {
            const string sql = @"
SELECT
    l.commodityGroup
    , o.objectID
    , l.name
    , l.h1
    , o.shortLink
    , o.title
    , o.keywords
    , o.pageDescription
FROM
    t_LandingPage l 
    inner join t_Object o on l.OID = o.OID and o.objectID = @id    
";
            var item = sql.ExecSql(new { id }).AsEnumerable()
                .Select(r => new CatalogMenu
                {
                    NodeId = r.Field<int>("objectID"),
                    OID = r.Field<Guid>("commodityGroup"),
                    Name = r.Field<string>("name"),
                    H1 = r.Field<string>("h1"),
                    Title = r.Field<string>("title"),
                    Keywords = r.Field<string>("keywords"),
                    PageDescription = r.Field<string>("pageDescription"),
                    StringRepresentation = r.Field<string>("shortLink"),
                    ChildMenu = new[] { new CatalogMenu { NodeId = r.Field<int>("objectID"), Name = r.Field<string>("name") } }
                }).DefaultIfEmpty(new CatalogMenu()).Single();
            return item;
        }

        public IList<GoodsItem> GetGoodsList(
            int shopType,
            int objectId,
            int skip,
            int take,
            string sort,
            List<Guid> manufs,
            IList<CustomFilter> filters,
            out int total)
        {
            var goods = GetAllFilteredGoodsByLandingPage(shopType, objectId, manufs, filters);
            total = goods.Count;

            return TrimResult(goods, skip, take, sort);
        }

        [Cache]
        public IList<CatalogMenu> GetParentsMenu(int objectId)
        {
            const string sql = @"
select DISTINCT 
    n2.nodeID
	, n2.nodeName
	, n2.parentNode
    , n2.lft
    , o1.shortLink
	, countChild = (n2.rgt - n2.lft - 1)/2
from 
	t_Nodes n2
    inner join t_Object o1 on n2.object = o1.OID
	inner join t_Nodes n on n.tree = n2.tree and n2.lft BETWEEN n.lft and n.rgt and n.nodeID = 22427
	inner join (
	SELECT minValue = MIN(n.lft), maxValue = MAX(n.lft)
	FROM t_Goods g
		inner join t_CommodityActualGoods cag on g.OID = cag.goodsOID
        inner join t_LandingPage l on l.commodityGroup = cag.OID
        inner join t_Object o on l.OID = o.OID and o.objectID = @objectId
		inner join t_ThemeMasters tm on g.category = tm.masterOID
		inner join t_Theme t on tm.OID = t.OID
		inner join t_Nodes n on n.object = t.OID
		inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = 22427
	) tmp on n2.lft <= tmp.minValue and n2.rgt >= tmp.maxValue
order by
	n2.lft

SELECT
    l.name
FROM t_LandingPage l
    inner join t_Object o on l.OID = o.OID and o.objectID = @objectId
";
            var data = sql.ExecMSql(new { objectId });
            var menus = data[0].AsEnumerable()
                .Select(r => new CatalogMenu
                {
                    NodeId = r.Field<int>("nodeID"),
                    ParentNodeId = r.Field<int>("parentNode"),
                    StringRepresentation = r.Field<string>("shortLink")?.ToLower(),
                    Name = r.Field<string>("nodeName"),
                    HasChild = r.Field<int>("countChild") != 0
                }).ToList();

            menus.Add(data[1].AsEnumerable().Select(r => new CatalogMenu
            {
                Name = r.Field<string>("name")
            }).SingleOrDefault());
            return menus;
        }

        [Cache]
        public IList<Guid> GetPageManufacturers(int objectId)
        {
            const string sql = @"
SELECT DISTINCT
    manufacturerOID
FROM
    t_CommodityManufThemes cmt
    inner join t_LandingPage l on l.commodityGroup = cmt.OID
    inner join t_Object o on l.OID = o.OID and o.objectID = @objectId
WHERE
    manufacturerOID is not null
";
            return sql.ExecSql(new { objectId }).AsEnumerable()
                .Select(r => r.Field<Guid>("manufacturerOID")).ToList();
        }

        [Cache]
        public IList<int> GetPageFilters(int objectId)
        {
            const string sql = @"
SELECT 
    id
FROM
    t_CommodityThemeParams ctp
    inner join t_LandingPage l on l.commodityGroup = ctp.OID
    inner join t_Object o on l.OID = o.OID and o.objectID = @objectId
";
            return sql.ExecSql(new { objectId }).AsEnumerable()
                .Select(r => r.Field<int>("id")).ToList();
        }

        [Cache]
        public string GetLandingUrl(int objectId)
        {
            var menu = GetParentsMenu(objectId).Last(m => !string.IsNullOrEmpty(m.StringRepresentation));
            return $"/catalog/{menu.StringRepresentation}";
        }

        [Cache]
        public Article GetSeoArticle(int objectId, Action<Article> process)
        {
            const string sql = @"
SELECT article
FROM
	t_LandingPage l
    inner join t_Object o on l.OID = o.OID and o.objectID = @objectId
";
            var OID = sql.ExecSql(new { objectId }).AsEnumerable().Select(r => r.Field<Guid?>("article")).DefaultIfEmpty(Guid.Empty).Single();
            return _articleRepository.GetArticle(OID ?? Guid.Empty, process);
        }

        private IList<GoodsItem> GetAllFilteredGoodsByLandingPage(
            int shopType,
            int objectId,
            IList<Guid> manufs,
            IList<CustomFilter> filters)
        {
            IList<GoodsItem> goods = GetAllGoodsByLandingPage(shopType, objectId);
            IEnumerable<GoodsItem> result = goods;

            if (manufs != null && manufs.Count > 0)
                result = result.Where(k => k.Manufacturer.HasValue && manufs.Contains(k.Manufacturer.Value)).ToList();

            if (filters != null && filters.Count != 0)
                result = result.Where(k => filters.Count(k1 => k1.HasGoods(k)) == filters.Count);

            return result.ToList();
        }

        [Cache]
        private IList<GoodsItem> GetAllGoodsByLandingPage(int shopType, int objectId)
        {
            const string sql = @"
SELECT DISTINCT
	g.OID
	, o.objectID
	, g.model
	, g.pseudoID
	, g.productType
    , g.manufacturer
    , c.companyName
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, gp.price
    , gp.oldPrice
	, gp.deliveryPrice
    , gs.inStock
    , gs.stockAmount
    , isHit = ISNULL(gh.isHit, 0)
	, hitWeight = ISNULL(gh.popularity, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, o.dateCreate
FROM
    t_LandingPage l
    inner join t_Object o1 on l.OID = o1.OID and o1.objectID = @objectId
	inner join t_CommodityActualGoods cag on l.commodityGroup = cag.OID
	inner join t_Goods g on g.OID = cag.goodsOID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	inner join t_Object o on g.OID = o.OID
	inner join t_Company c on g.manufacturer = c.OID
	outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
UNION ALL
SELECT DISTINCT
	g.OID
	, o.objectID
	, g.model
	, g.pseudoID
	, g.productType
    , g.manufacturer
    , c.companyName
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, gp.price
    , gp.oldPrice
	, gp.deliveryPrice
    , gs.inStock
    , gs.stockAmount
    , isHit = ISNULL(gh.isHit, 0)
	, hitWeight = ISNULL(gh.popularity, 0)
	, isNew = ISNULL(gn.isNew, 0)
	, o.dateCreate
FROM
    t_LandingPage l
    inner join t_Object o1 on l.OID = o1.OID and o1.objectID = @objectId
	inner join t_CommodityOfflineGoods cog on l.commodityGroup = cog.OID
	inner join t_Goods g on g.OID = cog.goodsOID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
	left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsIsNewes gn on g.OID = gn.OID and gn.shopType = @shopType
	inner join t_Object o on g.OID = o.OID
	inner join t_Company c on g.manufacturer = c.OID
	outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_LandingPage l
    inner join t_Object o1 on l.OID = o1.OID and o1.objectID = @objectId
	inner join t_CommodityActualGoods cag on l.commodityGroup = cag.OID
	inner join t_Goods g on g.OID = cag.goodsOID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
    inner join t_ThemeMasters tm on tm.masterOID = g.category
	inner join t_TemplateParams tp on tp.OID = tm.OID And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID 
UNION ALL
SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_LandingPage l
    inner join t_Object o1 on l.OID = o1.OID and o1.objectID = @objectId
	inner join t_CommodityOfflineGoods cog on l.commodityGroup = cog.OID
	inner join t_Goods g on g.OID = cog.goodsOID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price > 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType
    inner join t_ThemeMasters tm on tm.masterOID = g.category
	inner join t_TemplateParams tp on tp.OID = tm.OID And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID 
  
";
            var prms = new Hashtable { { "objectId", objectId }, { "shopType", shopType } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            var list = (from row in ds.Table.AsEnumerable()
                        select new GoodsItem
                        {
                            OID = row.Field<Guid>("OID"),
                            ObjectID = row.Field<int>("objectID"),
                            ProductType = row.Field<string>("productType"),
                            Model = row.Field<string>("model"),
                            PseudoID = row.Field<string>("pseudoID"),
                            Manufacturer = row.Field<Guid?>("manufacturer"),
                            ManufacturerName = row.Field<string>("companyName"),
                            Image = new Image
                            {
                                Id = row.Field<int?>("imageID") ?? GoodsRepository.BlankImageSmallId,
                                OID = row.Field<Guid?>("image") ?? GoodsRepository.BlankImageSmall,
                                Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                                Width = row.Field<int?>("imageWidth") ?? GoodsRepository.BlankImageSmallWidth,
                                Height = row.Field<int?>("imageHeight") ?? GoodsRepository.BlankImageSmallHeight,
                                MimeType = row.Field<string>("mimeType") ?? GoodsRepository.BlankImageSmallMimeType
                            },
                            Price = row.Field<decimal>("price"),
                            OldPrice = row.Field<decimal>("oldPrice"),
                            InStock = row.Field<bool>("inStock"),
                            StockAmount = row.Field<int>("stockAmount"),
                            IsHit = row.Field<bool>("isHit"),
                            HitWeight = row.Field<int>("hitWeight"),
                            IsNew = row.Field<bool>("isNew"),
                            DateCreate = row.Field<DateTime>("dateCreate"),
                            Delivery = new DeliveryInfo { Price = row.Field<decimal?>("deliveryPrice") ?? 0 },
                            GoodsParams = ds.Tables[1].AsEnumerable()
                                .Where(k => k.Field<Guid>("OID") == row.Field<Guid>("OID") && k.Field<Guid>("paramTypeOID") != Settings.Default.ShortDescriptionParam)
                                .OrderBy(k => k.Field<int>("ordValue"))
                                .Select(k => new ParamItem
                                {
                                    ParamType = k.Field<Guid>("paramTypeOID"),
                                    ParamName = k.Field<string>("name"),
                                    ParamValue = k.Field<string>("value"),
                                    IsFilter = k.Field<bool>("isFilter"),
                                    IsFullName = k.Field<bool>("isFullName"),
                                    Unit = k.Field<string>("unit")
                                }).ToList()
                        }).ToList();
            return list;
        }

        private static IList<GoodsItem> TrimResult(IEnumerable<GoodsItem> items, int skip, int take, string sort)
        {
            var result = items;
            switch (sort)
            {
                case null:
                case "pop": result = result.OrderByDescending(v => v.InStock).ThenByDescending(v => v.HitWeight).ThenByDescending(v => v.StockAmount).ThenBy(v => v.Price); break;
                case "up":
                    result = result.OrderByDescending(v => v.InStock).ThenBy(v => v.Price).ThenByDescending(v => v.StockAmount);
                    break;
                case "down": result = result.OrderByDescending(v => v.InStock).ThenByDescending(v => v.Price).ThenByDescending(v => v.StockAmount); break;
            }

            return take > 0 ? result.Skip(skip).Take(take).ToList() : result.ToList();
        }
    }
}