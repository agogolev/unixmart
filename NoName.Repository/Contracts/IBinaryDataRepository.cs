﻿using System.IO;

namespace NoName.Repository.Contracts
{
    public interface IBinaryDataRepository
    {
        byte[] GetImageById(int id, out string mimeType);
    }
}