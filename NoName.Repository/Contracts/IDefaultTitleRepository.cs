﻿using System;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface IDefaultTitleRepository
    {
        TitleData LoadTitle(int defaultType, Guid categoryOID, int rootNode);
        TitleData LoadTitle(int defaultType, string shortLink, int rootNode);
    }
}