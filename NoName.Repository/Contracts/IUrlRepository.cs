﻿namespace NoName.Repository.Contracts
{
    public interface IUrlRepository
    {
        string CanonicalHomeUrl();
        string CanonicalCatShortLinkUrl(string categoryId, string manufId, int rootNode);
        string CanonicalGoodsUrl(int goodsId);
    }
}