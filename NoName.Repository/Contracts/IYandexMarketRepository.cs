using System;
using System.Web;
using Ecommerce.Domain;

namespace NoName.Repository.Contracts
{
	public interface IYandexMarketRepository
	{
        void GetMarketData(HttpResponseBase response, int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup, bool hasStore, bool second);

        void GetMarketDataRise(HttpResponseBase response, int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup, bool hasStore);
        void RefreshMarketData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup, bool hasStore);

	    void RefreshAdmitadData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup);

		void RefreshGoogleData(int shopType, string siteName, string siteUrl, int rootNode);

		void RefreshWikimartData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup);

		void RefreshWikiData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup);

		void RefreshNadaviData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup);

		void RefreshUtinetData(int shopType, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup);

		void RefreshCommodityGroupData(Guid OID, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup);

		void CreateCommodityFile(string fileName, Guid OID, string siteName, string siteTitle, string siteUrl, int rootNode, bool hasPickup, CommodityGroupOption option);

	    void RegisterMarket(string marketUrl, string fullUrl);
	}
}