using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface IDeliveryCalcService
    {
        IList<DeliveryInfoItem> GetDeliveries(IEnumerable<DeliveryInfoItem> infoItems, DeliveryInfo delivery, IList<BasketContent> basketContent);
        decimal CalcDeliveryPrice(IList<BasketContent> items);
        decimal SelfDeliveryPrice(IList<BasketContent> items, decimal priceSelf);
    }
}