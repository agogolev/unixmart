﻿using System;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface IBasketRepository
	{
        void ModifyGoodsInBasket(Basket basket, Guid goodsOID, int count, bool relative, bool isSale);
		Basket GetBasketBySession(int shopType, int rootNode, Guid sessionOID);
	    IList<GoodsItem> GetBasketAccessory(int shopType, int rootNode, Guid sessionOID);
	    void SaveDeliveryType(Guid basket, int deliveryType, int calcDistance);
        void SavePayType(Guid basket, int payType);
	    void SavePromoCode(Guid basket, string promoCode);
        //void SaveContacts(Guid basket, string name, string phone, string email);
        //void SaveAddress(Guid basket, string city, string streetName, string house, string korp, string build, string flat, string metroName);
    }
}
