﻿using System;
using System.Collections.Generic;
using Ecommerce.Domain;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface ILandingPageRepository
    {
        CatalogMenu GetItem(int id);
        IList<GoodsItem> GetGoodsList(
            int shopType,
            int objectId,
            int skip,
            int take,
            string sort,
            List<Guid> manufs,
            IList<CustomFilter> filters,
            out int total);
        IList<CatalogMenu> GetParentsMenu(int objectId);
        IList<Guid> GetPageManufacturers(int objectId);
        IList<int> GetPageFilters(int objectId);
        string GetLandingUrl(int objectId);
        Article GetSeoArticle(int objectId, Action<Article> process);
    }
}