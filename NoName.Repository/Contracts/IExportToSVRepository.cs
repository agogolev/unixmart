using System.Collections;
using MetaData;

namespace NoName.Repository.Contracts
{
	public interface IExportToSVRepository
	{
		string GetStockInfo(int stockAmount, int shopAmount, int pathAmount, int centralStockAmount, string login, string password);
		DataSetISM GetDataSetISM(string sql, Hashtable prms, bool IsStoredProc, string login, string password);
	}
}