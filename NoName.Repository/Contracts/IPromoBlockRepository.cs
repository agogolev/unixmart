using System;
using System.Collections.Generic;
using System.Web;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface IPromoBlockRepository
	{
		IList<Domain.PromoBlock> GetPromoList(int parentNode, Action<PromoBlock> process);

		IList<IList<Domain.PromoBlock>> GetPromoMultiList(int mainPromoBlock);

		Domain.PromoBlock GetPromo(int id);

		HtmlString GetSliderContent(int nodeID);
	}
}