using System;
using System.Collections.Generic;
using NoName.Domain;
using MetaData;
using SelectListItem = System.Web.Mvc.SelectListItem;

namespace NoName.Repository.Contracts
{
    public interface IOrderRepository
    {
        IList<PayInfoItem> GetPayTypeLookup(int shopType);
        Order GetOrder(Guid orderOID, int rootNode);
        Order GetPreOrder(Guid orderOID);
        Order GetPreOrder(int preorderNum);
        Order GetPreOrder(string paymentNumber);
        void SavePaymentNumber(Guid orderOID, string paymentNumber);
        IList<BasketContent> GetOrderContent(BasketContent[] content, int shopType);
        Guid SaveOrder(Basket basket, int shopType, int rootNode, int defaultDeliveryType);
        Guid SavePreOrder(Basket basket, int shopType, int rootNode, int defaultDeliveryType);
        Guid SaveOrderFromPreOrder(Guid OID, Guid basketOID, string approvalCode);
        int CardPayment { get; }
        IList<Order> ExportOrders();
    }
}