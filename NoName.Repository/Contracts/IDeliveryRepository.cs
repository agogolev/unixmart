﻿using System;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface IDeliveryRepository
	{
		decimal ExpressDelivery { get; }
	    int[] SelfDeliveries { get; }
        IList<DeliveryInfoItem> GetDeliveries(int shopType, DeliveryInfo delivery, IList<BasketContent> basketContents, bool instantDeliveryPossible);
		decimal CalcDeliveryPrice(IList<BasketContent> items);
	    decimal SelfDeliveryPrice(IList<BasketContent> items, decimal unitPrice);

        decimal GetAddPrice(int? deliveryType);
	    decimal GetDistancePrice(int? deliveryType, int calcDistance);
        bool DetermineInstantDeliveryPossibility(DateTime date);
	}
}
