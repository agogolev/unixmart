using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Ecommerce.Domain;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface IGoodsRepository
    {
        IList<GoodsItem> GetAllGoodsByCategory(int shopType, int selectedNode);
        IList<Tag> GetCategoryTags(int selectedNode);
        IList<Tag> GetLandingPageTags(int objectID);
        IList<GoodsItem> GetGoodsByCategory(int shopType, int selectedNode, int skip, int take, string sort,
            IList<Guid> manufs, IList<CustomFilter> filters, out int total);
        IList<GoodsItem> GetHits(int shopType, int rootNode);
        IList<GoodsItem> GetNew(int shopType, int rootNode);
        IList<GoodsItem> GetSpecial(int shopType, int rootNode);
        IList<GoodsItem> GetSputnikGoods(int shopType, int rootNode, int selectedNode, int objectID, decimal price, int maxValue);
        IList<GoodsItem> GetSimilarGoods(int shopType, int rootNode, int selectedNode, int objectID, decimal price, int minValue, int maxValue);
        IList<GoodsItem> GetAccessoryGoods(int objectId, int shopType);
        IList<Company> GetManufacturers(int shopType, int selectedNode);
        IList<PriceIntervalMenu> GetGoodsPriceIntervals(int shopType, int selectedNode);
        IList<CustomFilter> GetFilters(int shopType, int id);
        IList<CustomFilter> GetCurrentFilter(int shopType, int id, NameValueCollection prms);
        GoodsItem GetGoodsItem(int objectID, int shopType, int rootNode);
        int? GetFirstGoodsCategory(int oid, int catalogRoot);
        int GetGoodsCategoryId(int objectId, int catalogRoot);
        IDictionary<int, string> GetGoodsUrl(int shopType, int[] objectIDs);
        string GetGoodsPath(int objectID);
    }
}