using System;
using Ecommerce.Domain;

namespace NoName.Repository.Contracts
{
	public interface IBannerRepository
	{
		AdvertPlace GetBanners(int clientId, int placeId, Guid sessionId, int regionId, Guid? oid, int rootNode);
		string GetBannerUrl(int id);
		void RegisterClick(int id, Guid showKey);
	}
}