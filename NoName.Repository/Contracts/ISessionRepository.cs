﻿using System;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface ISessionRepository
	{
		/// <summary>
		/// номер последнего заказа
		/// </summary>
		Guid LastOrderOID { get; set; }

        int TotalFinishCount { get; set; }

		/// <summary>
		/// ID сессии пользователя
		/// </summary>
		Guid AdvertSessionId { get; }

		bool InstantDeliveryPossible { get; set; }

		string UserHostAddress { get; set; }

        Basket Basket { get; set; }

		/// <summary>
		/// последнее выбранное кол-во на странице
		/// </summary>
		int PreferedNumOnBatch { get; set; }

		/// <summary>
		/// последняя выбранная сортировка
		/// </summary>
		string PreferedSort { get; set; }

		/// <summary>
		/// Идентификатор фильтруемого в данный момент каталога
		/// </summary>
		int FilterCatalogId { get; set; }

		/// <summary>
		/// выбранная строка поиска
		/// </summary>
		string SearchQuery { get; set; }

        string RefererLabel { get; set; }

		/// <summary>
		/// результаты поиска по товарам
		/// </summary>
		IList<SearchedGoods> SearchResult { get; set; }

		int RegionId { get; set; }

		void RegisterSession();
	}
}