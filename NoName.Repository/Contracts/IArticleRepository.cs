using System;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface IArticleRepository
    {
        Article GetArticle(int id, Action<Article> process);
        Article GetArticle(string shortLink, Action<Article> process);
        Article GetArticle(Guid id, Action<Article> process);
        Article GetNodeArticle(int id, Action<Article> process);
        IList<Article> GetAllArticles(int articleType);
        IList<Article> GetLastNewsArticles(int count = 4);
        IList<Article> GetLinkedArticles(int parentNodeId);
        IList<int> GetArticleThemes(Guid articleOID, int parentNode);
        IList<Article> GetAllThemeArticles(int id);
        IList<Article> GetArticles(int articleType, int skip, int take, string sort, out int total);
        IList<Article> GetThemeArticles(int id, int skip, int take, out int total);
        IList<Comment> GetComments(Guid oid);
        void AddComment(string userName, string new_com, Guid oid);
        Article Prepare4Mail(Article article, string siteUrl);
        Article GetSeoArticle(int categoryId, Guid? manufacturerOID, Action<Article> process);
    }
}