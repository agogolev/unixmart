﻿using System;

namespace NoName.Repository.Contracts
{
	public interface ICookieRepository
	{
        Guid SessionId { get; set; }
        string ActionpayId { get; set; }
    }
}