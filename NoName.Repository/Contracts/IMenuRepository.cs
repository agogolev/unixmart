using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface IMenuRepository
    {
        IList<NodeMenu> GetNodeMenu(int parentNode);
        int? FindNodeByObject(int rootNode, string shortLink);
        int GetParentNode(int id);
        IList<InfoMenu> GetInfoMenu(int infoRoot);
        IList<CatalogMenu> GetCatalogMenu(int rootNode, int shopType);
        IList<CatalogMenu> GetCatalogMenuFlat(int rootNode, int shopType);
        CatalogMenu GetCatalogSubMenu(int rootNode, int shopType);
        CatalogMenu GetManufacturerSubMenu(int rootNode, int shopType, int manufId, string manufRepresentation);
        IList<CatalogMenu> GetCategoryPath(int rootNode, int id, int shopType);
        IList<CatalogMenu> GetManufCategoryPath(int rootNode, int id, int shopType, string manufName);
        IList<CatalogMenu> GetManufacturerPath(int rootNode, int id, int shopType, int manufId, string manufRepresentation, string manufName);
        CatalogMenu GetCategoryItem(int rootNode, int id, int shopType);
        string GetCategoryUrl(string siteUrl, string shortLink, int rootNode);
        string GetShortCategoryUrl(string shortLink, int rootNode);
        string GetShortManufUrl(string caregoryId, string manufId, int rootNode);
        IList<NodeMenu> GetShopMenu(int shopType);
        CatalogMenu FromShortName(string item, int rootNode);
        string GetShortLink(int objectId);
    }
}