﻿using System.Xml.Linq;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface ISiteMapRepository
	{
		void RefreshSiteMap(string siteUrl, int shopType, int catalogRoot, int infoRoot);
	}
}
