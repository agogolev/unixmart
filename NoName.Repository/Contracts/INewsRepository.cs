using System;
using System.Collections.Generic;

namespace NoName.Repository.Contracts
{
	public interface INewsRepository
	{
		IList<Domain.Article> GetAllNewsArticle();
		IList<Domain.Article> GetNewsArticleList(int skip, int take, out int total);

	}
}