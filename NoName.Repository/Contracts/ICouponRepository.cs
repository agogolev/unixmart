﻿using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface ICouponRepository
    {
        decimal GoodsPrice(GoodsItem goods, Coupon coupon);
        //void UpdateBasketCoupon(Basket basket, string couponCode);
    }
}