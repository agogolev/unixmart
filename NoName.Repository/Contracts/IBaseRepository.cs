using System;

namespace NoName.Repository.Contracts
{
    public interface IBaseRepository
    {
        Guid? GetObject(int id);
        Guid? GetObject(string shortLink);
        int? GetObjectID(string shortLink);
        int? GetObjectID(Guid OID);
        Guid? GetNodeObject(int id);
    }
}