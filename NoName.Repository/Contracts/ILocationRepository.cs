namespace NoName.Repository.Contracts
{
	public interface ILocationRepository
	{
		Domain.Location GetLocation(int id);
	}
}