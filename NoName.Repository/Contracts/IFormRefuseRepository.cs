﻿using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface IFormRefuseRepository
    {
        void SaveFormRefuse(FormRefuse form);
    }
}