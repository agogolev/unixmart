﻿using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface IMailRepository
	{
	    /// <summary>
	    /// Отправка сообщения о совершении заказа
	    /// </summary>
	    /// <param name="order"></param>
	    /// <param name="orderMail"></param>
	    /// <param name="siteUrl"></param>
	    /// <param name="clientMailBody"></param>
	    void SendOrderInfo(Domain.Order order, string orderMail, string siteUrl, string clientMailBody);

	    /// <summary>
	    /// Запрос на звонок клиенту
	    /// </summary>
	    /// <param name="phoneNumber"></param>
	    /// <param name="clientName"></param>
	    /// <param name="requestMail"></param>
	    void RequestOperator(string phoneNumber, string requestMail);

        void ReportAvailable(Domain.GoodsItem goods, string phone, string email, string requestEmail);
        void SendFormRefuse(FormRefuse model, string requestMail);
    }
}
