﻿using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
	public interface ISearchRepository
	{
		IList<SearchedGoods> SearchResult(int shopType, int rootNode, string query);

        IList<GoodsItem> SearchFilteredResult(IList<SearchedGoods> items, int skip, int take, string sort, int catalogID, out int total);

		IList<CatalogMenu> SearchResultCatalogs(IList<SearchedGoods> items);
	}
}
