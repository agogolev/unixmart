﻿using System;
using System.Collections.Generic;
using NoName.Domain;

namespace NoName.Repository.Contracts
{
    public interface ICompanyMenuRepository
    {
        IList<Domain.DirectoryItem> GetCompanyMenuByABC(int shopType, int rootNode);
        IList<CatalogMenu> GetCompanyMenuFlat(int rootNode, int shopType);
        Company GetCompany(string shortLink);
        void GetCompanyCategoryTitle(int nodeID, Guid companyOID, out string title, out string keywords, out string pageDescription);
    }
}