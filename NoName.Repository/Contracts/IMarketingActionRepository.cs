using System;
using System.Collections.Generic;

namespace NoName.Repository.Contracts
{
	public interface IMarketingActionRepository
	{
		//IList<Domain.MarketingAction> GetActionList();
		Domain.MarketingAction GetActionById(int id, int rootNode, int shopType);
        IList<Domain.MarketingAction> GetActions(Guid sessionId, int regionId);
	    IList<Domain.MarketingAction> ListActions();
	}
}