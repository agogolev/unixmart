using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using Ecommerce.Extensions;
using MetaData;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class MenuRepository : IMenuRepository
    {
        private readonly ICompanyMenuRepository _companyMenuRepository;

        public MenuRepository(ICompanyMenuRepository companyMenuRepository)
        {
            _companyMenuRepository = companyMenuRepository;
        }

        [Cache]
        public IList<NodeMenu> GetNodeMenu(int parentNode)
        {
            const string sql = @"
SELECT
	n.nodeID,
	n.nodeName,
	n.nodeUrl,
    n.attr
FROM 
	t_Nodes n
WHERE n.parentNode = @parentNode
ORDER BY n.lft
";
            return sql.ExecSql(new { parentNode }).Select(row => new NodeMenu
            {
                Name = row.Field<string>("nodeName"),
                Url = row.Field<string>("nodeUrl"),
                Attr = row.Field<string>("attr"),
                Id = row.Field<int>("nodeID")
            }).ToList();
        }

        public int? FindNodeByObject(int rootNode, string shortLink)
        {
            const string sql = @"
SELECT TOP 1
	n.nodeID
FROM 
	t_Nodes n
    inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
    inner join t_Object o on n.object = o.OID and o.shortLink = @shortLink
ORDER BY n.lft
";
            return sql.ExecSql(new { rootNode, shortLink }).Select(row => row.Field<int?>("nodeID")).SingleOrDefault();
        }

        [Cache]
        public int GetParentNode(int id)
        {
            const string sql = @"
SELECT
    parentNode 
FROM 
    t_Nodes 
WHERE
    nodeId = @id
";
            return sql.ExecSql(new { id }).Select(row => row.Field<int>("parentNode")).SingleOrDefault();
        }

        [Cache]
        public IList<InfoMenu> GetInfoMenu(int infoRoot)
        {
            string sql = @"   
SELECT
	n.nodeID,
	n.nodeName,
	a.header,
	n.nodeUrl,
	n.lft,
	n.parentNode,
	o.objectId,
    o.shortLink,
    o.dateModify
FROM
	t_Nodes n
	inner join t_Object o 
		inner join t_Article a 
			on a.OID = o.OID
			on n.object = o.OID 
	inner join t_Nodes n1 on (n.lft > n1.lft And n.lft < n1.rgt) and n.tree = n1.tree and n.tree = n1.tree and n1.nodeID = @infoRoot
";
            IEnumerable<DataRow> rows = sql.ExecSql(new { infoRoot }).ToList();

            return (from row in rows
                    where row.Field<int>("parentNode") == infoRoot
                    orderby row.Field<int>("lft")
                    select new InfoMenu
                    {
                        Id = row.Field<int>("nodeID"),
                        ParentId = row.Field<int>("parentNode"),
                        Name = row.Field<string>("nodeName"),
                        Header = row.Field<string>("header").ToLower().TranslitEncode(true),
                        ObjectId = row.Field<int?>("objectID"),
                        Url = !string.IsNullOrWhiteSpace(row.Field<string>("nodeUrl"))
                            ? row.Field<string>("nodeUrl")
                            : $"/info/{row.Field<string>("shortLink").ToLower()}",
                        DateModify = row.Field<DateTime>("DateModify")
                    }).ToList();
        }

        [Cache]
        public IList<CatalogMenu> GetCatalogMenu(int rootNode, int shopType)
        {
            const string sql = @"
SELECT
	n.nodeID
	, n.nodeName
	, n.parentNode
    , n.lft
    , o.shortLink
    , image = ISNULL(bd.OID, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb')
	, imageID = ISNULL(obj.objectID, 37634)
	, imageWidth = ISNULL(bd.width, 133)
	, imageHeight = ISNULL(bd.height, 125)
    , mimeType = ISNULL(bd.mimeType, 'image/png')
	, countChild = (n.rgt - n.lft - 1)/2
	, t.goodsCount
	, l.level
    , t.css1
    , t.css2
    , t.themeName
    , t.altName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID
        left join t_ObjectImage oi  
		inner join t_BinaryData bd 
			on oi.binData = bd.OID and ordValue = 'для списка'
			on t.OID = oi.OID	
        left join t_Object obj
			on oi.binData = obj.OID
    inner join t_Object o on t.OId = o.OID
    cross apply (SELECT Count(*) - 1 
		FROM t_Nodes nn inner join t_Nodes nn1 on nn1.tree = nn.tree And nn.lft BETWEEN nn1.lft 
			And nn1.rgt and nn1.nodeID = @rootNode
		WHERE n.tree = nn.tree And n.lft BETWEEN nn.lft And nn.rgt
	) l(level)
WHERE
	t.goodsCount > 0 
ORDER BY
	n.lft
";
            var prms = new Hashtable();   
            prms["rootNode"] = rootNode;
            prms["shopType"] = shopType;
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            List<DataRow> rows = ds.Table.AsEnumerable().ToList();
            List<CatalogMenu> items = (rows.Where(row => row.Field<int>("parentNode") == rootNode)
                .OrderBy(row => row.Field<int>("lft"))
                .Select(row => new CatalogMenu
                {
                    NodeId = row.Field<int>("nodeID"),
                    ParentNodeId = row.Field<int>("parentNode"),
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("altName"),
                    HasChild = row.Field<int>("countChild") != 0,
                    Level = row.Field<int>("level"),
                    GoodsCount = row.Field<int>("goodsCount"),
                    Css1 = row.Field<string>("css1"),
                    Css2 = row.Field<string>("css2"),
                })
                ).ToList();
            foreach (CatalogMenu item in items)
            {
                item.ChildMenu = (rows.Where(row => row.Field<int>("parentNode") == item.NodeId)
                    .OrderBy(row => row.Field<int>("lft"))
                    .Select(row => new CatalogMenu
                    {
                        NodeId = row.Field<int>("nodeID"),
                        ParentNodeId = row.Field<int>("parentNode"),
                        StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                        Name = row.Field<string>("altName"),
                        Image = new Image
                        {
                            Id = row.Field<int>("imageID"),
                            OID = row.Field<Guid>("image"),
                            Name = row.Field<string>("altName").TranslitEncode(true).ToLower(),
                            Width = row.Field<int>("imageWidth"),
                            Height = row.Field<int>("imageHeight"),
                            MimeType = row.Field<string>("mimeType")
                        },
                        HasChild = row.Field<int>("countChild") != 0,
                        Level = row.Field<int>("level"),
                        GoodsCount = row.Field<int>("goodsCount")
                    })
                    ).ToList();
            }
            return items;
        }

        [Cache]
        public CatalogMenu GetCatalogSubMenu(int rootNode, int shopType)
        {
            const string sql = @"
SELECT
	n.nodeID
	, n.nodeName
	, n.parentNode, 0
	, n.lft
	, countChild = (n.rgt - n.lft - 1)/2
    , image = ISNULL(bd.OID, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb')
	, imageID = ISNULL(obj.objectID, 37634)
	, imageWidth = ISNULL(bd.width, 133)
	, imageHeight = ISNULL(bd.height, 125)
    , mimeType = ISNULL(bd.mimeType, 'image/png')
	, t.goodsCount
	, o.title
    , t.h1
	, o.keywords
	, o.pageDescription
    , o.shortLink
    , t.themeName
    , t.altName
FROM
	t_Nodes n
	inner join t_Theme t on n.object = t.OID
		left join t_ObjectImage oi  
		inner join t_BinaryData bd 
			on oi.binData = bd.OID
			on t.OID = oi.OID and ordValue = 'для списка'	
        left join t_Object obj
			on oi.binData = obj.OID
    inner join t_Object o on n.object = o.OID				
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
--WHERE
--	t.goodsCount > 0  
ORDER BY
	n.lft
";
            var prms = new Hashtable();
            prms["rootNode"] = rootNode;
            prms["shopType"] = shopType;
            int ordValue = 0;
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            List<DataRow> rows = ds.Table.AsEnumerable().ToList();
            CatalogMenu item = (rows.Where(row => row.Field<int>("nodeID") == rootNode)
                .OrderBy(row => row.Field<int>("lft"))
                .Select(row => new CatalogMenu
                {
                    NodeId = row.Field<int>("nodeID"),
                    ParentNodeId = row.Field<int>("parentNode"),
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("altName"),
                    GoodsCount = row.Field<int>("goodsCount"),
                    HasChild = row.Field<int>("countChild") > 0,
                    Image = new Image
                    {
                        Id = row.Field<int>("imageID"),
                        OID = row.Field<Guid>("image"),
                        Name = row.Field<string>("altName").TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("imageWidth"),
                        Height = row.Field<int>("imageHeight"),
                        MimeType = row.Field<string>("mimeType")
                    },
                    Title = row.Field<string>("title"),
                    H1 = row.Field<string>("h1"),
                    Keywords = row.Field<string>("keywords"),
                    PageDescription = row.Field<string>("pageDescription"),
                })
                .DefaultIfEmpty(new CatalogMenu
                {
                    NodeId = rootNode,
                    ParentNodeId = 0
                })
                ).Single();
            item.ChildMenu = (rows.Where(row => row.Field<int>("parentNode") == item.NodeId && row.Field<int>("goodsCount") > 0)
                .OrderBy(row => row.Field<int>("lft"))
                .Select(row => new CatalogMenu
                {
                    OrdValue = ++ordValue,
                    NodeId = row.Field<int>("nodeID"),
                    ParentNodeId = row.Field<int>("parentNode"),
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("altName"),
                    GoodsCount = row.Field<int>("goodsCount"),
                    HasChild = row.Field<int>("countChild") > 0,
                    Image = new Image
                    {
                        Id = row.Field<int>("imageID"),
                        OID = row.Field<Guid>("image"),
                        Name = row.Field<string>("altName").TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("imageWidth"),
                        Height = row.Field<int>("imageHeight"),
                        MimeType = row.Field<string>("mimeType")
                    }
                })
                ).ToList();

            return item;
        }

        [Cache]
        public IList<CatalogMenu> GetCatalogMenuFlat(int rootNode, int shopType)
        {
            const string sql = @"
SELECT
	n.nodeID
	, n.nodeName
	, n.parentNode
    , n.lft
    , o.shortLink
	, countChild = (n.rgt - n.lft - 1)/2
	, t.goodsList
	, t.goodsCount
	, l.level
    , t.themeName
    , t.altName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @rootNode
	inner join t_Theme t on n.object = t.OID
    inner join t_Object o on t.OId = o.OID
    cross apply (SELECT Count(*) - 1 
		FROM t_Nodes nn inner join t_Nodes nn1 on nn1.tree = nn.tree And nn.lft BETWEEN nn1.lft 
			And nn1.rgt and nn1.nodeID = @rootNode
		WHERE n.tree = nn.tree And n.lft BETWEEN nn.lft And nn.rgt
	) l(level)
WHERE
	t.goodsCount > 0 
ORDER BY
	n.lft
";
            var prms = new Hashtable();
            prms["rootNode"] = rootNode;
            prms["shopType"] = shopType;
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            List<DataRow> rows = ds.Table.AsEnumerable().ToList();
            List<CatalogMenu> items = rows
                .Select(row => new CatalogMenu
                {
                    NodeId = row.Field<int>("nodeID"),
                    ParentNodeId = row.Field<int>("parentNode"),
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("themeName"),
                    H1 = row.Field<string>("altName"),
                    HasChild = row.Field<int>("countChild") != 0,
                    GoodsList = row.Field<bool>("goodsList"),
                    Level = row.Field<int>("level"),
                    GoodsCount = row.Field<int>("goodsCount")
                }).ToList();
            return items;
        }

        [Cache]
        public CatalogMenu GetManufacturerSubMenu(int rootNode, int shopType, int manufId, string manufRepresentation)
        {
            const string sql = @"
SELECT
	n.nodeID
    , n.parentNode
	, n.nodeName
	, n.parentNode
	, n.lft
	, countChild = (n.rgt - n.lft - 1)/2
    , image = ISNULL(bd.OID, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb')
	, imageID = ISNULL(obj.objectID, 37634)
	, imageWidth = ISNULL(bd.width, 133)
	, imageHeight = ISNULL(bd.height, 125)
    , mimeType = ISNULL(bd.mimeType, 'image/png')
	, goodsCount = ISNULL(tmgc.goodsCount, 0)
	, o.title
	, o.keywords
	, o.pageDescription
    , o.shortLink
    , manufacturerOID = oc.OID
    , t.themeName
    , t.H1
    , t.altName
FROM
	t_Nodes n
	left join t_Theme t on n.object = t.OID
		left join t_ObjectImage oi  
		inner join t_BinaryData bd 
			on oi.binData = bd.OID
			on t.OID = oi.OID and ordValue = 'для списка'	
        left join t_Object obj
			on oi.binData = obj.OID
    left join t_Object o on n.object = o.OID				
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
    inner join t_Object oc on oc.objectID = @manufId
    left join t_ThemeManufGoodsCount tmgc on t.OID = tmgc.themeOID and oc.OID = tmgc.manufacturerOID
ORDER BY
	n.lft
";
            var prms = new Hashtable();
            prms["rootNode"] = rootNode;
            prms["shopType"] = shopType;
            prms["manufId"] = manufId;
            DataSetISM ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null); 
            List<DataRow> rows = ds.Table.AsEnumerable().ToList();
            CatalogMenu item = (rows.Where(row => row.Field<int>("nodeID") == rootNode)
                .OrderBy(row => row.Field<int>("lft"))
                .Select(row => new CatalogMenu
                {
                    SelectedBrand = new Company
                    {
                        Id = manufId,
                        ShortLink = manufRepresentation,
                        OID = row.Field<Guid>("manufacturerOID")
                    },
                    UseBrandUrl = rootNode == Settings.Default.CatalogRoot,
                    NodeId = row.Field<int>("nodeID"),
                    ParentNodeId = row.Field<int?>("parentNode") ?? 0,
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("themeName") ?? row.Field<string>("nodeName"),
                    H1 = row.Field<string>("H1") ?? row.Field<string>("altName") ?? row.Field<string>("nodeName"),
                    GoodsCount = row.Field<int>("goodsCount"),
                    Image = new Image
                    {
                        Id = row.Field<int>("imageID"),
                        OID = row.Field<Guid>("image"),
                        Name = (row.Field<string>("altName") ?? row.Field<string>("nodeName")).TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("imageWidth"),
                        Height = row.Field<int>("imageHeight"),
                        MimeType = row.Field<string>("mimeType")
                    },
                    Title = row.Field<string>("title"),
                    Keywords = row.Field<string>("keywords"),
                    PageDescription = row.Field<string>("pageDescription"),
                })
                ).Single();
            item.ChildMenu = (rows.Where(row => (row.Field<int?>("parentNode") ?? 0) == item.NodeId && row.Field<int>("goodsCount") > 0)
                .OrderBy(row => row.Field<int>("lft"))
                .Select(row => new CatalogMenu
                {
                    SelectedBrand = new Company
                    {
                        Id = manufId,
                        ShortLink = manufRepresentation,
                        OID = row.Field<Guid>("manufacturerOID")
                    },
                    UseBrandUrl = rootNode == Settings.Default.CatalogRoot,
                    NodeId = row.Field<int>("nodeID"),
                    ParentNodeId = row.Field<int>("parentNode"),
                    StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                    Name = row.Field<string>("themeName"),
                    H1 = row.Field<string>("H1") ?? row.Field<string>("altName") ?? row.Field<string>("nodeName"),
                    GoodsCount = row.Field<int>("goodsCount"),
                    //SimpleRepresentation = !row.Field<int?>("parentNode").HasValue || row.Field<int?>("parentNode").Value == Settings.Default.CatalogRoot,
                    Image = new Image
                    {
                        Id = row.Field<int>("imageID"),
                        OID = row.Field<Guid>("image"),
                        Name = row.Field<string>("altName").TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("imageWidth"),
                        Height = row.Field<int>("imageHeight"),
                        MimeType = row.Field<string>("mimeType")
                    }
                })
                ).ToList(); 

            return item;
        }

        public CatalogMenu GetCategoryItem(int rootNode, int id, int shopType)
        {
            const string sql = @"
SELECT
	n.nodeID
	, n.nodeName
	, n.parentNode
	, countChild = (n.rgt - n.lft - 1)/2
	, t.goodsCount
	, l.level,
    , t.themeName
    , t.altName
FROM
	t_Nodes n
    inner join t_Object o on n.object = o.OID
    inner join t_Theme t on o.OID = t.OID
    cross apply (SELECT Count(*) - 1 
		FROM t_Nodes nn inner join t_Nodes nn1 on nn1.tree = nn.tree And nn.lft BETWEEN nn1.lft 
			And nn1.rgt and nn1.nodeID = @rootNode
		WHERE n.tree = nn.tree And n.lft BETWEEN nn.lft And nn.rgt
	) l(level)
WHERE
    n.nodeID = @id
";

            return sql.ExecSql(new { id, rootNode, shopType }).Select(row => new CatalogMenu
            {
                NodeId = row.Field<int>("nodeID"),
                ParentNodeId = row.Field<int>("parentNode"),
                Name = row.Field<string>("altName") ?? row.Field<string>("nodeName"),
                StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                Level = row.Field<int>("level"),
                GoodsCount = row.Field<int>("goodsCount")
            }).SingleOrDefault();
        }

        public string GetCategoryUrl(string siteUrl, string shortLink, int rootNode)
        {
            return $"{siteUrl}{GetShortCategoryUrl(shortLink, rootNode)}";
        }

        public string GetShortCategoryUrl(string shortLink, int rootNode)
        {
            return $"/catalog/{FromShortName(shortLink, rootNode).StringRepresentation?.ToLower()}";
        }

        public string GetShortManufUrl(string caregoryId, string manufId, int rootNode)
        {
            var company = _companyMenuRepository.GetCompany(manufId);
            return $"{GetShortCategoryUrl(caregoryId, rootNode)}/{company.ShortLink?.ToLower()}";
        }

        [Cache]
        public IList<NodeMenu> GetShopMenu(int shopType)
        {
            const string sql = @"
SELECT
	shopType,
	name,
	address
FROM 
	t_TypeShop
WHERE 
	active = 1
ORDER BY 
	shopType
";
            return sql.ExecSql(null).Select(row => new NodeMenu
            {
                Name = row.Field<string>("name"),
                Url = row.Field<string>("address"),
                Id = row.Field<int>("shopType"),
                Selected = row.Field<int>("shopType") == shopType
            }).ToList();
        }

        [Cache]
        public IList<CatalogMenu> GetCategoryPath(int rootNode, int id, int shopType)
        {
            const string sql = @"
SELECT
	n.nodeName,
	n.nodeID,
    o.shortLink,
	countChild = (n.rgt - n.lft - 1)/2,
	t.goodsCount,
    t.themeName,
    t.altName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @rootNode
	inner join t_Nodes n2 on n.tree = n2.tree And n2.lft BETWEEN n.lft and n.rgt and n2.nodeID = @id
	inner join t_Theme t on n.object = t.OID
    inner join t_Object o on t.OID = o.OID
ORDER BY
	n.lft
";
            return sql.ExecSql(new { id, rootNode, shopType }).Select(row => new CatalogMenu
            {
                Name = row.Field<string>("altName") ?? row.Field<string>("nodeName"),
                NodeId = row.Field<int>("nodeID"),
                StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                HasChild = row.Field<int>("countChild") > 0,
                GoodsCount = row.Field<int>("goodsCount")
            }).ToList();
        }
        [Cache]
        public IList<CatalogMenu> GetManufCategoryPath(int rootNode, int id, int shopType, string manufName)
        {
            var items = GetCategoryPath(rootNode, id, shopType).Select(m => m).ToList();
            if (items.Count > 0)
            {
                string lastCategory = items.Last().Name;
                items.Add(new CatalogMenu
                {
                    Name = $"{lastCategory} {manufName}"
                });
            }
            return items;
        }

        [Cache]
        public IList<CatalogMenu> GetManufacturerPath(int rootNode, int id, int shopType, int manufId, string manufRepresentation, string manufName)
        {
            const string sql = @"
SELECT
	n.nodeName,
	n.nodeID,
	countChild = (n.rgt - n.lft - 1)/2,
    o.shortLink,
	goodsCount = ISNULL(tmgc.goodsCount, 0),
    t.themeName,
    t.altName
FROM
	t_Nodes n
	inner join t_Nodes n1 on n.tree = n1.tree And n.lft BETWEEN n1.lft and n1.rgt and n1.nodeID = @rootNode
	inner join t_Nodes n2 on n.tree = n2.tree And n2.lft BETWEEN n.lft and n.rgt and n2.nodeID = @id
	inner join t_Object oc on oc.objectID = @manufID
	left join t_Theme t on n.object = t.OID
    left join t_Object o on t.OID = o.OID
	left join t_ThemeManufGoodsCount tmgc on t.OID = tmgc.themeOID and oc.OID = tmgc.manufacturerOID
ORDER BY
	n.lft
";
            var items = sql.ExecSql(new {id, rootNode, shopType, manufId}).Select(row => new CatalogMenu
            {
                SelectedBrand = new Company
                {
                    Id = manufId,
                    ShortLink = manufRepresentation
                },
                UseBrandUrl = id == rootNode,
                Name = $"{row.Field<string>("themeName") ?? row.Field<string>("nodeName")} {manufName}",
                H1 = $"{row.Field<string>("altName") ?? row.Field<string>("nodeName")} {manufName}",
                NodeId = row.Field<int>("nodeID"),
                StringRepresentation = row.Field<string>("shortLink")?.ToLower(),
                //SimpleRepresentation = row.Field<int>("countChild") > 0,
                GoodsCount = row.Field<int>("goodsCount")
            }).ToList();
            return items;
        }

        [Cache]
        public static IDictionary<string, CatalogMenu> GetShortLinkIds(int rootNode)
        {
            const string sql = @"
SELECT
    nodeID = n.nodeID
	, shortLink = o.shortLink
    , countChild = (n.rgt - n.lft)/2
	, level = (
		SELECT count(*) 
		FROM t_Nodes n1 
			inner join t_Nodes n2 on n1.tree = n2.tree and n1.lft BETWEEN n2.lft and n2.rgt and n2.nodeID = @rootNode 
			inner join t_Nodes n3 on n1.tree = n3.tree and n3.lft BETWEEN n1.lft and n1.rgt and n3.nodeID = n.nodeID 
	  ) - 1
    , lft = n.lft
    , t.goodsList
    , isTag = convert(bit, 0)
FROM
	t_Nodes n
	inner join t_Object o on n.object = o.OID and o.shortLink <> ''
    inner join t_Theme t on t.OID = o.OID 
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @rootNode
UNION ALL
SELECT
    o.objectID
    , o.shortLink
    , 0
    , 0
    , 0
    , convert(bit, 1)
    , convert(bit, 1)
FROM
    t_LandingPage l
    inner join t_Object o on l.OID = o.OID
ORDER BY
	isTag, lft   
";
            var prms = new Hashtable();
            prms["rootNode"] = rootNode;
            var ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            var items = ds.Table.AsEnumerable().Select(k => new CatalogMenu
            {
                NodeId = k.Field<int>("nodeID"),
                StringRepresentation = k.Field<string>("shortLink")?.ToLower(),
                HasChild = /*k.Field<int>("level") < 2 && */k.Field<int>("countChild") != 0,
                GoodsList = k.Field<bool>("goodsList"),
                IsTag = k.Field<bool>("isTag")
            }
            );   
            var dict = new Dictionary<string, CatalogMenu>();
            foreach (var item in items)
            {
                if (!dict.ContainsKey(item.StringRepresentation))
                {
                    dict[item.StringRepresentation] = item;
                }
            }
            return dict;
        }

        
        public CatalogMenu FromShortName(string item, int rootNode)
        {
            var dict = GetShortLinkIds(rootNode);
            return dict.ContainsKey(item?.ToLower()) ? dict[item?.ToLower()] : new CatalogMenu();
        }

        public string GetShortLink(int objectId)
        {
            string sql = "SELECT shortLink FROM t_Object WHERE objectId = @objectId";
            return sql.ExecSql(new {objectId}).Select(r => r.Field<string>("shortLink")).SingleOrDefault();
        }
    }
}