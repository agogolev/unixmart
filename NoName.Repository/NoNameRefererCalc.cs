using Ecommerce.Repository.Referer;

namespace NoName.Repository
{
	public class NoNameRefererCalc : IRefererLabelCalc
	{
		public string Calculate(int id)
		{
			var firstSection = id % 100;
			var secondSection = id / 100;
			return $"{secondSection:00}-{firstSection:00}";
		}
		public int ReverseCalculate(string id)
		{
			var sections = id.Split("-".ToCharArray());
			return int.Parse(sections[0]) * 100 + int.Parse(sections[1]);
		}
	}
}
