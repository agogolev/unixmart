using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
	
	public class LocationRepository : ILocationRepository
	{
		[Cache]
		public Domain.Location GetLocation(int id)
		{
			const string sql = @"
SELECT
	name,
	lattitude,
	longitude,
	address
FROM
	t_Location l
	inner join t_Object o on l.OID = o.OID and o.objectID = @id
";
			return sql.ExecSql(new {id}).Select(row => new Domain.Location
			                                           	{
															Id = id,
			                                           		Name = row.Field<string>("name"),
															Lattitude = (decimal)row.Field<double>("lattitude"),
															Longitude = (decimal)row.Field<double>("longitude"),
															Address = row.Field<string>("address")
														}).DefaultIfEmpty(new Domain.Location()).SingleOrDefault();
		}
	}
}
