using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Core.Aspects;
using DBReader;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;
using MetaData;
using NoName.Extensions;

namespace NoName.Repository
{
    public class MarketingActionRepository : IMarketingActionRepository
    {
        private static readonly Regex TrimDoc = new Regex(@"\<doc\>(.*)\</doc\>", RegexOptions.IgnoreCase);

        [Cache]
        public MarketingAction GetActionById(int id, int rootNode, int shopType)
        {
            const string sql = @"
SELECT
	o.objectID
	, art.Header
	, art.Body
	, a.dateBegin
	, a.dateEnd
    , o1.title
	, o1.keywords
	, o1.pageDescription
FROM
	t_Action a
	inner join t_Object o on a.OID = o.OID
	inner join t_Article art on a.description = art.OID
	inner join t_Object o1 on art.OID = o1.OID
WHERE
	o.objectID = @id
 
SELECT
	g.OID
	, o1.objectID
	, g.pseudoID
	, g.model
	, g.productType
	, g.manufacturer
	, c.companyName
    , image = image.OID
	, imageID = image.objectID
	, imageWidth = image.width
	, imageHeight = image.height
    , mimeType = image.mimeType
	, gp.price
	, gp.oldPrice
	, gp.deliveryPrice
	, isNew = dbo.fIsNew(g.OID, @shopType)
	, isHit = dbo.fIsHit(g.OID, @shopType)
	, isSale = dbo.fIsSale(g.OID, @shopType)
	, mp.deliveryFirst
	, gs.inStock
    , hitWeight = ISNULL(gh.popularity, 0)
    , gs.stockAmount
	, gs.inPath
	, gs.onDemand
	, gs.pathDelivery
	, gd.description
	, o1.dateCreate
	, categoryId = category.Id
	, categoryName = category.Name
FROM
	t_Action act
	inner join t_Object o on act.OID = o.OID
	inner join t_CommodityActualGoods cad on act.commodityGroup = cad.OID 
	inner join t_Goods g on g.OID = cad.goodsOID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price <> 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
    left join t_GoodsIsHits gh on g.OID = gh.OID and gh.shopType = @shopType
	left join t_GoodsDescriptions gd on g.OID = gd.OID and gd.shopType = @shopType
	inner join t_Object o1 on g.OID = o1.OID
	inner join t_MasterPrices mp on g.category = mp.OID and mp.shopType = @shopType 
	inner join t_Company c on g.manufacturer = c.OID
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = g.OID and oi.ordValue like 'Для карточки%' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	cross apply (SELECT TOP 1 n.nodeID, n.NodeName FROM t_Nodes n inner join t_ThemeMasters tm on n.object = tm.OID and tm.masterOID = g.category ORDER BY n.lft) category(Id, name)
WHERE
	o.objectID = @id
ORDER BY
	cad.ordValue, gp.price

SELECT DISTINCT
	g.OID
	, gps.paramTypeOID
	, gps.ordValue
	, name = ISNULL(pt.altName, pt.name)
	, gps.value
	, gps.unit
	, tp.isFilter
	, tp.isFullName
FROM
	t_Action act
	inner join t_CommodityActualGoods cad on act.commodityGroup = cad.OID
	inner join t_Goods g on cad.goodsOID = g.OID
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100) and gp.price <> 0
	inner join t_GoodsInStocks gs on g.OID = gs.OID and gs.shopType = @shopType and gs.inStock = 1 and gs.zombi = 0
	inner join t_ThemeMasters tm on g.category = tm.masterOID
	inner join t_TemplateParams tp on tp.OID = tm.OID And tp.isFullName = 1
	inner join t_GoodsParams gps on gps.OID = g.OID And gps.paramTypeOID = tp.paramTypeOID 
	inner join t_ParamType pt on gps.paramTypeOID = pt.OID
";
            DataTableCollection ds = sql.ExecMSql(new { id, shopType, rootNode });

            return ds[0].AsEnumerable()
                .Select(row => new MarketingAction
                {
                    Id = row.Field<int>("objectID"),
                    Name = row.Field<string>("header"),
                    DateBegin = row.Field<DateTime?>("dateBegin") ?? DateTime.Today,
                    DateEnd = row.Field<DateTime?>("dateEnd"),
                    Description =
                        new Article { Body = new HtmlString(TrimDoc.Match(row.Field<string>("body")).Groups[1].Value) },
                    Title = row.Field<string>("title"),
                    Keywords = row.Field<string>("keywords"),
                    PageDescription = row.Field<string>("pageDescription"),
                    Goods = ds[1].AsEnumerable()
                        .Select(r1 => new GoodsItem
                        {
                            OID = r1.Field<Guid>("OID"),
                            ObjectID = r1.Field<int>("objectID"),
                            PseudoID = r1.Field<string>("pseudoID"),
                            CategoryId = r1.Field<int>("categoryId"),
                            //CategoryFullName = r1.Field<int>("categoryId").ToFullName(rootNode),
                            ProductType = r1.Field<string>("productType"),
                            Model = r1.Field<string>("model"),
                            Manufacturer = r1.Field<Guid?>("manufacturer"),
                            ManufacturerName = r1.Field<string>("companyName"),
                            Image = new Image
                            {
                                Id = row.Field<int?>("imageID") ?? GoodsRepository.BlankImageSmallId,
                                OID = row.Field<Guid?>("image") ?? GoodsRepository.BlankImageSmall,
                                Name = GoodsHelper.FullName(row.Field<string>("productType"), row.Field<string>("companyName"), row.Field<string>("model")).TranslitEncode(true).ToLower(),
                                Width = row.Field<int?>("imageWidth") ?? GoodsRepository.BlankImageSmallWidth,
                                Height = row.Field<int?>("imageHeight") ?? GoodsRepository.BlankImageSmallHeight,
                                MimeType = row.Field<string>("mimeType") ?? GoodsRepository.BlankImageSmallMimeType
                            },
                            Price = r1.Field<decimal>("price"),
                            OldPrice = r1.Field<decimal>("oldPrice"),
                            InStock = r1.Field<bool>("inStock"),
                            HitWeight = r1.Field<int>("hitWeight"),
                            StockAmount = r1.Field<int>("stockAmount"),
                            IsNew = r1.Field<bool>("isNew"),
                            IsHit = r1.Field<bool>("isHit"),
                            IsSale = r1.Field<bool>("isSale"),
                            DateCreate = r1.Field<DateTime>("dateCreate"),
                            Delivery =
                                new DeliveryInfo
                                {
                                    Price = r1.Field<decimal?>("deliveryPrice") ?? r1.Field<decimal>("deliveryFirst")
                                },
                            GoodsParams = ds[2].AsEnumerable()
                                .Where(k => k.Field<Guid>("OID") == r1.Field<Guid>("OID"))
                                .OrderBy(k => k.Field<int>("ordValue"))
                                .Select(k => new ParamItem
                                {
                                    ParamType = k.Field<Guid>("paramTypeOID"),
                                    ParamName = k.Field<string>("name"),
                                    ParamValue = k.Field<string>("value"),
                                    IsFilter = k.Field<bool>("isFilter"),
                                    IsFullName = k.Field<bool>("isFullName"),
                                    Unit = k.Field<string>("unit")
                                }).ToList()
                        }).OrderByDescending(i => i.HitWeight).ThenByDescending(i => i.StockAmount).ThenByDescending(i => i.Price).ToList()
                })
                .Single();
        }

        [Cache]
        public IList<MarketingAction> ListActions()
        {
            const string sql = @"
SELECT
    a.OID
	, o.objectID
	, art.Header
	, art.annotation
	, a.dateBegin
	, a.dateEnd
    , a.geoInclusive
	, image = ISNULL(image.OID, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb')
	, imageID = ISNULL(image.objectID, 37634)
	, imageWidth = ISNULL(image.width, 200)
	, imageHeight = ISNULL(image.height, 200)
    , mimeType = ISNULL(image.mimeType, 'image/png')
FROM
	t_Action a
	inner join t_Article art on a.description = art.OID
	inner join t_Object o on a.OID = o.OID
    outer apply (
        SELECT TOP 1 oi.binData, b.width, b.height, b.mimeType, obj.objectID 
        FROM t_ObjectImage oi 
            inner join t_Object obj on oi.binData = obj.OID
            inner join t_BinaryData b on oi.binData = b.OID 
        WHERE oi.OID = a.OID and oi.ordValue = 'headerImage' 
        ORDER BY oi.ordValue) image(OID, width, height, mimeType, objectID)
	outer apply (
        SELECT TOP 1 b.OID, b.width, b.height 
		FROM t_BinaryData b 
            inner join t_ObjectImage oi on oi.binData = b.OID and oi.ordValue = 'headerImage' and oi.OID = art.OID
	) image1(imageOID, width, height)
WHERE
	a.isPublic = 1
ORDER BY
    a.dateBegin desc, a.dateEnd
	
	
SELECT
	ai.OID,
	l.id 
FROM
	t_Action ai
	inner join t_Object o2 on ai.OID = o2.OID
	inner join t_ActionLocations al on ai.OID = al.OID
	inner join t_Nodes n on al.locationOID = n.object and n.tree = @tree 
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft And n.rgt
	inner join t_Location l on n1.object = l.OID And l.ID <> 0
";

            var prms = new Hashtable();
            prms["tree"] = Settings.Default.GeoTargetingTree;
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            return ds.Table.AsEnumerable().Select(row => new MarketingAction
            {
                OID = row.Field<Guid>("OID"),
                Id = row.Field<int>("objectId"),
                Name = row.Field<string>("header"),
                DateBegin = row.Field<DateTime?>("dateBegin") ?? DateTime.Today,
                DateEnd = row.Field<DateTime?>("dateEnd"),
                HeaderImage =
                    new Image
                    {
                        Id = row.Field<int>("imageID"),
                        OID = row.Field<Guid>("image"),
                        Name = row.Field<string>("header").TranslitEncode(true).ToLower(),
                        Width = row.Field<int>("imageWidth"),
                        Height = row.Field<int>("imageHeight"),
                        MimeType = row.Field<string>("mimeType")
                    },
                Description = new Article { Annotation = row.Field<string>("annotation").ToHtml() },
                GeoInclusive = row.Field<bool>("geoInclusive"),
                GeoLocations = ds.Tables[1].AsEnumerable()
                    .Where(row1 => row1.Field<Guid>("OID") == row.Field<Guid>("OID"))
                    .Select(row1 => row1.Field<int>("ID")).ToList()
            }).ToList();
        }

        public IList<MarketingAction> GetActions(Guid sessionId, int regionId)
        {
            List<MarketingAction> items = ListActions()
                .Where(a =>
                    (
                        (a.GeoInclusive && a.GeoLocations.Count == 0) ||
                        (a.GeoInclusive && a.GeoLocations.Count(i => i == regionId) != 0) ||
                        (!a.GeoInclusive && a.GeoLocations.Count() != 0 && a.GeoLocations.Count(i => i == regionId) == 0)
                        )).ToList();
            return items;
        }
    }
}