﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using DBReader;
using NoName.Domain;
using NoName.Domain.Classes;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class FormRefuseRepository : IFormRefuseRepository
    {
        public void SaveFormRefuse(FormRefuse form)
        {
            var refuse = new CFormRefuse(null)
            {
                FIO = form.FIO,
                Phone = form.Phone,
                EMail = form.EMail,
                Address = form.Address,
                DateOccur = form.DateOccur ?? DateTime.MinValue,
                GoodsName = form.GoodsName,
                BillNumber = form.BillNumber,
                Description = form.Description,
                Requirement = form.Requirement
            };

            var conn = new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"]);
            conn.Open();

            try
            {
                var tran = conn.BeginTransaction();
                refuse.SetConnTrans(Guid.Empty, conn, tran);
                refuse.Save();
                if (form.Attachments != null && form.Attachments.Any())
                {
                    var files = SaveAttachments(form.Attachments, conn, tran);

                    foreach (CBinaryData file in files)
                    {
                        refuse.AddMulti("accessories", file.OID, null);
                    }
                }

                tran.Commit();
            }
            catch (SqlException ex)
            {
                throw new Exception("Ошибка сохранения формы возврата " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private IEnumerable<CBinaryData> SaveAttachments(IEnumerable<HttpPostedFileBase> attachments, SqlConnection conn, SqlTransaction tran)
        {
            return attachments.Where(a => a != null).Select(attach =>
            {
                CBinaryData data = new CBinaryData(null);
                data.FileName = attach.FileName;
                data.Name = Path.GetFileNameWithoutExtension(attach.FileName);
                data.MimeType = attach.ContentType;
                data.Data = new byte[attach.ContentLength];
                attach.InputStream.Read(data.Data, 0, data.Data.Length);
                data.SetConnTrans(Guid.Empty, conn, tran);
                data.Save();
                return data;
            });
        }
    }
}