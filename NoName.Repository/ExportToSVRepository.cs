using System.Collections;
using System.Data.SqlClient;
using System.Text;
using DBReader;
using MetaData;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
	public class ExportToSVRepository : IExportToSVRepository
	{
		public ExportToSVRepository(string stockDB)
		{
			StockDB = stockDB;
		}
		public string StockDB { get; internal set; }
		public string GetStockInfo(int stockAmount, int shopAmount, int pathAmount, int centralStockAmount, string login, string password)
		{
			const string sql = @"
SELECT
	s.*,
	manufacturerId = b.Id,
	manufacturerName = b.name,
	Model = n.Name,
	n.classId,
	n.Description,
	n.roznId,
	n.status,
	cbid = 0, --p1.value,
	bid = 0 --p2.value
FROM
	Stok s
	inner join Nomencl n on s.modelId = n.id
	inner join Brand b on n.brandId = b.Id
	--left join ParamValues p1 on s.modelid = p1.nomenclID and p1.name = 'cbid'
	--left join ParamValues p2 on s.modelid = p2.nomenclID and p2.name = 'bid'
WHERE
	{0}

SELECT * FROM Qualifier
ORDER BY id
";
			var sb = new StringBuilder();
			sb.Append("(Quantity >= @stockAmount Or ");
			if (shopAmount >= 0) sb.Append("Quantity3 >= @shopAmount Or ");
			if (pathAmount >= 0) sb.Append("Quantity2 >= @pathAmount Or ");
			if (centralStockAmount >= 0) sb.Append("Quantity4 >= @centralStockAmount Or ");
			sb.Length -= 4;
			sb.Append(") and NOT s.price is null");
			var prms = new Hashtable();
			prms["shopAmount"] = shopAmount;
			prms["stockAmount"] = stockAmount;
			prms["pathAmount"] = pathAmount;
			prms["centralStockAmount"] = centralStockAmount;
			var ds = GetDataSetISM(string.Format(sql, sb), prms, false, login, password);
			return ds.SaveToString();

		}
		public DataSetISM GetDataSetISM(string sql, Hashtable prms, bool IsStoredProc, string login, string password)
		{
			var connStr = StockDB;
			connStr = connStr.Replace("%login%", login).Replace("%password%", password);
			using (var conn = new SqlConnection(connStr))
			{
				conn.Open();
				return DBHelper.GetDataSetISMSql(sql, null, prms, IsStoredProc, conn, null);
			}

		}
	}
	public static class Exporter
	{
		public static string ToParamString(this string item)
		{
			return item ?? "";
		}
	}
}
