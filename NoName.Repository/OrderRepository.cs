using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Core.Aspects;
using DBReader;
using NoName.Domain;
using NoName.Domain.Classes;
using NoName.Repository.Contracts;
using MetaData;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private const string sqlPreorder = @"
SELECT
	o.*
	, payName = tp.name
	, deliveryName = td.name
	, o1.dateCreate
    , refererName = r.code + ' - ' + r.name
    , td.isCalc
FROM
	t_PreOrder o
	inner join t_Object o1 on o.OID = o1.OID
	left join t_TypePay tp on o.payType = tp.payType
	left join t_TypeDelivery td on o.deliveryType = td.deliveryType
    left join t_Referer r on o.referer = r.id
WHERE
	{0}

SELECT
	oc.*,
	g.Model,
    c.companyName,
	g.productType,
	g.pseudoID,
	ob.objectID
FROM
	t_PreOrderContents oc 
	inner join t_PreOrder o on oc.OID = o.OID
	inner join t_Goods g on oc.goodsOID = g.OID 
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_Object ob on g.OID = ob.OID
WHERE
	{0}
";
        private const int _cardPayment = 2;
        public int CardPayment => _cardPayment;

        private readonly IBasketRepository _basketRepository;

        private readonly ICookieRepository _cookieRepository;

        private readonly ISessionRepository _sessionRepository;

        public OrderRepository(
            IBasketRepository basketRepository,
            ICookieRepository cookieRepository,
            ISessionRepository sessionRepository)
        {
            _basketRepository = basketRepository;
            _cookieRepository = cookieRepository;
            _sessionRepository = sessionRepository;
        }

        [Cache]
        public IList<PayInfoItem> GetPayTypeLookup(int shopType)
        {
            const string sql = @"
SELECT
	payType,
	nameHtml,
    description,
    onlinePay
FROM
	t_TypePay
WHERE
	shopType = @shopType and isActive = 1
ORDER BY
	payType
";
            var result = sql.ExecSql(new { shopType }).Select(row =>
                  new PayInfoItem
                  {
                      PayType = row.Field<int>("payType"),
                      Name = new HtmlString(row.Field<string>("nameHtml")),
                      Description = new HtmlString(row.Field<string>("description")),
                      OnlinePay = row.Field<bool>("onlinePay")
                  }).ToList();
            return Settings.Default.EnableCardPayment ? result : result.Where(p => !p.OnlinePay).ToList();
        }

        [Cache]
        public bool HasInstantDelivery(int shopType)
        {
            return false;
            //const string sql = "SELECT optionValue FROM t_Option WHERE optionId = 2";
            //return sql.ExecSql(null).Select(row => 
            //    row.Field<string>("optionValue")).First().Split(",".ToCharArray()).AsEnumerable().Select(int.Parse).Contains(shopType);
        }

        public Order GetOrder(Guid orderOID, int rootNode)
        {
            const string sql = @"
SELECT
	o.*
	, metroName = m.name
	, liftName = tl.name
	, payName = tp.name
	, deliveryName = td.name
	, o1.dateCreate
    , refererName = r.code + ' - ' + r.name
    , td.isCalc
    , couponCode = pc.promoCode
FROM
	t_Order o
	inner join t_Object o1 on o.OID = o1.OID
	left join t_Metro m on o.metroId = m.metroId
	left join t_TypeLift tl on o.liftType = tl.liftType
	left join t_TypePay tp on o.payType = tp.payType
	left join t_TypeDelivery td on o.deliveryType = td.deliveryType
    left join t_Referer r on o.referer = r.id
    left join t_PromoCode pc on o.promoCode = pc.OID
WHERE
	o.OID = @orderOID

SELECT
	oc.*,
	g.Model,
    c.companyName,
	g.productType,
	g.pseudoID,
	ob.objectID,
	nn.categoryName
FROM
	t_OrderContents oc 
	inner join t_Order o on oc.OID = o.OID
	inner join t_Goods g on oc.goodsOID = g.OID 
    inner join t_Company c on g.manufacturer = c.OID
	inner join t_Object ob on g.OID = ob.OID
	outer apply (
		select top 1 n.nodeName
		from t_ThemeMasters tm
			inner join t_Nodes n on n.object = tm.OID
			inner join t_Nodes n1 on n.tree = n1.tree and n.lft > n1.lft and n.lft < n1.rgt and n1.nodeID = @rootNode
		where tm.masterOID = g.category
	) nn(categoryName)
WHERE
	o.OID = @orderOID
";
            var prms = new Hashtable { { "orderOID", orderOID }, { "rootNode", rootNode } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            Order item = (ds.Table.AsEnumerable()
                .Select(row => new Order
                {
                    OID = row.Field<Guid>("oid"),
                    OrderNum = row.Field<int>("orderNum"),
                    City = row.Field<string>("city"),
                    Street = row.Field<string>("streetName"),
                    MetroName = row.Field<string>("MetroName"),
                    House = row.Field<string>("house"),
                    Build = row.Field<string>("build"),
                    Korp = row.Field<string>("Korp"),
                    Flat = row.Field<string>("Flat"),
                    Code = row.Field<string>("Code"),
                    Phone = row.Field<string>("Phone"),
                    Person = row.Field<string>("Person"),
                    EMail = row.Field<string>("EMail"),
                    PayType = row.Field<int>("payType"),
                    PayName = row.Field<string>("payName"),
                    DeliveryType = row.Field<int>("deliveryType"),
                    DeliveryName = row.Field<string>("deliveryName"),
                    DeliveryPrice = row.Field<decimal>("deliveryPrice"),
                    CalcDistance = row.Field<bool>("isCalc") ? row.Field<int>("calcDistance") : 0,
                    DateCreate = row.Field<DateTime>("dateCreate"),
                    Comments = row.Field<string>("comment"),
                    Referer = row.Field<int?>("referer") ?? 0,
                    RefererName = row.Field<string>("refererName"),
                    ApprovalCode = row.Field<string>("approvalCode"),
                    Coupon = row.Field<Guid?>("promoCode").HasValue
                        ? new Coupon
                        {
                            OID = row.Field<Guid?>("promoCode").Value,
                            PromoCode = row.Field<string>("couponCode")
                        } : null
                })).SingleOrDefault();

            if (item != null)
            {
                item.Items = (from row in ds.Tables[1].AsEnumerable()
                              select new OrderContent
                              {
                                  OrdValue = row.Field<int>("ordValue"),
                                  Goods = row.Field<Guid>("goodsOID"),
                                  ObjectID = row.Field<int>("objectID"),
                                  CategoryFullName = row.Field<string>("categoryName"),
                                  ProductType = row.Field<string>("ProductType"),
                                  Model = row.Field<string>("Model"),
                                  ManufacturerName = row.Field<string>("companyName"),
                                  Amount = row.Field<int>("amount"),
                                  Price = row.Field<decimal>("price"),
                                  PseudoID = row.Field<string>("pseudoID")
                              }).ToList();
            }
            return item;
        }

        public Order GetPreOrder(Guid orderOID)
        {
            var sql = string.Format(sqlPreorder, "o.OID = @orderOID");
            var prms = new Hashtable { { "orderOID", orderOID } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            Order item = (from row in ds.Table.AsEnumerable()
                          select new Order
                          {
                              OID = row.Field<Guid>("oid"),
                              OrderNum = row.Field<int>("orderNum"),
                              City = row.Field<string>("city"),
                              Street = row.Field<string>("streetName"),
                              House = row.Field<string>("house"),
                              Build = row.Field<string>("build"),
                              Korp = row.Field<string>("Korp"),
                              Flat = row.Field<string>("Flat"),
                              Code = row.Field<string>("Code"),
                              Phone = row.Field<string>("Phone"),
                              Person = row.Field<string>("Person"),
                              EMail = row.Field<string>("EMail"),
                              PayType = row.Field<int>("payType"),
                              PayName = row.Field<string>("payName"),
                              DeliveryType = row.Field<int>("deliveryType"),
                              DeliveryName = row.Field<string>("deliveryName"),
                              DeliveryPrice = row.Field<decimal>("deliveryPrice"),
                              CalcDistance = row.Field<bool>("isCalc") ? row.Field<int>("calcDistance") : 0,
                              DateCreate = row.Field<DateTime>("dateCreate"),
                              Comments = row.Field<string>("comment"),
                              Referer = row.Field<int?>("referer") ?? 0,
                              RefererName = row.Field<string>("refererName")
                          }).SingleOrDefault();

            if (item != null)
            {
                item.Items = (from row in ds.Tables[1].AsEnumerable()
                              select new OrderContent
                              {
                                  OrdValue = row.Field<int>("ordValue"),
                                  Goods = row.Field<Guid>("goodsOID"),
                                  ObjectID = row.Field<int>("objectID"),
                                  ProductType = row.Field<string>("ProductType"),
                                  Model = row.Field<string>("Model"),
                                  ManufacturerName = row.Field<string>("companyName"),
                                  Amount = row.Field<int>("amount"),
                                  Price = row.Field<decimal>("price"),
                                  PseudoID = row.Field<string>("pseudoID")
                              }).ToList();
            }
            return item;
        }

        public Order GetPreOrder(int preorderNum)
        {
            var sql = string.Format(sqlPreorder, "o.orderNum = @preorderNum");
            var prms = new Hashtable { { "preorderNum", preorderNum } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            Order item = (from row in ds.Table.AsEnumerable()
                          select new Order
                          {
                              OID = row.Field<Guid>("oid"),
                              OrderNum = row.Field<int>("orderNum"),
                              City = row.Field<string>("city"),
                              Street = row.Field<string>("streetName"),
                              House = row.Field<string>("house"),
                              Build = row.Field<string>("build"),
                              Korp = row.Field<string>("Korp"),
                              Flat = row.Field<string>("Flat"),
                              Code = row.Field<string>("Code"),
                              Phone = row.Field<string>("Phone"),
                              Person = row.Field<string>("Person"),
                              EMail = row.Field<string>("EMail"),
                              PayType = row.Field<int>("payType"),
                              PayName = row.Field<string>("payName"),
                              DeliveryType = row.Field<int>("deliveryType"),
                              DeliveryName = row.Field<string>("deliveryName"),
                              DeliveryPrice = row.Field<decimal>("deliveryPrice"),
                              CalcDistance = row.Field<bool>("isCalc") ? row.Field<int>("calcDistance") : 0,
                              DateCreate = row.Field<DateTime>("dateCreate"),
                              Comments = row.Field<string>("comment"),
                              Referer = row.Field<int?>("referer") ?? 0,
                              RefererName = row.Field<string>("refererName")
                          }).SingleOrDefault();

            if (item != null)
            {
                item.Items = (from row in ds.Tables[1].AsEnumerable()
                              select new OrderContent
                              {
                                  OrdValue = row.Field<int>("ordValue"),
                                  Goods = row.Field<Guid>("goodsOID"),
                                  ObjectID = row.Field<int>("objectID"),
                                  ProductType = row.Field<string>("ProductType"),
                                  Model = row.Field<string>("Model"),
                                  ManufacturerName = row.Field<string>("companyName"),
                                  Amount = row.Field<int>("amount"),
                                  Price = row.Field<decimal>("price"),
                                  PseudoID = row.Field<string>("pseudoID")
                              }).ToList();
            }
            return item;
        }

        public Order GetPreOrder(string paymentNumber)
        {
            var sql = string.Format(sqlPreorder, "o.paymentNumber = @paymentNumber");
            var prms = new Hashtable { { "paymentNumber", paymentNumber } };
            DataSetISM ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
            Order item = (from row in ds.Table.AsEnumerable()
                          select new Order
                          {
                              OID = row.Field<Guid>("oid"),
                              OrderNum = row.Field<int>("orderNum"),
                              City = row.Field<string>("city"),
                              Street = row.Field<string>("streetName"),
                              House = row.Field<string>("house"),
                              Build = row.Field<string>("build"),
                              Korp = row.Field<string>("Korp"),
                              Flat = row.Field<string>("Flat"),
                              Code = row.Field<string>("Code"),
                              Phone = row.Field<string>("Phone"),
                              Person = row.Field<string>("Person"),
                              EMail = row.Field<string>("EMail"),
                              PayType = row.Field<int>("payType"),
                              PayName = row.Field<string>("payName"),
                              DeliveryType = row.Field<int>("deliveryType"),
                              DeliveryName = row.Field<string>("deliveryName"),
                              DeliveryPrice = row.Field<decimal>("deliveryPrice"),
                              CalcDistance = row.Field<bool>("isCalc") ? row.Field<int>("calcDistance") : 0,
                              DateCreate = row.Field<DateTime>("dateCreate"),
                              Comments = row.Field<string>("comment"),
                              Referer = row.Field<int?>("referer") ?? 0,
                              RefererName = row.Field<string>("refererName")
                          }).SingleOrDefault();

            if (item != null)
            {
                item.Items = (from row in ds.Tables[1].AsEnumerable()
                              select new OrderContent
                              {
                                  OrdValue = row.Field<int>("ordValue"),
                                  Goods = row.Field<Guid>("goodsOID"),
                                  ObjectID = row.Field<int>("objectID"),
                                  ProductType = row.Field<string>("ProductType"),
                                  Model = row.Field<string>("Model"),
                                  ManufacturerName = row.Field<string>("companyName"),
                                  Amount = row.Field<int>("amount"),
                                  Price = row.Field<decimal>("price"),
                                  PseudoID = row.Field<string>("pseudoID")
                              }).ToList();
            }
            return item;
        }

        public void SavePaymentNumber(Guid orderOID, string paymentNumber)
        {
            @"
UPDATE t_PreOrder SET paymentNumber = @paymentNumber WHERE OID = @orderOID
SELECT OPID = @orderOID, paymentNumber = @paymentNumber
".ExecSql(new { orderOID, paymentNumber });
        }

        public IList<BasketContent> GetOrderContent(BasketContent[] content, int shopType)
        {
            if (content.Length == 0) return new List<BasketContent>();
            const string sql = @"

SELECT
	g.OID,
	gp.deliveryPrice,
	gp.deliveryPrice2,
	mp.deliveryFirst,
	mp.deliverySecond
FROM
	t_Goods g
	inner join t_MasterPrices mp on g.category = mp.OID and mp.shopType = @shopType
	inner join t_GoodsPrices gp on g.OID = gp.OID and gp.shopType = (@shopType + 100)
WHERE
	g.OID in ({0})
";
            var sb = new StringBuilder();
            foreach (BasketContent item in content)
            {
                sb.AppendFormat("'{0}', ", item.Goods);
            }
            sb.Length -= 2;
            var prms = new Hashtable();
            prms["shopType"] = shopType;
            DataSetISM ds = DBHelper.GetDataSetMTSql(string.Format(sql, sb), prms, false, null);
            List<BasketContent> items = (
                from item in content
                select new BasketContent
                {
                    OrdValue = item.OrdValue,
                    Goods = item.Goods,
                    Amount = item.Amount,
                    Price = item.Price,
                    DeliveryFirst = (from row in ds.Table.AsEnumerable()
                                     where row.Field<Guid>("OID") == item.Goods
                                     select row.Field<decimal?>("deliveryPrice") ?? row.Field<decimal>("deliveryFirst"))
                        .SingleOrDefault(),
                    DeliverySecond = (from row in ds.Table.AsEnumerable()
                                      where row.Field<Guid>("OID") == item.Goods
                                      select row.Field<decimal?>("deliveryPrice2") ?? row.Field<decimal>("deliverySecond"))
                        .SingleOrDefault()
                }).ToList();
            return items;
        }

        public Guid SaveOrder(Basket basket, int shopType, int rootNode, int defaultDeliveryType)
        {
            var order = new COrder(null)
            {
                City = basket.City,
                StreetName = basket.StreetName,
                House = basket.House,
                Build = basket.Build,
                Korp = basket.Korp,
                Flat = basket.Flat,
                Phone = basket.Phone,
                Person = basket.Name,
                EMail = basket.Email,
                PayType = basket.PayType ?? 1,
                Comment = basket.Comments,
                ShopType = shopType,
                DeliveryType = basket.DeliveryType ?? defaultDeliveryType,
                DeliveryPrice = basket.DeliveryPrice,
                CalcDistance = basket.CalcDistance,
                Referer = basket.Referer,
                PromoCode = basket.Coupon?.OID ?? Guid.Empty
            };

            var conn = new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"]);
            conn.Open();

            try
            {
                var tran = conn.BeginTransaction();
                order.SetConnTrans(Guid.Empty, conn, tran);
                order.Save();
                const string sql = @"
Begin tran

if EXISTS(SELECT * FROM t_BasketObjects WHERE OID = @basketOID) Begin
	DELETE FROM t_OrderContents WHERE OID = @OID

	INSERT INTO t_OrderContents (OID, ordValue, goodsOID, amount, price) 
	SELECT 
        @OID, 
        bo.ordValue, 
        bo.goodsOID, 
        bo.amount, 
	    CASE bo.sale
            WHEN 0 THEN gp.price - ISNULL(promo.discount, 0)
            ELSE ISNULL(gp.salePrice, gp.price) - ISNULL(promo.discount, 0) 
        end	 
	FROM
		t_BasketObjects bo
	    inner join t_GoodsPrices gp on bo.goodsOID = gp.OID and gp.shopType = (@shopType + 100)
        inner join t_Basket b on bo.OID = b.OID
        outer apply (SELECT TOP 1 discount FROM t_PromoCode p inner join t_PromoCodeGoods pg on p.OID = pg.OID and pg.OID = b.promoCode and pg.goodsOID = bo.goodsOID) promo(discount)
	WHERE
		bo.OID = @basketOID

    exec spDeleteObject @basketOID

End

commit tran

";
                var prms = new Hashtable { { "OID", order.OID }, { "basketOID", basket.OID }, { "shopType", shopType } };
                DBHelper.ExecuteCommand(sql, prms, false, conn, tran);
                tran.Commit();
                return order.OID;
            }
            catch (SqlException ex)
            {
                throw new Exception("Ошибка сохранения заказа " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public Guid SavePreOrder(Basket basket, int shopType, int rootNode, int defaultDeliveryType)
        {
            var order = new CPreOrder(null)
            {
                City = basket.City,
                StreetName = basket.StreetName,
                House = basket.House,
                Build = basket.Build,
                Korp = basket.Korp,
                Flat = basket.Flat,
                Phone = basket.Phone,
                Person = basket.Name,
                EMail = basket.Email,
                PayType = basket.PayType ?? 1,
                Comment = basket.Comments,
                ShopType = shopType,
                DeliveryType = basket.DeliveryType ?? defaultDeliveryType,
                DeliveryPrice = basket.DeliveryPrice,
                CalcDistance = basket.CalcDistance,
                Referer = basket.Referer,
                PromoCode = basket.Coupon?.OID ?? Guid.Empty
            };

            var conn = new SqlConnection(ConfigurationManager.AppSettings["dbdata.connection"]);
            conn.Open();

            try
            {
                var tran = conn.BeginTransaction();
                order.SetConnTrans(Guid.Empty, conn, tran);
                order.Save();
                const string sql = @"
Begin tran

if EXISTS(SELECT * FROM t_BasketObjects WHERE OID = @basketOID) Begin
	DELETE FROM t_PreOrderContents WHERE OID = @OID

	INSERT INTO t_PreOrderContents (OID, ordValue, goodsOID, amount, price) 
	SELECT 
        @OID, 
        bo.ordValue, 
        bo.goodsOID, 
        bo.amount,
        CASE bo.sale
            WHEN 0 THEN gp.price - ISNULL(promo.discount, 0)
            ELSE ISNULL(gp.salePrice, gp.price) - ISNULL(promo.discount, 0) 
        end	    
	FROM
		t_BasketObjects bo
	    inner join t_GoodsPrices gp on bo.goodsOID = gp.OID and gp.shopType = (@shopType + 100)
        inner join t_Basket b on bo.OID = b.OID
        outer apply (SELECT TOP 1 discount FROM t_PromoCode p inner join t_PromoCodeGoods pg on p.OID = pg.OID and pg.OID = b.promoCode and pg.goodsOID = bo.goodsOID) promo(discount)
	WHERE
		bo.OID = @basketOID
End

commit tran

";
                var prms = new Hashtable { { "OID", order.OID }, { "basketOID", basket.OID }, { "shopType", shopType } };
                DBHelper.ExecuteCommand(sql, prms, false, conn, tran);
                tran.Commit();
                _sessionRepository.Basket = _basketRepository.GetBasketBySession(shopType, rootNode,
                    _cookieRepository.SessionId);
                return order.OID;
            }
            catch (SqlException ex)
            {
                throw new Exception("Ошибка сохранения предварительного заказа " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public Guid SaveOrderFromPreOrder(Guid OID, Guid basketOID, string approvalCode)
        {
            const string sql = @"
Declare
    @orderOID uniqueidentifier

Begin tran

exec spInnerCreateObject 'COrder', @orderOID OUTPUT

UPDATE t_Order SET
	city           = po.city,
	streetName     = po.streetName,
	metroId        = po.metroId,
	house          = po.house,
	build          = po.build,
	korp           = po.korp,
	flat           = po.flat,
	code           = po.code,
	level          = po.level,
	podyezd        = po.podyezd,
	liftType       = po.liftType,
	person         = po.person,
	phone          = po.phone,
	secondPhone    = po.secondPhone,
	eMail          = po.eMail,
	payType        = po.payType,
	operator       = po.operator,
	status         = po.status,
	dateAccepted   = po.dateAccepted,
	shopType       = po.shopType,
	deliveryType   = po.deliveryType,
	deliveryPrice  = po.deliveryPrice,
	comment        = po.comment,
	deliveryDate   = po.deliveryDate,
	deliveryPeriod = po.deliveryPeriod,
	typePay        = po.typePay,
	docType        = po.docType,
	isSendToSV     = po.isSendToSV,
	wikiMID        = po.wikiMID,
	sourceType     = po.sourceType,
	svID           = po.svID,
	distribution   = po.distribution,
	referer        = po.referer,
	calcDistance   = po.calcDistance,
    promoCode      = po.promoCode,
    approvalCode   = @approvalCode
FROM
    t_Order o, t_PreOrder po
WHERE
    o.OID = @orderOID and po.OID = @OID

INSERT INTO t_OrderContents (OID, ordValue, goodsOID, amount, price) 
SELECT @orderOID, ordValue, goodsOID, amount, price FROM t_PreOrderContents WHERE OID = @OID

exec spDeleteObject @basketOID

SELECT OID = @orderOID

commit tran
";
            var prms = new Hashtable { { "OID", OID }, { "basketOID", basketOID }, { "approvalCode", approvalCode } };
            var ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
            return ds.Table.AsEnumerable().Select(r => r.Field<Guid>("OID")).Single();
        }

        public IList<Order> ExportOrders()
        {
            var sql = @"
SELECT
    o.OID
    , o.OrderNum
    , o1.dateCreate
    , o.person
    , o.payType
    , payName = tp.name
    , o.deliveryType
    , deliveryName = td.name
    , o.deliveryPrice
    , td.isCalc
    , o.comment
    , o.referer
    , refererName = r.code + ' - ' + r.name
    , o.calcDistance
    , o.approvalCode
    , o.city
    , o.streetName
    , o.house
    , o.flat
    , o.phone
    , o.email
    , o.promoCode
    , couponCode = pc.promoCode
FROM
    t_Order o
    inner join t_Object o1 on o.OID = o1.OID
    inner join t_TypePay tp on o.payType = tp.payType
    inner join t_TypeDelivery td on o.deliveryType = td.deliveryType
    inner join t_Referer r on o.referer = r.id
    left join t_PromoCode pc on o.promoCode = pc.OID
WHERE
    o1.dateCreate BETWEEN DateAdd(Day, -1 * @daysOffset, GetDate()) And GetDate()

SELECT
   oc.*
   , g.Model
   , c.companyName
   , g.productType
   , g.pseudoID
   , g.postavshik_id
FROM 
   t_OrderContents oc 
   inner join t_Order o on oc.OID = o.OID
   inner join t_Object o1 on o.OID = o1.OID
   inner join t_Goods g on oc.goodsOID = g.OID 
   inner join t_Company c on g.manufacturer = c.OID
   inner join t_Object ob on g.OID = ob.OID
WHERE
    o1.dateCreate BETWEEN DateAdd(Day, -1 * @daysOffset, GetDate()) And GetDate()
";
            var ds = DBHelper.GetDataSetMTSql(sql, new Hashtable
            {
                {
                  "daysOffset",
                  Settings.Default.ExportOrdersDaysOffset
                }
              }, false, null);
            return ds.Table.AsEnumerable().OrderBy(r => r.Field<int>("orderNum")).Select(row => new Order
            {
                OID = row.Field<Guid>("OID"),
                OrderNum = row.Field<int>("orderNum"),
                City = row.Field<string>("city"),
                Street = row.Field<string>("streetName"),
                House = row.Field<string>("house"),
                Flat = row.Field<string>("Flat"),
                Phone = row.Field<string>("Phone"),
                Person = row.Field<string>("Person"),
                EMail = row.Field<string>("EMail"),
                PayType = row.Field<int>("payType"),
                PayName = row.Field<string>("payName"),
                DeliveryType = row.Field<int>("deliveryType"),
                DeliveryName = row.Field<string>("deliveryName"),
                DeliveryPrice = row.Field<Decimal>("deliveryPrice"),
                CalcDistance = row.Field<bool>("isCalc") ? row.Field<int>("calcDistance") : 0,
                DateCreate = row.Field<DateTime>("dateCreate"),
                Comments = row.Field<string>("comment"),
                Referer = row.Field<int?>("referer") ?? 0,
                RefererName = row.Field<string>("refererName"),
                ApprovalCode = row.Field<string>("approvalCode"),
                Coupon = row.Field<Guid?>("promoCode").HasValue
                    ? new Coupon
                    {
                        OID = row.Field<Guid?>("promoCode").Value,
                        PromoCode = row.Field<string>("couponCode")
                    } : null,
                Items = ds.Tables[1].AsEnumerable().Where(k => k.Field<Guid>("OID") == row.Field<Guid>("OID")).OrderBy(k => k.Field<int>("ordValue")).Select(k => new OrderContent
                {
                    OrdValue = k.Field<int>("ordValue"),
                    Goods = k.Field<Guid>("goodsOID"),
                    PostavshikID = k.Field<int?>("postavshik_id"),
                    ProductType = k.Field<string>("ProductType"),
                    Model = k.Field<string>("Model"),
                    ManufacturerName = k.Field<string>("companyName"),
                    Amount = k.Field<int>("amount"),
                    Price = k.Field<Decimal>("price"),
                    PseudoID = k.Field<string>("pseudoID")
                }).ToList()
            }).ToList();
        }

    }
}