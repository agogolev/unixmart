using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using Ecommerce.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
	public class BannerRepository : IBannerRepository
	{
		public AdvertPlace GetBanners(int clientId, int placeId, Guid sessionId, int regionId, Guid? oid, int rootNode)
		{
			var stored = GetPlaceById(placeId);
			var place = (AdvertPlace)stored.Clone();

			var parents = oid.HasValue ? GetParentItems(rootNode).Parents(oid.Value) : (IList<Guid>)null;
			var allItems = ListBanners(clientId, placeId)
				.Where(a =>
					(
					 (a.GeoInclusive && a.GeoLocations.Count == 0) ||
					 (a.GeoInclusive && a.GeoLocations.Count(i => i == regionId) != 0) ||
					 (!a.GeoInclusive && a.GeoLocations.Count() != 0 && a.GeoLocations.Count(i => i == regionId) == 0)
					)
					&&
					(
					 (a.Objects.Count == 0) ||
					 (oid.HasValue && a.Objects.Count(g => parents.IndexOf(g) != -1) != 0)
					)).ToList();
			var maxIndex = allItems.Count;
			if (place.ItemsCount == 0 || maxIndex <= place.ItemsCount)
			{
				place.Items = new List<AdvertItem>();
				allItems.OrderByDescending(a => a.Weight).ToList().ForEach(a =>
				{
					var item = (AdvertItem)a.Clone();
					if (place.IsVerical) item.Width = place.Width;
					else item.Height = place.Height;
					item.ShowKey = RegisterShow(item.ObjectId, sessionId);
					place.Items.Add(item);

				});
			}
			else
			{
				var itemsSelected = new Dictionary<Guid, AdvertItem>();
				var rnd = new Random();
				do
				{
					var i = rnd.Next(maxIndex);
					if (itemsSelected.ContainsKey(allItems[i].OID)) continue;
					var item = (AdvertItem)allItems[i].Clone();
					if (place.IsVerical) item.Width = place.Width;
					else item.Height = place.Height;
					item.ShowKey = RegisterShow(item.ObjectId, sessionId);
					itemsSelected[allItems[i].OID] = item;
				}
				while (itemsSelected.Count != place.ItemsCount);
				place.Items = itemsSelected.Values.OrderByDescending(a => a.Weight).ToList();
			}


			return place;
		}
		[Cache]
		public string GetBannerUrl(int id)
		{
			const string sql = @"
SELECT
	url
FROM
	t_AdvertItem ai
	inner join t_Object o on ai.OID = o.OID And o.objectId = @id	
";
			var url = sql.ExecSql(new { id }).AsEnumerable()
				.Select(row => row.Field<string>("url")).FirstOrDefault();
			return string.IsNullOrWhiteSpace(url) ? "/" : url;
		}
		[Cache]
		protected AdvertPlace GetPlaceById(int placeId)
		{
			const string sql = @"
SELECT 
	ap.OID,
	ap.width,
	ap.height,
	ap.itemWidth,
	ap.itemHeight,
	ap.itemsCount,
	ap.isVertical,
	ap.advertPlaceType
FROM
	t_AdvertPlace ap 
	inner join t_Object o on ap.OID = o.OID And o.objectId = @placeId	
";
			var prms = new Hashtable(); 
			prms["placeId"] = placeId;
			var ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
			return ds.Table.AsEnumerable().Select(k => new AdvertPlace
			{
				OID = k.Field<Guid>("OID"),
				Width = k.Field<int>("width"),
				Height = k.Field<int>("height"),
				ItemWidth = k.Field<int>("itemWidth"),
				ItemHeight = k.Field<int>("itemHeight"),
				ItemsCount = k.Field<int>("itemsCount"),
				IsVerical = k.Field<bool>("isVertical"),
				AdvertPlaceType = k.Field<int>("advertPlaceType")
			}).DefaultIfEmpty(new AdvertPlace()).Single();
		}
		[Cache]
		protected IList<AdvertItem> ListBanners(int clientId, int placeId)
		{
			const string sql = @"
SELECT DISTINCT
	ai.OID,
	ai.binData,
	ai.html,
	ai.width,
	ai.height,
	ai.advertType,
	ai.url,
	ai.parentType,
	ai.geoInclusive,
	aci.weight,
	o2.objectId
FROM
	t_AdvertItem ai
	inner join t_Object o2 on ai.OID = o2.OID
	inner join t_AdvertCompanyItems aci on aci.itemOID = ai.OID and GetDate() BETWEEN aci.dateBegin And ISNULL(aci.DateEnd, GetDate())
	inner join t_AdvertCompany ac on ac.OID = aci.OID and GetDate() BETWEEN ac.dateBegin And ISNULL(ac.DateEnd, GetDate())
	inner join t_Site s on ac.site = s.OID
	inner join t_Object o on s.OID = o.OID And o.objectId = @clientId
	inner join t_AdvertPlace ap on ap.OID = aci.advertPlace 
	inner join t_Object o1 on ap.OID = o1.OID And o1.objectId = @placeId	

SELECT
	ai.OID,
	l.id 
FROM
	t_AdvertItem ai
	inner join t_Object o2 on ai.OID = o2.OID
	inner join t_AdvertCompanyItems aci on aci.itemOID = ai.OID and GetDate() BETWEEN aci.dateBegin And ISNULL(aci.DateEnd, GetDate())
	inner join t_AdvertCompany ac on ac.OID = aci.OID and GetDate() BETWEEN ac.dateBegin And ISNULL(ac.DateEnd, GetDate())
	inner join t_Site s on ac.site = s.OID
	inner join t_Object o on s.OID = o.OID And o.objectId = @clientId
	inner join t_AdvertPlace ap on ap.OID = aci.advertPlace 
	inner join t_Object o1 on ap.OID = o1.OID And o1.objectId = @placeId	
	inner join t_AdvertItemLocations al on ai.OID = al.OID
	inner join t_Nodes n on al.locationOID = n.object and n.tree = @tree 
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft And n.rgt
	inner join t_Location l on n1.object = l.OID And l.ID <> 0

SELECT
	ai.OID,
	aio.objectOID 
FROM
	t_AdvertItem ai
	inner join t_Object o2 on ai.OID = o2.OID
	inner join t_AdvertCompanyItems aci on aci.itemOID = ai.OID and GetDate() BETWEEN aci.dateBegin And ISNULL(aci.DateEnd, GetDate())
	inner join t_AdvertCompany ac on ac.OID = aci.OID and GetDate() BETWEEN ac.dateBegin And ISNULL(ac.DateEnd, GetDate())
	inner join t_Site s on ac.site = s.OID
	inner join t_Object o on s.OID = o.OID And o.objectId = @clientId
	inner join t_AdvertPlace ap on ap.OID = aci.advertPlace 
	inner join t_Object o1 on ap.OID = o1.OID And o1.objectId = @placeId	
	inner join t_AdvertItemObjects aio on ai.OID = aio.OID
";

			var prms = new Hashtable();
			prms["clientId"] = clientId;
			prms["placeId"] = placeId;
			prms["tree"] = new Guid("3094c215-57e7-4623-b4b9-6c93914e8c88");
			var ds = DBHelper.GetDataSetMTSql(sql, prms, false, null);
			return ds.Table.AsEnumerable().Select(k => new AdvertItem
			{
				OID = k.Field<Guid>("OID"),
				ObjectId = k.Field<int>("objectId"),
				BinData = k.Field<Guid?>("binData"),
				Html = k.Field<string>("html"),
				Width = k.Field<int>("width"),
				Height = k.Field<int>("height"),
				AdvertType = k.Field<int>("advertType"),
				Url = k.Field<string>("url"),
				ParentType = k.Field<int>("parentType"),
				Weight = k.Field<int>("weight"),
				GeoInclusive = k.Field<bool>("geoInclusive"),
				GeoLocations = ds.Tables[1].AsEnumerable()
					.Where(row => row.Field<Guid>("OID") == k.Field<Guid>("OID"))
					.Select(row => row.Field<int>("ID")).ToList(),
				Objects = ds.Tables[2].AsEnumerable()
					.Where(row => row.Field<Guid>("OID") == k.Field<Guid>("OID"))
					.Select(row => row.Field<Guid>("objectOID")).ToList()
			}).ToList();
		}

		[Cache]
		protected IDictionary<Guid, IList<Guid>> GetParentItems(int rootNode)
		{
			string sql = @"
SELECT
	n.object,
	n.lft,
	n.rgt
FROM
	t_Nodes n
	inner join t_Object o on n.object = o.OID
	inner join t_Nodes n1 on n.tree = n1.tree and n.lft BETWEEN n1.lft and n1.rgt and n1.nodeId = @rootNode
ORDER BY
	n.lft
";
			var items = sql.ExecSql(new { rootNode })
				.AsEnumerable()
				.Select(row => new Domain.NodeMenu
				{
					ObjectOID = row.Field<Guid>("object"),
					Lft = row.Field<int>("lft"),
					Rgt = row.Field<int>("rgt")
				})
				.ToList();
			var ret = new Dictionary<Guid, IList<Guid>>();
			items
				.Select(a => a.ObjectOID)
				.Distinct()
				.ToList()
				.ForEach(m =>
				{
					var node = items.First(n => n.ObjectOID == m);
					ret[m] =
						items.Where(n => node.Lft >= n.Lft && node.Lft <= n.Rgt).Select(n => n.ObjectOID).Distinct().ToList();
				});
			return ret;
		}

		private Guid RegisterShow(int id, Guid sessionKey)
		{
			const string sql = "upRegisterShow";
			var prms = new Hashtable();
			prms["id"] = id;
			prms["sessionKey"] = sessionKey;
			return (Guid)DBHelper.ExecuteScalar(sql, prms, true);
		}

		public void RegisterClick(int id, Guid showKey)
		{
			const string sql = "upRegisterClick";
			var prms = new Hashtable();
			prms["id"] = id;
			prms["showKey"] = showKey;
			DBHelper.ExecuteCommand(sql, prms, true);
		}
	}
	internal static class ParentsExtension
	{
		public static IList<T> Parents<T>(this IDictionary<T, IList<T>> dict, T item)
		{
			return !dict.ContainsKey(item) ? new[] { item } : dict[item];
		}
	}
}
