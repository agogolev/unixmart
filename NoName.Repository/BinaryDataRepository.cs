using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBReader;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class BinaryDataRepository : IBinaryDataRepository
    {
        public byte[] GetImageById(int id, out string mimeType)
        {
            const string sql =
                "SELECT data, mimeType FROM t_BinaryData b inner join t_Object o on b.OID = o.OID WHERE o.objectID = @id";
            var ds = sql.ExecSql(new {id}).ToList();
            mimeType = ds.Select(row => row.Field<string>("mimeType")).SingleOrDefault();
            return ds.Select(row => row.Field<byte[]>("data")).SingleOrDefault();
        }
    }
}
