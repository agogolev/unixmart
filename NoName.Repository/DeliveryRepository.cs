using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Core.Aspects;
using DBReader;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
	public class DeliveryRepository : IDeliveryRepository
	{
	    private readonly IDeliveryCalcService _deliveryCalcService;

	    public DeliveryRepository(IDeliveryCalcService deliveryCalcService)
	    {
	        _deliveryCalcService = deliveryCalcService;
	    }

	    private const decimal PricePerKm = DeliveryConstants.PRICE_PER_KM; 
		public decimal ExpressDelivery => GetDeliveryPrice(DeliveryConstants.ExpressDeliveryId);
	    public int[] SelfDeliveries => DeliveryConstants.SelfDeliveryIds;

	    public IList<DeliveryInfoItem> GetDeliveries(int shopType, DeliveryInfo delivery, IList<BasketContent> basketContents, bool instantDeliveryPossible)
	    {
	        var items = _deliveryCalcService.GetDeliveries(GetDeliveries(shopType), delivery, basketContents);

            if (!instantDeliveryPossible || !DetermineInstantDeliveryPossibility(DateTime.Now))
			{
				return items.Where(k => k.ExpressDelivery == false).ToList();
			}

			return items.ToList();
		}

		[Cache]  
		private IEnumerable<DeliveryInfoItem> GetDeliveries(int shopType)
		{
			const string sql = @"
SELECT
 *
FROM
 t_TypeDelivery
WHERE
 shopType = @shopType
ORDER BY
 ordValue
";
			var prms = new Hashtable();
			prms["shopType"] = shopType;
			var ds = DBHelper.GetDataSetISMSql(sql, null, prms, false, null);
			return (from row in ds.Table.AsEnumerable()
					select new DeliveryInfoItem
					{
						DeliveryType = row.Field<int>("deliveryType"),
						ExpressDelivery = row.Field<int>("deliveryType") == ExpressDelivery,
                        Name = row.Field<string>("name"),
                        Description = row.Field<string>("description"),
                        Url = row.Field<string>("url"),
						Price = row.Field<decimal>("price"),
						PriceSuffix = row.Field<string>("priceSuffix"),
                        FixPrice = row.Field<decimal?>("fixPrice"),
                        IsCalc = row.Field<bool>("isCalc")
                    }
				).ToList();
		}

		[Cache]
		public decimal GetDeliveryPrice(int deliveryType)
		{
			const string sql = @"
SELECT
	price
FROM
	t_TypeDelivery
WHERE
	deliveryType = @deliveryType
";
			var prms = new Hashtable();
			prms["deliveryType"] = deliveryType;
			return (decimal)DBHelper.ExecuteScalar(sql, prms, false);
		}

		public decimal CalcDeliveryPrice(IList<BasketContent> items)
		{
			return _deliveryCalcService.CalcDeliveryPrice(items);
		}

	    public decimal SelfDeliveryPrice(IList<BasketContent> items, decimal unitPrice)
	    {
	        return _deliveryCalcService.SelfDeliveryPrice(items, unitPrice);
	    }

        public decimal GetAddPrice(int? deliveryType)
		{
			if (!deliveryType.HasValue) return 0;
			const string sql = @"
SELECT 
	price 
FROM 
	t_TypeDelivery 
WHERE 
	deliveryType = @deliveryType";
			return sql.ExecSql(new { deliveryType })
					   .Select(r => r.Field<decimal?>("price"))
					   .First() ?? 0;
		}

        public decimal GetDistancePrice(int? deliveryType, int calcDistance)
        {
            if (!deliveryType.HasValue) return 0;
            const string sql = @"
SELECT 
	isCalc 
FROM 
	t_TypeDelivery 
WHERE 
	deliveryType = @deliveryType";
            var isCalc = sql.ExecSql(new { deliveryType })
                       .Select(r => r.Field<bool>("isCalc"))
                       .First();
            return !isCalc ? 0 : calcDistance*PricePerKm;
        }

        [Cache]
		public bool DetermineInstantDeliveryPossibility(DateTime date)
		{
		    return false;
			const string sql =
				@"  
SELECT 
	* 
FROM 
	t_DisableExpressDelivery 
WHERE 
	dateOccur BETWEEN DateAdd(Day, -1, @date) And  DateAdd(Day, 1, @date)
";
			return date.TimeOfDay < new TimeSpan(12, 30, 0) && date.DayOfWeek != DayOfWeek.Saturday &&
				   date.DayOfWeek != DayOfWeek.Sunday &&
				   sql.ExecSql(new { date }).Count(row => row.Field<DateTime>("dateOccur").Date == date.Date) == 0;
		}
	}
}
