using System.Collections.Generic;
using System.Linq;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class DeliveryCalcService : IDeliveryCalcService
    {
        public IList<DeliveryInfoItem> GetDeliveries(IEnumerable<DeliveryInfoItem> infoItems, DeliveryInfo delivery, IList<BasketContent> basketContent)
        {
            var items = infoItems.Select(k => new DeliveryInfoItem
            {
                DeliveryType = k.DeliveryType,
                ExpressDelivery = k.ExpressDelivery,
                Name = k.Name,
                Description = k.Description,
                Price = DeliveryConstants.SelfDeliveryIds.Any(id => k.DeliveryType == id) ? SelfDeliveryPrice(basketContent, delivery.PriceSelf) : OrdinaryDeliveryItem(delivery.Price, k.FixPrice, k.Price),
                PriceSuffix = k.PriceSuffix,
                Url = k.Url,
                IsCalc = k.IsCalc
            });

            return items.ToList();
        }

        private decimal OrdinaryDeliveryItem(decimal variableDeliveryPart, decimal? fixPrice, decimal price)
        {
            return fixPrice ?? (variableDeliveryPart + price);
        }

        public decimal CalcDeliveryPrice(IList<BasketContent> items)
        {
            if (items.Count == 0)
                return 0;

            var maxDeliveryPrice = (from item in items
                select item.DeliveryFirst).Max();
            var deliverySecondForMaxDeliveryFirst = (from item in items
                where item.DeliveryFirst == maxDeliveryPrice
                select item.DeliverySecond).OrderByDescending(item => item).First();
            return (from item in items
                       select item.DeliverySecond * item.Amount).Sum() + maxDeliveryPrice - deliverySecondForMaxDeliveryFirst;
        }

        public decimal SelfDeliveryPrice(IList<BasketContent> items, decimal priceSelf)
        {
            if (items == null || items.Count == 0)
                return priceSelf;

            return items.Sum(b => b.Amount * b.DeliverySelf);
        }


    }
}
