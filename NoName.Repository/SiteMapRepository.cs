using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Ecommerce.Extensions;
using NoName.Domain;
using NoName.Domain.Enums;
using NoName.Extensions;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class SiteMapRepository : ISiteMapRepository
    {
        private readonly ICompanyMenuRepository _companyMenuRepository;

        private readonly IGoodsRepository _goodsRepository;

        private readonly IMarketingActionRepository _marketingActionRepository;

        private readonly IMenuRepository _menuRepository;

        private const int _goodsOnPage = 12;

        public SiteMapRepository(
            ICompanyMenuRepository companyMenuRepository,
            IGoodsRepository goodsRepository,
            IMarketingActionRepository marketingActionRepository,
            IMenuRepository menuRepository)
        {
            _companyMenuRepository = companyMenuRepository;
            _goodsRepository = goodsRepository;
            _marketingActionRepository = marketingActionRepository;
            _menuRepository = menuRepository;
        }

        private string SiteUrl { get; set; }
        private int ShopType { get; set; }
        private int CatalogRoot { get; set; }
        private int InfoRoot { get; set; }

        private void WriteSiteMapXml(XmlWriter xmlWriter)
        {
            var mainPage = new SiteMapItem(SiteUrl + "/", DateTime.MinValue, SiteMapItemChangeFreq.Daily, 1);
            WriteSiteMapItem(xmlWriter, mainPage);
            foreach (var item in GetCatalogsAndGoods())
            {
                WriteSiteMapItem(xmlWriter, item);
            }

            foreach (var item in GetBrandAndCategories())
            {
                WriteSiteMapItem(xmlWriter, item);
            }

            foreach (var item in GetArticles())
            {
                WriteSiteMapItem(xmlWriter, item);
            }
        }

        private void WriteSiteMapItem(XmlWriter xmlWriter, SiteMapItem item)
        {
            try
            {
                xmlWriter.WriteStartElement("url");
                try
                {
                    xmlWriter.WriteStartElement("loc");
                    xmlWriter.WriteString(item.Url);
                }
                finally
                {
                    xmlWriter.WriteEndElement();
                }

                if (item.Lastmod > DateTime.MinValue)
                {
                    try
                    {
                        xmlWriter.WriteStartElement("lastmod");
                        xmlWriter.WriteString(item.Lastmod.ToString("yyyy-MM-dd"));
                    }
                    finally
                    {
                        xmlWriter.WriteEndElement();
                    }
                }

                if (item.ChangeFreq != SiteMapItemChangeFreq.None)
                {
                    try
                    {
                        xmlWriter.WriteStartElement("changefreq");
                        xmlWriter.WriteString(item.ChangeFreq.ToString());
                    }
                    finally
                    {
                        xmlWriter.WriteEndElement();
                    }
                }

                try
                {
                    xmlWriter.WriteStartElement("priority");
                    xmlWriter.WriteString(item.Priority.ToString(CultureInfo.InvariantCulture));
                }
                finally
                {
                    xmlWriter.WriteEndElement();
                }
            }
            finally
            {
                xmlWriter.WriteEndElement();
            }
        }

        public void RefreshSiteMap(string siteUrl, int shopType, int catalogRoot, int infoRoot)
        {
            SiteUrl = siteUrl;
            ShopType = shopType;
            CatalogRoot = catalogRoot;
            InfoRoot = infoRoot;

            var mutexSecurity = new MutexSecurity();
            mutexSecurity.AddAccessRule(new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                MutexRights.Synchronize | MutexRights.Modify, AccessControlType.Allow));

            bool createdNew;
            var xmlFolder = new Mutex(false, string.Format("sitemapUnixmart{0}", ShopType), out createdNew, mutexSecurity);
            xmlFolder.WaitOne();
            try
            {
                string fileName = HttpContext.Current.Server.MapPath("sitemap.xml");
                using (var w = new XmlTextWriter(fileName, Encoding.UTF8))
                {
                    w.Formatting = Formatting.Indented;
                    w.IndentChar = '\t';
                    w.Indentation = 2;
                    try
                    {
                        w.WriteStartDocument();
                        try
                        {
                            w.WriteStartElement("urlset", "http://www.sitemaps.org/schemas/sitemap/0.9");
                            WriteSiteMapXml(w);
                        }
                        finally
                        {
                            w.WriteEndElement();
                        }
                    }
                    finally
                    {
                        w.WriteEndDocument();
                    }
                }
            }
            finally
            {
                xmlFolder.ReleaseMutex();
            }
        }

        private IEnumerable<SiteMapItem> GetCatalogsAndGoods()
        {
            IList<CatalogMenu> catalogs = _menuRepository.GetCatalogMenuFlat(CatalogRoot, ShopType);
            var items = new List<SiteMapItem>();
            foreach (CatalogMenu c in catalogs)
            {
                if (c.GoodsList || !c.HasChild)
                {
                    //добавляем каталог
                    items.Add(new SiteMapItem(GetCatalogUrl(c), DateTime.MinValue, SiteMapItemChangeFreq.Daily,
                        MapCatalogLevelToPriority(c.Level)));

                    // плюс его постраничные партнёры
                    //for (int pageIndex = 1; pageIndex <= c.GoodsCount/_goodsOnPage; pageIndex++)
                    //{
                    //    items.Add(new SiteMapItem(string.Format("{0}?page={1}", GetCatalogUrl(c), pageIndex + 1),
                    //        DateTime.MinValue, SiteMapItemChangeFreq.Daily,
                    //        MapCatalogLevelToPriority(c.Level)));
                    //}
                }
                else
                {
                    items.Add(new SiteMapItem(GetCatalogUrl(c), DateTime.MinValue, SiteMapItemChangeFreq.Daily,
                        MapCatalogLevelToPriority(c.Level)));
                }
            }

            IList<GoodsItem> goods = _goodsRepository.GetAllGoodsByCategory(ShopType, CatalogRoot);
            items.AddRange(goods.Select(
                    g => new SiteMapItem(GetGoodUrl(g), g.DateCreate, SiteMapItemChangeFreq.Daily, 0.8M)));

            return items;
        }

        private IEnumerable<SiteMapItem> GetBrandAndCategories()
        {
            IList<DirectoryItem> brands = _companyMenuRepository.GetCompanyMenuByABC(ShopType, CatalogRoot);
            var items = new List<SiteMapItem>();
            foreach (var companyMenu in brands.SelectMany(d => d.Items).Where(menu => !string.IsNullOrEmpty(menu.ManufRepresentation)))
            {
                items.Add(new SiteMapItem(GetBrandUrl(companyMenu), companyMenu.DateCreate, SiteMapItemChangeFreq.Weekly));
            }

            IList<CatalogMenu> catalog = _companyMenuRepository.GetCompanyMenuFlat(CatalogRoot, ShopType);
            foreach (var catalogMenu in catalog.Where(menu => !string.IsNullOrEmpty(menu.SelectedBrand.ShortLink)))
            {
                items.Add(new SiteMapItem(GetBrandCategoryUrl(catalogMenu), catalogMenu.DateCreate, SiteMapItemChangeFreq.Weekly));
            }
            return items;
        }

        private static decimal MapCatalogLevelToPriority(int level)
        {
            return 0.9M;
        }

        private IEnumerable<SiteMapItem> GetArticles()
        {
            List<SiteMapItem> result = _menuRepository.GetInfoMenu(InfoRoot).Select(SiteMapItemFromArticles).ToList();
            return result;
        }

        private SiteMapItem SiteMapItemFromArticles(NodeMenu a)
        {
            return new SiteMapItem(SiteUrl + a.Url,
                a.DateModify,
                SiteMapItemChangeFreq.Daily,
                0.7M);
        }

        private string GetCatalogUrl(CatalogMenu item)
        {
            return _menuRepository.GetCategoryUrl(SiteUrl, item.StringRepresentation, CatalogRoot);
        }

        private string GetGoodUrl(GoodsItem item)
        {
            return GoodsHelper.GoodsUrl(SiteUrl, item.ProductType, item.ManufacturerName, item.Model, item.ObjectID);
        }

        private string GetBrandUrl(CompanyMenu item)
        {
            return $"{SiteUrl}/brand/{item.ManufRepresentation.ToLower()}";
        }

        private string GetBrandCategoryUrl(CatalogMenu category)
        {
            if (category.Level == 1)
            {
                return
                    $"{SiteUrl}/brand/{category.SelectedBrand.ShortLink.ToLower()}/{category.StringRepresentation.ToLower()}";
            }

            return
                $"{SiteUrl}/catalog/{category.StringRepresentation.ToLower()}/{category.SelectedBrand.ShortLink.ToLower()}";
        }
    }
}