using System;
using Core.Aspects;
using System.Collections;
using DBReader;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public abstract class BaseRepository : IBaseRepository
    {
		[Cache]
		public Guid? GetObject(int id)
		{
			const string sql = @"
SELECT
	OID
FROM 
	t_Object 
WHERE objectID = @id
";
			var prms = new Hashtable();
			prms["id"] = id;
			return (Guid?)DBHelper.ExecuteScalar(sql, prms, false);
		}

        [Cache]
        public Guid? GetObject(string shortLink)
        {
            const string sql = @"
SELECT
	OID
FROM 
	t_Object 
WHERE shortLink = @shortLink
";
            var prms = new Hashtable();
            prms["shortLink"] = shortLink;
            return (Guid?)DBHelper.ExecuteScalar(sql, prms, false);
        }

        [Cache]
        public int? GetObjectID(string shortLink)
        {
            const string sql = @"
SELECT
	objectID
FROM 
	t_Object 
WHERE shortLink = @shortLink
";
            var prms = new Hashtable();
            prms["shortLink"] = shortLink;
            return (int?)DBHelper.ExecuteScalar(sql, prms, false);
        }

        [Cache]
        public int? GetObjectID(Guid OID)
        {
            const string sql = @"
SELECT
	objectID
FROM 
	t_Object 
WHERE OID = @OID
";
            var prms = new Hashtable();
            prms["OID"] = OID;
            return (int?)DBHelper.ExecuteScalar(sql, prms, false);
        }

        [Cache]
		public Guid? GetNodeObject(int id)
		{
			const string sql = @"
SELECT
	object
FROM 
	t_Nodes 
WHERE nodeID = @id
";
			var prms = new Hashtable();
			prms["id"] = id;
			return (Guid?)DBHelper.ExecuteScalar(sql, prms, false);
		}

    }
}