using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class UrlRepository : IUrlRepository
    {
        private readonly IGoodsRepository _goodsRepository;
        private readonly IMenuRepository _menuRepository;

        public UrlRepository(IGoodsRepository goodsRepository, IMenuRepository menuRepository)
        {
            _goodsRepository = goodsRepository;
            _menuRepository = menuRepository;
        }


        public string CanonicalHomeUrl()
        {
            return "/";
        }

        public string CanonicalCatShortLinkUrl(string categoryId, string manufId, int rootNode)
        {
            return string.IsNullOrEmpty(manufId)
                ? _menuRepository.GetShortCategoryUrl(categoryId, rootNode)
                : _menuRepository.GetShortManufUrl(categoryId, manufId, rootNode);
        }

        public string CanonicalGoodsUrl(int goodsId)
        {
            return _goodsRepository.GetGoodsPath(goodsId);
        }
    }
}