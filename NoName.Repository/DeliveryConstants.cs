using System.Linq;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class DeliveryConstants
    {
        public static int[] SelfDeliveryIds = Settings.Default.SelfDelivery.Split(",".ToCharArray()).AsEnumerable().Select(i => int.Parse(i)).ToArray();
        public static int ExpressDeliveryId = Settings.Default.EXPRESS_DELIVERY;
        public const decimal PRICE_PER_KM = 25M;
    }
}
