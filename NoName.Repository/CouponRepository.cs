using System;
using System.Collections;
using System.Linq;
using DBReader;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
    public class CouponRepository : ICouponRepository
    {
        public decimal GoodsPrice(GoodsItem goods, Coupon coupon)
        {
            if (coupon == null || coupon.AffectedGoods.All(g => g.OID != goods.OID)) return goods.Price;
            return Math.Round(goods.Price - coupon.Discount);
        }

//        public void UpdateBasketCoupon(Basket basket, string couponCode)
//        {
//            const string sql = @"
//Declare
//    @OID uniqueidentifier
//
//SELECT @OID = OID FROM t_Coupon WHERE code = @code
//
//UPDATE t_Basket SET coupon = @OID WHERE session = @session
//";
//            DBHelper.ExecuteCommand(sql, new Hashtable { { "code", couponCode }, { "session", basket.Session } }, false);
//        }
    }
}