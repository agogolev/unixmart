using System;
using System.Web;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
	public class CookieRepository : ICookieRepository
	{
		public Guid SessionId
		{
			get
			{
				string value = GetValue("SessionId");
				if (string.IsNullOrEmpty(value))
				{
					Guid val = Guid.NewGuid();
					SessionId = val;
					return val;
				}
				return new Guid(value);
			}
			set
			{
				SetValue("SessionId", value.ToString(), DateTime.Now.AddDays(30));
			}
		}

        public string ActionpayId
        {
            get
            {
                return GetValue("ActionpayId");
            }
            set
            {
                SetValue("ActionpayId", value, DateTime.Now.AddDays(30));
            }
        }

        private static string GetValue(string key)
		{
			var cookie = HttpContext.Current.Request.Cookies[key];
			if (cookie != null)
				return cookie.Value != null ? cookie.Value.Replace("|", ";") : null;
			return null;
		}

		private static void SetValue(string key, string value, DateTime expires)
		{
			SetValue(key, value, null, null, expires);
		}

		private static void SetValue(string key, string value, string domain, string path, DateTime expires)
		{
			var httpCookie = HttpContext.Current.Response.Cookies[key];
			if (httpCookie == null) return;
			httpCookie.Value = value != null ? value.Replace(";", "|") : null;
			if (!string.IsNullOrEmpty(domain)) httpCookie.Domain = domain;
			if (expires != DateTime.MinValue) httpCookie.Expires = expires;
			if (!string.IsNullOrEmpty(path)) httpCookie.Path = path;
		}
	}
}
