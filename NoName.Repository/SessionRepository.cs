using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using DBReader;
using NoName.Domain;
using NoName.Repository.Contracts;

namespace NoName.Repository
{
	public class SessionRepository : BaseRepository, ISessionRepository
	{
		public bool InstantDeliveryPossible
		{
			get
			{
				if (HttpContext.Current.Session["InstantDeliveryPossible"] == null)
				{
					HttpContext.Current.Session["InstantDeliveryPossible"] = false;
				}
				return (bool)HttpContext.Current.Session["InstantDeliveryPossible"];
			}
			set { HttpContext.Current.Session["InstantDeliveryPossible"] = value; }
		}

		public Guid LastOrderOID
		{
			get
			{
				if (HttpContext.Current.Session["LastOrderOID"] == null)
				{
					HttpContext.Current.Session["LastOrderOID"] = Guid.Empty;
				}
				return (Guid)HttpContext.Current.Session["LastOrderOID"];
			}
			set { HttpContext.Current.Session["LastOrderOID"] = value; }
		}

        public int TotalFinishCount {
            get
            {
                if (HttpContext.Current.Session["TotalFinishCount"] == null)
                {
                    HttpContext.Current.Session["TotalFinishCount"] = 0;
                }
                return (int)HttpContext.Current.Session["TotalFinishCount"];
            }
            set { HttpContext.Current.Session["TotalFinishCount"] = value; }
        }

        public Guid AdvertSessionId
		{
			get
			{
				if (HttpContext.Current.Session["AdvertSessionId"] == null)
				{
					HttpContext.Current.Session["AdvertSessionId"] = Guid.NewGuid();
				}
				return (Guid)HttpContext.Current.Session["AdvertSessionId"];
			}
		}

		/// <summary>
		/// последнее выбранное кол-во на странице
		/// </summary>
		public int PreferedNumOnBatch
		{
			get
			{
				var value = HttpContext.Current.Session["PreferedNumOnBatch"];
				return value == null ? 10 :  (int)value;
			}
			set { HttpContext.Current.Session["PreferedNumOnBatch"] = value; }
		}

		/// <summary>
		/// последняя выбранная сортировка
		/// </summary>
		public string PreferedSort
		{
			get
			{
				var value = HttpContext.Current.Session["PreferedSort"] as string;
				return value ?? "down";
			}
			set { HttpContext.Current.Session["PreferedSort"] = value; }
		}
			
		/// <summary>
		/// Идентификатор фильтруемого в данный момент каталога
		/// </summary>
		public int FilterCatalogId
		{
			get { return (int?)HttpContext.Current.Session?["FilterCategoryId"] ?? 0; }
			set { HttpContext.Current.Session["FilterCategoryId"] = value; }
		}

		/// <summary>
		/// выбранная строка поиска
		/// </summary>
		public string SearchQuery
		{
			get
			{
				var value = HttpContext.Current.Session["SearchQuery"] as string;
				return value ?? string.Empty;
			}
			set { HttpContext.Current.Session["SearchQuery"] = value; }
		}

		/// <summary>
		/// результаты поиска по товарам
		/// </summary>
		public IList<SearchedGoods> SearchResult
		{
			get
			{
				var value = HttpContext.Current.Session["SearchResult"] as IList<SearchedGoods>;
				return value ?? new List<SearchedGoods>();
			}
			set { HttpContext.Current.Session["SearchResult"] = value; }
		}

		/// <summary>
		/// с какого IP пришёл пользователь
		/// </summary>
		public string UserHostAddress
		{
			get
			{
				var value = HttpContext.Current.Session["UserHostAddress"] as string;
				return value ?? string.Empty;
			}
			set { HttpContext.Current.Session["UserHostAddress"] = value; }
		}

        public Basket Basket
        {
            get
            {
                return (Basket)HttpContext.Current.Session["Basket"] ?? new Basket();
            }
            set
            {
                HttpContext.Current.Session["Basket"] = value;
            }
        }

        //public int GoodsCount
        //{
        //    get
        //    {
        //        return (int?)HttpContext.Current.Session["GoodsCount"] ?? 0;
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session["GoodsCount"] = value;
        //    }
        //}

        //public decimal GoodsTotalPrice
        //{
        //    get
        //    {
        //        return (decimal?)HttpContext.Current.Session["GoodsTotalPrice"] ?? 0;
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session["GoodsTotalPrice"] = value;
        //    }
        //}

		public int RegionId
		{
			get
			{
				return (int)HttpContext.Current.Session["regionId"];
			}
			set
			{
				HttpContext.Current.Session["regionId"] = value;
			}
		}

        public string RefererLabel
        {
            get
            {
                return HttpContext.Current.Session["refererLabel"] as string;
            }
            set
            {
                HttpContext.Current.Session["refererLabel"] = value;
            }
        }

		public void RegisterSession()
		{
			const string sql = "upRegisterSession";
			var prms = new Hashtable();
			prms["sessionOID"] = AdvertSessionId;
			prms["ipAddress"] = UserHostAddress;
			prms["regionID"] = RegionId;
			DBHelper.ExecuteCommand(sql, prms, true);
		}
	}
}
