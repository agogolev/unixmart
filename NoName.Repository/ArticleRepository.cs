using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using Core.Aspects;
using DBReader;
using Ecommerce.Extensions;
using NoName.Domain;
using NoName.Repository.Contracts;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class ArticleRepository : IArticleRepository
    {
        private static DateTimeFormatInfo dtfi = CultureInfo.GetCultureInfo("RU-ru").DateTimeFormat;
        private static readonly Regex findYandexMap = new Regex("yandexMap_");
        IBaseRepository _baseRepository;

        public ArticleRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public Article GetArticle(string shortLink, Action<Article> process)
        {
            Guid? obj = _baseRepository.GetObject(shortLink);

            return obj.HasValue ? GetArticle(obj.Value, process) : new Article();
        }
        public Article GetArticle(int id, Action<Article> process)
        {
            Guid? obj = _baseRepository.GetObject(id);

            return obj.HasValue ? GetArticle(obj.Value, process) : new Article();
        }

        [Cache]
        public Article GetArticle(Guid id, Action<Article> process)
        {
            if (id != Guid.Empty)
            {
                const string sql = "upGetArticle";
                var prms = new Hashtable { { "OID", id } };
                XElement doc = DBHelper.GetXmlData(sql, prms, null, true);
                var n = new Article
                {
                    OID = id,
                    Id = int.Parse(doc.Attributes("ID").Single().Value),
                    Header = doc.Descendants("header").Single().Value.ToHtml(),
                    Annotation = doc.Descendants("annotation").Single().Value.CrLf2Br().ToHtml(),
                    PubDate = DateTime.Parse(doc.Descendants("pubDate").Single().Value, dtfi),
                    Body = doc.Descendants("body").Single().Value.DocToHtml(),
                    HasYandexMap = findYandexMap.IsMatch(doc.Descendants("body").Single().InnerXml()),
                    Tags = doc.Descendants("tag").Select(k => k.Value).ToList(),
                    Title = doc.Descendants("title").Select(e => e.Value).SingleOrDefault(),
                    Keywords = doc.Descendants("keywords").Select(e => e.Value).SingleOrDefault(),
                    PageDescription = doc.Descendants("pageDescription").Select(e => e.Value).SingleOrDefault()
                };
                process?.Invoke(n);
                return n;
            }
            return new Article();
        }

        public Article GetNodeArticle(int id, Action<Article> process)
        {
            Guid? obj = _baseRepository.GetNodeObject(id);

            return obj.HasValue ? GetArticle(obj.Value, process) : new Article();
        }

        [Cache]
        public IList<Article> GetAllArticles(int articleType)
        {
            const string sql = @"
SELECT
	a.OID, 
	o.objectID,
	a.header,
	a.annotation,
	a.pubDate,
	ISNULL(image.binData, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb') [image],
	isnull(image.width, 0) width,
	isnull(image.height, 0) height,
	(select tagName 'tag' from t_articletags at where at.oid = a.oid order by at.ordValue for xml path(''), root('tags')) tags
FROM 
	t_Article a
	join t_object o on a.OID = o.OID
	outer apply (select top 1 binData, width, height from t_ObjectImage oi inner join t_binarydata b on oi.binData = b.OID and oi.ordValue = 'headerImage' where o.OID = oi.OID) image
WHERE
	a.articleType = @articleType
";
            return sql.ExecSql(new { articleType }).Select(row => new Article
            {
                OID = row.Field<Guid>("OID"),
                Id = row.Field<int>("objectID"),
                Header = row.Field<string>("header").ToHtml(),
                Annotation = row.Field<string>("annotation").Cut(200, string.Empty, string.Empty).CrLf2Br().ToHtml(),
                PubDate = row.Field<DateTime>("pubDate"),
                Image =
                    new Image
                    {
                        OID = row.Field<Guid>("image"),
                        Width = row.Field<int>("width"),
                        Height = row.Field<int>("height")
                    },
                Tags =
                    !string.IsNullOrEmpty(row.Field<string>("tags"))
                        ? XElement.Parse(row.Field<string>("tags")).Elements("tag").Select(x => x.Value).ToList()
                        : null
            }).ToList();
        }

        [Cache]
        public IList<Article> GetLastNewsArticles(int count = 4)
        {
            const string sql = @"
SELECT top (@count)
	a.OID,
	o.objectID,
	a.header,
	a.annotation,
	a.pubDate,
	ISNULL(image.binData, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb') [image],
	isnull(image.width, 0) width,
	isnull(image.height, 0) height,
	(select tagName 'tag' from t_articletags at where at.oid = a.oid order by at.ordValue for xml path(''), root('tags')) tags
FROM 
	t_Article a
	join t_object o on a.OID = o.OID
	outer apply (select top 1 binData, b.width, b.height from t_ObjectImage oi inner join t_binarydata b on oi.binData = b.OID where o.OID = oi.OID and oi.ordValue = 'для списка') image
WHERE
	a.articleType = 3
order by
	a.pubDate desc
";
            return sql.ExecSql(new { count }).Select(row => new Article
            {
                OID = row.Field<Guid>("OID"),
                Id = row.Field<int>("objectID"),
                Header = row.Field<string>("header").ToHtml(),
                Annotation = row.Field<string>("annotation").Cut(200, string.Empty, string.Empty).CrLf2Br().ToHtml(),
                PubDate = row.Field<DateTime>("pubDate"),
                Image =
                    new Image
                    {
                        OID = row.Field<Guid>("image"),
                        Width = row.Field<int>("width"),
                        Height = row.Field<int>("height")
                    },
                Tags =
                    !string.IsNullOrEmpty(row.Field<string>("tags"))
                        ? XElement.Parse(row.Field<string>("tags")).Elements("tag").Select(x => x.Value).ToList()
                        : null
            }).ToList();
        }

        [Cache]
        public IList<Article> GetLinkedArticles(int parentNodeId)
        {
            const string sql = @"
SELECT
    o.title
    , o.keywords
    , a.body
FROM
    t_Article a
    inner join t_Object o on a.OID = o.OID
    inner join t_Nodes n on a.OID = n.object and n.parentNode = @parentNodeId
ORDER BY 
    n.lft
";
            return sql.ExecSql(new {parentNodeId}).Select(row => new Article
            {
                Title = row.Field<string>("title"),
                Keywords = row.Field<string>("keywords"),
                Body = new HtmlString(row.Field<string>("body"))
            }).ToList();
        }

        [Cache]
        public IList<int> GetArticleThemes(Guid articleOID, int parentNode)
        {
            const string sql = @"
SELECT
	n.nodeId
FROM
	t_ObjectThemes ot
	inner join t_Nodes n on ot.themeOID = n.object and n.parentNode = @parentNode	
WHERE
	OID = @articleOID
ORDER BY
	n.lft
";
            return sql.ExecSql(new { parentNode, articleOID }).Select(row => row.Field<int>("nodeId")).ToList();
        }

        [Cache]
        public IList<Article> GetAllThemeArticles(int id)
        {
            const string sql = @"
SELECT 
	a.OID,
	o.objectID,
	a.header,
	a.annotation,
	a.pubDate,
	ISNULL(image.binData, '3b57f372-ea1b-4bd6-ae08-1841bf0f8fdb') [image],
	isnull(image.width, 0) width,
	isnull(image.height, 0) height,
	(select tagName 'tag' from t_articletags at where at.oid = a.oid order by at.ordValue for xml path(''), root('tags')) tags,
	oc.date,
	oc.cnt
FROM 
	t_Article a 
	join t_object o on a.OID = o.OID
	inner join t_ObjectThemes ot on a.OID = ot.OID
	inner join t_Nodes n on n.object = ot.themeOID and n.nodeID = @id
	outer apply (select top 1 binData, width, height from t_ObjectImage oi inner join t_binarydata b on oi.binData = b.OID and oi.ordValue = 'Для списка' where a.OID = oi.OID) image    
ORDER BY
	a.pubDate desc
";
            return sql.ExecSql(new { id }).Select(row => new Article
            {
                OID = row.Field<Guid>("OID"),
                Id = row.Field<int>("objectID"),
                Header = row.Field<string>("header").ToHtml(),
                Annotation = row.Field<string>("annotation").Cut(200, string.Empty, string.Empty).CrLf2Br().ToHtml(),
                PubDate = row.Field<DateTime>("pubDate"),
                Image =
                    new Image
                    {
                        OID = row.Field<Guid>("image"),
                        Width = row.Field<int>("width"),
                        Height = row.Field<int>("height")
                    },
                Tags =
                    !string.IsNullOrEmpty(row.Field<string>("tags"))
                        ? XElement.Parse(row.Field<string>("tags")).Elements("tag").Select(x => x.Value).ToList()
                        : null
            }).ToList();
        }

        public IList<Article> GetArticles(int articleType, int skip, int take, string sort, out int total)
        {
            IList<Article> allArticles = GetAllArticles(articleType);
            total = allArticles.Count;
            IEnumerable<Article> result = allArticles;
            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "dateUp":
                        result = result.AsEnumerable().OrderBy(v => v.PubDate);
                        break;
                    case "dateDown":
                        result = result.AsEnumerable().OrderByDescending(v => v.PubDate);
                        break;
                }
            }
            return result.Skip(skip).Take(take).ToList();
        }

        public IList<Article> GetThemeArticles(int id, int skip, int take, out int total)
        {
            IList<Article> allThemeArticles = GetAllThemeArticles(id);
            total = allThemeArticles.Count;
            return allThemeArticles.Skip(skip).Take(take).ToList();
        }

        public IList<Comment> GetComments(Guid oid)
        {
            const string sql = @"
select oco.dateCreate, oc.oid, oc.selfComment, p.firstname
from t_objectcomment oc join t_object oco on oc.oid = oco.oid join t_people p on oc.peopleOID = p.OID
where oc.object = @oid
order by oco.dateCreate desc
";
            return sql.ExecSql(new { oid }).Select(row => new Comment
            {
                OID = row.Field<Guid>("oid"),
                DateCreate = row.Field<DateTime>("dateCreate"),
                FirstName = row.Field<string>("firstname"),
                SelfComment = row.Field<string>("selfComment")
            }).ToList();
        }

        public void AddComment(string userName, string new_com, Guid oid)
        {
            const string sql = @"
declare @peopleOID uniqueidentifier = (select top 1 [OID] from [dbo].[t_People] [p] with (nolock) where [eMail] = @userName);
if (@peopleOID is not null)
begin
	declare @new_oid uniqueidentifier = newid();

	insert t_object (oid, cid)
	values (@new_oid, '6BC76A6A-2BB5-4813-953C-BA8E2382C2B6');

	insert t_objectcomment (oid, object, peopleOID, selfComment)
	values (@new_oid, @oid, @peopleOID, @new_com);
end
";
            var prms = new Hashtable { { "userName", userName }, { "new_com", new_com }, { "oid", oid } };
            DBHelper.ExecuteCommand(sql, prms, false);
        }

        public Article Prepare4Mail(Article article, string siteUrl)
        {
            XElement body = XElement.Parse(string.Format("<doc>{0}</doc>", article.Body));
            SetAttribute(body, siteUrl, "href");
            SetAttribute(body, siteUrl, "src");
            return new Article
            {
                Header = article.Header,
                Body = new HtmlString(body.InnerXml())
            };
        }

        [Cache]
        public Article GetSeoArticle(int categoryId, Guid? manufacturerOID, Action<Article> process)
        {
            const string sql = @"
SELECT sa.articleOID, sa.manufacturerOID
FROM
	t_ThemeSeoArticles sa
	inner join t_Nodes n on n.object = sa.OID WHERE nodeID = @categoryId and n.tree = @catalogTree
ORDER BY
	sa.manufacturerOID, sa.ordValue
";
            List<DataRow> items = sql.ExecSql(new { categoryId, Settings.Default.CatalogTree }).ToList();
            Guid articleOID;
            if (manufacturerOID.HasValue && manufacturerOID != Guid.Empty)
                articleOID = items
                    .Where(row => row.Field<Guid?>("manufacturerOID") == manufacturerOID.Value)
                    .Select(row => row.Field<Guid>("articleOID"))
                    .FirstOrDefault();
            else
            {
                articleOID =
                    items.Where(row => !row.Field<Guid?>("manufacturerOID").HasValue)
                        .Select(row => row.Field<Guid>("articleOID"))
                        .FirstOrDefault();
            }

            return GetArticle(articleOID, process);
        }

        private static void SetAttribute(XContainer body, string siteUrl, string attributeName)
        {
            body.Descendants()
                .Where(e => e.Attribute(attributeName) != null)
                .ToList()
                .ForEach(e =>
                {
                    XAttribute attr = e.Attribute(attributeName);
                    string attrValue = attr.Value;
                    if (attrValue.IndexOf(":") != -1) return; // ссылка не содержит протокола (или порта)
                    e.Attribute(attributeName).Value = string.Format("{0}{1}{2}", siteUrl,
                        attrValue.StartsWith("/") ? "" : "/",
                        attrValue);
                });
        }
    }
}