using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using NoName.Domain;
using NoName.Repository.Contracts;
using NLog;
using NoName.Repository.Properties;

namespace NoName.Repository
{
    public class MailRepository : IMailRepository
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public void RequestOperator(string phoneNumber, string requestMail)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    using (MailMessage mailMessage = BuildRequestOperatorMessage(phoneNumber, requestMail))
                    {
                        log.Info("RequestOperatorMessage sending: {0}, phone number: {1}", requestMail, phoneNumber);
                        smtpClient.Send(mailMessage);
                        log.Info("RequestOperatorMessage sent: {0}, phone number: {1}", requestMail, phoneNumber);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("BuildRequestOperatorMessage error, Message: {0}\r\nStackTrace: {1}\r\nInnerException: {2}\r\nInnerInnerException: {3}", e.Message, e.StackTrace,
                    e.InnerException?.Message ?? "", e.InnerException?.InnerException?.Message ?? "");
            }
        }

        public void ReportAvailable(GoodsItem goods, string phone, string email, string requestEmail)
        {

            using (var smtpClient = new SmtpClient())
            {
                try
                {
                    using (MailMessage mailMessage = BuildReportAvailableMessage(goods, phone, email, requestEmail))
                    {
                        log.Info("ReportAvailable sending: {0}, {1}", phone, email);
                        smtpClient.Send(mailMessage);
                        log.Info("ReportAvailable sent: {0}, {1}", phone, email);
                    }
                }
                catch (Exception e)
                {
                    log.Error("BuildOrderOperatorMessage error, Message: {0}\r\nStackTrace: {1}\r\nInnerException: {2}\r\nInnerInnerException: {3}", e.Message, e.StackTrace,
                        e.InnerException?.Message ?? "", e.InnerException?.InnerException?.Message ?? "");
                }
            }
        }

        public void SendFormRefuse(FormRefuse model, string requestMail)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    using (MailMessage mailMessage = BuildFormRefuseMessage(model, requestMail))
                    {
                        log.Info("SendFormRefuse sending: {0}", model.FIO);
                        smtpClient.Send(mailMessage);
                        log.Info("SendFormRefuse sent: {0}", model.FIO);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("SendFormRefuse error, Message: {0}\r\nStackTrace: {1}\r\nInnerException: {2}\r\nInnerInnerException: {3}", e.Message, e.StackTrace,
                    e.InnerException?.Message ?? "", e.InnerException?.InnerException?.Message ?? "");
            }

            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    using (MailMessage mailMessage = BuildFormRefuseClientMessage(model))
                    {
                        log.Info("ClientSendFormRefuse sending: {0}", model.EMail);
                        smtpClient.Send(mailMessage);
                        log.Info("ClientSendFormRefuse sent: {0}", model.EMail);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ClientSendFormRefuse error, Message: {0}\r\nStackTrace: {1}\r\nInnerException: {2}\r\nInnerInnerException: {3}", e.Message, e.StackTrace,
                    e.InnerException?.Message ?? "", e.InnerException?.InnerException?.Message ?? "");
            }
        }

        public void SendOrderInfo(Order order, string orderMail, string siteUrl, string clientMailBody)
        {
            using (var smtpClient = new SmtpClient())
            {
                try
                {
                    using (MailMessage mailMessage = BuildOrderOperatorMessage(order, orderMail))
                    {
                        log.Info("OperatorMessage sending: {0}", orderMail);
                        smtpClient.Send(mailMessage);
                        log.Info("OperatorMessage sent: {0}", orderMail);
                    }
                }
                catch (Exception e)
                {
                    log.Error("BuildOrderOperatorMessage error, Message: {0}\r\nStackTrace: {1}\r\nInnerException: {2}\r\nInnerInnerException: {3}", e.Message, e.StackTrace,
                        e.InnerException?.Message ?? "", e.InnerException?.InnerException?.Message ?? "");
                }

                try
                {
                    if (Settings.Default.EnableClientMail)
                    {
                        using (MailMessage mailMessage = BuildOrderClientMessage(order, siteUrl, clientMailBody))
                        {
                            log.Info("ClientMessage sending: {0}", order.EMail);
                            smtpClient.Send(mailMessage);
                            log.Info("ClientMessage sent: {0}", order.EMail);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error("BuildOrderClientMessage error, Message: {0}\r\nStackTrace: {1}\r\nInnerException: {2}\r\nInnerInnerException: {3}", e.Message, e.StackTrace,
                        e.InnerException?.Message ?? "", e.InnerException?.InnerException?.Message ?? "");
                }

            }
        }

        private static MailMessage BuildRequestOperatorMessage(string phoneNumber, string requestEmail)
        {
            var body = new StringBuilder();
            body.AppendLine("Пришёл запрос на вызов оператора");
            body.AppendFormat("Телефон: {0}\r\n", phoneNumber);
            string toMail = requestEmail;
            var msg = new MailMessage();
            IEnumerable<MailAddress> mails = (from item in toMail.Split(";".ToCharArray())
                                              where !string.IsNullOrWhiteSpace(item)
                                              select new MailAddress(item));
            foreach (MailAddress item in mails)
                msg.To.Add(item);
            msg.Subject = "Пришёл запрос на вызов оператора";
            msg.Body = body.ToString();
            return msg;
        }

        private MailMessage BuildFormRefuseMessage(FormRefuse model, string requestEmail)
        {
            var body = new StringBuilder();
            body.AppendLine("Пришёл запрос на возврат товара");
            body.AppendFormat("ФИО: {0}\r\n", model.FIO);
            body.AppendFormat("Телефон: {0}\r\n", model.Phone);
            body.AppendFormat("EMail: {0}\r\n", model.EMail);
            body.AppendFormat("Адрес: {0}\r\n", model.Address);
            body.AppendFormat("Куплен: {0}\r\n", model.DateOccur);
            body.AppendFormat("Название товара: {0}\r\n", model.GoodsName);
            body.AppendFormat("Номер чека: {0}\r\n", model.BillNumber);
            body.AppendFormat("Описание: {0}\r\n", model.Description);
            body.AppendFormat("Требование: {0}\r\n", model.Requirement);
            string toMail = requestEmail;
            var msg = new MailMessage();
            IEnumerable<MailAddress> mails = (from item in toMail.Split(";".ToCharArray())
                                              where !string.IsNullOrWhiteSpace(item)
                                              select new MailAddress(item));
            foreach (MailAddress item in mails)
                msg.To.Add(item);
            msg.Subject = "Запрос в разделе \"Возврат и обмен товара\" - Unixmart.ru";
            msg.Body = body.ToString();
            if (model.Attachments != null && model.Attachments.Any())
            {
                foreach (var file in model.Attachments.Where(a => a != null))
                {
                    file.InputStream.Position = 0;
                    msg.Attachments.Add(new Attachment(file.InputStream, file.FileName, file.ContentType));
                }
            }

            return msg;
        }

        private MailMessage BuildFormRefuseClientMessage(FormRefuse model)
        {
            var body = new StringBuilder();
            body.AppendLine("Уважаемый покупатель.");
            body.AppendLine("В раздел нашего сайта \"Возврат и обмен\" от Вашего имени поступило обращение.");
            body.AppendLine("Незамедлительно сотрудники нашей компании приступают к его обработке.");
            body.AppendLine("В установленный законом срок Ваши законные требования (при наличии) будут удовлетворены либо Вам будет направлен мотивированный ответ.");
            body.AppendLine("Узнать текущую информацию по своему обращению Вы можете по тел: 84956275707, через день после его оформления.");
            var msg = new MailMessage();
            msg.To.Add(model.EMail);
            msg.Subject = "Запрос в разделе \"Возврат и обмен товара\" - Unixmart.ru";
            msg.Body = body.ToString();
            return msg;
        }

        private static MailMessage BuildReportAvailableMessage(GoodsItem goods, string phoneNumber, string email, string requestEmail)
        {
            var body = new StringBuilder();
            body.AppendLine("Пришёл запрос 'Сообщить о поступлении'");
            body.AppendFormat("Телефон: {0}\r\n", phoneNumber);
            body.AppendFormat("EMail: {0}\r\n", email);
            body.AppendFormat("Товар: {0}\r\n", goods.FullName);
            body.AppendFormat("Псевдокод: {0}\r\n", goods.PseudoID);
            string toMail = requestEmail;
            var msg = new MailMessage();
            IEnumerable<MailAddress> mails = (from item in toMail.Split(";".ToCharArray())
                                              where !string.IsNullOrWhiteSpace(item)
                                              select new MailAddress(item));
            foreach (MailAddress item in mails)
                msg.To.Add(item);
            msg.Subject = "Пришёл запрос 'Сообщить о поступлении'";
            msg.Body = body.ToString();
            return msg;
        }

        private static MailMessage BuildOrderOperatorMessage(Order order, string orderMail)
        {
            var body = new StringBuilder();
            var subject = order.PayType == 2 ?
                $"На сайте зарегистрирован новый заказ с оплатой пластиковой картой на сайте, код авторизации: {order.ApprovalCode}" :
                "На сайте зарегистрирован новый заказ";
            body.AppendLine(subject + "<br />");
            body.AppendFormat("Заказ {0}, дата создания {1:dd.MM.yyyy HH:mm}<br />", order.OrderNum, order.DateCreate);
            body.AppendLine("<br />");
            body.AppendLine("Параметры заказа:<br />");
            body.AppendLine("<br />");
            body.AppendLine("Заказанные товары:<br />");
            foreach (OrderContent item in order.Items)
            {
                body.AppendFormat("{0}\t\t{1}шт.\t\t{2:#,0.## р\\.}<br />", item.FullName, item.Amount, item.Amount * item.Price);
            }
            body.AppendLine("<br />");
            if (order.Coupon != null)
            {
                body.AppendFormat("Купон: {0}<br />", order.Coupon.PromoCode);
                body.AppendLine("<br />");
            }

            body.AppendLine("Способ доставки:<br />");
            body.AppendFormat("{0}\t\t{1:#,0.## р\\.}<br />", order.DeliveryName, order.DeliveryPrice);
            if (order.CalcDistance != 0)
            {
                body.AppendLine("Рассчётное расстояние:<br />");
                body.AppendFormat("{0} км.<br />", order.CalcDistance);
            }
            body.AppendLine("<br />");
            if (order.Coupon != null)
            {
                body.AppendFormat("Промо код: {0}<br />", order.Coupon.PromoCode);
                body.AppendLine("<br />");
            }
            body.AppendLine("Общая стоимость заказа:<br />");
            body.AppendFormat("{0:#,0.## р\\.}<br />", order.TotalPrice);
            body.AppendLine("<br />");
            body.AppendLine("Адрес доставки:<br />");
            if (!string.IsNullOrWhiteSpace(order.City))
                body.AppendFormat("Город: {0}<br />", order.City);
            body.AppendFormat("Улица: {0}<br />", order.Street);
            body.AppendFormat("Ближайшее метро: {0}<br />", order.MetroName);
            body.AppendFormat("Дом: {0}<br />", order.House);
            body.AppendFormat("Корпус: {0}<br />", order.Korp);
            body.AppendFormat("Строение: {0}<br />", order.Build);
            body.AppendFormat("Квартира: {0}<br />", order.Flat);
            body.AppendLine("<br />");
            body.AppendLine("Заказчик:<br />");
            body.AppendFormat("Контактный телефон: {0}<br />", order.Phone);
            body.AppendFormat("Контактное лицо: {0}<br />", order.Person);
            body.AppendFormat("E-Mail: {0}<br />", order.EMail);
            body.AppendLine("<br />");
            body.AppendLine("Способ оплаты:<br />");
            if (order.PayType == 2)
            {
                body.AppendFormat("<strong style=\"color: red;\">{0}, код авторизации: {1}</strong><br />", order.PayName, order.ApprovalCode);
            }
            else
            {
                body.AppendFormat("{0}<br />", order.PayName);
            }

            body.AppendFormat("Комментарий: {0}<br />", order.Comments);
            body.AppendLine("Источник заказа:<br />");
            body.AppendFormat("{0}<br />", order.RefererName);


            string toMail = orderMail;
            var msg = new MailMessage();
            try
            {
                IEnumerable<MailAddress> mails = (from item in toMail.Split(";".ToCharArray())
                                                  where !string.IsNullOrWhiteSpace(item)
                                                  select new MailAddress(item));
                foreach (MailAddress item in mails) msg.To.Add(item);
            }
            catch
            {
                throw new Exception("Проблема с почтовым адресом: " + toMail);
            }
            msg.Subject = "На сайте зарегистрирован новый заказ";
            msg.IsBodyHtml = true;
            msg.Body = body.ToString();
            return msg;
        }

        private static MailMessage BuildOrderClientMessage(Order order, string siteUrl, string clientMailBody)
        {
            string toMail = order.EMail;
            var msg = new MailMessage();
            try
            {
                IEnumerable<MailAddress> mails = (from item in toMail.Split(";".ToCharArray())
                                                  where !string.IsNullOrWhiteSpace(item)
                                                  select new MailAddress(item));
                foreach (MailAddress item in mails) msg.To.Add(item);
            }
            catch
            {
                throw new Exception("Проблема с почтовым адресом: " + toMail);
            }
            msg.Subject = string.Format("Заказ на сайте {0}", siteUrl);

            if (string.IsNullOrWhiteSpace(clientMailBody))
            {
                var body = new StringBuilder();
                body.AppendFormat("Ваш заказ {0}, поступил к нашим операторам в {1:HH:mm МСК}\r\n", order.OrderNum,
                    order.DateCreate);
                body.AppendLine(
                    "В ближайшее время, наш менеджер свяжется с Вами по телефону для уточнения удобного времени доставки.");
                body.AppendLine("");
                body.AppendLine("Параметры заказа:");
                body.AppendLine("");
                body.AppendLine("Заказанные товары:");
                foreach (OrderContent item in order.Items)
                {
                    body.AppendFormat("{0}\t\t{1}шт.\t\t{2:#,0.## р\\.}\r\n", item.FullName, item.Amount, item.Amount * item.Price);
                }
                body.AppendLine("");
                body.AppendLine("Способ доставки:");
                body.AppendFormat("{0}\t\t{1:#,0.## р\\.}\r\n", order.DeliveryName, order.DeliveryPrice);
                body.AppendLine("");
                if (order.Coupon != null)
                {
                    body.AppendFormat("Промо код: {0}\r\n", order.Coupon.PromoCode);
                    body.AppendLine("");
                }
                body.AppendLine("Общая стоимость заказа:");
                body.AppendFormat("{0:#,0.## р\\.}\r\n", order.TotalPrice);
                body.AppendLine("");
                body.AppendLine("Адрес доставки:");
                if (!string.IsNullOrWhiteSpace(order.City))
                    body.AppendFormat("Город: {0}\r\n", order.City);
                body.AppendFormat("Улица: {0}\r\n", order.Street);
                body.AppendFormat("Ближайшее метро: {0}\r\n", order.MetroName);
                body.AppendFormat("Дом: {0}\r\n", order.House);
                body.AppendFormat("Корпус: {0}\r\n", order.Korp);
                body.AppendFormat("Строение: {0}\r\n", order.Build);
                body.AppendFormat("Квартира: {0}\r\n", order.Flat);
                body.AppendLine("");
                body.AppendLine("Заказчик:");
                body.AppendFormat("Контактный телефон: {0}\r\n", order.Phone);
                body.AppendFormat("Контактное лицо: {0}\r\n", order.Person);
                body.AppendFormat("E-Mail: {0}\r\n", order.EMail);
                body.AppendLine("");
                body.AppendLine("Способ оплаты:");
                body.AppendFormat("{0}\r\n", order.PayName);
                body.AppendFormat("Комментарий: {0}\r\n", order.Comments);
                clientMailBody = body.ToString();
            }
            else msg.IsBodyHtml = true;

            msg.Body = clientMailBody;
            return msg;
        }
    }
}