﻿using System;
using System.Collections.Generic;

namespace NoName.Domain
{
	public class MasterCatalog
	{
		public Guid OID { get; set; }
		public string Name { get; set; }
		public int ShopType { get; set; }
		public decimal AddPricePercent { get; set; }
		public IDictionary<Guid, decimal> AddPricePercents { get; set; }
	}
}
