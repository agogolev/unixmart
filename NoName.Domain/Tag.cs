﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Domain
{
    public class Tag
    {
        public int OrdValue { get; set; }
        public string Name { get; set; }
        public string ShortLink { get; set; }
    }
}
