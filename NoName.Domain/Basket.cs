﻿using System;
using System.Linq;
using System.Collections.Generic;
using MetaData;

namespace NoName.Domain
{
	public class Basket
	{
		public Guid OID { get; set; }
        public Guid? OrderOID { get; set; }
        public Guid Session { get; set; }
        public int? DeliveryType { get; set; }
        public int? PayType { get; set; }
        public bool NeedCityInOrder { get; set; }
        public bool InstantDeliveryPossible { get; set; }
        public decimal VariableDeliveryPart { get; set; }
        public decimal DeliveryPrice { get; set; }
        public decimal DeliveryPriceWithoutDistance { get; set; }
        public int CalcDistance { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string StreetName { get; set; }
        public string MetroName { get; set; }
        public string House { get; set; }
        public string Korp { get; set; }
        public string Build { get; set; }
        public string Flat { get; set; }
        public string Comments { get; set; }
        public int Referer { get; set; }
        public Coupon Coupon { get; set; }

	    public decimal Discount
	    {
	        get
	        {
                return Items.Sum(g => g.Amount * (Coupon != null && Coupon.AffectedGoods.Any(ag => ag.OID == g.OID) ? Coupon.Discount : 0));
	        }
	    }

	    public IList<BasketContent> Items { get; set; }

        public int? LastAddedItem { get; set; }

        public int TotalGoodsCount
		{
			get
			{
				return Items?.Sum(k => k.Amount) ?? 0;
			}
		}

		public decimal TotalPrice
		{
			get
			{
				decimal totalPrice = 0;
				if (Items != null) foreach (BasketContent item in Items) totalPrice += item.TotalPrice;
				return totalPrice;
			}
		}

	    public bool HasOnlinePay
	    {
	        get { return Items.Any(c => c.OnlinePay); }
	    }
    }
}
