﻿using System;
using System.Collections.Generic;

namespace NoName.Domain
{
	public class Store
	{
		public Guid OID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public string VirtualTourUrl { get; set; }
		public int RegionID { get; set; }
		public bool HasGallery { get; set; }
	}
}
