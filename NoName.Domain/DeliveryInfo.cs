﻿using System.Collections.Generic;
using System.Web;

namespace NoName.Domain
{
	public class DeliveryInfo
	{
		//public bool ShowInstantDelivery { get; set; }
		public bool InstantDelivery { get; set; }
	    public decimal Price { get; set; }
	    public decimal PriceSelf { get; set; }

        public IList<DeliveryInfoItem> Items { get; set; }
	    public string GoodsName { get; set; }
	    //public Article DeliveryArticle { get; set; }
	}
}
