﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Domain
{
    public class TitleData
    {
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
    }
}
