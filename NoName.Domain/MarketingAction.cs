﻿using System;
using System.Collections.Generic;

namespace NoName.Domain
{
	public class MarketingAction
	{
        public Guid OID { get; set; }

        public int Id { get; set; }
		
        public Image HeaderImage { get; set; }
		
        public string Name { get; set; }
		
        public DateTime DateBegin { get; set; }
		
        public DateTime? DateEnd { get; set; }

	    public string Period
	    {
	        get
	        {
	            return DateEnd.HasValue
	                ? string.Format("{0:dd MMMM yyyy} - {1:dd MMMM yyyy}", DateBegin, DateEnd.Value)
                    : DateBegin.ToString("dd MMMM yyyy");
	        }
	    }
        
        public DateTime DateModify { get; set; }
        
        public bool IsPublic { get; set; }
		
        public Article Description { get; set; }
		
        public IList<GoodsItem> Goods { get; set; }
        
        public bool GeoInclusive { get; set; }
        
        public IList<int> GeoLocations { get; set; }
        
        public string Title { get; set; }

        public string Keywords { get; set; }

        public string PageDescription { get; set; }
	}
}
