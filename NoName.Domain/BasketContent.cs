﻿using System;
using NoName.Extensions;

namespace NoName.Domain
{
    public class BasketContent
    {
        public Guid OID { get; set; }
        private string _fullName;
        private string _itemUrl;
        public string ProductType { get; set; }

        public string ManufacturerName { get; set; }

        public string PseudoID { get; set; }

        public string Model { get; set; }

        public string ItemUrl
        {
            get
            {
                return _itemUrl ?? (
                    _itemUrl = GoodsHelper.ShortNameEncoded(ProductType, ManufacturerName, Model));
            }
        }

        public string FullName
        {
            get { return _fullName ?? (_fullName = GoodsHelper.FullName(ProductType, ManufacturerName, Model)); }
        }

        public string ColorUrl { get; set; }

        public int ObjectId { get; set; }

        public Image Image { get; set; }

        public decimal OriginalPrice { get; set; }

        public string OriginalPriceString => $@"{OriginalPrice:# ##0} руб.";

        public decimal Price { get; set; }

        public bool IsSale { get; set; }

        public string PriceString => $@"{Price:# ##0} руб.";
        public bool OnlinePay { get; set; }

        public int OrdValue { get; set; }

        public Guid Goods { get; set; }

        public int Amount { get; set; }

        public decimal TotalPrice
        {
            get { return Price * Amount; }
        }

        public decimal TotalOriginalPrice
        {
            get { return OriginalPrice * Amount; }
        }

        public string TotalPriceString => $@"{TotalPrice:# ##0} руб.";

        public string TotalOriginalPriceString => $@"{TotalOriginalPrice:# ##0} руб.";

        public decimal DeliveryFirst { get; set; }

        public decimal DeliverySecond { get; set; }

        public decimal DeliverySelf { get; set; }
    }
}