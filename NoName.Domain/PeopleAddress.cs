﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NoName.Domain
{
	public class PeopleAddress
	{
		public Guid OID { get; set; }
		public int OrdValue { get; set; }
		public int? PayType { get; set; }

		public string ZipCode { get; set; }
		public string City { get; set; }

		[DisplayName("Ближайшее метро:")]
		public int? MetroId { get; set; }

		public string MetroName { get; set; }

		[DisplayName("Улица: *")]
		public string StreetName { get; set; }

		[DisplayName("Дом: *")]
		public string House { get; set; }

		[DisplayName("Строение:")]
		public string Build { get; set; }

		[DisplayName("Корпус:")]
		public string Korp { get; set; }

		[DisplayName("Подъезд:")]
		public string Podyezd { get; set; }

		[DisplayName("Домофон (или код):")]
		public string Code { get; set; }

		[DisplayName("Этаж:")]
		public string Level { get; set; }

		[DisplayName("Квартира:")]
		public string Flat { get; set; }

		public int LiftId { get; set; }

		public string LiftName { get; set; }

		[DisplayName("Комментарий:")]
		public string Comment { get; set; }

		[DisplayName("Контактный телефон: *")]
		[Required(ErrorMessage = "Не указан контактный телефон")]
		public string Phone { get; set; }

		[DisplayName("Стационарный телефон: *")]
		public string HomePhone { get; set; }

		[DisplayName("Контактное лицо: *")]
		[Required(ErrorMessage = "Не указана контактная персона")]
		public string Person { get; set; }

		[DisplayName("E-Mail: *")]
		[Required(ErrorMessage = "Не указан e-mail")]
		[RegularExpression(@"^([a-zA-Z0-9]+[\w\.-]?)*[a-zA-Z0-9]@([a-zA-Z0-9]+[\w\.-]?)*[a-zA-Z0-9]\.[a-zA-Z]{2,9}$", ErrorMessage = "Неверный EMail")]
		public string Email { get; set; }
	}
}
