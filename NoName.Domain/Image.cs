﻿using System;
using Ecommerce.Extensions;
using NoName.Domain.Properties;

namespace NoName.Domain
{
	public class Image
	{
        public Guid OID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

	    private string NameNormalized
	    {
	        get { return (Name ?? "NoName").Replace("  ", " ").Replace(" ", "-").TranslitEncode(); }
	    }

	    public string Description { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
	    public string MimeType { get; set; }
	    public string Src => Settings.Default.cdn ? $"{Settings.Default.cdnAddress}/img/{Id}-{NameNormalized}{Extension}" : $"/img/{Id}-{NameNormalized}{Extension}";
	    public string Url => $"/image/{OID}/{Width}/{Height}";

	    public int GetWidth(int maxWidth, int maxHeight)
		{
			return GetImageWidth(maxWidth, GetHeight(maxHeight));
		}

        public string Extension
        {
            get
            {
                switch (MimeType)
                {
                    case "image/jpeg":
                    case "image/pjpeg":
                    case "image/jpg":
                        return ".jpg";
                    case "image/png":
                        return ".png";
                    case "image/gif":
                        return ".gif";
                    default:
                        return "";
                }
            }
        }

        public int GetHeight(int maxHeight)
		{
			return Math.Min(Height, maxHeight);
		}

		public int GetImageWidth(int maxWidth, int maxHeight)
		{
			if (Width <= maxWidth && Height <= maxHeight) return Width;
			if (Height >= maxHeight)
			{
				var newWidth = (int)(Width * ((maxHeight * 1.0) / Height));
				return newWidth > maxWidth ? maxWidth : newWidth;
			}
			return Width > maxWidth ? maxWidth : Width;
		}
	}
}
