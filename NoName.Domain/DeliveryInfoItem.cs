﻿using System;

namespace NoName.Domain
{
	public class DeliveryInfoItem
	{
		public int DeliveryType { get; set; }
		public bool ExpressDelivery { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
		public decimal Price { get; set; }
		public string PriceSuffix { get; set; }
	    public string PriceString => Price <= 0 ? "бесплатно" : $"{Price:#,##0}{PriceSuffix} руб.";
	    public decimal? FixPrice { get; set; }
	    public bool IsCalc { get; set; }
	}
}
