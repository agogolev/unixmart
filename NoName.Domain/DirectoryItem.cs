﻿using System.Collections.Generic;

namespace NoName.Domain
{
	public class DirectoryItem
	{
		public string Header { get; set; }
        public IList<CompanyMenu> Items { get; set; }
	}
}
