﻿using System;

namespace NoName.Domain
{
	public class SVItem
	{
		public int ID { get; set; }
		public string Name { get; set; }
	}
}
