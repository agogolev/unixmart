﻿using System;

namespace NoName.Domain
{
	public class ImageTrio
	{
		public int OrdValue { get; set; }
		public Image SmallImage { get; set; }
		public Image Image { get; set; }
		public Image LargeImage { get; set; }
	}
}
