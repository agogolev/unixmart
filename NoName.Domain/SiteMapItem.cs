﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NoName.Domain.Enums;

namespace NoName.Domain
{
	public class SiteMapItem
	{
		public string Url { get; private set; }
		public DateTime Lastmod { get; private set; }
		public SiteMapItemChangeFreq ChangeFreq { get; private set; }

		/// <summary>
		/// Valid values range from 0.0 to 1.0
		/// </summary>
		public Decimal Priority { get; private set; }

		public SiteMapItem(string url, DateTime lastmod, SiteMapItemChangeFreq changeFreq, Decimal priority)
		{
			if (priority < 0 || priority > 1)
				throw new ArgumentNullException("priority",
												"Invalid value '" + priority +
												"' priority. Valid values range from 0.0 to 1.0.");

			Url = url;
			Lastmod = lastmod;
			ChangeFreq = changeFreq;
			Priority = priority;
		}

		public SiteMapItem(string url, DateTime lastmod, SiteMapItemChangeFreq changeFreq)
			: this(url, lastmod, changeFreq, 0.5m)
		{
		}

		public SiteMapItem(string url, DateTime lastmod)
			: this(url, lastmod, SiteMapItemChangeFreq.None, 0.5m)
		{
		}

		public SiteMapItem(string url)
			: this(url, DateTime.MinValue, SiteMapItemChangeFreq.None, 0.5m)
		{
		}
	}
}
