﻿using System.Collections.Generic;
using System.Linq;
using MetaData;

namespace NoName.Domain.Extensions
{
	public static class SearchedGoodsExtension
	{
		public static IEnumerable<GoodsItem> DistinctGoods(this IEnumerable<SearchedGoods> items)
		{
			var ret = new List<GoodsItem>();
			items.Distinct(new LambdaComparer<SearchedGoods>((x, y) => x.ObjectID == y.ObjectID)).ToList().ForEach(ret.Add);
			return ret;
		}
	}
}
