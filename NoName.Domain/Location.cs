﻿namespace NoName.Domain
{
	public class Location
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public decimal Lattitude { get; set; }
		public decimal Longitude { get; set; }
		public string Address { get; set; }
	}
}
