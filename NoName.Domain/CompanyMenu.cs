﻿using System;
using Ecommerce.Extensions;

namespace NoName.Domain
{
	public class CompanyMenu
	{
		public Guid OID { get; set; }
		public string CompanyName { get; set; }
	    public string ManufRepresentation { get; set; }
        public int GoodsCount { get; set; }
		public int ItemId { get; set; }
	    public DateTime DateCreate { get; set; }
	}
}
