﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NoName.Domain
{
    public class PayInfoItem
    {
        public int PayType { get; set; }
        public bool Selected { get; set; }
        public HtmlString Name { get; set; }
        public HtmlString Description { get; set; }
        public bool OnlinePay { get; set; }
    }
}
