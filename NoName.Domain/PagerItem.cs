﻿using System;

namespace NoName.Domain
{
	public class PagerItem
	{
		public int PageNumber { get; set; }
		public bool IsCurrent { get; set; }
	}
}
