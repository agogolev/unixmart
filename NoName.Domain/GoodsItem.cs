﻿using System;
using System.Collections.Generic;
using Ecommerce.Domain;
using Ecommerce.Extensions;
using System.Web;
using NoName.Extensions;

namespace NoName.Domain
{
    public class GoodsItem : GoodsItemBase
    {
        public Guid OID { get; set; }
        public string PseudoID { get; set; }
        public int CategoryId { get; set; }
        public Guid? CategoryOID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryNameEncoded { get; set; }
        public int ObjectID { get; set; }
        public string ProductType { get; set; }
        public string Model { get; set; }
        public HtmlString Description { get; set; }
        public HtmlString ShortDescription { get; set; }
        public Guid? Manufacturer { get; set; }
        public string ManufacturerName { get; set; }
        public string ManufacturerAltName { get; set; }
        public string ManufacturerURL { get; set; }
        public Image Image { get; set; }
        public decimal Price { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal OldPrice { get; set; }
        public bool IsNew { get; set; }
        public bool IsHit { get; set; }
        public int HitWeight { get; set; }
        public bool IsSale { get; set; }
        public bool InStock { get; set; }
        public int StockAmount { get; set; }
        public bool Zombi { get; set; }
        public bool OnlinePay { get; set; }
        public bool InPresent => InStock || !Zombi;
        public DeliveryInfo Delivery { get; set; }
        public DateTime DateCreate { get; set; }
        private string _itemUrl;
        public string ItemUrl => _itemUrl ?? (_itemUrl = GoodsHelper.ShortNameEncoded(ProductType, ManufacturerName, Model));
        private string _fullName;
        public string FullName => _fullName ?? (_fullName = GoodsHelper.FullName(ProductType, ManufacturerName, Model));
        public IList<ImageTrio> GoodsImages;
        public IList<Image> Docs;
        public string PriceRub => $@"{Price:0.##}";
        public string PriceString => $@"{Price:# ##0} руб.";
        public string OldPriceString => $@"{OldPrice:# ##0} руб.";
        public string SalePriceString => $@"{SalePrice:# ##0} руб.";
        public string DiscountString => $@"{(Price - OldPrice)/Price:P0}";
        public string PriceCreditString => $@"{Price/10:# ##0} руб.";
        public string PriceStringShort => $@"{Price:# ##0} руб.";
        public string PriceCreditStringShort => $@"{Price/10:# ##0} руб.";
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
    }
}
