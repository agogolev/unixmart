﻿using System;

namespace NoName.Domain
{
	public class ImageMenu : NodeMenu
	{
		public int SortOrder { get; set; }
		public string ImageUrl { get; set; }
		public string ClassName { get; set; }
	}
}
