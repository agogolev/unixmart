﻿using System;

namespace NoName.Domain
{
	public class PromoBlock
	{
		public string Name { get; set; }
		public string Header { get; set; }
		public string Description { get; set; }
		public string Url { get; set; }
		public Image Image { get; set; }
		public bool ImageOnly { get; set; }
	}
}
