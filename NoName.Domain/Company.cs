﻿using System;

namespace NoName.Domain
{
    public class Company
    {
        public Guid OID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string AltName { get; set; }
        public string ShortLink { get; set; }
        public int GoodsCount { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
    }
}