﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NoName.Domain
{
    public class FormRefuse
    {
        public IList<InfoMenu> Menu { get; set; }
        public int SelectedId { get; set; }
        public Article Article { get; set; }
        [Required(ErrorMessage = "Назовите себя")]
        public string FIO { get; set; }
        [Required(ErrorMessage = "Укажите номер телефона")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Укажите Вашу электронную почту")]
        public string EMail { get; set; }
        [Required(ErrorMessage = "Укажите адрес")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Укажите дату покупки")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOccur { get; set; }
        [Required(ErrorMessage = "Укажите название товара")]
        public string GoodsName { get; set; }
        [Required(ErrorMessage = "Укажите номер чека")]
        public string BillNumber { get; set; }
        [Required(ErrorMessage = "Опишите неисправность")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Укажите Ваши требования")]
        public string Requirement { get; set; }

        public string ReCaptcha { get; set; }
        public string WelcomeTitle { get; set; }
        public string WelcomeString { get; set; }
        public IEnumerable<HttpPostedFileBase> Attachments { get; set; }
    }
}
