﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace NoName.Domain
{
    public class PayerType
    {
        public int Type { get; set; }
        public string Name { get; set; }
    }
}
