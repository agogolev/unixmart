﻿using System.Collections.Generic;

namespace NoName.Domain
{
	public class Pager 
    {
		public int NumInBatch { get; set; }
		public int ItemsCount { get; set; }
		public IList<PagerItem> Items { get; set; }
	}
}
