﻿namespace NoName.Domain
{
	public class SearchedGoods : GoodsItem
	{
		public int CatalogId { get; set; }
		public string CatalogName { get; set; }
	}
}
