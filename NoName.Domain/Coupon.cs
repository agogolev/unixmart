﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Domain
{
    public class Coupon
    {
        public Guid OID { get; set; }

        //public string Name { get; set; }

        public string PromoCode { get; set; }
        
        public decimal Discount { get; set; }

        public IList<GoodsItem> AffectedGoods { get; set; }
    }
}
