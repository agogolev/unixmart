﻿using System;
using System.Collections.Generic;

namespace NoName.Domain
{
	public class CatalogMenu
	{
	    public Company SelectedBrand { get; set; }
	    public bool UseBrandUrl { get; set; }
        public Guid OID { get; set; }
		public Image Image { get; set; }
	    public int OrdValue { get; set; }
	    public int NodeId { get; set; }
        public string StringRepresentation { get; set; }
        public int ParentNodeId{ get; set; }
        public string Name { get; set; }
        public string Css1 { get; set; }
        public string Css2 { get; set; }
        public bool HasChild { get; set; }
        public bool Selected { get; set; }
        public IList<CatalogMenu> ChildMenu { get; set; }
        public int GoodsCount { get; set; }
        public int Level { get; set; }
        public DateTime DateCreate { get; set; }
        public string Title { get; set; }
        public string H1 { get; set; }
        public string Keywords { get; set; }
        public string PageDescription { get; set; }
	    public bool IsTag { get; set; }
        public bool GoodsList { get; set; }
    }
}
