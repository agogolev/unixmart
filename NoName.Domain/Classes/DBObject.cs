using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Globalization;
using System.Xml;

namespace NoName.Domain.Classes
{
    public abstract class DBObject
    {
        public DBObject(string connStr)
        {
            _connStr = connStr;
        }
        public DBObject(string connStr, Guid OID)
        {
            _connStr = connStr;
            if (OID != Guid.Empty) InitObject(OID);
        }

        public abstract Guid CID
        {
            get;
        }
        public static Guid ClassCID
        {
            get
            {
                return Guid.Empty;
            }
        }

        public abstract string ClassName
        {
            get;
        }
        public NumberFormatInfo NumberFormat
        {
            get
            {
                NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
                nfi.NumberDecimalSeparator = ".";
                return nfi;
            }
        }
        public DateTimeFormatInfo DateTimeFormat
        {
            get
            {
                DateTimeFormatInfo dtfi = new CultureInfo("ru-RU").DateTimeFormat;
                return dtfi;
            }
        }

#if (ENABLE_SS)
		public string PeopleOID = "";
#endif

        protected string _connStr = "";
        protected XmlElement root;

        private string _nk;
        public string NK
        {
            get
            {
                if (_nk == null)
                {
                    SqlCommand cmd = new SqlCommand("", new SqlConnection(_connStr));
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spNK";
                    cmd.Parameters.AddWithValue("@OID", OID);
                    cmd.Connection.Open();
                    try
                    {
                        _nk = (string)cmd.ExecuteScalar();
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
                return _nk;
            }

        }
        private Guid _OID = Guid.Empty;
        protected Guid _CID;

        //		protected StringBuilder _fieldNames;
        //		protected StringBuilder _fieldValues;
        protected Hashtable _fields;
        //		protected bool _useFieldsXml = false;

        private void InitObject(Guid OID)
        {
            _OID = OID;
            SqlConnection conn = new SqlConnection(_connStr);
            SqlCommand cmd = new SqlCommand("", conn);
            cmd.CommandText = "spGetObject '" + OID + "', 0, 1";
            SqlDataReader dr = null;
            try
            {
                conn.Open();
                dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                if (dr.Read())
                {
                    _CID = (Guid)dr["CID"];
                    BuildFieldValues(dr);
                }
            }
            finally
            {
                /*if(_CID == CID)*/
                dr.Close();
            }
        }

        public abstract void BuildFieldValues(SqlDataReader dr);

        public void LoadObject(Guid OID)
        {
            InitObject(OID);
        }

        public Guid OID
        {
            get { return _OID; }
        }
        public abstract void BuildNameValues();

        /// <summary>
        /// ����� ��� ��������� ������������� �������� ���������� � ���������� 
        /// (!!! ��������� ������, �������� SaveXml(string objectXml), ��������� ���� !!!)
        /// ���� ����� ������������ ����� ���� OID � ���� �������� ���������� � ����������,
        /// ��� ������������� ����� ������ �� ������� ������������ ����������� (string connStr, Guid OID)
        /// </summary>
        /// <param name="OID">OID �������, � ������ ������ ������������� Guid.Empty</param>
        /// <param name="Conn">����������</param>
        /// <param name="Trans">����������</param>
        public void SetConnTrans(Guid OID, SqlConnection Conn, SqlTransaction Trans)
        {
            _OID = OID;
            _conn = Conn;
            _trans = Trans;
        }

        private SqlConnection _conn;
        private SqlTransaction _trans;

        public void LoadXml(string objectXml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(objectXml);
            root = doc.DocumentElement;
            LoadSingleProp();
        }
        /// <summary>
        /// ������� �����
        /// </summary>
        /// <param name="objectXml"></param>
        /// <returns></returns>
        public string SaveXml(string objectXml)
        {
            string result = null;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(objectXml);
            root = doc.DocumentElement;
            LoadSingleProp();
            _conn = new SqlConnection(_connStr);
            _conn.Open();
            _trans = _conn.BeginTransaction();
#if (ENABLE_SS)
			IInterProcessConnection clientConnection = null;
#endif
            try
            {
                ProcessProcedures("BeforeSave");
                Save();

#if (ENABLE_SS)
				//���� ������ ������� withLock � ��������� yes, �� ����� ���������� ������� ���������� ��� �������������
				if(PeopleOID != null && PeopleOID != "" && doc.DocumentElement.Attributes.GetNamedItem("withLock") != null && doc.DocumentElement.Attributes.GetNamedItem("withLock").Value == "yes")
				{
					//���������� ������
					clientConnection = new ClientPipeConnection("ISMPipe", ".");
					clientConnection.Connect();
			
					clientConnection.Write(JSONMessages.LockObject(_OID.ToString(),this.CID.ToString(),PeopleOID).ToString());
					JSONObject jsonResp = new JSONObject(clientConnection.Read());
					clientConnection.Close();
					//���������� ���������������� ����������� ���������
					if(jsonResp.getString("TypeOfMessage") == "Exception" || jsonResp.getString("TypeOfMessage") == "Error")
						throw new Exception(jsonResp.getString("Text"));
				}
#endif

                ProcessMultiProp();
                ProcessExtMultiProp();
                ProcessProcedures("AfterSave");

                //��������� ����� dateModify - ���� ����
                XmlNode n = root.SelectSingleNode("singleProp/dateModify");
                if (n != null)
                    result = "<dateModify>" + GetLastModifyDate() + "</dateModify>";
                _trans.Commit();
            }
            catch (Exception e)
            {
#if (ENABLE_SS)
				if(clientConnection != null)
					clientConnection.Dispose();
#endif
                _trans.Rollback();
                throw e;
            }
            finally
            {
                _conn.Close();
            }
            return result;
        }

        /// <summary>
        /// ����� � ������� �����������
        /// </summary>
        /// <param name="objectXml"></param>
        /// <param name="conn"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public string SaveXml(string objectXml, SqlConnection conn, SqlTransaction trans)
        {
            string result = null;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(objectXml);
            root = doc.DocumentElement;
            LoadSingleProp();
            _conn = conn;
            _trans = trans;
#if (ENABLE_SS)
			IInterProcessConnection clientConnection = null;
#endif
            try
            {
                Save();

#if (ENABLE_SS)
				//���� ������ ������� withLock � ��������� yes, �� ����� ���������� ������� ���������� ��� �������������
				if(PeopleOID != null && PeopleOID != "" && doc.DocumentElement.Attributes.GetNamedItem("withLock") != null && doc.DocumentElement.Attributes.GetNamedItem("withLock").Value == "yes")
				{
					//���������� ������
					clientConnection = new ClientPipeConnection("ISMPipe", ".");
					clientConnection.Connect();
			
					clientConnection.Write(JSONMessages.LockObject(_OID.ToString(),this.CID.ToString(),PeopleOID).ToString());
					JSONObject jsonResp = new JSONObject(clientConnection.Read());
					clientConnection.Close();
					//���������� ���������������� ����������� ���������
					if(jsonResp.getString("TypeOfMessage") == "Exception" || jsonResp.getString("TypeOfMessage") == "Error")
						throw new Exception(jsonResp.getString("Text"));
				}
#endif

                ProcessMultiProp();
                ProcessExtMultiProp();

                //��������� ����� dateModify - ���� ����
                XmlNode n = root.SelectSingleNode("singleProp/dateModify");
                if (n != null)
                    result = "<dateModify>" + GetLastModifyDate() + "</dateModify>";
            }
            catch (Exception e)
            {
#if (ENABLE_SS)
				if(clientConnection != null)
					clientConnection.Dispose();
#endif
                throw e;
            }
            return result;
        }

        private string GetLastModifyDate()
        {//������� ���������� ����� dateModify
            string sql = @"SELECT dateModify FROM t_Object WHERE OID='" + _OID.ToString() + "'";
            sql = ((DateTime)ExecScalar(sql)).ToString("dd.MM.yyyy HH:mm:ss.fff");
            return sql;
        }

        protected virtual void LoadSingleProp()
        {
            if (new Guid(root.SelectSingleNode("singleProp/CID").InnerText) != CID) throw new Exception("SaveXml: Invalid object class");
            if (root.Attributes["OID"] != null && root.Attributes["OID"].Value != String.Empty)
            {
                _OID = new Guid(root.Attributes["OID"].Value);
            }
        }

        protected abstract void ProcessMultiProp();

        private void ProcessExtMultiProp()
        {
            XmlNodeList extMultiPropNodes = root.SelectSingleNode("extMultiProp").ChildNodes;
            if (extMultiPropNodes.Count > 0)
            {
                foreach (XmlNode extMultiPropNode in extMultiPropNodes)
                {
                    string propName = extMultiPropNode.Name;
                    XmlDocument doc = new XmlDocument();
                    XmlElement rt = doc.CreateElement("root");
                    rt.InnerXml = extMultiPropNode.InnerXml;

                    string processContent = "<?xml version=\"1.0\" encoding=\"Windows-1251\" ?>" + rt.OuterXml;
                    DBReader.CollectionAdapter ca = new DBReader.CollectionAdapter(_OID.ToString(), ((CObject)this).ClassName, propName, processContent);
                    ca.AcceptChanges(_conn, _trans);
                }
            }
        }

        public void ProcessExtMultiProp(XmlNode extMultiPropNode)
        {
            if (extMultiPropNode != null)
            {
                string propName = extMultiPropNode.Name;
                XmlDocument doc = new XmlDocument();
                XmlElement rt = doc.CreateElement("root");
                rt.InnerXml = extMultiPropNode.InnerXml;

                string processContent = "<?xml version=\"1.0\" encoding=\"Windows-1251\" ?>" + rt.OuterXml;
                DBReader.CollectionAdapter ca = new DBReader.CollectionAdapter(_OID.ToString(), ((CObject)this).ClassName, propName, processContent);
                ca.AcceptChanges(_conn, _trans);
            }
        }

        /// <summary>
        /// ���������� ���������� �������� ��������
        /// </summary>
        protected void ProcessProcedures(string strRunTime)
        {
            XmlNodeList procedureNodes = root.SelectSingleNode("procs").ChildNodes;
            if (procedureNodes.Count > 0)
            {
                foreach (XmlNode procedureNode in procedureNodes)
                {
                    if (procedureNode.Attributes.GetNamedItem("runtime").Value != strRunTime)
                        continue;

                    string procName = procedureNode.Name;
                    string strParams = "";
                    foreach (XmlNode paramNode in procedureNode)
                    {
                        if (paramNode.Name == "param")
                            strParams += paramNode.InnerText + ",";
                        else if (paramNode.Name == "field")
                            if (paramNode.InnerText == "OID")
                                strParams += "'" + _OID.ToString() + "',";
                    }

                    if (strParams.Length > 0)
                        strParams = strParams.Substring(0, strParams.Length - 1);

                    string sqlProc = "exec " + procName + " " + strParams;
                    ExecSQL(sqlProc);
                }
            }
        }

        public void Save()
        {
            _fields = new Hashtable();

            BuildNameValues();
            if (_fields.Count > 0)
            {
                //������� xml, � ����� �������� �������� ��� ���� � ��������
                XmlDocument fieldsXml = new XmlDocument();
                fieldsXml.LoadXml("<?xml version=\"1.0\"?><fields></fields>");
                XmlElement fieldNode;
                XmlElement innerNode;
                foreach (DictionaryEntry de in _fields)
                {
                    fieldNode = fieldsXml.CreateElement("field");
                    innerNode = fieldsXml.CreateElement("name");
                    innerNode.InnerText = (string)de.Key;
                    fieldNode.AppendChild(innerNode);
                    innerNode = fieldsXml.CreateElement("value");
                    innerNode.InnerText = (string)de.Value;
                    fieldNode.AppendChild(innerNode);
                    fieldsXml.DocumentElement.AppendChild(fieldNode);
                }

                String sql = "spUpdateObject ";
                if (_OID != Guid.Empty)
                    sql += "'" + OID + "', null, N'" + fieldsXml.OuterXml + "'";
                else
                    sql += "null, null, N'" + fieldsXml.OuterXml + "', '" + ClassName + "'";

                sql += ", @sysUpdate=1";
                _OID = (Guid)ExecScalar(sql);
            }
            else if (_OID == Guid.Empty)
            {
                String sql = "spCreateObject '" + ClassName + "'";
                _OID = (Guid)ExecScalar(sql);
            }
        }

        public void Delete()
        {
            if (_OID == Guid.Empty) throw new Exception("object not exist");
            String sql = "exec spDeleteObject '" + _OID.ToString().ToString() + "'";
            try
            {
                ExecSQL(sql);
            }
            catch (Exception e)
            {
                throw new DBException("Delete", e.Message);
            }
        }

        public virtual void DeleteMulti(string propName, object propValue, object ordValue)
        {
            if (_OID == Guid.Empty) throw new Exception("object not exist");

            String sql = "exec spDeleteMulti '" + OID.ToString() + "', '" + propName + "', ";
            if (propValue != null) sql += "'" + ToOurString(propValue) + "'";
            else sql += "null";
            if (ordValue != null) sql += ", '" + ToOurString(ordValue) + "'";
            ExecSQL(sql);
        }

        public virtual void DetachMulti(string propName, Guid propValue)
        {
            if (_OID == Guid.Empty) throw new Exception("object not exist");
            string sql = DBReader.QueryConstructor.BuildDetachMulti(ClassName, propName, _OID, propValue);
            SqlCommand cmd = GetCommand(sql, CommandType.Text);
            ExecCommand(cmd);
        }

        public virtual void AddMulti(string propName, object propValue, object ordValue)
        {
            if (_OID == Guid.Empty) throw new Exception("object not exist");

            String sql = "exec spAddMulti '" + OID.ToString() + "', '" + propName + "', '" + ToOurString(propValue) + "'";
            if (ordValue != null) sql += ", '" + ToOurString(ordValue) + "'";
            ExecSQL(sql);
        }

        public virtual void AttachMulti(string propName, Guid propValue)
        {
            if (_OID == Guid.Empty) throw new Exception("object not exist");

            string sql = DBReader.QueryConstructor.BuildAttachMulti(ClassName, propName, _OID, propValue);
            SqlCommand cmd = GetCommand(sql, CommandType.Text);
            ExecCommand(cmd);
        }

        public virtual void SetMulti(string propName, object propValue, object ordValue)
        {
            if (_OID == Guid.Empty) throw new Exception("object not exist");

            String sql = "exec spSetMulti '" + OID.ToString() + "', '" + propName + "', '" + ToOurString(propValue) + "'";
            if (ordValue != null) sql += ", '" + ToOurString(ordValue) + "'";
            ExecSQL(sql);
        }

        protected SqlCommand GetCommand(string sql, CommandType type)
        {
            SqlCommand cmd = new SqlCommand("", _conn, _trans);
            cmd.CommandText = sql;
            cmd.CommandType = type;
            return cmd;
        }

        protected void ExecSQL(String sql)
        {
            bool isPrevOpened = true;
            if (_conn == null || _conn.State != ConnectionState.Open)
            {
                isPrevOpened = false;
                if (_conn == null) _conn = new SqlConnection(_connStr);
                _conn.Open();
            }
            SqlCommand cmd = new SqlCommand("", _conn, _trans);
            cmd.CommandText = sql;
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new DBException(cmd.CommandText, e.Message);
            }
            finally
            {
                if (!isPrevOpened) _conn.Close();
            }
        }

        protected void ExecCommand(SqlCommand cmd)
        {
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new DBException(cmd.CommandText, e.Message);
            }
        }

        protected object ExecScalar(String sql)
        {
            bool isPrevOpened = true;
            if (_conn == null || _conn.State != ConnectionState.Open)
            {
                isPrevOpened = false;
                if (_conn == null) _conn = new SqlConnection(_connStr);
                _conn.Open();
            }
            SqlCommand cmd = new SqlCommand("", _conn, _trans);
            cmd.CommandText = sql;
            try
            {
                return cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                throw new DBException(cmd.CommandText, e.Message);
            }
            finally
            {
                if (!isPrevOpened) _conn.Close();
            }
        }

        protected object GetValue(SqlDataReader r, String fieldName)
        {
            int i = r.GetOrdinal(fieldName);
            if (!r.IsDBNull(i))
                return r[i];
            else
            {
                if (r.GetFieldType(i) == System.Type.GetType("System.String")) return "";
                else if (r.GetFieldType(i) == System.Type.GetType("System.DateTime")) return DateTime.MinValue;
                else if (r.GetFieldType(i) == System.Type.GetType("System.Int32")) return int.MinValue;
                else if (r.GetFieldType(i) == System.Type.GetType("System.Double")) return double.MinValue;
                else if (r.GetFieldType(i) == System.Type.GetType("System.Decimal")) return decimal.MinValue;
                else if (r.GetFieldType(i) == System.Type.GetType("System.Guid")) return Guid.Empty;
                else if (r.GetFieldType(i) == System.Type.GetType("System.Boolean")) return false;
                else if (r.GetFieldType(i) == System.Type.GetType("System.Byte[]")) return null;
                else return null;
            }
        }

        public string ToOurString(object obj)
        {
            return DBReader.DBHelper.ToOurString(obj);
        }
    }

    public class DBException : Exception 
	{
		public string action;
		public DBException(string Action, string Message): base(Message) 
		{
			action = Action;
		}
	}
}