﻿using System;
using System.Collections.Generic;

namespace NoName.Domain
{
	public class Comment 
	{
        public Guid OID { get; set; }
        public DateTime DateCreate { get; set; }
        public string FirstName { get; set; }
        public string SelfComment { get; set; }
	}
}
