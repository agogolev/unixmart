﻿using System;
using System.Collections.Generic;
using System.Web;
using Ecommerce.Extensions;

namespace NoName.Domain
{
	public class NodeMenu
	{
        public string Name { get; set; }
        public virtual string MenuUrl
		{
			get { return Url; }
		}

	    public HtmlString Text
	    {
	        get { return Name.ToHtml(); }
	    }

	    public string Url { get; set; }
	    public string Attr { get; set; }
        public int Id { get; set; }
		public int Lft { get; set; }
		public int Rgt { get; set; }
		public Guid ObjectOID { get; set; }
		public int ParentId { get; set; }
		public bool Selected { get; set; }
        public DateTime DateModify { get; set; }
        public IList<NodeMenu> ChildMenu { get; set; }
	}
}
