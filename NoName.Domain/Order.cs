﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;

namespace NoName.Domain
{
	public class Order
	{
		public Guid OID { get; set; }
		public int OrderNum { get; set; }
		public int Status { get; set; }
		public string City { get; set; }
		public string Street { get; set; }
		public string MetroName { get; set; }
		public string House { get; set; }
		public string Build { get; set; }
		public string Korp { get; set; }
		public string Flat { get; set; }
        public string Comments { get; set; }
		public string Code { get; set; }
		public string Phone { get; set; }
		public string Person { get; set; }
		public string EMail { get; set; }
		public int? PayType { get; set; }
		public string PayName { get; set; }
        public int DeliveryType { get; set; }
        public string DeliveryName { get; set; }
        public decimal DeliveryPrice { get; set; }
        public int CalcDistance { get; set; }
        public bool ShowAddress { get; set; }
        public DateTime DateCreate { get; set; }
        public Coupon Coupon { get; set; }
	    public string ApprovalCode { get; set; }
		public IList<OrderContent> Items { get; set; }
		public decimal TotalPrice
		{
			get
			{
				decimal price = 0;
				if (Items != null) price += Items.Sum(item => item.Amount*item.Price);
			    return price + DeliveryPrice;
			}
		}

	    public string FullAddress
	    {
	        get
	        {
	            StringBuilder stringBuilder = new StringBuilder();
	            if (!string.IsNullOrEmpty(City))
	                stringBuilder.AppendFormat("{0}, ", City);
	            if (!string.IsNullOrEmpty(Street))
	                stringBuilder.AppendFormat("{0}, ", Street);
	            if (!string.IsNullOrEmpty(House))
	                stringBuilder.AppendFormat("дом {0}, ", House);
	            if (!string.IsNullOrEmpty(Flat))
	                stringBuilder.AppendFormat("кв. {0}, ", Flat);
	            if (stringBuilder.Length >= 2)
	                stringBuilder.Length -= 2;
	            return stringBuilder.ToString().Trim();
	        }
	    }

        public int Referer { get; set; }
        public string RefererName { get; set; }
    }
}
