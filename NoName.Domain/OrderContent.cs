﻿using System;
using Ecommerce.Extensions;
using System.Web;

namespace NoName.Domain
{
	public class OrderContent
	{
		public int OrdValue { get; set; }
		public string PseudoID { get; set; }
		public Guid Goods { get; set; }
		public int ObjectID { get; set; }
	    public int? PostavshikID { get; set; }
        public string CategoryFullName { get; set; }
		public string ProductType { get; set; }
		public string Model { get; set; }
		public string ManufacturerName { get; set; }
		public int Amount { get; set; }
		public decimal Price { get; set; }
		public decimal FullPrice => Amount * Price;
	    public string FullName => $"{(ProductType ?? string.Empty).Trim()} {(ManufacturerName ?? string.Empty).Trim()} {(Model ?? string.Empty).Trim()}";
	    private string itemUrl;
		public string ItemUrlTranslit => itemUrl ??
		    (itemUrl =
	        $"{(ProductType ?? string.Empty).Trim().TranslitEncode(true)}-{(ManufacturerName ?? string.Empty).Trim().TranslitEncode(true)}-{(Model ?? string.Empty).Trim().TranslitEncode(true)}");

	    public HtmlString PriceString => new HtmlString($"{Price:#,#0} руб.");
	}
}
