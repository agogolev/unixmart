﻿using System;
using System.Collections.Generic;
using System.Web;
using Ecommerce.Extensions;

namespace NoName.Domain
{
	public class Article
	{
		public Guid OID { get; set; }
        
        public HtmlString Header { get; set; }
        
        public string HeaderUrl { get { return Header.ToString().TranslitEncode(true); } }
		
        public HtmlString Annotation { get; set; }
		
        public DateTime PubDate { get; set; }
		
        public HtmlString Body { get; set; }
		
        public int Id { get; set; }
		
        public DateTime DateModify { get; set; }
		
        public Image Image { get; set; }
		
        public string Author { get; set; }
		
        public IList<string> Tags { get; set; }
        
        public bool HasYandexMap { get; set; }

        public string Title { get; set; }

        public string Keywords { get; set; }

        public string PageDescription { get; set; }
	}
}
