﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NoName.Domain.Enums
{
	public enum SiteMapItemChangeFreq
	{
		/// <summary>
		///  should be used to describe documents that change each time they are accessed
		/// </summary>
		Always,
		Hourly,
		Daily,
		Weekly,
		Monthly,
		Yearly,
		/// <summary>
		/// should be used to describe archived URLs
		/// </summary>
		Never,
		None
	}
}
