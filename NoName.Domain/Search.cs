﻿using System.Collections.Generic;
using System;

namespace NoName.Domain
{
	public class Search
	{
		public Search()
		{
			PageNum = 1;
			NumOnPage = 10;
		}
		public string QueryText { get; set; }
		public int ObjectID { get; set; }
		public int GoodsCount { get; set; }
		public int NumOnPage { get; set; }
		public int PageNum { get; set; }
		public string SortType { get; set; }
		public int PageCount
		{
			get { return NumOnPage != 0 ? (int)Math.Ceiling((decimal) GoodsCount/NumOnPage) : 0; }
		}
		public IList<GoodsItem> GoodsItems { get; set; }
	}
}
