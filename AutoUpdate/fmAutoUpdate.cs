﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace AutoUpdate
{
	/// <summary>
	/// Summary description for AutoUpdate.
	/// </summary>
	public class fmAutoUpdate : System.Windows.Forms.Form	{
		private System.Windows.Forms.Label lblWait;
		private System.Windows.Forms.Label lbUpdate;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnCancel;

		const int ERROR_FILE_NOT_FOUND = 2;
		const int ERROR_ACCESS_DENIED = 5;

		private FileProvider.FileProvider provider = Classes.ServiceUtility.FileProvider;
		private static bool updateOnly;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public fmAutoUpdate()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lblWait = new System.Windows.Forms.Label();
            this.lbUpdate = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblWait
            // 
            this.lblWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWait.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWait.Location = new System.Drawing.Point(0, 0);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(350, 50);
            this.lblWait.TabIndex = 0;
            this.lblWait.Text = "Пожалуйста подождите, идёт обновление системы!";
            this.lblWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblWait.Visible = false;
            // 
            // lbUpdate
            // 
            this.lbUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbUpdate.Location = new System.Drawing.Point(0, 0);
            this.lbUpdate.Name = "lbUpdate";
            this.lbUpdate.Size = new System.Drawing.Size(350, 50);
            this.lbUpdate.TabIndex = 1;
            this.lbUpdate.Text = "Приступить к обновлению?";
            this.lbUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnStart.Location = new System.Drawing.Point(254, 16);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(90, 27);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Да";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnCancel.Location = new System.Drawing.Point(10, 16);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 27);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Выйти";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // fmAutoUpdate
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(350, 50);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lbUpdate);
            this.Controls.Add(this.lblWait);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fmAutoUpdate";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Система автоматического обновления";
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if (args == null || args.Length < 1) {
				updateOnly = true;
			}

			if (InstanceExistsAU()) return;
			if (InstanceExistsStock())
			{
				mutexStock.WaitOne();
				mutexStock.ReleaseMutex();
			} 

			Application.Run(new fmAutoUpdate());
		}

		static private Mutex mutexAU, mutexStock;
		private static string strMutexUpdate = "invMutexELBClientU";
		private static string strMutexApp = "invMutexELBClient";
		private static string strExeFile = "ELBClient.exe";
		static private bool InstanceExistsAU()
		{ 
			bool createdNew; 
			mutexAU = new Mutex(true, strMutexUpdate, out createdNew );
			return !createdNew;
		}
		static private bool InstanceNotExistsStock() 
		{ 
			bool createdNew; 
			mutexStock = new Mutex(true, strMutexApp, out createdNew );
			return createdNew;
		}

		static private bool InstanceExistsStock() {
			bool createdNew;
			mutexStock = new Mutex(true, strMutexApp, out createdNew);
			return !createdNew;
		}

		private void downloadAll()
		{
			string [] fileNames;
			FileProvider.ExceptionISM ex = null;
			try
			{
				fileNames = provider.GetAllFileNames(ref ex);
				if(ex != null && fileNames == null)
					MessageBox.Show(this,"Ошибка получения списка файлов!\n"+ex.LiteralMessage,"Ошибка!",MessageBoxButtons.OK,MessageBoxIcon.Error);
				else
				{
					foreach(string fileName in fileNames)
					{
						if(fileName != "AutoUpdate.exe" && fileName != "ELBClient.exe")
						{
							try
							{
								download(fileName);
							}
							catch(Exception e)
							{
								MessageBox.Show(this,e.Message,"Ошибка!",MessageBoxButtons.OK,MessageBoxIcon.Error);
							}
						}
						try
						{
							download("ELBClient.exe");
						}
						catch (Exception e)
						{
							MessageBox.Show(this, e.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
						}
					}
				}
			}
			catch(Exception e)
			{
				MessageBox.Show(this,e.Message,"Ошибка!",MessageBoxButtons.OK,MessageBoxIcon.Error);	
			}
		}


		private void download(string fileName)
		{
			//Download
			byte [] data;
			FileProvider.ExceptionISM ex = null;
			data = provider.DownloadFile(fileName, ref ex);
			if(ex != null && data == null)
				MessageBox.Show(this,"Ошибка скачки файла!\n"+ex.LiteralMessage,"Ошибка!",MessageBoxButtons.OK,MessageBoxIcon.Error);
			else 
			{
				string dir = Application.StartupPath;
				try
				{
					string[] dirs = fileName.Split('/');
					if(dirs.Length > 1)
					{
						string subDirs = "";
						for(int i =0; i < dirs.Length - 1; i++)
						{						
							subDirs +=  "/"+dirs[i];
							DirectoryInfo di = new DirectoryInfo(dir+subDirs);
							if (di.Exists) 
								continue;

							di.Create();
						}
					}
					FileStream fs = new FileStream(dir+"/"+fileName, FileMode.Create);
					fs.Write(data, 0, data.Length);
					fs.Close();
				}
				catch(Exception exc)
				{
					MessageBox.Show(this,exc.Message,"Ошибка!",MessageBoxButtons.OK,MessageBoxIcon.Error);
				}
			}
		}

		private void btnStart_Click(object sender, System.EventArgs e)
		{
			this.lbUpdate.Visible = false;
			this.btnCancel.Visible = false;
			this.btnStart.Visible = false;
			this.lblWait.Visible = true;
			downloadAll();//update
			//заканчиваем этот процесс, стартуем сток
			closeApp();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void closeApp()
		{
			try {
				if (!updateOnly) {
					Process p = Process.Start(strExeFile);
				}
				else {
					MessageBox.Show(this, "Обновление закончено!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch(Win32Exception e)
			{
				if(e.NativeErrorCode == ERROR_FILE_NOT_FOUND)
					MessageBox.Show("Файл запуска приложения не найден!","Ошибка!",MessageBoxButtons.OK, MessageBoxIcon.Error);
				else if (e.NativeErrorCode == ERROR_ACCESS_DENIED)
					MessageBox.Show("У Вас недостаточно прав для запуска программы!\nСвяжитесь с администратором.","Ошибка!",MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally 
			{
				//Close process by sending a close message to its main window
				Process.GetCurrentProcess().CloseMainWindow();
				//Free resources associated with process.
				Process.GetCurrentProcess().Close();
			}
		}

	}
}
