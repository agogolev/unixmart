﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Net;

namespace AutoUpdate.Classes {
	/// <summary>
	/// Summary description for ServiceUtility.
	/// </summary>
	public class ServiceUtility {

		public static FileProvider.FileProvider FileProvider {
			get {
				FileProvider.FileProvider provider = new FileProvider.FileProvider();
				provider.Url = GetConnection(provider.Url);
				provider.Proxy = ConfigProxy();
				return provider;
			}
		}

		private static IWebProxy ConfigProxy() {
			if(UseProxy()) {
				string proxyAddr = ConfigurationManager.AppSettings["proxyAddr"];
				IWebProxy wProxy;
				if(proxyAddr == "default") wProxy = WebRequest.DefaultWebProxy;
				else wProxy = new WebProxy(proxyAddr,true);
				string proxyAuth = ConfigurationManager.AppSettings["proxyAuth"];
				if(proxyAuth == "integrated")	wProxy.Credentials = CredentialCache.DefaultCredentials; 
				else {
					string login = ConfigurationManager.AppSettings["login"];
					string domain = ConfigurationManager.AppSettings["domain"];
					string password = ConfigurationManager.AppSettings["password"];
					wProxy.Credentials = new NetworkCredential(login, password, domain);
				}
				return wProxy;
			}
			else return null;
		}
		public static bool UseProxy() {
			foreach(string key in ConfigurationManager.AppSettings.Keys) {
				if(key == "proxyAddr") return true;
			}
			return false;
		}

		private static string GetConnection(string url)
        {
            var serviceLocation = ConfigurationManager.AppSettings["connection"];
            var re = new Regex("https?://([^/]*)/", RegexOptions.IgnoreCase);
            return re.Replace(url, serviceLocation + "/");
        }
	}
}
